package com.paytm.datamanager;

public class FundThreadManager {

    public static ThreadLocal<FundManager> fundManagerThreadLocal = new ThreadLocal<FundManager>();

    public static FundManager getInstance(){
        if(fundManagerThreadLocal.get()==null){
            FundManager fundManager = new FundManager();
            fundManagerThreadLocal.set(fundManager);
        }
        return fundManagerThreadLocal.get();
    }


    public static void removeInstance(){
        fundManagerThreadLocal.remove();
    }


}
