package com.paytm.datamanager;

import com.paytm.constants.Constants;
import com.paytm.constants.DBQueries;
import com.paytm.enums.DBEnums;
import com.paytm.enums.UserHeaders;
import com.paytm.enums.UserTypes;
import com.paytm.framework.core.ExtentManager;
import com.paytm.framework.datareader.DataReaderUtil;
import com.paytm.framework.datawriter.DataWriterUtil;
import com.paytm.framework.utils.DatabaseUtil;
import com.paytm.framework.utils.PropertyUtil;
import com.paytm.helpers.AuthHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.utils.DBValidation;

import java.util.*;

public class UserManager {

	private static UserManager instance;
	private static LinkedHashMap<String, LinkedHashMap<String, String>> allUserData = new LinkedHashMap<>();
	private static LinkedHashMap<String, LinkedList<String>> phoneUserMap = new LinkedHashMap<>();
	private static LinkedHashMap<Long, String> payeePhoneMap = new LinkedHashMap<>();
	private static LinkedHashMap<Long, String> payerPhoneMap = new LinkedHashMap<>();
	private static volatile List<String> tokenValidated = new LinkedList<>();

	public static UserManager getInstance() {
		if (instance == null) {
			synchronized (UserManager.class) {
				if (instance == null) {
					instance = new UserManager();
				}
			}
		}
		return instance;
	}

	public void readUserData() {
		PropertyUtil.getInstance().load("UserAttributeMapping.properties");
		String[][] userData = DataReaderUtil.readCSV("UsersData.csv", "UserData");
		LinkedHashMap<String, String> sampleUserData = new LinkedHashMap<>();
		for (int i = 1; i < userData.length; i++) {
			LinkedHashMap<String, String> data = new LinkedHashMap<>();
			for (int j = 0; j < userData[0].length; j++) {
				data.put(userData[0][j], userData[i][j]);
				if (i == 1) {
					sampleUserData.put(userData[0][j], "");
				}
			}
			allUserData.put(userData[i][0], data);
		}
		for (Object key : PropertyUtil.getInstance().getKeySet()) {
			List<String> userTypes = new ArrayList<>(
					Arrays.asList(PropertyUtil.getInstance().getValue(key.toString()).split(",", -1)));
			for (String userType : userTypes) {
				LinkedList<String> phoneList = new LinkedList<>();
				if (phoneUserMap.containsKey(userType)) {
					phoneList = phoneUserMap.get(userType);
				}
				phoneList.add(key.toString());
				phoneUserMap.put(userType, phoneList);
			}
			if (allUserData.get(key) == null) {
				LinkedHashMap<String, String> sampleDataUse = new LinkedHashMap<String, String>(sampleUserData);
				sampleDataUse.put("MOBILENUMBER", key.toString());
				allUserData.put(key.toString(), sampleDataUse);
			}
		}
	}

	/**
	 * Assign payer to the TC with lock
	 *
	 * @param userTypes - List of different UserTypes
	 * @return - Mobile Number
	 */
	public String getNewPayerUser(UserTypes... userTypes) {
		int runCount = 0;
		if (allUserData.size() == 0 || phoneUserMap.size() == 0) {
			readUserData();
		}
		LinkedList<String> phoneNumberList = getPhoneNumberList(userTypes);
		if (phoneNumberList.size() != 0) {
			while (runCount < Constants.SystemVariables.waitTimeInMinutes) {
				for (String phone : phoneNumberList) {
					LinkedHashMap<Long, String> tempPayeePhoneMap = new LinkedHashMap<>();
					LinkedHashMap<Long, String> tempPayerPhoneMpa = new LinkedHashMap<>();
					tempPayeePhoneMap.putAll(payeePhoneMap);
					tempPayerPhoneMpa.putAll(payerPhoneMap);
					if (!containsPhone(tempPayeePhoneMap, phone) && !containsPhone(tempPayerPhoneMpa, phone)) {
						synchronized (this) {
							if (!containsPhone(payeePhoneMap, phone) && !containsPhone(payerPhoneMap, phone)) {
								payerPhoneMap.put(Thread.currentThread().getId(), phone);
								ExtentManager.logInfo("Payer assigned is: " + phone, "extent");
								System.out.println("Payer phone is: " + phone);
								return phone;
							}
						}
					}
				}
				try {
					Thread.sleep(Constants.SystemVariables.waitTimeCounter);
				} catch (Exception e) {
				}
				runCount++;
			}
		}
		throw new RuntimeException("No free user...");
	}

	/**
	 * Provide user to the TC without lock
	 *
	 * @param userTypes - List of different UserTypes
	 * @return - Mobile Number
	 */
	public String getUserWithoutLock(UserTypes... userTypes) {
		int runCount = 0;
		if (allUserData.size() == 0 || phoneUserMap.size() == 0) {
			readUserData();
		}
		LinkedList<String> phoneNumberList = getPhoneNumberList(userTypes);
		if (phoneNumberList.size() != 0) {
			while (runCount < Constants.SystemVariables.waitTimeInMinutes) {
				for (String phone : phoneNumberList) {
					LinkedHashMap<Long, String> tempPayeePhoneMap = new LinkedHashMap<>();
					LinkedHashMap<Long, String> tempPayerPhoneMpa = new LinkedHashMap<>();
					tempPayeePhoneMap.putAll(payeePhoneMap);
					tempPayerPhoneMpa.putAll(payerPhoneMap);
					if (!containsPhone(tempPayeePhoneMap, phone) && !containsPhone(tempPayerPhoneMpa, phone)) {
						synchronized (this) {
							if (!containsPhone(payeePhoneMap, phone) && !containsPhone(payerPhoneMap, phone)) {
								ExtentManager.logInfo("Payer assigned is: " + phone, "extent");
								System.out.println("Payer phone is: " + phone);
								return phone;
							}
						}
					}
				}
				try {
					Thread.sleep(Constants.SystemVariables.waitTimeCounter);
				} catch (Exception e) {
				}
				runCount++;
			}
		}
		throw new RuntimeException("No free user...");
	}

	/**
	 * Assign payee to the TC with lock
	 *
	 * @param userTypes - List of different UserTypes
	 * @return - Mobile Number
	 */
	public String getNewPayeeUser(UserTypes... userTypes) {
		int runCount = 0;
		if (allUserData.size() == 0 || phoneUserMap.size() == 0) {
			readUserData();
		}
		LinkedList<String> phoneNumberList = getPhoneNumberList(userTypes);
		if (phoneNumberList.size() != 0) {
			while (runCount < Constants.SystemVariables.waitTimeInMinutes) {
				for (String phone : phoneNumberList) {
					LinkedHashMap<Long, String> tempPayeePhoneMap = new LinkedHashMap<>();
					LinkedHashMap<Long, String> tempPayerPhoneMpa = new LinkedHashMap<>();
					tempPayeePhoneMap.putAll(payeePhoneMap);
					tempPayerPhoneMpa.putAll(payerPhoneMap);
					if (!containsPhone(tempPayeePhoneMap, phone) && !containsPhone(tempPayerPhoneMpa, phone)) {
						synchronized (this) {
							if (!containsPhone(payeePhoneMap, phone) && !containsPhone(payerPhoneMap, phone)) {
								payeePhoneMap.put(Thread.currentThread().getId(), phone);
								ExtentManager.logInfo("Payee assigned is: " + phone, "extent");
								System.out.println("Payee phone is: " + phone);
								return phone;
							}
						}
					}
				}
				try {
					Thread.sleep(Constants.SystemVariables.waitTimeCounter);
				} catch (Exception e) {
				}
				runCount++;
			}
		}
		throw new RuntimeException("No free user...");
	}

	/**
	 *
	 * @param userTypes - List of userTypes
	 * @return - Mobile Number list with the list of userTypes
	 */
	private LinkedList<String> getPhoneNumberList(UserTypes... userTypes) {
		LinkedList<String> phoneNumberList = new LinkedList<>();
		List<UserTypes> flaggedUserTypeList = getUserTypeList();
		for (UserTypes userType : userTypes) {
			if (flaggedUserTypeList.contains(userType)) {
				flaggedUserTypeList.remove(userType);
			}
			if (phoneNumberList.size() == 0) {
				phoneNumberList = new LinkedList<>(phoneUserMap.get(userType.toString()));
			} else {
				phoneNumberList.retainAll(phoneUserMap.get(userType.toString()));
			}
		}
		for (UserTypes userType : flaggedUserTypeList) {
			if (phoneUserMap.get(userType.toString()) != null && phoneUserMap.get(userType.toString()).size() != 0) {
				phoneNumberList.removeAll(phoneUserMap.get(userType.toString()));
			}
		}
		return phoneNumberList;
	}

	private List<UserTypes> getUserTypeList() {
		List<UserTypes> flaggedList = new LinkedList<>();
		List<UserTypes> listUserTypes = Arrays.asList(UserTypes.values());
		for (UserTypes userType : listUserTypes) {
			if (userType.getFlag()) {
				flaggedList.add(userType);
			}
		}
		return flaggedList;
	}

	public String getSpecificUserDetail(UserHeaders attribute, String phoneNumber) {
		LinkedHashMap<String, String> userData = allUserData.get(phoneNumber);
		String requiredData = userData.get(attribute.toString());
		if (Constants.SystemVariables.TESTING_MOCK.toString().equals("false")) {
			if (!tokenValidated.contains(phoneNumber) && (attribute.equals(UserHeaders.WALLETSCOPETOKEN)
					|| attribute.equals(UserHeaders.PAYTMSCOPETOKEN))) {
				AuthHelpers authHelpers = new AuthHelpers();
				String tokenStatus = authHelpers.getTokenStatus(userData.get(attribute.toString()));
				if (tokenStatus.contains("Not a valid token") || tokenStatus.contains("Token missing")) {
					userData.put(attribute.toString(), "");
					requiredData = "";
				}
				tokenValidated.add(phoneNumber);
			}
		} else {
			String query = "select * from Token where phone = '" + phoneNumber + "';";
			List<Map<String, Object>> list = DatabaseUtil.getInstance()
					.executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, query);
			if (list.size() == 0) {
				String dbQuery = "INSERT INTO Token VALUES('" + userData.get(UserHeaders.WALLETSCOPETOKEN.toString())
						+ "','" + userData.get(UserHeaders.CUSTID.toString()) + "','" + phoneNumber + "');";
				DatabaseUtil.getInstance().executeUpdateQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL,
						dbQuery);
			}
		}
		if (requiredData.equalsIgnoreCase("")) {
			synchronized (this) {
				requiredData = userData.get(attribute.toString());
				if (requiredData.equalsIgnoreCase("")) {
					addMissingData(attribute, phoneNumber);
					requiredData = allUserData.get(phoneNumber).get(attribute.toString());
				}
			}
		}
		return requiredData;
	}

	public String getPayeeUserDetails(UserHeaders attribute) {
		Long threadId = Thread.currentThread().getId();
		if (payeePhoneMap.containsKey(threadId)) {
			String phoneNumber = payeePhoneMap.get(threadId);
			String requiredData = getSpecificUserDetail(attribute, phoneNumber);
			return requiredData;
		} else {
			throw new RuntimeException("No user mapped to the running thread...");
		}
	}

	public String getPayerUserDetails(UserHeaders attribute) {
		Long threadId = Thread.currentThread().getId();
		if (payerPhoneMap.containsKey(threadId)) {
			String phoneNumber = payerPhoneMap.get(threadId);
			String requiredData = getSpecificUserDetail(attribute, phoneNumber);
			return requiredData;
		} else {
			throw new RuntimeException("No user mapped to the running thread...");
		}
	}

	public synchronized void releaseUser() {
		Long threadId = Thread.currentThread().getId();
		if (payerPhoneMap.containsKey(threadId)) {
			tokenValidated.remove(payerPhoneMap.get(threadId));
			payerPhoneMap.remove(threadId);
		}
		if (payeePhoneMap.containsKey(threadId)) {
			tokenValidated.remove(payeePhoneMap.get(threadId));
			payeePhoneMap.remove(threadId);
		}
	}

	public void writeUserData() {
		LinkedList<String[]> list = new LinkedList<>();
		for (String key : allUserData.keySet()) {
			if (list.size() == 0) {
				String[] value = new String[allUserData.get(key).size()];
				int j = 0;
				for (String key1 : allUserData.get(key).keySet()) {
					value[j] = key1;
					j++;
				}
				list.add(value);
			}
			String[] value = new String[allUserData.get(key).size()];
			int j = 0;
			for (String key1 : allUserData.get(key).keySet()) {
				value[j] = allUserData.get(key).get(key1);
				j++;
			}
			list.add(value);
		}
		DataWriterUtil.writeCSV(System.getProperty("user.dir") + "/src/main/resources/TestData/UsersData", "UserData",
				list);
	}

	private Boolean containsPhone(HashMap<Long, String> map, String phone) {
		for (Long key : map.keySet()) {
			if (map.get(key).equalsIgnoreCase(phone)) {
				return true;
			}
		}
		return false;
	}


    private void addMissingData(UserHeaders attribute, String phoneNumber) {
        if (attribute.equals(UserHeaders.WALLETSCOPETOKEN)) {
            AuthHelpers authHelpers = new AuthHelpers();
            allUserData.get(phoneNumber).put(attribute.toString(), authHelpers.getWalletAccessToken(phoneNumber));
        }else if (attribute.equals(UserHeaders.PAYTMSCOPETOKEN)) {
            AuthHelpers authHelpers = new AuthHelpers();
            allUserData.get(phoneNumber).put(attribute.toString(), authHelpers.getPaytmAccessToken(phoneNumber));
        }else if(attribute.equals(UserHeaders.WALLETID)){
            WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
            allUserData.get(phoneNumber).put(attribute.toString(), walletAPIHelpers.fetchWalletId(phoneNumber));
        }else if(attribute.equals(UserHeaders.CUSTID)){
            WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
            allUserData.get(phoneNumber).put(attribute.toString(), walletAPIHelpers.fetchUserId(phoneNumber));
        }else if(attribute.equals(UserHeaders.WALLETRBITYPE)||attribute.equals(UserHeaders.TRUSTFACTOR)||attribute.equals(UserHeaders.FIRSTNAME)||attribute.equals(UserHeaders.LASTNAME)||attribute.equals(UserHeaders.FLAGGEDMERCHANT)||attribute.equals(UserHeaders.PAN)||attribute.equals(UserHeaders.EMAIL)){
            addBasicUserDetails(phoneNumber);
        }
    }

    private void addBasicUserDetails(String mobileNo){
        DBValidation dbUtils = new DBValidation();
        Map<Object,Object> params = new LinkedHashMap<>();
        params.put(DBEnums.MOBILENO,mobileNo);
        Map<String,Object> result = dbUtils.runDbQuery(DBQueries.getUserType,params,"log4j").get(0);
        allUserData.get(mobileNo).put(UserHeaders.WALLETRBITYPE.toString(),result.get("wallet_rbi_type").toString());
        allUserData.get(mobileNo).put(UserHeaders.TRUSTFACTOR.toString(),result.get("trust_factor").toString());
        if(result.get("first_name")!=null) {
            allUserData.get(mobileNo).put(UserHeaders.FIRSTNAME.toString(), result.get("first_name").toString());
        }
        if(result.get("last_name")!=null) {
            allUserData.get(mobileNo).put(UserHeaders.LASTNAME.toString(), result.get("last_name").toString());
        }
        allUserData.get(mobileNo).put(UserHeaders.FLAGGEDMERCHANT.toString(),result.get("flagged_merchant").toString());
        allUserData.get(mobileNo).put(UserHeaders.PAN.toString(),result.get("pan_verified").toString());
        if(result.get("email_id")!=null) {
            allUserData.get(mobileNo).put(UserHeaders.EMAIL.toString(), result.get("email_id").toString());
        }
    }


}
