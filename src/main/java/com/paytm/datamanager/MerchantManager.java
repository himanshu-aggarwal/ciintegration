package com.paytm.datamanager;

import java.util.LinkedHashMap;

import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.framework.datareader.DataReaderUtil;

public class MerchantManager {

    private static MerchantManager instance;
    private static LinkedHashMap<String, LinkedHashMap<String, String>> allMerchantData = new LinkedHashMap<>();

    public static MerchantManager getInstance() {
        if (instance == null) {
            synchronized (MerchantManager.class) {
                if (instance == null) {
                    instance = new MerchantManager();
                }
            }
        }
        return instance;
    }

    public void readMerchantData() {
        String[][] merchantData = DataReaderUtil.readCSV("MerchantData.csv","MerchantData");        
        for(int i =1;i<merchantData.length;i++){
            LinkedHashMap<String, String> data = new LinkedHashMap<>();
            for(int j=0;j<merchantData[0].length;j++){
                data.put(merchantData[0][j],merchantData[i][j]);
            }
            allMerchantData.put(merchantData[i][1],data);
        }
    }

    public String getMerchantDetails(MerchantTypes MerchantType, MerchantHeaders attribute){
        return allMerchantData.get(MerchantType.toString()).get(attribute.toString());
    }

}
