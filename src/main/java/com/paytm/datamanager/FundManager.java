package com.paytm.datamanager;

import com.paytm.constants.LocalConfig;
import com.paytm.helpers.SubWalletHelpers;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class FundManager {

	private BigDecimal totalAmount = BigDecimal.ZERO;
	private BigDecimal foodWalletAmount = BigDecimal.ZERO;
	private BigDecimal giftWalletAmount = BigDecimal.ZERO;
	private BigDecimal blockedWalletAmount = BigDecimal.ZERO;
	private BigDecimal tollWalletAmount = BigDecimal.ZERO;
	private BigDecimal closedLoopWalletAmount = BigDecimal.ZERO;
	private BigDecimal closedLoopSubWalletAmount = BigDecimal.ZERO;
	private BigDecimal fuelWalletAmount = BigDecimal.ZERO;
	private BigDecimal internationalWalletAmount = BigDecimal.ZERO;
	private BigDecimal giftVoucherAmount = BigDecimal.ZERO;
	private BigDecimal cashBackWalletAmount = BigDecimal.ZERO;
	private BigDecimal mainWalletAmount = BigDecimal.ZERO;
	private Boolean calculateMainWallet = true;
	private BigDecimal commissionAmount=BigDecimal.ZERO;
	private Boolean postConvMerchant = false;
	
	public FundManager withPostConvMerchant(Boolean postConvMerchant){
		this.postConvMerchant=postConvMerchant;
		return this;
	}

	public BigDecimal getCommissionAmount() {
		return commissionAmount;
	}

	@Deprecated
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}

	public FundManager withCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
		return this;
	}
	
	public FundManager withTotalAmount(BigDecimal totalAmount) {
		if(totalAmount!=null) {
			this.totalAmount = totalAmount;
		}
		return this;
	}

	public FundManager withTotalAmount(BigDecimal totalAmount, String merchantGuid) {
		if(totalAmount!=null) {
			this.totalAmount = totalAmount;
			if(merchantGuid!=null&&!merchantGuid.equalsIgnoreCase("")){
				SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
				subWalletHelpers.calculateCommisionValue(merchantGuid,"RETAIL");
			}

		}
		return this;
	}

	public FundManager withFoodWalletAmount(BigDecimal foodWalletAmount) {
		if(foodWalletAmount!=null)
		this.foodWalletAmount = foodWalletAmount.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : foodWalletAmount;
		return this;
	}

	public FundManager withGiftWalletAmount(BigDecimal giftWalletAmount) {
		if(giftWalletAmount!=null)
		this.giftWalletAmount = giftWalletAmount;
		return this;
	}

	public FundManager withBlockedWalletAmount(BigDecimal blockedWalletAmount) {
		if(blockedWalletAmount!=null)
		this.blockedWalletAmount = blockedWalletAmount;
		return this;
	}

	public FundManager withTollWalletAmount(BigDecimal tollWalletAmount) {
		if(tollWalletAmount!=null)
		this.tollWalletAmount = tollWalletAmount;
		return this;
	}

	public FundManager withClosedLoopWalletAmount(BigDecimal closedLoopWalletAmount) {
		if(closedLoopWalletAmount!=null)
		this.closedLoopWalletAmount = closedLoopWalletAmount;
		return this;
	}

	public FundManager withClosedLoopSubWalletAmount(BigDecimal closedLoopSubWalletAmount) {
		if(closedLoopSubWalletAmount!=null)
		this.closedLoopSubWalletAmount = closedLoopSubWalletAmount;
		return this;
	}

	public FundManager withFuelWalletAmount(BigDecimal fuelWalletAmount) {
		if(fuelWalletAmount!=null)
		this.fuelWalletAmount = fuelWalletAmount;
		return this;
	}

	public FundManager withInternationalWalletAmount(BigDecimal internationalWalletAmount) {
		if(internationalWalletAmount!=null)
		this.internationalWalletAmount = internationalWalletAmount;
		return this;
	}

	public FundManager withGiftVoucherAmount(BigDecimal giftVoucherAmount) {
		if(giftVoucherAmount!=null)
		this.giftVoucherAmount = giftVoucherAmount;
		return this;
	}

	public FundManager withCashBackWalletAmount(BigDecimal cashBackWalletAmount) {
		if(cashBackWalletAmount!=null)
		this.cashBackWalletAmount = cashBackWalletAmount;
		return this;
	}

	public FundManager withMainWalletAmount(BigDecimal mainWalletAmount) {
		calculateMainWallet = false;
		if(mainWalletAmount!=null) {
			this.mainWalletAmount = mainWalletAmount;
		}
		return this;
	}

	public BigDecimal getTotalAmount() {
		if(this.postConvMerchant){
			return this.totalAmount.add(this.commissionAmount);
		}else {
			return this.totalAmount;
		}
	}

	public BigDecimal getFoodWalletAmount() {
		return this.foodWalletAmount.setScale(2);
	}

	public BigDecimal getGiftWalletAmount() {
		return this.giftWalletAmount.setScale(2);
	}

	public BigDecimal getBlockedWalletAmount() {
		return this.blockedWalletAmount.setScale(2);
	}

	public BigDecimal getTollWalletAmount() {
		return this.tollWalletAmount.setScale(2);
	}

	public BigDecimal getClosedLoopWalletAmount() {
		return this.closedLoopWalletAmount.setScale(2);
	}

	public BigDecimal getClosedLoopSubWalletAmount() {
		return this.closedLoopSubWalletAmount.setScale(2);
	}

	public BigDecimal getFuelWalletAmount() {
		return this.fuelWalletAmount.setScale(2);
	}

	public BigDecimal getInternationalWalletAmount() {
		return this.internationalWalletAmount.setScale(2);
	}

	public BigDecimal getGiftVoucherAmount() {
		return this.giftVoucherAmount.setScale(2);
	}

	public BigDecimal getCashBackWalletAmount() {
		return this.cashBackWalletAmount.setScale(2);
	}

	public BigDecimal getMainWalletAmount() {
		BigDecimal mainWalletAmount = this.mainWalletAmount;
		if (calculateMainWallet == true)
			mainWalletAmount = getTotalAmount().subtract(this.foodWalletAmount).subtract(this.cashBackWalletAmount)
					.subtract(this.closedLoopSubWalletAmount).subtract(this.closedLoopWalletAmount)
					.subtract(this.fuelWalletAmount).subtract(this.tollWalletAmount)
					.subtract(this.internationalWalletAmount).subtract(this.giftWalletAmount)
					.subtract(this.giftVoucherAmount);
		return mainWalletAmount;
	}

	public BigDecimal getallMainGroupsAmount() {
		BigDecimal amount = getMainWalletAmount().add(this.foodWalletAmount).add(this.tollWalletAmount)
				.add(this.closedLoopSubWalletAmount).add(this.fuelWalletAmount).add(this.internationalWalletAmount);
		return amount;
	}

	public BigDecimal getAggregateGroupsAmount() {
		BigDecimal amount = getMainWalletAmount().add(this.foodWalletAmount).add(this.tollWalletAmount)
				.add(this.closedLoopSubWalletAmount).add(this.fuelWalletAmount).add(this.internationalWalletAmount)
				.add(this.blockedWalletAmount).add(this.giftWalletAmount).add(this.giftVoucherAmount);
		return amount;
	}

	public Map<String, BigDecimal> walletMapViaWalletType() {
		Map<String, BigDecimal> walletMap = new HashMap<>();
		if (getFoodWalletAmount() != null) {
			walletMap.put("FOOD", getFoodWalletAmount());
		}
		if (getFuelWalletAmount() != null) {
			walletMap.put("FUEL", getFuelWalletAmount());
		}
		if (getTollWalletAmount() != null) {
			walletMap.put("TOLL", getTollWalletAmount());
		}
		/*
		 * if(getCashBackWalletAmount()!=null ){ walletMap.put("CASHBACK",
		 * getCashBackWalletAmount()); }
		 */
		if (getClosedLoopSubWalletAmount() != null) {
			walletMap.put("CLOSED_LOOP_SUB_WALLET", getClosedLoopSubWalletAmount());
		}
		if (getClosedLoopWalletAmount() != null) {
			walletMap.put("CLOSED_LOOP_WALLET", getClosedLoopWalletAmount());
		}
		/*
		 * if(getGiftVoucherAmount()!=null){ walletMap.put("GIFT_VOUCHER",
		 * getGiftVoucherAmount()); }
		 */
		if (getGiftWalletAmount() != null) {
			walletMap.put("GIFT", getGiftWalletAmount());
		}
		if (getBlockedWalletAmount() != null) {
			walletMap.put("BLOCKED", getBlockedWalletAmount());
		}
		if (getInternationalWalletAmount() != null) {
			walletMap.put("INTERNATIONAL_FUNDS_TRANSFER", getInternationalWalletAmount());
		}
		if (getMainWalletAmount() != null) {
			walletMap.put("MAIN", getMainWalletAmount());
		}
		return walletMap;
	}

	public Map<String, BigDecimal> walletMapViaWalletId() {
		Map<String, BigDecimal> walletTypeMap = walletMapViaWalletType();
		Map<String, BigDecimal> walletMap = new HashMap<>();
		for (String wallet : walletTypeMap.keySet()) {
			walletMap.put(LocalConfig.subWalletMapping.get(wallet), walletTypeMap.get(wallet));
		}
		return walletMap;
	}

}
