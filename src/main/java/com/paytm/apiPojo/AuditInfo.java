package com.paytm.apiPojo;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.exceptions.JSONExceptions.JSONParseMappingException;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

public class AuditInfo {

	private static String json = "{\"a\": 0.0,\"c\": 0,\"r\": 0,\"o\": 0,\"l\": {\"1\": 0.0,\"2\": 0.0}}";

	public LimitAuditInfo createLimitAuditInfoPojo(String json) {
		try {
			if (null != json) {
				return JacksonJsonImpl.getInstance().fromJson(json, AuditInfo.LimitAuditInfo.class);
			} else {
				LimitAuditInfo limitAuditInfo = new LimitAuditInfo();
				limitAuditInfo.setDayLimit(null);
				limitAuditInfo.setYearlyLimit(null);
				limitAuditInfo.setMonthLimit(null);
				return limitAuditInfo;
			}
		} catch (IOException e) {
			throw new JSONParseMappingException(e);
		}
	}

	public static class LimitAuditInfo {

		private LimitDetail dayLimit;
		private LimitDetail monthLimit;
		private LimitDetail yearlyLimit;

		public LimitDetail getDayLimit() {
			return dayLimit;
		}

		public void setDayLimit(LimitDetail dayLimit) {
			this.dayLimit = dayLimit;
		}

		public LimitDetail getMonthLimit() {
			return monthLimit;
		}

		public void setMonthLimit(LimitDetail monthLimit) {
			this.monthLimit = monthLimit;
		}

		public LimitDetail getYearlyLimit() {
			return yearlyLimit;
		}

		public void setYearlyLimit(LimitDetail yearlyLimit) {
			this.yearlyLimit = yearlyLimit;
		}

		@Override
		public String toString() {
			return "LimitInfo [dayLimit=" + dayLimit + ", monthLimit=" + monthLimit + ", yearlyLimit=" + yearlyLimit
					+ "]";
		}

		public static class LimitDetail {

			private Map<String, List<LimitCounter>> limitCounters = new HashMap<>();

			public Map<String, List<LimitCounter>> getLimitCounters() {
				return limitCounters;
			}

			public void setLimitCounters(Map<String, List<LimitCounter>> limitCounters) {
				this.limitCounters = limitCounters;
			}

			@Override
			public String toString() {
				return "LimitDetail [limitCounters=" + limitCounters + "]";
			}

			public static LimitDetail createDefaultLimitDetail(int txnType, Short operationType) {
				LimitDetail limitDetail = new LimitDetail();
				LimitCounter counter = new LimitCounter();
				limitDetail.setLimitCounters(counter.limitCounterMap(txnType, operationType));
				return limitDetail;
			}

			public static class LimitCounter {

				// TODO-- Add some serious comments about when these values are updated.
				@JsonProperty("a")
				private BigDecimal amount;

				@JsonProperty("c")
				private Integer count;

				@JsonProperty("r")
				private BigDecimal rollbackAmount;

				@JsonProperty("o")
				private Short operationType;

				@JsonProperty("l")
				private Map<Integer, BigDecimal> ppiGroupWiseCounts;

				public LimitCounter() {
					this.amount = BigDecimal.ZERO;
					this.count = 0;
					this.rollbackAmount = BigDecimal.ZERO;
				}

				public LimitCounter(BigDecimal amount, Integer count, Short operationType) {
					this.amount = amount;
					this.count = count;
					this.operationType = operationType;
					this.rollbackAmount = BigDecimal.ZERO;
				}

				public LimitCounter(BigDecimal amount, BigDecimal rollbackAmount, Integer count, Short operationType) {
					this.amount = amount;
					this.count = count;
					this.operationType = operationType;
					this.rollbackAmount = rollbackAmount;
				}

				public LimitCounter(BigDecimal amount, Integer count) {
					this.amount = amount;
					this.count = count;
					this.rollbackAmount = BigDecimal.ZERO;
				}

				public Integer getCount() {
					return count;
				}

				public void setCount(Integer count) {
					this.count = count;
				}

				public BigDecimal getAmount() {
					return amount;
				}

				public void setAmount(BigDecimal amount) {
					this.amount = amount;
				}

				public Short getOperationType() {
					return operationType;
				}

				public void setOperationType(Short operationType) {
					this.operationType = operationType;
				}

				public BigDecimal getRollbackAmount() {
					return rollbackAmount;
				}

				public void setRollbackAmount(BigDecimal rollbackAmount) {
					this.rollbackAmount = rollbackAmount;
				}

				public Map<Integer, BigDecimal> getPpiGroupWiseCounts() {
					return ppiGroupWiseCounts;
				}

				public void setPpiGroupWiseCounts(Map<Integer, BigDecimal> ppiGroupWiseCounts) {
					this.ppiGroupWiseCounts = ppiGroupWiseCounts;
				}

				@Override
				public String toString() {
					return "LimitCounter{" + "amount=" + amount + ", count=" + count + ", rollbackAmount="
							+ rollbackAmount + ", operationType=" + operationType + ", ppiGroupWiseCounts="
							+ ppiGroupWiseCounts + '}';
				}

				public LimitCounter createLimitCounter(Short operationType) {
					try {
						LimitCounter counter = JacksonJsonImpl.getInstance().fromJson(json,
								AuditInfo.LimitAuditInfo.LimitDetail.LimitCounter.class);
						counter.setOperationType(operationType);
						return counter;
					} catch (IOException e) {
						throw new JSONParseMappingException(e);
					}
				}

				public Map<String, List<LimitCounter>> limitCounterMap(int txnType, Short operationType) {
					List<LimitCounter> list = new LinkedList<>();
					list.add(createLimitCounter(operationType));
					Map<String, List<LimitCounter>> map = new HashMap<>();
					map.put(Integer.toString(txnType), list);
					return map;
				}
			}
		}
	}

	public static void main(String[] args) throws IOException {
		AuditInfo audit = new AuditInfo();
		LimitAuditInfo limit = audit.createLimitAuditInfoPojo(json);
		System.out.println("Limit is " + limit);
	}

}
