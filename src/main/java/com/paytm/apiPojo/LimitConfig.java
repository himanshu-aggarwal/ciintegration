package com.paytm.apiPojo;

import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

import java.io.IOException;
import java.math.BigDecimal;

public class LimitConfig {

	private PeriodLimits periodLimits;
	private BigDecimal balance;
	private String ec;

	public LimitConfig createPojo(String json) throws IOException {
		return JacksonJsonImpl.getInstance().fromJson(json, LimitConfig.class);
	}
	
	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	public String getEc() {
		return ec;
	}

	public void setEc(String ec) {
		this.ec = ec;
	}

	public PeriodLimits getPeriodLimits() {
		return periodLimits;
	}

	public void setPeriodLimits(PeriodLimits periodLimits) {
		this.periodLimits = periodLimits;
	}

	public class PeriodLimits {

		private PeriodLimit dayPeriodLimit;
		private PeriodLimit monthPeriodLimit;
		private PeriodLimit yearPeriodLimit;

		public PeriodLimit getDayPeriodLimit() {
			return dayPeriodLimit;
		}

		public void setDayPeriodLimit(PeriodLimit dayPeriodLimit) {
			this.dayPeriodLimit = dayPeriodLimit;
		}

		public PeriodLimit getMonthPeriodLimit() {
			return monthPeriodLimit;
		}

		public void setMonthPeriodLimit(PeriodLimit monthPeriodLimit) {
			this.monthPeriodLimit = monthPeriodLimit;
		}

		public PeriodLimit getYearPeriodLimit() {
			return yearPeriodLimit;
		}

		public void setYearPeriodLimit(PeriodLimit yearPeriodLimit) {
			this.yearPeriodLimit = yearPeriodLimit;
		}

		public class PeriodLimit {

			private BigDecimal amount;
			private Integer count;
			private String aec;
			private String cec;

			public BigDecimal getAmount() {
				return amount;
			}

			public void setAmount(BigDecimal amount) {
				this.amount = amount;
			}

			public Integer getCount() {
				return count;
			}

			public void setCount(Integer count) {
				this.count = count;
			}

			public String getAec() {
				return aec;
			}

			public void setAec(String aec) {
				this.aec = aec;
			}

			public String getCec() {
				return cec;
			}

			public void setCec(String cec) {
				this.cec = cec;
			}

		}

	}

	public static void main(String[] args) throws IOException {
		LimitConfig config = new LimitConfig();
		String json = "{\"balance\":1000000000,\"ec\":\"RWL_1000\"}";
		config =  config.createPojo(json);
		System.out.println("schema is " + config.getBalance());
		
	}

}
