package com.paytm.apiPojo;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

public class SubWalletConsolidatedTxn {

	private SubWalletConsolidatedTxn subwallet;

	public SubWalletConsolidatedTxn test(String json) {
		try {
			return subwallet = JacksonJsonImpl.getInstance().fromJson(json, SubWalletConsolidatedTxn.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private Debit debit;
	private Credit credit;

	public Debit getDebit() {
		return debit;
	}

	public void setDebit(Debit debit) {
		this.debit = debit;
	}

	public SubWalletConsolidatedTxn withDebit(Debit debit) {
		this.debit = debit;
		return this;
	}

	public Credit getCredit() {
		return credit;
	}

	public void setCredit(Credit credit) {
		this.credit = credit;
	}

	public SubWalletConsolidatedTxn withCredit(Credit credit) {
		this.credit = credit;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("debit", debit).append("credit", credit).toString();
	}

	public void createDefaultCreditMap(SubWalletConsolidatedTxn subwallet) throws IOException {
		String json = "{\"transaction_count\":0,\"throughput_amount\":0.0}";
		Credit credit = JacksonJsonImpl.getInstance().fromJson(json, SubWalletConsolidatedTxn.Credit.class);
		subwallet.setCredit(credit);
	}

	public void createDefaultDebitMap(SubWalletConsolidatedTxn subwallet) throws IOException {
		String json = "{\"transaction_count\":0,\"throughput_amount\":0.0}";
		Debit debit = JacksonJsonImpl.getInstance().fromJson(json, SubWalletConsolidatedTxn.Debit.class);
		subwallet.setDebit(debit);
	}

	public void createDebitMap(SubWalletConsolidatedTxn subwallet) throws IOException {
		String json = "{\"transaction_count\":1,\"throughput_amount\":10.0}";
		Debit debit = JacksonJsonImpl.getInstance().fromJson(json, SubWalletConsolidatedTxn.Debit.class);
		subwallet.setDebit(debit);
	}

	public static class Credit {

		@JsonProperty("transaction_count")
		private Integer transactionCount;
		@JsonProperty("throughput_amount")
		private BigDecimal throughputAmount;

		public Integer getTransactionCount() {
			return transactionCount;
		}

		public void setTransactionCount(Integer transactionCount) {
			this.transactionCount = transactionCount;
		}

		public Credit withTransactionCount(Integer transactionCount) {
			this.transactionCount = transactionCount;
			return this;
		}

		public BigDecimal getThroughputAmount() {
			return throughputAmount;
		}

		public void setThroughputAmount(BigDecimal throughputAmount) {
			this.throughputAmount = throughputAmount;
		}

		public Credit withThroughputAmount(BigDecimal throughputAmount) {
			this.throughputAmount = throughputAmount;
			return this;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("transactionCount", transactionCount)
					.append("throughputAmount", throughputAmount).toString();
		}

	}

	public static class Debit {

		@JsonProperty("transaction_count")
		private Integer transactionCount;
		@JsonProperty("throughput_amount")
		private BigDecimal throughputAmount;

		public Integer getTransactionCount() {
			return transactionCount;
		}

		public void setTransactionCount(Integer transactionCount) {
			this.transactionCount = transactionCount;
		}

		public Debit withTransactionCount(Integer transactionCount) {
			this.transactionCount = transactionCount;
			return this;
		}

		public BigDecimal getThroughputAmount() {
			return throughputAmount;
		}

		public void setThroughputAmount(BigDecimal throughputAmount) {
			this.throughputAmount = throughputAmount.setScale(2);
		}

		public Debit withThroughputAmount(BigDecimal throughputAmount) {
			this.throughputAmount = throughputAmount;
			return this;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("transactionCount", transactionCount)
					.append("throughputAmount", throughputAmount).toString();
		}

	}

	public static void main(String[] args) throws IOException {
		SubWalletConsolidatedTxn subwallet = new SubWalletConsolidatedTxn();
		subwallet.createDebitMap(subwallet);
		System.out.println(JacksonJsonImpl.getInstance().toJSon(subwallet));

	}

}
