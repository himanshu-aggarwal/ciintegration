package com.paytm.apiPojo.wallet.oldService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class FundCheckResponse extends responsePojo {

	private Object requestGuid;
	private Object orderId;
	private String status;
	private String statusCode;
	private String statusMessage;
	private Response response;
	private Object metadata;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Object getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}

	public Object getOrderId() {
		return orderId;
	}

	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public Object getMetadata() {
		return metadata;
	}

	public void setMetadata(Object metadata) {
		this.metadata = metadata;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public class Response {

		private BigDecimal amount;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
		}
	}

}
