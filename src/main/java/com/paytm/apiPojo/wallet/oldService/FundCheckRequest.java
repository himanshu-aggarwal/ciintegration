package com.paytm.apiPojo.wallet.oldService;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class FundCheckRequest extends requestPojo {

	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public class Request {

		private String walletGUID;
		private String channelType;

		public String getWalletGUID() {
			return walletGUID;
		}

		public void setWalletGUID(String walletGUID) {
			this.walletGUID = walletGUID;
		}

		public String getChannelType() {
			return channelType;
		}

		public void setChannelType(String channelType) {
			this.channelType = channelType;
		}

	}

}
