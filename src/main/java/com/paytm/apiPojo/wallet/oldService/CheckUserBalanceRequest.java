package com.paytm.apiPojo.wallet.oldService;

import java.math.BigDecimal;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;


public class CheckUserBalanceRequest extends requestPojo {

	private Request request;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public CheckUserBalanceRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("request", request).toString();
	}

	public static class Request {

		private String merchantGuid;
		private String mid;
		private String isDetailInfo;
		private String payeePhoneNo;
		private String payeeSsoId;
		private BigDecimal computeAddableAmount;
		private String isClubSubwalletsRequired;

		public String getMerchantGuid() {
			return merchantGuid;
		}

		public void setMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
		}

		public Request withMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
			return this;
		}

		public String getisClubSubwalletsRequired() {
			return isClubSubwalletsRequired;
		}

		public void setisClubSubwalletsRequired(String isClubSubwalletsRequired) {
			this.isClubSubwalletsRequired = isClubSubwalletsRequired;
		}

		public Request withisClubSubwalletsRequired(String isClubSubwalletsRequired) {
			this.isClubSubwalletsRequired = isClubSubwalletsRequired;
			return this;
		}

		public BigDecimal getComputeAddableAmount() {
			return computeAddableAmount;
		}

		public void setComputeAddableAmount(BigDecimal computeAddableAmount) {
			this.computeAddableAmount = computeAddableAmount;
		}

		public Request withComputeAddableAmount(BigDecimal computeAddableAmount) {
			this.computeAddableAmount = computeAddableAmount;
			return this;
		}

		public String getMid() {
			return mid;
		}

		public void setMid(String mid) {
			this.mid = mid;
		}

		public Request withMid(String mid) {
			this.mid = mid;
			return this;
		}

		public String getIsDetailInfo() {
			return isDetailInfo;
		}

		public void setIsDetailInfo(String isDetailInfo) {
			this.isDetailInfo = isDetailInfo;
		}

		public Request withIsDetailInfo(String isDetailInfo) {
			this.isDetailInfo = isDetailInfo;
			return this;
		}

		public String getPayeePhoneNo() {
			return payeePhoneNo;
		}

		public void setPayeePhoneNo(String payeePhoneNo) {
			this.payeePhoneNo = payeePhoneNo;
		}

		public Request withPayeePhoneNo(String payeePhoneNo) {
			this.payeePhoneNo = payeePhoneNo;
			return this;
		}

		public String getPayeeSsoId() {
			return payeeSsoId;
		}

		public void setPayeeSsoId(String payeeSsoId) {
			this.payeeSsoId = payeeSsoId;
		}

		public Request withPayeeSsoId(String payeeSsoId) {
			this.payeeSsoId = payeeSsoId;
			return this;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("merchantGuid", merchantGuid).append("mid", mid)
					.append("isDetailInfo", isDetailInfo).append("payeePhoneNo", payeePhoneNo)
					.append("payeeSsoId", payeeSsoId).toString();
		}

	}

}
