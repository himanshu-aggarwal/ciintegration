package com.paytm.apiPojo.wallet.oldService;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class CheckUserBalanceResponse extends responsePojo {

    private Object requestGuid;
    private Object orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;

    public Object getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public CheckUserBalanceResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public CheckUserBalanceResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CheckUserBalanceResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public CheckUserBalanceResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public CheckUserBalanceResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public CheckUserBalanceResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
                .append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
                .append("response", response).toString();
    }

    public static class Response {

        private BigDecimal totalBalance;
        private BigDecimal addableAmount;
        private BigDecimal paytmWalletBalance;
        private BigDecimal otherSubWalletBalance;
        private String ownerGuid;
        private String walletGrade;
        private String ssoId;
        private List<SubWalletDetailsList> subWalletDetailsList = null;

        public BigDecimal getTotalBalance() {
            return totalBalance;
        }

        public void setTotalBalance(BigDecimal totalBalance) {
            this.totalBalance = totalBalance;
        }

        public Response withTotalBalance(BigDecimal totalBalance) {
            this.totalBalance = totalBalance;
            return this;
        }

        public BigDecimal getPaytmWalletBalance() {
            return paytmWalletBalance;
        }

        public void setPaytmWalletBalance(BigDecimal paytmWalletBalance) {
            this.paytmWalletBalance = paytmWalletBalance;
        }

        public Response withPaytmWalletBalance(BigDecimal paytmWalletBalance) {
            this.paytmWalletBalance = paytmWalletBalance;
            return this;
        }
        
        public BigDecimal getAddableBalance() {
            if(addableAmount==null) {
                return addableAmount;
            }else{
                return addableAmount.setScale(2);
            }
        }

        public void setAddableBalance(BigDecimal addableAmount) {
            this.addableAmount = addableAmount;
        }

        public Response withAddableBalance(BigDecimal addableAmount) {
            this.addableAmount = addableAmount;
            return this;
        }

        public BigDecimal getOtherSubWalletBalance() {
            return otherSubWalletBalance;
        }

        public void setOtherSubWalletBalance(BigDecimal otherSubWalletBalance) {
            this.otherSubWalletBalance = otherSubWalletBalance;
        }

        public Response withOtherSubWalletBalance(BigDecimal otherSubWalletBalance) {
            this.otherSubWalletBalance = otherSubWalletBalance;
            return this;
        }

        public String getOwnerGuid() {
            return ownerGuid;
        }

        public void setOwnerGuid(String ownerGuid) {
            this.ownerGuid = ownerGuid;
        }

        public Response withOwnerGuid(String ownerGuid) {
            this.ownerGuid = ownerGuid;
            return this;
        }

        public String getWalletGrade() {
            return walletGrade;
        }

        public void setWalletGrade(String walletGrade) {
            this.walletGrade = walletGrade;
        }

        public Response withWalletGrade(String walletGrade) {
            this.walletGrade = walletGrade;
            return this;
        }

        public String getSsoId() {
            return ssoId;
        }

        public void setSsoId(String ssoId) {
            this.ssoId = ssoId;
        }

        public Response withSsoId(String ssoId) {
            this.ssoId = ssoId;
            return this;
        }

        public List<SubWalletDetailsList> getSubWalletDetailsList() {
            return subWalletDetailsList;
        }

        public void setSubWalletDetailsList(List<SubWalletDetailsList> subWalletDetailsList) {
            this.subWalletDetailsList = subWalletDetailsList;
        }

        public Response withSubWalletDetailsList(List<SubWalletDetailsList> subWalletDetailsList) {
            this.subWalletDetailsList = subWalletDetailsList;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("totalBalance", totalBalance)
                    .append("paytmWalletBalance", paytmWalletBalance)
                    .append("otherSubWalletBalance", otherSubWalletBalance).append("ownerGuid", ownerGuid)
                    .append("walletGrade", walletGrade).append("ssoId", ssoId)
                    .append("subWalletDetailsList", subWalletDetailsList).toString();
        }

        public static class SubWalletDetailsList {

            private int id;
            private String displayName;
            private BigDecimal balance;
            private String imageUrl;
            private String walletType;
            private String subWalletType;
            private int status;
            private String message;
            private int count;
            private String subWalletName;
            private String issuerMetadata;
            private String expiry;
            private String issuerId;
            private Long ppiDetailsId;
            private boolean isValid;
            private String issuedOn;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public SubWalletDetailsList withId(int id) {
                this.id = id;
                return this;
            }

            public String getDisplayName() {
                return displayName;
            }

            public void setDisplayName(String displayName) {
                this.displayName = displayName;
            }

            public SubWalletDetailsList withDisplayName(String displayName) {
                this.displayName = displayName;
                return this;
            }

            public BigDecimal getBalance() {
                return balance.setScale(2);
            }

            public void setBalance(BigDecimal balance) {
                this.balance = balance;
            }

            public SubWalletDetailsList withBalance(BigDecimal balance) {
                this.balance = balance;
                return this;
            }

            public String getImageUrl() {
                return imageUrl;
            }

            public void setImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
            }

            public SubWalletDetailsList withImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
                return this;
            }

            public String getWalletType() {
                return walletType;
            }

            public void setWalletType(String walletType) {
                this.walletType = walletType;
            }

            public SubWalletDetailsList withWalletType(String walletType) {
                this.walletType = walletType;
                return this;
            }

            public String getSubWalletType() {
                return subWalletType;
            }

            public void setSubWalletType(String subWalletType) {
                this.subWalletType = subWalletType;
            }

            public SubWalletDetailsList withSubWalletType(String subWalletType) {
                this.subWalletType = subWalletType;
                return this;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public SubWalletDetailsList withStatus(int status) {
                this.status = status;
                return this;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public SubWalletDetailsList withMessage(String message) {
                this.message = message;
                return this;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }

            public SubWalletDetailsList withCount(int count) {
                this.count = count;
                return this;
            }

            public String getSubWalletName() {
                return subWalletName;
            }

            public void setSubWalletName(String subWalletName) {
                this.subWalletName = subWalletName;
            }

            public SubWalletDetailsList withSubWalletName(String subWalletName) {
                this.subWalletName = subWalletName;
                return this;
            }

            public String getIssuerMetadata() {
                return issuerMetadata;
            }

            public void setIssuerMetadata(String issuerMetadata) {
                this.issuerMetadata = issuerMetadata;
            }

            public SubWalletDetailsList withIssuerMetadata(String issuerMetadata) {
                this.issuerMetadata = issuerMetadata;
                return this;
            }

            public String getExpiry() {
                return expiry;
            }

            public void setExpiry(String expiry) {
                this.expiry = expiry;
            }

            public SubWalletDetailsList withExpiry(String expiry) {
                this.expiry = expiry;
                return this;
            }

            public String getIssuerId() {
                return issuerId;
            }

            public void setIssuerId(String issuerId) {
                this.issuerId = issuerId;
            }

            public SubWalletDetailsList withIssuerId(String issuerId) {
                this.issuerId = issuerId;
                return this;
            }

            public Long getPpiDetailsId() {
                return ppiDetailsId;
            }

            public void setPpiDetailsId(Long ppiDetailsId) {
                this.ppiDetailsId = ppiDetailsId;
            }

            public SubWalletDetailsList withPpiDetailsId(Long ppiDetailsId) {
                this.ppiDetailsId = ppiDetailsId;
                return this;
            }

            public boolean isIsValid() {
                return isValid;
            }

            public void setIsValid(boolean isValid) {
                this.isValid = isValid;
            }

            public SubWalletDetailsList withIsValid(boolean isValid) {
                this.isValid = isValid;
                return this;
            }

            public String getIssuedOn() {
                return issuedOn;
            }

            public void setIssuedOn(String issuedOn) {
                this.issuedOn = issuedOn;
            }

            public SubWalletDetailsList withIssuedOn(String issuedOn) {
                this.issuedOn = issuedOn;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("id", id).append("displayName", displayName)
                        .append("balance", balance).append("imageUrl", imageUrl).append("walletType", walletType)
                        .append("subWalletType", subWalletType).append("status", status).append("message", message)
                        .append("count", count).append("subWalletName", subWalletName)
                        .append("issuerMetadata", issuerMetadata).append("expiry", expiry)
                        .append("issuerId", issuerId).append("ppiDetailsId", ppiDetailsId)
                        .append("isValid", isValid).append("issuedOn", issuedOn).toString();
            }

        }

    }



}
