package com.paytm.apiPojo.wallet;

import com.paytm.framework.api.BaseApi;
import io.restassured.response.Response;

import java.math.BigDecimal;

public abstract class APIInterface extends BaseApi {

	public abstract responsePojo getResponsePojo();

	public requestPojo getRequestPojo(){
		return null;
	}

	public abstract Response getApiResponse();

	public abstract void createRequestJsonAndExecute();

	public static abstract class responsePojo {
		public abstract Object getStatus();

		public abstract String getStatusCode();

		public abstract String getStatusMessage();
	}

	public static abstract class requestPojo {

	}

	public static abstract class SubWalletInterface {

		public abstract BigDecimal getfOOD();

		public abstract BigDecimal getgIFT();

		public abstract BigDecimal gettOLL();

		public abstract BigDecimal getfUEL();

		public abstract BigDecimal getcLOSED_LOOP_WALLET();

		public abstract BigDecimal getiNTERNATIONAL_FUNDS_TRANSFER();

		public abstract BigDecimal getgIFT_VOUCHER();

		public abstract BigDecimal getcLOSED_LOOP_SUB_WALLET();

		public abstract void setfOOD(BigDecimal fOOD);

		public abstract void setcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET);

		public abstract void setgIFT(BigDecimal gIFT);

		public abstract void settOLL(BigDecimal tOLL);

		public abstract void setfUEL(BigDecimal fUEL);

		public abstract void setcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET);

		public abstract void setiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER);

		public abstract void setgIFT_VOUCHER(BigDecimal gIFT_VOUCHER);

		public abstract void setcASHBACK(BigDecimal cASHBACK);

	}
}
