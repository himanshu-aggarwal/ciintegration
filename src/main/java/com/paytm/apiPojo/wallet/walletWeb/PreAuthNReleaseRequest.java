package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface.SubWalletInterface;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.helpers.CommonHelpers;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

public class PreAuthNReleaseRequest extends requestPojo {

    private Request request;
    private String ipAddress;
    private String platformName;
    private String operationType;
    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager) {
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager() {
        return this.updateAmountManager;
    }


    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public PreAuthNReleaseRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public PreAuthNReleaseRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public PreAuthNReleaseRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public PreAuthNReleaseRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress).append("platformName", platformName).append("operationType", operationType).toString();
    }

    public class Request {

        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String merchantOrderId = "AutoPAP" + Thread.currentThread().getId() + CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private String merchantGuid;
        private BigDecimal amount;
        private String currencyCode;
        private String operationType;
        private SubWalletAmount subWalletAmount;
        private String preAuthId;

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Request withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public String getPreAuthId() {
            return preAuthId;
        }

        public void setPreAuthId(String preAuthId) {
            this.preAuthId = preAuthId;
        }

        public Request withPreAuthId(String preAuthId) {
            this.preAuthId = preAuthId;
            return this;
        }

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            return this;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
            if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
        }

        public Request withAmount(BigDecimal amount) {
            this.amount = amount;
            if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
            return this;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Request withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public String getOperationType() {
            return operationType;
        }

        public void setOperationType(String operationType) {
            this.operationType = operationType;
        }

        public Request withOperationType(String operationType) {
            this.operationType = operationType;
            return this;
        }

        public SubWalletAmount getSubWalletAmount() {
            return subWalletAmount;
        }

        public void setSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
        }

        public Request withSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("merchantOrderId", merchantOrderId).append("merchantGuid", merchantGuid).append("amount", amount).append("currencyCode", currencyCode).append("operationType", operationType).append("subWalletAmount", subWalletAmount).toString();
        }

        public class SubWalletAmount extends SubWalletInterface {
            @JsonProperty("FOOD")
            private BigDecimal fOOD;
            @JsonProperty("GIFT")
            private BigDecimal gIFT;
            @JsonProperty("TOLL")
            private BigDecimal tOLL;
            @JsonProperty("FUEL")
            private BigDecimal fUEL;
            @JsonProperty("CLOSED_LOOP_WALLET")
            private BigDecimal cLOSED_LOOP_WALLET;
            @JsonProperty("CLOSED_LOOP_SUB_WALLET")
            private BigDecimal cLOSED_LOOP_SUB_WALLET;
            @JsonProperty("INTERNATIONAL_FUNDS_TRANSFER")
            private BigDecimal iNTERNATIONAL_FUNDS_TRANSFER;
            @JsonProperty("GIFT_VOUCHER")
            private BigDecimal gIFT_VOUCHER;
            @JsonProperty("CASHBACK")
            private BigDecimal cASHBACK;

            public BigDecimal getfOOD() {
                return fOOD;
            }

            public void setfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFoodWalletAmount(fOOD);
                }
            }

            public SubWalletAmount withfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFoodWalletAmount(fOOD);
                }
                return this;
            }

            public BigDecimal getcLOSED_LOOP_SUB_WALLET() {
                return cLOSED_LOOP_SUB_WALLET;
            }

            public void setcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(cLOSED_LOOP_SUB_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(cLOSED_LOOP_SUB_WALLET);
                }
                return this;
            }

            public BigDecimal getgIFT() {
                return gIFT;
            }

            public void setgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftWalletAmount(gIFT);
                }
            }

            public SubWalletAmount withgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftWalletAmount(gIFT);
                }
                return this;
            }

            public BigDecimal gettOLL() {
                return tOLL;
            }

            public void settOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withTollWalletAmount(tOLL);
                }
            }

            public SubWalletAmount withtOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withTollWalletAmount(tOLL);
                }
                return this;
            }

            public BigDecimal getfUEL() {
                return fUEL;
            }

            public void setfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFuelWalletAmount(fUEL);
                }
            }

            public SubWalletAmount withfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFuelWalletAmount(fUEL);
                }
                return this;
            }

            public BigDecimal getcLOSED_LOOP_WALLET() {
                return cLOSED_LOOP_WALLET;
            }

            public void setcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(cLOSED_LOOP_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(cLOSED_LOOP_WALLET);
                }
                return this;
            }

            public BigDecimal getiNTERNATIONAL_FUNDS_TRANSFER() {
                return iNTERNATIONAL_FUNDS_TRANSFER;
            }

            public void setiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withInternationalWalletAmount(iNTERNATIONAL_FUNDS_TRANSFER);
                }
            }

            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withInternationalWalletAmount(iNTERNATIONAL_FUNDS_TRANSFER);
                }
                return this;
            }

            public BigDecimal getgIFT_VOUCHER() {
                return gIFT_VOUCHER;
            }

            public void setgIFT_VOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftVoucherAmount(gIFT_VOUCHER);
                }
            }

            public SubWalletAmount withgIFTVOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftVoucherAmount(gIFT_VOUCHER);
                }
                return this;
            }

            public BigDecimal getcASHBACK() {
                return cASHBACK;
            }

            public void setcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withCashBackWalletAmount(cASHBACK);
                }
            }

            public SubWalletAmount withcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(PreAuthNReleaseRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withCashBackWalletAmount(cASHBACK);
                }
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("fOOD", fOOD).append("gIFT", gIFT).append("tOLL", tOLL)
                        .append("fUEL", fUEL).append("cLOSED_LOOP_WALLET", cLOSED_LOOP_WALLET)
                        .append("iNTERNATIONAL_FUNDS_TRANSFER", iNTERNATIONAL_FUNDS_TRANSFER)
                        .append("gIFT_VOUCHER", gIFT_VOUCHER).toString();
            }


        }

    }
}
