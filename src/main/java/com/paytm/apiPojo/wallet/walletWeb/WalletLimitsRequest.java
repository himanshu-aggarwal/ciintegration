package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.datamanager.FundThreadManager;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;
import java.util.List;

public class WalletLimitsRequest extends APIInterface.requestPojo {

    @JsonProperty("request")
    private Request request;
    @JsonProperty("ipAddress")
    private String ipAddress;
    @JsonProperty("platformName")
    private String platformName;
    @JsonProperty("operationType")
    private String operationType;
    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager){
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager(){
        return this.updateAmountManager;
    }


    @JsonProperty("request")
    public Request getRequest() {
        return request;
    }

    @JsonProperty("request")
    public void setRequest(Request request) {
        this.request = request;
    }

    public WalletLimitsRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    @JsonProperty("ipAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ipAddress")
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public WalletLimitsRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    @JsonProperty("platformName")
    public String getPlatformName() {
        return platformName;
    }

    @JsonProperty("platformName")
    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public WalletLimitsRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    @JsonProperty("operationType")
    public String getOperationType() {
        return operationType;
    }

    @JsonProperty("operationType")
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public WalletLimitsRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress).append("platformName", platformName).append("operationType", operationType).toString();
    }

    public class Request {
/*
        @JsonProperty("walletOperation")
        private String walletOperation;
*/

        @JsonProperty("targetCustId")
        private String targetCustId;

        @JsonProperty("targetPhoneNo")
        private String targetPhoneNo;

/*
        @JsonProperty("addMoneyDestination")
        private String addMoneyDestination;
*/

        @JsonProperty("ssoId")
        private String ssoId;
        @JsonProperty("walletOperationTypeList")
        private List<String> walletOperationTypeList = null;
        @JsonProperty("walletOperationTypeBasedAmountList")
        private List<BigDecimal> walletOperationTypeBasedAmountList = null;
        @JsonProperty("walletOperationType")
        private String walletOperationType;
        @JsonProperty("amount")
        private BigDecimal amount;

        @JsonProperty("ssoId")
        public String getSsoId() {
            return ssoId;
        }

        @JsonProperty("ssoId")
        public void setSsoId(String ssoId) {
            this.ssoId = ssoId;
        }

        @JsonProperty("targetCustId")
        public String getTargetCustId() {
            return targetCustId;
        }

        @JsonProperty("targetCustId")
        public void setTargetCustId(String targetCustId) {
            this.targetCustId = targetCustId;
        }

        public Request withTargetCustId(String targetCustId) {
            this.targetCustId = targetCustId;
            return this;
        }

        @JsonProperty("targetPhoneNo")
        public String getTargetPhoneNo() {
            return targetPhoneNo;
        }

        @JsonProperty("targetPhoneNo")
        public void setTargetPhoneNo(String targetPhoneNo) {
            this.targetPhoneNo = targetPhoneNo;
        }

        public Request withTargetPhoneNo(String targetPhoneNo) {
            this.targetPhoneNo = targetPhoneNo;
            return this;
        }

        public Request withSsoId(String ssoId) {
            this.ssoId = ssoId;

            return this;
        }

        @JsonProperty("walletOperationTypeList")
        public List<String> getWalletOperationTypeList() {
            return walletOperationTypeList;
        }

        @JsonProperty("walletOperationTypeList")
        public void setWalletOperationTypeList(List<String> walletOperationTypeList) {
            this.walletOperationTypeList = walletOperationTypeList;
        }

        public Request withWalletOperationTypeList(List<String> walletOperationTypeList) {
            this.walletOperationTypeList = walletOperationTypeList;
            return this;
        }

        @JsonProperty("walletOperationTypeBasedAmountList")
        public List<BigDecimal> getWalletOperationTypeBasedAmountList() {
            return walletOperationTypeBasedAmountList;
        }

        @JsonProperty("walletOperationTypeBasedAmountList")
        public void setWalletOperationTypeBasedAmountList(List<BigDecimal> walletOperationTypeBasedAmountList) {
            this.walletOperationTypeBasedAmountList = walletOperationTypeBasedAmountList;
        }

        public Request withWalletOperationTypeBasedAmountList(List<BigDecimal> walletOperationTypeBasedAmountList) {
            this.walletOperationTypeBasedAmountList = walletOperationTypeBasedAmountList;
            return this;
        }

        @JsonProperty("walletOperationType")
        public String getWalletOperationType() {
            return walletOperationType;
        }

        @JsonProperty("walletOperationType")
        public void setWalletOperationType(String walletOperationType) {
            this.walletOperationType = walletOperationType;
        }

        public Request withWalletOperationType(String walletOperationType) {
            this.walletOperationType = walletOperationType;
            return this;
        }

        @JsonProperty("amount")
        public BigDecimal getAmount() {
            return amount;
        }

        @JsonProperty("amount")
        public void setAmount(BigDecimal amount) {
            this.amount = amount;
            if(WalletLimitsRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
        }

        public Request withAmount(BigDecimal amount) {
            this.amount = amount;
            if(WalletLimitsRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("ssoId", ssoId).append("walletOperationTypeList", walletOperationTypeList).append("walletOperationTypeBasedAmountList", walletOperationTypeBasedAmountList).append("walletOperationType", walletOperationType).append("amount", amount).toString();
        }

    }
}
