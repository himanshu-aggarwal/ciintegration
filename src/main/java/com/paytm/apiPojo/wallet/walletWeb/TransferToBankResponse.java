package com.paytm.apiPojo.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class TransferToBankResponse extends responsePojo {

	private Object type;
	private Object requestGuid;
	private Object orderId;
	private String status;
	private String statusCode;
	private String statusMessage;
	private Response response;
	private Object metadata;

	public Object getType() {
		return type;
	}

	public void setType(Object type) {
		this.type = type;
	}

	public Object getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}

	public Object getOrderId() {
		return orderId;
	}

	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public Object getMetadata() {
		return metadata;
	}

	public void setMetadata(Object metadata) {
		this.metadata = metadata;
	}

	public class Response {

		private Object state;
		private String heading;
		private String walletSysTransactionId;
		private String bankTransactionId;
		private Boolean firstPPBTransaction;
		private Boolean isBeneficiaryAdded;
		private Object coolingOffTime;
		private Double commission;
		private Double txnAmount;

		public Object getState() {
			return state;
		}

		public void setState(Object state) {
			this.state = state;
		}

		public String getHeading() {
			return heading;
		}

		public void setHeading(String heading) {
			this.heading = heading;
		}

		public String getWalletSysTransactionId() {
			return walletSysTransactionId;
		}

		public void setWalletSysTransactionId(String walletSysTransactionId) {
			this.walletSysTransactionId = walletSysTransactionId;
		}

		public String getBankTransactionId() {
			return bankTransactionId;
		}

		public void setBankTransactionId(String bankTransactionId) {
			this.bankTransactionId = bankTransactionId;
		}

		public Boolean getFirstPPBTransaction() {
			return firstPPBTransaction;
		}

		public void setFirstPPBTransaction(Boolean firstPPBTransaction) {
			this.firstPPBTransaction = firstPPBTransaction;
		}

		public Boolean getIsBeneficiaryAdded() {
			return isBeneficiaryAdded;
		}

		public void setIsBeneficiaryAdded(Boolean isBeneficiaryAdded) {
			this.isBeneficiaryAdded = isBeneficiaryAdded;
		}

		public Object getCoolingOffTime() {
			return coolingOffTime;
		}

		public void setCoolingOffTime(Object coolingOffTime) {
			this.coolingOffTime = coolingOffTime;
		}

		public Double getCommission() {
			return commission;
		}

		public void setCommission(Double commission) {
			this.commission = commission;
		}

		public Double getTxnAmount() {
			return txnAmount;
		}

		public void setTxnAmount(Double txnAmount) {
			this.txnAmount = txnAmount;
		}

	}

}
