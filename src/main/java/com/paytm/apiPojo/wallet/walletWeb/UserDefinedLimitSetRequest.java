package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.exceptions.JSONExceptions.JSONParseMappingException;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public class UserDefinedLimitSetRequest extends APIInterface.requestPojo {

    private static String periodLimitsJson = "{\"dayPeriodLimit\": {\"amount\":1000000,\"count\":2000},\"monthPeriodLimit\": {\"amount\":1000000,\"count\":2000}}";

    @JsonProperty("request")
    private Request request;

    @JsonProperty("request")
    public Request getRequest() {
        return request;
    }

    @JsonProperty("request")
    public void setRequest(Request request) {
        this.request = request;
    }

    public UserDefinedLimitSetRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).toString();
    }

    public static class Request {

        @JsonProperty("configList")
        private Map<String, ConfigList> configList;

        @JsonProperty("configList")
        public Map<String, ConfigList> getConfigList() {
            return configList;
        }

        @JsonProperty("configList")
        public void setConfigList(Map<String, ConfigList> configList) {
            this.configList = configList;
        }

        public Request withConfigList(Map<String, ConfigList> configList) {
            this.configList = configList;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("configList", configList).toString();
        }

        public static class ConfigList {

            @JsonProperty("status")
            private int status;
            @JsonProperty("periodLimits")
            private PeriodLimits periodLimits;

            @JsonProperty("status")
            public int getStatus() {
                return status;
            }

            @JsonProperty("status")
            public void setStatus(int status) {
                this.status = status;
            }

            public ConfigList withStatus(int status) {
                this.status = status;
                return this;
            }

            @JsonProperty("periodLimits")
            public PeriodLimits getPeriodLimits() {
                return periodLimits;
            }

            @JsonProperty("periodLimits")
            public void setPeriodLimits(PeriodLimits periodLimits) {
                this.periodLimits = periodLimits;
            }

            public ConfigList withPeriodLimits(PeriodLimits periodLimits) {
                this.periodLimits = periodLimits;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("status", status).append("periodLimits", periodLimits).toString();
            }

            public void createPeriodLimits(){
                try {
                    PeriodLimits periodLimits = JacksonJsonImpl.getInstance().fromJson(periodLimitsJson, PeriodLimits.class);
                    withPeriodLimits(periodLimits);
//                    return periodLimits;
                }catch(IOException e){
                    throw new JSONParseMappingException(e);
                }
            }

            public static class PeriodLimits {

                @JsonProperty("dayPeriodLimit")
                private SpecificPeriodLimit dayPeriodLimit;
                @JsonProperty("monthPeriodLimit")
                private SpecificPeriodLimit monthPeriodLimit;


                @JsonProperty("dayPeriodLimit")
                public SpecificPeriodLimit getDayPeriodLimit() {
                    return dayPeriodLimit;
                }

                @JsonProperty("dayPeriodLimit")
                public void setDayPeriodLimit(SpecificPeriodLimit dayPeriodLimit) {
                    this.dayPeriodLimit = dayPeriodLimit;
                }

                public PeriodLimits withDayPeriodLimit(SpecificPeriodLimit dayPeriodLimit) {
                    this.dayPeriodLimit = dayPeriodLimit;
                    return this;
                }

                @JsonProperty("monthPeriodLimit")
                public SpecificPeriodLimit getMonthPeriodLimit() {
                    return monthPeriodLimit;
                }

                @JsonProperty("monthPeriodLimit")
                public void setMonthPeriodLimit(SpecificPeriodLimit monthPeriodLimit) {
                    this.monthPeriodLimit = monthPeriodLimit;
                }

                public PeriodLimits withMonthPeriodLimit(SpecificPeriodLimit monthPeriodLimit) {
                    this.monthPeriodLimit = monthPeriodLimit;
                    return this;
                }


                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("dayPeriodLimit", dayPeriodLimit).append("monthPeriodLimit", monthPeriodLimit).toString();
                }


                public static class SpecificPeriodLimit {

                    @JsonProperty("amount")
                    private BigDecimal amount;
                    @JsonProperty("count")
                    private int count;

                    @JsonProperty("amount")
                    public BigDecimal getAmount() {
                        return amount;
                    }

                    @JsonProperty("amount")
                    public void setAmount(BigDecimal amount) {
                        this.amount = amount;
                    }

                    public SpecificPeriodLimit withAmount(BigDecimal amount) {
                        this.amount = amount;
                        return this;
                    }

                    @JsonProperty("count")
                    public int getCount() {
                        return count;
                    }

                    @JsonProperty("count")
                    public void setCount(int count) {
                        this.count = count;
                    }

                    public SpecificPeriodLimit withCount(int count) {
                        this.count = count;
                        return this;
                    }

                    @Override
                    public String toString() {
                        return new ToStringBuilder(this).append("amount", amount).append("count", count).toString();
                    }

                }


            }


        }

    }


}
