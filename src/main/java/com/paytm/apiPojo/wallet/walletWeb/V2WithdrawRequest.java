package com.paytm.apiPojo.wallet.walletWeb;

import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface.SubWalletInterface;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;


public class V2WithdrawRequest extends requestPojo {

    private Request request;
    private String platformName;
    private String ipAddress;
    private String operationType;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public V2WithdrawRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public V2WithdrawRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public V2WithdrawRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public V2WithdrawRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("platformName", platformName).append("ipAddress", ipAddress).append("operationType", operationType).toString();
    }

    public class InvoiceDetails {

        private List<String> headerName = null;
        private List<List<String>> itemDetails = null;
        private List<String> taxDetails = null;
        private List<String> otherDetails = null;

        public List<String> getHeaderName() {
            return headerName;
        }

        public void setHeaderName(List<String> headerName) {
            this.headerName = headerName;
        }

        public InvoiceDetails withHeaderName(List<String> headerName) {
            this.headerName = headerName;
            return this;
        }

        public List<List<String>> getItemDetails() {
            return itemDetails;
        }

        public void setItemDetails(List<List<String>> itemDetails) {
            this.itemDetails = itemDetails;
        }

        public InvoiceDetails withItemDetails(List<List<String>> itemDetails) {
            this.itemDetails = itemDetails;
            return this;
        }

        public List<String> getTaxDetails() {
            return taxDetails;
        }

        public void setTaxDetails(List<String> taxDetails) {
            this.taxDetails = taxDetails;
        }

        public InvoiceDetails withTaxDetails(List<String> taxDetails) {
            this.taxDetails = taxDetails;
            return this;
        }

        public List<String> getOtherDetails() {
            return otherDetails;
        }

        public void setOtherDetails(List<String> otherDetails) {
            this.otherDetails = otherDetails;
        }

        public InvoiceDetails withOtherDetails(List<String> otherDetails) {
            this.otherDetails = otherDetails;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("headerName", headerName).append("itemDetails", itemDetails).append("taxDetails", taxDetails).append("otherDetails", otherDetails).toString();
        }
    }

    public class OtherDetail {

        private String rowname;
        private String rowvalue;

        public String getRowname() {
            return rowname;
        }

        public void setRowname(String rowname) {
            this.rowname = rowname;
        }

        public OtherDetail withRowname(String rowname) {
            this.rowname = rowname;
            return this;
        }

        public String getRowvalue() {
            return rowvalue;
        }

        public void setRowvalue(String rowvalue) {
            this.rowvalue = rowvalue;
        }

        public OtherDetail withRowvalue(String rowvalue) {
            this.rowvalue = rowvalue;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("rowname", rowname).append("rowvalue", rowvalue).toString();
        }

    }


    public static class Request {
        private Boolean skipRefill;
        private BigDecimal totalAmount;
        private String currencyCode;
        private String merchantGuid;
        private String merchantOrderId;
        private String industryType;
        private String posId;
        private String comment;
        private String uniqueReferenceLabel;
        private SubWalletAmount subWalletAmount;
        private String uniqueReferenceValue;
        private InvoiceDetails invoiceDetails;

        public Boolean getSkipRefill() {
            return skipRefill;
        }

        public void setSkipRefill(Boolean skipRefill) {
            this.skipRefill = skipRefill;
        }

        public Request withSkipRefill(Boolean skipRefill) {
            this.skipRefill = skipRefill;
            return this;
        }

        public BigDecimal getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Request withTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
            return this;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Request withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Request withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public String getIndustryType() {
            return industryType;
        }

        public void setIndustryType(String industryType) {
            this.industryType = industryType;
        }

        public Request withIndustryType(String industryType) {
            this.industryType = industryType;
            return this;
        }

        public String getPosId() {
            return posId;
        }

        public void setPosId(String posId) {
            this.posId = posId;
        }

        public Request withPosId(String posId) {
            this.posId = posId;
            return this;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Request withComment(String comment) {
            this.comment = comment;
            return this;
        }

        public String getUniqueReferenceLabel() {
            return uniqueReferenceLabel;
        }

        public void setUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
        }

        public Request withUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
            return this;
        }

        public SubWalletAmount getSubWalletAmount() {
            return subWalletAmount;
        }

        public void setSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
        }

        public Request withSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
            return this;
        }

        public String getUniqueReferenceValue() {
            return uniqueReferenceValue;
        }

        public void setUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
        }

        public Request withUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
            return this;
        }

        public InvoiceDetails getInvoiceDetails() {
            return invoiceDetails;
        }

        public void setInvoiceDetails(InvoiceDetails invoiceDetails) {
            this.invoiceDetails = invoiceDetails;
        }

        public Request withInvoiceDetails(InvoiceDetails invoiceDetails) {
            this.invoiceDetails = invoiceDetails;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("skipRefill", skipRefill).append("totalAmount", totalAmount).append("currencyCode", currencyCode).append("merchantGuid", merchantGuid).append("merchantOrderId", merchantOrderId).append("industryType", industryType).append("posId", posId).append("comment", comment).append("uniqueReferenceLabel", uniqueReferenceLabel).append("subWalletAmount", subWalletAmount).append("uniqueReferenceValue", uniqueReferenceValue).append("invoiceDetails", invoiceDetails).toString();
        }

        public static class SubWalletAmount extends SubWalletInterface {

            private BigDecimal fOOD;
            private BigDecimal gIFT;
            private BigDecimal tOLL;
            private BigDecimal fUEL;
            private BigDecimal cLOSED_LOOP_WALLET;
            private BigDecimal cLOSED_LOOP_SUB_WALLET;
            private BigDecimal iNTERNATIONAL_FUNDS_TRANSFER;
            private BigDecimal gIFT_VOUCHER;
            private BigDecimal cASHBACK;

            @JsonProperty("FOOD")
            public BigDecimal getfOOD() {
                return fOOD;
            }

            @JsonProperty("FOOD")
            public void setfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
            }

            @JsonProperty("FOOD")
            public SubWalletAmount withfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                return this;
            }

            @JsonProperty("CLOSED_LOOP_SUB_WALLET")
            public BigDecimal getcLOSED_LOOP_SUB_WALLET() {
                return cLOSED_LOOP_SUB_WALLET;
            }

            @JsonProperty("CLOSED_LOOP_SUB_WALLET")
            public void setcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
            }

            @JsonProperty("CLOSED_LOOP_SUB_WALLET")
            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                return this;
            }

            @JsonProperty("GIFT")
            public BigDecimal getgIFT() {
                return gIFT;
            }

            @JsonProperty("GIFT")
            public void setgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
            }

            @JsonProperty("GIFT")
            public SubWalletAmount withgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                return this;
            }

            @JsonProperty("TOLL")
            public BigDecimal gettOLL() {
                return tOLL;
            }

            @JsonProperty("TOLL")
            public void settOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
            }

            @JsonProperty("TOLL")
            public SubWalletAmount withtOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                return this;
            }

            @JsonProperty("FUEL")
            public BigDecimal getfUEL() {
                return fUEL;
            }

            @JsonProperty("FUEL")
            public void setfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
            }

            @JsonProperty("FUEL")
            public SubWalletAmount withfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                return this;
            }

            @JsonProperty("CLOSED_LOOP_WALLET")
            public BigDecimal getcLOSED_LOOP_WALLET() {
                return cLOSED_LOOP_WALLET;
            }

            @JsonProperty("CLOSED_LOOP_WALLET")
            public void setcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
            }

            @JsonProperty("CLOSED_LOOP_WALLET")
            public SubWalletAmount withcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                return this;
            }

            @JsonProperty("INTERNATIONAL_FUNDS_TRANSFER")
            public BigDecimal getiNTERNATIONAL_FUNDS_TRANSFER() {
                return iNTERNATIONAL_FUNDS_TRANSFER;
            }

            @JsonProperty("INTERNATIONAL_FUNDS_TRANSFER")
            public void setiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
            }

            @JsonProperty("INTERNATIONAL_FUNDS_TRANSFER")
            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                return this;
            }

            @JsonProperty("GIFT_VOUCHER")
            public BigDecimal getgIFT_VOUCHER() {
                return gIFT_VOUCHER;
            }

            @JsonProperty("GIFT_VOUCHER")
            public void setgIFT_VOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
            }

            @JsonProperty("GIFT_VOUCHER")
            public SubWalletAmount withgIFTVOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                return this;
            }

            @JsonProperty("CASHBACK")
            public BigDecimal getcASHBACK() {
                return cASHBACK;
            }

            @JsonProperty("CASHBACK")
            public void setcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
            }

            @JsonProperty("CASHBACK")
            public SubWalletAmount withcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("fOOD", fOOD).append("gIFT", gIFT).append("tOLL", tOLL)
                        .append("fUEL", fUEL).append("cLOSED_LOOP_WALLET", cLOSED_LOOP_WALLET)
                        .append("iNTERNATIONAL_FUNDS_TRANSFER", iNTERNATIONAL_FUNDS_TRANSFER)
                        .append("gIFT_VOUCHER", gIFT_VOUCHER).toString();
            }

        }

        public class TaxDetail {

            private String rowname;
            private String rowvalue;

            public String getRowname() {
                return rowname;
            }

            public void setRowname(String rowname) {
                this.rowname = rowname;
            }

            public TaxDetail withRowname(String rowname) {
                this.rowname = rowname;
                return this;
            }

            public String getRowvalue() {
                return rowvalue;
            }

            public void setRowvalue(String rowvalue) {
                this.rowvalue = rowvalue;
            }

            public TaxDetail withRowvalue(String rowvalue) {
                this.rowvalue = rowvalue;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("rowname", rowname).append("rowvalue", rowvalue).toString();
            }

        }
    }
}

