package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;


public class AddMoneyResponse extends responsePojo {

    private Object type;
    private String requestGuid;
    private String orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public AddMoneyResponse withType(Object type) {
        this.type = type;
        return this;
    }

    public String getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
    }

    public AddMoneyResponse withRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public AddMoneyResponse withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AddMoneyResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public AddMoneyResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public AddMoneyResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public AddMoneyResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public AddMoneyResponse withMetadata(Object metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid).append("orderId", orderId).append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage).append("response", response).append("metadata", metadata).toString();
    }


    public static class Response {

        private String txnGuid;
        private String txnStatus;
        private String ssoId;
        private Object monthlyAvailableAmount;
        private Object subWalletDetails;

        public String getTxnGuid() {
            return txnGuid;
        }

        public void setTxnGuid(String txnGuid) {
            this.txnGuid = txnGuid;
        }

        public Response withTxnGuid(String txnGuid) {
            this.txnGuid = txnGuid;
            return this;
        }

        public String getTxnStatus() {
            return txnStatus;
        }

        public void setTxnStatus(String txnStatus) {
            this.txnStatus = txnStatus;
        }

        public Response withTxnStatus(String txnStatus) {
            this.txnStatus = txnStatus;
            return this;
        }

        public String getSsoId() {
            return ssoId;
        }

        public void setSsoId(String ssoId) {
            this.ssoId = ssoId;
        }

        public Response withSsoId(String ssoId) {
            this.ssoId = ssoId;
            return this;
        }

        public Object getMonthlyAvailableAmount() {
            return monthlyAvailableAmount;
        }

        public void setMonthlyAvailableAmount(Object monthlyAvailableAmount) {
            this.monthlyAvailableAmount = monthlyAvailableAmount;
        }

        public Response withMonthlyAvailableAmount(Object monthlyAvailableAmount) {
            this.monthlyAvailableAmount = monthlyAvailableAmount;
            return this;
        }

        public Object getSubWalletDetails() {
            return subWalletDetails;
        }

        public void setSubWalletDetails(Object subWalletDetails) {
            this.subWalletDetails = subWalletDetails;
        }

        public Response withSubWalletDetails(Object subWalletDetails) {
            this.subWalletDetails = subWalletDetails;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("txnGuid", txnGuid).append("txnStatus", txnStatus).append("ssoId", ssoId).append("monthlyAvailableAmount", monthlyAvailableAmount).append("subWalletDetails", subWalletDetails).toString();
        }

    }

}

