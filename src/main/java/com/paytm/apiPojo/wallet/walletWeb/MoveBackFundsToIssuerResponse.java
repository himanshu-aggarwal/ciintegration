package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class MoveBackFundsToIssuerResponse extends responsePojo {

    private Object requestGuid;
    private Object orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;

    public Object getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public MoveBackFundsToIssuerResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public MoveBackFundsToIssuerResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MoveBackFundsToIssuerResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public MoveBackFundsToIssuerResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public MoveBackFundsToIssuerResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public MoveBackFundsToIssuerResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId).append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage).append("response", response).toString();
    }

    public class Response {

        private String walletSystemTxnId;

        public String getWalletSystemTxnId() {
            return walletSystemTxnId;
        }

        public void setWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
        }

        public Response withWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("walletSystemTxnId", walletSystemTxnId).toString();
        }

    }


}

