package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class ClaimLifafaRequest extends requestPojo {

    private String lifafaKey;

    public String getLifafaKey() {
        return lifafaKey;
    }

    public void setLifafaKey(String lifafaKey) {
        this.lifafaKey = lifafaKey;
    }

    public ClaimLifafaRequest withLifafaKey(String lifafaKey) {
        this.lifafaKey = lifafaKey;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("lifafaKey", lifafaKey).toString();
    }
}

