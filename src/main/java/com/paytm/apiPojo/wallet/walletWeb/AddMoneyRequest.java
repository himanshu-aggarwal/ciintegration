package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.helpers.CommonHelpers;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

public class AddMoneyRequest extends requestPojo {

    private Request request;
    private String metadata;
    private String ipAddress;
    private String platformName;
    private String operationType;
    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager){
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager(){
        return this.updateAmountManager;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public AddMoneyRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public AddMoneyRequest withMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public AddMoneyRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public AddMoneyRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public AddMoneyRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("metadata", metadata).append("ipAddress", ipAddress).append("platformName", platformName).append("operationType", operationType).toString();
    }


    public class Request {

        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String pgTxnId = "AutoAMRP"+ Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private BigDecimal txnAmount;
        private String txnCurrency;
        private String txnStatus;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String merchantOrderId = "AutoAMRM"+ Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private String merchantGuid;
        private String pgResponseCode;
        private String pgResponseMessage;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String bankTxnId = "AutoAMRB"+ Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private String bankName;
        private String gatewayName;
        private String paymentMode;
        private Object binNumber;
        private Object cardType;
        private String mid;
        private String requestType;

        public String getPgTxnId() {
            return pgTxnId;
        }

        public void setPgTxnId(String pgTxnId) {
            this.pgTxnId = pgTxnId;
        }

        public Request withPgTxnId(String pgTxnId) {
            this.pgTxnId = pgTxnId;
            return this;
        }

        public BigDecimal getTxnAmount() {
            return txnAmount;
        }

        public void setTxnAmount(BigDecimal txnAmount) {
            this.txnAmount = txnAmount;
            if(AddMoneyRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(txnAmount);
            }
        }

        public Request withTxnAmount(BigDecimal txnAmount) {
            this.txnAmount = txnAmount;
            if(AddMoneyRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(txnAmount);
            }
            return this;
        }

        public String getTxnCurrency() {
            return txnCurrency;
        }

        public void setTxnCurrency(String txnCurrency) {
            this.txnCurrency = txnCurrency;
        }

        public Request withTxnCurrency(String txnCurrency) {
            this.txnCurrency = txnCurrency;
            return this;
        }

        public String getTxnStatus() {
            return txnStatus;
        }

        public void setTxnStatus(String txnStatus) {
            this.txnStatus = txnStatus;
        }

        public Request withTxnStatus(String txnStatus) {
            this.txnStatus = txnStatus;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Request withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            return this;
        }

        public String getPgResponseCode() {
            return pgResponseCode;
        }

        public void setPgResponseCode(String pgResponseCode) {
            this.pgResponseCode = pgResponseCode;
        }

        public Request withPgResponseCode(String pgResponseCode) {
            this.pgResponseCode = pgResponseCode;
            return this;
        }

        public String getPgResponseMessage() {
            return pgResponseMessage;
        }

        public void setPgResponseMessage(String pgResponseMessage) {
            this.pgResponseMessage = pgResponseMessage;
        }

        public Request withPgResponseMessage(String pgResponseMessage) {
            this.pgResponseMessage = pgResponseMessage;
            return this;
        }

        public String getBankTxnId() {
            return bankTxnId;
        }

        public void setBankTxnId(String bankTxnId) {
            this.bankTxnId = bankTxnId;
        }

        public Request withBankTxnId(String bankTxnId) {
            this.bankTxnId = bankTxnId;
            return this;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public Request withBankName(String bankName) {
            this.bankName = bankName;
            return this;
        }

        public String getGatewayName() {
            return gatewayName;
        }

        public void setGatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
        }

        public Request withGatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
            return this;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Request withPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
            return this;
        }

        public Object getBinNumber() {
            return binNumber;
        }

        public void setBinNumber(Object binNumber) {
            this.binNumber = binNumber;
        }

        public Request withBinNumber(Object binNumber) {
            this.binNumber = binNumber;
            return this;
        }

        public Object getCardType() {
            return cardType;
        }

        public void setCardType(Object cardType) {
            this.cardType = cardType;
        }

        public Request withCardType(Object cardType) {
            this.cardType = cardType;
            return this;
        }

        public String getMid() {
            return mid;
        }

        public void setMid(String mid) {
            this.mid = mid;
        }

        public Request withMid(String mid) {
            this.mid = mid;
            return this;
        }

        public String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        public Request withRequestType(String requestType) {
            this.requestType = requestType;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("pgTxnId", pgTxnId).append("txnAmount", txnAmount).append("txnCurrency", txnCurrency).append("txnStatus", txnStatus).append("merchantOrderId", merchantOrderId).append("merchantGuid", merchantGuid).append("pgResponseCode", pgResponseCode).append("pgResponseMessage", pgResponseMessage).append("bankTxnId", bankTxnId).append("bankName", bankName).append("gatewayName", gatewayName).append("paymentMode", paymentMode).append("binNumber", binNumber).append("cardType", cardType).append("mid", mid).append("requestType", requestType).toString();
        }

    }

}

