package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class V2ValidateRequest extends requestPojo {

    private Request request;
    private String ipAddress;
    private String platformName;
    private String operationType;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public V2ValidateRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public V2ValidateRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public V2ValidateRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public V2ValidateRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("platformName", platformName).append("operationType", operationType).toString();
    }

    public static class Request {
        private String state;
        private String otp;

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Request withState(String state) {
            this.state = state;
            return this;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public Request withOtp(String otp) {
            this.otp = otp;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("state", state).append("otp", otp).toString();
        }
    }

}

