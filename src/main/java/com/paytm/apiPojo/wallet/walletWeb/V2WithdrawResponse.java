package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class V2WithdrawResponse extends responsePojo {

    private Object type;
    private Object requestGuid;
    private Object orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public V2WithdrawResponse withType(Object type) {
        this.type = type;
        return this;
    }

    public Object getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public V2WithdrawResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public V2WithdrawResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public V2WithdrawResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public V2WithdrawResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public V2WithdrawResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public V2WithdrawResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public V2WithdrawResponse withMetadata(Object metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid).append("orderId", orderId).append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage).append("response", response).append("metadata", metadata).toString();
    }


    public static class Response {

        private Object userGuid;
        private Object pgTxnId;
        private Object timestamp;
        private Object cashBackStatus;
        private Object cashBackMessage;
        private String state;
        private String heading;
        private Object walletSysTransactionId;
        private Object walletSystemTxnId;
        private Object comment;
        private Object posId;
        private Integer txnAmount;
        private String merchantOrderId;
        private Object uniqueReferenceLabel;
        private Object uniqueReferenceValue;
        private Object pccCode;

        public Object getUserGuid() {
            return userGuid;
        }

        public void setUserGuid(Object userGuid) {
            this.userGuid = userGuid;
        }

        public Response withUserGuid(Object userGuid) {
            this.userGuid = userGuid;
            return this;
        }

        public Object getPgTxnId() {
            return pgTxnId;
        }

        public void setPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
        }

        public Response withPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
            return this;
        }

        public Object getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Object timestamp) {
            this.timestamp = timestamp;
        }

        public Response withTimestamp(Object timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Object getCashBackStatus() {
            return cashBackStatus;
        }

        public void setCashBackStatus(Object cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
        }

        public Response withCashBackStatus(Object cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
            return this;
        }

        public Object getCashBackMessage() {
            return cashBackMessage;
        }

        public void setCashBackMessage(Object cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
        }

        public Response withCashBackMessage(Object cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
            return this;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Response withState(String state) {
            this.state = state;
            return this;
        }

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public Response withHeading(String heading) {
            this.heading = heading;
            return this;
        }

        public Object getWalletSysTransactionId() {
            return walletSysTransactionId;
        }

        public void setWalletSysTransactionId(Object walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
        }

        public Response withWalletSysTransactionId(Object walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
            return this;
        }

        public Object getWalletSystemTxnId() {
            return walletSystemTxnId;
        }

        public void setWalletSystemTxnId(Object walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
        }

        public Response withWalletSystemTxnId(Object walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
            return this;
        }

        public Object getComment() {
            return comment;
        }

        public void setComment(Object comment) {
            this.comment = comment;
        }

        public Response withComment(Object comment) {
            this.comment = comment;
            return this;
        }

        public Object getPosId() {
            return posId;
        }

        public void setPosId(Object posId) {
            this.posId = posId;
        }

        public Response withPosId(Object posId) {
            this.posId = posId;
            return this;
        }

        public Integer getTxnAmount() {
            return txnAmount;
        }

        public void setTxnAmount(Integer txnAmount) {
            this.txnAmount = txnAmount;
        }

        public Response withTxnAmount(Integer txnAmount) {
            this.txnAmount = txnAmount;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Response withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public Object getUniqueReferenceLabel() {
            return uniqueReferenceLabel;
        }

        public void setUniqueReferenceLabel(Object uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
        }

        public Response withUniqueReferenceLabel(Object uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
            return this;
        }

        public Object getUniqueReferenceValue() {
            return uniqueReferenceValue;
        }

        public void setUniqueReferenceValue(Object uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
        }

        public Response withUniqueReferenceValue(Object uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
            return this;
        }

        public Object getPccCode() {
            return pccCode;
        }

        public void setPccCode(Object pccCode) {
            this.pccCode = pccCode;
        }

        public Response withPccCode(Object pccCode) {
            this.pccCode = pccCode;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("userGuid", userGuid).append("pgTxnId", pgTxnId).append("timestamp", timestamp).append("cashBackStatus", cashBackStatus).append("cashBackMessage", cashBackMessage).append("state", state).append("heading", heading).append("walletSysTransactionId", walletSysTransactionId).append("walletSystemTxnId", walletSystemTxnId).append("comment", comment).append("posId", posId).append("txnAmount", txnAmount).append("merchantOrderId", merchantOrderId).append("uniqueReferenceLabel", uniqueReferenceLabel).append("uniqueReferenceValue", uniqueReferenceValue).append("pccCode", pccCode).toString();
        }
    }

}

