package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class SendMoneyResponse extends responsePojo {

    private Object type;
    private Object requestGuid;
    private Object orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public SendMoneyResponse withType(Object type) {
        this.type = type;
        return this;
    }

    public Object getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public SendMoneyResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public SendMoneyResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SendMoneyResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public SendMoneyResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public SendMoneyResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public SendMoneyResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public SendMoneyResponse withMetadata(Object metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid).append("orderId", orderId).append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage).append("response", response).append("metadata", metadata).toString();
    }

    public static class Response {

        private String heading;
        private Object state;
        private String walletSysTransactionId;
        private int payeeSsoId;
        private Long timestamp;
        private String displayName;
        private boolean isBeneficiaryAdded;
        private Object coolingOffTime;

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public Response withHeading(String heading) {
            this.heading = heading;
            return this;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Response withState(Object state) {
            this.state = state;
            return this;
        }

        public String getWalletSysTransactionId() {
            return walletSysTransactionId;
        }

        public void setWalletSysTransactionId(String walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
        }

        public Response withWalletSysTransactionId(String walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
            return this;
        }

        public int getPayeeSsoId() {
            return payeeSsoId;
        }

        public void setPayeeSsoId(int payeeSsoId) {
            this.payeeSsoId = payeeSsoId;
        }

        public Response withPayeeSsoId(int payeeSsoId) {
            this.payeeSsoId = payeeSsoId;
            return this;
        }

        public Long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Long timestamp) {
            this.timestamp = timestamp;
        }

        public Response withTimestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public Response withDisplayName(String displayName) {
            this.displayName = displayName;
            return this;
        }

        public boolean isIsBeneficiaryAdded() {
            return isBeneficiaryAdded;
        }

        public void setIsBeneficiaryAdded(boolean isBeneficiaryAdded) {
            this.isBeneficiaryAdded = isBeneficiaryAdded;
        }

        public Response withIsBeneficiaryAdded(boolean isBeneficiaryAdded) {
            this.isBeneficiaryAdded = isBeneficiaryAdded;
            return this;
        }

        public Object getCoolingOffTime() {
            return coolingOffTime;
        }

        public void setCoolingOffTime(Object coolingOffTime) {
            this.coolingOffTime = coolingOffTime;
        }

        public Response withCoolingOffTime(Object coolingOffTime) {
            this.coolingOffTime = coolingOffTime;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("heading", heading).append("state", state).append("walletSysTransactionId", walletSysTransactionId).append("payeeSsoId", payeeSsoId).append("timestamp", timestamp).append("displayName", displayName).append("isBeneficiaryAdded", isBeneficiaryAdded).append("coolingOffTime", coolingOffTime).toString();
        }

    }

}

