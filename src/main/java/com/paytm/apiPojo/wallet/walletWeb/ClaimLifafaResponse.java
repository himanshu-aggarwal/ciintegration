package com.paytm.apiPojo.wallet.walletWeb;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class ClaimLifafaResponse extends responsePojo {

    private String statusCode;
    private String statusMessage;
    private String categoryResponse;
    private ClaimLifafaResponse.ThemeInfo themeInfo;
    private Integer claimedQuantity;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ClaimLifafaResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ClaimLifafaResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public String getCategoryResponse() {
        return categoryResponse;
    }

    public void setCategoryResponse(String categoryResponse) {
        this.categoryResponse = categoryResponse;
    }

    public ClaimLifafaResponse withCategoryResponse(String categoryResponse) {
        this.categoryResponse = categoryResponse;
        return this;
    }

    public ClaimLifafaResponse.ThemeInfo getThemeInfo() {
        return themeInfo;
    }

    public void setThemeInfo(ClaimLifafaResponse.ThemeInfo themeInfo) {
        this.themeInfo = themeInfo;
    }

    public ClaimLifafaResponse withThemeInfo(ClaimLifafaResponse.ThemeInfo themeInfo) {
        this.themeInfo = themeInfo;
        return this;
    }

    public Integer getClaimedQuantity() {
        return claimedQuantity;
    }

    public void setClaimedQuantity(Integer claimedQuantity) {
        this.claimedQuantity = claimedQuantity;
    }

    public ClaimLifafaResponse withClaimedQuantity(Integer claimedQuantity) {
        this.claimedQuantity = claimedQuantity;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("statusCode", statusCode).append("statusMessage", statusMessage)
                .append("categoryResponse", categoryResponse).append("themeInfo", themeInfo)
                .append("claimedQuantity", claimedQuantity).toString();
    }

    public static class ThemeInfo {

        private String themeName;
        private String id;
        private Metadata metadata;

        public String getThemeName() {
            return themeName;
        }

        public void setThemeName(String themeName) {
            this.themeName = themeName;
        }

        public ClaimLifafaResponse.ThemeInfo withThemeName(String themeName) {
            this.themeName = themeName;
            return this;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ClaimLifafaResponse.ThemeInfo withId(String id) {
            this.id = id;
            return this;
        }

        public Metadata getMetadata() {
            return metadata;
        }

        public void setMetadata(Metadata metadata) {
            this.metadata = metadata;
        }

        public ClaimLifafaResponse.ThemeInfo withMetadata(Metadata metadata) {
            this.metadata = metadata;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("themeName", themeName).append("id", id)
                    .append("metadata", metadata).toString();
        }

        public static class Metadata {

            private List<ThumbnailView> thumbnailView = null;
            private List<PassbookView> passbookView = null;
            private List<PushView> pushView = null;
            private List<SummaryView> summaryView = null;
            private List<ApplicationView> applicationView = null;

            public List<ThumbnailView> getThumbnailView() {
                return thumbnailView;
            }

            public void setThumbnailView(List<ThumbnailView> thumbnailView) {
                this.thumbnailView = thumbnailView;
            }

            public Metadata withThumbnailView(List<ThumbnailView> thumbnailView) {
                this.thumbnailView = thumbnailView;
                return this;
            }

            public List<PassbookView> getPassbookView() {
                return passbookView;
            }

            public void setPassbookView(List<PassbookView> passbookView) {
                this.passbookView = passbookView;
            }

            public Metadata withPassbookView(List<PassbookView> passbookView) {
                this.passbookView = passbookView;
                return this;
            }

            public List<PushView> getPushView() {
                return pushView;
            }

            public void setPushView(List<PushView> pushView) {
                this.pushView = pushView;
            }

            public Metadata withPushView(List<PushView> pushView) {
                this.pushView = pushView;
                return this;
            }

            public List<SummaryView> getSummaryView() {
                return summaryView;
            }

            public void setSummaryView(List<SummaryView> summaryView) {
                this.summaryView = summaryView;
            }

            public Metadata withSummaryView(List<SummaryView> summaryView) {
                this.summaryView = summaryView;
                return this;
            }

            public List<ApplicationView> getApplicationView() {
                return applicationView;
            }

            public void setApplicationView(List<ApplicationView> applicationView) {
                this.applicationView = applicationView;
            }

            public Metadata withApplicationView(List<ApplicationView> applicationView) {
                this.applicationView = applicationView;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("thumbnailView", thumbnailView)
                        .append("passbookView", passbookView).append("pushView", pushView)
                        .append("summaryView", summaryView).append("applicationView", applicationView).toString();
            }

            public static class PassbookView {

                private String resolution;
                private String url1;

                public String getResolution() {
                    return resolution;
                }

                public void setResolution(String resolution) {
                    this.resolution = resolution;
                }

                public PassbookView withResolution(String resolution) {
                    this.resolution = resolution;
                    return this;
                }

                public String getUrl1() {
                    return url1;
                }

                public void setUrl1(String url1) {
                    this.url1 = url1;
                }

                public PassbookView withUrl1(String url1) {
                    this.url1 = url1;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("resolution", resolution).append("url1", url1)
                            .toString();
                }
            }

            public static class PushView {

                @Override
                public String toString() {
                    return new ToStringBuilder(this).toString();
                }

            }

            public static class SummaryView {

                private String resolution;
                private String url1;

                public String getResolution() {
                    return resolution;
                }

                public void setResolution(String resolution) {
                    this.resolution = resolution;
                }

                public SummaryView withResolution(String resolution) {
                    this.resolution = resolution;
                    return this;
                }

                public String getUrl1() {
                    return url1;
                }

                public void setUrl1(String url1) {
                    this.url1 = url1;
                }

                public SummaryView withUrl1(String url1) {
                    this.url1 = url1;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("resolution", resolution).append("url1", url1)
                            .toString();
                }

            }

            public static class ThumbnailView {

                private String resolution;
                private String url1;

                public String getResolution() {
                    return resolution;
                }

                public void setResolution(String resolution) {
                    this.resolution = resolution;
                }

                public ThumbnailView withResolution(String resolution) {
                    this.resolution = resolution;
                    return this;
                }

                public String getUrl1() {
                    return url1;
                }

                public void setUrl1(String url1) {
                    this.url1 = url1;
                }

                public ThumbnailView withUrl1(String url1) {
                    this.url1 = url1;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("resolution", resolution).append("url1", url1)
                            .toString();
                }

            }

            public static class ApplicationView {

                private String resolution;
                private String url1;
                private String url2;
                private String url3;
                private String url4;

                public String getResolution() {
                    return resolution;
                }

                public void setResolution(String resolution) {
                    this.resolution = resolution;
                }

                public ApplicationView withResolution(String resolution) {
                    this.resolution = resolution;
                    return this;
                }

                public String getUrl1() {
                    return url1;
                }

                public void setUrl1(String url1) {
                    this.url1 = url1;
                }

                public ApplicationView withUrl1(String url1) {
                    this.url1 = url1;
                    return this;
                }

                public String getUrl2() {
                    return url2;
                }

                public void setUrl2(String url2) {
                    this.url2 = url2;
                }

                public ApplicationView withUrl2(String url2) {
                    this.url2 = url2;
                    return this;
                }

                public String getUrl3() {
                    return url3;
                }

                public void setUrl3(String url3) {
                    this.url3 = url3;
                }

                public ApplicationView withUrl3(String url3) {
                    this.url3 = url3;
                    return this;
                }

                public String getUrl4() {
                    return url4;
                }

                public void setUrl4(String url4) {
                    this.url4 = url4;
                }

                public ApplicationView withUrl4(String url4) {
                    this.url4 = url4;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("resolution", resolution).append("url1", url1)
                            .append("url2", url2).append("url3", url3).append("url4", url4).toString();
                }

            }
        }
    }

    @Override
    public Object getStatus() {
        // TODO Auto-generated method stub
        return null;
    }
}

