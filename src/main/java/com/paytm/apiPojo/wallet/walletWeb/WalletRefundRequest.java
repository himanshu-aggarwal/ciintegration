package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.helpers.CommonHelpers;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;
import java.util.List;

public class WalletRefundRequest extends requestPojo {

    private Request request;
    private String ipAddress;
    private String platformName;
    private String operationType;
    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager){
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager(){
        return this.updateAmountManager;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public WalletRefundRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public WalletRefundRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public WalletRefundRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public WalletRefundRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("platformName", platformName).append("operationType", operationType).toString();
    }

    public class Request {

        private String txnGuid;
        private Object pgTxnId;
        private BigDecimal amount;
        private String currencyCode;
        private String merchantGuid;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String merchantOrderId= "AutomRF" + Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private List<Object> itemDetails = null;
        private SubWalletAmount subWalletAmount;
        private String isCommissionRollback;
        private BigDecimal baseAmount;
        private BigDecimal commissionRollBackAmount;

        public String getTxnGuid() {
            return txnGuid;
        }

        public BigDecimal getBaseAmount() {
            return baseAmount;
        }

        public void setBaseAmount(BigDecimal baseAmount) {
            this.baseAmount = baseAmount;
        }

        public Request withBaseAmount(BigDecimal baseAmount){
            this.baseAmount = baseAmount;
            return this;
        }

        public BigDecimal getCommissionRollBackAmount() {
            return commissionRollBackAmount;
        }

        public void setCommissionRollBackAmount(BigDecimal commissionRollBackAmount) {
            this.commissionRollBackAmount = commissionRollBackAmount;
        }

        public Request withCommissionRollBackAmount(BigDecimal commissionRollBackAmount){
            this.commissionRollBackAmount=commissionRollBackAmount;
            return this;
        }

        public void setTxnGuid(String txnGuid) {
            this.txnGuid = txnGuid;
        }

        public Request withTxnGuid(String txnGuid) {
            this.txnGuid = txnGuid;
            return this;
        }


        public String getIsCommissionRollback() {
            return isCommissionRollback;
        }

        public void setIsCommissionRollback(String isCommissionRollback) {
            this.isCommissionRollback = isCommissionRollback;
        }

        public Request withIsCommissionRollback(String isCommissionRollback){
            this.isCommissionRollback=isCommissionRollback;
            return this;
        }

        public Object getPgTxnId() {
            return pgTxnId;
        }

        public void setPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
        }

        public Request withPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
            return this;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
            if(WalletRefundRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
        }

        public Request withAmount(BigDecimal amount) {
            this.amount = amount;
            if(WalletRefundRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
            return this;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Request withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Request withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public List<Object> getItemDetails() {
            return itemDetails;
        }

        public void setItemDetails(List<Object> itemDetails) {
            this.itemDetails = itemDetails;
        }

        public Request withItemDetails(List<Object> itemDetails) {
            this.itemDetails = itemDetails;
            return this;
        }

        public SubWalletAmount getSubWalletAmount() {
            return subWalletAmount;
        }

        public void setSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
        }

        public Request withSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("txnGuid", txnGuid).append("pgTxnId", pgTxnId)
                    .append("amount", amount).append("currencyCode", currencyCode)
                    .append("merchantGuid", merchantGuid).append("merchantOrderId", merchantOrderId)
                    .append("itemDetails", itemDetails).append("subWalletAmount", subWalletAmount).append("isCommissionRollback",isCommissionRollback).append("baseAmount",baseAmount).append("commissionRollBackAmount",commissionRollBackAmount).toString();
        }

        @JsonPropertyOrder({"TOLL","INTERNATIONAL_FUNDS_TRANSFER","GIFT_VOUCHER","GIFT","CASHBACK","FUEL","FOOD","CLOSED_LOOP_SUB_WALLET","CLOSED_LOOP_WALLET"})
        public class SubWalletAmount {

            @JsonProperty("FOOD")
            private BigDecimal fOOD;
            @JsonProperty("GIFT")
            private BigDecimal gIFT;
            @JsonProperty("TOLL")
            private BigDecimal tOLL;
            @JsonProperty("FUEL")
            private BigDecimal fUEL;
            @JsonProperty("CLOSED_LOOP_WALLET")
            private BigDecimal cLOSED_LOOP_WALLET;
            @JsonProperty("CLOSED_LOOP_SUB_WALLET")
            private BigDecimal cLOSED_LOOP_SUB_WALLET;
            @JsonProperty("INTERNATIONAL_FUNDS_TRANSFER")
            private BigDecimal iNTERNATIONAL_FUNDS_TRANSFER;
            @JsonProperty("GIFT_VOUCHER")
            private BigDecimal gIFT_VOUCHER;
            @JsonProperty("CASHBACK")
            private BigDecimal cASHBACK;

            public BigDecimal getfOOD() {
                return fOOD;
            }

            public void setfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFoodWalletAmount(fOOD);
                }
            }

            public SubWalletAmount withfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFoodWalletAmount(fOOD);
                }
                return this;
            }

            public SubWalletAmount withfOOD(int fOOD) {
                this.fOOD = new BigDecimal(fOOD);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFoodWalletAmount(this.fOOD);
                }
                return this;
            }

            public BigDecimal getcLOSED_LOOP_SUB_WALLET() {
                return cLOSED_LOOP_SUB_WALLET;
            }

            public void setcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
                return this;
            }

            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(int cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = new BigDecimal(cLOSED_LOOP_SUB_WALLET);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
                return this;
            }

            public BigDecimal getgIFT() {
                return gIFT;
            }

            public void setgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
            }

            public SubWalletAmount withgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
                return this;
            }

            public SubWalletAmount withgIFT(int gIFT) {
                this.gIFT = new BigDecimal(gIFT);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
                return this;
            }

            public BigDecimal gettOLL() {
                return tOLL;
            }

            public void settOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
            }

            public SubWalletAmount withtOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
                return this;
            }

            public SubWalletAmount withtOLL(int tOLL) {
                this.tOLL = new BigDecimal(tOLL);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
                return this;
            }

            public BigDecimal getfUEL() {
                return fUEL;
            }

            public void setfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
            }

            public SubWalletAmount withfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
                return this;
            }

            public SubWalletAmount withfUEL(int fUEL) {
                this.fUEL = new BigDecimal(fUEL);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
                return this;
            }

            public BigDecimal getcLOSED_LOOP_WALLET() {
                return cLOSED_LOOP_WALLET;
            }

            public void setcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
                return this;
            }

            public SubWalletAmount withcLOSED_LOOP_WALLET(int cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = new BigDecimal(cLOSED_LOOP_WALLET);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
                return this;
            }

            public BigDecimal getiNTERNATIONAL_FUNDS_TRANSFER() {
                return iNTERNATIONAL_FUNDS_TRANSFER;
            }

            public void setiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
            }

            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
                return this;
            }

            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(int iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = new BigDecimal(iNTERNATIONAL_FUNDS_TRANSFER);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
                return this;
            }

            public BigDecimal getgIFT_VOUCHER() {
                return gIFT_VOUCHER;
            }

            public void setgIFT_VOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
            }

            public SubWalletAmount withgIFTVOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
                return this;
            }

            public SubWalletAmount withgIFTVOUCHER(int gIFT_VOUCHER) {
                this.gIFT_VOUCHER = new BigDecimal(gIFT_VOUCHER);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
                return this;
            }

            public BigDecimal getcASHBACK() {
                return cASHBACK;
            }

            public void setcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
            }

            public SubWalletAmount withcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
                return this;
            }

            public SubWalletAmount withcASHBACK(int cASHBACK) {
                this.cASHBACK = new BigDecimal(cASHBACK);
                if(WalletRefundRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("FOOD", fOOD).append("GIFT", gIFT).append("TOLL", tOLL)
                        .append("FUEL", fUEL).append("CLOSED_LOOP_WALLET", cLOSED_LOOP_WALLET)
                        .append("CLOSED_LOOP_SUB_WALLET", cLOSED_LOOP_SUB_WALLET).append("CASHBACK", cASHBACK)
                        .append("INTERNATIONAL_FUNDS_TRANSFER", iNTERNATIONAL_FUNDS_TRANSFER)
                        .append("GIFT_VOUCHER", gIFT_VOUCHER).toString();
            }
        }
    }
}

