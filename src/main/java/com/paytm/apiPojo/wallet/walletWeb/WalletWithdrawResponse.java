package com.paytm.apiPojo.wallet.walletWeb;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class WalletWithdrawResponse extends responsePojo {

    private Object type;
    private Object requestGuid;
    private String orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public enum StatusTxt {
        SUCCESS, FAILURE
    }

    public Object getType() {
        return type;
    }

    public Object getRequestGuid() {
        return requestGuid;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public Response getResponse() {
        return response;
    }

    public Object getMetadata() {
        return metadata;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid)
                .append("orderId", orderId).append("status", status).append("statusCode", statusCode)
                .append("statusMessage", statusMessage).append("response", response).append("metadata", metadata)
                .append("additionalProperties", additionalProperties).toString();
    }

    public static class Response {

        private String userGuid;
        private Object pgTxnId;
        private Long timestamp;
        private Object cashBackStatus;
        private Object cashBackMessage;
        private Object state;
        private String heading;
        private Object walletSysTransactionId;
        private String walletSystemTxnId;
        private String comment;
        private String posId;
        private Double txnAmount;
        private String merchantOrderId;
        private Object uniqueReferenceLabel;
        private Object uniqueReferenceValue;
        private Object pccCode;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        public String getUserGuid() {
            return userGuid;
        }

        public void setUserGuid(String userGuid) {
            this.userGuid = userGuid;
        }

        public Response withUserGuid(String userGuid) {
            this.userGuid = userGuid;
            return this;
        }

        public Object getPgTxnId() {
            return pgTxnId;
        }

        public void setPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
        }

        public Response withPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
            return this;
        }

        public Long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Long timestamp) {
            this.timestamp = timestamp;
        }

        public Response withTimestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Object getCashBackStatus() {
            return cashBackStatus;
        }

        public void setCashBackStatus(Object cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
        }

        public Response withCashBackStatus(Object cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
            return this;
        }

        public Object getCashBackMessage() {
            return cashBackMessage;
        }

        public void setCashBackMessage(Object cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
        }

        public Response withCashBackMessage(Object cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
            return this;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Response withState(Object state) {
            this.state = state;
            return this;
        }

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public Response withHeading(String heading) {
            this.heading = heading;
            return this;
        }

        public Object getWalletSysTransactionId() {
            return walletSysTransactionId;
        }

        public void setWalletSysTransactionId(Object walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
        }

        public Response withWalletSysTransactionId(Object walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
            return this;
        }

        public String getWalletSystemTxnId() {
            return walletSystemTxnId;
        }

        public void setWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
        }

        public Response withWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
            return this;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Response withComment(String comment) {
            this.comment = comment;
            return this;
        }

        public String getPosId() {
            return posId;
        }

        public void setPosId(String posId) {
            this.posId = posId;
        }

        public Response withPosId(String posId) {
            this.posId = posId;
            return this;
        }

        public Double getTxnAmount() {
            return txnAmount;
        }

        public void setTxnAmount(Double txnAmount) {
            this.txnAmount = txnAmount;
        }

        public Response withTxnAmount(Double txnAmount) {
            this.txnAmount = txnAmount;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Response withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public Object getUniqueReferenceLabel() {
            return uniqueReferenceLabel;
        }

        public void setUniqueReferenceLabel(Object uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
        }

        public Response withUniqueReferenceLabel(Object uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
            return this;
        }

        public Object getUniqueReferenceValue() {
            return uniqueReferenceValue;
        }

        public void setUniqueReferenceValue(Object uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
        }

        public Response withUniqueReferenceValue(Object uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
            return this;
        }

        public Object getPccCode() {
            return pccCode;
        }

        public void setPccCode(Object pccCode) {
            this.pccCode = pccCode;
        }

        public Response withPccCode(Object pccCode) {
            this.pccCode = pccCode;
            return this;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        public Response withAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("userGuid", userGuid).append("pgTxnId", pgTxnId)
                    .append("timestamp", timestamp).append("cashBackStatus", cashBackStatus)
                    .append("cashBackMessage", cashBackMessage).append("state", state).append("heading", heading)
                    .append("walletSysTransactionId", walletSysTransactionId)
                    .append("walletSystemTxnId", walletSystemTxnId).append("comment", comment)
                    .append("posId", posId).append("txnAmount", txnAmount)
                    .append("merchantOrderId", merchantOrderId).append("uniqueReferenceLabel", uniqueReferenceLabel)
                    .append("uniqueReferenceValue", uniqueReferenceValue).append("pccCode", pccCode)
                    .append("additionalProperties", additionalProperties).toString();
        }

    }

}
