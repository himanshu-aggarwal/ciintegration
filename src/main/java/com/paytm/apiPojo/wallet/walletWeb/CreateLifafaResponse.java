package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class CreateLifafaResponse extends responsePojo {

    private String statusCode;
    private String statusMessage;
    private String lifafaKey;
    private Object qrCodeId;
    private FailedRecipientMap failedRecipientMap;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public CreateLifafaResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public CreateLifafaResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public String getLifafaKey() {
        return lifafaKey;
    }

    public void setLifafaKey(String lifafaKey) {
        this.lifafaKey = lifafaKey;
    }

    public CreateLifafaResponse withLifafaKey(String lifafaKey) {
        this.lifafaKey = lifafaKey;
        return this;
    }

    public Object getQrCodeId() {
        return qrCodeId;
    }

    public void setQrCodeId(Object qrCodeId) {
        this.qrCodeId = qrCodeId;
    }

    public CreateLifafaResponse withQrCodeId(Object qrCodeId) {
        this.qrCodeId = qrCodeId;
        return this;
    }

    public FailedRecipientMap getFailedRecipientMap() {
        return failedRecipientMap;
    }

    public void setFailedRecipientMap(FailedRecipientMap failedRecipientMap) {
        this.failedRecipientMap = failedRecipientMap;
    }

    public CreateLifafaResponse withFailedRecipientMap(FailedRecipientMap failedRecipientMap) {
        this.failedRecipientMap = failedRecipientMap;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("statusCode", statusCode).append("statusMessage", statusMessage)
                .append("lifafaKey", lifafaKey).append("qrCodeId", qrCodeId)
                .append("failedRecipientMap", failedRecipientMap).toString();
    }

    public class FailedRecipientMap {

        @Override
        public String toString() {
            return new ToStringBuilder(this).toString();
        }

    }

    @Override
    public String getStatus() {
        // TODO Auto-generated method stub
        return null;
    }

}

