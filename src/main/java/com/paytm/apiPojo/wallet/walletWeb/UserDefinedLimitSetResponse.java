package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface;
import org.apache.commons.lang.builder.ToStringBuilder;

public class UserDefinedLimitSetResponse extends APIInterface.responsePojo {

    @JsonProperty("statusCode")
    private String statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("status")
    @JsonIgnore
    private String status;
    @JsonProperty("otpState")
    private Object otpState;


    public String getStatus() {
        return status;
    }


    @JsonProperty("statusCode")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public UserDefinedLimitSetResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public UserDefinedLimitSetResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    @JsonProperty("otpState")
    public Object getOtpState() {
        return otpState;
    }

    @JsonProperty("otpState")
    public void setOtpState(Object otpState) {
        this.otpState = otpState;
    }

    public UserDefinedLimitSetResponse withOtpState(Object otpState) {
        this.otpState = otpState;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("statusCode", statusCode).append("statusMessage", statusMessage).append("otpState", otpState).toString();
    }


}
