package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "request", "ipAddress", "platformName", "operationType" })
public class UpgradeWalletRequest extends requestPojo {
	@JsonProperty("request")
	private Request request;
	@JsonProperty("ipAddress")
	private String ipAddress;
	@JsonProperty("platformName")
	private String platformName;
	@JsonProperty("operationType")
	private String operationType;

	private Boolean updateAmountManager = false;

	public void setUpdateAmountManager(Boolean updateAmountManager) {
		this.updateAmountManager = updateAmountManager;
	}

	public Boolean getUpdateAmountManager() {
		return this.updateAmountManager;
	}

	@JsonProperty("request")
	public Request getRequest() {
		return request;
	}

	@JsonProperty("request")
	public void setRequest(Request request) {
		this.request = request;
	}

	public UpgradeWalletRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	@JsonProperty("ipAddress")
	public String getIpAddress() {
		return ipAddress;
	}

	@JsonProperty("ipAddress")
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public UpgradeWalletRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	@JsonProperty("platformName")
	public String getPlatformName() {
		return platformName;
	}

	@JsonProperty("platformName")
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public UpgradeWalletRequest withPlatformName(String platformName) {
		this.platformName = platformName;
		return this;
	}

	@JsonProperty("operationType")
	public String getOperationType() {
		return operationType;
	}

	@JsonProperty("operationType")
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public UpgradeWalletRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({ "ssoId", "idProofVerified", "addressProofVerified" })
	public class Request {

		@JsonProperty("ssoId")
		private String ssoId;
		@JsonProperty("idProofVerified")
		private Boolean idProofVerified;
		@JsonProperty("walletState")
		private String walletState;
		@JsonProperty("rbiMigration")
		private Boolean rbiMigration;
		@JsonProperty("addressProofVerified")
		private Boolean addressProofVerified;

		@JsonProperty("ssoId")
		public String getSsoId() {
			return ssoId;
		}

		@JsonProperty("ssoId")
		public void setSsoId(String ssoId) {
			this.ssoId = ssoId;
		}

		public Request withSsoId(String ssoId) {
			this.ssoId = ssoId;
			return this;
		}

		@JsonProperty("idProofVerified")
		public Boolean getIdProofVerified() {
			return idProofVerified;
		}

		@JsonProperty("idProofVerified")
		public void setIdProofVerified(Boolean idProofVerified) {
			this.idProofVerified = idProofVerified;
		}

		public Request withIdProofVerified(Boolean idProofVerified) {
			this.idProofVerified = idProofVerified;
			return this;
		}

		@JsonProperty("walletState")
		public String getWalletState() {
			return walletState;
		}

		@JsonProperty("walletState")
		public void setWalletState(String walletState) {
			this.walletState = walletState;
		}

		public Request withWalletState(String walletState) {
			this.walletState = walletState;
			return this;
		}

		@JsonProperty("rbiMigration")
		public Boolean getRbiMigration() {
			return rbiMigration;
		}

		@JsonProperty("rbiMigration")
		public void setRbiMigration(Boolean rbiMigration) {
			this.rbiMigration = rbiMigration;
		}

		public Request withRbiMigration(Boolean rbiMigration) {
			this.rbiMigration = rbiMigration;
			return this;
		}

		@JsonProperty("addressProofVerified")
		public Boolean getAddressProofVerified() {
			return addressProofVerified;
		}

		@JsonProperty("addressProofVerified")
		public void setAddressProofVerified(Boolean addressProofVerified) {
			this.addressProofVerified = addressProofVerified;
		}

		public Request withAddressProofVerified(Boolean addressProofVerified) {
			this.addressProofVerified = addressProofVerified;
			return this;
		}

	}
}
