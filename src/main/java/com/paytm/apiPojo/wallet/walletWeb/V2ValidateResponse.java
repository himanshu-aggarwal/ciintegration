package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class V2ValidateResponse extends responsePojo {

    private Object type;
    private Object requestGuid;
    private Object orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public V2ValidateResponse withType(Object type) {
        this.type = type;
        return this;
    }

    public Object getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public V2ValidateResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public V2ValidateResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public V2ValidateResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public V2ValidateResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public V2ValidateResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public V2ValidateResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public V2ValidateResponse withMetadata(Object metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid)
                .append("orderId", orderId).append("status", status).append("statusCode", statusCode)
                .append("statusMessage", statusMessage).append("response", response).append("metadata", metadata)
                .toString();
    }

    public static class Response {

        private String walletSysTransactionId;
        private String heading;
        private Object payeeSsoId;
        private Object displayName;
        private String userGuid;
        private Object pgTxnId;
        private Integer timestamp;
        private String cashBackStatus;
        private String cashBackMessage;
        private String comment;
        private String posId;
        private String uniqueReferenceLabel;
        private String uniqueReferenceValue;
        private Integer txnAmount;
        private String merchantOrderId;
        private Boolean firstPPBTransaction;
        private Object isBeneficiaryAdded;
        private Object coolingOffTime;

        public String getWalletSysTransactionId() {
            return walletSysTransactionId;
        }

        public void setWalletSysTransactionId(String walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
        }

        public Response withWalletSysTransactionId(String walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
            return this;
        }

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public Response withHeading(String heading) {
            this.heading = heading;
            return this;
        }

        public Object getPayeeSsoId() {
            return payeeSsoId;
        }

        public void setPayeeSsoId(Object payeeSsoId) {
            this.payeeSsoId = payeeSsoId;
        }

        public Response withPayeeSsoId(Object payeeSsoId) {
            this.payeeSsoId = payeeSsoId;
            return this;
        }

        public Object getDisplayName() {
            return displayName;
        }

        public void setDisplayName(Object displayName) {
            this.displayName = displayName;
        }

        public Response withDisplayName(Object displayName) {
            this.displayName = displayName;
            return this;
        }

        public String getUserGuid() {
            return userGuid;
        }

        public void setUserGuid(String userGuid) {
            this.userGuid = userGuid;
        }

        public Response withUserGuid(String userGuid) {
            this.userGuid = userGuid;
            return this;
        }

        public Object getPgTxnId() {
            return pgTxnId;
        }

        public void setPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
        }

        public Response withPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
            return this;
        }

        public Integer getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Integer timestamp) {
            this.timestamp = timestamp;
        }

        public Response withTimestamp(Integer timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public String getCashBackStatus() {
            return cashBackStatus;
        }

        public void setCashBackStatus(String cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
        }

        public Response withCashBackStatus(String cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
            return this;
        }

        public String getCashBackMessage() {
            return cashBackMessage;
        }

        public void setCashBackMessage(String cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
        }

        public Response withCashBackMessage(String cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
            return this;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Response withComment(String comment) {
            this.comment = comment;
            return this;
        }

        public String getPosId() {
            return posId;
        }

        public void setPosId(String posId) {
            this.posId = posId;
        }

        public Response withPosId(String posId) {
            this.posId = posId;
            return this;
        }

        public String getUniqueReferenceLabel() {
            return uniqueReferenceLabel;
        }

        public void setUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
        }

        public Response withUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
            return this;
        }

        public String getUniqueReferenceValue() {
            return uniqueReferenceValue;
        }

        public void setUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
        }

        public Response withUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
            return this;
        }

        public Integer getTxnAmount() {
            return txnAmount;
        }

        public void setTxnAmount(Integer txnAmount) {
            this.txnAmount = txnAmount;
        }

        public Response withTxnAmount(Integer txnAmount) {
            this.txnAmount = txnAmount;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Response withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public Boolean getFirstPPBTransaction() {
            return firstPPBTransaction;
        }

        public void setFirstPPBTransaction(Boolean firstPPBTransaction) {
            this.firstPPBTransaction = firstPPBTransaction;
        }

        public Response withFirstPPBTransaction(Boolean firstPPBTransaction) {
            this.firstPPBTransaction = firstPPBTransaction;
            return this;
        }

        public Object getIsBeneficiaryAdded() {
            return isBeneficiaryAdded;
        }

        public void setIsBeneficiaryAdded(Object isBeneficiaryAdded) {
            this.isBeneficiaryAdded = isBeneficiaryAdded;
        }

        public Response withIsBeneficiaryAdded(Object isBeneficiaryAdded) {
            this.isBeneficiaryAdded = isBeneficiaryAdded;
            return this;
        }

        public Object getCoolingOffTime() {
            return coolingOffTime;
        }

        public void setCoolingOffTime(Object coolingOffTime) {
            this.coolingOffTime = coolingOffTime;
        }

        public Response withCoolingOffTime(Object coolingOffTime) {
            this.coolingOffTime = coolingOffTime;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("walletSysTransactionId", walletSysTransactionId)
                    .append("heading", heading).append("payeeSsoId", payeeSsoId).append("displayName", displayName)
                    .append("userGuid", userGuid).append("pgTxnId", pgTxnId).append("timestamp", timestamp)
                    .append("cashBackStatus", cashBackStatus).append("cashBackMessage", cashBackMessage)
                    .append("comment", comment).append("posId", posId)
                    .append("uniqueReferenceLabel", uniqueReferenceLabel)
                    .append("uniqueReferenceValue", uniqueReferenceValue).append("txnAmount", txnAmount)
                    .append("merchantOrderId", merchantOrderId).append("firstPPBTransaction", firstPPBTransaction)
                    .append("isBeneficiaryAdded", isBeneficiaryAdded).append("coolingOffTime", coolingOffTime)
                    .toString();
        }

    }

}

