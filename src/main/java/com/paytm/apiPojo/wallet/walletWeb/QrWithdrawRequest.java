package com.paytm.apiPojo.wallet.walletWeb;
import com.paytm.apiPojo.wallet.APIInterface.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;
import java.util.List;

public class QrWithdrawRequest extends requestPojo {

    private Request request;
    private String platformName;
    private String ipAddress;
    private String channel;
    private String version;
    private String operationType;

    public Request getRequest() {
        return request;
    }

    public QrWithdrawRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getPlatformName() {
        return platformName;
    }

    public QrWithdrawRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }


    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public QrWithdrawRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getChannel() {
        return channel;
    }

    public QrWithdrawRequest withChannel(String channel) {
        this.channel = channel;
        return this;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getVersion() {
        return version;
    }

    public QrWithdrawRequest withVersion(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOperationType() {
        return operationType;
    }

    public QrWithdrawRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("platformName", platformName).append("operationType", operationType).append("channel", channel).append("version", version).toString();
    }

    public static class Request {
        private BigDecimal totalAmount;
        private String currencyCode;
        private String merchantGuid;
        private String industryType;
        private String sourceId;
        private String sourceName;
        private String destinationId;
        private String destinationName;
        private String busType;
        private String routeId;
        private String uniqueReferenceLabel;
        private String uniqueReferenceValue;
        private String routeName;
        private List<PaxDetails> paxDetails;

        public BigDecimal getTotalAmount() {
            return totalAmount;
        }

        public Request withTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
            return this;
        }

        public void setTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public Request withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            return this;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
        }

        public String getIndustryType() {
            return industryType;
        }

        public Request withIndustryType(String industryType) {
            this.industryType = industryType;
            return this;
        }

        public void setIndustryType(String industryType) {
            this.industryType = industryType;
        }

        public String getSourceId() {
            return sourceId;
        }

        public Request withSourceId(String sourceId) {
            this.sourceId = sourceId;
            return this;
        }

        public void setSourceId(String sourceId) {
            this.sourceId = sourceId;
        }

        public String getSourceName() {
            return sourceName;
        }

        public Request withSourceName(String sourceName) {
            this.sourceName = sourceName;
            return this;
        }

        public void setSourceName(String sourceName) {
            this.sourceName = sourceName;
        }

        public String getDestinationId() {
            return destinationId;
        }

        public Request withDestinationId(String destinationId) {
            this.destinationId = destinationId;
            return this;
        }

        public void setDestinationId(String destinationId) {
            this.destinationId = destinationId;
        }

        public String getDestinationName() {
            return destinationName;
        }

        public Request withDestinationName(String destinationName) {
            this.destinationName = destinationName;
            return this;
        }

        public void setDestinationName(String destinationName) {
            this.destinationName = destinationName;
        }

        public String getBusType() {
            return busType;
        }

        public Request withBusType(String busType) {
            this.busType = busType;
            return this;
        }

        public void setBusType(String busType) {
            this.busType = busType;
        }

        public String getRouteId() {
            return routeId;
        }

        public Request withRouteId(String routeId) {
            this.routeId = routeId;
            return this;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getUniqueReferenceLabel() {
            return uniqueReferenceLabel;
        }

        public Request withUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
            return this;
        }

        public void setUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
        }

        public String getUniqueReferenceValue() {
            return uniqueReferenceValue;
        }

        public Request withUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
            return this;
        }

        public void setUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
        }

        public String getRouteName() {
            return routeName;
        }

        public Request withRouteName(String routeName) {
            this.routeName = routeName;
            return this;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public List<PaxDetails> getPaxDetails() {
            return paxDetails;
        }

        public Request withPaxDetails(List<PaxDetails> paxDetails) {
            this.paxDetails = paxDetails;
            return this;
        }

        public void setPaxDetails(List<PaxDetails> paxDetails) {
            this.paxDetails = paxDetails;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("totalAmount", totalAmount).append("currencyCode", currencyCode)
                    .append("merchantGuid", merchantGuid).append("industryType", industryType).append("sourceId", sourceId).append("sourceName", sourceName)
                    .append("destinationId", destinationId).append("destinationName", destinationName).append("busType", busType).append("routeId", routeId)
                    .append("uniqueReferenceLabel", uniqueReferenceLabel).append("uniqueReferenceValue", uniqueReferenceValue).append("routeName", routeName).append("PaxDetails", paxDetails).toString();

        }

        public static class PaxDetails {
            public int quantity;
            public String pax;

            public int getQuantity() {
                return quantity;
            }

            public PaxDetails withQuantity(int quantity) {
                this.quantity = quantity;
                return this;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public String getPax() {
                return pax;
            }

            public PaxDetails withPax(String pax) {
                this.pax = pax;
                return this;
            }

            public void setPax(String pax) {
                this.pax = pax;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("quantity", quantity).append("pax", pax).toString();
            }

        }
    }
}