package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "requestGuid", "orderId", "status", "statusCode", "statusMessage", "response" })
public class UpgradeWalletResponse extends responsePojo{

	@JsonProperty("requestGuid")
	private Object requestGuid;
	@JsonProperty("orderId")
	private Object orderId;
	@JsonProperty("status")
	private Object status;
	@JsonProperty("statusCode")
	private String statusCode;
	@JsonProperty("statusMessage")
	private String statusMessage;
	@JsonProperty("response")
	private Response response;

	@JsonProperty("requestGuid")
	public Object getRequestGuid() {
		return requestGuid;
	}

	@JsonProperty("requestGuid")
	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}

	public UpgradeWalletResponse withRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
		return this;
	}

	@JsonProperty("orderId")
	public Object getOrderId() {
		return orderId;
	}

	@JsonProperty("orderId")
	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}

	public UpgradeWalletResponse withOrderId(Object orderId) {
		this.orderId = orderId;
		return this;
	}

	@JsonProperty("status")
	public Object getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(Object status) {
		this.status = status;
	}

	public UpgradeWalletResponse withStatus(Object status) {
		this.status = status;
		return this;
	}

	@JsonProperty("statusCode")
	public String getStatusCode() {
		return statusCode;
	}

	@JsonProperty("statusCode")
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public UpgradeWalletResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	@JsonProperty("statusMessage")
	public String getStatusMessage() {
		return statusMessage;
	}

	@JsonProperty("statusMessage")
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public UpgradeWalletResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	@JsonProperty("response")
	public Response getResponse() {
		return response;
	}

	@JsonProperty("response")
	public void setResponse(Response response) {
		this.response = response;
	}

	public UpgradeWalletResponse withResponse(Response response) {
		this.response = response;
		return this;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({ "ssoId", "walletRbiType" })
	public class Response {

		@JsonProperty("ssoId")
		private String ssoId;
		@JsonProperty("walletRbiType")
		private String walletRbiType;

		@JsonProperty("ssoId")
		public String getSsoId() {
			return ssoId;
		}

		@JsonProperty("ssoId")
		public void setSsoId(String ssoId) {
			this.ssoId = ssoId;
		}

		public Response withSsoId(String ssoId) {
			this.ssoId = ssoId;
			return this;
		}

		@JsonProperty("walletRbiType")
		public String getWalletRbiType() {
			return walletRbiType;
		}

		@JsonProperty("walletRbiType")
		public void setWalletRbiType(String walletRbiType) {
			this.walletRbiType = walletRbiType;
		}

		public Response withWalletRbiType(String walletRbiType) {
			this.walletRbiType = walletRbiType;
			return this;
		}

	}
}
