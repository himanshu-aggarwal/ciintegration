package com.paytm.apiPojo.wallet.walletWeb;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class WalletRefundResponse extends responsePojo {

    private Object type;
    private Object requestGuid;
    private String orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public enum StatusTxt {
        SUCCESS, FAILURE
    }

    public Object getType() {
        return type;
    }

    public Object getRequestGuid() {
        return requestGuid;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public Response getResponse() {
        return response;
    }

    public Object getMetadata() {
        return metadata;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid)
                .append("orderId", orderId).append("status", status).append("statusCode", statusCode)
                .append("statusMessage", statusMessage).append("response", response).append("metadata", metadata)
                .append("additionalProperties", additionalProperties).toString();
    }

    public static class Response {

        private String refundTxnGuid;
        private String refundTxnStatus;

        public String getRefundTxnGuid() {
            return refundTxnGuid;
        }

        public void setRefundTxnGuid(String refundTxnGuid) {
            this.refundTxnGuid = refundTxnGuid;
        }

        public Response withRefundTxnGuid(String refundTxnGuid) {
            this.refundTxnGuid = refundTxnGuid;
            return this;
        }

        public String getRefundTxnStatus() {
            return refundTxnStatus;
        }

        public void setRefundTxnStatus(String refundTxnStatus) {
            this.refundTxnStatus = refundTxnStatus;
        }

        public Response withRefundTxnStatus(String refundTxnStatus) {
            this.refundTxnStatus = refundTxnStatus;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("refundTxnGuid", refundTxnGuid)
                    .append("refundTxnStatus", refundTxnStatus).toString();
        }
    }
}

