package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.math.BigDecimal;
import java.util.Map;

public class UserDefinedLimitFetchResponse extends APIInterface.responsePojo {


    @JsonProperty("statusCode")
    private String statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("status")
    private String status;
    @JsonProperty("userLimitConfig")
    private Map<String, ConfigList> configList;
    @JsonProperty("limitDetails")
    private Map<String, LimitDetailsList> limitDetailsList;


    @JsonProperty("userLimitConfig")
    public Map<String, ConfigList> getConfigList() {
        return configList;
    }

    @JsonProperty("userLimitConfig")
    public void setConfigList(Map<String, ConfigList> configList) {
        this.configList = configList;
    }

    public UserDefinedLimitFetchResponse withConfigList(Map<String, ConfigList> configList) {
        this.configList = configList;
        return this;
    }

    @JsonProperty("limitDetails")
    public Map<String, LimitDetailsList> getLimitDetailsList() {
        return limitDetailsList;
    }

    @JsonProperty("limitDetails")
    public void setLimitDetailsList(Map<String, LimitDetailsList> limitDetailsList) {
        this.limitDetailsList = limitDetailsList;
    }

    public UserDefinedLimitFetchResponse withLimitDetailsList(Map<String, LimitDetailsList> limitDetailsList) {
        this.limitDetailsList = limitDetailsList;
        return this;
    }


    @JsonProperty("statusCode")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public UserDefinedLimitFetchResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public UserDefinedLimitFetchResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public UserDefinedLimitFetchResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userLimitConfig", configList).append("limitDetails",limitDetailsList).append("statusCode",statusCode).append("statusMessage",statusMessage).append("status",status).toString();
    }

    public static class ConfigList {

            @JsonProperty("status")
            private Boolean status;
            @JsonProperty("periodLimits")
            private PeriodLimits periodLimits;

            @JsonProperty("status")
            public Boolean getStatus() {
                return status;
            }

            @JsonProperty("status")
            public void setStatus(Boolean status) {
                this.status = status;
            }

            public ConfigList withStatus(Boolean status) {
                this.status = status;
                return this;
            }

            @JsonProperty("periodLimits")
            public PeriodLimits getPeriodLimits() {
                return periodLimits;
            }

            @JsonProperty("periodLimits")
            public void setPeriodLimits(PeriodLimits periodLimits) {
                this.periodLimits = periodLimits;
            }

            public ConfigList withPeriodLimits(PeriodLimits periodLimits) {
                this.periodLimits = periodLimits;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("status", status).append("periodLimits", periodLimits).toString();
            }

            public static class PeriodLimits {

                @JsonProperty("dayPeriodLimit")
                private SpecificPeriodLimit dayPeriodLimit;
                @JsonProperty("monthPeriodLimit")
                private SpecificPeriodLimit monthPeriodLimit;
                @JsonProperty("yearPeriodLimit")
                private SpecificPeriodLimit yearPeriodLimit;


                @JsonProperty("yearPeriodLimit")
                public SpecificPeriodLimit getYearPeriodLimit() {
                    return yearPeriodLimit;
                }

                @JsonProperty("yearPeriodLimit")
                public void setYearPeriodLimit(SpecificPeriodLimit yearPeriodLimit) {
                    this.yearPeriodLimit = yearPeriodLimit;
                }

                public PeriodLimits withYearPeriodLimit(SpecificPeriodLimit yearPeriodLimit) {
                    this.yearPeriodLimit = yearPeriodLimit;
                    return this;
                }

                @JsonProperty("dayPeriodLimit")
                public SpecificPeriodLimit getDayPeriodLimit() {
                    return dayPeriodLimit;
                }

                @JsonProperty("dayPeriodLimit")
                public void setDayPeriodLimit(SpecificPeriodLimit dayPeriodLimit) {
                    this.dayPeriodLimit = dayPeriodLimit;
                }

                public PeriodLimits withDayPeriodLimit(SpecificPeriodLimit dayPeriodLimit) {
                    this.dayPeriodLimit = dayPeriodLimit;
                    return this;
                }

                @JsonProperty("monthPeriodLimit")
                public SpecificPeriodLimit getMonthPeriodLimit() {
                    return monthPeriodLimit;
                }

                @JsonProperty("monthPeriodLimit")
                public void setMonthPeriodLimit(SpecificPeriodLimit monthPeriodLimit) {
                    this.monthPeriodLimit = monthPeriodLimit;
                }

                public PeriodLimits withMonthPeriodLimit(SpecificPeriodLimit monthPeriodLimit) {
                    this.monthPeriodLimit = monthPeriodLimit;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("dayPeriodLimit", dayPeriodLimit).append("monthPeriodLimit", monthPeriodLimit).append("yearPeriodLimit",yearPeriodLimit).toString();
                }


                public static class SpecificPeriodLimit {

                    @JsonProperty("amount")
                    private BigDecimal amount;
                    @JsonProperty("count")
                    private int count;

                    @JsonProperty("amount")
                    public BigDecimal getAmount() {
                        return amount;
                    }

                    @JsonProperty("amount")
                    public void setAmount(BigDecimal amount) {
                        this.amount = amount;
                    }

                    public SpecificPeriodLimit withAmount(BigDecimal amount) {
                        this.amount = amount;
                        return this;
                    }

                    @JsonProperty("count")
                    public int getCount() {
                        return count;
                    }

                    @JsonProperty("count")
                    public void setCount(int count) {
                        this.count = count;
                    }

                    public SpecificPeriodLimit withCount(int count) {
                        this.count = count;
                        return this;
                    }

                    @Override
                    public String toString() {
                        return new ToStringBuilder(this).append("amount", amount).append("count", count).toString();
                    }

                }


            }


        }

    public static class LimitDetailsList {

        @JsonProperty("status")
        private Boolean status;
        @JsonProperty("properties")
        private PeriodProperties periodProperties;

        @JsonProperty("status")
        public Boolean getStatus() {
            return status;
        }

        @JsonProperty("status")
        public void setStatus(Boolean status) {
            this.status = status;
        }

        public LimitDetailsList withStatus(Boolean status) {
            this.status = status;
            return this;
        }

        @JsonProperty("properties")
        public PeriodProperties getPeriodProperties() {
            return periodProperties;
        }

        @JsonProperty("properties")
        public void setPeriodProperties(PeriodProperties periodProperties) {
            this.periodProperties = periodProperties;
        }

        public LimitDetailsList withPeriodProperties(PeriodProperties periodProperties) {
            this.periodProperties = periodProperties;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("status", status).append("properties", periodProperties).toString();
        }

        public static class PeriodProperties {

            @JsonProperty("dayProperties")
            private SpecificPeriodProperties dayProperties;
            @JsonProperty("monthProperties")
            private SpecificPeriodProperties monthProperties;


            @JsonProperty("dayProperties")
            public SpecificPeriodProperties getDayProperties() {
                return dayProperties;
            }

            @JsonProperty("dayProperties")
            public void setDayProperties(SpecificPeriodProperties dayProperties) {
                this.dayProperties = dayProperties;
            }

            public PeriodProperties withDayProperties(SpecificPeriodProperties dayProperties) {
                this.dayProperties = dayProperties;
                return this;
            }

            @JsonProperty("monthProperties")
            public SpecificPeriodProperties getMonthProperties() {
                return monthProperties;
            }

            @JsonProperty("monthProperties")
            public void setMonthProperties(SpecificPeriodProperties monthProperties) {
                this.monthProperties = monthProperties;
            }

            public PeriodProperties withMonthProperties(SpecificPeriodProperties monthProperties) {
                this.monthProperties = monthProperties;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("dayProperties", dayProperties).append("monthProperties", monthProperties).toString();
            }


            public static class SpecificPeriodProperties {

                @JsonProperty("amountConsumed")
                private BigDecimal amountConsumed;
                @JsonProperty("countConsumed")
                private int countConsumed;

                @JsonProperty("amountConsumed")
                public BigDecimal getAmountConsumed() {
                    return amountConsumed;
                }

                @JsonProperty("amountConsumed")
                public void setAmountConsumed(BigDecimal amountConsumed) {
                    this.amountConsumed = amountConsumed;
                }

                public SpecificPeriodProperties withAmountConsumed(BigDecimal amountConsumed) {
                    this.amountConsumed = amountConsumed;
                    return this;
                }

                @JsonProperty("countConsumed")
                public int getCountConsumed() {
                    return countConsumed;
                }

                @JsonProperty("countConsumed")
                public void setCountConsumed(int countConsumed) {
                    this.countConsumed = countConsumed;
                }

                public SpecificPeriodProperties withCountConsumed(int countConsumed) {
                    this.countConsumed = countConsumed;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("amountConsumed", amountConsumed).append("countConsumed", countConsumed).toString();
                }

            }


        }


    }

}
