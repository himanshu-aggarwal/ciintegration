package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.apiPojo.wallet.APIInterface.SubWalletInterface;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.helpers.CommonHelpers;
import com.paytm.helpers.SubWalletHelpers;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class WalletWithdrawRequest extends requestPojo {

    private Request request;
    private String platformName;
    private String ipAddress;
    private String operationType;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager){
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager(){
        return this.updateAmountManager;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public WalletWithdrawRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public WalletWithdrawRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }


    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public WalletWithdrawRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public WalletWithdrawRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public WalletWithdrawRequest withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("platformName", platformName)
                .append("ipAddress", ipAddress).append("operationType", operationType)
                .append("additionalProperties", additionalProperties).toString();
    }

    public class Request {

        private Boolean skipRefill;
        private BigDecimal totalAmount;
        private String currencyCode;
        private String merchantGuid;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String merchantOrderId = "AutomW" + Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private String industryType;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String posId = "AutoWWP" +Thread.currentThread().getId()+ CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private String comment;
        private SubWalletAmount subWalletAmount;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
        private String pgTxnId;
        private String state;
    	private String otp;

        public Request() throws Exception {
        }
        
        

        public String getState() {
			return state;
		}



		public void setState(String state) {
			this.state = state;
		}
 
		public Request withState(String state) {
            this.state = state;
            return this;
        }


		public String getOtp() {
			return otp;
		}


		public void setOtp(String otp) {
			this.otp = otp;
		}

		public Request withOtp(String otp) {
            this.otp = otp;
            return this;
        }


		public Boolean getSkipRefill() {
            return skipRefill;
        }

        public void setSkipRefill(Boolean skipRefill) {
            this.skipRefill = skipRefill;
        }

        public Request withSkipRefill(Boolean skipRefill) {
            this.skipRefill = skipRefill;
            return this;
        }

        public SubWalletAmount getSubWalletAmount() {
            return subWalletAmount;
        }

        public void setSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
        }

        public BigDecimal getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
            if(WalletWithdrawRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(totalAmount);
                if(merchantGuid!=null&&!merchantGuid.equalsIgnoreCase("") && totalAmount != null && totalAmount.compareTo(BigDecimal.ZERO)>0){
                    SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
                    subWalletHelpers.calculateCommisionValue(this.merchantGuid,"RETAIL");
                }
            }
        }

        public Request withTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()){
                    FundThreadManager.getInstance().withTotalAmount(totalAmount);
                    if(merchantGuid!=null&&!merchantGuid.equalsIgnoreCase("") && totalAmount != null && totalAmount.compareTo(BigDecimal.ZERO)>0){
                        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
                        subWalletHelpers.calculateCommisionValue(this.merchantGuid,"RETAIL");
                    }

                }
            return this;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Request withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                if (totalAmount != null && totalAmount.compareTo(BigDecimal.ZERO)>0 && merchantGuid!=null&&!merchantGuid.equalsIgnoreCase("")) {
                    SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
                    subWalletHelpers.calculateCommisionValue(this.merchantGuid, "RETAIL");
                }
            }
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                if (totalAmount != null && totalAmount.compareTo(BigDecimal.ZERO) > 0 && merchantGuid!=null&&!merchantGuid.equalsIgnoreCase("")) {
                    SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
                    subWalletHelpers.calculateCommisionValue(this.merchantGuid, "RETAIL");
                }
            }
                return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Request withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }
        
        public String getpgTxnId() {
            return pgTxnId;
        }

        public void setpgTxnId(String pgTxnId) {
            this.pgTxnId = pgTxnId;
        }

        public Request withpgTxnId(String pgTxnId) {
            this.pgTxnId = pgTxnId;
            return this;
        }

        public String getIndustryType() {
            return industryType;
        }

        public void setIndustryType(String industryType) {
            this.industryType = industryType;
        }

        public Request withIndustryType(String industryType) {
            this.industryType = industryType;
            return this;
        }

        public String getPosId() {
            return posId;
        }

        public void setPosId(String posId) {
            this.posId = posId;
        }

        public Request withPosId(String posId) {
            this.posId = posId;
            return this;
        }

        public String getComment() {
            return comment;
        }

        public Request withComment(String comment) {
            this.comment = comment;
            return this;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        public Request withAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("skipRefill", skipRefill).append("totalAmount", totalAmount)
                    .append("currencyCode", currencyCode).append("merchantGuid", merchantGuid)
                    .append("merchantOrderId", merchantOrderId).append("industryType", industryType)
                    .append("posId", posId).append("comment", comment)
                    .append("additionalProperties", additionalProperties).toString();
        }

        public class SubWalletAmount extends SubWalletInterface {

            @JsonProperty("FOOD")
            private BigDecimal fOOD;
            @JsonProperty("GIFT")
            private BigDecimal gIFT;
            @JsonProperty("TOLL")
            private BigDecimal tOLL;
            @JsonProperty("FUEL")
            private BigDecimal fUEL;
            @JsonProperty("CLOSED_LOOP_WALLET")
            private BigDecimal cLOSED_LOOP_WALLET;
            @JsonProperty("CLOSED_LOOP_SUB_WALLET")
            private BigDecimal cLOSED_LOOP_SUB_WALLET;
            @JsonProperty("INTERNATIONAL_FUNDS_TRANSFER")
            private BigDecimal iNTERNATIONAL_FUNDS_TRANSFER;
            @JsonProperty("GIFT_VOUCHER")
            private BigDecimal gIFT_VOUCHER;
            @JsonProperty("CASHBACK")
            private BigDecimal cASHBACK;

            public BigDecimal getfOOD() {
                return fOOD;
            }

            public void setfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFoodWalletAmount(this.fOOD);
                }
            }

            public SubWalletAmount withfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFoodWalletAmount(this.fOOD);
                }
                return this;
            }

            public SubWalletAmount withfOOD(int fOOD) {
                this.fOOD = new BigDecimal(fOOD);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFoodWalletAmount(this.fOOD);
                }
                return this;
            }

            public BigDecimal getcLOSED_LOOP_SUB_WALLET() {
                return cLOSED_LOOP_SUB_WALLET;
            }

            public void setcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
                return this;
            }

            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(int cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = new BigDecimal(cLOSED_LOOP_SUB_WALLET);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
                return this;
            }

            public BigDecimal getgIFT() {
                return gIFT;
            }

            public void setgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
            }

            public SubWalletAmount withgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
                return this;
            }

            public SubWalletAmount withgIFT(int gIFT) {
                this.gIFT = new BigDecimal(gIFT);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
                return this;
            }

            public BigDecimal gettOLL() {
                return tOLL;
            }

            public void settOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
            }

            public SubWalletAmount withtOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
                return this;
            }

            public SubWalletAmount withtOLL(int tOLL) {
                this.tOLL = new BigDecimal(tOLL);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
                return this;
            }

            public BigDecimal getfUEL() {
                return fUEL;
            }

            public void setfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
            }

            public SubWalletAmount withfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
                return this;
            }

            public SubWalletAmount withfUEL(int fUEL) {
                this.fUEL = new BigDecimal(fUEL);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
                return this;
            }

            public BigDecimal getcLOSED_LOOP_WALLET() {
                return cLOSED_LOOP_WALLET;
            }

            public void setcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
                return this;
            }

            public SubWalletAmount withcLOSED_LOOP_WALLET(int cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = new BigDecimal(cLOSED_LOOP_WALLET);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
                return this;
            }
              public BigDecimal getiNTERNATIONAL_FUNDS_TRANSFER() {
                return iNTERNATIONAL_FUNDS_TRANSFER;
            }

            public void setiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
            }

            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
                return this;
            }

            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(int iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = new BigDecimal(iNTERNATIONAL_FUNDS_TRANSFER);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
                return this;
            }

            public BigDecimal getgIFT_VOUCHER() {
                return gIFT_VOUCHER;
            }

            public void setgIFT_VOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
            }

            public SubWalletAmount withgIFTVOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
                return this;
            }

            public SubWalletAmount withgIFTVOUCHER(int gIFT_VOUCHER) {
                this.gIFT_VOUCHER = new BigDecimal(gIFT_VOUCHER);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
                return this;
            }

            public BigDecimal getcASHBACK() {
                return cASHBACK;
            }

            public void setcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
            }

            public SubWalletAmount withcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
                return this;
            }

            public SubWalletAmount withcASHBACK(int cASHBACK) {
                this.cASHBACK = new BigDecimal(cASHBACK);
                if(WalletWithdrawRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("fOOD", fOOD).append("gIFT", gIFT).append("tOLL", tOLL)
                        .append("fUEL", fUEL).append("cLOSED_LOOP_WALLET", cLOSED_LOOP_WALLET)
                        .append("iNTERNATIONAL_FUNDS_TRANSFER", iNTERNATIONAL_FUNDS_TRANSFER)
                        .append("gIFT_VOUCHER", gIFT_VOUCHER).toString();
            }

        }

    }

}
