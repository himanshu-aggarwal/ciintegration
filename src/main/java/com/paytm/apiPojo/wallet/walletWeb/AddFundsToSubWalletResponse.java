package com.paytm.apiPojo.wallet.walletWeb;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class AddFundsToSubWalletResponse extends responsePojo {

    private Object requestGuid;
    private Object orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;

    public Object getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public AddFundsToSubWalletResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public AddFundsToSubWalletResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AddFundsToSubWalletResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public AddFundsToSubWalletResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public AddFundsToSubWalletResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public AddFundsToSubWalletResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId).append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage).append("response", response).toString();
    }


    public static class Response {

        private String walletSystemTxnId;
        private List<Response.SubWalletDetail> subWalletDetails = null;

        public String getWalletSystemTxnId() {
            return walletSystemTxnId;
        }

        public void setWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
        }

        public Response withWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
            return this;
        }

        public List<Response.SubWalletDetail> getSubWalletDetails() {
            return subWalletDetails;
        }

        public void setSubWalletDetails(List<Response.SubWalletDetail> subWalletDetails) {
            this.subWalletDetails = subWalletDetails;
        }

        public Response withSubWalletDetails(List<Response.SubWalletDetail> subWalletDetails) {
            this.subWalletDetails = subWalletDetails;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("walletSystemTxnId", walletSystemTxnId).append("subWalletDetails", subWalletDetails).toString();
        }

        public static class SubWalletDetail {

            private int id;
            private int subWalletType;
            private int ppiDetailsId;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public Response.SubWalletDetail withId(int id) {
                this.id = id;
                return this;
            }

            public int getSubWalletType() {
                return subWalletType;
            }

            public void setSubWalletType(int subWalletType) {
                this.subWalletType = subWalletType;
            }

            public Response.SubWalletDetail withSubWalletType(int subWalletType) {
                this.subWalletType = subWalletType;
                return this;
            }

            public int getPpiDetailsId() {
                return ppiDetailsId;
            }

            public void setPpiDetailsId(int ppiDetailsId) {
                this.ppiDetailsId = ppiDetailsId;
            }

            public Response.SubWalletDetail withPpiDetailsId(int ppiDetailsId) {
                this.ppiDetailsId = ppiDetailsId;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("id", id).append("subWalletType", subWalletType).append("ppiDetailsId", ppiDetailsId).toString();
            }

        }

    }

}
