package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class PreAuthNReleaseResponse extends responsePojo {

    private Object type;
    private String requestGuid;
    private String orderId;
    private Object status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public PreAuthNReleaseResponse withType(Object type) {
        this.type = type;
        return this;
    }

    public String getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
    }

    public PreAuthNReleaseResponse withRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PreAuthNReleaseResponse withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public PreAuthNReleaseResponse withStatus(Object status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public PreAuthNReleaseResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public PreAuthNReleaseResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public PreAuthNReleaseResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public PreAuthNReleaseResponse withMetadata(Object metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid).append("orderId", orderId).append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage).append("response", response).append("metadata", metadata).toString();
    }

    public class Response {

        private String preAuthId;
        private String txnStatus;
        private String amount;
        private String userBalance;

        public String getPreAuthId() {
            return preAuthId;
        }

        public void setPreAuthId(String preAuthId) {
            this.preAuthId = preAuthId;
        }

        public Response withPreAuthId(String preAuthId) {
            this.preAuthId = preAuthId;
            return this;
        }

        public String getTxnStatus() {
            return txnStatus;
        }

        public void setTxnStatus(String txnStatus) {
            this.txnStatus = txnStatus;
        }

        public Response withTxnStatus(String txnStatus) {
            this.txnStatus = txnStatus;
            return this;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public Response withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public String getUserBalance() {
            return userBalance;
        }

        public void setUserBalance(String userBalance) {
            this.userBalance = userBalance;
        }

        public Response withUserBalance(String userBalance) {
            this.userBalance = userBalance;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("preAuthId", preAuthId).append("txnStatus", txnStatus).append("amount", amount).append("userBalance", userBalance).toString();
        }

    }

}