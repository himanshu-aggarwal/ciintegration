package com.paytm.apiPojo.wallet.walletWeb;

import java.math.BigDecimal;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.helpers.CommonHelpers;


public class TransferToBankRequest extends requestPojo {

	@JsonProperty("request")
	private Request request;
	@JsonProperty("ipAddress")
	private String ipAddress;
	@JsonProperty("platformName")
	private String platformName;
	@JsonProperty("operationType")
	private String operationType;
	private Boolean updateAmountManager = false;

	public void setUpdateAmountManager(Boolean updateAmountManager) {
		this.updateAmountManager = updateAmountManager;
	}

	public Boolean getUpdateAmountManager() {
		return this.updateAmountManager;
	}

	@JsonProperty("request")
	public Request getRequest() {
		return request;
	}

	@JsonProperty("request")
	public void setRequest(Request request) {
		this.request = request;
	}

	public TransferToBankRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	@JsonProperty("ipAddress")
	public String getIpAddress() {
		return ipAddress;
	}

	@JsonProperty("ipAddress")
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public TransferToBankRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	@JsonProperty("platformName")
	public String getPlatformName() {
		return platformName;
	}

	@JsonProperty("platformName")
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public TransferToBankRequest withPlatformName(String platformName) {
		this.platformName = platformName;
		return this;
	}

	@JsonProperty("operationType")
	public String getOperationType() {
		return operationType;
	}

	@JsonProperty("operationType")
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public TransferToBankRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
				.append("platformName", platformName).append("operationType", operationType).toString();
	}


	public class Request {

		@JsonProperty("payerGuid")
		private String payerGuid;
		@JsonProperty("bankName")
		private String bankName;
		@JsonProperty("nickName")
		private String nickName;
		@JsonProperty("bankAccountNo")
		private String bankAccountNo;
		@JsonProperty("ifscCode")
		private String ifscCode;
		@JsonProperty("currencyCode")
		private String currencyCode;
		@JsonProperty("comment")
		private String comment;
		@JsonProperty("amount")
		private BigDecimal amount;
		@JsonProperty("merchantOrderId")
		private String merchantOrderId = "AutomW" + Thread.currentThread().getId() + CommonHelpers.getInstance().getCurrentTimeInMilliSec();
		@JsonProperty("senderName")
		private String senderName;
		@JsonProperty("accType")
		private String accType;
		@JsonProperty("contactNo")
		private String contactNo;

		
		@JsonProperty("payerGuid")
		public String getPayerGuid() {
			return payerGuid;
		}

		@JsonProperty("payerGuid")
		public void setPayerGuid(String payerGuid) {
			this.payerGuid = payerGuid;
		}

		public Request withPayerGuid(String payerGuid) {
			this.payerGuid = payerGuid;
			return this;
		}

		@JsonProperty("bankName")
		public String getBankName() {
			return bankName;
		}

		@JsonProperty("bankName")
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}

		public Request withBankName(String bankName) {
			this.bankName = bankName;
			return this;
		}

		@JsonProperty("nickName")
		public String getNickName() {
			return nickName;
		}

		@JsonProperty("nickName")
		public void setNickName(String nickName) {
			this.nickName = nickName;
		}

		public Request withNickName(String nickName) {
			this.nickName = nickName;
			return this;
		}

		@JsonProperty("bankAccountNo")
		public String getBankAccountNo() {
			return bankAccountNo;
		}

		@JsonProperty("bankAccountNo")
		public void setBankAccountNo(String bankAccountNo) {
			this.bankAccountNo = bankAccountNo;
		}

		public Request withBankAccountNo(String bankAccountNo) {
			this.bankAccountNo = bankAccountNo;
			return this;
		}

		@JsonProperty("ifscCode")
		public String getIfscCode() {
			return ifscCode;
		}

		@JsonProperty("ifscCode")
		public void setIfscCode(String ifscCode) {
			this.ifscCode = ifscCode;
		}

		public Request withIfscCode(String ifscCode) {
			this.ifscCode = ifscCode;
			return this;
		}

		@JsonProperty("currencyCode")
		public String getCurrencyCode() {
			return currencyCode;
		}

		@JsonProperty("currencyCode")
		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}

		public Request withCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
			return this;
		}

		@JsonProperty("comment")
		public String getComment() {
			return comment;
		}

		@JsonProperty("comment")
		public void setComment(String comment) {
			this.comment = comment;
		}

		public Request withComment(String comment) {
			this.comment = comment;
			return this;
		}

		@JsonProperty("amount")
		public BigDecimal getAmount() {
			return amount;
		}

		@JsonProperty("amount")
		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public Request withAmount(BigDecimal amount) {
			if (TransferToBankRequest.this.getUpdateAmountManager()) {
				FundThreadManager.getInstance().withTotalAmount(amount);
			}
			this.amount = amount;
			return this;
		}

		@JsonProperty("merchantOrderId")
		public String getMerchantOrderId() {
			return merchantOrderId;
		}

		@JsonProperty("merchantOrderId")
		public void setMerchantOrderId(String merchantOrderId) {
			this.merchantOrderId = merchantOrderId;
		}

		public Request withMerchantOrderId(String merchantOrderId) {
			this.merchantOrderId = merchantOrderId;
			return this;
		}

		@JsonProperty("senderName")
		public String getSenderName() {
			return senderName;
		}

		@JsonProperty("senderName")
		public void setSenderName(String senderName) {
			this.senderName = senderName;
		}

		public Request withSenderName(String senderName) {
			this.senderName = senderName;
			return this;
		}

		@JsonProperty("accType")
		public String getAccType() {
			return accType;
		}

		@JsonProperty("accType")
		public void setAccType(String accType) {
			this.accType = accType;
		}

		public Request withAccType(String accType) {
			this.accType = accType;
			return this;
		}

		@JsonProperty("contactNo")
		public String getContactNo() {
			return contactNo;
		}

		@JsonProperty("contactNo")
		public void setContactNo(String contactNo) {
			this.contactNo = contactNo;
		}

		public Request withContactNo(String contactNo) {
			this.contactNo = contactNo;
			return this;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("payerGuid", payerGuid).append("bankName", bankName)
					.append("nickName", nickName).append("bankAccountNo", bankAccountNo).append("ifscCode", ifscCode)
					.append("currencyCode", currencyCode).append("comment", comment).append("amount", amount)
					.append("merchantOrderId", merchantOrderId).append("senderName", senderName)
					.append("accType", accType).append("contactNo", contactNo).toString();
		}

	}

}
