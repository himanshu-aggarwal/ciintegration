package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class MoveBackFundsToIssuerRequest extends requestPojo {

    private Request request;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public MoveBackFundsToIssuerRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).toString();
    }

    public static class Request {

        private String subwalletID;
        private String mid;

        public String getSubwalletID() {
            return subwalletID;
        }

        public void setSubwalletID(String subwalletID) {
            this.subwalletID = subwalletID;
        }

        public Request withSubwalletID(String subwalletID) {
            this.subwalletID = subwalletID;
            return this;
        }

        public String getMid() {
            return mid;
        }

        public void setMid(String mid) {
            this.mid = mid;
        }

        public Request withMid(String mid) {
            this.mid = mid;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("subwalletID", subwalletID).append("mid", mid).toString();
        }

    }

}
