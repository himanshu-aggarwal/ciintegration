package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.apiPojo.wallet.APIInterface.SubWalletInterface;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.helpers.CommonHelpers;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

public class SendMoneyRequest extends requestPojo {
    private Request request;
    private String ipAddress;
    private String platformName;
    private String operationType;
    private String version;
    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager){
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager(){
        return this.updateAmountManager;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public SendMoneyRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public SendMoneyRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public SendMoneyRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public SendMoneyRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public SendMoneyRequest withVersion(String version) {
        this.version = version;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress).append("platformName", platformName).append("operationType", operationType).append("version", version).toString();
    }


    public class Request {

        private int isToVerify;
        private int isLimitApplicable;
        private String payeeEmailId;
        private String payeePhoneNumber;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String merchantOrderId="AutomSM" + Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private BigDecimal amount;
        private String currencyCode;
        private String comment;
        private SubWalletAmount subWalletAmount;

        public int getIsToVerify() {
            return isToVerify;
        }

        public void setIsToVerify(int isToVerify) {
            this.isToVerify = isToVerify;
        }

        public Request withIsToVerify(int isToVerify) {
            this.isToVerify = isToVerify;
            return this;
        }

        public int getIsLimitApplicable() {
            return isLimitApplicable;
        }

        public void setIsLimitApplicable(int isLimitApplicable) {
            this.isLimitApplicable = isLimitApplicable;
        }

        public Request withIsLimitApplicable(int isLimitApplicable) {
            this.isLimitApplicable = isLimitApplicable;
            return this;
        }

        public String getPayeeEmailId() {
            return payeeEmailId;
        }

        public void setPayeeEmailId(String payeeEmailId) {
            this.payeeEmailId = payeeEmailId;
        }

        public Request withPayeeEmailId(String payeeEmailId) {
            this.payeeEmailId = payeeEmailId;
            return this;
        }

        public String getPayeePhoneNumber() {
            return payeePhoneNumber;
        }

        public void setPayeePhoneNumber(String payeePhoneNumber) {
            this.payeePhoneNumber = payeePhoneNumber;
        }

        public Request withPayeePhoneNumber(String payeePhoneNumber) {
            this.payeePhoneNumber = payeePhoneNumber;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Request withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
            if(SendMoneyRequest.this.getUpdateAmountManager()) {
                FundThreadManager.getInstance().withTotalAmount(this.amount);
            }
        }

        public Request withAmount(BigDecimal amount) {
            this.amount = amount;
            if(SendMoneyRequest.this.getUpdateAmountManager()) {
                FundThreadManager.getInstance().withTotalAmount(this.amount);
            }
            return this;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Request withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Request withComment(String comment) {
            this.comment = comment;
            return this;
        }

        public SubWalletAmount getSubWalletAmount() {
            return subWalletAmount;
        }

        public void setSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
        }

        public Request withSubWalletAmount(SubWalletAmount subWalletAmount) {
            this.subWalletAmount = subWalletAmount;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("isToVerify", isToVerify).append("isLimitApplicable", isLimitApplicable).append("payeeEmailId", payeeEmailId).append("payeePhoneNumber", payeePhoneNumber).append("merchantOrderId", merchantOrderId).append("amount", amount).append("currencyCode", currencyCode).append("comment", comment).append("subWalletAmount", subWalletAmount).toString();
        }

        public class SubWalletAmount extends SubWalletInterface {

            @JsonProperty("FOOD")
            private BigDecimal fOOD;
            @JsonProperty("GIFT")
            private BigDecimal gIFT;
            @JsonProperty("TOLL")
            private BigDecimal tOLL;
            @JsonProperty("FUEL")
            private BigDecimal fUEL;
            @JsonProperty("CLOSED_LOOP_WALLET")
            private BigDecimal cLOSED_LOOP_WALLET;
            @JsonProperty("CLOSED_LOOP_SUB_WALLET")
            private BigDecimal cLOSED_LOOP_SUB_WALLET;
            @JsonProperty("INTERNATIONAL_FUNDS_TRANSFER")
            private BigDecimal iNTERNATIONAL_FUNDS_TRANSFER;
            @JsonProperty("GIFT_VOUCHER")
            private BigDecimal gIFT_VOUCHER;
            @JsonProperty("CASHBACK")
            private BigDecimal cASHBACK;

            public BigDecimal getfOOD() {
                return fOOD;
            }

            public void setfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if (SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFoodWalletAmount(this.fOOD);
                }
            }

            public SubWalletAmount withfOOD(BigDecimal fOOD) {
                this.fOOD = fOOD;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFoodWalletAmount(this.fOOD);
                }
                    return this;
            }

            public SubWalletAmount withfOOD(int fOOD) {
                this.fOOD = new BigDecimal(fOOD);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFoodWalletAmount(this.fOOD);
                }
                    return this;
            }

            public BigDecimal getcLOSED_LOOP_SUB_WALLET() {
                return cLOSED_LOOP_SUB_WALLET;
            }

            public void setcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(BigDecimal cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = cLOSED_LOOP_SUB_WALLET;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
                return this;
            }

            public SubWalletAmount withcLOSED_LOOP_SUB_WALLET(int cLOSED_LOOP_SUB_WALLET) {
                this.cLOSED_LOOP_SUB_WALLET = new BigDecimal(cLOSED_LOOP_SUB_WALLET);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopSubWalletAmount(this.cLOSED_LOOP_SUB_WALLET);
                }
                return this;
            }

            public BigDecimal getgIFT() {
                return gIFT;
            }

            public void setgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
            }

            public SubWalletAmount withgIFT(BigDecimal gIFT) {
                this.gIFT = gIFT;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
                return this;
            }

            public SubWalletAmount withgIFT(int gIFT) {
                this.gIFT = new BigDecimal(gIFT);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftWalletAmount(this.gIFT);
                }
                return this;
            }

            public BigDecimal gettOLL() {
                return tOLL;
            }

            public void settOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
            }

            public SubWalletAmount withtOLL(BigDecimal tOLL) {
                this.tOLL = tOLL;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
                return this;
            }

            public SubWalletAmount withtOLL(int tOLL) {
                this.tOLL = new BigDecimal(tOLL);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withTollWalletAmount(this.tOLL);
                }
                return this;
            }

            public BigDecimal getfUEL() {
                return fUEL;
            }

            public void setfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
            }

            public SubWalletAmount withfUEL(BigDecimal fUEL) {
                this.fUEL = fUEL;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
                return this;
            }

            public SubWalletAmount withfUEL(int fUEL) {
                this.fUEL = new BigDecimal(fUEL);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withFuelWalletAmount(this.fUEL);
                }
                return this;
            }

            public BigDecimal getcLOSED_LOOP_WALLET() {
                return cLOSED_LOOP_WALLET;
            }

            public void setcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
            }

            public SubWalletAmount withcLOSED_LOOP_WALLET(BigDecimal cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = cLOSED_LOOP_WALLET;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
                return this;
            }

            public SubWalletAmount withcLOSED_LOOP_WALLET(int cLOSED_LOOP_WALLET) {
                this.cLOSED_LOOP_WALLET = new BigDecimal(cLOSED_LOOP_WALLET);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withClosedLoopWalletAmount(this.cLOSED_LOOP_WALLET);
                }
                return this;
            }

            public BigDecimal getiNTERNATIONAL_FUNDS_TRANSFER() {
                return iNTERNATIONAL_FUNDS_TRANSFER;
            }

            public void setiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
            }

            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(BigDecimal iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = iNTERNATIONAL_FUNDS_TRANSFER;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
                return this;
            }

            public SubWalletAmount withiNTERNATIONAL_FUNDS_TRANSFER(int iNTERNATIONAL_FUNDS_TRANSFER) {
                this.iNTERNATIONAL_FUNDS_TRANSFER = new BigDecimal(iNTERNATIONAL_FUNDS_TRANSFER);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withInternationalWalletAmount(this.iNTERNATIONAL_FUNDS_TRANSFER);
                }
                return this;
            }

            public BigDecimal getgIFT_VOUCHER() {
                return gIFT_VOUCHER;
            }

            public void setgIFT_VOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
            }

            public SubWalletAmount withgIFTVOUCHER(BigDecimal gIFT_VOUCHER) {
                this.gIFT_VOUCHER = gIFT_VOUCHER;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
                return this;
            }

            public SubWalletAmount withgIFTVOUCHER(int gIFT_VOUCHER) {
                this.gIFT_VOUCHER = new BigDecimal(gIFT_VOUCHER);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withGiftVoucherAmount(this.gIFT_VOUCHER);
                }
                return this;
            }

            public BigDecimal getcASHBACK() {
                return cASHBACK;
            }

            public void setcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
            }

            public SubWalletAmount withcASHBACK(BigDecimal cASHBACK) {
                this.cASHBACK = cASHBACK;
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
                return this;
            }

            public SubWalletAmount withcASHBACK(int cASHBACK) {
                this.cASHBACK = new BigDecimal(cASHBACK);
                if(SendMoneyRequest.this.getUpdateAmountManager()) {
                    FundThreadManager.getInstance().withCashBackWalletAmount(this.cASHBACK);
                }
                return this;
            }


            @Override
            public String toString() {
                return new ToStringBuilder(this).append("fOOD", fOOD).append("gIFT", gIFT).append("tOLL", tOLL)
                        .append("fUEL", fUEL).append("cLOSED_LOOP_WALLET", cLOSED_LOOP_WALLET)
                        .append("iNTERNATIONAL_FUNDS_TRANSFER", iNTERNATIONAL_FUNDS_TRANSFER)
                        .append("gIFT_VOUCHER", gIFT_VOUCHER).toString();
            }

        }

    }

}

