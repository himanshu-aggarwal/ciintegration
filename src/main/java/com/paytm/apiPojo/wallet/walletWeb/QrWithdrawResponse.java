package com.paytm.apiPojo.wallet.walletWeb;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.*;
import org.luaj.vm2.ast.Str;

import java.util.List;


public class QrWithdrawResponse extends responsePojo {
    private Object type;
    private Object requestGuid;
    private Object orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    private Object metadata;

    public Object getType() {
        return type;
    }

    public QrWithdrawResponse withType(Object type) {
        this.type = type;
        return this;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getRequestGuid() {
        return requestGuid;
    }

    public QrWithdrawResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public Object getOrderId() {
        return orderId;
    }

    public QrWithdrawResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }


    public Object getStatus() {
        return status;
    }

    public QrWithdrawResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public QrWithdrawResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public QrWithdrawResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Response getResponse() {
        return response;
    }

    public QrWithdrawResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Object getMetadata() {
        return metadata;
    }

    public QrWithdrawResponse withMetadata(Object metadata) {
        this.metadata = metadata;
        return this;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid)
                .append("orderId", orderId).append("status", status).append("statusCode", statusCode)
                .append("statusMessage", statusMessage).append("response", response).append("metadata", metadata)
                .toString();
    }

    public static class Response {
        private String userGuid;
        private Object pgTxnId;
        private long timestamp;
        private String cashBackStatus;
        private String cashBackMessage;
        private String walletSysTransactionId;
        private String walletSystemTxnId;
        private String state;
        private String heading;
        private String comment;
        private String posId;
        private Integer txnAmount;
        private String merchantOrderId;
        private String uniqueReferenceLabel;
        private String uniqueReferenceValue;
        private String pccCode;
        private TxnExtendedInfo txnExtendedInfo;
        private String currentServerTime;

        public String getUserGuid() {
            return userGuid;
        }

        public Response withUserGuid(String userGuid) {
            this.userGuid = userGuid;
            return this;
        }

        public void setUserGuid(String userGuid) {
            this.userGuid = userGuid;
        }

        public Object getPgTxnId() {
            return pgTxnId;
        }

        public Response withPgTxnId(String pgTxnId) {
            this.pgTxnId = pgTxnId;
            return this;
        }

        public void setPgTxnId(Object pgTxnId) {
            this.pgTxnId = pgTxnId;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public Response withTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public String getCashBackStatus() {
            return cashBackStatus;
        }

        public Response withCashBackStatus(String cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
            return this;
        }

        public void setCashBackStatus(String cashBackStatus) {
            this.cashBackStatus = cashBackStatus;
        }

        public String getCashBackMessage() {
            return cashBackMessage;
        }

        public Response withCashBackMessage(String cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
            return this;
        }

        public void setCashBackMessage(String cashBackMessage) {
            this.cashBackMessage = cashBackMessage;
        }
        public String getState() {
            return state;
        }

        public Response withState(String state) {
            this.state = state;
            return this;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getHeading() {
            return heading;
        }

        public Response withHeading(String heading) {
            this.heading=heading;
            return this;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public String getWalletSysTransactionId() {
            return walletSysTransactionId;
        }

        public Response withWalletSysTransactionId(String walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
            return this;
        }

        public void setWalletSysTransactionId(String walletSysTransactionId) {
            this.walletSysTransactionId = walletSysTransactionId;
        }

        public String getWalletSystemTxnId() {
            return walletSystemTxnId;
        }

        public Response withWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
            return this;
        }

        public void setWalletSystemTxnId(String walletSystemTxnId) {
            this.walletSystemTxnId = walletSystemTxnId;
        }



        public String getComment() {
            return comment;
        }

        public Response withComment(String comment) {
            this.comment = comment;
            return this;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getPosId() {
            return posId;
        }

        public Response withPosId(String posId) {
            this.posId = posId;
            return this;
        }

        public void setPosId(String posId) {
            this.posId = posId;
        }

        public Integer getTxnAmount() {
            return txnAmount;
        }

        public Response withTxnAmount(Integer txnAmount) {
            this.txnAmount = txnAmount;
            return this;
        }

        public void setTxnAmount(Integer txnAmount) {
            this.txnAmount = txnAmount;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public Response withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public String getUniqueReferenceLabel() {
            return uniqueReferenceLabel;
        }

        public Response withUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
            return this;
        }

        public void setUniqueReferenceLabel(String uniqueReferenceLabel) {
            this.uniqueReferenceLabel = uniqueReferenceLabel;
        }

        public String getUniqueReferenceValue() {
            return uniqueReferenceValue;
        }

        public Response withUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
            return this;
        }

        public void setUniqueReferenceValue(String uniqueReferenceValue) {
            this.uniqueReferenceValue = uniqueReferenceValue;
        }

        public String getPccCode() {
            return pccCode;
        }

        public Response withPccCode(String pccCode) {
            this.pccCode = pccCode;
            return this;
        }

        public void setPccCode(String pccCode) {
            this.pccCode = pccCode;
        }

        public TxnExtendedInfo getTxnExtendedInfo() {
            return txnExtendedInfo;
        }

        public Response withTxnExtendedInfo(TxnExtendedInfo txnExtendedInfo) {
            this.txnExtendedInfo = txnExtendedInfo;
            return this;
        }

        public void setTxnExtendedInfo(TxnExtendedInfo txnExtendedInfo) {
            this.txnExtendedInfo = txnExtendedInfo;
        }

        public String getCurrentServerTime() {
            return currentServerTime;
        }

        public Response withCurrentServerTime(String currentServerTime) {
            this.currentServerTime = currentServerTime;
            return this;
        }

        public void setCurrentServerTime(String currentServerTime) {
            this.currentServerTime = currentServerTime;

        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("userGuid", userGuid).append("pgTxnId", pgTxnId)
                    .append("timestamp", timestamp).append("cashBackStatus", cashBackStatus).append("cashBackMessage", cashBackMessage)
                    .append("walletSysTransactionId", walletSysTransactionId).append("walletSystemTxnId", walletSystemTxnId).append("state", state).append("heading", heading).append("comment", comment).append("posId", posId).append("txnAmount", txnAmount)
                    .append("merchantOrderId", merchantOrderId).append("uniqueReferenceLabel", uniqueReferenceLabel).append("uniqueReferenceValue", uniqueReferenceValue).append("pccCode", pccCode)
                    .append("txnExtendedInfo", txnExtendedInfo).append("currentServerTime", currentServerTime).toString();


        }


        public static class TxnExtendedInfo {
            public String source;
            public String destination;
            public String validity;
            public List<AmountDetails> amountDetails;
            public String barCodeId;
            public String barCodeImage;
            public String routeId;
            public String routeName;

            public String getSource() {
                return source;
            }

            public TxnExtendedInfo withSource(String source) {
                this.source = source;
                return this;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getDestination() {
                return destination;
            }

            public TxnExtendedInfo withDestination(String destination) {
                this.destination = destination;
                return this;
            }

            public void setDestination(String destination) {
                this.destination = destination;
            }

            public String getValidity() {
                return validity;
            }

            public TxnExtendedInfo withValidity(String validity) {
                this.validity = validity;
                return this;
            }

            public void setValidity(String validity) {
                this.validity = validity;
            }

            public List<AmountDetails> getAmountDetails() {
                return amountDetails;
            }

            public TxnExtendedInfo withAmountDetails(List<AmountDetails> amountDetails) {
                this.amountDetails = amountDetails;
                return this;
            }

            public void setAmountDetails(List<AmountDetails> amountDetails) {
                this.amountDetails = amountDetails;
            }

            public String getBarCodeId() {
                return barCodeId;
            }

            public TxnExtendedInfo withBarCodeId(String barcodeId) {
                this.barCodeId = barcodeId;
                return this;
            }

            public void setBarCodeId(String barCodeId) {
                this.barCodeId = barCodeId;
            }

            public String getBarCodeImage() {
                return barCodeImage;
            }

            public TxnExtendedInfo withBarCodeImage(String barCodeImage) {
                this.barCodeImage = barCodeImage;
                return this;
            }

            public void setBarCodeImage(String barCodeImage) {
                this.barCodeImage = barCodeImage;
            }

            public String getRouteId() {
                return routeId;
            }

            public TxnExtendedInfo withRouteId(String routeId) {
                this.routeId = routeId;
                return this;
            }

            public void setRouteId(String routeId) {
                this.routeId = routeId;
            }

            public String getRouteName() {
                return routeName;
            }

            public TxnExtendedInfo withRouteName(String routeName) {
                this.routeName = routeName;
                return this;
            }

            public void setRouteName(String routeName) {
                this.routeName = routeName;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("source", source).append("destination", destination)
                        .append("validity", validity).append("amountDetails", amountDetails).append("barCodeId",barCodeId).append("barCodeImage",barCodeImage).append("routeId",routeId).append("routeName",routeName).toString();
            }
            public static class AmountDetails{
                public String pax;
                public String quantity;
                public String amount;

                public String getPax() {
                    return pax;
                }
                public AmountDetails withPax(String pax){
                    this.pax=pax;
                    return this;
                }

                public void setPax(String pax) {
                    this.pax = pax;
                }

                public String getQuantity() {
                    return quantity;
                }
                public AmountDetails withQuantity(String quantity){
                    this.quantity=quantity;
                    return this;
                }

                public void setQuantity(String quantity) {
                    this.quantity = quantity;
                }

                public String getAmount() {
                    return amount;
                }
                public AmountDetails withAmount(String amount){
                    this.amount=amount;
                    return this;
                }

                public void setAmount(String amount) {
                    this.amount = amount;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("pax", pax).append("quantity", quantity)
                            .append("amount", amount).toString();
                }
            }
        }
    }
}

