package com.paytm.apiPojo.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class OTPWithdrawRequest extends requestPojo{
	
	  private Request request;
	  private String ipAddress;
	  private String platformName;
	  private String operationType;
	  
	  
	  public Request getRequest() { 
		  return this.request; 
		 
	  }
	  
	  public void setRequest(Request request) {
		  this.request = request;
		 
	  }
	
	  public String getIpAddress() { 
		  return this.ipAddress; 
		  
	  }

	  public void setIpAddress(String ipAddress) {
		  this.ipAddress = ipAddress;
		  
	  }

	  public String getPlatformName() { 
		  return this.platformName;
	  
	  }

	  public void setPlatformName(String platformName) {
		  this.platformName = platformName; 
		  
	  }


	  public String getOperationType() { 
		  return this.operationType; 
		  
	  }

	  public void setOperationType(String operationType) { 
		  this.operationType = operationType; 
		  
	  }
	
	public class Request {

	private String state;
	private String otp;

	public String getState() {
	return state;
	}

	public void setState(String state) {
	this.state = state;
	}
	
	
	public Request withState(String state) {
        this.state = state;
        return this;
    }

	public String getOtp() {
	return otp;
	}

	public void setOtp(String otp) {
	this.otp = otp;
	}
	
	public Request withOtp(String otp) {
        this.otp = otp;
        return this;
    }
	
	}
	
}
