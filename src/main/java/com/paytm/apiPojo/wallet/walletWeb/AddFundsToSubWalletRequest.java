package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.helpers.CommonHelpers;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

public class AddFundsToSubWalletRequest extends requestPojo {

    private Request request;
    private String metadata;
    private String ipAddress;
    private String platformName;
    private String operationType;
    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager){
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager(){
        return this.updateAmountManager;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public AddFundsToSubWalletRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public AddFundsToSubWalletRequest withMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public AddFundsToSubWalletRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public AddFundsToSubWalletRequest withPlatformName(String platformName) {
        this.platformName = platformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public AddFundsToSubWalletRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("metadata", metadata).append("ipAddress", ipAddress).append("platformName", platformName).append("operationType", operationType).toString();
    }


    public class Request {

        private String merchantGuid;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String merchantOrderId = "AutomMOI" +Thread.currentThread().getId()+ CommonHelpers.getInstance().getCurrentTimeInMilliSec();
        private String merchantSubWalletGuid;
        private String payeeEmailId;
        private String payeeSsoId;
        private String payeePhoneNumber;
        private BigDecimal amount;
        private String currencyCode;
        private String userSubWalletType;
        private boolean isReloadable;
        private String comment;

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
            return this;
        }

        public String getMerchantOrderId() {
            return merchantOrderId;
        }

        public void setMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
        }

        public Request withMerchantOrderId(String merchantOrderId) {
            this.merchantOrderId = merchantOrderId;
            return this;
        }

        public String getMerchantSubWalletGuid() {
            return merchantSubWalletGuid;
        }

        public void setMerchantSubWalletGuid(String merchantSubWalletGuid) {
            this.merchantSubWalletGuid = merchantSubWalletGuid;
        }

        public Request withMerchantSubWalletGuid(String merchantSubWalletGuid) {
            this.merchantSubWalletGuid = merchantSubWalletGuid;
            return this;
        }

        public String getPayeeEmailId() {
            return payeeEmailId;
        }

        public void setPayeeEmailId(String payeeEmailId) {
            this.payeeEmailId = payeeEmailId;
        }

        public Request withPayeeEmailId(String payeeEmailId) {
            this.payeeEmailId = payeeEmailId;
            return this;
        }

        public String getPayeeSsoId() {
            return payeeSsoId;
        }

        public void setPayeeSsoId(String payeeSsoId) {
            this.payeeSsoId = payeeSsoId;
        }

        public Request withPayeeSsoId(String payeeSsoId) {
            this.payeeSsoId = payeeSsoId;
            return this;
        }

        public String getPayeePhoneNumber() {
            return payeePhoneNumber;
        }

        public void setPayeePhoneNumber(String payeePhoneNumber) {
            this.payeePhoneNumber = payeePhoneNumber;
        }

        public Request withPayeePhoneNumber(String payeePhoneNumber) {
            this.payeePhoneNumber = payeePhoneNumber;
            return this;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
            if(AddFundsToSubWalletRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
        }

        public Request withAmount(BigDecimal amount) {
            this.amount = amount;
            if(AddFundsToSubWalletRequest.this.getUpdateAmountManager()){
                FundThreadManager.getInstance().withTotalAmount(amount);
            }
            return this;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Request withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public String getUserSubWalletType() {
            return userSubWalletType;
        }

        public void setUserSubWalletType(String userSubWalletType) {
            this.userSubWalletType = userSubWalletType;
        }

        public Request withUserSubWalletType(String userSubWalletType) {
            this.userSubWalletType = userSubWalletType;
            return this;
        }

        public boolean isIsReloadable() {
            return isReloadable;
        }

        public void setIsReloadable(boolean isReloadable) {
            this.isReloadable = isReloadable;
        }

        public Request withIsReloadable(boolean isReloadable) {
            this.isReloadable = isReloadable;
            return this;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Request withComment(String comment) {
            this.comment = comment;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("merchantGuid", merchantGuid).append("merchantOrderId", merchantOrderId).append("merchantSubWalletGuid", merchantSubWalletGuid).append("payeeEmailId", payeeEmailId).append("payeeSsoId", payeeSsoId).append("payeePhoneNumber", payeePhoneNumber).append("amount", amount).append("currencyCode", currencyCode).append("userSubWalletType", userSubWalletType).append("isReloadable", isReloadable).append("comment", comment).toString();
        }

    }
}
