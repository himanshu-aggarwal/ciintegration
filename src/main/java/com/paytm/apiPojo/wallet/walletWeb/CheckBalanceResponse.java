package com.paytm.apiPojo.wallet.walletWeb;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class CheckBalanceResponse extends responsePojo {

	private String statusCode;
	private String statusMessage;
	private Response response;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public static class Response {

		private BigDecimal amount;
		private String currencyCode;
		private Integer status;
		private String message;
		private String ownerGuid;
		private String walletType;
		private String ssoId;
		private Boolean txnPinRequired;
		private Boolean otpPinRequired;
		private String walletGrade;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public String getCurrencyCode() {
			return currencyCode;
		}

		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getOwnerGuid() {
			return ownerGuid;
		}

		public void setOwnerGuid(String ownerGuid) {
			this.ownerGuid = ownerGuid;
		}

		public String getWalletType() {
			return walletType;
		}

		public void setWalletType(String walletType) {
			this.walletType = walletType;
		}

		public String getSsoId() {
			return ssoId;
		}

		public void setSsoId(String ssoId) {
			this.ssoId = ssoId;
		}

		public Boolean getTxnPinRequired() {
			return txnPinRequired;
		}

		public void setTxnPinRequired(Boolean txnPinRequired) {
			this.txnPinRequired = txnPinRequired;
		}

		public Boolean getOtpPinRequired() {
			return otpPinRequired;
		}

		public void setOtpPinRequired(Boolean otpPinRequired) {
			this.otpPinRequired = otpPinRequired;
		}

		public String getWalletGrade() {
			return walletGrade;
		}

		public void setWalletGrade(String walletGrade) {
			this.walletGrade = walletGrade;
		}

		public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
		}

	}

	@Override
	public Object getStatus() {
		// TODO Auto-generated method stub
		return null;
	}

}
