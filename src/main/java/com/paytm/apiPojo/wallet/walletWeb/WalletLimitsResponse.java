package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface;
import org.apache.commons.lang.builder.ToStringBuilder;

public class WalletLimitsResponse extends APIInterface.responsePojo {


    @JsonProperty("type")
    private Object type;
    @JsonProperty("requestGuid")
    private Object requestGuid;
    @JsonProperty("orderId")
    private Object orderId;
    @JsonProperty("status")
    private Object status;
    @JsonProperty("statusCode")
    private String statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("response")
    private Response response;
    @JsonProperty("metadata")
    private Object metadata;

    @JsonProperty("type")
    public Object getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(Object type) {
        this.type = type;
    }

    public WalletLimitsResponse withType(Object type) {
        this.type = type;
        return this;
    }

    @JsonProperty("requestGuid")
    public Object getRequestGuid() {
        return requestGuid;
    }

    @JsonProperty("requestGuid")
    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public WalletLimitsResponse withRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    @JsonProperty("orderId")
    public Object getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public WalletLimitsResponse withOrderId(Object orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("status")
    public Object getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Object status) {
        this.status = status;
    }

    public WalletLimitsResponse withStatus(Object status) {
        this.status = status;
        return this;
    }

    @JsonProperty("statusCode")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public WalletLimitsResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public WalletLimitsResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    @JsonProperty("response")
    public Response getResponse() {
        return response;
    }

    @JsonProperty("response")
    public void setResponse(Response response) {
        this.response = response;
    }

    public WalletLimitsResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    @JsonProperty("metadata")
    public Object getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public WalletLimitsResponse withMetadata(Object metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("requestGuid", requestGuid).append("orderId", orderId).append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage).append("response", response).append("metadata", metadata).toString();
    }

    public class Response {

        @JsonProperty("currentbalance")
        private String currentbalance;
        @JsonProperty("walletRbiType")
        private String walletRbiType;
        @JsonProperty("trustFactor")
        private String trustFactor;
        @JsonProperty("walletOperationType")
        private String walletOperationType;
        @JsonProperty("isLimitApplicable")
        private Boolean isLimitApplicable;
        @JsonProperty("limitMessage")
        private Object limitMessage;
        @JsonProperty("message")
        private Object message;
        @JsonProperty("allowedBalance")
        private Object allowedBalance;
        @JsonProperty("addMoneyDestination")
        private String addMoneyDestination;
        @JsonProperty("label")
        private Object label;
        @JsonProperty("deepLink")
        private Object deepLink;
        @JsonProperty("response")
        private Object response;

        @JsonProperty("currentbalance")
        public String getCurrentbalance() {
            return currentbalance;
        }

        @JsonProperty("currentbalance")
        public void setCurrentbalance(String currentbalance) {
            this.currentbalance = currentbalance;
        }

        public Response withCurrentbalance(String currentbalance) {
            this.currentbalance = currentbalance;
            return this;
        }

        @JsonProperty("walletRbiType")
        public String getWalletRbiType() {
            return walletRbiType;
        }

        @JsonProperty("walletRbiType")
        public void setWalletRbiType(String walletRbiType) {
            this.walletRbiType = walletRbiType;
        }

        public Response withWalletRbiType(String walletRbiType) {
            this.walletRbiType = walletRbiType;
            return this;
        }

        @JsonProperty("trustFactor")
        public String getTrustFactor() {
            return trustFactor;
        }

        @JsonProperty("trustFactor")
        public void setTrustFactor(String trustFactor) {
            this.trustFactor = trustFactor;
        }

        public Response withTrustFactor(String trustFactor) {
            this.trustFactor = trustFactor;
            return this;
        }

        @JsonProperty("walletOperationType")
        public String getWalletOperationType() {
            return walletOperationType;
        }

        @JsonProperty("walletOperationType")
        public void setWalletOperationType(String walletOperationType) {
            this.walletOperationType = walletOperationType;
        }

        public Response withWalletOperationType(String walletOperationType) {
            this.walletOperationType = walletOperationType;
            return this;
        }

        @JsonProperty("isLimitApplicable")
        public Boolean getIsLimitApplicable() {
            return isLimitApplicable;
        }

        @JsonProperty("isLimitApplicable")
        public void setIsLimitApplicable(Boolean isLimitApplicable) {
            this.isLimitApplicable = isLimitApplicable;
        }

        public Response withIsLimitApplicable(Boolean isLimitApplicable) {
            this.isLimitApplicable = isLimitApplicable;
            return this;
        }

        @JsonProperty("limitMessage")
        public Object getLimitMessage() {
            return limitMessage;
        }

        @JsonProperty("limitMessage")
        public void setLimitMessage(Object limitMessage) {
            this.limitMessage = limitMessage;
        }

        public Response withLimitMessage(Object limitMessage) {
            this.limitMessage = limitMessage;
            return this;
        }

        @JsonProperty("message")
        public Object getMessage() {
            return message;
        }

        @JsonProperty("message")
        public void setMessage(Object message) {
            this.message = message;
        }

        public Response withMessage(Object message) {
            this.message = message;
            return this;
        }

        @JsonProperty("allowedBalance")
        public Object getAllowedBalance() {
            return allowedBalance;
        }

        @JsonProperty("allowedBalance")
        public void setAllowedBalance(Object allowedBalance) {
            this.allowedBalance = allowedBalance;
        }

        public Response withAllowedBalance(Object allowedBalance) {
            this.allowedBalance = allowedBalance;
            return this;
        }

        @JsonProperty("addMoneyDestination")
        public String getAddMoneyDestination() {
            return addMoneyDestination;
        }

        @JsonProperty("addMoneyDestination")
        public void setAddMoneyDestination(String addMoneyDestination) {
            this.addMoneyDestination = addMoneyDestination;
        }

        public Response withAddMoneyDestination(String addMoneyDestination) {
            this.addMoneyDestination = addMoneyDestination;
            return this;
        }

        @JsonProperty("label")
        public Object getLabel() {
            return label;
        }

        @JsonProperty("label")
        public void setLabel(Object label) {
            this.label = label;
        }

        public Response withLabel(Object label) {
            this.label = label;
            return this;
        }

        @JsonProperty("deepLink")
        public Object getDeepLink() {
            return deepLink;
        }

        @JsonProperty("deepLink")
        public void setDeepLink(Object deepLink) {
            this.deepLink = deepLink;
        }

        public Response withDeepLink(Object deepLink) {
            this.deepLink = deepLink;
            return this;
        }

        @JsonProperty("response")
        public Object getResponse() {
            return response;
        }

        @JsonProperty("response")
        public void setResponse(Object response) {
            this.response = response;
        }

        public Response withResponse(Object response) {
            this.response = response;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("currentbalance", currentbalance).append("walletRbiType", walletRbiType).append("trustFactor", trustFactor).append("walletOperationType", walletOperationType).append("isLimitApplicable", isLimitApplicable).append("limitMessage", limitMessage).append("message", message).append("allowedBalance", allowedBalance).append("addMoneyDestination", addMoneyDestination).append("label", label).append("deepLink", deepLink).append("response", response).toString();
        }

    }

}