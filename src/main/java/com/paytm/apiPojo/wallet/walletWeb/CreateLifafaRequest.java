package com.paytm.apiPojo.wallet.walletWeb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.helpers.CommonHelpers;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;
import java.util.List;

public class CreateLifafaRequest extends requestPojo {

    @JsonIgnore
    private Boolean updateAmountManager = false;

    public void setUpdateAmountManager(Boolean updateAmountManager){
        this.updateAmountManager = updateAmountManager;
    }

    public Boolean getUpdateAmountManager(){
        return this.updateAmountManager;
    }



    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String orderId="AutomLC" + Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec();
    private String name;
    private String message;
    private Integer proposedReceiverCount;
    private BigDecimal proposedQuantity;
    private String unit;
    private String themeGuid;
    private String creatorType;
    private String creatorId;
    private String distributionType;
    private String strategyType;
    private List<String> recipientList = null;
    private Boolean gamify;
    private String category;
    private String activationTimeStamp;
    private String expiryTimeStamp;
    private String recipientListType;
    private Integer minQuantity;
    private String merchantLogo;

    public String getOrderId() {
        return orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreateLifafaRequest withName(String name) {
        this.name = name;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CreateLifafaRequest withMessage(String message) {
        this.message = message;
        return this;
    }

    public Integer getProposedReceiverCount() {
        return proposedReceiverCount;
    }

    public void setProposedReceiverCount(Integer proposedReceiverCount) {
        this.proposedReceiverCount = proposedReceiverCount;
    }

    public CreateLifafaRequest withProposedReceiverCount(Integer proposedReceiverCount) {
        this.proposedReceiverCount = proposedReceiverCount;
        return this;
    }

    public BigDecimal getProposedQuantity() {
        return proposedQuantity;
    }

    public void setProposedQuantity(BigDecimal proposedQuantity) {
        this.proposedQuantity = proposedQuantity;
        if(CreateLifafaRequest.this.getUpdateAmountManager()){
            FundThreadManager.getInstance().withTotalAmount(proposedQuantity);
        }

    }

    public CreateLifafaRequest withProposedQuantity(BigDecimal proposedQuantity) {
        this.proposedQuantity = proposedQuantity;
        if(CreateLifafaRequest.this.getUpdateAmountManager()){
            FundThreadManager.getInstance().withTotalAmount(proposedQuantity);
        }
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public CreateLifafaRequest withUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public String getThemeGuid() {
        return themeGuid;
    }

    public void setThemeGuid(String themeGuid) {
        this.themeGuid = themeGuid;
    }

    public CreateLifafaRequest withThemeGuid(String themeGuid) {
        this.themeGuid = themeGuid;
        return this;
    }

    public String getCreatorType() {
        return creatorType;
    }

    public void setCreatorType(String creatorType) {
        this.creatorType = creatorType;
    }

    public CreateLifafaRequest withCreatorType(String creatorType) {
        this.creatorType = creatorType;
        return this;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public CreateLifafaRequest withCreatorId(String creatorId) {
        this.creatorId = creatorId;
        return this;
    }

    public String getDistributionType() {
        return distributionType;
    }

    public void setDistributionType(String distributionType) {
        this.distributionType = distributionType;
    }

    public CreateLifafaRequest withDistributionType(String distributionType) {
        this.distributionType = distributionType;
        return this;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    public CreateLifafaRequest withStrategyType(String strategyType) {
        this.strategyType = strategyType;
        return this;
    }

    public List<String> getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(List<String> recipientList) {
        this.recipientList = recipientList;
    }

    public CreateLifafaRequest withRecipientList(List<String> recipientList) {
        this.recipientList = recipientList;
        return this;
    }

    public Boolean getGamify() {
        return gamify;
    }

    public void setGamify(Boolean gamify) {
        this.gamify = gamify;
    }

    public CreateLifafaRequest withGamify(Boolean gamify) {
        this.gamify = gamify;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public CreateLifafaRequest withCategory(String category) {
        this.category = category;
        return this;
    }

    public String getActivationTimeStamp() {
        return activationTimeStamp;
    }

    public void setActivationTimeStamp(String activationTimeStamp) {
        this.activationTimeStamp = activationTimeStamp;
    }

    public CreateLifafaRequest withActivationTimeStamp(String activationTimeStamp) {
        this.activationTimeStamp = activationTimeStamp;
        return this;
    }

    public String getExpiryTimeStamp() {
        return expiryTimeStamp;
    }

    public void setExpiryTimeStamp(String expiryTimeStamp) {
        this.expiryTimeStamp = expiryTimeStamp;
    }

    public CreateLifafaRequest withExpiryTimeStamp(String expiryTimeStamp) {
        this.expiryTimeStamp = expiryTimeStamp;
        return this;
    }

    public String getRecipientListType() {
        return recipientListType;
    }

    public void setRecipientListType(String recipientListType) {
        this.recipientListType = recipientListType;
    }

    public CreateLifafaRequest withRecipientListType(String recipientListType) {
        this.recipientListType = recipientListType;
        return this;
    }

    public Integer getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(Integer minQuantity) {
        this.minQuantity = minQuantity;
    }

    public CreateLifafaRequest withMinQuantity(Integer minQuantity) {
        this.minQuantity = minQuantity;
        return this;
    }

    public String getMerchantLogo() {
        return merchantLogo;
    }

    public void setMerchantLogo(String merchantLogo) {
        this.merchantLogo = merchantLogo;
    }

    public CreateLifafaRequest withMerchantLogo(String merchantLogo) {
        this.merchantLogo = merchantLogo;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orderId", orderId).append("name", name).append("message", message)
                .append("proposedReceiverCount", proposedReceiverCount).append("proposedQuantity", proposedQuantity)
                .append("unit", unit).append("themeGuid", themeGuid).append("creatorType", creatorType)
                .append("creatorId", creatorId).append("distributionType", distributionType)
                .append("strategyType", strategyType).append("recipientList", recipientList)
                .append("gamify", gamify).append("category", category)
                .append("activationTimeStamp", activationTimeStamp).append("expiryTimeStamp", expiryTimeStamp)
                .append("recipientListType", recipientListType).append("minQuantity", minQuantity)
                .append("merchantLogo", merchantLogo).toString();
    }

}

