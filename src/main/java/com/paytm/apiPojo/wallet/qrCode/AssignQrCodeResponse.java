package com.paytm.apiPojo.wallet.qrCode;
import com.paytm.apiPojo.wallet.APIInterface;
import org.apache.commons.lang.builder.ToStringBuilder;

public class AssignQrCodeResponse extends APIInterface.responsePojo {
	public String requestGuid;
	public String orderId;
	public String status;
	public String statusCode;
	public String statusMessage;
	public Response response;
	
	public String getRequestGuid(){
		return requestGuid;
	}
	
	public void setRequestGuid(String requestGuid){
		this.requestGuid=requestGuid;
	}
	
	public AssignQrCodeResponse withRequestGuid(String requestGuid){
		this.requestGuid=requestGuid;
		return this;
	}
	
	public String getOrderId(){
		return orderId;
	}
	
	public void setOrderId(String orderId){
		this.orderId=orderId;
	}
	
	public AssignQrCodeResponse withOrderId(String orderId){
		this.orderId=orderId;
		return this;
	}
	
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	
	public AssignQrCodeResponse withStatus(String status){
		this.status=status;
		return this;
	}
	
	public String getStatusCode(){
		return statusCode;
	}
	
	public void setStatusCode(String statusCode){
		this.statusCode=statusCode;
	}
	public AssignQrCodeResponse withStatusCode(String statusCode){
	 this.statusCode=statusCode;
	 return this;
	}
	public String getStatusMessage(){
		return statusMessage;
	}
	
	public void setStatusMessage(String statusMessage){
		this.statusMessage=statusMessage;
	}
	
	public AssignQrCodeResponse withStatusMessage(String statusMessage){
		this.statusMessage=statusMessage;
		return this;
	}
	
	public Response getResponse(){
		return response;
	}
	public void setResponse(Response response){
		this.response=response;
	}
	public AssignQrCodeResponse withResponse(Response response){
		this.response=response;
		return this;
	}
	
	public String toString(){
		return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId",orderId).append("status", status).
				append("statusCode", statusCode).append("statusMessage",statusMessage).append("response", response).toString();		
	}
	
	public static class Response{
		private String stickerId;
		private String qrCodeId;
		private String mobileNo;
		private String displayName;
		private String path;
		
		public String getStickerId(){
			return stickerId;
		}
		public void setStickerId(String stickerId){
			this.stickerId=stickerId;
		}
		public Response withSticketId(String stickerId){
			this.stickerId=stickerId;
			return this;
		}
		public String getQrCodeId(){
			return qrCodeId;
		}
		public void setQrCodeId(String qrCodeId){
			this.qrCodeId=qrCodeId;
		}
		public Response withQrCodeId(String qrCodeId){
			this.qrCodeId=qrCodeId;
			return this;
		}
		public String getMobileNo(){
			return mobileNo;
		}
		public void setMobileNo(String mobileNo){
			this.mobileNo=mobileNo;
		}
		public Response withMobileNo(String mobileNo){
			this.mobileNo=mobileNo;
			return this;
		}
		public String getDisplayName(){
			return displayName;
	    }
	    public Response withDisplayName(String displayName){
	    	this.displayName=displayName;
	    	return this;
	    }
	    public String getPath(){
	    	return path;
	    }
	    public void setPath(String path){
	    	this.path=path;
	    }
	    public Response withPath(String path){
	    	this.path=path;
	    	return this;
	    }
	    public String toString() {
	        return new ToStringBuilder(this).append("stickerId", stickerId).append("qrCodeId", qrCodeId)
	                .append("mobileNo", mobileNo).append("displayName", displayName).append("path",path).toString();
	    }
	    
	}

}