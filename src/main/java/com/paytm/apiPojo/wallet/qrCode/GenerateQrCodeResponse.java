package com.paytm.apiPojo.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

;

public class GenerateQrCodeResponse extends responsePojo {

	private Object requestGuid;
	private Object orderId;
	private Object status;
	private String statusCode;
	private String statusMessage;
	private List<Response> response = null;

	public Object getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}

	public GenerateQrCodeResponse withRequestGuid(Object requestguid) {
		this.requestGuid = requestguid;
		return this;
	}

	public Object getOrderId() {
		return orderId;
	}

	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}

	public GenerateQrCodeResponse withOrderId(Object orderId) {
		this.orderId = orderId;
		return this;
	}

	public Object getStatus() {
		return status;
	}

	public void setStatus(Object status) {
		this.status = status;
	}

	public GenerateQrCodeResponse withStatus(Object status) {
		this.status = status;
		return this;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public GenerateQrCodeResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public GenerateQrCodeResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public List<Response> getResponse() {
		return response;
	}

	public void setResponse(List<Response> response) {
		this.response = response;
	}

	public GenerateQrCodeResponse withResponse(List<Response> response) {
		this.response = response;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
				.append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
				.append("response", response).toString();
	}

	public static class Response{

		private String stickerId;
		private String qrCodeId;
		private MapResponse mapResponse;

		public String getStickerId() {
			return stickerId;
		}

		public void setStickerId(String stickerId) {
			this.stickerId = stickerId;
		}

		public Response withStickerId(String stickerId) {
			this.stickerId = stickerId;
			return this;
		}

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}

		public Response withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public MapResponse getMapResponse() {
			return mapResponse;
		}

		public void setMapResponse(MapResponse mapResponse) {
			this.mapResponse = mapResponse;
		}

		public Response withMapResponse(MapResponse mapResponse) {
			this.mapResponse = mapResponse;
			return this;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("stickerId", stickerId).append("qrCodeId", qrCodeId)
					.append("mapResponse", mapResponse).toString();
		}

		public static class MapResponse{

			private String mappingId;
			private String mappingType;
			private String typeOfQrCode;
			private String status;
			private String displayName;
			private String category;
			private String subCategory;
			private String tagLine;
			private String secondaryPhoneNumber;
			private String posId;
			private Object metadata;

			public String getMappingId() {
				return mappingId;
			}

			public void setMappingId(String mappingId) {
				this.mappingId = mappingId;
			}

			public MapResponse withMappingId(String mappingId) {
				this.mappingId = mappingId;
				return this;
			}

			public String getMappingType() {
				return mappingType;
			}

			public void setMappingType(String mappingType) {
				this.mappingType = mappingType;
			}

			public MapResponse withMappingType(String mappingType) {
				this.mappingType = mappingType;
				return this;
			}

			public String getTypeOfQrCode() {
				return typeOfQrCode;
			}

			public void setTypeOfQrCode(String typeOfQrCode) {
				this.typeOfQrCode = typeOfQrCode;
			}

			public MapResponse withTypeOfQrCode(String typeOfQrCode) {
				this.typeOfQrCode = typeOfQrCode;
				return this;
			}

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}

			public MapResponse withStatus(String status) {
				this.status = status;
				return this;
			}

			public String getDisplayName() {
				return displayName;
			}

			public void setDisplayName(String displayName) {
				this.displayName = displayName;
			}

			public MapResponse withDisplayName(String displayName) {
				this.displayName = displayName;
				return this;
			}

			public String getCategory() {
				return category;
			}

			public void setCategory(String category) {
				this.category = category;
			}

			public MapResponse withCategory(String category) {
				this.category = category;
				return this;
			}

			public String getSubCategory() {
				return subCategory;
			}

			public void setSubCategory(String subCategory) {
				this.subCategory = subCategory;
			}

			public MapResponse withSubCategory(String subCategory) {
				this.subCategory = subCategory;
				return this;
			}

			public String getTagLine() {
				return tagLine;
			}

			public void setTagLine(String tagLine) {
				this.tagLine = tagLine;
			}

			public MapResponse withTagLine(String tagLine) {
				this.tagLine = tagLine;
				return this;
			}

			public String getSecondaryPhoneNumber() {
				return secondaryPhoneNumber;
			}

			public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
				this.secondaryPhoneNumber = secondaryPhoneNumber;
			}

			public MapResponse withSecondaryPhoneNumber(String secondaryPhoneNumber) {
				this.secondaryPhoneNumber = secondaryPhoneNumber;
				return this;
			}

			public String getPosId() {
				return posId;
			}

			public void setPosId(String posId) {
				this.posId = posId;
			}

			public MapResponse withPosId(String posId) {
				this.posId = posId;
				return this;
			}

			public Object getMetadata() {
				return metadata;
			}

			public void setMetadata(Object metadata) {
				this.metadata = metadata;
			}

			public MapResponse withMetadata(Object metadata) {
				this.metadata = metadata;
				return this;
			}

			@Override
			public String toString() {
				return new ToStringBuilder(this).append("mappingId", mappingId).append("mappingType", mappingType)
						.append("typeOfQrCode", typeOfQrCode).append("status", status)
						.append("displayName", displayName).append("category", category)
						.append("subCategory", subCategory).append("tagLine", tagLine)
						.append("secondaryPhoneNumber", secondaryPhoneNumber).append("posId", posId)
						.append("metadata", metadata).toString();
			}
		}
	}
}
