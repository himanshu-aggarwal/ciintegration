package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;;

public class FetchQrCodeDetailsRequest extends requestPojo {

    private FetchQrCodeDetailsRequest.Request request;
    private String ipAddress;
    private String platformName;
    private String operationType;

    public FetchQrCodeDetailsRequest.Request getRequest() {
        return request;
    }

    public void setRequest(FetchQrCodeDetailsRequest.Request request) {
        this.request = request;
    }

    public FetchQrCodeDetailsRequest withRequest(FetchQrCodeDetailsRequest.Request request) {
        this.request = request;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public FetchQrCodeDetailsRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public FetchQrCodeDetailsRequest withPlateformName(String plateformName) {
        this.platformName = plateformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public FetchQrCodeDetailsRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("platformName", platformName).append("operationType", operationType).toString();
    }

    public static class Request {

        private String qrCodeId;
        private String stickerId;
        private String mappingId;
        private String posId;
        private String mappingType;

        public String getQrCodeId() {
            return qrCodeId;
        }

        public void setQrCodeId(String qrCodeId) {
            this.qrCodeId = qrCodeId;
        }

        public FetchQrCodeDetailsRequest.Request withQrCodeId(String qrCodeId) {
            this.qrCodeId = qrCodeId;
            return this;
        }

        public String getStickerId() {
            return stickerId;
        }

        public void setStickerId(String stickerId) {
            this.stickerId = stickerId;
        }

        public FetchQrCodeDetailsRequest.Request withStickrtId(String stickerId) {
            this.stickerId = stickerId;
            return this;
        }

        public String getMappingId() {
            return mappingId;
        }

        public void setMappingId(String mappingId) {
            this.mappingId = mappingId;
        }

        public FetchQrCodeDetailsRequest.Request withMappingId(String mappingId) {
            this.mappingId = mappingId;
            return this;
        }

        public String getPosId() {
            return posId;
        }

        public void setPosId(String posId) {
            this.posId = posId;
        }

        public FetchQrCodeDetailsRequest.Request withPosId(String posId) {
            this.posId = posId;
            return this;
        }

        public String getMappingType() {
            return mappingType;
        }

        public void setMappingType(String mappingType) {
            this.mappingType = mappingType;
        }

        public FetchQrCodeDetailsRequest.Request withMappingType(String mappingType) {
            this.mappingType = mappingType;
            return this;
        }

        public String toString() {
            return new ToStringBuilder(this).append("qrCodeId", qrCodeId).append("stickerId", stickerId)
                    .append("mappingId", mappingId).append("posId", posId).append("mappingType", mappingType)
                    .toString();
        }
    }

}
