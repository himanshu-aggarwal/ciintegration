package com.paytm.apiPojo.wallet.qrCode;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface.responsePojo;;

public class GetQrCodeInfoResponse extends responsePojo {

	private String status;
	private String statusCode;
	private String statusMessage;
	private Response response;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public GetQrCodeInfoResponse withStatus(String status) {
		this.status = status;
		return this;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public GetQrCodeInfoResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public GetQrCodeInfoResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public GetQrCodeInfoResponse withResponse(Response response) {
		this.response = response;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("status", status).append("statusCode", statusCode)
				.append("statusMessage", statusMessage).append("response", response).toString();
	}

	public static class Response {

		private String payeeType;
		private String payeeId;
		private String currencyCode;
		private String tagLine;
		private String category;
		private String subCategory;
		private String payeeSecondaryNumber;
		private String service;
		private ExtendedInfo extendedInfo;
		private String mode;
		private Boolean offlinePostConvenience;
		private String mappingId;
		private Boolean pgEnabled;
		private String qrCodeId;
		private Uts uts;
		@JsonProperty("REQUEST_TYPE")
		private String rEQUESTTYPE;
		@JsonProperty("EXPIRY_DATE")
		private String eXPIRYDATE;
		@JsonProperty("NAME")
		private String nAME;
		@JsonProperty("MOBILE_NO")
		private String mOBILENO;
		@JsonProperty("TXN_AMOUNT")
		private String tXNAMOUNT;
		@JsonProperty("ORDER_ID")
		private String oRDERID;
		@JsonProperty("MERCHANT_STATUS")
		private String mERCHANTSTATUS;

		public String getPayeeType() {
			return payeeType;
		}

		public void setPayeeType(String payeeType) {
			this.payeeType = payeeType;
		}

		public Response withPayeeType(String payeeType) {
			this.payeeType = payeeType;
			return this;
		}

		public String getPayeeId() {
			return payeeId;
		}

		public void setPayeeId(String payeeId) {
			this.payeeId = payeeId;
		}

		public Response withPayeeId(String payeeId) {
			this.payeeId = payeeId;
			return this;
		}

		public String getCurrencyCode() {
			return currencyCode;
		}

		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}

		public Response withCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
			return this;
		}

		public String getTagLine() {
			return tagLine;
		}

		public void setTagLine(String tagLine) {
			this.tagLine = tagLine;
		}

		public Response withTagLine(String tagLine) {
			this.tagLine = tagLine;
			return this;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public Response withCategory(String category) {
			this.category = category;
			return this;
		}

		public String getSubCategory() {
			return subCategory;
		}

		public void setSubCategory(String subCategory) {
			this.subCategory = subCategory;
		}

		public Response withSubCategory(String subCategory) {
			this.subCategory = subCategory;
			return this;
		}

		public String getPayeeSecondaryNumber() {
			return payeeSecondaryNumber;
		}

		public void setPayeeSecondaryNumber(String payeeSecondaryNumber) {
			this.payeeSecondaryNumber = payeeSecondaryNumber;
		}

		public Response withPayeeSecondaryNumber(String payeeSecondaryNumber) {
			this.payeeSecondaryNumber = payeeSecondaryNumber;
			return this;
		}

		public String getService() {
			return service;
		}

		public void setService(String service) {
			this.service = service;
		}

		public Response withService(String service) {
			this.service = service;
			return this;
		}

		public ExtendedInfo getExtendedInfo() {
			return extendedInfo;
		}

		public void setExtendedInfo(ExtendedInfo extendedInfo) {
			this.extendedInfo = extendedInfo;
		}

		public Response withExtendedInfo(ExtendedInfo extendedInfo) {
			this.extendedInfo = extendedInfo;
			return this;
		}

		public String getMode() {
			return mode;
		}

		public void setMode(String mode) {
			this.mode = mode;
		}

		public Response withMode(String mode) {
			this.mode = mode;
			return this;
		}

		public Boolean getOfflinePostConvenience() {
			return offlinePostConvenience;
		}

		public void setOfflinePostConvenience(Boolean offlinePostConvenience) {
			this.offlinePostConvenience = offlinePostConvenience;
		}

		public Response withOfflinePostConvenience(Boolean offlinePostConvenience) {
			this.offlinePostConvenience = offlinePostConvenience;
			return this;
		}

		public String getMappingId() {
			return mappingId;
		}

		public void setMappingId(String mappingId) {
			this.mappingId = mappingId;
		}

		public Response withMappingId(String mappingId) {
			this.mappingId = mappingId;
			return this;
		}

		public Boolean getPgEnabled() {
			return pgEnabled;
		}

		public void setPgEnabled(Boolean pgEnabled) {
			this.pgEnabled = pgEnabled;
		}

		public Response withPgEnabled(Boolean pgEnabled) {
			this.pgEnabled = pgEnabled;
			return this;
		}

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}

		public Response withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}
		
		public Uts getUts() {
			return uts;
		}

		public void setUts(Uts uts) {
			this.uts = uts;
		}

		public Response withUts(Uts uts) {
			this.uts = uts;
			return this;
		}

		public String getREQUESTTYPE() {
			return rEQUESTTYPE;
		}

		public void setREQUESTTYPE(String rEQUESTTYPE) {
			this.rEQUESTTYPE = rEQUESTTYPE;
		}

		public Response withREQUESTTYPE(String rEQUESTTYPE) {
			this.rEQUESTTYPE = rEQUESTTYPE;
			return this;
		}

		public String getEXPIRYDATE() {
			return eXPIRYDATE;
		}

		public void setEXPIRYDATE(String eXPIRYDATE) {
			this.eXPIRYDATE = eXPIRYDATE;
		}

		public Response withEXPIRYDATE(String eXPIRYDATE) {
			this.eXPIRYDATE = eXPIRYDATE;
			return this;
		}

		public String getNAME() {
			return nAME;
		}

		public void setNAME(String nAME) {
			this.nAME = nAME;
		}

		public Response withNAME(String nAME) {
			this.nAME = nAME;
			return this;
		}

		public String getMOBILENO() {
			return mOBILENO;
		}

		public void setMOBILENO(String mOBILENO) {
			this.mOBILENO = mOBILENO;
		}

		public Response withMOBILENO(String mOBILENO) {
			this.mOBILENO = mOBILENO;
			return this;
		}

		public String getTXNAMOUNT() {
			return tXNAMOUNT;
		}

		public void setTXNAMOUNT(String tXNAMOUNT) {
			this.tXNAMOUNT = tXNAMOUNT;
		}

		public Response withTXNAMOUNT(String tXNAMOUNT) {
			this.tXNAMOUNT = tXNAMOUNT;
			return this;
		}

		public String getORDERID() {
			return oRDERID;
		}

		public void setORDERID(String oRDERID) {
			this.oRDERID = oRDERID;
		}

		public Response withORDERID(String oRDERID) {
			this.oRDERID = oRDERID;
			return this;
		}

		public String getMERCHANTSTATUS() {
			return mERCHANTSTATUS;
		}

		public void setMERCHANTSTATUS(String mERCHANTSTATUS) {
			this.mERCHANTSTATUS = mERCHANTSTATUS;
		}

		public Response withMERCHANTSTATUS(String mERCHANTSTATUS) {
			this.mERCHANTSTATUS = mERCHANTSTATUS;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("payeeType", payeeType).append("payeeId", payeeId)
					.append("currencyCode", currencyCode).append("service", service)
					.append("extendedInfo", extendedInfo).append("mode", mode)
					.append("offlinePostConvenience", offlinePostConvenience).append("mappingId", mappingId)
					.append("pgEnabled", pgEnabled).append("qrCodeId", qrCodeId).append("rEQUESTTYPE", rEQUESTTYPE)
					.append("eXPIRYDATE", eXPIRYDATE).append("nAME", nAME).append("mOBILENO", mOBILENO)
					.append("tXNAMOUNT", tXNAMOUNT).append("oRDERID", oRDERID).append("mERCHANTSTATUS", mERCHANTSTATUS)
					.append("uts", uts).toString();
		}

		public static class ExtendedInfo {
			private String logoURL;

			public String getLogoURL() {
				return logoURL;
			}

			public void setLogoURL(String logoURL) {
				this.logoURL = logoURL;
			}

			public ExtendedInfo withLogoUrl(String logoURL) {
				this.logoURL = logoURL;
				return this;
			}
		}

		public static class Uts {

			private String merchantGuid;
			private String industryType;
			private String merchantLogo;
			private String merchantName;
			private String merchantStatus;
			private String sourceName;
			private Integer sourceId;
			private List<Object> routes = null;
			private List<String> pax = null;

			public String getMerchantGuid() {
				return merchantGuid;
			}

			public void setMerchantGuid(String merchantGuid) {
				this.merchantGuid = merchantGuid;
			}

			public String getIndustryType() {
				return industryType;
			}

			public void setIndustryType(String industryType) {
				this.industryType = industryType;
			}

			public String getMerchantLogo() {
				return merchantLogo;
			}

			public void setMerchantLogo(String merchantLogo) {
				this.merchantLogo = merchantLogo;
			}

			public String getMerchantName() {
				return merchantName;
			}

			public void setMerchantName(String merchantName) {
				this.merchantName = merchantName;
			}

			public String getMerchantStatus() {
				return merchantStatus;
			}

			public void setMerchantStatus(String merchantStatus) {
				this.merchantStatus = merchantStatus;
			}

			public String getSourceName() {
				return sourceName;
			}

			public void setSourceName(String sourceName) {
				this.sourceName = sourceName;
			}

			public Integer getSourceId() {
				return sourceId;
			}

			public void setSourceId(Integer sourceId) {
				this.sourceId = sourceId;
			}

			public List<Object> getRoutes() {
				return routes;
			}

			public void setRoutes(List<Object> routes) {
				this.routes = routes;
			}

			public List<String> getPax() {
				return pax;
			}

			public void setPax(List<String> pax) {
				this.pax = pax;
			}

			public String toString() {
				return new ToStringBuilder(this).append("merchantGuid", merchantGuid)
						.append("industryType", industryType).append("merchantLogo", merchantLogo)
						.append("merchantName", merchantName).append("merchantStatus", merchantStatus)
						.append("sourceName", sourceName).append("sourceId", sourceId).append("routes", routes)
						.append("pax", pax).toString();
			}
		}
	}
}
