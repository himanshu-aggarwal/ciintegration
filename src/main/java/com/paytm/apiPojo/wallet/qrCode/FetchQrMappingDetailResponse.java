package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class FetchQrMappingDetailResponse extends responsePojo {

	private Object requestGuid;
	private Object orderId;
	private Object status;
	private String statusCode;
	private String statusMessage;
	private Response response;

	public Object getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}

	public FetchQrMappingDetailResponse withRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
		return this;
	}

	public Object getOrderId() {
		return orderId;
	}

	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}

	public FetchQrMappingDetailResponse withOrderId(Object orderId) {
		this.orderId = orderId;
		return this;
	}

	public Object getStatus() {
		return status;
	}

	public void setStatus(Object status) {
		this.status = status;
	}

	public FetchQrMappingDetailResponse withStatus(Object status) {
		this.status = status;
		return this;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public FetchQrMappingDetailResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public FetchQrMappingDetailResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public FetchQrMappingDetailResponse withResponse(Response response) {
		this.response = response;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
				.append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
				.append("response", response).toString();
	}

	public static class Response {

		private String stickerId;
		private String qrCodeId;
		private String batchId;
		private String mappingStatus;
		private String mappedBy;

		public String getStickerId() {
			return stickerId;
		}

		public void setStickerId(String stickerId) {
			this.stickerId = stickerId;
		}

		public Response withStickerId(String stickerId) {
			this.stickerId = stickerId;
			return this;
		}

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}

		public Response withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public String getBatchId() {
			return batchId;
		}

		public void setBatchId(String batchId) {
			this.batchId = batchId;
		}

		public Response withBatchId(String batchId) {
			this.batchId = batchId;
			return this;
		}

		public String getMappingStatus() {
			return mappingStatus;
		}

		public void setMappingStatus(String mappingStatus) {
			this.mappingStatus = mappingStatus;
		}

		public Response withMappingStatus(String mappingStatus) {
			this.mappingStatus = mappingStatus;
			return this;
		}

		public String getMappedBy() {
			return mappedBy;
		}

		public void setMappedBy(String mappedBy) {
			this.mappedBy = mappedBy;
		}

		public Response withMappedBy(String mappedBy) {
			this.mappedBy = mappedBy;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("stickerId", stickerId).append("qrCodeId", qrCodeId)
					.append("batchId", batchId).append("mappingStatus", mappingStatus).append("mappedBy", mappedBy)
					.toString();
		}
	}
}


