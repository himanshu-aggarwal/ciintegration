package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;
import com.paytm.helpers.CommonHelpers;

public class MapDeeplinkToQrRequest extends requestPojo {

	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public MapDeeplinkToQrRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public MapDeeplinkToQrRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public MapDeeplinkToQrRequest withPlateformName(String plateformName) {
		this.platformName = plateformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public MapDeeplinkToQrRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
				.append("plateformName", platformName).append("operationType", operationType).toString();
	}

	public static class Request {
		
		@JsonProperty(access = JsonProperty.Access.READ_ONLY)
		private String deepLink = "www.paytm" + Thread.currentThread().getId()+CommonHelpers.getInstance().getCurrentTimeInMilliSec() + ".com";
		private String merchantMid;
		private String stickerId;
		private String displayName;
		private String status;
		private String posId;
		private String phoneNo;
		private String tagLine;
		
		public String getDeepLink() {
			return deepLink;
		}

		public void setDeepLink(String deepLink) {
			this.deepLink = deepLink;
		}

		public Request withDeepLink(String deepLink) {
			this.deepLink = deepLink;
			return this;
		}

		public String getMerchantMid() {
			return merchantMid;
		}

		public void setMerchantMid(String merchantMid) {
			this.merchantMid = merchantMid;
		}

		public Request withMerchantMid(String merchantMid) {
			this.merchantMid = merchantMid;
			return this;
		}

		public String getStickerId() {
			return this.stickerId;
		}

		public void setStickerId(String stickerId) {
			this.stickerId = stickerId;
		}

		public Request withStickerId(String stickerId) {
			this.stickerId = stickerId;
			return this;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public Request withDisplayName(String displayName) {
			this.displayName = displayName;
			return this;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Request withStatus(String status) {
			this.status = status;
			return this;
		}
		
		public String getPosId() {
			return posId;
		}

		public void setPosId(String posId) {
			this.posId = posId;
		}
		
		public Request withPosId(String posId) {
			this.posId = posId;
			return this;
		}
		
		public String getPhoneNo() {
			return phoneNo;
		}

		public void setPhoneNo(String phoneNo) {
			this.phoneNo = phoneNo;
		}

		public Request withPhoneNo(String phoneNo) {
			this.phoneNo = phoneNo;
			return this;
		}
		
		public String getTagLine() {
			return tagLine;
		}

		public void setTagLine(String tagLine) {
			this.tagLine = tagLine;
		}
		
		public Request withTagLine(String tagLine) {
			this.tagLine = tagLine;
			return this;
		}
		
		@Override
		public String toString() {
			return new ToStringBuilder(this).append("deepLink", deepLink).append("merchantMid", merchantMid)
					.append("stickerId", stickerId).append("displayName", displayName).append("status", status)
					.append("posId", posId).append("phoneNo", phoneNo).append("tagLine", tagLine).toString();
		}
	}
}
