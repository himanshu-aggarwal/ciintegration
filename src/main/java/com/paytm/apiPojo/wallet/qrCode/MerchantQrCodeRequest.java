package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class MerchantQrCodeRequest extends requestPojo {
	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}
	
	public MerchantQrCodeRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public MerchantQrCodeRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	
	public MerchantQrCodeRequest withPlatformName(String plateformName) {
		this.platformName = plateformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	
	public MerchantQrCodeRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
				.append("plateformName", platformName).append("operationType", operationType).toString();
	}

	public static class Request {

		private String requestType;

		public String getRequestType() {
			return requestType;
		}

		public void setRequestType(String requestType) {
			this.requestType = requestType;
		}
		
		public Request withRequestType(String requestType) {
			this.requestType = requestType;
			return this;
		}
		
		@Override
		public String toString() {
			return new ToStringBuilder(this).append("requestType", requestType).toString();
		}

	}
}
