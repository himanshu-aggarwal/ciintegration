package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface;

public class AssignQrCodeRequest extends APIInterface.requestPojo {
	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;
	
	public Request getRequest(){
		return request;
	}
	
	public void setRequest(Request request){
		 this.request=request;
    }
	
	public AssignQrCodeRequest withRequest(Request request){
		this.request=request;
		return this;
	}
	public String getIpAddress(){
		return ipAddress;
	}
	
	public void setIpAdress(String ipAddress){
		this.ipAddress=ipAddress;
	}
	
	public AssignQrCodeRequest withIpAddress(String ipAddress){
		this.ipAddress=ipAddress;
		return this;
	}
	
	public String getPlatformName(){
	     return platformName;
	}
	
	public void setPlatformName(String platformName){
		this.platformName=platformName;
	}
	
	public AssignQrCodeRequest withPlatformName(String platformName){
		this.platformName=platformName;
		return this;
	}
	
	public String getOperationType(){
		return operationType;
	}
	
	public void setOperationType(String operationType){
		this.operationType=operationType;
	}
	
	public AssignQrCodeRequest withOperationType(String operationType){
		this.operationType=operationType;
		return this; 
	}
	
	public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("plateformName", platformName).append("operationType", operationType).toString();
    }
	
	public static class Request {
		private String businessType;
		private String phoneNo;
		private String displayName;
		
		public String getBusinessType(){
			System.out.println(this.businessType);
			return this.businessType;
		}
		
		public void setBusinessType(String businessType){
			this.businessType=businessType;
		}
		
		public Request withBusinessType(String businessType){
			this.businessType=businessType;
			return this;
		}
		
		public String getPhoneNo(){
			return phoneNo;
		}
		
		public void setPhoneNo(String phoneNo){
			this.phoneNo=phoneNo;
		}
		
		public Request withPhoneNo(String phoneNo){
			this.phoneNo=phoneNo;
			return this;
		}
		
		public String setDisplayName(){
			return displayName;
		}
		
		public void getDisplayName(String displayName){
		   this.displayName=displayName;
		}
		
		public Request withDisplayName(String displayName){
			this.displayName=displayName;
			return this;
		}
		
		public String toString(){
			return new ToStringBuilder(this).append("businessType", businessType).append("phoneNo", phoneNo).append("displayName",displayName).toString();
		}
		
	}
}
