package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;


public class CreateQrCodeResponse extends responsePojo {

	private Object requestGuid;
	private Object orderId;
	private String status;
	private String statusCode;
	private String statusMessage;
	private Response response;

	public Object getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}

	public CreateQrCodeResponse withRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
		return this;
	}

	public Object getOrderId() {
		return orderId;
	}

	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}

	public CreateQrCodeResponse withOrderId(Object orderId) {
		this.orderId = orderId;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CreateQrCodeResponse withStatus(String status) {
		this.status = status;
		return this;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public CreateQrCodeResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public CreateQrCodeResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public CreateQrCodeResponse withResponse(Response response) {
		this.response = response;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
				.append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
				.append("response", response).toString();
	}

	public static class Response {

		private String path;
		private String encryptedData;
		private String qrData;

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public Response withPath(String path) {
			this.path = path;
			return this;
		}

		public String getEncryptedData() {
			return encryptedData;
		}

		public void setEncryptedData(String encryptedData) {
			this.encryptedData = encryptedData;
		}

		public Response withEncryptedData(String encryptedData) {
			this.encryptedData = encryptedData;
			return this;
		}

		public String getQrData() {
			return qrData;
		}

		public void setQrData(String qrData) {
			this.qrData = qrData;
		}

		public Response withQrData(String qrData) {
			this.qrData = qrData;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("path", path).append("encryptedData", encryptedData)
					.append("qrData", qrData).toString();
		}
	}
}
