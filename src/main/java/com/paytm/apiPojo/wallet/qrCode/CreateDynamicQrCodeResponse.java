package com.paytm.apiPojo.wallet.qrCode;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class CreateDynamicQrCodeResponse extends responsePojo {


    private String requestGuid;
    private String orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;

    public String getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
    }

    public CreateDynamicQrCodeResponse withRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CreateDynamicQrCodeResponse withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CreateDynamicQrCodeResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public CreateDynamicQrCodeResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public CreateDynamicQrCodeResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public CreateDynamicQrCodeResponse withResponse(Response response) {
        this.response = response;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
                .append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
                .append("response", response).toString();
    }

    public static class Response {

        private List<DynamicQrCodes> dynamicQrCodes = null;

        public List<DynamicQrCodes> getDynamicQrCodes() { 
            return dynamicQrCodes;
        }

        public void setDynamicQrCodes(List<DynamicQrCodes> dynamicQrCodes) {
            this.dynamicQrCodes = dynamicQrCodes;
        }

        public Response withDynamicQrCode(List<DynamicQrCodes> dynamicQrCodes) {
            this.dynamicQrCodes = dynamicQrCodes;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("dynamicQrCodes", dynamicQrCodes).toString();
        }

        public static class DynamicQrCodes {

            private String strickerId;
            private String qrCodeId;

            public String getStrickerId() {
                return strickerId;
            }

            public void setStrickerId(String strickerId) {
                this.strickerId = strickerId;
            }

            public DynamicQrCodes withStrickerId(String strickerId) {
                this.strickerId = strickerId;
                return this;
            }

            public String getQrCodeId() {
                return qrCodeId;
            }

            public void setQrCodeId(String qrCodeId) {
                this.qrCodeId = qrCodeId;
            }

            public DynamicQrCodes withQrCodeId(String qrCodeId) {
                this.qrCodeId = qrCodeId;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("strickerId", strickerId).append("qrCodeId", qrCodeId).toString();
            }

        }

    }
}
