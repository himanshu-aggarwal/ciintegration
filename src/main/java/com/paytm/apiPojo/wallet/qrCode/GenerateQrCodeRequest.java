package com.paytm.apiPojo.wallet.qrCode;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;;

public class GenerateQrCodeRequest extends requestPojo {

	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}
	
	public GenerateQrCodeRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public GenerateQrCodeRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	
	public GenerateQrCodeRequest withplateformName(String platformName) {
		this.platformName = platformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	
	public GenerateQrCodeRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}
	
	@Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("platformName", platformName).append("operationType", operationType).toString();
    }

	public static class Request {

		private CreateRequest createRequest;
		private MapRequest mapRequest;
		private List<String> operationType = null;

		public CreateRequest getCreateRequest() {
			return createRequest;
		}

		public void setCreateRequest(CreateRequest createRequest) {
			this.createRequest = createRequest;
		}
		
		public Request withCreateRequest(CreateRequest createRequest) {
			this.createRequest = createRequest;
			return this;
		}

		public MapRequest getMapRequest() {
			return mapRequest;
		}

		public void setMapRequest(MapRequest mapRequest) {
			this.mapRequest = mapRequest;
		}
		
		public Request withMapRequest(MapRequest mapRequest) {
			this.mapRequest = mapRequest;
			return this;
		}

		public List<String> getOperationType() {
			return operationType;
		}

		public void setOperationType(List<String> operationType) {
			this.operationType = operationType;
		}
		
		public Request withOperationType(List<String> operationType) {
			this.operationType = operationType;
			return this;
		}
		
		@Override
	    public String toString() {
	        return new ToStringBuilder(this).append("createRequest", createRequest).append("mapRequest", mapRequest)
	                .append("operationType", operationType).toString();
	    }

		public static class CreateRequest {

			private String agentPhoneNo;
			private String agentEmailId;
			private String businessType;
			private Object batchSize;
			private Object batchCount;

			public String getAgentPhoneNo() {
				return agentPhoneNo;
			}

			public void setAgentPhoneNo(String agentPhoneNo) {
				this.agentPhoneNo = agentPhoneNo;
			}
			
			public CreateRequest withAgentPhoneNo(String agentPhoneNo) {
				this.agentPhoneNo = agentPhoneNo;
				return this;
			}

			public String getAgentEmailId() {
				return agentEmailId;
			}

			public void setAgentEmailId(String agentEmailId) {
				this.agentEmailId = agentEmailId;
			}
			
			public CreateRequest withAgentEmailId(String agentEmailId) {
				this.agentEmailId = agentEmailId;
				return this;
			}

			public String getBusinessType() {
				return businessType;
			}

			public void setBusinessType(String businessType) {
				this.businessType = businessType;
			}
			
			public CreateRequest withBusinessType(String businessType) {
				this.businessType = businessType;
				return this;
			}

			public Object getBatchSize() {
				return batchSize;
			}

			public void setBatchSize(Object batchSize) {
				this.batchSize = batchSize;
			}
			
			public CreateRequest withBatchSize(Object batchSize) {
				this.batchSize = batchSize;
				return this;
			}

			public Object getBatchCount() {
				return batchCount;
			}

			public void setBatchCount(Object batchCount) {
				this.batchCount = batchCount;
			}
			
			public CreateRequest withBatchCount(Object batchCount) {
				this.batchCount = batchCount;
				return this;
			}
			
			@Override
		    public String toString() {
		        return new ToStringBuilder(this).append("agentPhoneNo", agentPhoneNo).append("agentEmailId", agentEmailId)
		                .append("businessType", businessType).append("batchSize", batchSize).append("batchCount", batchCount).toString();
		    }

		}

		public static class MapRequest {

			private String stickerId;
			private String phoneNo;
			private String emailId;
			private String displayName;
			private String category;
			private String subCategory;
			private Object tagLine;
			private String secondaryPhoneNumber;
			private String mappingType;
			private String merchantGuid;
			private String typeOfQrCode;
			private String sourceId;
			private String merchantMid;
			private String posId;
			private String mappedBy;
			private String qrCodeId;
			private String latitude;
			private String longitude;
			private String allowedDistanceLimit;
			private String isDistanceLimitMandatory;
			private String deepLink;
			private Integer status;

			public String getStickerId() {
				return stickerId;
			}

			public void setStickerId(String stickerId) {
				this.stickerId = stickerId;
			}
			
			public MapRequest withStickerId(String stickerId) {
				this.stickerId = stickerId;
				return this;
			}

			public String getPhoneNo() {
				return phoneNo;
			}

			public void setPhoneNo(String phoneNo) {
				this.phoneNo = phoneNo;
			}
			
			public MapRequest withPhoneNo(String phoneNo) {
				this.phoneNo = phoneNo;
				return this;
			}

			public String getEmailId() {
				return emailId;
			}

			public void setEmailId(String emailId) {
				this.emailId = emailId;
			}
			
			public MapRequest withEmailId(String emailId) {
				this.emailId = emailId;
				return this;
			}

			public String getDisplayName() {
				return displayName;
			}

			public void setDisplayName(String displayName) {
				this.displayName = displayName;
			}
			
			public MapRequest withDisplayName(String displayName) {
				this.displayName = displayName;
				return this;
			}

			public String getCategory() {
				return category;
			}

			public void setCategory(String category) {
				this.category = category;
			}
			
			public MapRequest withCategory(String category) {
				this.category = category;
				return this;
			}

			public String getSubCategory() {
				return subCategory;
			}

			public void setSubCategory(String subCategory) {
				this.subCategory = subCategory;
			}
			
			public MapRequest withSubCategory(String subCategory) {
				this.subCategory = subCategory;
				return this;
			}

			public Object getTagLine() {
				return tagLine;
			}

			public void setTagLine(Object tagLine) {
				this.tagLine = tagLine;
			}
			
			public MapRequest withTagLine(Object tagLine) {
				this.tagLine = tagLine;
				return this;
			}

			public String getSecondaryPhoneNumber() {
				return secondaryPhoneNumber;
			}

			public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
				this.secondaryPhoneNumber = secondaryPhoneNumber;
			}
			
			public MapRequest withSecondaryPhoneNumber(String secondaryPhoneNumber) {
				this.secondaryPhoneNumber = secondaryPhoneNumber;
				return this;
			}

			public String getMappingType() {
				return mappingType;
			}

			public void setMappingType(String mappingType) {
				this.mappingType = mappingType;
			}
			
			public MapRequest withMappingType(String mappingType) {
				this.mappingType = mappingType;
				return this;
			}

			public String getMerchantGuid() {
				return merchantGuid;
			}

			public void setMerchantGuid(String merchantGuid) {
				this.merchantGuid = merchantGuid;
			}
			
			public MapRequest withMerchantGuid(String merchantGuid) {
				this.merchantGuid = merchantGuid;
				return this;
			}

			public String getTypeOfQrCode() {
				return typeOfQrCode;
			}

			public void setTypeOfQrCode(String typeOfQrCode) {
				this.typeOfQrCode = typeOfQrCode;
			}
			
			public MapRequest withTypeOfQrCode(String typeOfQrCode) {
				this.typeOfQrCode = typeOfQrCode;
				return this;
			}

			public String getSourceId() {
				return sourceId;
			}

			public void setSourceId(String sourceId) {
				this.sourceId = sourceId;
			}
			
			public MapRequest withSourceId(String sourceId) {
				this.sourceId = sourceId;
				return this;
			}

			public String getMerchantMid() {
				return merchantMid;
			}

			public void setMerchantMid(String merchantMid) {
				this.merchantMid = merchantMid;
			}
			
			public MapRequest withMerchantMid(String merchantMid) {
				this.merchantMid = merchantMid;
				return this;
			}

			public String getPosId() {
				return posId;
			}

			public void setPosId(String posId) {
				this.posId = posId;
			}
			
			public MapRequest withPosId(String posId) {
				this.posId = posId;
				return this;
			}

			public String getMappedBy() {
				return mappedBy;
			}

			public void setMappedBy(String mappedBy) {
				this.mappedBy = mappedBy;
			}
			
			public MapRequest withMappedBy(String mappedBy) {
				this.mappedBy = mappedBy;
				return this;
			}

			public String getQrCodeId() {
				return qrCodeId;
			}

			public void setQrCodeId(String qrCodeId) {
				this.qrCodeId = qrCodeId;
			}
			
			public MapRequest withQrCodeId(String qrCodeId) {
				this.qrCodeId = qrCodeId;
				return this;
			}

			public String getLatitude() {
				return latitude;
			}

			public void setLatitude(String latitude) {
				this.latitude = latitude;
			}
			
			public MapRequest withLatitude(String latitude) {
				this.latitude = latitude;
				return this;
			}

			public String getLongitude() {
				return longitude;
			}

			public void setLongitude(String longitude) {
				this.longitude = longitude;
			}
			
			public MapRequest withLongitude(String longitude) {
				this.longitude = longitude;
				return this;
			}

			public String getAllowedDistanceLimit() {
				return allowedDistanceLimit;
			}

			public void setAllowedDistanceLimit(String allowedDistanceLimit) {
				this.allowedDistanceLimit = allowedDistanceLimit;
			}
			
			public MapRequest withAllowedDistanceLimit(String allowedDistanceLimit) {
				this.allowedDistanceLimit = allowedDistanceLimit;
				return this;
			}

			public String getIsDistanceLimitMandatory() {
				return isDistanceLimitMandatory;
			}

			public void setIsDistanceLimitMandatory(String isDistanceLimitMandatory) {
				this.isDistanceLimitMandatory = isDistanceLimitMandatory;
			}
			
			public MapRequest withIsDistanceLimitMandatory(String isDistanceLimitMandatory) {
				this.isDistanceLimitMandatory = isDistanceLimitMandatory;
				return this;
			}

			public String getDeepLink() {
				return deepLink;
			}

			public void setDeepLink(String deepLink) {
				this.deepLink = deepLink;
			}
			
			public MapRequest withDeepLink(String deepLink) {
				this.deepLink = deepLink;
				return this;
			}

			public Integer getStatus() {
				return status;
			}

			public void setStatus(Integer status) {
				this.status = status;
			}
			
			public MapRequest withStatus(Integer status) {
				this.status = status;
				return this;
			}
			
			@Override
		    public String toString() {
		        return new ToStringBuilder(this).append("stickerId", stickerId).append("phoneNo", phoneNo).append("emailId", emailId)
		                .append("displayName", displayName).append("category", category).append("subCategory", subCategory)
		                .append("tagLine", tagLine).append("secondaryPhoneNumber", secondaryPhoneNumber).append("mappingType", mappingType)
		                .append("merchantGuid", merchantGuid).append("typeOfQrCode", typeOfQrCode).append("sourceId", sourceId)
		                .append("merchantMid", merchantMid).append("posId", posId).append("mappedBy", mappedBy).append("qrCodeId", qrCodeId)
		                .append("latitude", latitude).append("longitude", longitude).append("allowedDistanceLimit", allowedDistanceLimit)
		                .append("isDistanceLimitMandatory", isDistanceLimitMandatory).append("deepLink", deepLink)
		                .append("status", status).toString();
		    }
		}
	}
}
