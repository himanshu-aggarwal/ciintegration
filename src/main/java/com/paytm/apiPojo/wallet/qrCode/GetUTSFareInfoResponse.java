package com.paytm.apiPojo.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class GetUTSFareInfoResponse extends responsePojo {

	private String status;
	private String statusCode;
	private String statusMessage;
	private Response response;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public GetUTSFareInfoResponse withStatus(String status) {
		this.status = status;
		return this;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public GetUTSFareInfoResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public GetUTSFareInfoResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public GetUTSFareInfoResponse withResponse(Response response) {
		this.response = response;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("status", status).append("statusCode", statusCode)
				.append("statusMessage", statusMessage).append("response", response).toString();
	}

	public static class Response {

		private String merchantGuid;
		private String industryType;
		private List<FareDetail> fareDetails = null;
		private Integer sourceId;
		private String sourceName;
		private Integer destinationId;
		private String destinationName;
		private Integer routeId;
		private String routeName;
		private String busType;

		public String getMerchantGuid() {
			return merchantGuid;
		}

		public void setMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
		}

		public Response withMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
			return this;
		}

		public String getIndustryType() {
			return industryType;
		}

		public void setIndustryType(String industryType) {
			this.industryType = industryType;
		}

		public Response withIndustryType(String industryType) {
			this.industryType = industryType;
			return this;
		}

		public List<FareDetail> getFareDetails() {
			return fareDetails;
		}

		public void setFareDetails(List<FareDetail> fareDetails) {
			this.fareDetails = fareDetails;
		}

		public Response withFareDetails(List<FareDetail> fareDetails) {
			this.fareDetails = fareDetails;
			return this;
		}

		public Integer getSourceId() {
			return sourceId;
		}

		public void setSourceId(Integer sourceId) {
			this.sourceId = sourceId;
		}

		public Response withSourceId(Integer sourceId) {
			this.sourceId = sourceId;
			return this;
		}

		public String getSourceName() {
			return sourceName;
		}

		public void setSourceName(String sourceName) {
			this.sourceName = sourceName;
		}

		public Response withSourceName(String sourceName) {
			this.sourceName = sourceName;
			return this;
		}

		public Integer getDestinationId() {
			return destinationId;
		}

		public void setDestinationId(Integer destinationId) {
			this.destinationId = destinationId;
		}

		public Response withDestinationId(Integer destinationId) {
			this.destinationId = destinationId;
			return this;
		}

		public String getDestinationName() {
			return destinationName;
		}

		public void setDestinationName(String destinationName) {
			this.destinationName = destinationName;
		}

		public Response withDestinationName(String destinationName) {
			this.destinationName = destinationName;
			return this;
		}

		public Integer getRouteId() {
			return routeId;
		}

		public void setRouteId(Integer routeId) {
			this.routeId = routeId;
		}

		public Response withRouteId(Integer routeId) {
			this.routeId = routeId;
			return this;
		}

		public String getRouteName() {
			return routeName;
		}

		public void setRouteName(String routeName) {
			this.routeName = routeName;
		}

		public Response withRouteName(String routeName) {
			this.routeName = routeName;
			return this;
		}

		public String getBusType() {
			return busType;
		}

		public void setBusType(String busType) {
			this.busType = busType;
		}

		public Response withBusType(String busType) {
			this.busType = busType;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("merchantGuid", merchantGuid).append("industryType", industryType)
					.append("fareDetails", fareDetails).append("sourceId", sourceId).append("sourceName", sourceName)
					.append("destinationId", destinationId).append("destinationName", destinationName)
					.append("routeId", routeId).append("routeName", routeName).append("busType", busType).toString();
		}

		public static class FareDetail {

			private String pax;
			private Integer fare;

			public String getPax() {
				return pax;
			}

			public void setPax(String pax) {
				this.pax = pax;
			}

			public FareDetail withPax(String pax) {
				this.pax = pax;
				return this;
			}

			public Integer getFare() {
				return fare;
			}

			public void setFare(Integer fare) {
				this.fare = fare;
			}

			public FareDetail withFare(Integer fare) {
				this.fare = fare;
				return this;
			}

			public String toString() {
				return new ToStringBuilder(this).append("pax", pax).append("fare", fare).toString();
			}

		}
	}
}
