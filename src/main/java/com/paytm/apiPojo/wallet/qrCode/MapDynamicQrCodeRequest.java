package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;;

public class MapDynamicQrCodeRequest extends requestPojo {
	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;
	private String deviceId;
	private String metadata;
	private String channel;
	private String version;
	private String mode;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}
	
	public MapDynamicQrCodeRequest withRequest (Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public MapDynamicQrCodeRequest withIpAddress (String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	
	public MapDynamicQrCodeRequest withPlateformName (String plateformName) {
		this.platformName = plateformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	
	public MapDynamicQrCodeRequest withOperationType (String operationType) {
		this.operationType = operationType;
		return this;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public MapDynamicQrCodeRequest withDeviceId (String deviceId) {
		this.deviceId = deviceId;
		return this;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	
	public MapDynamicQrCodeRequest withMetadata (String metadata) {
		this.metadata = metadata;
		return this;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	public MapDynamicQrCodeRequest withChannel (String channel) {
		this.channel = channel;
		return this;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public MapDynamicQrCodeRequest withVersion (String version) {
		this.version = version;
		return this;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public MapDynamicQrCodeRequest withMode (String mode) {
		this.mode = mode;
		return this;
	}
	
	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress).append("platformName", platformName)
				.append("operationType", operationType).append("deviceId", deviceId).append("metadata", metadata).append("channel", channel)
				.append("version", version).append("mode", mode).toString();
	}

	public static class Request {

		private String stickerId;
		private String qrCodeId;
		private String phoneNo;
		private String emailId;
		private String displayName;
		private String category;
		private String subCategory;
		private String tagLine;
		private String secondaryPhoneNumber;
		private String mappingType;
		private String merchantGuid;
		private String typeOfQrCode;
		private String sourceId;
		private String deepLink;
		private String merchantMid;
		private String status;

		public String getStickerId() {
			return stickerId;
		}

		public void setStickerId(String stickerId) {
			this.stickerId = stickerId;
		}
		
		public Request withStickerId (String stickerId) {
			this.stickerId = stickerId;
			return this;
		}

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}
		
		public Request withQrCodeId (String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public String getPhoneNo() {
			return phoneNo;
		}

		public void setPhoneNo(String phoneNo) {
			this.phoneNo = phoneNo;
		}
		
		public Request withPhoneNo (String phoneNo) {
			this.phoneNo = phoneNo;
			return this;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}
		
		public Request withEmailId (String emailId) {
			this.emailId = emailId;
			return this;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}
		
		public Request withDisplayName(String displayName) {
			this.displayName = displayName;
			return this;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}
		
		public Request withCategory (String category) {
			this.category = category;
			return this;
		}

		public String getSubCategory() {
			return subCategory;
		}

		public void setSubCategory(String subCategory) {
			this.subCategory = subCategory;
		}
		
		public Request withSubCategory (String subCategory) {
			this.subCategory = subCategory;
			return this;
		}

		public String getTagLine() {
			return tagLine;
		}

		public void setTagLine(String tagLine) {
			this.tagLine = tagLine;
		}
		
		public Request withTagLine(String tagLine) {
			this.tagLine = tagLine;
			return this;
		}

		public String getSecondaryPhoneNumber() {
			return secondaryPhoneNumber;
		}

		public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
			this.secondaryPhoneNumber = secondaryPhoneNumber;
		}
		
		public Request withSecondaryPhoneNumber (String secondaryPhoneNumber) {
			this.secondaryPhoneNumber = secondaryPhoneNumber;
			return this;
		}

		public String getMappingType() {
			return mappingType;
		}

		public void setMappingType(String mappingType) {
			this.mappingType = mappingType;
		}
		
		public Request withMappingType(String mappingType) {
			this.mappingType = mappingType;
			return this;
		}
		
		public String getMerchantGuid() {
			return merchantGuid;
		}
		
		public void setMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
		}
		
		public Request withMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
			return this;
		}

		public String getTypeOfQrCode() {
			return typeOfQrCode;
		}

		public void setTypeOfQrCode(String typeOfQrCode) {
			this.typeOfQrCode = typeOfQrCode;
		}
		
		public Request withTypeOfQrCode(String typeOfQrCode) {
			this.typeOfQrCode = typeOfQrCode;
			return this;
		}

		public String getSourceId() {
			return sourceId;
		}

		public void setSourceId(String sourceId) {
			this.sourceId = sourceId;
		}
		
		public Request withSourceId (String sourceId) {
			this.sourceId = sourceId;
			return this;
		}

		public String getDeepLink() {
			return deepLink;
		}
		
		public void setDeepLink(String deepLink) {
			this.deepLink = deepLink;
		}
		
		public Request withDeepLink(String deepLink) {
			this.deepLink = deepLink;
			return this;
		}
		
		public String getMerchantMid() {
			return merchantMid;
		}

		public void setMerchantMid(String merchantMid) {
			this.merchantMid = merchantMid;
		}

		public Request withMerchantMid(String merchantMid) {
			this.merchantMid = merchantMid;
			return this;
		}
		
		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Request withStatus(String status) {
			this.status = status;
			return this;
		}
		
		public String toString() {
			return new ToStringBuilder(this).append("stickerId", stickerId).append("qrCodeId", qrCodeId).append("phoneNo", phoneNo)
					.append("emailId", emailId).append("displayName", displayName).append("category", category)
					.append("subCategory", subCategory).append("tagLine", tagLine).append("secondaryPhoneNumber", secondaryPhoneNumber)
					.append("mappingType", mappingType).append("typeOfQrCode", typeOfQrCode).append("sourceId", sourceId)
					.append("merchantMid", merchantMid).append("status", status).toString();
		}
	}
}
