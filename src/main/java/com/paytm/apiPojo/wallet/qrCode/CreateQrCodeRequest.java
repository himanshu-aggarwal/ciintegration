package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class CreateQrCodeRequest extends requestPojo {

	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public CreateQrCodeRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public CreateQrCodeRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public CreateQrCodeRequest withPlatformName(String platformName) {
		this.platformName = platformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public CreateQrCodeRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
				.append("plateformName", platformName).append("operationType", operationType).toString();
	}

	public static class Request {

		private String merchantGuid;
		private String requestType;
		private String orderDetails;
		private String productId;
		private String productDetails;
		private String orderId;
		private String amount;
		private String merchantName;
		private String mid;
		private String expiryDate;
		private String posId;
		private QrInfo qrInfo;
		private Boolean imageRequired;
		private String validity;

		public String getMerchantGuid() {
			return merchantGuid;
		}

		public void setMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
		}

		public Request withMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
			return this;
		}

		public String getRequestType() {
			return requestType;
		}

		public void setRequestType(String requestType) {
			this.requestType = requestType;
		}

		public Request withRequestType(String requestType) {
			this.requestType = requestType;
			return this;
		}

		public String getOrderDetails() {
			return orderDetails;
		}

		public void setOrderDetails(String orderDetails) {
			this.orderDetails = orderDetails;
		}

		public Request withOrderDetails(String orderDetails) {
			this.orderDetails = orderDetails;
			return this;
		}

		public String getProductId() {
			return productId;
		}

		public void setProductId(String productId) {
			this.productId = productId;
		}

		public Request withProductId(String productId) {
			this.productId = productId;
			return this;
		}

		public String getProductDetails() {
			return productDetails;
		}

		public void setProductDetails(String productDetails) {
			this.productDetails = productDetails;
		}

		public Request withProductDetails(String productDetails) {
			this.productDetails = productDetails;
			return this;
		}

		public String getOrderId() {
			return orderId;
		}

		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}

		public Request withOrderId(String orderId) {
			this.orderId = orderId;
			return this;
		}

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public Request withAmount(String amount) {
			this.amount = amount;
			return this;
		}

		public String getMerchantName() {
			return merchantName;
		}

		public void setMerchantName(String merchantName) {
			this.merchantName = merchantName;
		}

		public Request withMerchantName(String merchantName) {
			this.merchantName = merchantName;
			return this;
		}

		public String getMid() {
			return mid;
		}

		public void setMid(String mid) {
			this.mid = mid;
		}

		public Request withMid(String mid) {
			this.mid = mid;
			return this;
		}

		public String getExpiryDate() {
			return expiryDate;
		}

		public void setExpiryDate(String expiryDate) {
			this.expiryDate = expiryDate;
		}

		public Request withExpiryDate(String expiryDate) {
			this.expiryDate = expiryDate;
			return this;
		}

		public String getPosId() {
			return posId;
		}

		public void setPosId(String posId) {
			this.posId = posId;
		}

		public Request withPosId(String posId) {
			this.posId = posId;
			return this;
		}

		public QrInfo getQrInfo() {
			return qrInfo;
		}

		public void setQrInfo(QrInfo qrInfo) {
			this.qrInfo = qrInfo;
		}

		public Request withQrInfo(QrInfo qrInfo) {
			this.qrInfo = qrInfo;
			return this;
		}

		public Boolean getImageRequired() {
			return imageRequired;
		}

		public void setImageRequired(Boolean imageRequired) {
			this.imageRequired = imageRequired;
		}

		public Request withImageRequired(Boolean imageRequired) {
			this.imageRequired = imageRequired;
			return this;
		}

		public String getValidity() {
			return validity;
		}

		public void setValidity(String validity) {
			this.validity = validity;
		}

		public Request withValidity(String validity) {
			this.validity = validity;
			return this;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("merchantGuid", merchantGuid).append("requestType", requestType)
					.append("orderDetails", orderDetails).append("productId", productId)
					.append("productDetails", productDetails).append("orderId", orderId).append("amount", amount)
					.append("merchantName", merchantName).append("mid", mid).append("expiryDate", expiryDate)
					.append("posId", posId).append("qrInfo", qrInfo).append("imageRequired", imageRequired)
					.append("validity", validity).toString();
		}

		public static class QrInfo {

			private String handle;

			public String getHandle() {
				return handle;
			}

			public void setHandle(String handle) {
				this.handle = handle;
			}

			public QrInfo withHandle(String handle) {
				this.handle = handle;
				return this;
			}

			@Override
			public String toString() {
				return new ToStringBuilder(this).append("handle", handle).toString();
			}
		}
	}
}
