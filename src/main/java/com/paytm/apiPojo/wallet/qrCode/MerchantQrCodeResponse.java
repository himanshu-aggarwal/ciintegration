package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class MerchantQrCodeResponse extends responsePojo {

	private Object requestGuid;
	private Object orderId;
	private String status;
	private String statusCode;
	private String statusMessage;
	private Response response;

	public Object getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}
	
	public MerchantQrCodeResponse withRequestguid(Object requestGuid) {
		this.requestGuid = requestGuid;
		return this;
	}

	public Object getOrderId() {
		return orderId;
	}

	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}
	
	public MerchantQrCodeResponse withOrderId(Object orderId) {
		this.orderId = orderId;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public MerchantQrCodeResponse withStatus(String status) {
		this.status = status;
		return this;
	}
	
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public MerchantQrCodeResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	public MerchantQrCodeResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
	public MerchantQrCodeResponse withResponse(Response response) {
		this.response = response;
		return this;
	}
	
	public String toString() {
		return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
				.append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
				.append("response", response).toString();
	}

	public static class Response {

		private String qrCodeId;
		private String encryptedData;
		private String displayName;

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}
		
		public Response withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public String getEncryptedData() {
			return encryptedData;
		}

		public void setEncryptedData(String encryptedData) {
			this.encryptedData = encryptedData;
		}
		
		public Response withEncryptedData(String encryptedData) {
			this.encryptedData = encryptedData;
			return this;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}
		
		public Response withDisplayName(String displayName) {
			this.displayName = displayName;
			return this;
		}
		
		public String toString() {
			return new ToStringBuilder(this).append("qrCodeId", qrCodeId).append("encryptedData", encryptedData)
					.append("displayName", displayName).toString();
		}

	}
}
