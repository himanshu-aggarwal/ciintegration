package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class EditQrCodeDetailsResponse extends responsePojo {

	private Object requestGuid;
	private Object orderId;
	private Object status;
	private String statusCode;
	private String statusMessage;
	private Response response;

	public Object getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
	}

	public EditQrCodeDetailsResponse withRequestGuid(Object requestGuid) {
		this.requestGuid = requestGuid;
		return this;
	}

	public Object getOrderId() {
		return orderId;
	}

	public void setOrderId(Object orderId) {
		this.orderId = orderId;
	}

	public EditQrCodeDetailsResponse withOrderId(Object orderId) {
		this.orderId = orderId;
		return this;
	}

	public Object getStatus() {
		return status;
	}

	public void setStatus(Object status) {
		this.status = status;
	}

	public EditQrCodeDetailsResponse withStatus(Object status) {
		this.status = status;
		return this;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public EditQrCodeDetailsResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public EditQrCodeDetailsResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public EditQrCodeDetailsResponse withResponse(Response response) {
		this.response = response;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
				.append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
				.append("response", response).toString();
	}

	public static class Response {

		private String qrCodeId;
		private String editResponse;

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}

		public Response withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public String getEditResponse() {
			return editResponse;
		}

		public void setEditResponse(String editResponse) {
			this.editResponse = editResponse;
		}

		public Response withEditResponse(String editResponse) {
			this.editResponse = editResponse;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("qrCodeId", qrCodeId).append("editResponse", editResponse)
					.toString();
		}
	}
}
