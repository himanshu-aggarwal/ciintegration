package com.paytm.apiPojo.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import org.apache.commons.lang.builder.ToStringBuilder;
public class GetMerchantInfoRequest extends APIInterface.requestPojo {
    private Request request;
    private String ipAddress;
    private String platformName;
    private String operationType;

    public Request getRequest(){
        return request;
    }

    public void setRequest(Request request){
        this.request=request;
    }

    public GetMerchantInfoRequest withRequest(Request request){
        this.request=request;
        return this;
    }
    public String getIpAddress(){
        return ipAddress;
    }

    public void setIpAdress(String ipAddress){
        this.ipAddress=ipAddress;
    }

    public GetMerchantInfoRequest withIpAddress(String ipAddress){
        this.ipAddress=ipAddress;
        return this;
    }

    public String getPlatformName(){
        return platformName;
    }

    public void setPlatformName(String platformName){
        this.platformName=platformName;
    }

    public GetMerchantInfoRequest withPlatformName(String platformName){
        this.platformName=platformName;
        return this;
    }

    public String getOperationType(){
        return operationType;
    }

    public void setOperationType(String operationType){
        this.operationType=operationType;
    }

    public GetMerchantInfoRequest withOperationType(String operationType){
        this.operationType=operationType;
        return this;
    }

    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("plateformName", platformName).append("operationType", operationType).toString();
    }

    public static class Request {
        private String mid;
        private String merchantGuid;

        public String getMid() {
            return mid;
        }

        public Request withMid(String mid) {
            this.mid = mid;
            return this;
        }

        public void setMid(String mid) {
            this.mid = mid;
        }

        public String getMerchantGuid() {
            return merchantGuid;
        }

        public Request withMerchantGuid(String merchantGuid) {
            this.merchantGuid= merchantGuid;
            return this;
        }

        public void setMerchantGuid(String merchantGuid) {
            this.merchantGuid = merchantGuid;
        }

        public String toString() {
            return new ToStringBuilder(this).append("mid", mid).append("merchantGuid", merchantGuid).toString();
        }
    }
}
