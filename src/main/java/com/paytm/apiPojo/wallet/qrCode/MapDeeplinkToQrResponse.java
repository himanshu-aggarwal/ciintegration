package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class MapDeeplinkToQrResponse extends responsePojo {

	private String requestGuid;
	private String orderId;
	private String status;
	private String statusCode;
	private String statusMessage;
	private Response response;

	public String getRequestGuid() {
		return requestGuid;
	}

	public void setRequestGuid(String requestGuid) {
		this.requestGuid = requestGuid;
	}

	public MapDeeplinkToQrResponse withRequestGuid(String requestGuid) {
		this.requestGuid = requestGuid;
		return this;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public MapDeeplinkToQrResponse withOrderId(String orderId) {
		this.orderId = orderId;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public MapDeeplinkToQrResponse withStatus(String status) {
		this.status = status;
		return this;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public MapDeeplinkToQrResponse withStatusCode(String statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public MapDeeplinkToQrResponse withStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
		return this;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public MapDeeplinkToQrResponse withResponse(Response response) {
		this.response = response;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
				.append("status", status).append("statusCode", statusCode).append("statusMessage", statusMessage)
				.append("response", response).toString();
	}

	public static class Response {

		private String stickerId;
		private String qrCodeId;
		private String mappingId;
		private String mappingType;
		private String status;
		private String displayName;
		private String category;
		private String subCategory;
		private String tagLine;
		private Object metadata;
		private String typeOfQrCode;
		private String secondaryPhoneNumber;
		private String posId;

		public String getStickerId() {
			return stickerId;
		}

		public void setStickerId(String stickerId) {
			this.stickerId = stickerId;
		}

		public Response withStickerId(String stickerId) {
			this.stickerId = stickerId;
			return this;
		}

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}

		public Response withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public String getMappingId() {
			return mappingId;
		}

		public void setMappingId(String mappingId) {
			this.mappingId = mappingId;
		}

		public Response withMappingId(String mappingId) {
			this.mappingId = mappingId;
			return this;
		}

		public String getMappingType() {
			return mappingType;
		}

		public void setMappingType(String mappingType) {
			this.mappingType = mappingType;
		}

		public Response withMappingType(String mappingType) {
			this.mappingType = mappingType;
			return this;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Response withStatus(String status) {
			this.status = status;
			return this;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public Response withDisplayName(String displayName) {
			this.displayName = displayName;
			return this;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public Response withCategory(String category) {
			this.category = category;
			return this;
		}

		public String getSubCategory() {
			return subCategory;
		}

		public void setSubCategory(String subCategory) {
			this.subCategory = subCategory;
		}

		public Response withSubCategory(String subCategory) {
			this.subCategory = subCategory;
			return this;
		}

		public String getTagLine() {
			return tagLine;
		}

		public void setTagLine(String tagLine) {
			this.tagLine = tagLine;
		}

		public Response withTagLine(String tagLine) {
			this.tagLine = tagLine;
			return this;
		}

		public Object getMetadata() {
			return metadata;
		}

		public void setMetadata(Object metadata) {
			this.metadata = metadata;
		}

		public Response withMetadata(Object metadata) {
			this.metadata = metadata;
			return this;
		}

		public String getTypeOfQrCode() {
			return typeOfQrCode;
		}

		public void setTypeOfQrCode(String typeOfQrCode) {
			this.typeOfQrCode = typeOfQrCode;
		}

		public Response withTypeOfQrCode(String typeOfQrCode) {
			this.typeOfQrCode = typeOfQrCode;
			return this;
		}

		public String getSecondaryPhoneNumber() {
			return secondaryPhoneNumber;
		}

		public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
			this.secondaryPhoneNumber = secondaryPhoneNumber;
		}

		public Response withSecondaryPhoneNumber(String secondaryPhonenumber) {
			this.secondaryPhoneNumber = secondaryPhonenumber;
			return this;
		}

		public String getPosId() {
			return posId;
		}

		public void setPosId(String posId) {
			this.posId = posId;
		}

		public Response withPosId(String posId) {
			this.posId = posId;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("stickerId", stickerId).append("qrCodeId", qrCodeId)
					.append("mappingId", mappingId).append("mappingType", mappingType).append("status", status)
					.append("displayName", displayName).append("category", category).append("subCategory", subCategory)
					.append("tagLine", tagLine).append("metadata", metadata).append("typeOfQrCode", typeOfQrCode)
					.append("secondaryPhoneNumber", secondaryPhoneNumber).append("posId", posId).toString();
		}
	}
}

