package com.paytm.apiPojo.wallet.qrCode;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface;

public class GetMerchantInfoResponse extends APIInterface.responsePojo {
    private String requestGuid;
    private String orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private Response response;
    public String getRequestGuid(){
        return requestGuid;
    }

    public void setRequestGuid(String requestGuid){
        this.requestGuid=requestGuid;
    }

    public GetMerchantInfoResponse withRequestGuid(String requestGuid){
        this.requestGuid=requestGuid;
        return this;
    }

    public String getOrderId(){
        return orderId;
    }

    public void setOrderId(String orderId){
        this.orderId=orderId;
    }

    public GetMerchantInfoResponse withOrderId(String orderId){
        this.orderId=orderId;
        return this;
    }

    public String getStatus(){
        return status;
    }
    public void setStatus(String status){
        this.status=status;
    }

    public GetMerchantInfoResponse withStatus(String status){
        this.status=status;
        return this;
    }

    public String getStatusCode(){
        return statusCode;
    }

    public void setStatusCode(String statusCode){
        this.statusCode=statusCode;
    }
    public GetMerchantInfoResponse withStatusCode(String statusCode){
        this.statusCode=statusCode;
        return this;
    }
    public String getStatusMessage(){
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage){
        this.statusMessage=statusMessage;
    }

    public GetMerchantInfoResponse withStatusMessage(String statusMessage){
        this.statusMessage=statusMessage;
        return this;
    }

    public Response getResponse(){
        return response;
    }
    public void setResponse(Response response){
        this.response=response;
    }
    public GetMerchantInfoResponse withResponse(Response response){
        this.response=response;
        return this;
    }

    public String toString(){
        return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId",orderId).append("status", status).
                append("statusCode", statusCode).append("statusMessage",statusMessage).append("response",response).toString();
    }

    public static class Response{
        private String path;
        private String encryptedData;
        private String qrData;
        private String merchantName;
        private String logoUrl;

        public String getPath() {
            return path;
        }

        public Response withPath(String path){
            this.path=path;
            return this;
        }
        public void setPath(String path) {
            this.path = path;
        }

        public String getEncryptedData() {
            return encryptedData;
        }

        public Response withEncryptedData(String encryptedData){
            this.encryptedData=encryptedData;
            return this;
        }

        public void setEncryptedData(String encryptedData) {
            this.encryptedData = encryptedData;
        }

        public String getQrData() {
            return qrData;
        }
        public Response withQrData(String qrData){
            this.qrData=qrData;
            return this;
        }

        public void setQrData(String qrData) {
            this.qrData = qrData;
        }

        public String getMerchantName() {
            return merchantName;
        }
        public Response withMerchantName(String merchantName){
            this.merchantName=merchantName;
            return this;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getLogoUrl() {
            return logoUrl;
        }

        public Response withLogoUrl(String logoUrl){
            this.logoUrl=logoUrl;
            return this;
        }
        public void setLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
        }
        public String toString(){
            return new ToStringBuilder(this).append("path", path).append("encryptedData",encryptedData).append("qrData", qrData).
                    append("merchantName", merchantName).append("logoUrl",logoUrl).toString();
        }
    }

}
