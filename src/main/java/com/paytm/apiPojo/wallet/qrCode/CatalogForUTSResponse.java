package com.paytm.apiPojo.wallet.qrCode;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class CatalogForUTSResponse {

	private Integer id;
	private String name;
	private String path;
	@JsonProperty("meta_title")
	private String metaTitle;
	@JsonProperty("meta_description")
	private String metaDescription;
	@JsonProperty("meta_keyword")
	private String metaKeyword;
	@JsonProperty("offer_text")
	private String offerText;
	@JsonProperty("long_rich_desc")
	private String longRichDesc;
	@JsonProperty("canonical_url")
	private Object canonicalUrl;
	@JsonProperty("terms_conditions")
	private TermsConditions termsConditions;
	private List<Ancestor> ancestors = null;
	@JsonProperty("sorting_keys")
	private List<SortingKey> sortingKeys = null;
	@JsonProperty("pincode_filter")
	private Boolean pincodeFilter;
	@JsonProperty("grid_layout")
	private List<GridLayout> gridLayout = null;
	private List<Filter> filters = null;
	@JsonProperty("has_more")
	private Boolean hasMore;
	private Integer totalCount;
	@JsonProperty("no_index")
	private Boolean noIndex;
	@JsonProperty("no_follow")
	private Boolean noFollow;
	@JsonProperty("view_type")
	private String viewType;
	@JsonProperty("add_to_cart")
	private Integer addToCart;
	@JsonProperty("show_offers")
	private Integer showOffers;
	@JsonProperty("location_racking_enabled")
	private Boolean locationRackingEnabled;
	@JsonProperty("use_promo_offer")
	private Integer usePromoOffer;
	@JsonProperty("short_description")
	private Object shortDescription;
	private Object title;
	@JsonProperty("default_sorting_param")
	private String defaultSortingParam;
	@JsonProperty("ga_key")
	private String gaKey;
	private Verticals verticals;
	@JsonProperty("rich_text_filters")
	private List<Object> richTextFilters = null;
	@JsonProperty("frontend_filters")
	private List<Object> frontendFilters = null;
	private Integer cacheBits;
	private ContextParams contextParams;
	private Boolean redirect;
	@JsonProperty("redirect_url")
	private String redirectUrl;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getMetaKeyword() {
		return metaKeyword;
	}

	public void setMetaKeyword(String metaKeyword) {
		this.metaKeyword = metaKeyword;
	}

	public String getOfferText() {
		return offerText;
	}

	public void setOfferText(String offerText) {
		this.offerText = offerText;
	}

	public String getLongRichDesc() {
		return longRichDesc;
	}

	public void setLongRichDesc(String longRichDesc) {
		this.longRichDesc = longRichDesc;
	}

	public Object getCanonicalUrl() {
		return canonicalUrl;
	}

	public void setCanonicalUrl(Object canonicalUrl) {
		this.canonicalUrl = canonicalUrl;
	}

	public TermsConditions getTermsConditions() {
		return termsConditions;
	}

	public void setTermsConditions(TermsConditions termsConditions) {
		this.termsConditions = termsConditions;
	}

	public List<Ancestor> getAncestors() {
		return ancestors;
	}

	public void setAncestors(List<Ancestor> ancestors) {
		this.ancestors = ancestors;
	}

	public List<SortingKey> getSortingKeys() {
		return sortingKeys;
	}

	public void setSortingKeys(List<SortingKey> sortingKeys) {
		this.sortingKeys = sortingKeys;
	}

	public Boolean getPincodeFilter() {
		return pincodeFilter;
	}

	public void setPincodeFilter(Boolean pincodeFilter) {
		this.pincodeFilter = pincodeFilter;
	}

	public List<GridLayout> getGridLayout() {
		return gridLayout;
	}

	public void setGridLayout(List<GridLayout> gridLayout) {
		this.gridLayout = gridLayout;
	}

	public List<Filter> getFilters() {
		return filters;
	}

	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}

	public Boolean getHasMore() {
		return hasMore;
	}

	public void setHasMore(Boolean hasMore) {
		this.hasMore = hasMore;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Boolean getNoIndex() {
		return noIndex;
	}

	public void setNoIndex(Boolean noIndex) {
		this.noIndex = noIndex;
	}

	public Boolean getNoFollow() {
		return noFollow;
	}

	public void setNoFollow(Boolean noFollow) {
		this.noFollow = noFollow;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public Integer getAddToCart() {
		return addToCart;
	}

	public void setAddToCart(Integer addToCart) {
		this.addToCart = addToCart;
	}

	public Integer getShowOffers() {
		return showOffers;
	}

	public void setShowOffers(Integer showOffers) {
		this.showOffers = showOffers;
	}

	public Boolean getLocationRackingEnabled() {
		return locationRackingEnabled;
	}

	public void setLocationRackingEnabled(Boolean locationRackingEnabled) {
		this.locationRackingEnabled = locationRackingEnabled;
	}

	public Integer getUsePromoOffer() {
		return usePromoOffer;
	}

	public void setUsePromoOffer(Integer usePromoOffer) {
		this.usePromoOffer = usePromoOffer;
	}

	public Object getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(Object shortDescription) {
		this.shortDescription = shortDescription;
	}

	public Object getTitle() {
		return title;
	}

	public void setTitle(Object title) {
		this.title = title;
	}

	public String getDefaultSortingParam() {
		return defaultSortingParam;
	}

	public void setDefaultSortingParam(String defaultSortingParam) {
		this.defaultSortingParam = defaultSortingParam;
	}

	public String getGaKey() {
		return gaKey;
	}

	public void setGaKey(String gaKey) {
		this.gaKey = gaKey;
	}

	public Verticals getVerticals() {
		return verticals;
	}

	public void setVerticals(Verticals verticals) {
		this.verticals = verticals;
	}

	public List<Object> getRichTextFilters() {
		return richTextFilters;
	}

	public void setRichTextFilters(List<Object> richTextFilters) {
		this.richTextFilters = richTextFilters;
	}

	public List<Object> getFrontendFilters() {
		return frontendFilters;
	}

	public void setFrontendFilters(List<Object> frontendFilters) {
		this.frontendFilters = frontendFilters;
	}

	public Integer getCacheBits() {
		return cacheBits;
	}

	public void setCacheBits(Integer cacheBits) {
		this.cacheBits = cacheBits;
	}

	public ContextParams getContextParams() {
		return contextParams;
	}

	public void setContextParams(ContextParams contextParams) {
		this.contextParams = contextParams;
	}

	public Boolean getRedirect() {
		return redirect;
	}

	public void setRedirect(Boolean redirect) {
		this.redirect = redirect;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public static class TermsConditions {

		private String title;
		private String message;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}

	public static class Ancestor {

		private String name;
		private Integer id;
		@JsonProperty("url_key")
		private String urlKey;
		@JsonProperty("url_type")
		private String urlType;
		private String seourl;
		private String newurl;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getUrlKey() {
			return urlKey;
		}

		public void setUrlKey(String urlKey) {
			this.urlKey = urlKey;
		}

		public String getUrlType() {
			return urlType;
		}

		public void setUrlType(String urlType) {
			this.urlType = urlType;
		}

		public String getSeourl() {
			return seourl;
		}

		public void setSeourl(String seourl) {
			this.seourl = seourl;
		}

		public String getNewurl() {
			return newurl;
		}

		public void setNewurl(String newurl) {
			this.newurl = newurl;
		}
	}

	public static class SortingKey {

		private String name;
		private String urlParams;
		@JsonProperty("default")
		private String _default;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUrlParams() {
			return urlParams;
		}

		public void setUrlParams(String urlParams) {
			this.urlParams = urlParams;
		}

		public String getDefault() {
			return _default;
		}

		public void setDefault(String _default) {
			this._default = _default;
		}
	}

	public static class GridLayout {

		@JsonProperty("product_id")
		private Integer productId;
		@JsonProperty("parent_id")
		private Object parentId;
		@JsonProperty("complex_product_id")	
		private Object complexProductId;
		private String name;
		@JsonProperty("short_desc")	
		private String shortDesc;
		@JsonProperty("bullet_points")
		private BulletPoints bulletPoints;
		private String url;
		private String seourl;
		@JsonProperty("url_type")
		private String urlType;
		@JsonProperty("promo_text")
		private Object promoText;
		@JsonProperty("image_url")
		private String imageUrl;
		@JsonProperty("vertical_id")
		private Integer verticalId;
		@JsonProperty("offer_price")
		private Integer offerPrice;
		@JsonProperty("actual_price")
		private Integer actualPrice;
		@JsonProperty("merchant_name")
		private String merchantName;
		@JsonProperty("authorised_merchant")
		private Boolean authorisedMerchant;
		private Boolean stock;
		private String brand;
		private Object tag;
		@JsonProperty("product_tag")	
		private Object productTag;
		private Boolean shippable;
		@JsonProperty("created_at")
		private String createdAt;
		@JsonProperty("updated_at")
		private String updatedAt;
		@JsonProperty("img_width")
		private Integer imgWidth;
		@JsonProperty("img_height")
		private Integer imgHeight;
		private String source;
		@JsonProperty("location_score")
		private Integer locationScore;
		@JsonProperty("category_id")
		private Integer categoryId;
		private Integer discoverability;
		@JsonProperty("exchange_eligibility")
		private Boolean exchangeEligibility;
		@JsonProperty("add_to_cart")
		private Integer addToCart;
		@JsonProperty("min_quantity")
		private Integer minQuantity;
		@JsonProperty("max_quantity")
		private Integer maxQuantity;
		@JsonProperty("variant_selection_api")
		private String variantSelectionApi;
		@JsonProperty("merchant_id")
		private Integer merchantId;
		@JsonProperty("brand_id")
		private Integer brandId;
		private String newurl;
		private Attributes attributes;
		@JsonProperty("category_ids")
		private List<Integer> categoryIds = null;
		private String discount;

		public Integer getProductId() {
			return productId;
		}

		public void setProductId(Integer productId) {
			this.productId = productId;
		}

		public Object getParentId() {
			return parentId;
		}

		public void setParentId(Object parentId) {
			this.parentId = parentId;
		}

		public Object getComplexProductId() {
			return complexProductId;
		}

		public void setComplexProductId(Object complexProductId) {
			this.complexProductId = complexProductId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getShortDesc() {
			return shortDesc;
		}

		public void setShortDesc(String shortDesc) {
			this.shortDesc = shortDesc;
		}

		public BulletPoints getBulletPoints() {
			return bulletPoints;
		}

		public void setBulletPoints(BulletPoints bulletPoints) {
			this.bulletPoints = bulletPoints;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getSeourl() {
			return seourl;
		}

		public void setSeourl(String seourl) {
			this.seourl = seourl;
		}

		public String getUrlType() {
			return urlType;
		}

		public void setUrlType(String urlType) {
			this.urlType = urlType;
		}

		public Object getPromoText() {
			return promoText;
		}

		public void setPromoText(Object promoText) {
			this.promoText = promoText;
		}

		public String getImageUrl() {
			return imageUrl;
		}

		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}

		public Integer getVerticalId() {
			return verticalId;
		}

		public void setVerticalId(Integer verticalId) {
			this.verticalId = verticalId;
		}

		public Integer getOfferPrice() {
			return offerPrice;
		}

		public void setOfferPrice(Integer offerPrice) {
			this.offerPrice = offerPrice;
		}

		public Integer getActualPrice() {
			return actualPrice;
		}

		public void setActualPrice(Integer actualPrice) {
			this.actualPrice = actualPrice;
		}

		public String getMerchantName() {
			return merchantName;
		}

		public void setMerchantName(String merchantName) {
			this.merchantName = merchantName;
		}

		public Boolean getAuthorisedMerchant() {
			return authorisedMerchant;
		}

		public void setAuthorisedMerchant(Boolean authorisedMerchant) {
			this.authorisedMerchant = authorisedMerchant;
		}

		public Boolean getStock() {
			return stock;
		}

		public void setStock(Boolean stock) {
			this.stock = stock;
		}

		public String getBrand() {
			return brand;
		}

		public void setBrand(String brand) {
			this.brand = brand;
		}

		public Object getTag() {
			return tag;
		}

		public void setTag(Object tag) {
			this.tag = tag;
		}

		public Object getProductTag() {
			return productTag;
		}

		public void setProductTag(Object productTag) {
			this.productTag = productTag;
		}

		public Boolean getShippable() {
			return shippable;
		}

		public void setShippable(Boolean shippable) {
			this.shippable = shippable;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public Integer getImgWidth() {
			return imgWidth;
		}

		public void setImgWidth(Integer imgWidth) {
			this.imgWidth = imgWidth;
		}

		public Integer getImgHeight() {
			return imgHeight;
		}

		public void setImgHeight(Integer imgHeight) {
			this.imgHeight = imgHeight;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public Integer getLocationScore() {
			return locationScore;
		}

		public void setLocationScore(Integer locationScore) {
			this.locationScore = locationScore;
		}

		public Integer getCategoryId() {
			return categoryId;
		}

		public void setCategoryId(Integer categoryId) {
			this.categoryId = categoryId;
		}

		public Integer getDiscoverability() {
			return discoverability;
		}

		public void setDiscoverability(Integer discoverability) {
			this.discoverability = discoverability;
		}

		public Boolean getExchangeEligibility() {
			return exchangeEligibility;
		}

		public void setExchangeEligibility(Boolean exchangeEligibility) {
			this.exchangeEligibility = exchangeEligibility;
		}

		public Integer getAddToCart() {
			return addToCart;
		}

		public void setAddToCart(Integer addToCart) {
			this.addToCart = addToCart;
		}

		public Integer getMinQuantity() {
			return minQuantity;
		}

		public void setMinQuantity(Integer minQuantity) {
			this.minQuantity = minQuantity;
		}

		public Integer getMaxQuantity() {
			return maxQuantity;
		}

		public void setMaxQuantity(Integer maxQuantity) {
			this.maxQuantity = maxQuantity;
		}

		public String getVariantSelectionApi() {
			return variantSelectionApi;
		}

		public void setVariantSelectionApi(String variantSelectionApi) {
			this.variantSelectionApi = variantSelectionApi;
		}

		public Integer getMerchantId() {
			return merchantId;
		}

		public void setMerchantId(Integer merchantId) {
			this.merchantId = merchantId;
		}

		public Integer getBrandId() {
			return brandId;
		}

		public void setBrandId(Integer brandId) {
			this.brandId = brandId;
		}

		public String getNewurl() {
			return newurl;
		}

		public void setNewurl(String newurl) {
			this.newurl = newurl;
		}

		public Attributes getAttributes() {
			return attributes;
		}

		public void setAttributes(Attributes attributes) {
			this.attributes = attributes;
		}

		public List<Integer> getCategoryIds() {
			return categoryIds;
		}

		public void setCategoryIds(List<Integer> categoryIds) {
			this.categoryIds = categoryIds;
		}

		public String getDiscount() {
			return discount;
		}

		public void setDiscount(String discount) {
			this.discount = discount;
		}

		public static class BulletPoints {

		}

		public static class Attributes {

			private String source;
			private String destination;
			private String route;
			private String vendor;
			@JsonProperty("bus_type")
			private String busType;
			@JsonProperty("passenger_type")
			private String passengerType;
			@JsonProperty("source_id")
			private String sourceId;
			@JsonProperty("destination_id")
			private String destinationId;
			@JsonProperty("route_id")
			private String routeId;
			@JsonProperty("pax_limit")
			private String paxLimit;
			@JsonProperty("ticket_limit")
			private String ticketLimit;
			@JsonProperty("merchant_source")
			private String merchantSource;
			@JsonProperty("merchant_destination")
			private String merchantDestination;

			public String getSource() {
				return source;
			}

			public void setSource(String source) {
				this.source = source;
			}

			public String getDestination() {
				return destination;
			}

			public void setDestination(String destination) {
				this.destination = destination;
			}

			public String getRoute() {
				return route;
			}

			public void setRoute(String route) {
				this.route = route;
			}

			public String getVendor() {
				return vendor;
			}

			public void setVendor(String vendor) {
				this.vendor = vendor;
			}

			public String getBusType() {
				return busType;
			}

			public void setBusType(String busType) {
				this.busType = busType;
			}

			public String getPassengerType() {
				return passengerType;
			}

			public void setPassengerType(String passengerType) {
				this.passengerType = passengerType;
			}

			public String getSourceId() {
				return sourceId;
			}

			public void setSourceId(String sourceId) {
				this.sourceId = sourceId;
			}

			public String getDestinationId() {
				return destinationId;
			}

			public void setDestinationId(String destinationId) {
				this.destinationId = destinationId;
			}

			public String getRouteId() {
				return routeId;
			}

			public void setRouteId(String routeId) {
				this.routeId = routeId;
			}

			public String getPaxLimit() {
				return paxLimit;
			}

			public void setPaxLimit(String paxLimit) {
				this.paxLimit = paxLimit;
			}

			public String getTicketLimit() {
				return ticketLimit;
			}

			public void setTicketLimit(String ticketLimit) {
				this.ticketLimit = ticketLimit;
			}

			public String getMerchantSource() {
				return merchantSource;
			}

			public void setMerchantSource(String merchantSource) {
				this.merchantSource = merchantSource;
			}

			public String getMerchantDestination() {
				return merchantDestination;
			}

			public void setMerchantDestination(String merchantDestination) {
				this.merchantDestination = merchantDestination;
			}
		}
	}

	public static class Filter {

		private Metadata metadata;
		private String title;
		private String type;
		@JsonProperty("filter_param")
		private String filterParam;
		private List<Value> values = null;
		private List<Object> applied = null;

		public Metadata getMetadata() {
			return metadata;
		}

		public void setMetadata(Metadata metadata) {
			this.metadata = metadata;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getFilterParam() {
			return filterParam;
		}

		public void setFilterParam(String filterParam) {
			this.filterParam = filterParam;
		}

		public List<Value> getValues() {
			return values;
		}

		public void setValues(List<Value> values) {
			this.values = values;
		}

		public List<Object> getApplied() {
			return applied;
		}

		public void setApplied(List<Object> applied) {
			this.applied = applied;
		}

		public static class Metadata {

		}

		public static class Value {

			private String id;
			private String name;
			private Integer count;

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public Integer getCount() {
				return count;
			}

			public void setCount(Integer count) {
				this.count = count;
			}
		}
	}

	public static class Verticals {

		private Object _87;

		public Object get87() {
			return _87;
		}

		public void set87(Object _87) {
			this._87 = _87;
		}
	}

	public static class ContextParams {

		private String discoverability;

		public String getDiscoverability() {
			return discoverability;
		}

		public void setDiscoverability(String discoverability) {
			this.discoverability = discoverability;
		}
	}
}
