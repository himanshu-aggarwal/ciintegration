package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;


public class CreateDynamicQrCodeRequest extends requestPojo {

    private Request request;
    private String ipAddress;
    private String platformName;
    private String operationType;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public CreateDynamicQrCodeRequest withRequest(Request request) {
        this.request = request;
        return this;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public CreateDynamicQrCodeRequest withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public CreateDynamicQrCodeRequest withPlateformName(String plateformName) {
        this.platformName = plateformName;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public CreateDynamicQrCodeRequest withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
                .append("plateformName", platformName).append("operationType", operationType).toString();
    }

    public static class Request {
        private Integer batchCount;
        private Integer batchSize;
        private String agentPhoneNo;
        private String businessType;

        public Integer getBatchCount() {
            return batchCount;
        }

        public void setBatchCount(Integer batchCount) {
            this.batchCount = batchCount;
        }

        public Request withBatchCount(Integer batchCount) {
            this.batchCount = batchCount;
            return this;
        }

        public Integer getBatchSize() {
            return batchSize;
        }

        public void setBatchSize(Integer batchSize) {
            this.batchSize = batchSize;
        }

        public Request withBatchSize(Integer batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        public String getAgentPhoneNo() {
            return agentPhoneNo;
        }

        public void setAgentPhoneNo(String agentPhoneNo) {
            this.agentPhoneNo = agentPhoneNo;
        }

        public Request withAgentPhoneNo(String agentPhoneNo) {
            this.agentPhoneNo = agentPhoneNo;
            return this;
        }

        public String getBusinessType() {
            return businessType;
        }

        public void setBusinessType(String businessType) {
            this.businessType = businessType;
        }

        public Request withBusinessType(String businessType) {
            this.businessType = businessType;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("batchCount", batchCount).append("batchSize", batchSize)
                    .append("agentPhoneNo", agentPhoneNo).append("businessType", businessType).toString();
        }

    }
}
