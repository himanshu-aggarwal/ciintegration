package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class EditQrCodeDetailsRequest extends requestPojo {

	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public EditQrCodeDetailsRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public EditQrCodeDetailsRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public EditQrCodeDetailsRequest withPlateformName(String plateformName) {
		this.platformName = plateformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public EditQrCodeDetailsRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
				.append("platformName", platformName).append("operationType", operationType).toString();
	}

	public static class Request {

		private String qrCodeId;
		private String displayName;
		private String tagLine;
		private String posId;
		private String secondaryPhoneNumber;
		private String sourceName;
		private String sourceId;
		private String deepLink;
		private Integer activationStatus;

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}

		public Request withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public Request withDisplayName(String displayName) {
			this.displayName = displayName;
			return this;
		}

		public String getTagLine() {
			return tagLine;
		}

		public void setTagLine(String tagLine) {
			this.tagLine = tagLine;
		}

		public Request withTagLine(String tagLine) {
			this.tagLine = tagLine;
			return this;
		}

		public String getPosId() {
			return posId;
		}

		public void setPosId(String posId) {
			this.posId = posId;
		}

		public Request withPosId(String posId) {
			this.posId = posId;
			return this;
		}

		public String getSecondaryPhoneNumber() {
			return secondaryPhoneNumber;
		}

		public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
			this.secondaryPhoneNumber = secondaryPhoneNumber;
		}

		public Request withSecondaryPhoneNumber(String secondatyPhoneNumber) {
			this.secondaryPhoneNumber = secondatyPhoneNumber;
			return this;
		}

		public String getSourceName() {
			return sourceName;
		}

		public void setSourceName(String sourceName) {
			this.sourceName = sourceName;
		}

		public Request withSourceName(String sourceName) {
			this.sourceName = sourceName;
			return this;
		}

		public String getSourceId() {
			return sourceId;
		}

		public void setSourceId(String sourceId) {
			this.sourceId = sourceId;
		}

		public Request withSourceId(String sourceId) {
			this.sourceId = sourceId;
			return this;
		}

		public String getDeepLink() {
			return deepLink;
		}

		public void setDeepLink(String deepLink) {
			this.deepLink = deepLink;
		}

		public Request withDeepLink(String deepLink) {
			this.deepLink = deepLink;
			return this;
		}

		public Integer getActivationStatus() {
			return activationStatus;
		}

		public void setActivationStatus(Integer activationStatus) {
			this.activationStatus = activationStatus;
		}

		public Request withActivationStatus(Integer activationStatus) {
			this.activationStatus = activationStatus;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("qrCodeId", qrCodeId).append("displayName", displayName)
					.append("tagLine", tagLine).append("posId", posId)
					.append("secondaryPhoneNumber", secondaryPhoneNumber).append("sourceName", sourceName)
					.append("sourceId", sourceId).append("deepLink", deepLink)
					.append("activationStatus", activationStatus).toString();
		}
	}
}
