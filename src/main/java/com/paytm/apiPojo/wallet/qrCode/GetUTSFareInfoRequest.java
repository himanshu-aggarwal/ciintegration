package com.paytm.apiPojo.wallet.qrCode;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class GetUTSFareInfoRequest extends requestPojo {

	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public GetUTSFareInfoRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public GetUTSFareInfoRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public GetUTSFareInfoRequest withPlatformName(String platformName) {
		this.platformName = platformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public GetUTSFareInfoRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
				.append("plateformName", platformName).append("operationType", operationType).toString();
	}

	public static class Request {

		private String merchantGuid;
		private String industryType;
		private String routeId;
		private String routeName;
		private String sourceId;
		private String sourceName;
		private String destinationId;
		private String destinationName;
		private String busType;
		private List<String> pax = null;

		public String getMerchantGuid() {
			return merchantGuid;
		}

		public void setMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
		}

		public Request withMerchantGuid(String merchantGuid) {
			this.merchantGuid = merchantGuid;
			return this;
		}

		public String getIndustryType() {
			return industryType;
		}

		public void setIndustryType(String industryType) {
			this.industryType = industryType;
		}

		public Request withIndustryType(String industryType) {
			this.industryType = industryType;
			return this;
		}

		public String getRouteId() {
			return routeId;
		}

		public void setRouteId(String routeId) {
			this.routeId = routeId;
		}

		public Request withRouteId(String routeId) {
			this.routeId = routeId;
			return this;
		}

		public String getRouteName() {
			return routeName;
		}

		public void setRouteName(String routeName) {
			this.routeName = routeName;
		}

		public Request withRouteName(String routeName) {
			this.routeName = routeName;
			return this;
		}

		public String getSourceId() {
			return sourceId;
		}

		public void setSourceId(String sourceId) {
			this.sourceId = sourceId;
		}

		public Request withSourceId(String sourceId) {
			this.sourceId = sourceId;
			return this;
		}

		public String getSourceName() {
			return sourceName;
		}

		public void setSourceName(String sourceName) {
			this.sourceName = sourceName;
		}

		public Request withSourceName(String sourceName) {
			this.sourceName = sourceName;
			return this;
		}

		public String getDestinationId() {
			return destinationId;
		}

		public void setDestinationId(String destinationId) {
			this.destinationId = destinationId;
		}

		public Request withDestinationId(String destinationId) {
			this.destinationId = destinationId;
			return this;
		}

		public String getDestinationName() {
			return destinationName;
		}

		public void setDestinationName(String destinationName) {
			this.destinationName = destinationName;
		}

		public Request withDestinationName(String destinationName) {
			this.destinationName = destinationName;
			return this;
		}

		public String getBusType() {
			return busType;
		}

		public void setBusType(String busType) {
			this.busType = busType;
		}

		public Request withBusType(String busType) {
			this.busType = busType;
			return this;
		}

		public List<String> getPax() {
			return pax;
		}

		public void setPax(List<String> pax) {
			this.pax = pax;
		}

		public Request withPax(List<String> pax) {
			this.pax = pax;
			return this;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("merchantGuid", merchantGuid).append("industryType", industryType)
					.append("routeId", routeId).append("routeName", routeName).append("sourceId", sourceId)
					.append("sourceName", sourceName).append("destinationId", destinationId)
					.append("destinationName", destinationName).append("busType", busType).append("pax", pax)
					.toString();
		}

	}
}
