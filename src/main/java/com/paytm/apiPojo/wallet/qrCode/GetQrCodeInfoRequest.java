package com.paytm.apiPojo.wallet.qrCode;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.requestPojo;;

public class GetQrCodeInfoRequest extends requestPojo {

	private Request request;
	private String ipAddress;
	private String platformName;
	private String operationType;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public GetQrCodeInfoRequest withRequest(Request request) {
		this.request = request;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public GetQrCodeInfoRequest withIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public GetQrCodeInfoRequest withPlateformName(String plateformName) {
		this.platformName = plateformName;
		return this;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public GetQrCodeInfoRequest withOperationType(String operationType) {
		this.operationType = operationType;
		return this;
	}

	public String toString() {
		return new ToStringBuilder(this).append("request", request).append("ipAddress", ipAddress)
				.append("platformName", platformName).append("operationType", operationType).toString();
	}

	public class Request {

		private String qrCodeId;

		public String getQrCodeId() {
			return qrCodeId;
		}

		public void setQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
		}

		public Request withQrCodeId(String qrCodeId) {
			this.qrCodeId = qrCodeId;
			return this;
		}

		public String toString() {
			return new ToStringBuilder(this).append("qrCodeId", qrCodeId).toString();
		}
	}

}
