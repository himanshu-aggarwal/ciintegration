package com.paytm.apiPojo.wallet.qrCode;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import com.paytm.apiPojo.wallet.APIInterface.responsePojo;;

public class FetchQrCodeDetailsResponse extends responsePojo {

    private String requestGuid;
    private String orderId;
    private String status;
    private String statusCode;
    private String statusMessage;
    private List<FetchQrCodeDetailsResponse.Response> response = null;

    public String getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
    }

    public FetchQrCodeDetailsResponse withRequestGuid(String requestGuid) {
        this.requestGuid = requestGuid;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public FetchQrCodeDetailsResponse withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FetchQrCodeDetailsResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public FetchQrCodeDetailsResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public FetchQrCodeDetailsResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public List<FetchQrCodeDetailsResponse.Response> getResponse() {
        return response;
    }

    public void setResponse(List<FetchQrCodeDetailsResponse.Response> response) {
        this.response = response;
    }

    public FetchQrCodeDetailsResponse withResponse(List<FetchQrCodeDetailsResponse.Response> response) {
        this.response = response;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("requestGuid", requestGuid).append("orderId", orderId)
                .append("status", status).append("statusMessage", statusMessage)
                .append("response", response).toString();
    }

    public static class Response {

        private String stickerId;
        private String qrCodeId;
        private String secondaryPhoneNumber;
        private String displayName;
        private String tagline;
        private String path;
        private String posId;
        private String qrType;
        private String sourceName;
        private String sourceId;
        private String mappingId;
        private String deepLink;
        private String amount;
        private String productId;
        private String productType;
        private String productDetails;
        private String expiryDate;
        private String mobileNo;

        public String getStickerId() {
            return stickerId;
        }

        public void setStickerId(String stickerId) {
            this.stickerId = stickerId;
        }

        public FetchQrCodeDetailsResponse.Response withStickerId (String stickerId) {
            this.stickerId = stickerId;
            return this;
        }

        public String getQrCodeId() {
            return qrCodeId;
        }

        public void setQrCodeId(String qrCodeId) {
            this.qrCodeId = qrCodeId;
        }

        public FetchQrCodeDetailsResponse.Response withQrCodeId(String qrCodeId) {
            this.qrCodeId = qrCodeId;
            return this;
        }

        public String getSecondaryPhoneNumber() {
            return secondaryPhoneNumber;
        }

        public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
            this.secondaryPhoneNumber = secondaryPhoneNumber;
        }

        public FetchQrCodeDetailsResponse.Response withSecondaryPhoneNumber (String secondaryPhoneNumber) {
            this.secondaryPhoneNumber = secondaryPhoneNumber;
            return this;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public FetchQrCodeDetailsResponse.Response withDisplayName (String displayName) {
            this.displayName = displayName;
            return this;
        }

        public String getTagline() {
            return tagline;
        }

        public void setTagline(String tagline) {
            this.tagline = tagline;
        }

        public FetchQrCodeDetailsResponse.Response withTagLine (String tagLine) {
            this.tagline = tagLine;
            return this;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public FetchQrCodeDetailsResponse.Response withPath (String path) {
            this.path = path;
            return this;
        }

        public String getPosId() {
            return posId;
        }

        public void setPosId(String posId) {
            this.posId = posId;
        }

        public FetchQrCodeDetailsResponse.Response withPosId (String posId) {
            this.posId = posId;
            return this;
        }

        public String getQrType() {
            return qrType;
        }

        public void setQrType(String qrType) {
            this.qrType = qrType;
        }

        public FetchQrCodeDetailsResponse.Response withQrType (String qrType) {
            this.qrType = qrType;
            return this;
        }

        public String getSourceName() {
            return sourceName;
        }

        public void setSourceName(String sourceName) {
            this.sourceName = sourceName;
        }

        public FetchQrCodeDetailsResponse.Response withSourceName (String sourceName) {
            this.sourceName = sourceName;
            return this;
        }

        public String getSourceId() {
            return sourceId;
        }

        public void setSourceId(String sourceId) {
            this.sourceId = sourceId;
        }

        public FetchQrCodeDetailsResponse.Response withSourceId (String sourceId) {
            this.sourceId = sourceId;
            return this;
        }

        public String getMappingId() {
            return mappingId;
        }

        public void setMappingId(String mappingId) {
            this.mappingId = mappingId;
        }

        public FetchQrCodeDetailsResponse.Response withMappingId (String mappingId) {
            this.mappingId = mappingId;
            return this;
        }

        public String getDeepLink() {
            return deepLink;
        }

        public void setDeepLink(String deepLink) {
            this.deepLink = deepLink;
        }

        public FetchQrCodeDetailsResponse.Response withDeepLink (String deepLink) {
            this.deepLink = deepLink;
            return this;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public FetchQrCodeDetailsResponse.Response withAmount (String amount) {
            this.amount = amount;
            return this;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public FetchQrCodeDetailsResponse.Response withProductId (String productId) {
            this.productId = productId;
            return this;
        }

        public String getProductType() {
            return productType;
        }

        public void setProductType(String productType) {
            this.productType = productType;
        }

        public FetchQrCodeDetailsResponse.Response withProductType (String productType) {
            this.productType = productType;
            return this;
        }

        public String getProductDetails() {
            return productDetails;
        }

        public void setProductDetails(String productDetails) {
            this.productDetails = productDetails;
        }

        public FetchQrCodeDetailsResponse.Response withProductDetails (String productDetails) {
            this.productDetails = productDetails;
            return this;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public FetchQrCodeDetailsResponse.Response withExpiryDate (String expiryDate) {
            this.expiryDate = expiryDate;
            return this;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public FetchQrCodeDetailsResponse.Response withMobileNo (String mobileNo) {
            this.mobileNo = mobileNo;
            return this;
        }

        public String toString() {
            return new ToStringBuilder(this).append("stickerId", stickerId).append("qrCodeId", qrCodeId)
                    .append("secondaryPhoneNumber", secondaryPhoneNumber).append("displayName", displayName)
                    .append("tagline", tagline).append("path", path).append("posId", posId)
                    .append("qrType", qrType).append("sourceName", sourceName).append("sourceId", sourceId)
                    .append("mappingId", mappingId).append("deepLink", deepLink).append("amount", amount)
                    .append("productId", productId).append("productType", productType)
                    .append("productDetails", productDetails).append("expiryDate", expiryDate)
                    .append("mobileNo", mobileNo).toString();
        }
    }
}
