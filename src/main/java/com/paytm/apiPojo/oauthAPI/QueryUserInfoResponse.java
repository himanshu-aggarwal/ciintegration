package com.paytm.apiPojo.oauthAPI;

import java.util.List;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class QueryUserInfoResponse extends responsePojo{

	private Response response;
	private String signature;

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public static class Response {

		private Head head;
		private Body body;

		public Head getHead() {
			return head;
		}

		public void setHead(Head head) {
			this.head = head;
		}

		public Body getBody() {
			return body;
		}

		public void setBody(Body body) {
			this.body = body;
		}

		public static class Head {

			private String function;
			private String clientId;
			private String reqMsgId;
			private String version;
			private String respTime;

			public String getFunction() {
				return function;
			}

			public void setFunction(String function) {
				this.function = function;
			}

			public String getClientId() {
				return clientId;
			}

			public void setClientId(String clientId) {
				this.clientId = clientId;
			}

			public String getReqMsgId() {
				return reqMsgId;
			}

			public void setReqMsgId(String reqMsgId) {
				this.reqMsgId = reqMsgId;
			}

			public String getVersion() {
				return version;
			}

			public void setVersion(String version) {
				this.version = version;
			}

			public String getRespTime() {
				return respTime;
			}

			public void setRespTime(String respTime) {
				this.respTime = respTime;
			}

		}

		public static class Body {

			private ResultInfo resultInfo;
			private UserInfo userInfo;

			public ResultInfo getResultInfo() {
				return resultInfo;
			}

			public void setResultInfo(ResultInfo resultInfo) {
				this.resultInfo = resultInfo;
			}

			public UserInfo getUserInfo() {
				return userInfo;
			}

			public void setUserInfo(UserInfo userInfo) {
				this.userInfo = userInfo;
			}

			public static class ResultInfo {

				private String resultCodeId;
				private String resultStatus;
				private String resultMsg;
				private String resultCode;

				public String getResultCodeId() {
					return resultCodeId;
				}

				public void setResultCodeId(String resultCodeId) {
					this.resultCodeId = resultCodeId;
				}

				public String getResultStatus() {
					return resultStatus;
				}

				public void setResultStatus(String resultStatus) {
					this.resultStatus = resultStatus;
				}

				public String getResultMsg() {
					return resultMsg;
				}

				public void setResultMsg(String resultMsg) {
					this.resultMsg = resultMsg;
				}

				public String getResultCode() {
					return resultCode;
				}

				public void setResultCode(String resultCode) {
					this.resultCode = resultCode;
				}

			}

			public static class UserInfo {

				private String userStatus;
				private List<Object> mobileNoInfos = null;
				private List<Object> contactAddresses = null;
				private String updateTime;
				private String userId;
				private String loginStatus;
				private List<LoginIdInfo> loginIdInfos = null;
				private List<Object> emailInfos = null;
				private String registeredTime;
				private Boolean certified;

				public String getUserStatus() {
					return userStatus;
				}

				public void setUserStatus(String userStatus) {
					this.userStatus = userStatus;
				}

				public List<Object> getMobileNoInfos() {
					return mobileNoInfos;
				}

				public void setMobileNoInfos(List<Object> mobileNoInfos) {
					this.mobileNoInfos = mobileNoInfos;
				}

				public List<Object> getContactAddresses() {
					return contactAddresses;
				}

				public void setContactAddresses(List<Object> contactAddresses) {
					this.contactAddresses = contactAddresses;
				}

				public String getUpdateTime() {
					return updateTime;
				}

				public void setUpdateTime(String updateTime) {
					this.updateTime = updateTime;
				}

				public String getUserId() {
					return userId;
				}

				public void setUserId(String userId) {
					this.userId = userId;
				}

				public String getLoginStatus() {
					return loginStatus;
				}

				public void setLoginStatus(String loginStatus) {
					this.loginStatus = loginStatus;
				}

				public List<LoginIdInfo> getLoginIdInfos() {
					return loginIdInfos;
				}

				public void setLoginIdInfos(List<LoginIdInfo> loginIdInfos) {
					this.loginIdInfos = loginIdInfos;
				}

				public List<Object> getEmailInfos() {
					return emailInfos;
				}

				public void setEmailInfos(List<Object> emailInfos) {
					this.emailInfos = emailInfos;
				}

				public String getRegisteredTime() {
					return registeredTime;
				}

				public void setRegisteredTime(String registeredTime) {
					this.registeredTime = registeredTime;
				}

				public Boolean getCertified() {
					return certified;
				}

				public void setCertified(Boolean certified) {
					this.certified = certified;
				}

				public static class LoginIdInfo {

					private Boolean verified;
					private String loginIdType;
					private String loginId;
					private String displayLoginId;

					public Boolean getVerified() {
						return verified;
					}

					public void setVerified(Boolean verified) {
						this.verified = verified;
					}

					public String getLoginIdType() {
						return loginIdType;
					}

					public void setLoginIdType(String loginIdType) {
						this.loginIdType = loginIdType;
					}

					public String getLoginId() {
						return loginId;
					}

					public void setLoginId(String loginId) {
						this.loginId = loginId;
					}

					public String getDisplayLoginId() {
						return displayLoginId;
					}

					public void setDisplayLoginId(String displayLoginId) {
						this.displayLoginId = displayLoginId;
					}

				}

			}
		}
	}

	@Override
	public Object getStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStatusCode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStatusMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
