package com.paytm.apiPojo.oauthAPI;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class QueryUserInfoRequest extends requestPojo{

	private Request request;
	private String signature;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public class Request {

		private Body body;
		private Head head;

		public Body getBody() {
			return body;
		}

		public void setBody(Body body) {
			this.body = body;
		}

		public Head getHead() {
			return head;
		}

		public void setHead(Head head) {
			this.head = head;
		}

		public class Head {

			private String version;
			private String function;
			private String clientId;
			private String reqTime;
			private String reqMsgId;
			private String clientSecret;

			public String getVersion() {
				return version;
			}

			public void setVersion(String version) {
				this.version = version;
			}

			public String getFunction() {
				return function;
			}

			public void setFunction(String function) {
				this.function = function;
			}

			public String getClientId() {
				return clientId;
			}

			public void setClientId(String clientId) {
				this.clientId = clientId;
			}

			public String getReqTime() {
				return reqTime;
			}

			public void setReqTime(String reqTime) {
				this.reqTime = reqTime;
			}

			public String getReqMsgId() {
				return reqMsgId;
			}

			public void setReqMsgId(String reqMsgId) {
				this.reqMsgId = reqMsgId;
			}

			public String getClientSecret() {
				return clientSecret;
			}

			public void setClientSecret(String clientSecret) {
				this.clientSecret = clientSecret;
			}

		}

		public class Body {

			private LoginIdInfo loginIdInfo;
			private String userQueryType;

			public LoginIdInfo getLoginIdInfo() {
				return loginIdInfo;
			}

			public void setLoginIdInfo(LoginIdInfo loginIdInfo) {
				this.loginIdInfo = loginIdInfo;
			}

			public String getUserQueryType() {
				return userQueryType;
			}

			public void setUserQueryType(String userQueryType) {
				this.userQueryType = userQueryType;
			}

			public class LoginIdInfo {

				private String loginId;
				private String loginIdType;

				public String getLoginId() {
					return loginId;
				}

				public void setLoginId(String loginId) {
					this.loginId = loginId;
				}

				public String getLoginIdType() {
					return loginIdType;
				}

				public void setLoginIdType(String loginIdType) {
					this.loginIdType = loginIdType;
				}

			}
		}
	}

}
