package com.paytm.apiPojo.oauthAPI;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class ResetPasswordRequest extends requestPojo{

	private Request request;
	private String signature;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public class Request {

		private Body body;
		private Head head;

		public Body getBody() {
			return body;
		}

		public void setBody(Body body) {
			this.body = body;
		}

		public Head getHead() {
			return head;
		}

		public void setHead(Head head) {
			this.head = head;
		}

		public class Head {

			private String version;
			private String function;
			private String clientId;
			private String reqTime;
			private String reqMsgId;
			private String clientSecret;

			public String getVersion() {
				return version;
			}

			public void setVersion(String version) {
				this.version = version;
			}

			public String getFunction() {
				return function;
			}

			public void setFunction(String function) {
				this.function = function;
			}

			public String getClientId() {
				return clientId;
			}

			public void setClientId(String clientId) {
				this.clientId = clientId;
			}

			public String getReqTime() {
				return reqTime;
			}

			public void setReqTime(String reqTime) {
				this.reqTime = reqTime;
			}

			public String getReqMsgId() {
				return reqMsgId;
			}

			public void setReqMsgId(String reqMsgId) {
				this.reqMsgId = reqMsgId;
			}

			public String getClientSecret() {
				return clientSecret;
			}

			public void setClientSecret(String clientSecret) {
				this.clientSecret = clientSecret;
			}

		}

		public class Body {

			private String userId;
			private String newPassword;
			private String securityId;
			private EnvInfo envInfo;
			private AuditInfo auditInfo;
			private ActorContext actorContext;

			public String getUserId() {
				return userId;
			}

			public void setUserId(String userId) {
				this.userId = userId;
			}

			public String getNewPassword() {
				return newPassword;
			}

			public void setNewPassword(String newPassword) {
				this.newPassword = newPassword;
			}

			public String getSecurityId() {
				return securityId;
			}

			public void setSecurityId(String securityId) {
				this.securityId = securityId;
			}

			public EnvInfo getEnvInfo() {
				return envInfo;
			}

			public void setEnvInfo(EnvInfo envInfo) {
				this.envInfo = envInfo;
			}

			public AuditInfo getAuditInfo() {
				return auditInfo;
			}

			public void setAuditInfo(AuditInfo auditInfo) {
				this.auditInfo = auditInfo;
			}

			public ActorContext getActorContext() {
				return actorContext;
			}

			public void setActorContext(ActorContext actorContext) {
				this.actorContext = actorContext;
			}

			public class ActorContext {

				private String actorId;
				private String actorType;

				public String getActorId() {
					return actorId;
				}

				public void setActorId(String actorId) {
					this.actorId = actorId;
				}

				public String getActorType() {
					return actorType;
				}

				public void setActorType(String actorType) {
					this.actorType = actorType;
				}

			}

			public class AuditInfo {

				private String actionReason;

				public String getActionReason() {
					return actionReason;
				}

				public void setActionReason(String actionReason) {
					this.actionReason = actionReason;
				}

			}

			public class EnvInfo {

				private String terminalType;
				private String websiteLanguage;
				private String clientIp;
				private String osType;
				private String extendInfo;

				public String getTerminalType() {
					return terminalType;
				}

				public void setTerminalType(String terminalType) {
					this.terminalType = terminalType;
				}

				public String getWebsiteLanguage() {
					return websiteLanguage;
				}

				public void setWebsiteLanguage(String websiteLanguage) {
					this.websiteLanguage = websiteLanguage;
				}

				public String getClientIp() {
					return clientIp;
				}

				public void setClientIp(String clientIp) {
					this.clientIp = clientIp;
				}

				public String getOsType() {
					return osType;
				}

				public void setOsType(String osType) {
					this.osType = osType;
				}

				public String getExtendInfo() {
					return extendInfo;
				}

				public void setExtendInfo(String extendInfo) {
					this.extendInfo = extendInfo;
				}

			}
		}
	}
}
