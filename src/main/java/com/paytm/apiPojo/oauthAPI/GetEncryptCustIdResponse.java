package com.paytm.apiPojo.oauthAPI;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class GetEncryptCustIdResponse {

	@JsonProperty("userId")
	private Integer userId;
	@JsonProperty("encUserIds")
	private List<EncUserId> encUserIds = null;

	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@JsonProperty("encUserIds")
	public List<EncUserId> getEncUserIds() {
		return encUserIds;
	}

	@JsonProperty("encUserIds")
	public void setEncUserIds(List<EncUserId> encUserIds) {
		this.encUserIds = encUserIds;
	}
	
	@Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("encUserIds", encUserIds).toString();
    }

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({ "clientId", "userId" })
	public static class EncUserId {

		@JsonProperty("clientId")
		private String clientId;
		@JsonProperty("userId")
		private String userId;

		@JsonProperty("clientId")
		public String getClientId() {
			return clientId;
		}

		@JsonProperty("clientId")
		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

		@JsonProperty("userId")
		public String getUserId() {
			return userId;
		}

		@JsonProperty("userId")
		public void setUserId(String userId) {
			this.userId = userId;
		}
		
		@Override
	    public String toString() {
	        return new ToStringBuilder(this).append("clientId", clientId).append("userId", userId).toString();
	    }
	}

}
