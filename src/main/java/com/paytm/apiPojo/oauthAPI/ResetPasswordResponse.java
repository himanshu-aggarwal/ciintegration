package com.paytm.apiPojo.oauthAPI;

import com.paytm.apiPojo.wallet.APIInterface.responsePojo;

public class ResetPasswordResponse extends responsePojo {

	private Response response;
	private String signature;

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public static class Response {

		private Head head;
		private Body body;

		public Head getHead() {
			return head;
		}

		public void setHead(Head head) {
			this.head = head;
		}

		public Body getBody() {
			return body;
		}

		public void setBody(Body body) {
			this.body = body;
		}

		public static class Head {

			private String function;
			private String clientId;
			private String reqMsgId;
			private String version;
			private String respTime;

			public String getFunction() {
				return function;
			}

			public void setFunction(String function) {
				this.function = function;
			}

			public String getClientId() {
				return clientId;
			}

			public void setClientId(String clientId) {
				this.clientId = clientId;
			}

			public String getReqMsgId() {
				return reqMsgId;
			}

			public void setReqMsgId(String reqMsgId) {
				this.reqMsgId = reqMsgId;
			}

			public String getVersion() {
				return version;
			}

			public void setVersion(String version) {
				this.version = version;
			}

			public String getRespTime() {
				return respTime;
			}

			public void setRespTime(String respTime) {
				this.respTime = respTime;
			}

		}

		public static class Body {

			private ResultInfo resultInfo;

			public ResultInfo getResultInfo() {
				return resultInfo;
			}

			public void setResultInfo(ResultInfo resultInfo) {
				this.resultInfo = resultInfo;
			}

			public static class ResultInfo {

				private String resultCode;
				private String resultCodeId;
				private String resultStatus;
				private String resultMsg;

				public String getResultCode() {
					return resultCode;
				}

				public void setResultCode(String resultCode) {
					this.resultCode = resultCode;
				}

				public String getResultCodeId() {
					return resultCodeId;
				}

				public void setResultCodeId(String resultCodeId) {
					this.resultCodeId = resultCodeId;
				}

				public String getResultStatus() {
					return resultStatus;
				}

				public void setResultStatus(String resultStatus) {
					this.resultStatus = resultStatus;
				}

				public String getResultMsg() {
					return resultMsg;
				}

				public void setResultMsg(String resultMsg) {
					this.resultMsg = resultMsg;
				}

			}
		}
	}

	@Override
	public Object getStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStatusCode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStatusMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
