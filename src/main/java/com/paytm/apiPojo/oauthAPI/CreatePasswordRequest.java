package com.paytm.apiPojo.oauthAPI;

import com.paytm.apiPojo.wallet.APIInterface.requestPojo;

public class CreatePasswordRequest extends requestPojo{

	private Request request;
	private String signature;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public class Request {

		private Head head;
		private Body body;

		public Head getHead() {
			return head;
		}

		public void setHead(Head head) {
			this.head = head;
		}

		public Body getBody() {
			return body;
		}

		public void setBody(Body body) {
			this.body = body;
		}

		public class Head {

			private String function;
			private String clientId;
			private String reqTime;
			private String reqMsgId;
			private String clientSecret;
			private String accessToken;
			private String reserve;
			private String version;

			public String getFunction() {
				return function;
			}

			public void setFunction(String function) {
				this.function = function;
			}

			public String getClientId() {
				return clientId;
			}

			public void setClientId(String clientId) {
				this.clientId = clientId;
			}

			public String getReqTime() {
				return reqTime;
			}

			public void setReqTime(String reqTime) {
				this.reqTime = reqTime;
			}

			public String getReqMsgId() {
				return reqMsgId;
			}

			public void setReqMsgId(String reqMsgId) {
				this.reqMsgId = reqMsgId;
			}

			public String getClientSecret() {
				return clientSecret;
			}

			public void setClientSecret(String clientSecret) {
				this.clientSecret = clientSecret;
			}

			public String getAccessToken() {
				return accessToken;
			}

			public void setAccessToken(String accessToken) {
				this.accessToken = accessToken;
			}

			public String getReserve() {
				return reserve;
			}

			public void setReserve(String reserve) {
				this.reserve = reserve;
			}

			public String getVersion() {
				return version;
			}

			public void setVersion(String version) {
				this.version = version;
			}

		}

		public class Body {

			private String userId;
			private String password;
			private EnvInfo envInfo;
			private AuditInfo auditInfo;
			private ActorContext actorContext;

			public String getUserId() {
				return userId;
			}

			public void setUserId(String userId) {
				this.userId = userId;
			}

			public String getPassword() {
				return password;
			}

			public void setPassword(String password) {
				this.password = password;
			}

			public EnvInfo getEnvInfo() {
				return envInfo;
			}

			public void setEnvInfo(EnvInfo envInfo) {
				this.envInfo = envInfo;
			}

			public AuditInfo getAuditInfo() {
				return auditInfo;
			}

			public void setAuditInfo(AuditInfo auditInfo) {
				this.auditInfo = auditInfo;
			}

			public ActorContext getActorContext() {
				return actorContext;
			}

			public void setActorContext(ActorContext actorContext) {
				this.actorContext = actorContext;
			}

			public class AuditInfo {

				private String actionReason;
				private String thirdClientId;

				public String getActionReason() {
					return actionReason;
				}

				public void setActionReason(String actionReason) {
					this.actionReason = actionReason;
				}

				public String getThirdClientId() {
					return thirdClientId;
				}

				public void setThirdClientId(String thirdClientId) {
					this.thirdClientId = thirdClientId;
				}

			}

			public class ActorContext {

				private String actorId;
				private String actorType;

				public String getActorId() {
					return actorId;
				}

				public void setActorId(String actorId) {
					this.actorId = actorId;
				}

				public String getActorType() {
					return actorType;
				}

				public void setActorType(String actorType) {
					this.actorType = actorType;
				}

			}

			public class EnvInfo {

				private String sessionId;
				private String tokenId;
				private String websiteLanguage;
				private String clientIp;
				private String osType;
				private String orderOsType;
				private String merchantAppVersion;
				private String extendInfo;

				public String getSessionId() {
					return sessionId;
				}

				public void setSessionId(String sessionId) {
					this.sessionId = sessionId;
				}

				public String getTokenId() {
					return tokenId;
				}

				public void setTokenId(String tokenId) {
					this.tokenId = tokenId;
				}

				public String getWebsiteLanguage() {
					return websiteLanguage;
				}

				public void setWebsiteLanguage(String websiteLanguage) {
					this.websiteLanguage = websiteLanguage;
				}

				public String getClientIp() {
					return clientIp;
				}

				public void setClientIp(String clientIp) {
					this.clientIp = clientIp;
				}

				public String getOsType() {
					return osType;
				}

				public void setOsType(String osType) {
					this.osType = osType;
				}

				public String getOrderOsType() {
					return orderOsType;
				}

				public void setOrderOsType(String orderOsType) {
					this.orderOsType = orderOsType;
				}

				public String getMerchantAppVersion() {
					return merchantAppVersion;
				}

				public void setMerchantAppVersion(String merchantAppVersion) {
					this.merchantAppVersion = merchantAppVersion;
				}

				public String getExtendInfo() {
					return extendInfo;
				}

				public void setExtendInfo(String extendInfo) {
					this.extendInfo = extendInfo;
				}

			}
		}
	}
}
