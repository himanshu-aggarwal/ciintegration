package com.paytm.apiPojo.oauthAPI;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.util.List;


public class GetBeneficiaryResponse {

    @JsonProperty("data")
    private List<Data> data = null;
    @JsonProperty("totalCount")
    private Integer totalCount;

    @JsonProperty("data")
    public List<Data> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Data> data) {
        this.data = data;
    }

    public GetBeneficiaryResponse withData(List<Data> data) {
        this.data = data;
        return this;
    }

    @JsonProperty("totalCount")
    public Integer getTotalCount() {
        return totalCount;
    }

    @JsonProperty("totalCount")
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public GetBeneficiaryResponse withTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).append("totalCount", totalCount).toString();
    }


    public static class Data {

        @JsonProperty("beneficiaryId")
        private String beneficiaryId;
        @JsonProperty("nickName")
        private String nickName;
        @JsonProperty("instrumentPreferences")
        private InstrumentPreferences instrumentPreferences;

        @JsonProperty("beneficiaryId")
        public String getBeneficiaryId() {
            return beneficiaryId;
        }

        @JsonProperty("beneficiaryId")
        public void setBeneficiaryId(String beneficiaryId) {
            this.beneficiaryId = beneficiaryId;
        }

        public Data withBeneficiaryId(String beneficiaryId) {
            this.beneficiaryId = beneficiaryId;
            return this;
        }

        @JsonProperty("nickName")
        public String getNickName() {
            return nickName;
        }

        @JsonProperty("nickName")
        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public Data withNickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        @JsonProperty("instrumentPreferences")
        public InstrumentPreferences getInstrumentPreferences() {
            return instrumentPreferences;
        }

        @JsonProperty("instrumentPreferences")
        public void setInstrumentPreferences(InstrumentPreferences instrumentPreferences) {
            this.instrumentPreferences = instrumentPreferences;
        }

        public Data withInstrumentPreferences(InstrumentPreferences instrumentPreferences) {
            this.instrumentPreferences = instrumentPreferences;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("beneficiaryId", beneficiaryId).append("nickName", nickName).append("instrumentPreferences", instrumentPreferences).toString();
        }

        public static class InstrumentPreferences {

            @JsonProperty("wallet")
            private Accounts wallet;
            @JsonProperty("otherBank")
            private Accounts otherBank;

            @JsonProperty("wallet")
            public Accounts getWallet() {
                return wallet;
            }

            @JsonProperty("wallet")
            public void setWallet(Accounts wallet) {
                this.wallet = wallet;
            }

            public InstrumentPreferences withWallet(Accounts wallet) {
                this.wallet = wallet;
                return this;
            }

            @JsonProperty("otherBank")
            public Accounts getOtherBank() {
                return otherBank;
            }

            @JsonProperty("otherBank")
            public void setOtherBank(Accounts otherBank) {
                this.otherBank = otherBank;
            }

            public InstrumentPreferences withOtherBank(Accounts otherBank) {
                this.otherBank = otherBank;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("wallet", wallet).append("otherBank", otherBank).toString();
            }

            public static class Accounts {

                @JsonProperty("accounts")
                private List<Account> accounts = null;

                @JsonProperty("accounts")
                public List<Account> getAccounts() {
                    return accounts;
                }

                @JsonProperty("accounts")
                public void setAccounts(List<Account> accounts) {
                    this.accounts = accounts;
                }

                public Accounts withAccounts(List<Account> accounts) {
                    this.accounts = accounts;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("accounts", accounts).toString();
                }

                public static class Account {

                    @JsonProperty("settings")
                    private Settings settings;
                    @JsonProperty("accountDetail")
                    private AccountDetail accountDetail;
                    @JsonProperty("limits")
                    private List<Limit> limits = null;
                    @JsonProperty("uuid")
                    private String uuid;
                    @JsonProperty("creationTime")
                    private String creationTime;
                    @JsonProperty("detailHash")
                    private String detailHash;
                    @JsonProperty("source")
                    private Source source;

                    @JsonProperty("settings")
                    public Settings getSettings() {
                        return settings;
                    }

                    @JsonProperty("settings")
                    public void setSettings(Settings settings) {
                        this.settings = settings;
                    }

                    public Account withSettings(Settings settings) {
                        this.settings = settings;
                        return this;
                    }

                    @JsonProperty("accountDetail")
                    public AccountDetail getAccountDetail() {
                        return accountDetail;
                    }

                    @JsonProperty("accountDetail")
                    public void setAccountDetail(AccountDetail accountDetail) {
                        this.accountDetail = accountDetail;
                    }

                    public Account withAccountDetail(AccountDetail accountDetail) {
                        this.accountDetail = accountDetail;
                        return this;
                    }

                    @JsonProperty("limits")
                    public List<Limit> getLimits() {
                        return limits;
                    }

                    @JsonProperty("limits")
                    public void setLimits(List<Limit> limits) {
                        this.limits = limits;
                    }

                    public Account withLimits(List<Limit> limits) {
                        this.limits = limits;
                        return this;
                    }

                    @JsonProperty("uuid")
                    public String getUuid() {
                        return uuid;
                    }

                    @JsonProperty("uuid")
                    public void setUuid(String uuid) {
                        this.uuid = uuid;
                    }

                    public Account withUuid(String uuid) {
                        this.uuid = uuid;
                        return this;
                    }

                    @JsonProperty("creationTime")
                    public String getCreationTime() {
                        return creationTime;
                    }

                    @JsonProperty("creationTime")
                    public void setCreationTime(String creationTime) {
                        this.creationTime = creationTime;
                    }

                    public Account withCreationTime(String creationTime) {
                        this.creationTime = creationTime;
                        return this;
                    }

                    @JsonProperty("detailHash")
                    public String getDetailHash() {
                        return detailHash;
                    }

                    @JsonProperty("detailHash")
                    public void setDetailHash(String detailHash) {
                        this.detailHash = detailHash;
                    }

                    public Account withDetailHash(String detailHash) {
                        this.detailHash = detailHash;
                        return this;
                    }

                    @JsonProperty("source")
                    public Source getSource() {
                        return source;
                    }

                    @JsonProperty("source")
                    public void setSource(Source source) {
                        this.source = source;
                    }

                    public Account withSource(Source source) {
                        this.source = source;
                        return this;
                    }

                    @Override
                    public String toString() {
                        return new ToStringBuilder(this).append("settings", settings).append("accountDetail", accountDetail).append("limits", limits).append("uuid", uuid).append("creationTime", creationTime).append("detailHash", detailHash).append("source", source).toString();
                    }

                    public static class Settings {

                        @JsonProperty("displayOrder")
                        private String displayOrder;
                        @JsonProperty("status")
                        private String status;

                        @JsonProperty("displayOrder")
                        public String getDisplayOrder() {
                            return displayOrder;
                        }

                        @JsonProperty("displayOrder")
                        public void setDisplayOrder(String displayOrder) {
                            this.displayOrder = displayOrder;
                        }

                        public Settings withDisplayOrder(String displayOrder) {
                            this.displayOrder = displayOrder;
                            return this;
                        }

                        @JsonProperty("status")
                        public String getStatus() {
                            return status;
                        }

                        @JsonProperty("status")
                        public void setStatus(String status) {
                            this.status = status;
                        }

                        public Settings withStatus(String status) {
                            this.status = status;
                            return this;
                        }

                        @Override
                        public String toString() {
                            return new ToStringBuilder(this).append("displayOrder", displayOrder).append("status", status).toString();
                        }

                    }

                    public static class AccountDetail {

                        @JsonProperty("beneficiaryCustId")
                        private String beneficiaryCustId;
                        @JsonProperty("beneficiaryPhone")
                        private String beneficiaryPhone;
                        @JsonProperty("accountNumber")
                        private String accountNumber;
                        @JsonProperty("ifscCode")
                        private String ifscCode;
                        @JsonProperty("bankName")
                        private String bankName;
                        @JsonProperty("accountHolderName")
                        private String accountHolderName;

                        @JsonProperty("accountNumber")
                        public String getAccountNumber() {
                            return accountNumber;
                        }

                        @JsonProperty("accountNumber")
                        public void setAccountNumber(String accountNumber) {
                            this.accountNumber = accountNumber;
                        }

                        public AccountDetail withAccountNumber(String accountNumber) {
                            this.accountNumber = accountNumber;
                            return this;
                        }

                        @JsonProperty("ifscCode")
                        public String getIfscCode() {
                            return ifscCode;
                        }

                        @JsonProperty("ifscCode")
                        public void setIfscCode(String ifscCode) {
                            this.ifscCode = ifscCode;
                        }

                        public AccountDetail withIfscCode(String ifscCode) {
                            this.ifscCode = ifscCode;
                            return this;
                        }

                        @JsonProperty("bankName")
                        public String getBankName() {
                            return bankName;
                        }

                        @JsonProperty("bankName")
                        public void setBankName(String bankName) {
                            this.bankName = bankName;
                        }

                        public AccountDetail withBankName(String bankName) {
                            this.bankName = bankName;
                            return this;
                        }

                        @JsonProperty("accountHolderName")
                        public String getAccountHolderName() {
                            return accountHolderName;
                        }

                        @JsonProperty("accountHolderName")
                        public void setAccountHolderName(String accountHolderName) {
                            this.accountHolderName = accountHolderName;
                        }

                        public AccountDetail withAccountHolderName(String accountHolderName) {
                            this.accountHolderName = accountHolderName;
                            return this;
                        }

                        @JsonProperty("beneficiaryCustId")
                        public String getBeneficiaryCustId() {
                            return beneficiaryCustId;
                        }

                        @JsonProperty("beneficiaryCustId")
                        public void setBeneficiaryCustId(String beneficiaryCustId) {
                            this.beneficiaryCustId = beneficiaryCustId;
                        }

                        public AccountDetail withBeneficiaryCustId(String beneficiaryCustId) {
                            this.beneficiaryCustId = beneficiaryCustId;
                            return this;
                        }

                        @JsonProperty("beneficiaryPhone")
                        public String getBeneficiaryPhone() {
                            return beneficiaryPhone;
                        }

                        @JsonProperty("beneficiaryPhone")
                        public void setBeneficiaryPhone(String beneficiaryPhone) {
                            this.beneficiaryPhone = beneficiaryPhone;
                        }

                        public AccountDetail withBeneficiaryPhone(String beneficiaryPhone) {
                            this.beneficiaryPhone = beneficiaryPhone;
                            return this;
                        }

                        @Override
                        public String toString() {
                            return new ToStringBuilder(this).append("beneficiaryCustId", beneficiaryCustId).append("beneficiaryPhone", beneficiaryPhone).append("accountNumber", accountNumber).append("ifscCode", ifscCode).append("bankName", bankName).append("accountHolderName", accountHolderName).toString();
                        }

                    }

                    public static class Limit {

                        @JsonProperty("ruleId")
                        private String ruleId;
                        @JsonProperty("ruleParams")
                        private RuleParams ruleParams;

                        @JsonProperty("ruleId")
                        public String getRuleId() {
                            return ruleId;
                        }

                        @JsonProperty("ruleId")
                        public void setRuleId(String ruleId) {
                            this.ruleId = ruleId;
                        }

                        public Limit withRuleId(String ruleId) {
                            this.ruleId = ruleId;
                            return this;
                        }

                        @JsonProperty("ruleParams")
                        public RuleParams getRuleParams() {
                            return ruleParams;
                        }

                        @JsonProperty("ruleParams")
                        public void setRuleParams(RuleParams ruleParams) {
                            this.ruleParams = ruleParams;
                        }

                        public Limit withRuleParams(RuleParams ruleParams) {
                            this.ruleParams = ruleParams;
                            return this;
                        }

                        @Override
                        public String toString() {
                            return new ToStringBuilder(this).append("ruleId", ruleId).append("ruleParams", ruleParams).toString();
                        }

                        public static class RuleParams {

                            @JsonProperty("duration")
                            private String duration;
                            @JsonProperty("amount")
                            private String amount;
                            @JsonProperty("durationUnit")
                            private String durationUnit;
                            @JsonProperty("txn")
                            private String txn;

                            @JsonProperty("duration")
                            public String getDuration() {
                                return duration;
                            }

                            @JsonProperty("duration")
                            public void setDuration(String duration) {
                                this.duration = duration;
                            }

                            public RuleParams withDuration(String duration) {
                                this.duration = duration;
                                return this;
                            }

                            @JsonProperty("amount")
                            public String getAmount() {
                                return amount;
                            }

                            @JsonProperty("amount")
                            public void setAmount(String amount) {
                                this.amount = amount;
                            }

                            public RuleParams withAmount(String amount) {
                                this.amount = amount;
                                return this;
                            }

                            @JsonProperty("durationUnit")
                            public String getDurationUnit() {
                                return durationUnit;
                            }

                            @JsonProperty("durationUnit")
                            public void setDurationUnit(String durationUnit) {
                                this.durationUnit = durationUnit;
                            }

                            public RuleParams withDurationUnit(String durationUnit) {
                                this.durationUnit = durationUnit;
                                return this;
                            }

                            @JsonProperty("txn")
                            public String getTxn() {
                                return txn;
                            }

                            @JsonProperty("txn")
                            public void setTxn(String txn) {
                                this.txn = txn;
                            }

                            public RuleParams withTxn(String txn) {
                                this.txn = txn;
                                return this;
                            }

                            @Override
                            public String toString() {
                                return new ToStringBuilder(this).append("duration", duration).append("amount", amount).append("durationUnit", durationUnit).append("txn", txn).toString();
                            }

                        }

                    }

                    public static class Source {

                        @JsonProperty("oba")
                        private String oba;

                        @JsonProperty("oba")
                        public String getOba() {
                            return oba;
                        }

                        @JsonProperty("oba")
                        public void setOba(String oba) {
                            this.oba = oba;
                        }

                        public Source withOba(String oba) {
                            this.oba = oba;
                            return this;
                        }

                        @Override
                        public String toString() {
                            return new ToStringBuilder(this).append("oba", oba).toString();
                        }

                    }

                }

            }


        }

    }


}
