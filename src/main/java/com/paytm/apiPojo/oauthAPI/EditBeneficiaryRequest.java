package com.paytm.apiPojo.oauthAPI;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.util.List;


public class EditBeneficiaryRequest {

    @JsonProperty("beneficiaryId")
    private String beneficiaryId;
    @JsonProperty("instrumentPreferences")
    private InstrumentPreferences instrumentPreferences;

    @JsonProperty("beneficiaryId")
    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    @JsonProperty("beneficiaryId")
    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public EditBeneficiaryRequest withBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
        return this;
    }

    @JsonProperty("instrumentPreferences")
    public InstrumentPreferences getInstrumentPreferences() {
        return instrumentPreferences;
    }

    @JsonProperty("instrumentPreferences")
    public void setInstrumentPreferences(InstrumentPreferences instrumentPreferences) {
        this.instrumentPreferences = instrumentPreferences;
    }

    public EditBeneficiaryRequest withInstrumentPreferences(InstrumentPreferences instrumentPreferences) {
        this.instrumentPreferences = instrumentPreferences;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("beneficiaryId", beneficiaryId).append("instrumentPreferences", instrumentPreferences).toString();
    }

    public static class InstrumentPreferences {

        @JsonProperty("wallet")
        private Accounts wallet;
        @JsonProperty("otherBank")
        private Accounts otherBank;

        @JsonProperty("wallet")
        public Accounts getWallet() {
            return wallet;
        }

        @JsonProperty("wallet")
        public void setWallet(Accounts wallet) {
            this.wallet = wallet;
        }

        public InstrumentPreferences withWallet(Accounts wallet) {
            this.wallet = wallet;
            return this;
        }

        @JsonProperty("otherBank")
        public Accounts getOtherBank() {
            return otherBank;
        }

        @JsonProperty("otherBank")
        public void setOtherBank(Accounts otherBank) {
            this.otherBank = otherBank;
        }

        public InstrumentPreferences withOtherBank(Accounts otherBank) {
            this.otherBank = otherBank;
            return this;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("wallet", wallet).append("otherBank", otherBank).toString();
        }

        public static class Accounts {

            @JsonProperty("accounts")
            private List<Account> accounts = null;

            @JsonProperty("accounts")
            public List<Account> getAccounts() {
                return accounts;
            }

            @JsonProperty("accounts")
            public void setAccounts(List<Account> accounts) {
                this.accounts = accounts;
            }

            public Accounts withAccounts(List<Account> accounts) {
                this.accounts = accounts;
                return this;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("accounts", accounts).toString();
            }

            public static class Account {

                @JsonProperty("uuid")
                private String uuid;
                @JsonProperty("accountDetail")
                private AccountDetail accountDetail;
                @JsonProperty("limits")
                private List<Limit> limits = null;
                @JsonProperty("source")
                private Source source;

                @JsonProperty("source")
                public Source getSource() {
                    return source;
                }

                @JsonProperty("source")
                public void setSource(Source source) {
                    this.source = source;
                }

                public Account withSource(Source source) {
                    this.source = source;
                    return this;
                }

                @JsonProperty("uuid")
                public String getUuid() {
                    return uuid;
                }

                @JsonProperty("uuid")
                public void setUuid(String uuid) {
                    this.uuid = uuid;
                }

                public Account withUuid(String uuid) {
                    this.uuid = uuid;
                    return this;
                }

                @JsonProperty("accountDetail")
                public AccountDetail getAccountDetail() {
                    return accountDetail;
                }

                @JsonProperty("accountDetail")
                public void setAccountDetail(AccountDetail accountDetail) {
                    this.accountDetail = accountDetail;
                }

                public Account withAccountDetail(AccountDetail accountDetail) {
                    this.accountDetail = accountDetail;
                    return this;
                }

                @JsonProperty("limits")
                public List<Limit> getLimits() {
                    return limits;
                }

                @JsonProperty("limits")
                public void setLimits(List<Limit> limits) {
                    this.limits = limits;
                }

                public Account withLimits(List<Limit> limits) {
                    this.limits = limits;
                    return this;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("uuid", uuid).append("accountDetail", accountDetail).append("limits", limits).append("source", source).toString();
                }

                public static class AccountDetail {

                    @JsonProperty("accountNumber")
                    private String accountNumber;
                    @JsonProperty("ifscCode")
                    private String ifscCode;
                    @JsonProperty("bankName")
                    private String bankName;
                    @JsonProperty("accountHolderName")
                    private String accountHolderName;

                    @JsonProperty("accountNumber")
                    public String getAccountNumber() {
                        return accountNumber;
                    }

                    @JsonProperty("accountNumber")
                    public void setAccountNumber(String accountNumber) {
                        this.accountNumber = accountNumber;
                    }

                    public AccountDetail withAccountNumber(String accountNumber) {
                        this.accountNumber = accountNumber;
                        return this;
                    }

                    @JsonProperty("ifscCode")
                    public String getIfscCode() {
                        return ifscCode;
                    }

                    @JsonProperty("ifscCode")
                    public void setIfscCode(String ifscCode) {
                        this.ifscCode = ifscCode;
                    }

                    public AccountDetail withIfscCode(String ifscCode) {
                        this.ifscCode = ifscCode;
                        return this;
                    }

                    @JsonProperty("bankName")
                    public String getBankName() {
                        return bankName;
                    }

                    @JsonProperty("bankName")
                    public void setBankName(String bankName) {
                        this.bankName = bankName;
                    }

                    public AccountDetail withBankName(String bankName) {
                        this.bankName = bankName;
                        return this;
                    }

                    @JsonProperty("accountHolderName")
                    public String getAccountHolderName() {
                        return accountHolderName;
                    }

                    @JsonProperty("accountHolderName")
                    public void setAccountHolderName(String accountHolderName) {
                        this.accountHolderName = accountHolderName;
                    }

                    public AccountDetail withAccountHolderName(String accountHolderName) {
                        this.accountHolderName = accountHolderName;
                        return this;
                    }

                    @Override
                    public String toString() {
                        return new ToStringBuilder(this).append("accountNumber", accountNumber).append("ifscCode", ifscCode).append("bankName", bankName).append("accountHolderName", accountHolderName).toString();
                    }

                }

                public static class Limit {

                    @JsonProperty("ruleId")
                    private String ruleId;
                    @JsonProperty("ruleParams")
                    private RuleParams ruleParams;

                    @JsonProperty("ruleId")
                    public String getRuleId() {
                        return ruleId;
                    }

                    @JsonProperty("ruleId")
                    public void setRuleId(String ruleId) {
                        this.ruleId = ruleId;
                    }

                    public Limit withRuleId(String ruleId) {
                        this.ruleId = ruleId;
                        return this;
                    }

                    @JsonProperty("ruleParams")
                    public RuleParams getRuleParams() {
                        return ruleParams;
                    }

                    @JsonProperty("ruleParams")
                    public void setRuleParams(RuleParams ruleParams) {
                        this.ruleParams = ruleParams;
                    }

                    public Limit withRuleParams(RuleParams ruleParams) {
                        this.ruleParams = ruleParams;
                        return this;
                    }

                    @Override
                    public String toString() {
                        return new ToStringBuilder(this).append("ruleId", ruleId).append("ruleParams", ruleParams).toString();
                    }

                    public static class RuleParams {

                        @JsonProperty("amount")
                        private String amount;
                        @JsonProperty("source")
                        private String source;
                        @JsonProperty("duration")
                        private String duration;
                        @JsonProperty("durationUnit")
                        private String durationUnit;
                        @JsonProperty("txn")
                        private String txn;

                        @JsonProperty("duration")
                        public String getDuration() {
                            return duration;
                        }

                        @JsonProperty("duration")
                        public void setDuration(String duration) {
                            this.duration = duration;
                        }

                        public RuleParams withDuration(String duration) {
                            this.duration = duration;
                            return this;
                        }

                        @JsonProperty("durationUnit")
                        public String getDurationUnit() {
                            return durationUnit;
                        }

                        @JsonProperty("durationUnit")
                        public void setDurationUnit(String durationUnit) {
                            this.durationUnit = durationUnit;
                        }

                        public RuleParams withDurationUnit(String durationUnit) {
                            this.durationUnit = durationUnit;
                            return this;
                        }

                        @JsonProperty("txn")
                        public String getTxn() {
                            return txn;
                        }

                        @JsonProperty("txn")
                        public void setTxn(String txn) {
                            this.txn = txn;
                        }

                        public RuleParams withTxn(String txn) {
                            this.txn = txn;
                            return this;
                        }

                        @JsonProperty("amount")
                        public String getAmount() {
                            return amount;
                        }

                        @JsonProperty("amount")
                        public void setAmount(String amount) {
                            this.amount = amount;
                        }

                        public RuleParams withAmount(String amount) {
                            this.amount = amount;
                            return this;
                        }

                        @JsonProperty("source")
                        public String getSource() {
                            return source;
                        }

                        @JsonProperty("source")
                        public void setSource(String source) {
                            this.source = source;
                        }

                        public RuleParams withSource(String source) {
                            this.source = source;
                            return this;
                        }

                        @Override
                        public String toString() {
                            return new ToStringBuilder(this).append("amount", amount).append("source", source).append("duration", duration).append("durationUnit", durationUnit).append("txn", txn).toString();
                        }

                    }

                }

                public static class Source {

                    @JsonProperty("wallet")
                    private String wallet;

                    @JsonProperty("wallet")
                    public String getWallet() {
                        return wallet;
                    }

                    @JsonProperty("wallet")
                    public void setWallet(String wallet) {
                        this.wallet = wallet;
                    }

                    public Source withWallet(String wallet) {
                        this.wallet = wallet;
                        return this;
                    }

                    @Override
                    public String toString() {
                        return new ToStringBuilder(this).append("wallet", wallet).toString();
                    }

                }

            }

        }

    }

}

