package com.paytm.apiPojo;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

public class SubwalletIssuerMerchantMapping {

	@JsonProperty("mid_list")
	private List<String> midList = null;
	@JsonProperty("category_list")
	private List<Object> categoryList = null;
	private SubwalletIssuerMerchantMapping issuerMapping;

	public SubwalletIssuerMerchantMapping test(String json) throws IOException {
		return issuerMapping = JacksonJsonImpl.getInstance().fromJson(json, SubwalletIssuerMerchantMapping.class);
	}

	public List<String> getMidList() {
		return midList;
	}

	public void setMidList(List<String> midList) {
		this.midList = midList;
	}

	public SubwalletIssuerMerchantMapping withMidList(List<String> midList) {
		this.midList = midList;
		return this;
	}

	public List<Object> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Object> categoryList) {
		this.categoryList = categoryList;
	}

	public SubwalletIssuerMerchantMapping withCategoryList(List<Object> categoryList) {
		this.categoryList = categoryList;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("midList", midList).append("categoryList", categoryList).toString();
	}

	public static void main(String[] args) throws IOException {
		String json = "{\"mid_list\":[\"100289\",\"1107223253\"],\"category_list\":[\"restaurant\"]}";
		SubwalletIssuerMerchantMapping mapping = new SubwalletIssuerMerchantMapping();
		SubwalletIssuerMerchantMapping object = mapping.test(json);


	}

}
