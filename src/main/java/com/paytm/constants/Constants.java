package com.paytm.constants;

public class Constants {

	public static class SystemVariables {
		public static final String TESTNG_RUNMODE = System.getProperty("testngRunMode", "sanity");
		public static final String TESTNG_RUNAPIS = System.getProperty("testngRunApis", "ALL");
		public static final String TESTNG_RUNMODULE = System.getProperty("testngRunModule", "ALL");
		public static final String TESTING_MOCK = System.getProperty("testngMock","false");
		public static final String TESTNG_PARALLELRUNMODE = System.getProperty("testngParallelRunMode", "NONE");
		public static final String TESTNG_PARALLELTHREADCOUNT = System.getProperty("testngParallelThreadCount", "1");
		public static final Boolean NEWQRMODULE = Boolean.parseBoolean(System.getProperty("newQRModule","true"));
		public static final String CLIENTID = "a5516f104428408fb6051f833c9bb9e0";
		public static final String HASH = "d67d25073a05b3b47cfdc5e16f78dea39cee9d57c6a7a523321b3dd6dc975f94";
		public static final Boolean JENKINSRUN = Boolean.parseBoolean(System.getProperty("JenkinsRun", "false"));
		public static final Boolean testCaseListRequired = true;
		public static final int waitTimeInMinutes = 30;
		public static final int waitTimeCounter = 2000;
		public static final int seviceTaxPercentage = 18;
	}

	public static class DBConnectionURL {
		public static final String PGP_DB_CONNECTION_URL = LocalConfig.PGP_DB_CONNECTION_URL;
		public static final String AUTH_DB_CONNECTION_URL = LocalConfig.AUTH_DB_CONNECTION_URL;
		public static final String WALLET_DB_CONNECTION_URL = LocalConfig.WALLET_DB_CONNECTION_URL;
		public static final String WALLET_TRAIL_DB_CONNECTION_URL = LocalConfig.WALLET_TRAIL_DB_CONNECTION_URL;
		public static final String WALLET_ELASTIC_CONNECTION_URL = LocalConfig.WALLET_ES_HOST_CONNECTION_URL;
	}

	public static class AuthAPIresource {
		public static final String OAUTH2 = "/oauth2";
		public static final String AUTHORIZE = "/oauth2/authorize";
		public static final String TOKEN = "/oauth2/token";
		public static final String LOGOUT = "/oauth2/usertokens";
		public static final String V3REGISTER = "/v3/api/register";
		public static final String V3VALIDATE = "/v3/api/register/validate";
		public static final String CHECKUSEREXISTANCE = "/api/verifyAvailability/phone";
		public static final String OAUTH2USERTOKENS = "oauth2/usertokens";
		public static final String LOGINOTP = "/login/otp";
		public static final String LOGINVALIDATEOTP = "/login/validate/otp";
		public static final String OFFLINECONFIG = "/offline/config";
		public static final String DEVICEVERIFIED = "/user/device/status";
		public static final String V2USERID = "/v2/user";
		public static final String TOKENSTATUS = "/user";
		public static final String QUERYUSERINFO = "/alipayplus/user/profile/queryUserInfo.htm";
		public static final String RESETPASSWORD = "/alipayplus/user/security/resetPassword.htm";
		public static final String CREATEPASSWORD = "/alipayplus/user/security/createPassword.htm";
		public static final String GETBENEFICIARY = "/v1/getbeneficiary";
	}

	public static class WALLETAPIresource {
		public static final String CHECKUSERBALANCE = "/service/checkUserBalance";
		public static final String FUNDCHECKBALANCE = "/service/funds/checkbalance";
		public static final String CHECKBALANCE = "/wallet-web/checkBalance";
		public static final String CACHEREFRESH = "/wallet-web/cacheRefresh";
		public static final String ADDMONEY = "/wallet-web/AddMoney";
		public static final String TRANSFERTOBANK = "/wallet-web/transferToBank";
		public static final String WITHDRAWMONEY = "/wallet-web/withdraw";
		public static final String UPGRADEWALLET = "/wallet-web/upgradeWallet";
		public static final String V2WITHDRAWMONEY = "/wallet-web/v2/withdraw";
		public static final String V5WITHDRAWMONEY = "/wallet-web/v5/withdraw";
		public static final String V6WITHDRAWMONEY = "/wallet-web/v6/withdraw";
		public static final String V7WITHDRAWMONEY = "/wallet-web/v7/withdraw";
		public static final String V9WITHDRAWMONEY = "/wallet-web/v9/withdraw";
		public static final String V4WithDrawMoney = "/wallet-web/v4/withdraw";
		public static final String V2VALIDATETRANSACTION = "/wallet-web/v2/validateTransaction";
		public static final String V5VALIDATETRANSACTION = "/wallet-web/v5/validateTransaction";
		public static final String V4VALIDATETRANSACTION = "/wallet-web/v4/validateTransaction";
		public static final String ADDFUNDSTOSUBWALLET = "/wallet-web/addFundsToSubWallet";
		public static final String VALIDATEOTP = "/wallet-web/validateTransaction";
		public static final String SENDMONEY = "/wallet-web/sendMoney";
		public static final String WALLETREFUND = "/wallet-web/refundWalletTxn";
		public static final String CREATELIFAFA = "/wallet-web/lms/createLifafa";
		public static final String CLAIMLIFAFA = "/wallet-web/lms/claimLifafa";
		public static final String PREAUTHNRELEASE = "/wallet-web/preAuthNRelease";
		public static final String SETUSERDEFINEDLIMIT = "/wallet-web/userDefinedLimit/set";
		public static final String FETCHUSERDEFINEDLIMIT = "/wallet-web/userDefinedLimit/fetch";
		public static final String MOVEBACKFUNDSTOISSUER = "/wallet-web/moveBackFundsToIssuer";
		public static final String WALLETLIMITS = "/wallet-web/walletLimits";
		public static final String RESENDOTP="/wallet-web/resendOtp";
		public static final String V4RESENDOTP = "/wallet-web/v4/resendOtp";
		public static final String CREATEDYNAMICQRCODE = SystemVariables.NEWQRMODULE ? "/qrcode/v4/createDynamicQRCode": "/wallet-merchant/v4/createDynamicQRCode";
		public static final String FETCHQRCODEDETAILS = SystemVariables.NEWQRMODULE ? "/qrcode/v4/fetchQrCodeDetails" : "/wallet-merchant/v4/fetchQrCodeDetails";
		public static final String GETQRCODEINFO = SystemVariables.NEWQRMODULE ? "/qrcode/getQRCodeInfo" : "/service/getQRCodeInfo";
		public static final String MAPDYNAMICQRCODE = SystemVariables.NEWQRMODULE ? "/qrcode/v4/mapDynamicQRCode" : "/wallet-merchant/v4/mapDynamicQRCode";
		public static final String EDITQRCODEDETAILS = SystemVariables.NEWQRMODULE ? "/qrcode/v4/editQrCodeDetails" : "/wallet-merchant/v4/editQrCodeDetails";
		public static final String ASSIGNQRCODE = SystemVariables.NEWQRMODULE ? "/qrcode/v4/assignQrCode" : "/wallet-merchant/v4/assignQrCode";
		public static final String GENERATEQRCODE = SystemVariables.NEWQRMODULE ? "/qrcode/v4/generateQrCode" : "/wallet-merchant/v4/generateQrCode";
		public static final String CREATEQRCODE = SystemVariables.NEWQRMODULE ? "/qrcode/v4/createQRCode" : "/wallet-merchant/v4/createQRCode";
		public static final String V2CREATEQRCODE = SystemVariables.NEWQRMODULE ? "/qrcode/v2/createQRCode" : "/wallet-merchant/v2/createQRCode";
		public static final String MERCHANTQRCODE = SystemVariables.NEWQRMODULE ? "/qrcode/v9/merchantQrCode" : "/wallet-merchant/v9/merchantQrCode";
		public static final String GETUTSFAREINFO = SystemVariables.NEWQRMODULE ? "/qrcode/getUTSFareInfo" : "/service/getUTSFareInfo";
		public static final String GETMERCHANTINFO = SystemVariables.NEWQRMODULE ? "/qrcode/v4/getMerchantInfo" : "/wallet-merchant/v4/getMerchantInfo";
		public static final String FETCHQRMAPPINGDETAIL = SystemVariables.NEWQRMODULE ? "/qrcode/v4/fetchQrMappingDetail" : "/wallet-merchant/v4/fetchQrMappingDetail";
		public static final String MAPDEEPLINKTOQR = SystemVariables.NEWQRMODULE ? "/qrcode/v4/mapDeeplinkToQr" : "/wallet-merchant/v4/mapDeeplinkToQr";
		public static final String QRWITHDRAW="/wallet-web/qrWithdraw";
	}

}
