package com.paytm.constants;

import com.paytm.enums.WalletRbiTypes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface UserTypeExt {

	WalletRbiTypes UserNameProvided();

}
