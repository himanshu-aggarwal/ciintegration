package com.paytm.constants;

import com.paytm.framework.utils.PropertyUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LocalConfig {

	public static final String WALLET_HOST;
	public static final String WALLET_OLD_SERVICE_HOST;
	public static final String WALLET_MERCHANT_HOST;
	public static final String QR_CODE_HOST;
	public static final String PGP_HOST;
	public static final String AUTH_HOST;
	public static final String ACQUIRING_HOST_URL;
	public static final String PGP_DB_CONNECTION_URL;
	public static final String AUTH_DB_CONNECTION_URL;
	public static final String WALLET_DB_CONNECTION_URL;
	public static final String WALLET_TRAIL_DB_CONNECTION_URL;
	public static final String PGP_RESP_HOST;
	public static final String WALLET_ES_HOST_CONNECTION_URL;
	public static final String NOTIFICATION_CONNECTION_URL;
	public static final String AUTH_CLIENT_NAME;
	public static final String AUTH_CLIENT_SECRET;
	public static final String AUTH_CLIENT_AUTHENTICATION;
	public static final String READ_OTP_FROM_LOG;
	public static final String MERCHANT_GUID;
	public static final String EXECUTE_BEFORE_SUITE;
	public static final String MERCHANT_GUID_FOR_PUSH_NOTIFICATION;
	public static final String AUTH_USER_PASSWORD;
	public static final String WALLET_REDIS_URL;
	public static final String ALIPAY_OAUTH_URL;
	public static String FOOD;
	public static String GIFT;
	public static String TOLL;
	public static String INTERNATIONAL_FUNDS_TRANSFER;
	public static String CLOSED_LOOP_WALLET;
	public static String CLOSED_LOOP_SUB_WALLET;
	public static String BLOCKED;
	public static String FUEL;
	public static String GIFT_VOUCHER;
	public static String CASHBACK;
	public static String MAIN;
	public static String FETCH_OTP_LOGS;
	public static String AUTH_ROUTER_DB_CONNECTION_URL;
	public static String MERCHANT_CALLBACK_LOG_URL;
	public static Map<String, String> subWalletMapping = new HashMap<String, String>();

	static {
		try {
			PropertyUtil.getInstance().load("config.properties");
			WALLET_HOST = PropertyUtil.getInstance().getValue("WALLET_HOST");
			WALLET_OLD_SERVICE_HOST = PropertyUtil.getInstance().getValue("WALLET_OLD_SERVICE_HOST");
			WALLET_MERCHANT_HOST = PropertyUtil.getInstance().getValue("WALLET_MERCHANT_HOST");
			QR_CODE_HOST = PropertyUtil.getInstance().getValue("QR_CODE_HOST");
			AUTH_HOST = PropertyUtil.getInstance().getValue("AUTH_HOST");
			PGP_HOST = PropertyUtil.getInstance().getValue("PGP_HOST");
			PGP_DB_CONNECTION_URL = PropertyUtil.getInstance().getValue("PGP_DB_CONNECTION_URL");
			AUTH_DB_CONNECTION_URL = PropertyUtil.getInstance().getValue("AUTH_DB_CONNECTION_URL");
			WALLET_DB_CONNECTION_URL = PropertyUtil.getInstance().getValue("WALLET_DB_CONNECTION_URL");
			WALLET_TRAIL_DB_CONNECTION_URL = PropertyUtil.getInstance().getValue("WALLET_TRAIL_DB_CONNECTION_URL");
			PGP_RESP_HOST = PropertyUtil.getInstance().getValue("PGP_RESP_HOST");
			WALLET_ES_HOST_CONNECTION_URL = PropertyUtil.getInstance().getValue("WALLET_ES_HOST_CONNECTION_URL");
			ACQUIRING_HOST_URL = PropertyUtil.getInstance().getValue("ACQUIRING_HOST");
			NOTIFICATION_CONNECTION_URL = PropertyUtil.getInstance().getValue("NOTIFICATION_CONNECTION_URL");
			AUTH_CLIENT_NAME = PropertyUtil.getInstance().getValue("AUTH_CLIENT_NAME");
			AUTH_CLIENT_SECRET = PropertyUtil.getInstance().getValue("AUTH_CLIENT_SECRET");
			AUTH_CLIENT_AUTHENTICATION = PropertyUtil.getInstance().getValue("AUTH_CLIENT_AUTHENTICATION");
			READ_OTP_FROM_LOG = PropertyUtil.getInstance().getValue("READ_OTP_FROM_LOG");
			MERCHANT_GUID = PropertyUtil.getInstance().getValue("MERCHANT_GUID");
			EXECUTE_BEFORE_SUITE = PropertyUtil.getInstance().getValue("EXECUTE_BEFORE_SUITE");
			MERCHANT_GUID_FOR_PUSH_NOTIFICATION = PropertyUtil.getInstance()
					.getValue("MERCHANT_GUID_FOR_PUSH_NOTIFICATION");
			AUTH_USER_PASSWORD = PropertyUtil.getInstance().getValue("AUTH_USER_PASSWORD");
			FETCH_OTP_LOGS = PropertyUtil.getInstance().getValue("Fetch_OTPFromLogs");
			AUTH_ROUTER_DB_CONNECTION_URL = PropertyUtil.getInstance().getValue("AUTH_ROUTER_DB_CONNECTION_URL");
			WALLET_REDIS_URL = PropertyUtil.getInstance().getValue("WALLET_REDIS_URL");
			MERCHANT_CALLBACK_LOG_URL = PropertyUtil.getInstance().getValue("MERCHANT_CALLBACK_LOG_URL");
			ALIPAY_OAUTH_URL = PropertyUtil.getInstance().getValue("ALIPAY_OAUTH_URL");
			getSubWalletMapping();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException("Something wrong !!! Check configurations.", e);
		}
	}

	public static void getSubWalletMapping() throws IOException {
		PropertyUtil.getInstance().load("subWalletMapping.properties");
		FOOD = PropertyUtil.getInstance().getValue("FOOD");
		GIFT = PropertyUtil.getInstance().getValue("GIFT");
		BLOCKED = PropertyUtil.getInstance().getValue("BLOCKED");
		CLOSED_LOOP_WALLET = PropertyUtil.getInstance().getValue("CLOSED_LOOP_WALLET");
		CLOSED_LOOP_SUB_WALLET = PropertyUtil.getInstance().getValue("CLOSED_LOOP_SUB_WALLET");
		FUEL = PropertyUtil.getInstance().getValue("FUEL");
		INTERNATIONAL_FUNDS_TRANSFER = PropertyUtil.getInstance().getValue("INTERNATIONAL_FUNDS_TRANSFER");
		CASHBACK = PropertyUtil.getInstance().getValue("CASHBACK");
		GIFT_VOUCHER = PropertyUtil.getInstance().getValue("GIFT_VOUCHER");
		TOLL = PropertyUtil.getInstance().getValue("TOLL");
		MAIN = PropertyUtil.getInstance().getValue("MAIN");
		subWalletMapping.put("FOOD", FOOD);
		subWalletMapping.put("GIFT", GIFT);
		subWalletMapping.put("BLOCKED", BLOCKED);
		subWalletMapping.put("CLOSED_LOOP_WALLET", CLOSED_LOOP_WALLET);
		subWalletMapping.put("CLOSED_LOOP_SUB_WALLET", CLOSED_LOOP_SUB_WALLET);
		subWalletMapping.put("FUEL", FUEL);
		subWalletMapping.put("INTERNATIONAL_FUNDS_TRANSFER", INTERNATIONAL_FUNDS_TRANSFER);
		subWalletMapping.put("CASHBACK", CASHBACK);
		subWalletMapping.put("GIFT_VOUCHER", GIFT_VOUCHER);
		subWalletMapping.put("TOLL", TOLL);
		subWalletMapping.put("MAIN", MAIN);
	}

}
