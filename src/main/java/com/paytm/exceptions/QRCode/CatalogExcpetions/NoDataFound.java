package com.paytm.exceptions.QRCode.CatalogExcpetions;

public class NoDataFound extends RuntimeException {

    public NoDataFound() {
    }

    private NoDataFound(String var1) {
        super(var1);
    }

    public NoDataFound(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public NoDataFound(boolean var1) {
        this(String.valueOf(var1));
    }

    public NoDataFound(char var1) {
        this(String.valueOf(var1));
    }

    public NoDataFound(int var1) {
        this(String.valueOf(var1));
    }

    public NoDataFound(long var1) {
        this(String.valueOf(var1));
    }

    public NoDataFound(float var1) {
        this(String.valueOf(var1));
    }

    public NoDataFound(double var1) {
        this(String.valueOf(var1));
    }

    public NoDataFound(String var1, Throwable var2) {
        super(var1, var2);
    }


}
