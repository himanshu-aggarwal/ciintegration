package com.paytm.exceptions.LimitsException.BeneficiaryLimits;

public class BeneficiaryLimitValidationException extends RuntimeException {

    public BeneficiaryLimitValidationException() {
    }

    private BeneficiaryLimitValidationException(String var1) {
        super(var1);
    }

    public BeneficiaryLimitValidationException(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public BeneficiaryLimitValidationException(boolean var1) {
        this(String.valueOf(var1));
    }

    public BeneficiaryLimitValidationException(char var1) {
        this(String.valueOf(var1));
    }

    public BeneficiaryLimitValidationException(int var1) {
        this(String.valueOf(var1));
    }

    public BeneficiaryLimitValidationException(long var1) {
        this(String.valueOf(var1));
    }

    public BeneficiaryLimitValidationException(float var1) {
        this(String.valueOf(var1));
    }

    public BeneficiaryLimitValidationException(double var1) {
        this(String.valueOf(var1));
    }

    public BeneficiaryLimitValidationException(String var1, Throwable var2) {
        super(var1, var2);
    }


}
