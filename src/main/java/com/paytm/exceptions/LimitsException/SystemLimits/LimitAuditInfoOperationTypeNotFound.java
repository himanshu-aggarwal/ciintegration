package com.paytm.exceptions.LimitsException.SystemLimits;


public class LimitAuditInfoOperationTypeNotFound extends RuntimeException {

    public LimitAuditInfoOperationTypeNotFound() {
    }

    private LimitAuditInfoOperationTypeNotFound(String var1) {
        super(var1);
    }

    public LimitAuditInfoOperationTypeNotFound(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable) var1);
        }

    }

    public LimitAuditInfoOperationTypeNotFound(boolean var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoOperationTypeNotFound(char var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoOperationTypeNotFound(int var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoOperationTypeNotFound(long var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoOperationTypeNotFound(float var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoOperationTypeNotFound(double var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoOperationTypeNotFound(String var1, Throwable var2) {
        super(var1, var2);
    }
}