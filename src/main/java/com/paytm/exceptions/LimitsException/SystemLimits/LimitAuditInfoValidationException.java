package com.paytm.exceptions.LimitsException.SystemLimits;

public class LimitAuditInfoValidationException extends RuntimeException {

    public LimitAuditInfoValidationException() {
    }

    private LimitAuditInfoValidationException(String var1) {
        super(var1);
    }

    public LimitAuditInfoValidationException(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public LimitAuditInfoValidationException(boolean var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoValidationException(char var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoValidationException(int var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoValidationException(long var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoValidationException(float var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoValidationException(double var1) {
        this(String.valueOf(var1));
    }

    public LimitAuditInfoValidationException(String var1, Throwable var2) {
        super(var1, var2);
    }

}
