package com.paytm.exceptions.LimitsException.UserDefinedLimits;

public class UserLimitValidationException extends RuntimeException {

    public UserLimitValidationException() {
    }

    private UserLimitValidationException(String var1) {
        super(var1);
    }

    public UserLimitValidationException(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public UserLimitValidationException(boolean var1) {
        this(String.valueOf(var1));
    }

    public UserLimitValidationException(char var1) {
        this(String.valueOf(var1));
    }

    public UserLimitValidationException(int var1) {
        this(String.valueOf(var1));
    }

    public UserLimitValidationException(long var1) {
        this(String.valueOf(var1));
    }

    public UserLimitValidationException(float var1) {
        this(String.valueOf(var1));
    }

    public UserLimitValidationException(double var1) {
        this(String.valueOf(var1));
    }

    public UserLimitValidationException(String var1, Throwable var2) {
        super(var1, var2);
    }


}
