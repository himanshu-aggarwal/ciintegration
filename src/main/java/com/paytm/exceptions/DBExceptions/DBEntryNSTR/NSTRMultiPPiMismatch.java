package com.paytm.exceptions.DBExceptions.DBEntryNSTR;

public class NSTRMultiPPiMismatch extends RuntimeException {

    public NSTRMultiPPiMismatch() {
    }

    private NSTRMultiPPiMismatch(String var1) {
        super(var1);
    }

    public NSTRMultiPPiMismatch(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public NSTRMultiPPiMismatch(boolean var1) {
        this(String.valueOf(var1));
    }

    public NSTRMultiPPiMismatch(char var1) {
        this(String.valueOf(var1));
    }

    public NSTRMultiPPiMismatch(int var1) {
        this(String.valueOf(var1));
    }

    public NSTRMultiPPiMismatch(long var1) {
        this(String.valueOf(var1));
    }

    public NSTRMultiPPiMismatch(float var1) {
        this(String.valueOf(var1));
    }

    public NSTRMultiPPiMismatch(double var1) {
        this(String.valueOf(var1));
    }

    public NSTRMultiPPiMismatch(String var1, Throwable var2) {
        super(var1, var2);
    }


}
