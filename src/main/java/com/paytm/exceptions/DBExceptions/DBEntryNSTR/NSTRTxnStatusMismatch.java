package com.paytm.exceptions.DBExceptions.DBEntryNSTR;

public class NSTRTxnStatusMismatch extends RuntimeException {

    public NSTRTxnStatusMismatch() {
    }

    private NSTRTxnStatusMismatch(String var1) {
        super(var1);
    }

    public NSTRTxnStatusMismatch(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public NSTRTxnStatusMismatch(boolean var1) {
        this(String.valueOf(var1));
    }

    public NSTRTxnStatusMismatch(char var1) {
        this(String.valueOf(var1));
    }

    public NSTRTxnStatusMismatch(int var1) {
        this(String.valueOf(var1));
    }

    public NSTRTxnStatusMismatch(long var1) {
        this(String.valueOf(var1));
    }

    public NSTRTxnStatusMismatch(float var1) {
        this(String.valueOf(var1));
    }

    public NSTRTxnStatusMismatch(double var1) {
        this(String.valueOf(var1));
    }

    public NSTRTxnStatusMismatch(String var1, Throwable var2) {
        super(var1, var2);
    }


}
