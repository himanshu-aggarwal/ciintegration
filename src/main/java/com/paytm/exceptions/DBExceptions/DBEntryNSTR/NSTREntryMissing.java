package com.paytm.exceptions.DBExceptions.DBEntryNSTR;

public class NSTREntryMissing extends RuntimeException {

    public NSTREntryMissing() {
    }

    private NSTREntryMissing(String var1) {
        super(var1);
    }

    public NSTREntryMissing(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public NSTREntryMissing(boolean var1) {
        this(String.valueOf(var1));
    }

    public NSTREntryMissing(char var1) {
        this(String.valueOf(var1));
    }

    public NSTREntryMissing(int var1) {
        this(String.valueOf(var1));
    }

    public NSTREntryMissing(long var1) {
        this(String.valueOf(var1));
    }

    public NSTREntryMissing(float var1) {
        this(String.valueOf(var1));
    }

    public NSTREntryMissing(double var1) {
        this(String.valueOf(var1));
    }

    public NSTREntryMissing(String var1, Throwable var2) {
        super(var1, var2);
    }


}
