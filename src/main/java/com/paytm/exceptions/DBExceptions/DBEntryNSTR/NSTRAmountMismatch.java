package com.paytm.exceptions.DBExceptions.DBEntryNSTR;

public class NSTRAmountMismatch extends RuntimeException {

    public NSTRAmountMismatch() {
    }

    private NSTRAmountMismatch(String var1) {
        super(var1);
    }

    public NSTRAmountMismatch(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public NSTRAmountMismatch(boolean var1) {
        this(String.valueOf(var1));
    }

    public NSTRAmountMismatch(char var1) {
        this(String.valueOf(var1));
    }

    public NSTRAmountMismatch(int var1) {
        this(String.valueOf(var1));
    }

    public NSTRAmountMismatch(long var1) {
        this(String.valueOf(var1));
    }

    public NSTRAmountMismatch(float var1) {
        this(String.valueOf(var1));
    }

    public NSTRAmountMismatch(double var1) {
        this(String.valueOf(var1));
    }

    public NSTRAmountMismatch(String var1, Throwable var2) {
        super(var1, var2);
    }


}
