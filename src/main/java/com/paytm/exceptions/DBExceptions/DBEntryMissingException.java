package com.paytm.exceptions.DBExceptions;

public class DBEntryMissingException extends RuntimeException {

    public DBEntryMissingException() {
    }

    private DBEntryMissingException(String var1) {
        super(var1);
    }

    public DBEntryMissingException(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public DBEntryMissingException(boolean var1) {
        this(String.valueOf(var1));
    }

    public DBEntryMissingException(char var1) {
        this(String.valueOf(var1));
    }

    public DBEntryMissingException(int var1) {
        this(String.valueOf(var1));
    }

    public DBEntryMissingException(long var1) {
        this(String.valueOf(var1));
    }

    public DBEntryMissingException(float var1) {
        this(String.valueOf(var1));
    }

    public DBEntryMissingException(double var1) {
        this(String.valueOf(var1));
    }

    public DBEntryMissingException(String var1, Throwable var2) {
        super(var1, var2);
    }


}
