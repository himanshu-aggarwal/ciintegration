package com.paytm.exceptions.JSONExceptions;

public class JSONParseMappingException extends RuntimeException {

    public JSONParseMappingException() {
    }

    private JSONParseMappingException(String var1) {
        super(var1);
    }

    public JSONParseMappingException(Object var1) {
        this(String.valueOf(var1));
        if (var1 instanceof Throwable) {
            this.initCause((Throwable)var1);
        }

    }

    public JSONParseMappingException(boolean var1) {
        this(String.valueOf(var1));
    }

    public JSONParseMappingException(char var1) {
        this(String.valueOf(var1));
    }

    public JSONParseMappingException(int var1) {
        this(String.valueOf(var1));
    }

    public JSONParseMappingException(long var1) {
        this(String.valueOf(var1));
    }

    public JSONParseMappingException(float var1) {
        this(String.valueOf(var1));
    }

    public JSONParseMappingException(double var1) {
        this(String.valueOf(var1));
    }

    public JSONParseMappingException(String var1, Throwable var2) {
        super(var1, var2);
    }


}
