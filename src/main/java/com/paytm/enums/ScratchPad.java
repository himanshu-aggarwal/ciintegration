package com.paytm.enums;

import java.util.LinkedList;

public class ScratchPad {

    public enum WalletTypeActionList{
        PAYEE_USER_DEBIT("PAYEE_USER","DEBIT"),
        PAYEE_USER_CREDIT("PAYEE_USER","CREDIT"),
        PAYER_USER_DEBIT("PAYER_USER","DEBIT"),
        PAYER_USER_CREDIT("PAYER_USER","CREDIT"),
        SYSTEM_CREDIT("SYSTEM","CREDIT"),
        SYSTEM_DEBIT("SYSTEM","DEBIT"),
        SYSTEM_COMISSION_CREDIT("SYSTEM-COMISSION","CREDIT"),
        COMISSION_EARNED_WALLET_CREDIT("COMISSION_EARNED_WALLET","CREDIT"),
        COMISSION_EARNED_WALLET_DEBIT("COMISSION_EARNED_WALLET","DEBIT"),
        MERCHANT_WALLET_DEBIT("MERCHANT_WALLET","DEBIT"),
        MERCHANT_WALLET_CREDIT("MERCHANT_WALLET","CREDIT"),
        SUBWALLET_CREDIT("SUBWALLET","CREDIT"),
        SUBWALLET_DEBIT("SUBWALLET","DEBIT"),
        ONE97_CREDIT("ONE97_SYSTEM_WALLET","CREDIT"),
        ONE97_DEBIT("ONE97_SYSTEM_WALLET","DEBIT");

        private String walletTypeValue;
        private String creditDebit;

        WalletTypeActionList(String walletTypeValue,String creditDebit){
            this.walletTypeValue=walletTypeValue;
            this.creditDebit=creditDebit;
        }

        public String getWalletTypeValue(){
            return this.walletTypeValue;
        }

        public String getCreditDebit() {
            return this.creditDebit;
        }
    }

    public enum MainOnly implements ScratchPadWallet {

        Transaction_1(new WalletTypeActionList[]{WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT, WalletTypeActionList.MERCHANT_WALLET_CREDIT, WalletTypeActionList.COMISSION_EARNED_WALLET_CREDIT},WalletTypeActionList.MERCHANT_WALLET_DEBIT),
        Transaction_2(new WalletTypeActionList[]{WalletTypeActionList.MERCHANT_WALLET_DEBIT, WalletTypeActionList.SYSTEM_CREDIT, WalletTypeActionList.SYSTEM_DEBIT, WalletTypeActionList.PAYER_USER_CREDIT},null),
        Transaction_2R(new WalletTypeActionList[]{WalletTypeActionList.MERCHANT_WALLET_DEBIT,WalletTypeActionList.COMISSION_EARNED_WALLET_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_COMISSION_CREDIT, WalletTypeActionList.SYSTEM_DEBIT, WalletTypeActionList.PAYER_USER_CREDIT},WalletTypeActionList.SYSTEM_CREDIT),
        Transaction_5(new WalletTypeActionList[]{WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYEE_USER_CREDIT,WalletTypeActionList.COMISSION_EARNED_WALLET_CREDIT},WalletTypeActionList.PAYEE_USER_DEBIT),
        Transaction_8(new WalletTypeActionList[]{WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT},null),
        Transaction_22(new WalletTypeActionList[]{WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT},null),
        Transaction_69(new WalletTypeActionList[]{WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.ONE97_CREDIT,WalletTypeActionList.ONE97_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYEE_USER_CREDIT},null);

        private WalletTypeActionList[] walletList;
        private WalletTypeActionList impactedWallet;

        MainOnly(WalletTypeActionList[] walletList,WalletTypeActionList impactedWallet) {
            this.walletList = walletList;
            this.impactedWallet=impactedWallet;
        }

        public LinkedList<LinkedList<String>> getWalletList() {
            LinkedList<LinkedList<String>> expectedWalletList = new LinkedList<>();
            for(int i=0;i<this.walletList.length;i++) {
                LinkedList<String> walletVal = new LinkedList<>();
                walletVal.add(getWalletTypeValue(this.walletList[i]));
                walletVal.add(getcreditDebitVal(this.walletList[i]));
                expectedWalletList.add(walletVal);
            }
            return expectedWalletList;
        }

        public String getWalletTypeValue(WalletTypeActionList walletTypeActionList){
            return walletTypeActionList.getWalletTypeValue();
        }

        public String getcreditDebitVal(WalletTypeActionList walletTypeActionList){
            return walletTypeActionList.getCreditDebit();
        }

        public LinkedList<String> getimpactedWallet(){
            if(null==this.impactedWallet){
                return null;
            }
            LinkedList<String> walletVal = new LinkedList<>();
            walletVal.add(getWalletTypeValue(this.impactedWallet));
            walletVal.add(getcreditDebitVal(this.impactedWallet));
            return walletVal;
        }

    }

    public enum SubWalletMain implements ScratchPadWallet{
        Transaction_1(new WalletTypeActionList[]{WalletTypeActionList.SUBWALLET_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT,WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT, WalletTypeActionList.MERCHANT_WALLET_CREDIT, WalletTypeActionList.COMISSION_EARNED_WALLET_CREDIT},WalletTypeActionList.MERCHANT_WALLET_DEBIT),
        Transaction_2(new WalletTypeActionList[]{WalletTypeActionList.MERCHANT_WALLET_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT,WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SUBWALLET_CREDIT},null),
        Transaction_2R(new WalletTypeActionList[]{WalletTypeActionList.MERCHANT_WALLET_DEBIT,WalletTypeActionList.COMISSION_EARNED_WALLET_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_COMISSION_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT,WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SUBWALLET_CREDIT},WalletTypeActionList.SYSTEM_CREDIT),
        Transaction_5(new WalletTypeActionList[]{WalletTypeActionList.SUBWALLET_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT,WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYEE_USER_CREDIT,WalletTypeActionList.COMISSION_EARNED_WALLET_CREDIT},WalletTypeActionList.PAYEE_USER_DEBIT),
        Transaction_8(new WalletTypeActionList[]{WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT,WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SUBWALLET_CREDIT},null),
        Transaction_22(new WalletTypeActionList[]{WalletTypeActionList.SUBWALLET_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT,WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT},null),
        Transaction_50(new WalletTypeActionList[]{WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SUBWALLET_CREDIT},null),
        Transaction_69(new WalletTypeActionList[]{WalletTypeActionList.SUBWALLET_DEBIT,WalletTypeActionList.PAYER_USER_CREDIT,WalletTypeActionList.PAYER_USER_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.ONE97_CREDIT,WalletTypeActionList.ONE97_DEBIT,WalletTypeActionList.SYSTEM_CREDIT,WalletTypeActionList.SYSTEM_DEBIT,WalletTypeActionList.PAYEE_USER_CREDIT},null);


        private WalletTypeActionList[] walletList;
        private WalletTypeActionList impactedWallet;

        SubWalletMain(WalletTypeActionList[] walletList,WalletTypeActionList impactedWallet) {
            this.walletList = walletList;
            this.impactedWallet=impactedWallet;
        }

        public LinkedList<LinkedList<String>> getWalletList() {
            LinkedList<LinkedList<String>> expectedWalletList = new LinkedList<>();
            for(int i=0;i<this.walletList.length;i++) {
                LinkedList<String> walletVal = new LinkedList<>();
                walletVal.add(getWalletTypeValue(this.walletList[i]));
                walletVal.add(getcreditDebitVal(this.walletList[i]));
                expectedWalletList.add(walletVal);
            }
            return expectedWalletList;
        }

        public String getWalletTypeValue(WalletTypeActionList walletTypeActionList){
            return walletTypeActionList.getWalletTypeValue();
        }

        public String getcreditDebitVal(WalletTypeActionList walletTypeActionList){
            return walletTypeActionList.getCreditDebit();
        }

        public LinkedList<String> getimpactedWallet(){
            if(null==this.impactedWallet){
                return null;
            }
            LinkedList<String> walletVal = new LinkedList<>();
            walletVal.add(getWalletTypeValue(this.impactedWallet));
            walletVal.add(getcreditDebitVal(this.impactedWallet));
            return walletVal;
        }

    }

    public interface ScratchPadWallet{

        LinkedList<LinkedList<String>> getWalletList();
        LinkedList<String> getimpactedWallet();


    }



}
