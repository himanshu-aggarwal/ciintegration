package com.paytm.enums;

public enum WalletType {

    //PayeeUser and payerUser are introduced to handle scratchPad
    PAYEE_USER(1),
    PAYER_USER(1),
    MERCHANT_WALLET(2),
    POOLED_WALLET(3),
    COMISSION_EARNED_WALLET(4),
    SYSTEM(5),
    MERCHANT_SALES_WALLET(6),
    BUSINESS_WALLET(7),
    FINANCE(8),
    MARKETING_DEALS(9),
    CUSTOMER_SUPPORT(10),
    DISPUTED_SYSTEMS_WALLET(11),
    ESCROW_DISPUTED_SYSTEMS_WALLET(12),
    TOP_UP(13),
    CORPORATE_VOUCHER(14),
    TDS_WALLET(15),
    COMMISSION_DISBURSED_WALLET(16),
    CUSTOMER_GRIEVANCE_WALLET(17),
    FINANCE_POOL_WALLET(18),
    BC_POOL_WALLET(19),
    SELLER_POOL_WALLET(20),
    BLOCKED(21),
    RESELLER_POOL_WALLET(22),
    ONE97_SYSTEM_WALLET(23);

    private int walletTypeId;

    WalletType(int walletTypeId) {
        this.walletTypeId=walletTypeId;
    }

    public int getWalletTypeId(){
        return this.walletTypeId;
    }
}
