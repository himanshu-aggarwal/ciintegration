package com.paytm.enums;

public class LimitEnums {

    public enum  LimitTypeEnums{
        CREDIT_THROUGHPUT_LIMIT,
        DEBIT_THROUGHPUT_LIMIT,
        RBI_BALANCE,
        DEBIT_THROUGHPUT_LIMIT$ADD,
        DEBIT_THROUGHPUT_LIMIT$WITHDRAW,
        TXN_TYPE_DURATION_P2P,
        TXN_TYPE_DURATION_AMC,
        CUMULATIVE_P2P_P2B,
        TXN_TYPE_DURATION_ADD_MONEY_GV,
        WALLET_TO_WALLET_TRANSFER_LIMIT,
        WALLET_AGGREGATE_BALANCE_LIMIT,
        WALLET_TO_BANK_TRANSFER_LIMIT,
        PURCHASE_LIMIT;
    }

    public enum LimitPeriodEnums{
        day, month, year;
    }

}
