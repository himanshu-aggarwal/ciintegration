package com.paytm.enums;

public interface ValidationInterface {

    public int gethttpResponseCode();
    public Object getStatus();
    public String getStatusCode();
    public String getStatusMessage();

}
