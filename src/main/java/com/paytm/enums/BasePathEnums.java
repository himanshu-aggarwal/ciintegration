package com.paytm.enums;

import com.paytm.constants.Constants;

public class BasePathEnums {

	public enum WalletWithdraw {

		WalletWithDrawTest(Constants.WALLETAPIresource.WITHDRAWMONEY),
		V2WalletWithDrawTest(Constants.WALLETAPIresource.V2WITHDRAWMONEY),
		WalletAPIHelpers(Constants.WALLETAPIresource.WITHDRAWMONEY),
		V4WalletWithDrawTest(Constants.WALLETAPIresource.V4WithDrawMoney),
		V5WalletWithDrawTest(Constants.WALLETAPIresource.V5WITHDRAWMONEY),
		V6WalletWithDrawTest(Constants.WALLETAPIresource.V6WITHDRAWMONEY),
		V7WalletWithDrawTest(Constants.WALLETAPIresource.V7WITHDRAWMONEY),
		V9WalletWithDrawTest(Constants.WALLETAPIresource.V9WITHDRAWMONEY);;

		private String name;

		WalletWithdraw(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}
	}

	public enum ValidateOTPWithdraw {

		WalletWithDrawTest(Constants.WALLETAPIresource.VALIDATEOTP), 
		V2WalletWithDrawTest(Constants.WALLETAPIresource.V2VALIDATETRANSACTION),
		V4WalletWithDrawTest(Constants.WALLETAPIresource.V4VALIDATETRANSACTION),
		V5WalletWithDrawTest(Constants.WALLETAPIresource.V5VALIDATETRANSACTION);

		private String name;

		ValidateOTPWithdraw(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}
	}

	public enum ResendOTPWithdraw {

		V2WalletWithDrawTest(Constants.WALLETAPIresource.RESENDOTP),
		V4WalletWithDrawTest(Constants.WALLETAPIresource.V4RESENDOTP),
		V5WalletWithDrawTest(Constants.WALLETAPIresource.RESENDOTP);

		private String name;

		ResendOTPWithdraw(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}
	}

}
