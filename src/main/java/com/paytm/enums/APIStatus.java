package com.paytm.enums;



public class APIStatus {


    public enum SendMoney implements ValidationInterface {

        SUCCESS(200, "SUCCESS", "SS_0001", "Request Successfully fullfilled."),
        FAILURE(200, "SUCCESS", "SS_0001", "Request Successfully fullfilled."),
        PENDING(200, "PENDING", "OTP_0001", "Please enter the One-time password (OTP) sent on your registered mobile# to send money."),
        WM_1006(200, "FAILURE", "WM_1006", "Your balance is insufficient for this request. Please add money in your wallet before proceeding.");

        private int httpResponseCode;
        private String status;
        private String statusCode;
        private String statusMessage;

        SendMoney(int httpResponseCode, String status, String statusCode, String statusMessage) {
            this.httpResponseCode = httpResponseCode;
            this.status = status;
            this.statusCode = statusCode;
            this.statusMessage = statusMessage;
        }

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}

		}

	public enum AddFundsToSubWallet implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "SUCCESS", "SUCCESS");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		AddFundsToSubWallet(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}

	}


	public enum WalletWithdraw implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "SUCCESS", "SUCCESS"),
		WM_1006(200, "FAILURE", "WM_1006", "Your balance is insufficient for this request. Please add money in your wallet before proceeding."),
		WM_1003(200, "FAILURE", "WM_1003", "Merchant does not exist."),
		GE_1007(200, "FAILURE", "GE_1007", "Invalid Transaction Amount."),
		GE_1018(200, "FAILURE", "GE_1018", "Invalid currency code"),
		CODE_403(403, null, "403", "Unauthorized Access"),
		GE_1035(200, "FAILURE", "GE_1035", "Merchant is in inactive state."),
		WM_1011(200, "FAILURE", "WM_1011", "Repeated orderId. There exists an old request with the same orderId."),
		WM_1007(200, "FAILURE", "WM_1007", "Repeated pgTxnId. There exists an old PgTxnRequest with the same pgTxnId."),
		UNAUTHORIZED_ACCESS(403, null, "403", "Unauthorized Access"),
		GE_1026(200, "FAILURE", "GE_1026", "We can not process your request. Please try later."),
		OTP_VALIDATION(200, "SUCCESS", "SS_0001", "Request Successfully fullfilled."),
		OTP_INVALID(200, "FAILURE", "OTP_1008", "Invalid Otp."),
		SS_0001(200, "SUCCESS", "SS_0001", "Request Successfully fullfilled."),
		RESENDOTPSUCCESS(200, "SUCCESS", "OTP_1001", "OTP resent successfully. "),
		OTP_1005(200, "FAILURE", "OTP_1005", "You can not use a OTP multiple times."),
		ERROR_404(404, null, "404", "User doesn't exist."),
		GE_0003(200, "FAILURE", "GE_0003", "We could not get the requested details. Please try again."),
		unathorized_access(403, null, "403", "unathorized access"),
		INVALIDOTP(403, null, "403", "Invalid Otp."),
		BAD_REQUEST(400, null, "403", "Bad request");


		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		WalletWithdraw(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

        public int gethttpResponseCode() {
            return httpResponseCode;
        }

        public String getStatus() {
            return status;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

    }

    public enum QrWithdraw implements ValidationInterface {

        SUCCESS(200, null, "SUCCESS", "SUCCESS"),
        UTS_2026(200, "FAILURE", "UTS_2026", "Error occured during withdraw/withdrawNRelease"),
        UTS_2019(200, "FAILURE", "UTS_2019", "Incorrect amount."),
        GE_0001(200, "FAILURE", "GE_0001", "Unknown error"),
        GE_0003(200, "FAILURE", "GE_0003", "We could not get the requested details. Please try again."),
        UTS_2009(200, "FAILURE", "UTS_2009", "Sorry,Transport with given details isn't available at this route."),
        UTS_2002(200, "FAILURE", "UTS_2002", "Source Id cannot be blank."),
        UTS_2010(200, "FAILURE", "UTS_2010", "Source Name cannot be blank."),
        UTS_2003(200, "FAILURE", "UTS_2003", "Destination Id cannot be blank."),
        UTS_2011(200, "FAILURE", "UTS_2011", "Destination Name cannot be blank."),
        UTS_2007(200, "FAILURE", "UTS_2007", "Invalid type of bus."),
        UTS_2004(200, "FAILURE", "UTS_2004", "Must choose type of bus."),
        UTS_2016(200, "FAILURE", "UTS_2016", "Route Id cannot be null."),
        UTS_2017(200, "FAILURE", "UTS_2017", "Route Name cannot be null."),
        UTS_2005(200, "FAILURE", "UTS_2005", "Must choose pax type.");
        private int httpResponseCode;
        private String status;
        private String statusCode;
        private String statusMessage;

        QrWithdraw(int httpResponseCode, String status, String statusCode, String statusMessage) {
            this.httpResponseCode = httpResponseCode;
            this.status = status;
            this.statusCode = statusCode;
            this.statusMessage = statusMessage;
        }

        public int gethttpResponseCode() {
            return httpResponseCode;
        }

        public String getStatus() {
            return status;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

    }



        public enum SubWalletRefund implements ValidationInterface {

            SUCCESS(200, "SUCCESS", "SUCCESS", "SUCCESS"),
            RWS_0001(200, "FAILURE", "RWS_0001", "Invalid cap amount values in request."),
            RPG_001(200, "FAILURE", "RPG_001", "Refund is not possible as refund possibly already done or current refund request may cause refund to exceed actual transaction amount."),
            GE_1007(200, "FAILURE", "GE_1007", "Invalid Transaction Amount."),
			WM_1006(200, "FAILURE", "WM_1006", "Your balance is insufficient for this request. Please add money in your wallet before proceeding."),
            GE_1009(200, null, "GE_1009", "Invalid transaction id."),
			GE_0003(200, "FAILURE", "GE_0003", "We could not get the requested details. Please try again."),
            TS_1000(200, "FAILURE", "TS_1000", "Invalid txn, no txn exist for received txnid."),
            GE_1018(200, "FAILURE", "GE_1018", "Invalid currency code"),
            WM_1001(200, "FAILURE", "WM_1001", "Could not complete request. Please retry again."),
            WM_1003(200, "FAILURE", "WM_1003", "Merchant does not exist."),
            BADREQUEST(400, null, null, null),
            GE_1071(200, "FAILURE", "GE_1071", "Invalid Refund Request as either use CommissionRollBack flag or parameter."),
            GE_1072(200, "FAILURE", "GE_1072", "In case of commission rollback flag as false, amount populated in commission rollback amount should not be greater than zero."),
            GE_1020(200, "FAILURE", "GE_1020", "Can't established User/Merchant identity against given transaction id.");

            private int httpResponseCode;
            private String status;
            private String statusCode;
            private String statusMessage;

            SubWalletRefund(int httpResponseCode, String status, String statusCode, String statusMessage) {
                this.httpResponseCode = httpResponseCode;
                this.status = status;
                this.statusCode = statusCode;
                this.statusMessage = statusMessage;
            }

            public int gethttpResponseCode() {
                return httpResponseCode;
            }

            public String getStatus() {
                return status;
            }

            public String getStatusCode() {
                return statusCode;
            }

            public String getStatusMessage() {
                return statusMessage;
            }

        }


        public enum CheckUserBalance implements ValidationInterface {

            SUCCESS(200, "SUCCESS", "SUCCESS", "SUCCESS"),
            CBM_1001(200, "FAILURE", "CBM_1001", "Balance could not be fetched due to incorrect merchant details");

            private int httpResponseCode;
            private String status;
            private String statusCode;
            private String statusMessage;

            CheckUserBalance(int httpResponseCode, String status, String statusCode, String statusMessage) {
                this.httpResponseCode = httpResponseCode;
                this.status = status;
                this.statusCode = statusCode;
                this.statusMessage = statusMessage;
            }

            public int gethttpResponseCode() {
                return httpResponseCode;
            }

            public String getStatus() {
                return status;
            }

            public String getStatusCode() {
                return statusCode;
            }

            public String getStatusMessage() {
                return statusMessage;
            }

        }

        public enum CreateLifafa implements ValidationInterface {

            SUCCESS(200, null, "EMC_0000", "Lifafa Created");

            private int httpResponseCode;
            private String status;
            private String statusCode;
            private String statusMessage;

            CreateLifafa(int httpResponseCode, String status, String statusCode, String statusMessage) {
                this.httpResponseCode = httpResponseCode;
                this.status = status;
                this.statusCode = statusCode;
                this.statusMessage = statusMessage;
            }

            public int gethttpResponseCode() {
                return httpResponseCode;
            }

            public String getStatus() {
                return status;
            }

            public String getStatusCode() {
                return statusCode;
            }

		public String getStatusMessage() {
			return statusMessage;
		}

	}

	public enum PreAuthNRelease implements ValidationInterface {

		SUCCESS(200, null, "SUCCESS", "SUCCESS"),
		WM_1006(200, null, "WM_1006","Your balance is insufficient for this request. Please add money in your wallet before proceeding."),
		GE_0001(200, null, "GE_0001","Oops Something went wrong! Please try again after sometime!");;

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		PreAuthNRelease(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum CreateDynamicQRCode implements ValidationInterface {

		SUCCESS(200, null, null, null),
		DQR_0016(200, null, "DQR_0027", "Business Type is not valid"),
		DQR_0003(200, null, "DQR_0003", "Entered phoneNo is not valid at Oauth end"),
		MW_1001(200, null, "MW_1001", "Invalid Phone No and Email id"),
		BADREQUEST(400, null, null,	null),
		UNAUTHORIZED_ACCESS(403, null, "403", "Unauthorized Access");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		CreateDynamicQRCode(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}

	}

	public enum FetchQrCodeDetails implements ValidationInterface {

		SUCCESS(200, null, "200", "SUCCESS"),
		DQR_0049(200, null, "DQR_0049",	"Atleast one of mappingId, qrCodeId, stickerId, posId should be present in request."),
		DQR_0050(200, null, "DQR_0050", "No data exists for the given input."),
		BADREQUEST(400, null, null,	null),
		UNAUTHORIZED_ACCESS(403, null, "403", "Unauthorized Access");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		FetchQrCodeDetails(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum GetQrCodeInfo implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "SUCCESS", "SUCCESS"),
		QR_1018(200, "FAILURE", "QR_1018", "This Qr Code cannot be recognised."),
		GE_0001(200, "FAILURE", "GE_0001", "Oops, something went wrong!"),
		BADREQUEST(400, null, null,	null),
		UNAUTHORIZED_ACCESS(200, "FAILURE", "403", "Unauthorized Access");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		GetQrCodeInfo(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum MapDynamicQrCode implements ValidationInterface {

		SUCCESS(200, null, "DQR_0000", "SUCCESS"),
		DQR_0021(200, "FAILURE", "DQR_0021", "Invalid/Blank type of QrCode."),
		DQR_0024(200, "FAILURE", "DQR_0024", "Qr Code not valid for user."),
		QR_1031(200, "FAILURE", "QR_1031", "Deeplink should be present in request."),
		DQR_0005(200, "FAILURE", "DQR_0005", "Error while finding QR Code from DB"),
		DQR_0001(200, "FAILURE", "DQR_0001", "Sticker ID and Qr Code ID both are empty."),
		DQR_0002(200, "FAILURE", "DQR_0002", "Phone Number must be present"),
		DQR_0011(200, "FAILURE", "DQR_0011", "Phone no is not valid"),
		DQR_0037(200, "FAILURE", "DQR_0037", "Invalid/Blank mapping type."),
		DQR_0023(200, "FAILURE", "DQR_0023", "Qr Code not valid for merchant."),
		DQR_0019(200, "FAILURE", "DQR_0019", "MerchantId is empty"),
		GE_0001(200, null, "GE_0001", "Unknown error"),
		DQR_0009(200, "FAILURE", "DQR_0009", "QRCode is already mapped to User."),
		BADREQUEST(400,	null, null,	null),
		UNAUTHORIZED_ACCESS(403, null, "403", "Unauthorized Access"),
		GE_1010(200, "FAILURE",	"GE_1010", "We could not get the requested details. Please try again."),
		DQR_0003(200, "FAILURE", "DQR_0003", "Entered phoneNo is not valid at Oauth end"),
		DQR_0020(200, "FAILURE", "DQR_0020", "QrCode is already mapped to merchant"),
		DQR_0051(200, "FAILURE", "DQR_0051", "QR can't be mapped to any aggregator.");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		MapDynamicQrCode(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum EditQrCodeDetails implements ValidationInterface {

		SUCCESS(200, null, "200", "SUCCESS"),
		BADREQUEST(400, null, null, null),
		UNAUTHORIZED_ACCESS(403, null, "403", "Unauthorized Access"),
		DQR_1016(200, null, "DQR_1016", null);

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		EditQrCodeDetails(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum GenerateQrCode implements ValidationInterface {

		SUCCESS(200, null, "200", "SUCCESS"),
		DQR_0045(200, "FAILURE", "DQR_0045","Batch count or size should be present only in the BULK_CREATE request."),
		DQR_0043(200, "FAILURE", "DQR_0043", "Alipay codec not found for the input business type."),
		DQR_0003(200, "FAILURE", "DQR_0003", "Entered phoneNo is not valid at Oauth end"),
		DQR_0041(200, "FAILURE", "DQR_0041", "Batch count/size can not be null/blank/negative."),
		DQR_0042(200, "FAILURE", "DQR_0042", "Agent phone number can not be null/blank."),
		DQR_0040(200, "FAILURE", "DQR_0040", "Business type can not be blank."),
		DQR_0039(200, "FAILURE", "DQR_0039", "Create request cannot be null/blank."),
		BADREQUEST(400,	null, null, null),
		DQR_0011(200, "FAILURE", "DQR_0011", "Phone no is not valid"),
		DQR_0021(200, "FAILURE", "DQR_0021", "Invalid/Blank type of QrCode."),
		DQR_0037(200, "FAILURE", "DQR_0037", "Invalid/Blank mapping type."),
		DQR_0019(200, "FAILURE", "DQR_0019", "MerchantId is empty"),
		GE_1010(200, "FAILURE",	"GE_1010", "We could not get the requested details. Please try again."),
		QR_1031(200, "FAILURE",	"QR_1031", "Deeplink should be present in request."),
		DQR_0047(200, "FAILURE", "DQR_0047", "No other operation type is allowed with BULK_CREATE."),
		DQR_0046(200, "FAILURE", "DQR_0046", "Sticker Id or Qr Id should be present in the request in case of operation type is MAP only."),
		GE_0001(200, "FAILURE",	"GE_0001",	"Oops, something went wrong!"),
		DQR_0051(200, "FAILURE", "DQR_0051", "QR can't be mapped to any aggregator.");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		GenerateQrCode(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum CreateQrCode implements ValidationInterface {

		QR_0001(200, "SUCCESS", "QR_0001", "SUCCESS"),
		QR_1023(200, "FAILURE", "QR_1023", "Unable to find Merchant"),
		QR_1002(200, "FAILURE", "QR_1002", "Empty MID and MerchantGuid"),
		QR_1028(200, "FAILURE", "QR_1028", "Error Occur while generating QRCode"),
		QR_1027(200, "FAILURE",	"QR_1027", "Empty Order Details "),
		QR_1020(200, "SUCCESS", "QR_1020", "Data already exist for this Merchant with same Id."),
		QR_1004(200, "FAILURE",	"QR_1004", "Empty Order Id"),
		GE_1007(200, "FAILURE", "GE_1007", "Invalid Transaction Amount."),
		QR_1005(200, "FAILURE",	"QR_1005", "Invalid Amount"),
		QR_1030(200, "FAILURE",	"QR_1030", "Merchant guid and mid given in the request donot belong to same merchant."),
		QR_1007(200, "FAILURE", "QR_1007", "Invalid Expiry Date"),
		QR_1033(200, "FAILURE", "QR_1033", "A QR Code with same order id already exists."),
		QR_1032(200, "FAILURE",	"QR_1032", "Empty merchant handle."),
		QR_1006(200, "FAILURE",	"QR_1006", "Empty Product Id"),
		QR_1011(200, "FAILURE",	"QR_1011", "Invalid Request Type"),
		UNAUTHORIZED_ACCESS(401, null, "401","UnAuthorized Access"),
		QR_1034(200,"FAILURE","QR_1034","QR can't be mapped to any aggregator."),
		INTERNALSERVERERROR(500, null,null, null);

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		CreateQrCode(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum AssignQRCode implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "QR_0001", "SUCCESS"),
		DQR_003(200, null, "DQR_0003", "Entered phoneNo is not valid at Oauth end"),
		DQR_0026(200, "SUCCESS", "DQR_0026", "QR Code already assigned to user."),
		DQR_0027(200, null, "DQR_0027",	"Business Type is not valid"),
		DQR_0002(200, null, "DQR_0002",	"Phone Number must be present"),
		GE_1010(200, null, "GE_1010", "We could not get the requested details. Please try again."),
		DQR_0028(200, null, "DQR_0028",	"No Qr code found for this merchant."),
		BADREQUEST(400, null, null, null),
		UNAUTHORIZED_ACCESS(403, null, "403", "Unauthorized Access");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		AssignQRCode(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum GetMerchantInfo implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "QR_0001", "SUCCESS"),
		DQR_0028(200, "FAILURE", "DQR_0028", "No Qr code found for this merchant."),
		QR_1030(200, "FAILURE", "QR_1030", "Merchant guid and mid given in the request donot belong to same merchant."),
		QR_1002(200, "FAILURE", "QR_1002", "Empty MID and MerchantGuid"),
		QR_1023(200, null, "QR_1023", "Unable to find Merchant"),
		BADREQUEST(400, null, null,	null),
		UNAUTHORIZED_ACCESS(403, null, "403", "Unauthorized Access"),
		INTERNALSERVERERROR(500, null, null, null);

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		GetMerchantInfo(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}

	}

	public enum CheckBalance implements ValidationInterface {

		SUCCESS(200, null, "SUCCESS", "SUCCESS"),
        UNAUTHORIZED(403, null, "403", "Unauthorized Access");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		CheckBalance(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum MerchantQrCode implements ValidationInterface {

		QR_1020(200, "SUCCESS", "QR_1020", "Data already exist for this Merchant with same Id."),
		QR_1011(200, null, "QR_1011", "Invalid Request Type"),
		NONSDMERCHANT(408, null, "408", "User is not SD Merchant."),
		INVALIDTOKEN(408, null, "408", "Invalid token."),
		NOSDMERCHANTATUMP(408, null, "408", "User not associated with any merchant from UMP End."),
		UNAUTHORIZEDACCESS(403, null, "403", "Unauthorized Access"),
		BADREQUEST(408, null, "408", "Bad request"),
		QR_0001(200, "SUCCESS", "QR_0001", "SUCCESS");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		MerchantQrCode(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum GetUTSFareInfo implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "SUCCESS", "SUCCESS"),
        UNAUTHORIZEDACCESS(403, "FAILURE", "403","Unauthorized Access"),
        UTS_2006(200, "FAILURE", "UTS_2006", "MerchantGuid cannot be null."),
        UTS_2009(200, "FAILURE", "UTS_2009","Sorry,Transport with given details isn't available at this route."),
        UTS_2013(200, "FAILURE","UTS_2013", "Route Id cannot be Null."),
        UTS_2014(200, "FAILURE", "UTS_2014","Route Name cannot be blank."),
        UTS_2002(200, "FAILURE", "UTS_2002","Source Id cannot be blank."),
        UTS_2010(200, "FAILURE", "UTS_2010","Source Name cannot be blank."),
        UTS_2003(200, "FAILURE","UTS_2003","Destination Id cannot be blank."),
        UTS_2011(200,"FAILURE", "UTS_2011","Destination Name cannot be blank."),
        UTS_2004(200, "FAILURE", "UTS_2004","Must choose type of bus."),
        UTS_2007(200, "FAILURE", "UTS_2007","Invalid type of bus."),
        UTS_2005(200, "FAILURE","UTS_2005","Must choose pax type."),
        UTS_2008(200, "FAILURE","UTS_2008","Invalid type of PAX.");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		GetUTSFareInfo(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum UserDefinedLimitSet implements ValidationInterface {

		SUCCESS(200, null, "UDLS_0000", "Your self defined transactional limits have been updated successfully.");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		UserDefinedLimitSet(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum WalletLimits implements ValidationInterface {

		SUCCESS(200, null, "SUCCESS", "Wallet Limit Applied successfully.", null, null),
		RWL_1001(200, null, "SUCCESS","Wallet Limit Applied successfully.", "RWL_1001","This request will exceed maximum allowed money addition in wallet for this month."),
		RWL_2001(200,null, "SUCCESS", "Wallet Limit Applied successfully.", "RWL_2001",	"You will exceed maximum money addition in wallet."),
		RWL_1000(200, null, "SUCCESS","Wallet Limit Applied successfully.", "RWL_1000","May exceed maximum allowed balance"),
		RWL_3002(200, null, "SUCCESS","Wallet Limit Applied successfully.", "RWL_3002","The destination Paytm user will need to do KYC to add/receive money."),
        UDL_0006(200, null, "SUCCESS", "Wallet Limit Applied successfully.", "UDL_0006","Daily limit for total amount of Send Money transactions will exceed. Please update limits and retry."),
        UDL_0007(200, null, "SUCCESS", "Wallet Limit Applied successfully.","UDL_0007", "Daily limit for number of Send Money transactions will exceed. Please update limits and retry."),
        UDL_0008(200, null, "SUCCESS","Wallet Limit Applied successfully.", "UDL_0008","Monthly limit for total amount of Send Money transactions will exceed. Please update limits and retry."),
        UDL_0009(200, null, "SUCCESS","Wallet Limit Applied successfully.","UDL_0009","Monthly limit for number of Send Money transactions will exceed. Please update limits and retry."),
        UDL_0014(200, null, "SUCCESS","Wallet Limit Applied successfully.","UDL_0014","Daily limit for total amount of Payment to Merchant transactions will exceed. Please update limits and retry."),
        UDL_0015(200, null, "SUCCESS","Wallet Limit Applied successfully.",	"UDL_0015",	"Daily limit for number of Payment to Merchant transactions will exceed. Please update limits and retry."),
        UDL_0016(200, null, "SUCCESS","Wallet Limit Applied successfully.","UDL_0016","Monthly limit for total amount of Payment to Merchant transactions will exceed. Please update limits and retry."),
        UDL_0017(200, null,"SUCCESS","Wallet Limit Applied successfully.","UDL_0017","Monthly limit for number of Payment to Merchant transactions will exceed. Please update limits and retry."),
        AL_0006(200,null,"SUCCESS","Wallet Limit Applied successfully.","AL_0006","You cannot transfer an amount more than the configured monthly limit for a non-beneficiary"),
        AL_0001(200,null,"SUCCESS","Wallet Limit Applied successfully.","AL_0001","You cannot transfer an amount more than the configured monthly limit for this beneficiary"),
        AL_0002(200,null,"SUCCESS",	"Wallet Limit Applied successfully.","AL_0002",	"You cannot perform more number of transactions than the configured monthly limit for this beneficiary"),
        RWL_0004(200,null,"SUCCESS","Wallet Limit Applied successfully.","RWL_0004","You will exceed maximum number of allowed transactions for this month.increase txn limit."),
        RWL_0002(200,null,"SUCCESS","Wallet Limit Applied successfully.","RWL_0002","You will exceed maximum allowed transaction amount."),
        WLWTL_1001(200,	null,"WLWTL_1001","Trying to deduct amount greater than actual user wallet balance.",null,null),
        RWL_0008(200,null,"SUCCESS","SUCCESS","RWL_0008","You are not allowed to perform this transaction.");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		private String limitMessage;
		private String message;

		WalletLimits(int httpResponseCode, String status, String statusCode, String statusMessage, String limitMessage,
				String message) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
			this.limitMessage = limitMessage;
			this.message = message;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}

		public String getLimitMessage() {
			return limitMessage;
		}

		public String getMessage() {
			return message;
		}
	}

	public enum WalletLimitsList implements ValidationInterface {

		SUCCESS(200, null, "SUCCESS", "No Limit failure", null, null, null, null),
        RWL_1001(200, null, "FAILURE","Wallet Limit failed in ADD_MONEY", "RWL_1001","This request will exceed maximum allowed money addition in wallet for this month.", null,	null),
        RWL_2001(200, null, "FAILURE", "Wallet Limit failed in ADD_MONEY", "RWL_2001","You will exceed maximum money addition in wallet.", null,null),
        RWL_2001_II(200, null, "FAILURE", "Wallet Limit failed in ADD_MONEY", "RWL_2001","You will exceed maximum money addition in wallet.", "RWL_2001","You will exceed maximum money addition in wallet."),
        UDL_0014(200, null, "FAILURE","Wallet Limit failed in WITHDRAW_MONEY", null, null, "UDL_0014","Daily limit for total amount of Payment to Merchant transactions will exceed. Please update limits and retry."),
        UDL_0015(200, null, "FAILURE", "Wallet Limit failed in WITHDRAW_MONEY", null,null, "UDL_0015","Daily limit for number of Payment to Merchant transactions will exceed. Please update limits and retry."),
        UDL_0016(200, null, "FAILURE", "Wallet Limit failed in WITHDRAW_MONEY",	null, null, "UDL_0016","Monthly limit for total amount of Payment to Merchant transactions will exceed. Please update limits and retry."),
        UDL_0017(200, null, "FAILURE","Wallet Limit failed in WITHDRAW_MONEY", null, null,"UDL_0017","Monthly limit for number of Payment to Merchant transactions will exceed. Please update limits and retry."),
        BOTHRWL_0008(200, null, "FAILURE","FAILURE", "RWL_0008", "You are not allowed to perform this transaction.","RWL_0008",	"You are not allowed to perform this transaction."),
        WITHDRAWRWL_0008(200, null,"FAILURE","FAILURE",null,null,"RWL_0008","You are not allowed to perform this transaction."),
        ADDRWL_0008(200, null,"FAILURE","FAILURE","RWL_0008","You are not allowed to perform this transaction.",null,null);

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		private String firstlimitMessage;
		private String firstmessage;
		private String secondlimitMessage;
		private String secondmessage;

		WalletLimitsList(int httpResponseCode, String status, String statusCode, String statusMessage,
				String firstlimitMessage, String firstmessage, String secondlimitMessage, String secondmessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
			this.firstlimitMessage = firstlimitMessage;
			this.firstmessage = firstmessage;
			this.secondlimitMessage = secondlimitMessage;
			this.secondmessage = secondmessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}

		public String getFirstLimitMessage() {
			return firstlimitMessage;
		}

		public String getFirstMessage() {
			return firstmessage;
		}

		public String getSecondLimitMessage() {
			return secondlimitMessage;
		}

		public String getSecondMessage() {
			return secondmessage;
		}

	}

	public enum FetchQrMappingDetail implements ValidationInterface {

		SUCCESS(200, null, "QR_0001", "SUCCESS"),
        BADREQUEST(400, null, null, null),
        UNAUTHORIZED_ACCESS(403, null,"403", "Unauthorized Access");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		FetchQrMappingDetail(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum MapDeeplinkToQr implements ValidationInterface {

		SUCCESS(200, null, "DQR_0000", "SUCCESS"),
		GE_1052(200, "FAILURE", "GE_1052","Merchant ID and GUID from header do not match."),
		GE_1043(200, "FAILURE", "GE_1043","Merchant does not exists"),
		QR_1031(200, "FAILURE", "QR_1031","Deeplink should be present in request."),
		DQR_0020(200, "FAILURE", "DQR_0020","QrCode is already mapped to merchant"),
		DQR_0023(200, "FAILURE", "DQR_0023","Qr Code not valid for merchant."),
		BADREQUEST(400, null, null,	null),
		UNAUTHORIZEDACCESS(403, "FAILURE", "403","Unauthorized Access"),
		DQR_0016(200, "FAILURE","DQR_0016", "Duplicate request received.");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		MapDeeplinkToQr(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum AddMoney implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "SUCCESS", "SUCCESS");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		AddMoney(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum FundCheckBalance implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "SS_0001", "SUCCESS"),
		UNAUTHORIZED(500, "FAILURE", "403","Unauthorized Access"),
		FAILURE(500, "FAILURE", "500", "Wallets owner guid mismatch.");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		FundCheckBalance(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}

	public enum TransferToBank implements ValidationInterface {

		SUCCESS(200, "SUCCESS", "SS_0001", "Request Successfully fullfilled."),
		FAILURE(200, "FAILURE", "RWL_3005",	"You need to do KYC to send money to any user/bank account."),
		P2B_8000(200, "FAILURE", "P2B_8000","We found some technical error. Please try again."),
		USD_01(200, "PENDING", "USD_01","Subscription Registered Successfully"),
		P2B_2014(200, "FAILURE", "P2B_2014","This is a small glitch,Please try again");

		private int httpResponseCode;
		private String status;
		private String statusCode;
		private String statusMessage;

		TransferToBank(int httpResponseCode, String status, String statusCode, String statusMessage) {
			this.httpResponseCode = httpResponseCode;
			this.status = status;
			this.statusCode = statusCode;
			this.statusMessage = statusMessage;
		}

		public int gethttpResponseCode() {
			return httpResponseCode;
		}

		public String getStatus() {
			return status;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getStatusMessage() {
			return statusMessage;
		}
	}
}