package com.paytm.helpers;

import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.DBEnums;
import com.paytm.enums.ScratchPad.ScratchPadWallet;
import com.paytm.enums.UserHeaders;
import com.paytm.utils.DBValidation;

import java.math.BigDecimal;
import java.util.*;

public class ScratchPadValidator {

    private static final long System_WalletID = 93973;
    private static final long COMISSION_EARNED_WALLETID = 32411;
    private static final long ONE97_SYSTEM_WALLETID = 382527;
    private static ScratchPadValidator instance;

    private ScratchPadValidator() {

    }

    public static ScratchPadValidator getInstance() {
        if (instance == null) {
            synchronized (CommonValidation.class) {
                if (instance == null) {
                    instance = new ScratchPadValidator();
                }
            }
        }
        return instance;
    }

    private BigDecimal getCommissionAmt(String merchantOrderId) {
/*
        BigDecimal commissionAmt = BigDecimal.ZERO;
        DBValidation dbValidation = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.MERCHANTORDERID, merchantOrderId);
        Map<String, Object> nstrEntry = dbValidation.runDbQuery(DBQueries.nstr_multiPPI, params,"log4j").get(0);
        if (null != nstrEntry.get("commision_amt")) {
            commissionAmt = new BigDecimal(nstrEntry.get("commision_amt").toString());
        }
        return commissionAmt;
*/
        return FundThreadManager.getInstance().getCommissionAmount();
    }

    private Boolean getMerchantType(String merchantGuid){
        DBValidation dbValidation = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.MERCHANTGUID, merchantGuid);
        Map<String, Object> merchantData = dbValidation.runDbQuery(DBQueries.getMerchantType, params, "log4j").get(0);
        return (Boolean) merchantData.get("offline_post_convenience");
    }

    private List<Map<String, Object>> getDBExtendedSubWalletList(String merchantOrderId, Boolean refund) {
        DBValidation dbValidation = new DBValidation();
        List<Map<String, Object>> extendedSubWalletList;
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.MERCHANTORDERID, merchantOrderId);
        if (refund) {
            extendedSubWalletList = dbValidation.runDbQuery(DBQueries.extendedSubWalletDetailsRefund, params,"log4j");
        } else {
            extendedSubWalletList = dbValidation.runDbQuery(DBQueries.extendedSubWalletDetails, params,"log4j");
        }

        return extendedSubWalletList;
    }

    private List<Map<String, Object>> getSubWalletList(List<Map<String, Object>> extendedSubWalletList, Boolean refundTxn) {
        List<Map<String, Object>> subWalletList = new LinkedList<>();
        for (String prioritySubWallet : updatedPriorityList(refundTxn)) {
            for (Map<String, Object> subWalletData : extendedSubWalletList) {
                if (subWalletData.get("ppi_type").toString().equalsIgnoreCase(prioritySubWallet)) {
                    subWalletList.add(subWalletData);
                }
            }
        }
        return subWalletList;
    }

    private static LinkedList<String> updatedPriorityList(Boolean refundTxn) {
        LinkedList<String> priorityList;
        if (refundTxn) {
            priorityList = new LinkedList<>(SubWalletHelpers.reversePriorityList());
            priorityList.addFirst("5");
        } else {
            priorityList = new LinkedList<>(SubWalletHelpers.priorityList());
            priorityList.add("5");
        }
        return priorityList;
    }

    private BigDecimal getTotalSubWalletAmt(List<Map<String, Object>> extendedSubWalletList, Boolean refundTxn) {
        BigDecimal totalSubWalletAmt = java.math.BigDecimal.ZERO;
        for (String prioritySubWallet : updatedPriorityList(refundTxn)) {
            for (Map<String, Object> subWalletData : extendedSubWalletList) {
                if (subWalletData.get("ppi_type").toString().equalsIgnoreCase(prioritySubWallet)) {
                    totalSubWalletAmt = totalSubWalletAmt.add(new BigDecimal(subWalletData.get("txn_amount").toString()));
                }
            }
        }
        return totalSubWalletAmt;
    }

    private LinkedList<Map<Object, Object>> createExpectedMap(ScratchPadWallet scratchPadWallet, String guid, BigDecimal amount, String merchantOrderId) {
        LinkedList<Map<Object, Object>> expectedWalletList = new LinkedList<>();
        LinkedList<LinkedList<String>> walletOrder = scratchPadWallet.getWalletList();
        LinkedList<String> commissionImpactedWallet = scratchPadWallet.getimpactedWallet();
        BigDecimal commissionAmt = getCommissionAmt(merchantOrderId);
        BigDecimal totalSubWalletAmt = BigDecimal.ZERO;
        List<Map<String, Object>> extendedSubWalletList = new LinkedList<>();
        String impactedWalletInfo = null;
        String impactedWalletPymtMode = null;
        if (null != commissionImpactedWallet) {
            impactedWalletInfo = commissionImpactedWallet.get(0);
            impactedWalletPymtMode = commissionImpactedWallet.get(1);
        }
        BigDecimal updatedAmount = amount;
        if(commissionAmt.compareTo(BigDecimal.ZERO)>0 && null!=impactedWalletPymtMode && impactedWalletPymtMode.equalsIgnoreCase("CREDIT")){
            Boolean merchantType = getMerchantType(guid);
/*
            if(!merchantType) {
                updatedAmount = amount.subtract(commissionAmt);
            }
*/
            updatedAmount = amount.subtract(commissionAmt);
        }
        Boolean multiPPITxn = false;
        Boolean refund = !walletOrder.get(0).get(0).equalsIgnoreCase("SUBWALLET");
        if (scratchPadWallet.getClass().getSimpleName().equalsIgnoreCase("SubWalletMain")) {
            multiPPITxn = true;
            List<Map<String, Object>> dbExtendedSubWalletList = getDBExtendedSubWalletList(merchantOrderId, refund);
            totalSubWalletAmt = getTotalSubWalletAmt(dbExtendedSubWalletList, refund);
            extendedSubWalletList = getSubWalletList(dbExtendedSubWalletList, refund);
        }
        for (LinkedList<String> expectedWallet : walletOrder) {
            mapGenerator:
            {
                if (expectedWallet.get(0).contains("SUBWALLET") && multiPPITxn) {
                    for (Map<String, Object> subWallet : extendedSubWalletList) {
                        BigDecimal subAmount = new BigDecimal(subWallet.get("txn_amount").toString());
                        if (expectedWallet.get(1).equalsIgnoreCase("DEBIT")) {
                            subAmount = subAmount.negate();
                            expectedWalletList.add(generateSubWalletMap(Long.parseLong(subWallet.get("wallet_id").toString()), subAmount, Integer.parseInt(subWallet.get("ppi_type").toString())));
                        } else {
                            expectedWalletList.add(generateSubWalletMap(Long.parseLong(subWallet.get("wallet_id").toString()), subAmount, Integer.parseInt(subWallet.get("ppi_type").toString())));
                        }
                    }
                    break mapGenerator;
                }
                if (multiPPITxn && !refund && expectedWallet.get(0).equalsIgnoreCase("PAYER_USER") && expectedWallet.get(1).equalsIgnoreCase("Credit")) {
                    expectedWalletList.add(generateWalletMap(expectedWallet, UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER), guid, totalSubWalletAmt));
                    break mapGenerator;
                } else if (multiPPITxn && refund && expectedWallet.get(0).equalsIgnoreCase("PAYER_USER") && expectedWallet.get(1).equalsIgnoreCase("Debit")) {
                    expectedWalletList.add(generateWalletMap(expectedWallet, UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER), guid, totalSubWalletAmt));
                    break mapGenerator;
                }
                if (expectedWallet.get(0).contains("COMISSION_EARNED_WALLET")) {
                    if (null == commissionAmt || commissionAmt.compareTo(BigDecimal.ZERO) == 0.0) {
                        break mapGenerator;
                    } else {
                        expectedWalletList.add(generateWalletMap(expectedWallet, UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER), guid, commissionAmt));
                        break mapGenerator;
                    }
                }
                if(expectedWallet.get(0).contains("SYSTEM-COMISSION")){
                    if (null == commissionAmt || commissionAmt.compareTo(BigDecimal.ZERO) == 0.0) {
                        break mapGenerator;
                    } else {
                        expectedWallet.set(0,expectedWallet.get(0).split("-",-1)[0]);
                        expectedWalletList.add(generateWalletMap(expectedWallet, UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER), guid, commissionAmt));
                        break mapGenerator;
                    }
                }
                if (null != impactedWalletInfo && expectedWallet.get(0).equalsIgnoreCase(impactedWalletInfo) && !expectedWallet.get(1).equalsIgnoreCase(impactedWalletPymtMode)) {
                    if (null != commissionAmt && commissionAmt.compareTo(BigDecimal.ZERO) != 0.0) {
                        if (impactedWalletPymtMode.equalsIgnoreCase("DEBIT"))
                            updatedAmount = updatedAmount.subtract(commissionAmt);
                        else
                            updatedAmount = updatedAmount.add(commissionAmt);
                    }
                }
                if (expectedWallet.get(0).equalsIgnoreCase("PAYER_USER")) {
                    expectedWalletList.add(generateWalletMap(expectedWallet, UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER), guid, updatedAmount));
                } else if (expectedWallet.get(0).equalsIgnoreCase("PAYEE_USER")) {
                    expectedWalletList.add(generateWalletMap(expectedWallet, UserManager.getInstance().getPayeeUserDetails(UserHeaders.MOBILENUMBER), guid, updatedAmount));
                } else {
                    expectedWalletList.add(generateWalletMap(expectedWallet, UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER), guid, updatedAmount));
                }
            }
        }
        return expectedWalletList;
    }

    private Map<Object, Object> generateSubWalletMap(Long walletID, BigDecimal amount, int ppiType) {
        Map<Object, Object> expectedWalletData = new LinkedHashMap<>();
        expectedWalletData.put("wallet_id", walletID);
        expectedWalletData.put("deviation", amount);
        expectedWalletData.put("ppi_type", ppiType);
        //expectedWalletData.put("walletType",1);
        return expectedWalletData;
    }

    private Map<Object, Object> generateWalletMap(LinkedList<String> expectedWallet, String PhoneNumber, String guid, BigDecimal amount) {
        Map<Object, Object> expectedWalletData = new LinkedHashMap<>();
        String walletInfo = expectedWallet.get(0);
        String paymentMode = expectedWallet.get(1);
        expectedWalletData.put("wallet_id", getWalletID(PhoneNumber, walletInfo, guid));
        if (paymentMode.equalsIgnoreCase("CREDIT")) {
            expectedWalletData.put("deviation", amount.setScale(2,BigDecimal.ROUND_HALF_UP));
        } else if (paymentMode.equalsIgnoreCase("DEBIT")) {
            expectedWalletData.put("deviation", amount.setScale(2,BigDecimal.ROUND_HALF_UP).negate());
        }
        expectedWalletData.put("ppi_type", 0);
        return expectedWalletData;
    }

    private Long getWalletID(String PhoneNumber, String walletType, String guid) {
        switch (walletType) {
            case "SYSTEM":
                return this.System_WalletID;
            case "PAYEE_USER":
            case "PAYER_USER":
                return Long.parseLong(UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETID, PhoneNumber));
            case "MERCHANT_WALLET":
                DBValidation dbValidation = new DBValidation();
                Map<Object, Object> params = new HashMap<>();
                params.put(DBEnums.WALLETGUID, guid);
                return Long.parseLong(dbValidation.runDbQuery(DBQueries.merchantWalletId, params,"log4j").get(0).get("id").toString());
            case "COMISSION_EARNED_WALLET":
                return this.COMISSION_EARNED_WALLETID;
            case "ONE97_SYSTEM_WALLET":
                return this.ONE97_SYSTEM_WALLETID;
            default:
                throw new RuntimeException("New walletType encountered" + walletType);
        }

    }

    private LinkedList<Map<Object, Object>> getScratchPadData(String merchantOrderId) {
        DBValidation dbValidation = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.MERCHANTORDERID, merchantOrderId);
        LinkedList<Map<Object, Object>> scratchPadentries = new LinkedList<>();
        List<Map<String, Object>> actualScratchPadEnteries = dbValidation.runWalletTrailDbQuery(DBQueries.getScratchPadMaster, params,"log4j");
        for (Map<String, Object> singleScratchPadEntry : actualScratchPadEnteries) {
            Map<Object, Object> testing = new HashMap<>();
            for (String key : singleScratchPadEntry.keySet()) {
                if (key.equalsIgnoreCase("deviation")) {
                    testing.put(key, new BigDecimal(singleScratchPadEntry.get(key).toString()).setScale(2));
                } else {
                    testing.put(key, singleScratchPadEntry.get(key));
                }
            }
            scratchPadentries.add(testing);
        }
        //scratchPadentries.addAll((Collection<? extends Map<Object, Object>>)dbValidation.runDbQuery(DBQueries.getScratchPadMaster,params));
        return scratchPadentries;

    }


    public void validateScratchPad(ScratchPadWallet scratchPadWallet, String guid, String merchantOrderId) {
        BigDecimal amount = FundThreadManager.getInstance().getTotalAmount();
        LinkedList<Map<Object, Object>> expectedScratchPadEnteries = createExpectedMap(scratchPadWallet, guid, amount,
                merchantOrderId);
        LinkedList<Map<Object, Object>> actualScratchPadEnteries = getScratchPadData(merchantOrderId);
        if (expectedScratchPadEnteries.size() == actualScratchPadEnteries.size()) {
            Boolean flag = false;
            for (Map<Object,Object> expectedEntry: expectedScratchPadEnteries) {
                List<Map<Object,Object>> actualScratchPadEnteriesCopy = new ArrayList<>(actualScratchPadEnteries);
                for(Map<Object,Object> actualEntry : actualScratchPadEnteriesCopy){
                    try {
                        CommonHelpers.getInstance().compareObjectMap(actualEntry,
                                expectedEntry,"Scratchpad entry mismatch");
                        flag=true;
                        actualScratchPadEnteries.remove(actualEntry);
                        break;
                    }catch(AssertionError e){
                        flag=false;
                    }
                }
                if(flag==false){
                    AssertionValidations.verifyAssertFail("ScratchPad entry mismatch : "+expectedEntry);
                }
            }
        } else {
            AssertionValidations.verifyAssertFail("Expected scratpad size:" + expectedScratchPadEnteries.size()
                    + " is different from Actual ScratchPad size:" +actualScratchPadEnteries.size());
        }
    }

}