package com.paytm.helpers;

import static io.restassured.RestAssured.given;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.index.analysis.GermanStemTokenFilterFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.paytm.apiPojo.oauthAPI.GetBeneficiaryResponse;
import com.paytm.apiPojo.oauthAPI.GetEncryptCustIdResponse;
import com.paytm.apiRequestBuilders.oauthAPI.CreatePassword;
import com.paytm.apiRequestBuilders.oauthAPI.QueryUserInfo;
import com.paytm.apiRequestBuilders.oauthAPI.ResetPassword;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.Constants;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.UserHeaders;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.utils.DatabaseUtil;
import com.paytm.framework.utils.ServerUtil;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class AuthHelpers {

	private String authorizeUser(String mobileNo) {
		String body = "response_type=code&client_id=" + LocalConfig.AUTH_CLIENT_NAME + "&scope=wallet&username="
				+ mobileNo + "&password=" + LocalConfig.AUTH_USER_PASSWORD + "&do_not_redirect=true";
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		BaseApi baseApi = new BaseApi();
		baseApi.setMethod(BaseApi.MethodType.POST);
		RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.URLENC);
		requestSpecBuilder.addHeader("Authorization", authorization);
		requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.AUTHORIZE);
		requestSpecBuilder.setBody(body);
		Response response = baseApi.execute();
		if (response.jsonPath().getString("statusMessage") != null) {
			String resultCode = response.jsonPath().getString("statusMessage");
			if (resultCode.equals("Please enter valid Username and Password.")) {
				resetUserPassword(mobileNo);
				response = baseApi.execute();
			}
		}
		String code = response.jsonPath().get("code");
		if (null == code || code.isEmpty()) {
			throw new RuntimeException("Code either null or empty: " + code);
		}
		return code;
	}

	public String userIdFromToken() {
		BaseApi baseApi = new BaseApi();
		String userId = null;
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		{
			baseApi.setMethod(BaseApi.MethodType.GET);
			RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
			requestSpecBuilder.setContentType(ContentType.URLENC);
			requestSpecBuilder.addHeader("Authorization", authorization);
			requestSpecBuilder.addHeader("verification_type", "oauth_token");
			requestSpecBuilder.addHeader("data",
					UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN).toString());
			requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
			requestSpecBuilder.setBasePath(Constants.AuthAPIresource.V2USERID);
			requestSpecBuilder.addFormParam("fetch_strategy", "userId");
		}
		Response userResponse = baseApi.execute();
		System.out.println("userResponse is  " + userResponse);
		try {
			Long userIds = userResponse.jsonPath().get("userId");
			userId = userIds.toString();
		} catch (ClassCastException e) {
			int userIds = (int) userResponse.jsonPath().get("userId");
			userId = Integer.toString(userIds);
		}
		return userId;
	}

	private String oauth2Token(String code) {
		BaseApi baseApi = new BaseApi();
		String accessToken = null;
		String bodyToken = "grant_type=authorization_code&code=" + code;
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		baseApi.setMethod(BaseApi.MethodType.POST);
		RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.URLENC);
		requestSpecBuilder.addHeader("Authorization", authorization);
		requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.TOKEN);
		requestSpecBuilder.setBody(bodyToken);
		Response responseToken = baseApi.execute();
		accessToken = responseToken.jsonPath().get("access_token");
		if (accessToken == null || accessToken.isEmpty()) {
			throw new RuntimeException("SSO Token is either null or empty: " + accessToken);
		}
		return accessToken;
	}

	private String oauth2UserTokens(String accessToken) {
		BaseApi baseApi = new BaseApi();
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		baseApi.setMethod(BaseApi.MethodType.GET);
		RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
		requestSpecBuilder.addHeader("Authorization", authorization);
		requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.OAUTH2USERTOKENS + "/" + accessToken + "");
		Response tokenList = baseApi.execute();
		JsonObject responseObject = new JsonParser().parse(tokenList.asString()).getAsJsonObject();
		JsonArray jsonArray = (JsonArray) responseObject.get("tokens");
		String token = null;
		for (int i = 0; i < jsonArray.size(); i++) {
			JsonObject jsonObject = (JsonObject) jsonArray.get(i);
			if (jsonObject.get("scope").getAsString().equals("wallet")) {
				token = String.valueOf(jsonObject.get("access_token").getAsString());
			}
		}
		return token;
	}

	public String getTokenStatus(String accessToken) {
		BaseApi baseApi = new BaseApi();
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		baseApi.setMethod(BaseApi.MethodType.GET);
		RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
		requestSpecBuilder.addHeader("Authorization", authorization);
		requestSpecBuilder.addHeader("session_token", accessToken);
		requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.TOKENSTATUS);
		Response tokenList = baseApi.execute();
		return tokenList.getBody().asString();
	}

	public String getPaytmAccessToken(String mobileNo) {
		String code = authorizeUser(mobileNo);
		String token = oauth2Token(code);
		return token;
	}

	public String getWalletAccessToken(String mobileNo) {
		String code = authorizeUser(mobileNo);
		String token = oauth2Token(code);
		String walletToken = oauth2UserTokens(token);
		return walletToken;
	}

	public boolean isUserPresent(String mobileNo) {

		String body = "{\n" + "\"phone\":" + mobileNo + "\n" + "}";
		BaseApi baseApi = new BaseApi();
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		baseApi.setMethod(BaseApi.MethodType.POST);
		RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeader("Authorization", authorization);
		requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.CHECKUSEREXISTANCE);
		requestSpecBuilder.setBody(body);
		Response response = baseApi.execute();
		String responseCode = String.valueOf(response.getStatusCode());

		if (responseCode.equals("409")) {
			return true;
		} else {
			return false;
		}
	}

	public String getOtpForSignUp(String mobileNo) {

		String query = "select otp from otp_action where phone =" + mobileNo
				+ " and status = 0 order by create_timestamp desc limit 1";
		System.out.println("query for OTP" + query);

		List<Map<String, Object>> result = DatabaseUtil.getInstance()
				.executeSelectQuery(Constants.DBConnectionURL.AUTH_DB_CONNECTION_URL, query);
		String otp = null;
		if (!result.isEmpty()) {
			otp = (String) result.get(0).get("otp");
		} else {
			String key = "tail -1000 /paytm/logs/pgproxy-notification.log | grep --line-buffered ValueToSubstitue | grep --line-buffered alipayplus.communication.sms.send | grep --line-buffered  isyourverificationcodetoverifyyour | awk -F'\"content\":\"|isyourverificationcodetoverifyyour' '{if(NF > 1) print $2}' | tail -1";
			otp = getOtpFromLogs(mobileNo, key);
		}
		return otp;
	}

	public String getOtpFromLogs(String phone, String key) {
		ServerUtil serverUtil = new ServerUtil();
		Session session = serverUtil.getSession(LocalConfig.NOTIFICATION_CONNECTION_URL);
		String otp = null;
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int retryCount = 3;
		while (retryCount > 0) {
			try {
				ChannelExec channel = (ChannelExec) session.openChannel("exec");
				phone = phone.replace("+91", "");
				String newKey = key.replace("ValueToSubstitue", phone);
				channel.setCommand(newKey);
				channel.connect();
				BufferedReader reader = new BufferedReader(new InputStreamReader(channel.getInputStream()));
				otp = reader.readLine();
				System.out.println("otp is " + otp);
				if (!(otp == null)) {
					return otp;
				}
			} catch (JSchException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			retryCount--;
		}

		return otp;
	}

	private String fetchAlipayUserId(String phone) {
		QueryUserInfo queryUser = new QueryUserInfo(QueryUserInfo.defaultRequest);
		String userId = null;
		try {
			queryUser.getRequestPojo().getRequest().getBody().getLoginIdInfo().setLoginId("91-" + phone);
			queryUser.createRequestJsonAndExecute();
			userId = queryUser.getResponsePojo().getResponse().getBody().getUserInfo().getUserId();
		} catch (Exception e) {
			throw new RuntimeException("Not able to get the UserId " + queryUser.getApiResponse());
		}
		return userId;
	}

	public void resetUserPassword(String phone) {
		String userId = fetchAlipayUserId(phone);
		ResetPassword resetPassword = new ResetPassword(ResetPassword.defaultRequest);
		resetPassword.getRequestPojo().getRequest().getBody().setUserId(userId);
		resetPassword.createRequestJsonAndExecute();
		String resultCode = resetPassword.getResponsePojo().getResponse().getBody().getResultInfo().getResultCode();
		System.out.println("Password not set successfully as response is " + resetPassword.getApiResponse());
		if (resultCode.equals("SUCCESS"))
			System.out.println("Password Reset successfully");
		else
			createUserPassword(userId);
	}

	private void createUserPassword(String userId) {
		CreatePassword createPassword = new CreatePassword(CreatePassword.defaultRequest);
		createPassword.getRequestPojo().getRequest().getBody().setUserId(userId);
		createPassword.createRequestJsonAndExecute();
		String resultCode = createPassword.getResponsePojo().getResponse().getBody().getResultInfo().getResultCode();
		System.out.println("Password not set successfully as response is " + createPassword.getApiResponse());
		if (resultCode.equals("SUCCESS"))
			System.out.println("Password Created successfully");
		else
			throw new RuntimeException("Not able to create the Password " + createPassword.getApiResponse());
	}

	public List<GetBeneficiaryResponse.Data> getBenificiaries(String phoneNumber) {
		BaseApi baseApi = new BaseApi();
		baseApi.setMethod(BaseApi.MethodType.GET);
		RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
		requestSpecBuilder.addHeader("session_token",
				UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phoneNumber));
		requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.GETBENEFICIARY);
		Response response = baseApi.execute();
		AssertionValidations.verifyAssertEqual(response.getStatusCode(), 200);
		GetBeneficiaryResponse getBeneficiaryResponse = null;
		try {
			getBeneficiaryResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					GetBeneficiaryResponse.class);
			System.out.println(getBeneficiaryResponse);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return getBeneficiaryResponse.getData();
	}
	
	public Response getOfflineCode(String ssoToken) {
		String uri = LocalConfig.AUTH_HOST
				+ "offline/config?deviceIdentifier=XOLO-A500sLite-911372002696178&amp;deviceManufacturer=Apple&amp;deviceName=iPhone&amp;client=androidapp&amp;version=4.8.0&amp;lat=13.0945787&amp;long=77.5618789&amp;imei=911372002696178&amp;osVersion=4.2.2";
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		String jsonBody = "{\"deviceId\":\"haaagagfagfga7627627622\"}";

		Response response = given().header("Content-Type", "application/json").header("Authorization", authorization)
				.header("session_token", ssoToken).header("version", "2").body(jsonBody).when().post(uri);
		
		return response;
	}

	public String verifyDeviceAndGetOfflineCode(String mobileNumber, String ssoToken) {
		String code = null;
		String offline_header = null;
		
		Response response = getOfflineCode(ssoToken);
		
		System.out.println("Response is " + response.getBody().asString());

		if (response.jsonPath().getString("responseCode").equals("815")) {
			markDeviceVerified(mobileNumber, ssoToken);
			response = getOfflineCode(ssoToken);
			
			System.out.println("Response is " + response.getBody().asString());
			
		} 
		
		code = response.jsonPath().getString("ud_map");
		offline_header = response.jsonPath().getString("cgcp_offline_header");
		
		return (offline_header + code);
	}

	public void markDeviceVerified(String phone, String ssoToken) {
		OtpHelpers otpHelpers = new OtpHelpers();
		String sendOtpUri = LocalConfig.AUTH_HOST
				+ "api/sendOtp?deviceIdentifier=XOLO-A500sLite-911372002696178&amp;amp;deviceManufacturer=Apple&amp;amp;deviceName=iPhone&amp;amp;client=androidapp&amp;amp;version=4.8.0&amp;amp;lat=13.0945787&amp;amp;long=77.5618789&amp;amp;imei=911372002696178&amp;amp;osVersion=4.2.2";
		String sendOtpJsonBody = "{\"evaluationType\":\"DEVICE_VERIFICATION\"}";

		Response sendOtpResponse = given().header("Content-Type", "application/json").header("session_token", ssoToken)
				.body(sendOtpJsonBody).when().post(sendOtpUri);

		System.out.println("Response of sendOtp API is " + sendOtpResponse.getBody().asString());

		String code = sendOtpResponse.jsonPath().getString("code");
		String otp = null;

		otp = otpHelpers.FetchOtp(phone, "DEVICE_P2P_OTP");
		System.out.println("otp is:" + otp);
		
		String validateOtpUri = LocalConfig.AUTH_HOST
				+ "api/validateOtp?deviceIdentifier=XOLO-A500sLite-911372002696178&amp;amp;deviceManufacturer=Apple&amp;amp;deviceName=iPhone&amp;amp;client=androidapp&amp;amp;version=4.8.0&amp;amp;lat=13.0945787&amp;amp;long=77.5618789&amp;amp;imei=911372002696178&amp;amp;osVersion=4.2.2";
		String validateOtpJsonBody = "{\n" + " \"code\": \"" + code + "\",\n" + " \"otp\": \"" + otp + "\",\n"
				+ " \"evaluationType\": \"DEVICE_VERIFICATION\"\n" + "}";
		
		System.out.println(validateOtpUri);
		
		System.out.println(validateOtpJsonBody);

		Response validateOtpResponse = given().header("Content-Type", "application/json")
				.header("session_token", ssoToken).header("type", "mobile").body(validateOtpJsonBody).when()
				.post(validateOtpUri);
		
		System.out.println("Response of validateOtp API is " + validateOtpResponse.getBody().asString());
	}
	
	/**
    *
    * Get encrypted custId from oauth for the given user ssoToken
    *
    * @param ssoToken - ssoToken of user
    * @return encryptedCustId
    */
	public String getEncryptedCustId(String ssoToken) {
		String encryptedCustId = "";
		
		String authorization = LocalConfig.AUTH_CLIENT_AUTHENTICATION;
		Map<String, String> params = new HashMap<>();
		String encodeURL = null;
		try {
			encodeURL = URLDecoder.decode("USERID%2CENC_USER_ID_PER_CLIENT%5Bmerchant-effkon-staging%5D", "UTF-8" );
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		params.put("fetch_strategy", encodeURL);
		BaseApi baseApi = new BaseApi();
		baseApi.setMethod(BaseApi.MethodType.GET);
		RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
		requestSpecBuilder.addHeader("data",ssoToken);
		requestSpecBuilder.addHeader("Authorization",authorization);
		requestSpecBuilder.addHeader("verification_type","oauth_token");
		requestSpecBuilder.setBaseUri(LocalConfig.AUTH_HOST);
		requestSpecBuilder.setBasePath("v2/user");
		requestSpecBuilder.addParams(params);
		Response response = baseApi.execute();
		
		GetEncryptCustIdResponse getEncryptCustIdResponse = null;
		try {
			getEncryptCustIdResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					GetEncryptCustIdResponse.class);
			System.out.println(getEncryptCustIdResponse);
			encryptedCustId = getEncryptCustIdResponse.getEncUserIds().get(0).getUserId();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		
		return encryptedCustId;
	}

	public static void main(String[] args) {
		AuthHelpers authHelpers = new AuthHelpers();
		String temp = authHelpers.getEncryptedCustId("2f56e0de-535a-4be5-898f-059297743400");
		System.out.println(temp);
	}
}