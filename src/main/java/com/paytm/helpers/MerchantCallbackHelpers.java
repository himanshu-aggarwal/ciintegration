package com.paytm.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.Session;
import com.paytm.assertions.AssertionValidations;
import com.paytm.framework.utils.ServerUtil;
import com.paytm.constants.LocalConfig;

public class MerchantCallbackHelpers {

	private String getCallBackUrl(String txnId) {
		String count = null;
		String key = "cd /tmp; tail -100 /tmp/wallet-message-consumer.log | grep --line-buffered \"Request prepare to be sent to server url ::http://www.paytm.com\" | grep --line-buffered ValueToSubstitue | wc -l";

		try {
			Thread.sleep(2000);
			ServerUtil serverUtil = new ServerUtil();
			Session session = serverUtil.getSession(LocalConfig.MERCHANT_CALLBACK_LOG_URL);
			ChannelExec channel = (ChannelExec) session.openChannel("exec");
			String newKey = key.replace("ValueToSubstitue", txnId);
			System.out.println("Command is " + newKey);
			channel.setCommand(newKey);
			channel.connect();
			BufferedReader reader = new BufferedReader(new InputStreamReader(channel.getInputStream()));
			count = reader.readLine();
			channel.disconnect();
			session.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	public void validateCallbackUrl(String txnId) {
		String count = getCallBackUrl(txnId);
		AssertionValidations.verifyAssertEqual(count,"1");
	}

}
