package com.paytm.helpers;

import com.paytm.apiPojo.AuditInfo;
import com.paytm.apiPojo.AuditInfo.LimitAuditInfo;
import com.paytm.apiPojo.AuditInfo.LimitAuditInfo.LimitDetail.LimitCounter;
import com.paytm.apiPojo.LimitConfig;
import com.paytm.apiRequestBuilders.wallet.oldService.CheckUserBalance;
import com.paytm.apiPojo.AuditInfo.LimitAuditInfo.LimitDetail;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundManager;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.enums.LimitEnums.*;
import com.paytm.exceptions.LimitsException.SystemLimits.LimitAuditInfoValidationException;
import com.paytm.exceptions.LimitsException.SystemLimits.LimitAuditInfoOperationTypeNotFound;
import com.paytm.utils.DBValidation;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class LimitAuditInfoValidation{

    private String beforeJson;
    private String beforeTimeStamp;
    private String afterTimeStamp;
    private BigDecimal addedBalance;

    private LimitDetail validateLimitDetailsAndCounter(LimitDetail limitDetail, int txnType, Short operationType,
                                                                      String timeStampFormat) {
        if (limitDetail == null || CommonHelpers.getInstance().checkIfCountersNeedToBeRolledOver(timeStampFormat,
                this.beforeTimeStamp, this.afterTimeStamp)) {
            limitDetail = LimitDetail.createDefaultLimitDetail(txnType, operationType);
        } else if (limitDetail.getLimitCounters().get(Integer.toString(txnType)) == null) {
            LimitCounter counter = new LimitCounter();
            Map<String, List<LimitCounter>> limitCounterMap = counter.limitCounterMap(txnType, operationType);
            limitDetail.getLimitCounters().putAll(limitCounterMap);
        } else if (limitDetail.getLimitCounters().get(Integer.toString(txnType)).size() == 1 && limitDetail
                .getLimitCounters().get(Integer.toString(txnType)).get(0).getOperationType() != operationType) {
            LimitCounter counter = new LimitCounter();
            limitDetail.getLimitCounters().get(Integer.toString(txnType))
                    .add(counter.createLimitCounter(operationType));
        }
        return limitDetail;
    }

    private LimitCounter getLimitCounter(List<LimitCounter> limitCounter, Short operationType) {
        for (int i = 0; i < limitCounter.size(); i++) {
            if (limitCounter.get(i).getOperationType() == operationType) {
                return limitCounter.get(i);
            }
        }
        throw new LimitAuditInfoOperationTypeNotFound("Required Operation type not found i.e. " + operationType);
    }

    public void validateAllLimitDetails(LimitAuditInfo limitAudit, int txnType, Short operationType) {
        limitAudit.setDayLimit(
                validateLimitDetailsAndCounter(limitAudit.getDayLimit(), txnType, operationType, "yyyy-MM-dd"));
        limitAudit.setMonthLimit(
                validateLimitDetailsAndCounter(limitAudit.getMonthLimit(), txnType, operationType, "yyyy-MM"));
        limitAudit.setYearlyLimit(
                validateLimitDetailsAndCounter(limitAudit.getYearlyLimit(), txnType, operationType, "yyyy"));
    }

    private LimitCounter getDurationWiseLimitCounter(int txnType, LimitAuditInfo limitAudit, Short operationType,
                                                     LimitPeriodEnums period) {
        switch (period.toString()) {
            case "day":
                if (limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)) != null) {
                    try {
                        return getLimitCounter(limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)),
                                operationType);
                    } catch (LimitAuditInfoOperationTypeNotFound e) {
                        return null;
                    }
                }
                break;
            case "month":
                if (limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)) != null) {
                    try {
                        return getLimitCounter(limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)),
                                operationType);
                    } catch (LimitAuditInfoOperationTypeNotFound l) {
                        return null;
                    }
                }
                break;
            case "year":
                if (limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)) != null) {
                    try {
                        return getLimitCounter(
                                limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)), operationType);
                    } catch (LimitAuditInfoOperationTypeNotFound l) {
                        return null;
                    }
                }
                break;
        }
        return null;
    }

    private BigDecimal getAmount(int txnType, LimitAuditInfo limitAudit, Short operationType, int ppiGroup,
                                 LimitPeriodEnums period) {
        LimitCounter limitCounter = getDurationWiseLimitCounter(txnType, limitAudit, operationType, period);
        BigDecimal value = BigDecimal.ZERO;
        if (limitCounter != null) {
            if (ppiGroup == 0)
                value = limitCounter.getAmount();
            else
                value = limitCounter.getPpiGroupWiseCounts().get(ppiGroup);
        }
        return value;
    }

    private int getCount(int txnType, LimitAuditInfo limitAudit, Short operationType, LimitPeriodEnums period) {
        LimitCounter limitCounter = getDurationWiseLimitCounter(txnType, limitAudit, operationType, period);
        int value = 0;
        if(limitCounter!=null) {
            value = limitCounter.getCount();
        }
        return value;
    }

    private void setDayAmountLimit(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        BigDecimal value;
        LimitCounter limitCounter = getDurationWiseLimitCounter(txnType, limitAudit, operationType,LimitPeriodEnums.day);
        value = limitCounter.getAmount();
        BigDecimal updatedValue = value.add(amount);
        limitCounter.setAmount(updatedValue);
    }

    private void setMonthAmountLimit(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        BigDecimal value;
        LimitCounter limitCounter = getDurationWiseLimitCounter(txnType, limitAudit, operationType, LimitPeriodEnums.month);
        value = limitCounter.getAmount();
        BigDecimal updatedValue = value.add(amount);
        limitCounter.setAmount(updatedValue);
    }

    private void setYearAmountLimit(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        BigDecimal value;
        LimitCounter limitCounter = getDurationWiseLimitCounter(txnType, limitAudit, operationType,LimitPeriodEnums.year);
        value = limitCounter.getAmount();
        BigDecimal updatedValue = value.add(amount);
        limitCounter.setAmount(updatedValue);
    }

    private void setDayCountLimit(int txnType, int count, LimitAuditInfo limitAudit, Short operationType) {
        int currentCount = 0;
        currentCount = getLimitCounter(limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)),
                operationType).getCount();
        int updatedCount = currentCount + count;
        getLimitCounter(limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .setCount(updatedCount);
    }

    private void setMonthCountLimit(int txnType, int count, LimitAuditInfo limitAudit, Short operationType) {
        int currentCount = 0;
        currentCount = getLimitCounter(limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)),
                operationType).getCount();
        int updatedCount = currentCount + count;
        getLimitCounter(limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .setCount(updatedCount);
    }

    private void setYearCountLimit(int txnType, int count, LimitAuditInfo limitAudit, Short operationType) {
        int currentCount = 0;
        currentCount = getLimitCounter(limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)),
                operationType).getCount();
        int updatedCount = currentCount + count;
        getLimitCounter(limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .setCount(updatedCount);
    }

    private void setDayLimit1(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        LimitCounter limitCounter = getLimitCounter(
                limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)), operationType);
        BigDecimal value = limitCounter.getPpiGroupWiseCounts().get(1);
        BigDecimal updatedValue = value.add(amount);
        limitCounter.getPpiGroupWiseCounts().put(1, updatedValue);
    }

    private void setMonthLimit1(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        LimitCounter limitCounter = getLimitCounter(
                limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)), operationType);
        BigDecimal value = limitCounter.getPpiGroupWiseCounts().get(1);
        BigDecimal updatedValue = value.add(amount);
        limitCounter.getPpiGroupWiseCounts().put(1, updatedValue);
    }

    private void setYearLimit1(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        LimitCounter limitCounter = getLimitCounter(
                limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)), operationType);
        BigDecimal value = limitCounter.getPpiGroupWiseCounts().get(1);
        BigDecimal updatedValue = value.add(amount);
        limitCounter.getPpiGroupWiseCounts().put(1, updatedValue);
    }

    private void setDayLimit2(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        LimitCounter limitCounter = getLimitCounter(
                limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)), operationType);
        BigDecimal value = limitCounter.getPpiGroupWiseCounts().get(2);
        BigDecimal updatedValue = value.add(amount);
        limitCounter.getPpiGroupWiseCounts().put(2, updatedValue);
    }

    private void setMonthLimit2(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        LimitCounter limitCounter = getLimitCounter(
                limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)), operationType);
        BigDecimal value = limitCounter.getPpiGroupWiseCounts().get(2);
        BigDecimal updatedValue = value.add(amount);
        limitCounter.getPpiGroupWiseCounts().put(2, updatedValue);
    }

    private void setYearLimit2(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        LimitCounter limitCounter = getLimitCounter(
                limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)), operationType);
        BigDecimal value = limitCounter.getPpiGroupWiseCounts().get(2);
        BigDecimal updatedValue = value.add(amount);
        limitCounter.getPpiGroupWiseCounts().put(2, updatedValue);
    }

    private void setDayRollbackLimit(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        BigDecimal value = BigDecimal.ZERO;
        if (getLimitCounter(limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .getRollbackAmount() != null)
            value = getLimitCounter(limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)),
                    operationType).getRollbackAmount();
        BigDecimal updatedValue = value.add(amount);
        getLimitCounter(limitAudit.getDayLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .setRollbackAmount(updatedValue);
    }

    private void setMonthRollbackLimit(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        BigDecimal value = BigDecimal.ZERO;
        if (getLimitCounter(limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .getRollbackAmount() != null)
            value = getLimitCounter(limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)),
                    operationType).getRollbackAmount();
        BigDecimal updatedValue = value.add(amount);
        getLimitCounter(limitAudit.getMonthLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .setRollbackAmount(updatedValue);
    }

    private void setYearRollbackLimit(int txnType, BigDecimal amount, LimitAuditInfo limitAudit, Short operationType) {
        BigDecimal value = BigDecimal.ZERO;
        if (getLimitCounter(limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)),
                operationType).getRollbackAmount() != null)
            value = getLimitCounter(limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)),
                    operationType).getRollbackAmount();
        BigDecimal updatedValue = value.add(amount);
        getLimitCounter(limitAudit.getYearlyLimit().getLimitCounters().get(Integer.toString(txnType)), operationType)
                .setRollbackAmount(updatedValue);
    }

    // Setting the limits
    private void setLimitAuditInfo(LimitAuditInfo limitAudit, BigDecimal amount, BigDecimal l1Amount,
                                   BigDecimal l2Amount, int count, int txnType, Short operationType) {
        validateAllLimitDetails(limitAudit, txnType, operationType);
        setDayAmountLimit(txnType, amount, limitAudit, operationType);
        setMonthAmountLimit(txnType, amount, limitAudit, operationType);
        setYearAmountLimit(txnType, amount, limitAudit, operationType);
        setDayCountLimit(txnType, count, limitAudit, operationType);
        setMonthCountLimit(txnType, count, limitAudit, operationType);
        setYearCountLimit(txnType, count, limitAudit, operationType);
        setDayLimit1(txnType, l1Amount, limitAudit, operationType);
        setMonthLimit1(txnType, l1Amount, limitAudit, operationType);
        setYearLimit1(txnType, l1Amount, limitAudit, operationType);
        setDayLimit2(txnType, l2Amount, limitAudit, operationType);
        setMonthLimit2(txnType, l2Amount, limitAudit, operationType);
        setYearLimit2(txnType, l2Amount, limitAudit, operationType);
    }

    // SQL Query to db map
    private List<Map<String, Object>> getLimitAuditInfoMap(String custId) {
        DBValidation dbValidation = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.CUSTID, custId);
        List<Map<String, Object>> result = dbValidation.runDbQuery(DBQueries.limitAuditInfo, params,"log4j");
        return result;
    }

    // Get limit_info from map created from table
    private String getLimitAuditInfo(List<Map<String, Object>> result) {
        if (result.size() > 0)
            return result.get(0).get("limit_info").toString();
        else
            return null;
    }

    // Get timestamp from map created from table
    private String getLimitAuditUpdateTimestamp(List<Map<String, Object>> result) {
        if (result.size() > 0)
            return result.get(0).get("update_timestamp").toString();
        return null;
    }

    // Create LimitAudit Info pojo from json string
    private LimitAuditInfo getLimitAuditInfoPojo(String json) {
        AuditInfo audit = new AuditInfo();
        LimitAuditInfo limitAuditInfo = audit.createLimitAuditInfoPojo(json);
        return limitAuditInfo;
    }

    // Fetch the json and time stamp before the txn execution
    public void setBeforeJsonAndTimeStamp(String mobileNumber) {
        String custId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID,mobileNumber);
        System.out.println("custId is " + custId);
        List<Map<String, Object>> result = getLimitAuditInfoMap(custId);
        System.out.println("result is " + result);
        this.beforeTimeStamp = getLimitAuditUpdateTimestamp(result);
        this.beforeJson = getLimitAuditInfo(result);
    }

    // Generate the expected Limit Pojo from json string and update it as per
    // desired results
    private LimitAuditInfo createExpectedLimitAuditInfo(BigDecimal amount, BigDecimal l1Amount, BigDecimal l2Amount,
                                                        int count, int txnType, Short operationType) {
        LimitAuditInfo limitAuditInfo = getLimitAuditInfoPojo(this.beforeJson);
        setLimitAuditInfo(limitAuditInfo, amount, l1Amount, l2Amount, count, txnType, operationType);
        return limitAuditInfo;
    }

    //Test for Amount Code
    public void validateLimitAuditInfo(String mobileNumber, int count, int txnType, Short operationType) {
        String custId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID,mobileNumber);
        FundManager fundManager = FundThreadManager.getInstance();
        BigDecimal amount = fundManager.getTotalAmount();
        BigDecimal l1Amount = fundManager.getallMainGroupsAmount();
        BigDecimal l2Amount = fundManager.getAggregateGroupsAmount();
        List<Map<String, Object>> result = getLimitAuditInfoMap(custId);
        this.afterTimeStamp = getLimitAuditUpdateTimestamp(result);
        LimitAuditInfo actualAuditInfo = getLimitAuditInfoPojo(getLimitAuditInfo(result));
        LimitAuditInfo expectedAuditInfo;
        if(operationType==1){
            expectedAuditInfo = createExpectedLimitAuditInfo(amount.negate(), l1Amount.negate(), l2Amount.negate(), count, txnType,
                    operationType);
        }else{
            expectedAuditInfo = createExpectedLimitAuditInfo(amount, l1Amount, l2Amount, count, txnType,
                    operationType);
        }
        CommonHelpers.getInstance().comparePOJO(actualAuditInfo, expectedAuditInfo);
    }


    private void setPartialRollbackLimit(LimitAuditInfo limitAudit, int txnType, BigDecimal amount, BigDecimal l1Amount,
                                         Short operationType) {
        setDayRollbackLimit(txnType, amount, limitAudit, operationType);
        setMonthRollbackLimit(txnType, amount, limitAudit, operationType);
        setYearRollbackLimit(txnType, amount, limitAudit, operationType);
        setDayLimit1(txnType, l1Amount.negate(), limitAudit, operationType);
        setMonthLimit1(txnType, l1Amount.negate(), limitAudit, operationType);
        setYearLimit1(txnType, l1Amount.negate(), limitAudit, operationType);
        setDayLimit2(txnType, amount.negate(), limitAudit, operationType);
        setMonthLimit2(txnType, amount.negate(), limitAudit, operationType);
        setYearLimit2(txnType, amount.negate(), limitAudit, operationType);
    }

    private void setFullRollbackLimit(LimitAuditInfo limitAudit, int txnType, BigDecimal amount, BigDecimal l1Amount,
                                      int count, Short operationType) {
        setPartialRollbackLimit(limitAudit, txnType, amount, l1Amount, operationType);
        setDayCountLimit(txnType, -count, limitAudit, operationType);
        setMonthCountLimit(txnType, -count, limitAudit, operationType);
        setYearCountLimit(txnType, -count, limitAudit, operationType);
    }

    public BigDecimal systemLimitValidation(String custId, LimitTypeEnums limitName, LimitPeriodEnums period) {
        LimitAuditInfo limitAuditInfo = getUserLimitAuditInfo(custId);
        List<Map<String, Object>> result = getTxnTypeList(limitName);
        BigDecimal consumableAmount = consumableAmountPerLimit(result, limitAuditInfo, period);
        return consumableAmount;
    }

    private BigDecimal calculateConsumableAmount(String[] array, Short operationType, int ppiGroup,
                                                 LimitAuditInfo limitAuditInfo, LimitPeriodEnums period) {
        BigDecimal amount = BigDecimal.ZERO;
        for (int i = 0; i < array.length; i++) {
            amount = amount.add(
                    getAmount(Integer.parseInt(array[i].trim()), limitAuditInfo, operationType, ppiGroup, period));
        }
        System.out.println("Amount is " + amount);
        return amount;
    }

    private int calculateConsumableCount(String[] array, Short operationType, LimitAuditInfo limitAuditInfo, LimitPeriodEnums period) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            count = count + getCount(Integer.parseInt(array[i].trim()), limitAuditInfo, operationType, period);
        }
        return count;
    }

    public LimitAuditInfo getUserLimitAuditInfo(String phone) {
        this.afterTimeStamp = CommonHelpers.getInstance().getCurrentDateTime("yyyy-MM-dd hh:mm:ss");
        setBeforeJsonAndTimeStamp(phone);
        LimitAuditInfo limitAuditInfo = getLimitAuditInfoPojo(this.beforeJson);
        return limitAuditInfo;
    }

    private List<String> fetchPPIList(Map<Object, Object> params) {
        List<String> ppiGroupsList = null;
        DBValidation db = new DBValidation();
        try {
            List<Map<String, Object>> list = db.runDbQuery(DBQueries.ppiList, params,"log4j");
            ppiGroupsList = Arrays.asList(list.get(0).get("ppi_list").toString().replace("[", "").replace("]", "")
                    .replace(" ", "").split(",", -1));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ppiGroupsList;
    }

    private BigDecimal fetchLimit(Map<String, BigDecimal> userBalanceMap, String ppiGroup) {
        BigDecimal rbiAmount = BigDecimal.ZERO;
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.PPINAME, ppiGroup);
        List<String> ppiGroupsList = fetchPPIList(params);
        for (int i = 0; i < ppiGroupsList.size(); i++) {
            String ppiType = ppiGroupsList.get(i).replace("[", "").replace("]", "");
            if (userBalanceMap.containsKey(ppiType))
                rbiAmount = rbiAmount.add(userBalanceMap.get(ppiType));
        }
        return rbiAmount;
    }

    private LimitConfig getLimitConfig(Map<Object, Object> params) {
        DBValidation db = new DBValidation();
        LimitConfig config = new LimitConfig();
        List<Map<String, Object>> configList = db.runDbQuery(DBQueries.limitConfig, params,"log4j");
        System.out.println("configList is " + configList);
        if (configList.size() > 0) {
            String json = configList.get(0).get("config").toString().replaceAll("[\\[\\]]", "");
            try {
                return config.createPojo(json);
            } catch (IOException ie) {
            }
        }
        return null;
    }

    private BigDecimal debitThroughputForCredit(Map<Object, Object> params, LimitTypeEnums limitType,
                                                LimitAuditInfo limitAuditInfo, BigDecimal totalAmount) {
        BigDecimal creditConsumableAmount = getMinimumAmount(params, limitType, limitAuditInfo);
        BigDecimal minimumAmount = creditConsumableAmount.subtract(totalAmount);
        if (minimumAmount.compareTo(BigDecimal.ZERO) == -1)
            minimumAmount = BigDecimal.ZERO;
        return minimumAmount;
    }

    private BigDecimal debitThroughputForCreditPeriod(Map<Object, Object> params, LimitTypeEnums limitType, LimitAuditInfo limitAuditInfo, BigDecimal totalAmount, LimitPeriodEnums period) {
        BigDecimal creditConsumableAmount = getMinimumAmountPeriod(params, limitType, limitAuditInfo, period);
        BigDecimal minimumAmount = creditConsumableAmount.subtract(totalAmount);
        if (minimumAmount.compareTo(BigDecimal.ZERO) == -1)
            minimumAmount = BigDecimal.ZERO;
        return minimumAmount;
    }

    private BigDecimal getMinimumAmount(Map<Object, Object> params, LimitTypeEnums limitType, LimitAuditInfo limitAuditInfo) {
        BigDecimal minMonthAmount = getMinimumAmountPeriod(params, limitType, limitAuditInfo, LimitPeriodEnums.month);
        BigDecimal minYearAmount = getMinimumAmountPeriod(params, limitType, limitAuditInfo, LimitPeriodEnums.year);
        return minMonthAmount.compareTo(minYearAmount) == -1 ? minYearAmount : minMonthAmount;
    }

    private Integer getMinimumCountPeriod(Map<Object, Object> params, LimitTypeEnums limitType, LimitAuditInfo limitAuditInfo, LimitPeriodEnums period){
        List<Map<String, Object>> creditResult = getTxnTypeList(limitType);
        int minimumCount = 2000;
        params.put(DBEnums.LIMITNAME, limitType);
        LimitConfig config = getLimitConfig(params);
        if (period.equals(LimitPeriodEnums.month)) {
            if (config != null && config.getPeriodLimits().getMonthPeriodLimit() != null) {
                int consumedCount = consumableCountPerLimit(creditResult, limitAuditInfo, LimitPeriodEnums.month);
                minimumCount = config.getPeriodLimits().getMonthPeriodLimit().getCount() - consumedCount;
            }
        } else if (period.equals(LimitPeriodEnums.year)) {
            if (config != null && config.getPeriodLimits().getYearPeriodLimit() != null) {
                int consumedCount = consumableCountPerLimit(creditResult, limitAuditInfo, LimitPeriodEnums.year);
                minimumCount = config.getPeriodLimits().getYearPeriodLimit().getCount() - consumedCount;
            }
        } else if (period.equals(LimitPeriodEnums.day)) {
            if (config != null && config.getPeriodLimits().getDayPeriodLimit() != null) {
                int consumedCount = consumableCountPerLimit(creditResult, limitAuditInfo, LimitPeriodEnums.day);
                minimumCount = config.getPeriodLimits().getDayPeriodLimit().getCount() - consumedCount;
            }
        }
        minimumCount = minimumCount < 0 ? 0 : minimumCount;
        return minimumCount;
    }

    private BigDecimal getMinimumAmountPeriod(Map<Object, Object> params, LimitTypeEnums limitType, LimitAuditInfo limitAuditInfo, LimitPeriodEnums period) {
        List<Map<String, Object>> creditResult = getTxnTypeList(limitType);
        BigDecimal minimumAmount = new BigDecimal(1000000000);
        params.put(DBEnums.LIMITNAME, limitType);
        LimitConfig config = getLimitConfig(params);
        if (period.equals(LimitPeriodEnums.month)) {
            if (config != null && config.getPeriodLimits().getMonthPeriodLimit() != null) {
                BigDecimal creditThroughputAmount = consumableAmountPerLimit(creditResult, limitAuditInfo, LimitPeriodEnums.month);
                creditThroughputAmount = creditThroughputAmount.compareTo(BigDecimal.ZERO) > 0 ? creditThroughputAmount : creditThroughputAmount.negate();
                minimumAmount = config.getPeriodLimits().getMonthPeriodLimit().getAmount().subtract(creditThroughputAmount);
            }
        } else if (period.equals(LimitPeriodEnums.year)) {
            if (config != null && config.getPeriodLimits().getYearPeriodLimit() != null) {
                BigDecimal creditThroughputAmount = consumableAmountPerLimit(creditResult, limitAuditInfo, LimitPeriodEnums.year);
                creditThroughputAmount = creditThroughputAmount.compareTo(BigDecimal.ZERO) > 0 ? creditThroughputAmount : creditThroughputAmount.negate();
                minimumAmount = config.getPeriodLimits().getYearPeriodLimit().getAmount().subtract(creditThroughputAmount);
            }
        } else if (period.equals(LimitPeriodEnums.day)) {
            if (config != null && config.getPeriodLimits().getDayPeriodLimit() != null) {
                BigDecimal creditThroughputAmount = consumableAmountPerLimit(creditResult, limitAuditInfo, LimitPeriodEnums.day);
                creditThroughputAmount = creditThroughputAmount.compareTo(BigDecimal.ZERO) > 0 ? creditThroughputAmount : creditThroughputAmount.negate();
                minimumAmount = config.getPeriodLimits().getDayPeriodLimit().getAmount().subtract(creditThroughputAmount);
            }
        }
        minimumAmount = minimumAmount.compareTo(BigDecimal.ZERO) == -1 ? BigDecimal.ZERO : minimumAmount;
        return minimumAmount;
    }

    private BigDecimal getRBIBalance(BigDecimal rbiAmount, Map<Object, Object> params) {
        BigDecimal minimumAmount = new BigDecimal(1000000000);
        params.put(DBEnums.LIMITNAME, LimitTypeEnums.RBI_BALANCE);
        LimitConfig config = getLimitConfig(params);
        if (config.getBalance() != null) {
            minimumAmount = config.getBalance().subtract(rbiAmount);
        }
        minimumAmount = minimumAmount.compareTo(BigDecimal.ZERO) == -1 ? BigDecimal.ZERO : minimumAmount;
        return minimumAmount;
    }

    private BigDecimal getAggregateBalance(Map<Object, Object> params, BigDecimal aggregateBalance) {
        params.put(DBEnums.LIMITNAME, LimitTypeEnums.WALLET_AGGREGATE_BALANCE_LIMIT);
        BigDecimal minimumAmount = new BigDecimal(1000000000);
        LimitConfig config = getLimitConfig(params);
        if (config.getBalance() != null) {
            minimumAmount = config.getBalance().subtract(aggregateBalance);
        }
        minimumAmount = minimumAmount.compareTo(BigDecimal.ZERO) == -1 ? BigDecimal.ZERO : minimumAmount;
        return minimumAmount;
    }

    private BigDecimal getLimitSpecificAmount(LimitTypeEnums limitType, LimitAuditInfo limitAuditInfo,
                                              Map<Object, Object> params, BigDecimal rbiTotalAmount, BigDecimal aggregateBalance){
        BigDecimal amount = new BigDecimal(1000000000);
        switch (limitType) {
            case CREDIT_THROUGHPUT_LIMIT:
                amount = getMinimumAmount(params,LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT, limitAuditInfo);
                break;
            case DEBIT_THROUGHPUT_LIMIT:
                amount = debitThroughputForCredit(params, LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT, limitAuditInfo, rbiTotalAmount);
                break;
            case RBI_BALANCE:
                amount = getRBIBalance(rbiTotalAmount, params);
                break;
            case WALLET_AGGREGATE_BALANCE_LIMIT:
                amount = getAggregateBalance(params, aggregateBalance);
                break;
        }
        return amount;
    }

    public BigDecimal checkBalance(String phone, String txnType){
        DBValidation db = new DBValidation();
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        String panVerified = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAN, phone);
        String trustFactor = UserManager.getInstance().getSpecificUserDetail(UserHeaders.TRUSTFACTOR, phone);
        String walletRbiType = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETRBITYPE, phone);
        LimitAuditInfo limitAuditInfo = getUserLimitAuditInfo(phone);
        SubWalletHelpers helper = new SubWalletHelpers();
        WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
        CheckUserBalance checkUser = walletAPIHelpers.fetchCheckBalanceResponseALL(phone);
        Map<String, BigDecimal> userBalanceMap = helper.fetchSubWalletBalance(checkUser);
        BigDecimal rbiTotalAmount = fetchLimit(userBalanceMap, "MAIN");
        BigDecimal aggregateBalance = fetchLimit(userBalanceMap, "AGGREGATE_BALANCE_GROUP");
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.WALLETRBITYPE, walletRbiType);
        params.put(DBEnums.TRUSTFACTOR, trustFactor);
        params.put(DBEnums.PANVERIFIED, panVerified);
        params.put(DBEnums.TXNTYPE, txnType);
        BigDecimal[] array = new BigDecimal[4];
        List<Map<String, Object>> limitListing = db.runDbQuery(DBQueries.txnlimitListing, params,"log4j");
        validateAllLimitDetails(limitAuditInfo, 1, (short) 1);
        for (int i = 0; i < limitListing.size(); i++) {
            String limitType = limitListing.get(i).get("limit_name").toString().replaceAll("[\\[\\]]", "");
            array[i] = getLimitSpecificAmount(LimitTypeEnums.valueOf(limitType), limitAuditInfo, params, rbiTotalAmount, aggregateBalance);
        }
        Arrays.sort(array);
        BigDecimal amount = array[0];
        return amount.setScale(2);
    }

    public List<Map<String, Object>> getTxnTypeList(LimitTypeEnums limitType) {
        DBValidation db = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.LIMITNAME, limitType);
        List<Map<String, Object>> result = db.runDbQuery(DBQueries.limitBasedTxnTypes, params,"log4j");
        return result;
    }

    public BigDecimal consumableAmountPerLimit(List<Map<String, Object>> result, LimitAuditInfo limitAuditInfo,
                                             LimitPeriodEnums period) {
        String[] array = result.get(0).get("txn_type_list").toString().replace("[", "").replace("]", "").trim()
                .split(",");
        Short operationType = Short.parseShort(result.get(0).get("operation_type").toString());
        int ppiGroup = Integer.parseInt(result.get(0).get("ppi_group").toString());
        return calculateConsumableAmount(array, operationType, ppiGroup, limitAuditInfo, period);
    }

    public int consumableCountPerLimit(List<Map<String, Object>> result, LimitAuditInfo limitAuditInfo, LimitPeriodEnums period) {
        String[] array = result.get(0).get("txn_type_list").toString().replace("[", "").replace("]", "").trim()
                .split(",");
        Short operationType = Short.parseShort(result.get(0).get("operation_type").toString());
        return calculateConsumableCount(array, operationType, limitAuditInfo, period);
    }


    public void setSystemCountLimit(String phone, LimitTypeEnums limitType, LimitPeriodEnums period){
        String custId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, phone);
        String walletId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETID, phone);
        String panVerified = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAN, phone);
        String trustFactor = UserManager.getInstance().getSpecificUserDetail(UserHeaders.TRUSTFACTOR, phone);
        String walletRbiType = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETRBITYPE, phone);
        LimitAuditInfo limitAuditInfo = getUserLimitAuditInfo(phone);
        List<Map<String, Object>> limitDescriptionAndTxnTypeList = getTxnTypeList(LimitTypeEnums.valueOf(limitType.toString().split("\\$",-1)[0]));
        List<String> txnTypeList = new LinkedList<>();
        if (limitDescriptionAndTxnTypeList.get(0).get("txn_type_list") != null) {
            txnTypeList = new LinkedList<>(Arrays.asList(limitDescriptionAndTxnTypeList.get(0).get("txn_type_list").toString().replace("[", "").replace("]", "").trim()
                    .split(",")));
        }
        Short operationType=0;
        int transaction=0;
        if(limitType.equals(LimitTypeEnums.TXN_TYPE_DURATION_P2P)){
            transaction = Integer.parseInt(txnTypeList.get(0));
            operationType = ((Number) limitDescriptionAndTxnTypeList.get(0).get("operation_type")).shortValue();
            validateAllLimitDetails(limitAuditInfo, transaction, operationType);
        }
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.WALLETRBITYPE, walletRbiType);
        params.put(DBEnums.TRUSTFACTOR, trustFactor);
        params.put(DBEnums.PANVERIFIED, panVerified);
        int count;
        switch (limitType){
            case TXN_TYPE_DURATION_P2P: {
                count = getMinimumCountPeriod(params,limitType,limitAuditInfo,period);
                setUpdatedCountLimitAuditPojo(limitAuditInfo,period,count,transaction,operationType);
                if (this.beforeJson == null) {
                    insertLimitAuditInfoDB(custId, walletId, getLimitInfoFromPojo(limitAuditInfo));
                } else {
                    updateLimitAuditInfoDB(custId, getLimitInfoFromPojo(limitAuditInfo));
                }
                break;
            } default : {
                throw new LimitAuditInfoValidationException("Invalid limit Type passed");
            }

        }
    }


    public void setSystemLimit(String phone, LimitTypeEnums limitType, LimitPeriodEnums period, BigDecimal txnAmount) {
        String custId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, phone);
        String panVerified = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAN, phone);
        String trustFactor = UserManager.getInstance().getSpecificUserDetail(UserHeaders.TRUSTFACTOR, phone);
        String walletRbiType = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETRBITYPE, phone);
        String walletId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETID, phone);
        LimitAuditInfo limitAuditInfo = getUserLimitAuditInfo(phone);

        List<Map<String, Object>> limitDescriptionAndTxnTypeList = getTxnTypeList(LimitTypeEnums.valueOf(limitType.toString().split("\\$",-1)[0]));
        List<String> txnTypeList = new LinkedList<>();
        if (limitDescriptionAndTxnTypeList.get(0).get("txn_type_list") != null) {
            txnTypeList = new LinkedList<>(Arrays.asList(limitDescriptionAndTxnTypeList.get(0).get("txn_type_list").toString().replace("[", "").replace("]", "").trim()
                    .split(",")));
        }
        int ppiGroup = (Integer) limitDescriptionAndTxnTypeList.get(0).get("ppi_group");
        Map<String,BigDecimal> userBalanceMap = fetchCheckUserBalanceMap(limitType,phone);
        Short operationType=0;
        int transaction=0;
        if(limitType.equals(LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT)||limitType.equals(LimitTypeEnums.TXN_TYPE_DURATION_P2P)||limitType.equals(LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$ADD)||limitType.equals(LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$WITHDRAW)||limitType.equals(LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT)){
            transaction = Integer.parseInt(txnTypeList.get(0));
            operationType = ((Number) limitDescriptionAndTxnTypeList.get(0).get("operation_type")).shortValue();
            validateAllLimitDetails(limitAuditInfo, transaction, operationType);
        }
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.WALLETRBITYPE, walletRbiType);
        params.put(DBEnums.TRUSTFACTOR, trustFactor);
        params.put(DBEnums.PANVERIFIED, panVerified);
        BigDecimal amount;
        switch (limitType) {
            case CREDIT_THROUGHPUT_LIMIT: {
                amount = getMinimumAmountPeriod(params, limitType, limitAuditInfo, period).subtract(txnAmount);
                if (operationType == 1) {
                    amount = amount.negate();
                }
                setUpdatedAmountLimitAuditPojo(limitAuditInfo, period, amount, transaction, operationType, ppiGroup);
                if (this.beforeJson == null) {
                    insertLimitAuditInfoDB(custId, walletId, getLimitInfoFromPojo(limitAuditInfo));
                } else {
                    updateLimitAuditInfoDB(custId, getLimitInfoFromPojo(limitAuditInfo));
                }
                break;
            }
            case DEBIT_THROUGHPUT_LIMIT$ADD: {
                BigDecimal rbiTotalAmount = fetchLimit(userBalanceMap, "MAIN");
                amount = debitThroughputForCreditPeriod(params, LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT, limitAuditInfo, rbiTotalAmount, period).subtract(txnAmount);
                if (operationType == 1) {
                    amount = amount.negate();
                }
                setUpdatedAmountLimitAuditPojo(limitAuditInfo, period, amount, transaction, operationType, ppiGroup);
                if (this.beforeJson == null) {
                    insertLimitAuditInfoDB(custId, walletId, getLimitInfoFromPojo(limitAuditInfo));
                } else {
                    updateLimitAuditInfoDB(custId, getLimitInfoFromPojo(limitAuditInfo));
                }
                break;
            }
            case DEBIT_THROUGHPUT_LIMIT$WITHDRAW: {
                amount = getMinimumAmountPeriod(params, LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT, limitAuditInfo, period).subtract(txnAmount);
                if (operationType == 1) {
                    amount = amount.negate();
                }
                setUpdatedAmountLimitAuditPojo(limitAuditInfo, period, amount, transaction, operationType, ppiGroup);
                if (this.beforeJson == null) {
                    insertLimitAuditInfoDB(custId, walletId, getLimitInfoFromPojo(limitAuditInfo));
                } else {
                    updateLimitAuditInfoDB(custId, getLimitInfoFromPojo(limitAuditInfo));
                }
                break;
            }
            case TXN_TYPE_DURATION_P2P: {
                amount = getMinimumAmountPeriod(params, limitType, limitAuditInfo, period).subtract(txnAmount);
                if (operationType == 1) {
                    amount = amount.negate();
                }
                setUpdatedAmountLimitAuditPojo(limitAuditInfo, period, amount, transaction, operationType, ppiGroup);
                if (this.beforeJson == null) {
                    insertLimitAuditInfoDB(custId, walletId, getLimitInfoFromPojo(limitAuditInfo));
                } else {
                    updateLimitAuditInfoDB(custId, getLimitInfoFromPojo(limitAuditInfo));
                }
                break;
            }
            case RBI_BALANCE: {
                BigDecimal rbiBalance = fetchLimit(userBalanceMap, "MAIN");
                amount = getRBIBalance(rbiBalance, params).subtract(txnAmount);
                addFundsForLimitsBreach(phone, amount, "FUEL");
                if (this.beforeJson == null) {
                    deleteLimitAuditInfoDB(custId);
                } else {
                    updateLimitAuditInfoDB(custId, this.beforeJson);
                }
                break;
            }
            case WALLET_AGGREGATE_BALANCE_LIMIT: {
                BigDecimal aggregateBalance = fetchLimit(userBalanceMap, "AGGREGATE_BALANCE_GROUP");
                amount = getAggregateBalance(params, aggregateBalance).subtract(txnAmount);
                addFundsForLimitsBreach(phone, amount, "GIFT");
                if (this.beforeJson == null) {
                    deleteLimitAuditInfoDB(custId);
                } else {
                    updateLimitAuditInfoDB(custId, this.beforeJson);
                }
                break;
            }
            default : {
                throw new LimitAuditInfoValidationException("Invalid limit Type passed");
            }
        }
    }

    private Map<String,BigDecimal> fetchCheckUserBalanceMap(LimitTypeEnums limitType, String phone){
        Map<String, BigDecimal> userBalanceMap = new HashMap<>();
        if (!limitType.equals(LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT)) {
            SubWalletHelpers helper = new SubWalletHelpers();
            WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
            CheckUserBalance checkUser = walletAPIHelpers.fetchCheckBalanceResponseALL(phone);
            checkUser.createRequestJsonAndExecute();
            userBalanceMap = helper.fetchSubWalletBalance(checkUser);
        }
        return userBalanceMap;
    }

    private void setUpdatedAmountLimitAuditPojo(LimitAuditInfo limitAuditInfo, LimitPeriodEnums period, BigDecimal amount, int txnType, Short operationType, Integer ppiGroup) {
        if(operationType==1){
            amount=amount.subtract(BigDecimal.ONE);
        }else{
            amount = amount.add(BigDecimal.ONE);
        }
        if (period.equals(LimitPeriodEnums.month)) {
            if (ppiGroup == 1) {
                setMonthLimit1(txnType, amount, limitAuditInfo, operationType);
            } else if (ppiGroup == 2) {
                setMonthLimit2(txnType, amount, limitAuditInfo, operationType);
            } else {
                setMonthAmountLimit(txnType, amount, limitAuditInfo, operationType);
            }
        } else if (period.equals(LimitPeriodEnums.day)) {
            if (ppiGroup == 1) {
                setDayLimit1(txnType, amount, limitAuditInfo, operationType);
            } else if (ppiGroup == 2) {
                setDayLimit2(txnType, amount, limitAuditInfo, operationType);
            } else {
                setDayAmountLimit(txnType, amount, limitAuditInfo, operationType);
            }
        } else if (period.equals(LimitPeriodEnums.year)) {
            if (ppiGroup == 1) {
                setYearLimit1(txnType, amount, limitAuditInfo, operationType);
            } else if (ppiGroup == 2) {
                setYearLimit2(txnType, amount, limitAuditInfo, operationType);
            } else {
                setYearAmountLimit(txnType, amount, limitAuditInfo, operationType);
            }
        }
    }

    private void setUpdatedCountLimitAuditPojo(LimitAuditInfo limitAuditInfo, LimitPeriodEnums period, int count, int txnType, Short operationType) {
        if (period.equals(LimitPeriodEnums.month)) {
            setMonthCountLimit(txnType, count, limitAuditInfo, operationType);
        } else if (period.equals(LimitPeriodEnums.day)) {
            setDayCountLimit(txnType, count, limitAuditInfo, operationType);
        } else if (period.equals(LimitPeriodEnums.year)) {
            setYearCountLimit(txnType, count, limitAuditInfo, operationType);
        }
    }

    private void addFundsForLimitsBreach(String mobile, BigDecimal amount, String userSubWalletType) {
        amount = new BigDecimal(amount.add(BigDecimal.ONE).intValue());
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
            String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
            String corporateGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.CORPORATEWALLETGUID);
            deleteLimitAuditInfoDB(UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID,mobile));
            walletAPIHelpers.subWalletAdd(mobile, userSubWalletType, amount, mGuid, corporateGuid);
        }
        this.addedBalance = amount;
    }

    private void withdrawFundsLimitsReset(String mobile, BigDecimal amount, String userSubWalletType) {
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
            amount = amount.subtract(BigDecimal.ONE);
            String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
            walletAPIHelpers.subWalletWithdraw(mobile, userSubWalletType, amount, mGuid);
        }
    }

    private void updateLimitAuditInfoDB(String custId, String limitInfo) {
        DBValidation dbValidation = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.CUSTID, custId);
        params.put(DBEnums.LIMITINFO, limitInfo);
        dbValidation.runUpdateQuery(DBQueries.updateLimitAuditInfo, params);
    }

    private void insertLimitAuditInfoDB(String custId, String walletId, String limitInfo) {
        DBValidation dbValidation = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.CUSTID, custId);
        params.put(DBEnums.LIMITINFO, limitInfo);
        params.put(DBEnums.WALLETID, walletId);
        dbValidation.runUpdateQuery(DBQueries.insertLimitAuditInfo, params);
    }

    private void deleteLimitAuditInfoDB(String custId) {
        DBValidation dbValidation = new DBValidation();
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.CUSTID, custId);
        dbValidation.runUpdateQuery(DBQueries.deleteLimitAuditInfo, params);
        dbValidation.runUpdateQuery(DBQueries.deleteLimitAuditInfoMaster,params);
    }

    private String getLimitInfoFromPojo(LimitAuditInfo limitAuditInfo) {
        try {
            String limitInfo = JacksonJsonImpl.getInstance().toJSon(limitAuditInfo).trim();
            return limitInfo.replaceAll("\n", "").replaceAll(" ", "");
        } catch (IOException ie) {
            throw new LimitAuditInfoValidationException("Error in serializing POJO");
        }
    }

    public void resetLimits(String phone, LimitTypeEnums limitType) {
        String custId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, phone);
        if (limitType.equals(LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT) || limitType.equals(LimitTypeEnums.TXN_TYPE_DURATION_P2P)||limitType.equals(LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$ADD)||limitType.equals(LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$WITHDRAW)||limitType.equals(LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT)) {
            if (this.beforeJson == null) {
                deleteLimitAuditInfoDB(custId);
            } else {
                updateLimitAuditInfoDB(custId, this.beforeJson);
            }
        } else if (limitType.equals(LimitTypeEnums.RBI_BALANCE)) {
            try {
                withdrawFundsLimitsReset(phone, this.addedBalance, "FUEL");
            }catch(Throwable e){
                SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
                subWalletHelpers.setSubWalletBalanceZero(phone);
            }finally {
                if (this.beforeJson == null) {
                    deleteLimitAuditInfoDB(custId);
                } else {
                    updateLimitAuditInfoDB(custId, this.beforeJson);
                }
            }
        } else if (limitType.equals(LimitTypeEnums.WALLET_AGGREGATE_BALANCE_LIMIT)) {
            try{
            withdrawFundsLimitsReset(phone, this.addedBalance, "GIFT");
            }catch(Throwable e){
                SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
                subWalletHelpers.setSubWalletBalanceZero(phone);
            }finally {
                if (this.beforeJson == null) {
                    deleteLimitAuditInfoDB(custId);
                } else {
                    updateLimitAuditInfoDB(custId, this.beforeJson);
                }
            }
        }
    }
}
