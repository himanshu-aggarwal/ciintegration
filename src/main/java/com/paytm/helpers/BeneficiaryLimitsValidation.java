package com.paytm.helpers;

import com.paytm.apiPojo.oauthAPI.GetBeneficiaryResponse;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.DBEnums;
import com.paytm.enums.UserHeaders;
import com.paytm.exceptions.LimitsException.BeneficiaryLimits.BeneficiaryLimitValidationException;
import com.paytm.utils.DBValidation;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeneficiaryLimitsValidation {

    private String orignalCounterValue;
    private String counterKey;
    private static final String defaultCounterValue = "{\"dayLimit\":{\"a\":-11.00,\"c\":1,\"r\":10.00,\"o\":1},\"monthLimit\":{\"a\":-11.00,\"c\":1,\"r\":10.00,\"o\":1}}";


    private GetBeneficiaryResponse.Data getBeneficiariesDetailsP2P(List<GetBeneficiaryResponse.Data> beneficiariesList, String payeePhoneNumber){
        for(GetBeneficiaryResponse.Data data : beneficiariesList){
            if(data.getInstrumentPreferences().getWallet()!=null) {
                List<GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account> accounts = data.getInstrumentPreferences().getWallet().getAccounts();
                for (GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account account : accounts) {
                    if (account.getAccountDetail().getBeneficiaryPhone().equalsIgnoreCase(payeePhoneNumber)) {
                        return data;
                    }
                }
            }
        }
        return null;
    }

    private GetBeneficiaryResponse.Data getBeneficiariesDetailsP2B(List<GetBeneficiaryResponse.Data> beneficiariesList, String accountNumber, String ifscCode){
        for(GetBeneficiaryResponse.Data data : beneficiariesList){
            if(data.getInstrumentPreferences().getOtherBank()!=null) {
                List<GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account> accounts = data.getInstrumentPreferences().getOtherBank().getAccounts();
                for (GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account account : accounts) {
                    if (account.getAccountDetail().getAccountNumber().equalsIgnoreCase(accountNumber)&&account.getAccountDetail().getIfscCode().equalsIgnoreCase(ifscCode)) {
                        return data;
                    }
                }
            }
        }
        return null;
    }

    private void getLimitCounterDB(String payerPhoneNumber, String beneficiaryId, String txnType){
        this.counterKey = txnType + "_" + UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID,payerPhoneNumber) + "_" + beneficiaryId;
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.COUNTERKEY,this.counterKey);
        DBValidation dbValidation = new DBValidation();
        List<Map<String,Object>> dbData = dbValidation.runDbQuery(DBQueries.getGenericLimitCounter,params,"log4j");
        System.out.println(dbData.isEmpty());
        if(!dbData.isEmpty()) {
            this.orignalCounterValue = dbData.get(0).get("counter_value").toString();
        }
    }

    private void updateGenericLimitCounterDB(String counterValue){
        Map<Object,Object> params = new HashMap<>();
        params.put(DBEnums.COUNTERKEY,this.counterKey);
        params.put(DBEnums.COUNTERVALUE,counterValue);
        DBValidation dbValidation = new DBValidation();
        dbValidation.runUpdateQuery(DBQueries.updateGenericLimitCounterValue,params);
    }

    private void insertGenericLimitCounterDB(String counterValue){
        Map<Object,Object> params = new HashMap<>();
        params.put(DBEnums.COUNTERKEY,this.counterKey);
        params.put(DBEnums.COUNTERVALUE,counterValue);
        DBValidation dbValidation = new DBValidation();
        dbValidation.runUpdateQuery(DBQueries.insertGenericLimitCounterValue,params);
    }

    private String getDefaultLimitRules(String ruleId){
        DBValidation dbValidation = new DBValidation();
        List<Map<String,Object>> defaultAssociativeLimitRules = dbValidation.runDbQuery(DBQueries.getDefaultAssociativeLimitRule,null,"log4j");
        for(Map<String,Object> limitRule : defaultAssociativeLimitRules){
            if(limitRule.get("rule_id").toString().equalsIgnoreCase(ruleId)){
                return limitRule.get("default_config").toString();
            }
        }
        throw new BeneficiaryLimitValidationException("Invalid Rule_id : "+ruleId);
    }

    private String getLimitRuleCountAmount(String ruleId, List<GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account.Limit> limits){
        if(limits!=null) {
            for (GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account.Limit limit : limits) {
                if (limit.getRuleId().equalsIgnoreCase(ruleId)) {
                    if (limit.getRuleParams().getTxn() != null) {
                        return limit.getRuleParams().getTxn();
                    } else if (limit.getRuleParams().getAmount() != null) {
                        return limit.getRuleParams().getAmount();
                    } else {
                        throw new BeneficiaryLimitValidationException("Both amount and count are set to null");
                    }
                }
            }
        }
        String default_config = getDefaultLimitRules(ruleId);
        JSONObject jsonObject = (JSONObject) JSONValue.parse(default_config);
        if(jsonObject.containsKey("amount")){
            return jsonObject.get("amount").toString();
        }else if(jsonObject.containsKey("txn")){
            return jsonObject.get("txn").toString();
        }else{
            throw new BeneficiaryLimitValidationException("Both amount and count are set to null");
        }
    }

    private String getBeneficiaryIdP2P(GetBeneficiaryResponse.Data beneficiaryDetails, String payeePhoneNumber){
        List<GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account> accounts = beneficiaryDetails.getInstrumentPreferences().getWallet().getAccounts();
        int i =0;
        for (GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account account : accounts) {
            if (account.getAccountDetail().getBeneficiaryPhone().equalsIgnoreCase(payeePhoneNumber)) {
                return beneficiaryDetails.getInstrumentPreferences().getWallet().getAccounts().get(i).getUuid();
            }
            i++;
        }
        return null;
    }


    private String getBeneficiaryIdP2B(GetBeneficiaryResponse.Data beneficiaryDetails, String accountNumber, String ifscCode){
        List<GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account> accounts = beneficiaryDetails.getInstrumentPreferences().getWallet().getAccounts();
        int i =0;
        for (GetBeneficiaryResponse.Data.InstrumentPreferences.Accounts.Account account : accounts) {
            if (account.getAccountDetail().getAccountNumber().equalsIgnoreCase(accountNumber)&&account.getAccountDetail().getIfscCode().equalsIgnoreCase(ifscCode)) {
                return beneficiaryDetails.getInstrumentPreferences().getOtherBank().getAccounts().get(i).getUuid();
            }
            i++;
        }
        return null;
    }

    private String updateGenericLimitCounter(String value,String duration, String updateType){
         String updatedGenericLimitCounter = this.defaultCounterValue;
         JSONObject jsonObject = (JSONObject) JSONValue.parse(updatedGenericLimitCounter);
         if(duration.equalsIgnoreCase("day")){
             JSONObject jsonObject1 = (JSONObject) jsonObject.get("dayLimit");
             if(updateType.equalsIgnoreCase("count")){
                 String updateValue = value;
                 jsonObject1.put("c",updateValue);
             }else if(updateType.equalsIgnoreCase("amount")){
                 BigDecimal transactionAmount = FundThreadManager.getInstance().getTotalAmount();
                 BigDecimal currentUsedAmount = new BigDecimal(jsonObject1.get("a").toString()).subtract(new BigDecimal(jsonObject1.get("r").toString()));
                 BigDecimal updateValue = new BigDecimal(value).subtract(currentUsedAmount).subtract(transactionAmount).add(BigDecimal.ONE).negate();
                 jsonObject1.put("a",updateValue.toString());
             }else{
                 throw new BeneficiaryLimitValidationException("Invalid RuleId" +updateType);
             }
             jsonObject.put("dayLimit",jsonObject1);
             System.out.println(jsonObject.toJSONString());
         }else if(duration.equalsIgnoreCase("month")){
             JSONObject jsonObject1 = (JSONObject) jsonObject.get("monthLimit");
             if(updateType.equalsIgnoreCase("count")){
                 String updateValue = value;
                 jsonObject1.put("c",updateValue);
             }else if(updateType.equalsIgnoreCase("amount")){
                 BigDecimal transactionAmount = FundThreadManager.getInstance().getTotalAmount();
                 BigDecimal currentUsedAmount = new BigDecimal(jsonObject1.get("a").toString()).subtract(new BigDecimal(jsonObject1.get("r").toString()));
                 BigDecimal updateValue = new BigDecimal(value).subtract(currentUsedAmount).subtract(transactionAmount).add(BigDecimal.ONE).negate();
                 jsonObject1.put("a",updateValue.toString());
             }else{
                 throw new BeneficiaryLimitValidationException("Invalid RuleId" +updateType);
             }
             jsonObject.put("monthLimit",jsonObject1);
             System.out.println(jsonObject.toJSONString());
         }else{
             throw new BeneficiaryLimitValidationException("Invalid Duration : "+duration);
         }
         return jsonObject.toJSONString();
    }

    public void updateBeneficiaryCountP2P(String payerPhoneNumber, String payeePhoneNumber){
        AuthHelpers authHelpers = new AuthHelpers();
        List<GetBeneficiaryResponse.Data> beneficiariesList = authHelpers.getBenificiaries(payerPhoneNumber);
        GetBeneficiaryResponse.Data beneficiaryDetails = null;
        if(beneficiariesList!=null) {
            beneficiaryDetails = getBeneficiariesDetailsP2P(beneficiariesList, payeePhoneNumber);
        }
        if(beneficiariesList==null||beneficiaryDetails==null){
            throw new BeneficiaryLimitValidationException("Beneficiary Not found");
        }
        String beneficiaryId = getBeneficiaryIdP2P(beneficiaryDetails, payeePhoneNumber);
        getLimitCounterDB(payerPhoneNumber,beneficiaryId,"P2P");
        String value = getLimitRuleCountAmount("LIMITS-1",beneficiaryDetails.getInstrumentPreferences().getWallet().getAccounts().get(0).getLimits());
        String updatedCounterValue = updateGenericLimitCounter(value,"month","count");
        if(this.orignalCounterValue!=null){
            updateGenericLimitCounterDB(updatedCounterValue);
        }else{
            this.orignalCounterValue=this.defaultCounterValue;
            insertGenericLimitCounterDB(updatedCounterValue);
        }
    }

    public void updateBeneficiaryAmountP2P(String payerPhoneNumber, String payeePhoneNumber){
        AuthHelpers authHelpers = new AuthHelpers();
        List<GetBeneficiaryResponse.Data> beneficiariesList = authHelpers.getBenificiaries(payerPhoneNumber);
        GetBeneficiaryResponse.Data beneficiaryDetails = null;
        if(beneficiariesList!=null) {
            beneficiaryDetails = getBeneficiariesDetailsP2P(beneficiariesList, payeePhoneNumber);
        }
        String value;
        if(beneficiariesList!=null&&beneficiaryDetails!=null){
            String beneficiaryId = getBeneficiaryIdP2P(beneficiaryDetails, payeePhoneNumber);
            getLimitCounterDB(payerPhoneNumber,beneficiaryId,"P2P");
            value = getLimitRuleCountAmount("LIMITS-2",beneficiaryDetails.getInstrumentPreferences().getWallet().getAccounts().get(0).getLimits());
        }else{
            String beneficiarId = "ALL";
            getLimitCounterDB(payerPhoneNumber,beneficiarId,"P2P_P2B");
            value = getLimitRuleCountAmount("NON-BENEFICIARY-LIMITS-1",null);
        }
        String updatedCounterValue = updateGenericLimitCounter(value,"month","amount");
        if(this.orignalCounterValue!=null){
            updateGenericLimitCounterDB(updatedCounterValue);
        }else{
            this.orignalCounterValue=this.defaultCounterValue;
            insertGenericLimitCounterDB(updatedCounterValue);
        }
    }

    public void updateBeneficiaryCountP2B(String payerPhoneNumber, String accountNumber,String ifscCode){
        AuthHelpers authHelpers = new AuthHelpers();
        List<GetBeneficiaryResponse.Data> beneficiariesList = authHelpers.getBenificiaries(payerPhoneNumber);
        GetBeneficiaryResponse.Data beneficiaryDetails = null;
        if(beneficiariesList!=null) {
            beneficiaryDetails = getBeneficiariesDetailsP2B(beneficiariesList, accountNumber, ifscCode);
        }
        if(beneficiariesList==null||beneficiaryDetails==null){
            throw new BeneficiaryLimitValidationException("Beneficiary Not found");
        }
        String beneficiaryId = getBeneficiaryIdP2B(beneficiaryDetails, accountNumber, ifscCode);
        getLimitCounterDB(payerPhoneNumber,beneficiaryId,"P2B");
        String value = getLimitRuleCountAmount("LIMITS-1",beneficiaryDetails.getInstrumentPreferences().getOtherBank().getAccounts().get(0).getLimits());
        String updatedCounterValue = updateGenericLimitCounter(value,"month","count");
        if(this.orignalCounterValue!=null){
            updateGenericLimitCounterDB(updatedCounterValue);
        }else{
            this.orignalCounterValue=this.defaultCounterValue;
            insertGenericLimitCounterDB(updatedCounterValue);
        }
    }

    public void updateBeneficiaryAmountP2B(String payerPhoneNumber, String accountNumber,String ifscCode){
        AuthHelpers authHelpers = new AuthHelpers();
        List<GetBeneficiaryResponse.Data> beneficiariesList = authHelpers.getBenificiaries(payerPhoneNumber);
        GetBeneficiaryResponse.Data beneficiaryDetails = null;
        if(beneficiariesList!=null) {
            beneficiaryDetails = getBeneficiariesDetailsP2B(beneficiariesList, accountNumber, ifscCode);
        }
        String value;
        if(beneficiariesList!=null&&beneficiaryDetails!=null){
            String beneficiaryId = getBeneficiaryIdP2B(beneficiaryDetails, accountNumber, ifscCode);
            getLimitCounterDB(payerPhoneNumber,beneficiaryId,"P2B");
            value = getLimitRuleCountAmount("LIMITS-2",beneficiaryDetails.getInstrumentPreferences().getOtherBank().getAccounts().get(0).getLimits());
        }else{
            String beneficiarId = "ALL";
            getLimitCounterDB(payerPhoneNumber,beneficiarId,"P2P_P2B");
            value = getLimitRuleCountAmount("NON-BENEFICIARY-LIMITS-1",null);
        }
        String updatedCounterValue = updateGenericLimitCounter(value,"month","amount");
        if(this.orignalCounterValue!=null){
            updateGenericLimitCounterDB(updatedCounterValue);
        }else{
            this.orignalCounterValue=this.defaultCounterValue;
            insertGenericLimitCounterDB(updatedCounterValue);
        }
    }

    public void resetBeneficiaryCounterValue(){
        updateGenericLimitCounterDB(this.orignalCounterValue);
    }


}
