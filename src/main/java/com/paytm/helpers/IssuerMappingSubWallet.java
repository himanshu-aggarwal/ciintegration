package com.paytm.helpers;

import com.paytm.apiPojo.SubWalletConsolidatedTxn;
import com.paytm.constants.Constants;
import com.paytm.constants.DBQueries;
import com.paytm.enums.DBEnums;
import com.paytm.framework.utils.DatabaseUtil;
import com.paytm.constants.LocalConfig;
import com.paytm.utils.DBValidation;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class IssuerMappingSubWallet {

	public List<Map<String, Object>> fetchSubWalletData(List<Map<String, Object>> ppiDetails,
			Map<Object, Object> params, String key) {
		String query = null;
		List<Map<String, Object>> list = new ArrayList<>();
		DBValidation db = new DBValidation();
		switch (key) {
		case "foodLike":
			query = db.fetchQuery(DBQueries.foodBlockedTollFuelPpiDetail, params);
			list = db.runDbQuery(query, params, "log4j");
			break;
		case "giftLike":
			query = db.fetchQuery(DBQueries.giftIntlPpiDetail, params);
			list = db.runDbQuery(query, params, "log4j");

			break;
		case "clwLike":
			if (params.containsKey(DBEnums.MERCHANTGUID)) {
				query = db.fetchQuery(DBQueries.clwPpiDetail, params);
				list = db.runDbQuery(query, params, "log4j");
			}
			break;
		}
		if (list.size() > 0)
			ppiDetails.addAll(list);
		return ppiDetails;
	}

	public Map<String, Map<String, Object>> checkBalanceMerchantSpecificDetail(Map<Object, Object> params) {
		params = addSubcategoryToMerchant(params);
		List<Map<String, Object>> issuerConfigDetails = issuerDependentDetails(params);
		List<Map<String, Object>> subWalletConfigDetails = subWalletConfigDetails(params);
		issuerConfigDetails.addAll(subWalletConfigDetails);
		System.out.println("Now issuerConfigDetails is " + issuerConfigDetails);
		Map<String, Map<String, Object>> map = new HashMap<>();
		for (int i = 0; i < issuerConfigDetails.size(); i++) {
			List<Object> list = new ArrayList<>();
			BigDecimal amount = BigDecimal.ZERO;
			Map<String, Object> map1 = new HashMap<>();
			amount = new BigDecimal(issuerConfigDetails.get(i).get("balance").toString());
			Object sctId = issuerConfigDetails.get(i).get("sct_id");
			if (sctId != null)
				list.add(sctId);
			if (map.size() != 0 && map.containsKey(issuerConfigDetails.get(i).get("merchant_id").toString())) {
				map1 = map.get(issuerConfigDetails.get(i).get("merchant_id").toString());
				if (map1.containsKey("sctId")) {
					@SuppressWarnings("unchecked")
					List<Object> sctIdList = (List<Object>) map1.get("sctId");
					list.add(sctIdList);
				}
				if (map1.containsKey(
						issuerConfigDetails.get(i).get("ppi_type").toString().replaceAll("[\\[\\]]", ""))) {
					BigDecimal amountVar = (BigDecimal) map1
							.get(issuerConfigDetails.get(i).get("ppi_type").toString().replaceAll("[\\[\\]]", ""));
					amount = amount.add(amountVar);
				}
			}
			map1.put(issuerConfigDetails.get(i).get("ppi_type").toString(), amount);
			if (list.size() > 0)
				map1.put("sctId", list);
			map.put(issuerConfigDetails.get(i).get("merchant_id").toString(), map1);
		}
		return map;
	}


	public Map<String, Map<String, Object>> checkBalanceMerchantSpecificDetailP2P(Map<Object, Object> params) {
		DBValidation dbValidation = new DBValidation();
		List<Map<String, Object>> subCategoryList = dbValidation.runDbQuery(DBQueries.fetchFlaggedMerchantCategory, params, "log4j");
		if (subCategoryList.get(0).get("sub_category") == null)
			params.put(DBEnums.SUBCATEGORY, "NULL");
		else
			params.put(DBEnums.SUBCATEGORY,
					subCategoryList.get(0).get("sub_category").toString().replaceAll("[\\[\\]]", ""));
		List<Map<String, Object>> issuerConfigDetails = issuerDependentDetails(params);
		List<Map<String, Object>> subWalletConfigDetails = subWalletConfigDetails(params);
		issuerConfigDetails.addAll(subWalletConfigDetails);
		System.out.println("Now issuerConfigDetails is " + issuerConfigDetails);
		Map<String, Map<String, Object>> map = new HashMap<>();
		for (int i = 0; i < issuerConfigDetails.size(); i++) {
			List<Object> list = new ArrayList<>();
			BigDecimal amount = BigDecimal.ZERO;
			Map<String, Object> map1 = new HashMap<>();
			amount = new BigDecimal(issuerConfigDetails.get(i).get("balance").toString());
			Object sctId = issuerConfigDetails.get(i).get("sct_id");
			if (sctId != null)
				list.add(sctId);
			if (map.size() != 0 && map.containsKey(issuerConfigDetails.get(i).get("merchant_id").toString())) {
				map1 = map.get(issuerConfigDetails.get(i).get("merchant_id").toString());
				if (map1.containsKey("sctId")) {
					@SuppressWarnings("unchecked")
					List<Object> sctIdList = (List<Object>) map1.get("sctId");
					list.add(sctIdList);
				}
				if (map1.containsKey(
						issuerConfigDetails.get(i).get("ppi_type").toString().replaceAll("[\\[\\]]", ""))) {
					BigDecimal amountVar = (BigDecimal) map1
							.get(issuerConfigDetails.get(i).get("ppi_type").toString().replaceAll("[\\[\\]]", ""));
					amount = amount.add(amountVar);
				}
			}
			map1.put(issuerConfigDetails.get(i).get("ppi_type").toString(), amount);
			if (list.size() > 0)
				map1.put("sctId", list);
			map.put(issuerConfigDetails.get(i).get("merchant_id").toString(), map1);
		}
		return map;
	}


	private Map<Object, Object> fetchTotalAmount(Map<Object, Object> userSubWalletDetail) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (Object ppiType : userSubWalletDetail.keySet()) {
			@SuppressWarnings("unchecked")
			List<Object> subwalletBalance = (List<Object>) userSubWalletDetail.get(ppiType);
			totalAmount = totalAmount.add(new BigDecimal(subwalletBalance.get(1).toString()));
		}
		userSubWalletDetail.put("totalAmount", totalAmount);
		return userSubWalletDetail;
	}

	public Map<Object, Object> addSubcategoryToMerchant(Map<Object, Object> params) {
		DBValidation dbValidation = new DBValidation();
		List<Map<String, Object>> subCategoryList = dbValidation.runDbQuery(DBQueries.fetchMerchantCategory, params,
				"log4j");
		if (subCategoryList.get(0).get("sub_category") == null)
			params.put(DBEnums.SUBCATEGORY, "NULL");
		else
			params.put(DBEnums.SUBCATEGORY,
					subCategoryList.get(0).get("sub_category").toString().replaceAll("[\\[\\]]", ""));
		return params;
	}

	public Map<Object, Object> userSubWalletDetail(Map<Object, Object> params) {
		List<Map<String, Object>> issuerConfigDetails = issuerDependentDetails(params);
		List<Map<String, Object>> subWalletConfigDetails = subWalletConfigDetails(params);
		issuerConfigDetails.addAll(subWalletConfigDetails);
		System.out.println("issuerConfigDetails is " + issuerConfigDetails);
		Set<Map<String, Object>> set = new HashSet<Map<String, Object>>(issuerConfigDetails);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(set);
		Map<Object, Object> map = new HashMap<>();
		for (int i = 0; i < list.size(); i++) {
			BigDecimal amount = BigDecimal.ZERO;
			List<Object> newList = new ArrayList<>();
			if (map.containsKey(issuerConfigDetails.get(i).get("ppi_type").toString())) {
				List<Object> ppiList = (List<Object>) map.get(issuerConfigDetails.get(i).get("ppi_type").toString());
				BigDecimal tempAmount = new BigDecimal(ppiList.get(1).toString());
				amount = tempAmount.add(new BigDecimal(issuerConfigDetails.get(i).get("balance").toString()));
				newList.add("");
			} else {
				amount = new BigDecimal(issuerConfigDetails.get(i).get("balance").toString());
				String[] ppiIdList = issuerConfigDetails.get(i).get("ppiId").toString().split(",", -1);
				if (ppiIdList.length > 1)
					newList.add("");
				else
					newList.add(ppiIdList[0].toString());
			}
			newList.add(amount);
			map.put(issuerConfigDetails.get(i).get("ppi_type").toString(), newList);
		}
		map = fetchTotalAmount(map);
		return map;

	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> merchantBalanceMap(List<Map<String, Object>> issuerConfigDetails) {
		Map<String, Map<String, Map<String, Object>>> merchantMap = configurationMap(issuerConfigDetails);
		System.out.println("merchantMap is " + merchantMap);
		BigDecimal dailyTxnAmount = BigDecimal.ZERO;
		BigDecimal debitAmount = BigDecimal.ZERO;
		int dailyTxnCount = 0;
		int throughputCount = 0;
		Map<String, Object> subwalletMap = new LinkedHashMap<String, Object>();
		System.out.println("Merchant Map is " + merchantMap);
		List<Map<String, Object>> finalIssuerConfigDetails = new ArrayList<>();
		for (String key1 : merchantMap.keySet()) {
			DBValidation db = new DBValidation();
			Map<Object, Object> paramMap = new HashMap<>();
			BigDecimal countBalance = BigDecimal.ZERO;
			BigDecimal amountBalance = BigDecimal.ZERO;
			BigDecimal balance = BigDecimal.ZERO;
			paramMap.put(DBEnums.MERCHANTID, key1);
			List<Map<String, Object>> configList = db.runDbQuery(DBQueries.issuerSubwalletConfig, paramMap, "log4j");
			dailyTxnCount = Integer.parseInt(configList.get(0).get("daily_txn_count").toString());
			dailyTxnAmount = new BigDecimal(configList.get(0).get("daily_txn_amount").toString());
			for (String key : merchantMap.get(key1).keySet()) {
				Map<String, Map<String, Object>> merchantList = merchantMap.get(key1);
				debitAmount = new BigDecimal(merchantList.get(key).get("consolidatedDebitAmount").toString());
				throughputCount = Integer.parseInt(merchantList.get(key).get("consolidateCount").toString());
				subwalletMap = (Map<String, Object>) merchantList.get(key).get("subwalletDetails");
				if (subwalletMap.size() > 0) {
					if (dailyTxnCount > Integer.parseInt(merchantList.get(key).get("consolidateCount").toString())) {
						BigDecimal userBalance = new BigDecimal(
								merchantList.get(key).get("subWalletbalance").toString());
						countBalance = userBalance;
						amountBalance = userBalance;
						if (dailyTxnCount >= subwalletMap.size()) {
							countBalance = userBalance;
						} else if (dailyTxnCount < subwalletMap.size()) {
							countBalance = BigDecimal.ZERO;
							for (Object subwallet : subwalletMap.keySet()) {
								while (dailyTxnCount > 0) {
									countBalance = countBalance
											.add(new BigDecimal(subwalletMap.get(subwallet).toString()));
									dailyTxnCount--;
								}
							}
						}
						if (dailyTxnAmount.compareTo(debitAmount) == 0)
							amountBalance = dailyTxnAmount.subtract(debitAmount);
						if (dailyTxnAmount.compareTo(debitAmount) == 1) {
							amountBalance = dailyTxnAmount.subtract(debitAmount);
							if (userBalance.compareTo(dailyTxnAmount.subtract(debitAmount)) == -1)
								amountBalance = userBalance;
						}
					}
				}
				if (countBalance.compareTo(amountBalance) == 1)
					balance = amountBalance;
				else if (amountBalance.compareTo(countBalance) == 1)
					balance = countBalance;
				else if (amountBalance.compareTo(countBalance) == 0)
					balance = countBalance;
				Map<String, Object> finalMap = new HashMap<>();
				finalMap.put("merchant_id", key1);
				finalMap.put("balance", balance.setScale(2));
				finalMap.put("ppi_type", merchantList.get(key).get("ppiType").toString());
				finalMap.put("sct_id", merchantList.get(key).get("sctId"));
				finalMap.put("ppiId", merchantList.get(key).get("ppiId").toString());
				finalMap.put("dailyTxnAmount", dailyTxnAmount);
				finalMap.put("debitAmount", debitAmount);
				finalMap.put("dailyTxnCount", dailyTxnCount);
				finalMap.put("throughputCount", throughputCount);
				finalIssuerConfigDetails.add(finalMap);
			}
		}
		return finalIssuerConfigDetails;

	}

	@SuppressWarnings("unchecked")
	public Map<String, Map<String, Map<String, Object>>> configurationMap(
			List<Map<String, Object>> issuerConfigDetails) {
		System.out.println("issuerConfigDetails is " + issuerConfigDetails);
		Map<String, Map<String, Map<String, Object>>> merchantMap = new LinkedHashMap<>();
		for (int i = 0; i < issuerConfigDetails.size(); i++) {
			SubWalletConsolidatedTxn consolidate = new SubWalletConsolidatedTxn();
			UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			Map<String, Map<String, Object>> finalMap = new LinkedHashMap<String, Map<String, Object>>();
			Map<String, Object> map1 = new LinkedHashMap<String, Object>();
			BigDecimal subWalletbalance = new BigDecimal(issuerConfigDetails.get(i).get("balance").toString());
			Object sctId = issuerConfigDetails.get(i).get("sct_id");
			String ppiId = issuerConfigDetails.get(i).get("id").toString();
			int consolidateCount = 0;
			BigDecimal consolidatedDebitAmount = BigDecimal.ZERO;
			String mid = issuerConfigDetails.get(i).get("merchant_id").toString();
			String ppiType = issuerConfigDetails.get(i).get("ppi_type").toString();
			if (sctId != null) {
				consolidate = userSubwalletConsolidated.dailyTxnQuery(sctId.toString(), consolidate);
				if (consolidate.getDebit() != null) {
					consolidateCount = consolidate.getDebit().getTransactionCount();
					consolidatedDebitAmount = consolidate.getDebit().getThroughputAmount();
				}
			}
			if (!merchantMap.containsKey(mid)
					|| (merchantMap.containsKey(mid) && !merchantMap.get(mid).containsKey(ppiType))) {
				if (merchantMap.containsKey(mid))
					finalMap = merchantMap.get(mid);
				map.put("subWalletbalance", subWalletbalance);
				map.put("consolidatedDebitAmount", consolidatedDebitAmount);
				map.put("consolidateCount", consolidateCount);
				map.put("ppiType", ppiType);
				map.put("sctId", sctId);
				map.put("ppiId", ppiId);
				Map<String, Object> map2 = new LinkedHashMap<>();
				map2.put(ppiId, subWalletbalance);
				map.put("subwalletDetails", map2);
				finalMap.put(ppiType, map);
				merchantMap.put(mid, finalMap);
			} else if (merchantMap.containsKey(mid) && merchantMap.get(mid).containsKey(ppiType)) {
				BigDecimal subwalletBalance = new BigDecimal(
						merchantMap.get(mid).get(ppiType).get("subWalletbalance").toString()).add(subWalletbalance);
				BigDecimal throughputDebitAmount = new BigDecimal(
						merchantMap.get(mid).get(ppiType).get("consolidatedDebitAmount").toString())
								.add(consolidatedDebitAmount);
				int finalCount = Integer.parseInt(merchantMap.get(mid).get(ppiType).get("consolidateCount").toString())
						+ consolidateCount;
				map.put("subWalletbalance", subwalletBalance);
				map.put("consolidatedDebitAmount", throughputDebitAmount);
				map.put("consolidateCount", finalCount);
				map.put("ppiType", ppiType);
				Set<Object> sctIdList = new HashSet<Object>();
				sctIdList.add(sctId);
				sctIdList.add(merchantMap.get(mid).get(ppiType).get("sctId").toString());
				map.put("sctId", sctIdList.toString().replaceAll("[\\[\\]]", ""));
				Set<Object> ppiIdList = new HashSet<Object>();
				ppiIdList.add(ppiId);
				ppiIdList.add(merchantMap.get(mid).get(ppiType).get("ppiId").toString());
				map.put("ppiId", ppiIdList.toString().replaceAll("[\\[\\]]", ""));
				map1 = (Map<String, Object>) merchantMap.get(mid).get(ppiType).get("subwalletDetails");
				map1.put(ppiId, subWalletbalance);
				map.put("subwalletDetails", map1);
				merchantMap.get(mid).put(ppiType, map);
			}
		}
		return merchantMap;
	}

	private List<Map<String, Object>> acquirerCategorySubwallet(Map<Object, Object> params) {
		DBValidation db = new DBValidation();
		List<Map<String, Object>> acquirerCategoryBasedDetails = new ArrayList<>();
		List<Map<String, Object>> result = new ArrayList<>();
		acquirerCategoryBasedDetails = db.runDbQuery(DBQueries.acquirerCategoryBasedPpiDetail, params, "log4j");
		List<Map<String, Object>> acquirerCategoryBasedMap = new ArrayList<>();
		Map<Object, List<Map<String, Object>>> map = new HashMap<Object, List<Map<String, Object>>>();
		for (int i = 0; i < acquirerCategoryBasedDetails.size(); i++) {
			Map<Object, Object> issuerMap = new HashMap<>();
			issuerMap.put(DBEnums.ISSUERID, acquirerCategoryBasedDetails.get(i).get("merchant_id"));
			issuerMap.put(DBEnums.ISSUERPPIID, acquirerCategoryBasedDetails.get(i).get("ppi_type"));
			if (map.containsKey(issuerMap))
				result = map.get(issuerMap);
			else {
				result = db.runDbQuery(DBQueries.subWalletIssuerConfigEntry, issuerMap, "log4j");
				map.put(issuerMap, result);
			}
			if (result.size() == 0)
				acquirerCategoryBasedMap.add(acquirerCategoryBasedDetails.get(i));
		}
		System.out.println("acquirerCategoryBasedMap is " + acquirerCategoryBasedMap);
		return acquirerCategoryBasedMap;
	}

	public List<Map<String, Object>> issuerConfigDetails(Map<Object, Object> params) {
		List<Map<String, Object>> issuerConfigDetails = new ArrayList<>();
		List<Map<String, Object>> issuerConfigNullDetails = new ArrayList<>();
		List<Map<String, Object>> issuerConfigNullCLSWDetails = new ArrayList<>();
		DBValidation db = new DBValidation();
		issuerConfigDetails = db.runDbQuery(DBQueries.issuerMappedSubWalletDetails, params, "log4j");
		issuerConfigNullDetails = db.runDbQuery(DBQueries.issuerMappedNullCategorySubDetails, params, "log4j");
		issuerConfigNullCLSWDetails = db.runDbQuery(DBQueries.issuerMappedNullCategoryCLSW, params, "log4j");
		issuerConfigDetails.addAll(issuerConfigNullDetails);
		issuerConfigDetails.addAll(issuerConfigNullCLSWDetails);
		return merchantBalanceMap(issuerConfigDetails);
	}

	public List<Map<String, Object>> issuerDependentDetails(Map<Object, Object> params) {
		List<Map<String, Object>> issuerConfigDetails = issuerConfigDetails(params);
		List<Map<String, Object>> acquirerCategoryBasedMap = acquirerCategorySubwallet(params);
		issuerConfigDetails.addAll(acquirerCategoryBasedMap);
		return issuerConfigDetails;

	}

	public List<Map<String, Object>> subWalletConfigDetails(Map<Object, Object> params) {
		List<Map<String, Object>> ppiDetails = new ArrayList<>();
		List<Map<String, Object>> subWalletConfigDetails = new ArrayList<>();
		subWalletConfigDetails = DatabaseUtil.getInstance()
				.executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, DBQueries.subWalletConfig);
		List<String> foodPpiList = new ArrayList<>();
		List<String> giftPpiList = new ArrayList<>();
		List<String> clwPpiList = new ArrayList<>();
		for (int i = 0; i < subWalletConfigDetails.size(); i++) {
			String ppiType = subWalletConfigDetails.get(i).get("request_name").toString();
			if (ppiType.equals("FOOD") || ppiType.equals("BLOCKED") || ppiType.equals("TOLL") || ppiType.equals("FUEL"))
				foodPpiList.add(
						LocalConfig.subWalletMapping.get(subWalletConfigDetails.get(i).get("request_name").toString()));
			if (ppiType.equals("GIFT") || ppiType.equals("INTERNATIONAL_FUNDS_TRANSFER") || ppiType.equals("CASHBACK"))
				giftPpiList.add(
						LocalConfig.subWalletMapping.get(subWalletConfigDetails.get(i).get("request_name").toString()));
			if (ppiType.equals("CLOSED_LOOP_WALLET"))
				clwPpiList.add(
						LocalConfig.subWalletMapping.get(subWalletConfigDetails.get(i).get("request_name").toString()));
		}
		if (foodPpiList.size() > 0) {
			params.put(DBEnums.PPITYPE, foodPpiList.toString().replaceAll("[\\[\\]]", ""));
			ppiDetails = fetchSubWalletData(ppiDetails, params, "foodLike");
		}
		if (giftPpiList.size() > 0) {
			params.put(DBEnums.PPITYPE, giftPpiList.toString().replaceAll("[\\[\\]]", ""));
			ppiDetails = fetchSubWalletData(ppiDetails, params, "giftLike");
		}
		if (clwPpiList.size() > 0) {
			params.put(DBEnums.PPITYPE, clwPpiList.toString().replaceAll("[\\[\\]]", ""));
			ppiDetails = fetchSubWalletData(ppiDetails, params, "clwLike");
		}
		System.out.println("ppiDetails is " + ppiDetails);
		return ppiDetails;

	}

	public static void main(String[] args) throws IOException {
		Map<Object, Object> params = new HashMap<>();
		IssuerMappingSubWallet issue = new IssuerMappingSubWallet();
		DBValidation db = new DBValidation();
		params.put(DBEnums.MERCHANTID, "1107235412");
		params.put(DBEnums.USERID, "1107223291");
		List<Map<String, Object>> list = db.runDbQuery(DBQueries.fetchFlaggedMerchantCategory, params, "log4j");
		params.put(DBEnums.SUBCATEGORY, list.get(0).get("sub_category").toString().replaceAll("[\\[\\]]", ""));
		System.out.println("Issuer is " + issue.checkBalanceMerchantSpecificDetail(params));
	}

	private void setDebitInfo(String[] sctId) {
		DBValidation db = new DBValidation();
		Map<Object, Object> paramMap = new HashMap<>();
		paramMap.put(DBEnums.SCTID, sctId[0]);
		String query = db.fetchQuery(DBQueries.userSubwalletConsolidateTxn, paramMap);
		DatabaseUtil.getInstance().executeUpdateQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, query);

	}

	public Map<Object, Object> subwalletLimit(Map<String, Object> detailMap) {
		BigDecimal debitAmount = (BigDecimal) detailMap.get("debitAmount");
		int debitCount = Integer.parseInt(detailMap.get("throughputCount").toString());
		if (debitAmount.compareTo(BigDecimal.ZERO) == 0) {
			setDebitInfo(detailMap.get("sct_id").toString().split(","));
			debitAmount = BigDecimal.valueOf(10.00);
			debitCount = 1;
		}
		String ppiType = detailMap.get("ppi_type").toString();
		String mid = detailMap.get("merchant_id").toString();
		Map<Object, Object> paramMap = new HashMap<>();
		paramMap.put(DBEnums.DAILYTXNAMOUNT, debitAmount);
		paramMap.put(DBEnums.DAILYTXNCOUNT, debitCount);
		paramMap.put(DBEnums.PPITYPE, ppiType);
		paramMap.put(DBEnums.MERCHANTID, mid);

		return paramMap;
	}

	public void subwalletCountLimitBreach(Map<Object, Object> params) throws IOException {
		DBValidation db = new DBValidation();
		List<Map<String, Object>> issuerSubwalletDetails = issuerConfigDetails(params);
		for (int i = 0; i < issuerSubwalletDetails.size(); i++) {
			Map<String, Object> detailMap = issuerSubwalletDetails.get(i);
			int dailyTxnCount = Integer.parseInt(detailMap.get("dailyTxnCount").toString());
			Map<Object, Object> paramMap = subwalletLimit(detailMap);
			paramMap.put(DBEnums.DAILYTXNAMOUNT, 10000);
			int debitCount = Integer.parseInt(paramMap.get(DBEnums.DAILYTXNCOUNT).toString());
			if (dailyTxnCount > debitCount) {
				db.runUpdateQuery(DBQueries.subwalletIssuerConfigUpdate, paramMap);
			}
			System.out.println("testing");
		}
	}

	public void subwalletAmountLimitBreach(Map<Object, Object> params){
		DBValidation db = new DBValidation();
		List<Map<String, Object>> issuerSubwalletDetails = issuerConfigDetails(params);
		for (int i = 0; i < issuerSubwalletDetails.size(); i++) {
			BigDecimal dailyTxnAmount = (BigDecimal) issuerSubwalletDetails.get(i).get("dailyTxnAmount");
			Map<String, Object> detailMap = issuerSubwalletDetails.get(i);
			Map<Object, Object> paramMap = subwalletLimit(detailMap);
			paramMap.put(DBEnums.DAILYTXNCOUNT, 10000);
			BigDecimal debitAmount = (BigDecimal) paramMap.get(DBEnums.DAILYTXNAMOUNT);
			if (!(debitAmount.compareTo(dailyTxnAmount) == 0) && (dailyTxnAmount.compareTo(debitAmount) == 1)) {
				db.runUpdateQuery(DBQueries.subwalletIssuerConfigUpdate, paramMap);
			}
			System.out.println("testing");
		}
	}

	public void resetLimit() {
		DBValidation db = new DBValidation();
		Map<Object, Object> paramMap = new HashMap<Object, Object>();
		paramMap.put(DBEnums.DAILYTXNCOUNT, 10000);
		paramMap.put(DBEnums.DAILYTXNAMOUNT, 10000);
		paramMap.put(DBEnums.PPITYPE, "8");
		paramMap.put(DBEnums.MERCHANTID, "6393");
		String query = db.fetchQuery(DBQueries.subwalletIssuerConfigUpdate, paramMap);
		DatabaseUtil.getInstance().executeUpdateQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, query);
	}
}
