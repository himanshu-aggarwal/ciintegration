package com.paytm.helpers;

import com.paytm.apiRequestBuilders.wallet.walletWeb.UpgradeWallet;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.UserHeaders;

public class TransferToBankHelpers {

	public void upgradeWallet(String phone, String state) {
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		UpgradeWallet upgradeWallet = new UpgradeWallet(UpgradeWallet.defaultRequest, token);
		upgradeWallet.getRequestPojo().getRequest().withSsoId(custId).withWalletState(state);
		upgradeWallet.createRequestJsonAndExecute();

		System.out.println("Response is " + upgradeWallet.getApiResponse());
	}
	
	public void updateOtpValidation()
	{
		
	}

}
