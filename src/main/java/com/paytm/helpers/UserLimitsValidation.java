package com.paytm.helpers;

import com.paytm.apiPojo.AuditInfo;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitFetchResponse;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitSetRequest.Request;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitSetRequest.Request.ConfigList;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitSetRequest.Request.ConfigList.PeriodLimits;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitSetRequest.Request.ConfigList.PeriodLimits.SpecificPeriodLimit;
import com.paytm.apiRequestBuilders.wallet.walletWeb.UserDefinedLimitFetch;
import com.paytm.apiRequestBuilders.wallet.walletWeb.UserDefinedLimitSet;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.APIStatus;
import com.paytm.enums.LimitEnums.LimitPeriodEnums;
import com.paytm.enums.LimitEnums.LimitTypeEnums;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserHeaders;
import com.paytm.exceptions.LimitsException.UserDefinedLimits.UserLimitValidationException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UserLimitsValidation {

    private String existingJson;

    private void setAmount(BigDecimal amount, SpecificPeriodLimit specificPeriodLimit) {
        specificPeriodLimit.withAmount(amount);
    }

    private void setCount(int count, SpecificPeriodLimit specificPeriodLimit) {
        specificPeriodLimit.withCount(count);
    }

    private SpecificPeriodLimit getSpecificPeriod(PeriodLimits periodLimits, LimitPeriodEnums period) {
        if (period.toString().equalsIgnoreCase("day")) {
            return periodLimits.getDayPeriodLimit();
        } else if (period.toString().equalsIgnoreCase("month")) {
            return periodLimits.getMonthPeriodLimit();
        }
        throw new UserLimitValidationException("Period entered is incorrect : " + period);
    }

    private ConfigList getConfigList(LimitTypeEnums limitType, Request request) {
        if (limitType.toString().equalsIgnoreCase("WALLET_TO_WALLET_TRANSFER_LIMIT")) {
            return request.getConfigList().get("WALLET_TO_WALLET_TRANSFER_LIMIT");
        } else if (limitType.toString().equalsIgnoreCase("WALLET_TO_BANK_TRANSFER_LIMIT")) {
            return request.getConfigList().get("WALLET_TO_BANK_TRANSFER_LIMIT");
        } else if (limitType.toString().equalsIgnoreCase("PURCHASE_LIMIT")) {
            return request.getConfigList().get("PURCHASE_LIMIT");
        }
        throw new UserLimitValidationException("limitType entered is incorrect : " + limitType);
    }

    private BigDecimal getConsumedAmount(String mobileNumber, LimitPeriodEnums period, LimitTypeEnums limitType) {
        LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
        AuditInfo.LimitAuditInfo limitAuditInfo = limitAuditInfoValidation.getUserLimitAuditInfo(mobileNumber);
        List<Map<String, Object>> txnTypeList = limitAuditInfoValidation.getTxnTypeList(limitType);
        int transaction = Integer.parseInt(new LinkedList<>(Arrays.asList(txnTypeList.get(0).get("txn_type_list").toString().replace("[", "").replace("]", "").trim()
                .split(","))).get(0));
        Short operationType = ((Number) txnTypeList.get(0).get("operation_type")).shortValue();
        limitAuditInfoValidation.validateAllLimitDetails(limitAuditInfo, transaction, operationType);
        BigDecimal amount = limitAuditInfoValidation.consumableAmountPerLimit(txnTypeList, limitAuditInfo, period);
        return amount.compareTo(BigDecimal.ZERO)>0 ? amount : amount.negate();
    }

    private int getConsumedCount(String mobileNumber, LimitPeriodEnums period, LimitTypeEnums limitType) {
        LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
        AuditInfo.LimitAuditInfo limitAuditInfo = limitAuditInfoValidation.getUserLimitAuditInfo(mobileNumber);
        List<Map<String, Object>> txnTypeList = limitAuditInfoValidation.getTxnTypeList(limitType);
        int transaction = Integer.parseInt(new LinkedList<>(Arrays.asList(txnTypeList.get(0).get("txn_type_list").toString().replace("[", "").replace("]", "").trim()
                .split(","))).get(0));
        Short operationType = ((Number) txnTypeList.get(0).get("operation_type")).shortValue();
        limitAuditInfoValidation.validateAllLimitDetails(limitAuditInfo, transaction, operationType);
        return limitAuditInfoValidation.consumableCountPerLimit(txnTypeList, limitAuditInfo, period);
    }

    private void executeUserDefinedLimitSet(UserDefinedLimitSet userDefinedLimitSet) {
        userDefinedLimitSet.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(userDefinedLimitSet, APIStatus.UserDefinedLimitSet.SUCCESS);
    }

    private UserDefinedLimitFetchResponse fetchUserDefineLimits(String phoneNumber) {
        UserDefinedLimitFetch userDefinedLimitFetch = new UserDefinedLimitFetch(phoneNumber);
        userDefinedLimitFetch.setCaptureExtent(false);
        userDefinedLimitFetch.createRequestJsonAndExecute();
        return userDefinedLimitFetch.getResponsePojo();
    }

    private void mapUserDefinedFetchResponseWithSetRequest(UserDefinedLimitFetchResponse userDefinedLimitFetchResponse) {
        UserDefinedLimitSet userDefinedLimitSet = new UserDefinedLimitSet(UserDefinedLimitSet.allFalseUDLSetRequest);
        userDefinedLimitSet.setCaptureExtent(false);
        for (String config : userDefinedLimitFetchResponse.getConfigList().keySet()) {
            int status = userDefinedLimitFetchResponse.getConfigList().get(config).getStatus() ? 1 : 0;
            BigDecimal dayAmount = userDefinedLimitFetchResponse.getConfigList().get(config).getPeriodLimits().getDayPeriodLimit().getAmount();
            Integer dayCount = userDefinedLimitFetchResponse.getConfigList().get(config).getPeriodLimits().getDayPeriodLimit().getCount();
            BigDecimal monthAmount = userDefinedLimitFetchResponse.getConfigList().get(config).getPeriodLimits().getMonthPeriodLimit().getAmount();
            Integer monthCount = userDefinedLimitFetchResponse.getConfigList().get(config).getPeriodLimits().getMonthPeriodLimit().getCount();
            userDefinedLimitSet.getRequestPojo().getRequest().getConfigList().get(config).withStatus(status);
            userDefinedLimitSet.getRequestPojo().getRequest().getConfigList().get(config).createPeriodLimits();
            userDefinedLimitSet.getRequestPojo().getRequest().getConfigList().get(config).getPeriodLimits().getDayPeriodLimit().withAmount(dayAmount).withCount(dayCount);
            userDefinedLimitSet.getRequestPojo().getRequest().getConfigList().get(config).getPeriodLimits().getMonthPeriodLimit().withAmount(monthAmount).withCount(monthCount);
        }
        this.existingJson = userDefinedLimitSet.createRequestJsonOnly();
    }

    private void setLimitsToNullOnUser(String walletType, UserDefinedLimitSet userDefinedLimitSet){
        if(!walletType.equalsIgnoreCase("PAYTM_PRIME_WALLET")&&!walletType.equalsIgnoreCase("PAYTM_ADHAAR_OTP_KYC")){
            userDefinedLimitSet.getRequestPojo().getRequest().getConfigList().put("WALLET_TO_WALLET_TRANSFER_LIMIT",null);
            userDefinedLimitSet.getRequestPojo().getRequest().getConfigList().put("WALLET_TO_BANK_TRANSFER_LIMIT",null);
        }
    }


    public void setUserDefinedLimitAmount(String phoneNumber, LimitPeriodEnums period, LimitTypeEnums limitType) {
        BigDecimal amount = FundThreadManager.getInstance().getTotalAmount().subtract(BigDecimal.ONE);
        amount = amount.compareTo(BigDecimal.ZERO)>0?amount:BigDecimal.ZERO;
        UserDefinedLimitFetchResponse userDefinedLimitFetchResponse = fetchUserDefineLimits(phoneNumber);
        String walletType = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETRBITYPE,phoneNumber);
        mapUserDefinedFetchResponseWithSetRequest(userDefinedLimitFetchResponse);
        UserDefinedLimitSet userDefinedLimitSet = new UserDefinedLimitSet(UserDefinedLimitSet.normalUDLSetRequest, phoneNumber);
        userDefinedLimitSet.setCaptureExtent(false);
        amount = amount.add(getConsumedAmount(phoneNumber, period, limitType));
        setAmount(amount, getSpecificPeriod(getConfigList(limitType, userDefinedLimitSet.getRequestPojo().getRequest()).getPeriodLimits(), period));
        setLimitsToNullOnUser(walletType,userDefinedLimitSet);
        executeUserDefinedLimitSet(userDefinedLimitSet);
    }

    public void setUserDefinedLimitCount(String phoneNumber, LimitPeriodEnums period, LimitTypeEnums limitType) {
        UserDefinedLimitFetchResponse userDefinedLimitFetchResponse = fetchUserDefineLimits(phoneNumber);
        String walletType = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETRBITYPE,phoneNumber);
        mapUserDefinedFetchResponseWithSetRequest(userDefinedLimitFetchResponse);
        UserDefinedLimitSet userDefinedLimitSet = new UserDefinedLimitSet(UserDefinedLimitSet.normalUDLSetRequest, phoneNumber);
        userDefinedLimitSet.setCaptureExtent(false);
        int count = getConsumedCount(phoneNumber, period, limitType);
        if(count==0){
            switch(limitType) {
                case PURCHASE_LIMIT: {
                    WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
                    walletAPIHelpers.mainWalletAdd(new BigDecimal(10), phoneNumber);
                    walletAPIHelpers.mainWalletWithdraw(new BigDecimal(10), phoneNumber, MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
                    break;
                }
                case WALLET_TO_WALLET_TRANSFER_LIMIT: {
                    WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
                    walletAPIHelpers.mainWalletAdd(new BigDecimal(10), phoneNumber);
                    walletAPIHelpers.sendMoney(phoneNumber,UserManager.getInstance().getPayeeUserDetails(UserHeaders.MOBILENUMBER),new BigDecimal(10));
                    break;
                }
            }
            count = 1;
        }
        setCount(count, getSpecificPeriod(getConfigList(limitType, userDefinedLimitSet.getRequestPojo().getRequest()).getPeriodLimits(), period));
        setLimitsToNullOnUser(walletType,userDefinedLimitSet);
        executeUserDefinedLimitSet(userDefinedLimitSet);
    }

    public void resetLimits(String phoneNumber) {
        UserDefinedLimitSet userDefinedLimitSet = new UserDefinedLimitSet(this.existingJson, phoneNumber);
        userDefinedLimitSet.setCaptureExtent(false);
        String walletType = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETRBITYPE,phoneNumber);
        setLimitsToNullOnUser(walletType,userDefinedLimitSet);
        executeUserDefinedLimitSet(userDefinedLimitSet);
    }


}
