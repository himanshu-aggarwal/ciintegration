package com.paytm.helpers;

import com.jcraft.jsch.JSchException;
import com.paytm.constants.Constants;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.OtpStrings;
import com.paytm.enums.UserHeaders;
import com.paytm.framework.utils.DatabaseUtil;

import java.util.List;
import java.util.Map;

public class OtpHelpers {
	
	public String getOtpForLogin(String uid)
	{
		String otp = null;
		String query = "select otp_code from one_time_password_auth where user_id = '" + uid
				+ "' and status = '0' and purpose = 'LOGIN_PHONE' order by update_timestamp desc limit 1;";
		List<Map<String, Object>> result1 = DatabaseUtil.getInstance()
				.executeSelectQuery(Constants.DBConnectionURL.AUTH_DB_CONNECTION_URL, query);
		otp = (String) result1.get(0).get("otp_code");
		return otp;
	}
	
	public String getTransactionOtp(String phone,String SendStrategy) throws InterruptedException, JSchException
	{
		String otp = getOtpForLogin(UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID,phone));
		if(otp == null)
		otp = FetchOtp(phone, SendStrategy);
		return otp;
		
	}

		public String FetchOtp(String phone, String SendStrategy){
			String otp = null;
			try {
				switch (SendStrategy) {
				case "LOGIN_OTP":
					otp = OtpStrings.LOGIN_OTP.getOtp(phone);
					break;
				case "Login_Signup_OTP":
					otp = OtpStrings.Login_Signup_OTP.getOtp(phone);
					break;
				case "KYC_verification_OTP":
					otp = OtpStrings.KYC_verification_OTP.getOtp(phone);
					break;
				case "Offline_payment_OTP":
					otp = OtpStrings.Offline_payment_OTP.getOtp(phone);
					break;
				case "Signup_OTP":
					otp = OtpStrings.Signup_OTP.getOtp(phone);
					break;
				case "Current_phone_change_OTP":
					otp = OtpStrings.Current_phone_change_OTP.getOtp(phone);
					break;
				case "P2P_OTP":
					otp = OtpStrings.P2P_OTP.getOtp(phone);
					break;
				case "OTP_on_phone_post_Phone_merge":
					otp = OtpStrings.OTP_on_phone_post_Phone_merge.getOtp(phone);
					break;
				case "DEVICE_P2P_OTP":
					otp = OtpStrings.DEVICE_P2P_OTP.getOtp(phone);
					break;
				case "Google_OTP":
					otp = OtpStrings.Google_OTP.getOtp(phone);
					break;
				case "LOGIN_MerchantUber_OTP":
					otp = OtpStrings.LOGIN_MerchantUber_OTP.getOtp(phone);
					break;
				case "SignUp_MerchantUber_OTP":
					otp = OtpStrings.SignUp_MerchantUber_OTP.getOtp(phone);
					break;
				case "SetUserDefinedLimit_OTP":
					otp = OtpStrings.SetUserDefinedLimit_OTP.getOtp(phone);
					break;
				default:
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return otp;
		}

		
}
