package com.paytm.helpers;

import com.paytm.apiPojo.wallet.APIInterface.SubWalletInterface;
import com.paytm.apiRequestBuilders.wallet.oldService.CheckUserBalance;
import com.paytm.apiRequestBuilders.wallet.qrCode.CreateDynamicQrCode;
import com.paytm.apiRequestBuilders.wallet.qrCode.CreateQrCode;
import com.paytm.apiRequestBuilders.wallet.qrCode.GenerateQrCode;
import com.paytm.apiRequestBuilders.wallet.qrCode.MapDynamicQrCode;
import com.paytm.apiRequestBuilders.wallet.walletWeb.*;
import com.paytm.constants.Constants;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.utils.DBValidation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WalletAPIHelpers {

	public BigDecimal checkBalance() {
		CheckBalance checkBalance = new CheckBalance(
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		checkBalance.setCaptureExtent(false);
		checkBalance.createRequestJsonAndExecute();
		BigDecimal totalWalletBal = checkBalance.getResponsePojo().getResponse().getAmount();
		if (null == totalWalletBal || totalWalletBal.toString().isEmpty()) {
			throw new RuntimeException("not able to get the user balance");
		}
		return totalWalletBal;
	}

	public AddFundsToSubWallet subWalletAdd(String mobile, String userSubWalletType, BigDecimal amount, String mGuid,
			String subWalletGuid) {
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, mobile);
		AddFundsToSubWallet addFundsToSubWallet = new AddFundsToSubWallet(AddFundsToSubWallet.defaultRequest, token);
		addFundsToSubWallet.setCaptureExtent(false);
		addFundsToSubWallet.getRequestPojo().setUpdateAmountManager(false);
		addFundsToSubWallet.getRequestPojo().getRequest().withPayeePhoneNumber(mobile).withAmount(amount)
				.withMerchantGuid(mGuid).withMerchantSubWalletGuid(subWalletGuid)
				.withUserSubWalletType(userSubWalletType);
		addFundsToSubWallet.getRequestPojo().setUpdateAmountManager(false);
		addFundsToSubWallet.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(addFundsToSubWallet,APIStatus.AddFundsToSubWallet.SUCCESS);
		return addFundsToSubWallet;
	}

	/**
	 * This can be used in case both the mGuid and subWalletGuid are of same merchant
	 *
	 * @param mobile - Mobile number to add funds
	 * @param userSubWalletType - SubWallet To add Funds
	 * @param amount - Amount to be added
	 * @param merchantTypes - Merchant Type to add funds from
	 *
	 * @return - Instance of AddFundsToSubWallet
	 */
	public AddFundsToSubWallet subWalletAdd(String mobile, String userSubWalletType, BigDecimal amount, MerchantTypes merchantTypes) {
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,MerchantHeaders.MERCHANTGUID);
		String subWalletGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,MerchantHeaders.CORPORATEWALLETGUID);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, mobile);
		AddFundsToSubWallet addFundsToSubWallet = new AddFundsToSubWallet(AddFundsToSubWallet.defaultRequest, token);
		addFundsToSubWallet.setCaptureExtent(false);
		addFundsToSubWallet.getRequestPojo().setUpdateAmountManager(false);
		addFundsToSubWallet.getRequestPojo().getRequest().withPayeePhoneNumber(mobile).withAmount(amount)
				.withMerchantGuid(mGuid).withMerchantSubWalletGuid(subWalletGuid)
				.withUserSubWalletType(userSubWalletType);
		addFundsToSubWallet.getRequestPojo().setUpdateAmountManager(false);
		addFundsToSubWallet.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(addFundsToSubWallet,APIStatus.AddFundsToSubWallet.SUCCESS);
		return addFundsToSubWallet;
	}

	private void setRequest(String userSubWalletType, SubWalletInterface obj, BigDecimal amount) {
		switch (userSubWalletType) {
		case "FOOD":
			obj.setfOOD(amount);
			break;
		case "TOLL":
			obj.settOLL(amount);
			break;
		case "GIFT":
			obj.setgIFT(amount);
			break;
		case "FUEL":
			obj.setfUEL(amount);
			break;
		case "CLOSED_LOOP_WALLET":
			obj.setcLOSED_LOOP_WALLET(amount);
			break;
		case "CLOSED_LOOP_SUB_WALLET":
			obj.setcLOSED_LOOP_SUB_WALLET(amount);
			break;
		case "INTERNATIONAL_FUNDS_TRANSFER":
			obj.setiNTERNATIONAL_FUNDS_TRANSFER(amount);
			break;
		case "GIFT_VOUCHER":
			obj.setgIFT_VOUCHER(amount);
			break;
		}
	}

	public void mainWalletWithdraw(BigDecimal amount, String mobileNumber, String merchantGuid) {
		WalletWithDraw walletWithDraw = new WalletWithDraw(WalletWithDraw.subWalletRequest,
				UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, mobileNumber));
		walletWithDraw.setCaptureExtent(false);
		walletWithDraw.getRequestPojo().setUpdateAmountManager(false);
		walletWithDraw.getRequestPojo().getRequest().withTotalAmount(amount).withMerchantGuid(merchantGuid);
		walletWithDraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithDraw, APIStatus.WalletWithdraw.SUCCESS);
	}

	public void subWalletWithdraw(String mobile, String userSubWalletType, BigDecimal amount, String mGuid) {
		WalletWithDraw walletWithDraw = new WalletWithDraw(WalletWithDraw.subWalletRequest,
				UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, mobile));
		walletWithDraw.getRequestPojo().setUpdateAmountManager(false);
		walletWithDraw.setCaptureExtent(false);
		walletWithDraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(amount)
				.withMerchantGuid(mGuid);
		setRequest(userSubWalletType, walletWithDraw.getRequestPojo().getRequest().getSubWalletAmount(), amount);
		walletWithDraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithDraw, APIStatus.WalletWithdraw.SUCCESS);
	}

	public void mainWalletAdd(BigDecimal balance, String phoneNo) {
		balance = balance.compareTo(BigDecimal.ONE)<0?balance.setScale(0, RoundingMode.UP):balance;
		if (balance.doubleValue() < 1) {
			throw new RuntimeException("user balance can not be added because it is less than rs 1");
		} else {
			String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phoneNo);
			AddMoney addMoney = new AddMoney(AddMoney.defaultRequest, token);
			addMoney.getRequestPojo().setUpdateAmountManager(false);
			addMoney.setCaptureExtent(false);
			addMoney.getRequestPojo().getRequest().withTxnAmount(balance);
			addMoney.createRequestJsonAndExecute();
			CommonValidation.getInstance().basicAsserts(addMoney, APIStatus.AddMoney.SUCCESS);
		}
	}

	public void addBalanceToMainWalletDirectly(BigDecimal balance, String phoneNo) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.MOBILENO, phoneNo);
		params.put(DBEnums.BALANCE, balance);
		dbValidation.runUpdateQuery(DBQueries.addBalanceToMainWallet, params);
	}

	public String fetchWalletId(String PhoneNumber) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.PAYEEPHONENUMBER, PhoneNumber);
		return dbValidation.runDbQuery(DBQueries.wallet_id, params, "log4j").get(0).get("id").toString();
	}

	public String fetchUserId(String PhoneNumber) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.PAYEEPHONENUMBER, PhoneNumber);
		return dbValidation.runDbQuery(DBQueries.user_id, params, "log4j").get(0).get("cust_id").toString();
	}

	public String[] createDynamicQrCode(String businessType, String agentPhoneNumber) {
		String[] qrCodeDetails = new String[2];
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		createDynamicQrCode.setCaptureExtent(false);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType(businessType);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		qrCodeDetails[0] = createDynamicQrCode.getResponsePojo().getResponse().getDynamicQrCodes().get(0)
				.getStrickerId();
		qrCodeDetails[1] = createDynamicQrCode.getResponsePojo().getResponse().getDynamicQrCodes().get(0).getQrCodeId();

		return qrCodeDetails;
	}

	public void mapDynamicQrCode(String[] qrCodeDetails, String phoneNo, String typeOfQrCode, String mappingType,
			String status, String merchantGuid) {
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.setCaptureExtent(false);
		if (mappingType.equals("USER")) {
			mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0])
					.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType(mappingType)
					.withTypeOfQrCode(typeOfQrCode).withStatus(status);
		} else {
			mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0])
					.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType(mappingType)
					.withMerchantGuid(merchantGuid).withTypeOfQrCode(typeOfQrCode).withSourceId("20154")
					.withDeepLink("http://google.com").withStatus(status);
		}
		mapDynamicQrCode.createRequestJsonAndExecute();
	}

	public void sendMoney(String payerMobileNumber, String payeeMobileNumber, BigDecimal amount) {
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, payerMobileNumber);
		SendMoney sendMoney = new SendMoney(SendMoney.defaultRequest, token);
		sendMoney.setCaptureExtent(false);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(payeeMobileNumber).withAmount(amount);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
	}

	public ArrayList<String> createQrCodeHelper(String requestType, String whatToReturn) {
		ArrayList<String> repeatedParameter = new ArrayList<String>();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.setCaptureExtent(false);
		createQrCode.getRequestPojo().getRequest().withRequestType(requestType).withMerchantGuid(merchantGuid)
				.withMid("").withOrderId(String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()))
				.withProductId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();

		if (whatToReturn.equals("orderId")) {
			repeatedParameter.add(createQrCode.getRequestPojo().getRequest().getOrderId());
		} else {
			repeatedParameter.add(createQrCode.getRequestPojo().getRequest().getPosId());
			if (whatToReturn.equals("productId")) {
				repeatedParameter.add(createQrCode.getRequestPojo().getRequest().getProductId());
			}
		}

		return repeatedParameter;
	}

	public CheckUserBalance fetchCheckBalanceResponseALL() {
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		CheckUserBalance checkUserBalance = new CheckUserBalance(CheckUserBalance.defaultRequest, token);
		checkUserBalance.setCaptureExtent(false);
		checkUserBalance.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(checkUserBalance, APIStatus.CheckUserBalance.SUCCESS);
		return checkUserBalance;
	}

	public CheckUserBalance fetchCheckBalanceResponseALL(String mobileNumber) {
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, mobileNumber);
		CheckUserBalance checkUserBalance = new CheckUserBalance(CheckUserBalance.defaultRequest, token);
		checkUserBalance.setCaptureExtent(false);
		checkUserBalance.createRequestJsonAndExecute();
		checkUserBalance.getResponsePojo().getStatus();
		CommonValidation.getInstance().basicAsserts(checkUserBalance, APIStatus.CheckUserBalance.SUCCESS);
		return checkUserBalance;
	}

	public CheckUserBalance fetchCheckBalanceResponseMerchant(String merchantGuid) {
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		CheckUserBalance checkUserBalance = new CheckUserBalance(CheckUserBalance.merchantRequest, token);
		checkUserBalance.setCaptureExtent(false);
		checkUserBalance.getRequestPojo().getRequest().withMerchantGuid(merchantGuid);
		checkUserBalance.createRequestJsonAndExecute();
		checkUserBalance.getResponsePojo().getStatus();
		CommonValidation.getInstance().basicAsserts(checkUserBalance, APIStatus.CheckUserBalance.SUCCESS);
		System.out.println();
		return checkUserBalance;
	}

	public CheckUserBalance fetchCheckBalanceResponseP2P() {
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		CheckUserBalance checkUserBalance = new CheckUserBalance(CheckUserBalance.flaggedRequest, token);
		checkUserBalance.setCaptureExtent(false);
		checkUserBalance.getRequestPojo().getRequest()
				.withPayeePhoneNo(UserManager.getInstance().getPayeeUserDetails(UserHeaders.MOBILENUMBER))
				.withPayeeSsoId(UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID));
		checkUserBalance.createRequestJsonAndExecute();
		checkUserBalance.getResponsePojo().getStatus();
		CommonValidation.getInstance().basicAsserts(checkUserBalance, APIStatus.CheckUserBalance.SUCCESS);
		return checkUserBalance;
	}

	public String generateQrCode(List<String> operationType) {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("USER_QR_CODE")
				.withPhoneNo(agentPhoneNumber).withMappingType("USER");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);

		return generateQrCode.getResponsePojo().getResponse().get(0).getQrCodeId();
	}


	public void cacheRefresh(){
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN,"7111222120");
		CacheRefresh cacheRefresh = new CacheRefresh(CacheRefresh.defaultRequest,token);
		cacheRefresh.setCaptureExtent(false);
		cacheRefresh.createRequestJsonAndExecute();
	}

}
