package com.paytm.helpers;

import com.paytm.assertions.AssertionValidations;
import com.paytm.exceptions.JSONExceptions.JSONParseMappingException;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class CommonHelpers {

	private static volatile CommonHelpers instance;

	private CommonHelpers() {

	}

	public static CommonHelpers getInstance() {
		if (instance == null) {
			synchronized (CommonValidation.class) {
				if (instance == null) {
					instance = new CommonHelpers();
				}
			}
		}
		return instance;
	}

	public int getRandomNumber(int length) {
		Random random = new Random();
		int randomNumber = 0;
		boolean loop = true;
		while (loop) {
			randomNumber = random.nextInt();
			if (Integer.toString(randomNumber).length() == length && !Integer.toString(randomNumber).startsWith("-"))
				loop = false;
		}
		return randomNumber;
	}
	
	public String getCallerClassName(StackTraceElement[] stackTraceArray) 
	{
		
		StackTraceElement e1 = stackTraceArray[2];
    	String classPath = e1.getClassName();
    	String className=classPath.substring(classPath.lastIndexOf(".")+1);
    	
    	return className;
	}

	// This method generates 32 characters strong with "-" included.
	// example:012c1a7e-9900-4747-9bb1-b0d17fac052c
	public String getRandomString() {

		return UUID.randomUUID().toString();
	}

	public Long getCurrentTimeInMilliSec() {
		return Calendar.getInstance().getTimeInMillis();
	}

	public void checkNotNull(Object value) {
		if (null == value) {
			throw new RuntimeException("Null found");
		} else {
		}
	}

	public String getCurrentDateTime(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		String currentDateTime = dateFormat.format(cal.getTime());
		return currentDateTime;
	}

	public BigDecimal getDecimalRoundedOff(BigDecimal totalAmount, int numberOfDigit) {

		return totalAmount.setScale(numberOfDigit, BigDecimal.ROUND_HALF_EVEN);
	}

	public String getFututeDate(String format, int count) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		cal.setTime(cal.getTime());
		cal.add(Calendar.DATE, count);
		String futureDateTime = dateFormat.format(cal.getTime());
		return futureDateTime;
	}

	public String getFutureSecond(String format, int count) {
		String dateTime = getCurrentDateTime(format);
		return getFutureSecondFromDateTime(format, count, dateTime);
	}

	public String getFutureSecondFromDateTime(String format, int count, String dateTime) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		if (dateTime != null) {
			try {
				date = dateFormat.parse(dateTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		cal.setTime(date);
		cal.add(Calendar.SECOND, count);
		String futureDateTime = dateFormat.format(cal.getTime());
		return futureDateTime;
	}

	public String getFutureHour(String format, int count) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		cal.setTime(cal.getTime());
		cal.add(Calendar.HOUR_OF_DAY, count);
		String futureDateTime = dateFormat.format(cal.getTime());
		return futureDateTime;
	}

	public String getDateFormat(String dateValue, String input, String output) {
		DateFormat inputFormat = new SimpleDateFormat(input);
		DateFormat outputFormat = new SimpleDateFormat(output);
		Date endDate = null;
		try {
			endDate = inputFormat.parse(dateValue);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String FormattedDateValue = outputFormat.format(endDate);
		return FormattedDateValue;
	}

	public void comparePOJO(Object obj1, Object obj2) {
		try {
			AssertionValidations.verifyAssertEqual(JacksonJsonImpl.getInstance().toJSon(obj1),
					JacksonJsonImpl.getInstance().toJSon(obj2));
		} catch (IOException e) {
			throw new JSONParseMappingException(e);
		}
	}

	public Boolean checkIfCountersNeedToBeRolledOver(String format, String beforeTime, String afterTime) {
		String curTime = CommonHelpers.getInstance().getDateFormat(afterTime, "yyyy-MM-dd hh:mm:ss", format);
		String updatedTime = "";
		if (beforeTime != null) {
			if (format.equalsIgnoreCase("yyyy")) {
				return checkIfYearlyCountersNeedToBeRolledOver(beforeTime);
			} else {
				if (beforeTime != null) {
					updatedTime = CommonHelpers.getInstance().getDateFormat(beforeTime, "yyyy-MM-dd hh:mm:ss", format);
				}
				if (!updatedTime.equals(curTime)) {
					return true;
				} else {
					return false;
				}
			}
		} else {
			return true;
		}
	}

	private Boolean checkIfYearlyCountersNeedToBeRolledOver(String beforeTime) {
		String updateTime = CommonHelpers.getInstance().getDateFormat(beforeTime, "yyyy-MM-dd hh:mm:ss",
				"yyyy-MM-dd hh:mm:ss");
		DateTime lastTxnTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(updateTime);
		DateTime currentYearStart = new DateTime().withMonthOfYear(4).withDayOfMonth(1).withTimeAtStartOfDay()
				.minusMillis(1);
		int currentMonth = new DateTime().getMonthOfYear();
		if (currentMonth >= 1 && currentMonth < 4) {
			if (lastTxnTime.isBefore(currentYearStart.getMillis())) {
				return false;
			}
		} else {
			if (lastTxnTime.isAfter(currentYearStart.getMillis())) {
				return false;
			}
		}
		return true;
	}

	// Comparing 2 maps and assert if comparision is failure
	public void compareObjectMap(Map<Object, Object> actualMap, Map<Object, Object> expectedMap, String assertMessage)
			throws AssertionError {

		System.out.println("Expected map is " + expectedMap);

		System.out.println("actualMap map is " + actualMap);

		if (actualMap.size() == expectedMap.size()) {
			for (Object ch1 : actualMap.keySet()) {
				Object myListVal = actualMap.get(ch1).toString();
				Object dbMapVal = expectedMap.get(ch1).toString();
				if (!(myListVal.equals(dbMapVal))) {
					AssertionValidations.verifyAssertFail(assertMessage);
				}
			}
		} else {
			AssertionValidations.verifyAssertFail(assertMessage);
		}
	}
}
