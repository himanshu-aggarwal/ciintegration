package com.paytm.helpers;

import com.paytm.datamanager.UserManager;
import com.paytm.enums.UserHeaders;
import com.paytm.utils.Redis;

import redis.clients.jedis.Jedis;

public class RedisHelper {

	public void insertBlockedRedisKey() {
		Redis redis = new Redis();
		Jedis jedis = redis.createWalletRedisConnection();
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String key = "TIP-" + custId;
		redis.setRedisKey(jedis, key, "1");
		System.out.println("redis key is " + redis.getRedisKey(jedis,key));
		
	}

	public void deleteBlockedRedisKey() {
		Redis redis = new Redis();
		Jedis jedis = redis.createWalletRedisConnection();
		redis.deleteRedisKey(jedis,"*TIP*");
	}

}
