package com.paytm.helpers;

import com.paytm.apiPojo.wallet.qrCode.CatalogForUTSResponse.GridLayout;
import com.paytm.apiPojo.wallet.qrCode.CatalogForUTSResponse.GridLayout.Attributes;
import com.paytm.apiPojo.wallet.qrCode.CreateDynamicQrCodeResponse.Response.DynamicQrCodes;
import com.paytm.apiPojo.wallet.qrCode.GenerateQrCodeResponse.Response;
import com.paytm.apiRequestBuilders.wallet.qrCode.*;
import com.paytm.apiRequestBuilders.wallet.qrCode.GetMerchantInfo;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.exceptions.QRCode.CatalogExcpetions.NoDataFound;
import com.paytm.utils.DBValidation;
import org.assertj.core.api.Assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QrCodeHelpers {
	private static QrCodeHelpers instance;
	private static String qrCodeId;
	private static String unMappedQrCode;
	private static String inactiveQRCodeId;
	private static String dateFormat = "yyyy-MM-dd";
	private static String dateFormateWithTime = "yyyy-MM-dd HH:mm:ss";

	public static QrCodeHelpers getInstance() {
		if (instance == null) {
			instance = new QrCodeHelpers();
		}
		return instance;
	}

	public static void setQrCodeId(String qrCodeId) {
		QrCodeHelpers.qrCodeId = qrCodeId;
	}

	public static void setUnMappedQrCode(String unMappedQrCode) {
		QrCodeHelpers.unMappedQrCode = unMappedQrCode;
	}

	public static void setInactiveQRCodeId(String inactiveQRCodeId) {
		QrCodeHelpers.inactiveQRCodeId = inactiveQRCodeId;
	}

	/* Get Existing QRCode id based on requirement from DB */
	public synchronized String getQrIdFromId(String mappingStatus, String status) {
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.MAPPINGSTATUS, mappingStatus);
		DBValidation dbValidation = new DBValidation();
		List<Map<String, Object>> qrList = new ArrayList<>();
		if (mappingStatus.equals("1")) {
			if (status.equals("1")) {
				if (null == qrCodeId) {
					params.put(DBEnums.QRSTATUS, status);
					qrList = dbValidation.runDbQuery(DBQueries.fetchQrCode_getQrIdMappingStatus, params, "log4j");
					if (qrList.size() != 0) {
						System.out.println(qrList.get(0).get("qr_code_id"));
						qrCodeId = (String) qrList.get(0).get("qr_code_id");
					} else {
						qrCodeId = QrCodeHelpers.getInstance().noQrCodeInDatabase(mappingStatus, status);
					}
				}
				return qrCodeId;
			} else {
				if (null == inactiveQRCodeId) {
					params.put(DBEnums.QRSTATUS, status);
					qrList = dbValidation.runDbQuery(DBQueries.fetchQrCode_getQrIdMappingStatus, params, "log4j");
					if (qrList.size() != 0) {
						System.out.println(qrList.get(0).get("qr_code_id"));
						inactiveQRCodeId = (String) qrList.get(0).get("qr_code_id");
					} else {
						inactiveQRCodeId = QrCodeHelpers.getInstance().noQrCodeInDatabase(mappingStatus, status);
					}
				}
				return inactiveQRCodeId;
			}
		} else {
			if (null == unMappedQrCode) {
				qrList = dbValidation.runDbQuery(DBQueries.fetchQrCode_getQrIdMappingStatusZero, params, "log4j");
				if (qrList.size() != 0) {
					System.out.println(qrList.get(0).get("qr_code_id"));
					unMappedQrCode = (String) qrList.get(0).get("qr_code_id");
				} else {
					unMappedQrCode = QrCodeHelpers.getInstance().noQrCodeInDatabase(mappingStatus, status);
				}
			}
			return unMappedQrCode;
		}
	}

	/* Used in CreateDynamicQRCode Test cases for Response validation */
	public void dynamicQrCodesParameterValidations(CreateDynamicQrCode createDynamicQrCode, String expectedQRCode) {
		DBValidation dbValidation = new DBValidation();
		List<DynamicQrCodes> codesList = createDynamicQrCode.getResponsePojo().getResponse().getDynamicQrCodes();
		checkdynamicQrCodeListSize(createDynamicQrCode);
		checkdynamicQrCodeParameters(codesList);
		checkCreatedQrCodeId(codesList, expectedQRCode);
		createQrCodeDbValidation(codesList, dbValidation);
	}

	/*
	 * Used in CreateQR Code Test Cases for Response validation This function
	 * contains all the validations
	 */
	public void createQrCodeParameterValidation(CreateQrCode createQrCode, String merchantId, String currDate) {
		Boolean imageRequired = createQrCode.getRequestPojo().getRequest().getImageRequired();
		if (imageRequired != null && !imageRequired) {
			AssertionValidations.verifyAssertNull(createQrCode.getResponsePojo().getResponse().getPath());
		} else {
			AssertionValidations.verifyAssertNotNull(createQrCode.getResponsePojo().getResponse().getPath());
		}
		AssertionValidations.verifyAssertNotNull(createQrCode.getResponsePojo().getResponse().getEncryptedData());
		AssertionValidations.verifyAssertNotNull(createQrCode.getResponsePojo().getResponse().getQrData());

		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		String requestType = createQrCode.getRequestPojo().getRequest().getRequestType();
		params.put(DBEnums.QRCODEID, createQrCode.getResponsePojo().getResponse().getEncryptedData());
		params.put(DBEnums.MAPPINGID, merchantId);
		params.put(DBEnums.LABEL, createQrCode.getRequestPojo().getRequest().getRequestType());
		if (requestType.equals("QR_MERCHANT")) {
			params.put(DBEnums.POSID, createQrCode.getRequestPojo().getRequest().getPosId());
			dbValidation.isEntryPresent(DBQueries.createQrCodeWithPosId, params);
		} else if (requestType.equals("QR_PRODUCT") || requestType.equals("QR_PRICEDPRODUCT")) {
			params.put(DBEnums.QRORDERID, createQrCode.getRequestPojo().getRequest().getProductId());
			if (currDate == null) {
				dbValidation.isEntryPresent(DBQueries.qrProductWithNullExpiry, params);
			} else {
				params.put(DBEnums.EXPIRYDATE, currDate);
				dbValidation.isEntryPresent(DBQueries.qrProductWithExpiryDate, params);
			}
		} else {
			params.put(DBEnums.QRORDERID, createQrCode.getRequestPojo().getRequest().getOrderId());
			params.put(DBEnums.EXPIRYDATE, currDate);
			dbValidation.isEntryPresent(DBQueries.createQrCodeValidationDbQuery, params);
		}
	}

	/*
	 * Used in GenerateQR Code Test Cases for Response validation This function
	 * contains all the validations
	 */
	public void generateQrCodeParameterValidation(GenerateQrCode generateQrCode, String expectedQrCode,
			Integer batchSize, Integer batchCount, List<String> operationType, String phoneNo) {
		DBValidation dbValidation = new DBValidation();
		List<Response> generateQrList = generateQrCode.getResponsePojo().getResponse();
		String qrCodeId = generateQrCode.getRequestPojo().getRequest().getMapRequest().getQrCodeId();
		if (!(operationType.size() == 1 && operationType.contains("MAP"))) {
			qrCodeId = generateQrList.get(0).getQrCodeId();
			if (batchSize != null && batchCount != null) {
				AssertionValidations.verifyAssertEqual(generateQrList.size(), batchCount * batchSize);
			}

			for (int i = 0; i < generateQrList.size(); i++) {
				AssertionValidations.verifyAssertNotNull(generateQrList.get(i).getStickerId());
				AssertionValidations.verifyAssertNotNull(generateQrList.get(i).getQrCodeId());
				if (!operationType.contains("MAP")) {
					Assertions.assertThat(generateQrList.get(i).getMapResponse()).isNull();
				}
			}

			for (int i = 0; i < generateQrList.size(); i++) {
				AssertionValidations.verifyAssertEqual(generateQrList.get(i).getQrCodeId().substring(6, 8),
						expectedQrCode);
			}
		}

		if (operationType.contains("MAP")) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.QRCODEID, qrCodeId);
			params.put(DBEnums.MAPPINGSTATUS, "1");
			dbValidation.isEntryPresent(DBQueries.dynamicQrCodeMappingDetails, params);
			params.put(DBEnums.MAPPINGID, generateQrList.get(0).getMapResponse().getMappingId());
			params.put(DBEnums.MAPPINGTYPE, generateQrList.get(0).getMapResponse().getMappingType());
			params.put(DBEnums.DISPLAYNAME, generateQrList.get(0).getMapResponse().getDisplayName());
			params.put(DBEnums.QRSTATUS, generateQrList.get(0).getMapResponse().getStatus());
			params.put(DBEnums.TYPEOFQRCODE, generateQrList.get(0).getMapResponse().getTypeOfQrCode());
			String secondaryPhoneNumber = generateQrList.get(0).getMapResponse().getSecondaryPhoneNumber();
			if (secondaryPhoneNumber == null) {
				dbValidation.isEntryPresent(DBQueries.mapDynamicQrCode, params);
			} else {
				params.put(DBEnums.PAYEESECONDARYNUMBER, secondaryPhoneNumber);
				dbValidation.isEntryPresent(DBQueries.mapDynamicQrCodeWithSecondaryPhoneNumber, params);
			}
			if (phoneNo != null) {
				params.put(DBEnums.MOBILENO, phoneNo);
				dbValidation.isEntryPresent(DBQueries.flaggedMerchant, params);
				dbValidation.runUpdateQuery(DBQueries.updateEntryInFlaggedMerchant, params);
				revertingFlaggedToNormalUser();
			}
		} else {
			for (int i = 0; i < generateQrList.size(); i++) {
				Map<Object, Object> params = new HashMap<>();
				params.put(DBEnums.STICKERID, generateQrList.get(i).getStickerId());
				dbValidation.isEntryPresent(DBQueries.dynamicQrCodeMappingDetailsMappingStatusZero, params);
			}
		}
	}

	/* Validation of QRCodelist size w.r.t BatchCount * BatchSize from request */
	private void checkdynamicQrCodeListSize(CreateDynamicQrCode createDynamicQrCode) {
		List<DynamicQrCodes> codesList = createDynamicQrCode.getResponsePojo().getResponse().getDynamicQrCodes();
		int batchSize = createDynamicQrCode.getRequestPojo().getRequest().getBatchSize();
		int batchCount = createDynamicQrCode.getRequestPojo().getRequest().getBatchCount();
		AssertionValidations.verifyAssertEqual(codesList.size(), batchCount * batchSize);
	}

	/* StickerId and QRCode shouldn't be null in Response */
	private void checkdynamicQrCodeParameters(List<DynamicQrCodes> codesList) {
		for (int i = 0; i < codesList.size(); i++) {
			AssertionValidations.verifyAssertNotNull(codesList.get(i).getStrickerId());
			AssertionValidations.verifyAssertNotNull(codesList.get(i).getQrCodeId());
		}
	}

	/* Validation for type of QRCode generated like merchant or user */
	private void checkCreatedQrCodeId(List<DynamicQrCodes> codesList, String expectedQRCode) {
		for (int i = 0; i < codesList.size(); i++) {
			AssertionValidations.verifyAssertEqual(codesList.get(i).getQrCodeId().substring(6, 8), expectedQRCode);
		}
	}

	/* Validation of QRCodes generated w.r.t. DB */
	private void createQrCodeDbValidation(List<DynamicQrCodes> codesList, DBValidation dbValidation) {
		for (int i = 0; i < codesList.size(); i++) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.STICKERID, codesList.get(i).getStrickerId());
			dbValidation.isEntryPresent(DBQueries.dynamicQrCodeMappingDetailsMappingStatusZero, params);
			dbValidation.isEntryPresent(DBQueries.dynamicQrCodeBatchDetails, params);
		}
	}

	/*
	 * Used in MapDynamic Code Test Cases for Response validation This function
	 * contains all the validations
	 */
	public void mapDynamicQrCodeParameterValidation(MapDynamicQrCode mapDynamicQrCode, String phoneNo,
			String secondaryPhoneNo) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		if (mapDynamicQrCode.getResponsePojo().getStatusMessage().equals("Entered phoneNo is not valid at Oauth end")) {
			params.put(DBEnums.MOBILENO, phoneNo);
			dbValidation.isEntryPresent(DBQueries.walletCommunicationVeryfication, params);
		} else {
			params.put(DBEnums.QRCODEID, mapDynamicQrCode.getResponsePojo().getResponse().getQrCodeId());
			params.put(DBEnums.MAPPINGSTATUS, "1");
			dbValidation.isEntryPresent(DBQueries.dynamicQrCodeMappingDetails, params);
			params.put(DBEnums.MAPPINGID, mapDynamicQrCode.getResponsePojo().getResponse().getMappingId());
			params.put(DBEnums.MAPPINGTYPE, mapDynamicQrCode.getResponsePojo().getResponse().getMappingType());
			params.put(DBEnums.DISPLAYNAME, mapDynamicQrCode.getResponsePojo().getResponse().getDisplayName());
			params.put(DBEnums.QRSTATUS, mapDynamicQrCode.getResponsePojo().getResponse().getStatus());
			params.put(DBEnums.TYPEOFQRCODE, mapDynamicQrCode.getResponsePojo().getResponse().getTypeOfQrCode());
			if (secondaryPhoneNo == null) {
				dbValidation.isEntryPresent(DBQueries.mapDynamicQrCode, params);
			} else {
				params.put(DBEnums.PAYEESECONDARYNUMBER, secondaryPhoneNo);
				dbValidation.isEntryPresent(DBQueries.mapDynamicQrCodeWithSecondaryPhoneNumber, params);
			}
			if (phoneNo != null) {
				params.put(DBEnums.MOBILENO, phoneNo);
				dbValidation.isEntryPresent(DBQueries.flaggedMerchant, params);
				dbValidation.runUpdateQuery(DBQueries.updateEntryInFlaggedMerchant, params);
				revertingFlaggedToNormalUser();
			}
		}
	}

	/*
	 * Used in EditQrCodeDetails Code Test Cases for Response validation This
	 * function contains all the validations
	 */
	public void editQrCodeDetailsValidation(EditQrCodeDetails editQrCodeDetails, String toValidate, String typeOfQrCode,
			String phoneNo) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.QRCODEID, editQrCodeDetails.getResponsePojo().getResponse().getQrCodeId());
		if (toValidate.equals("display_name")) {
			if (typeOfQrCode.equals("DYNAMIC_QR_CODE")) {
				params.put(DBEnums.MOBILENO, phoneNo);
				dbValidation.runUpdateQuery(DBQueries.updateEntryInFlaggedMerchant, params);
				revertingFlaggedToNormalUser();
			}
			AssertionValidations.verifyAssertEqual(
					editQrCodeDetails.getRequestPojo().getRequest().getDisplayName().toString(),
					dbValidation.runDbQuery(DBQueries.dynamicQrCodeTable, params, "log4j").get(0)
							.get("display_name").toString());
		} else if (toValidate.equals("status")) {
			AssertionValidations.verifyAssertEqual(
					editQrCodeDetails.getRequestPojo().getRequest().getActivationStatus().toString(), dbValidation
							.runDbQuery(DBQueries.dynamicQrCodeTable, params, "log4j").get(0).get("status").toString());
		}
	}

	/*
	 * Used in FetchQrCodeDetails Code Test Cases for Response validation This
	 * function contains all the validations
	 */
	public void fetchQrCodeDetailsValidation(FetchQrCodeDetails fetchQrCodeDetails) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.STICKERID, fetchQrCodeDetails.getResponsePojo().getResponse().get(0).getStickerId());
		params.put(DBEnums.QRCODEID, fetchQrCodeDetails.getResponsePojo().getResponse().get(0).getQrCodeId());
		dbValidation.isEntryPresent(DBQueries.fetchQrCode_qrCodeMappingDetails, params);
	}

	/*
	 * Used in GetQrCodeInfo Code Test Cases for Response validation This function
	 * contains all the validations
	 */
	public void getQrCodeInfoValidation(GetQrCodeInfo getQrCodeInfo) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.QRCODEID, getQrCodeInfo.getResponsePojo().getResponse().getQrCodeId());
		String qrId = getQrCodeInfo.getResponsePojo().getResponse().getQrCodeId();
		if (qrId.substring(6, 8).equals("01")) {
			params.put(DBEnums.MAPPINGTYPE, "3");
			params.put(DBEnums.MAPPINGID, getQrCodeInfo.getResponsePojo().getResponse().getMappingId());
			params.put(DBEnums.SSOID, getQrCodeInfo.getResponsePojo().getResponse().getMappingId());
			AssertionValidations.verifyAssertEqual(
					dbValidation.runDbQuery(DBQueries.userTable, params, "log4j").get(0).get("phone_no").toString(),
					getQrCodeInfo.getResponsePojo().getResponse().getMOBILENO().toString());
		} else {
			params.put(DBEnums.MAPPINGTYPE, "2");
			params.put(DBEnums.MERCHANTNAME, getQrCodeInfo.getResponsePojo().getResponse().getMappingId());
			params.put(DBEnums.MAPPINGID,
					dbValidation.runDbQuery(DBQueries.merchantTable, params, "log4j").get(0).get("id"));
			if (getQrCodeInfo.getResponsePojo().getResponse().getREQUESTTYPE().equals("QR_UTS")) {
				AssertionValidations.verifyAssertEqual(
						dbValidation.runDbQuery(DBQueries.merchantTable, params, "log4j").get(0).get("status")
								.toString(),
						getQrCodeInfo.getResponsePojo().getResponse().getUts().getMerchantStatus().toString());
			} else {
				AssertionValidations
						.verifyAssertEqual(
								dbValidation.runDbQuery(DBQueries.merchantTable, params, "log4j").get(0)
										.get("contact_phone_no").toString(),
								getQrCodeInfo.getResponsePojo().getResponse().getMOBILENO().toString());
				AssertionValidations.verifyAssertEqual(
						dbValidation.runDbQuery(DBQueries.merchantTable, params, "log4j").get(0).get("status")
								.toString(),
						getQrCodeInfo.getResponsePojo().getResponse().getMERCHANTSTATUS().toString());
			}
		}
		dbValidation.isEntryPresent(DBQueries.getQrCodeInfoValidation, params);
	}

	/* Used to validate the response parameters in assignQrCode */
	public void assignQrCodeParameterValidation(AssignQrCode assignQrCode) {
		AssertionValidations.verifyAssertNotNull(assignQrCode.getResponsePojo().getResponse().getStickerId());
		AssertionValidations.verifyAssertNotNull(assignQrCode.getResponsePojo().getResponse().getQrCodeId());
		AssertionValidations.verifyAssertNotNull(assignQrCode.getResponsePojo().getResponse().getPath());
		AssertionValidations.verifyAssertEqual(assignQrCode.getResponsePojo().getResponse().getMobileNo(),
				assignQrCode.getRequestPojo().getRequest().getPhoneNo());
	}

	/* validation of user table and dynamicQrCode table after assignQrCode */
	public void assignQrCodeDBValidation(String stickerId) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		String phoneNo = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		params.put(DBEnums.MOBILENO, phoneNo);
		dbValidation.isEntryPresent(DBQueries.verifyUserTable, params);
		params.put(DBEnums.STICKERID, stickerId);
		dbValidation.isEntryPresent(DBQueries.assignQrVerification, params);
	}

	/* reverting the user from flagged_merchant to normal */
	public void revertingFlaggedToNormalUser() {
		DBValidation dbValidation = new DBValidation();
		String phoneNo = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.MOBILENO, phoneNo);
		dbValidation.runUpdateQuery(DBQueries.UpdateUserTable, params);
	}

	/* update the value of custId column of dynamic_qr_code table */
	public void updateMappingIdInDynamicQrCode(String qrCodeId) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.QRCODEID, qrCodeId);
		dbValidation.runUpdateQuery(DBQueries.UpdateAssignQrCode, params);
	}

	/* update of value qr_code_id column of dynamic_qr_code_mapping_deatils */
	public void DynamicQrMappingUpdate(String qrCodeId) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.STICKERID, qrCodeId);
		params.put(DBEnums.QRCODEID, String.valueOf(CommonHelpers.getInstance().getCurrentTimeInMilliSec()));
		dbValidation.runUpdateQuery(DBQueries.UpdateDynamicQrMapping, params);
	}

	public void setDynamicQRCodeToNull(String phoneNumber) {
		String custId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, phoneNumber);
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.CUSTID, custId);
		dbValidation.runUpdateQuery(DBQueries.updateDynamicQRCodeTONULL, params);
	}

	/* Returning date/dateTime for some future date */
	public String getDate(String dateToReturn, int daysToAdd) {
		if (dateToReturn.equals("withTime")) {
			return CommonHelpers.getInstance().getFututeDate(dateFormateWithTime, daysToAdd);
		} else {
			return CommonHelpers.getInstance().getFututeDate(dateFormat, daysToAdd);
		}
	}

	/* Used to validate the response parameters in getMerchantInfo */
	public void getMetchantInfoParameterValidation(GetMerchantInfo getMerchantInfo) {
		AssertionValidations.verifyAssertNotNull(getMerchantInfo.getResponsePojo().getResponse().getPath());
		AssertionValidations.verifyAssertNotNull(getMerchantInfo.getResponsePojo().getResponse().getEncryptedData());
	}

	/* verfication of GetMerchantInfo */
	public void getMerchantInfoValidation(String encryptedData, String Mid) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.QRCODEID, encryptedData);
		params.put(DBEnums.MERCHANTNAME, Mid);
		dbValidation.isEntryPresent(DBQueries.getMerchantInfoVerification, params);
	}

	/* Returning qrCodeId if no QrCode is found in database */
	public String noQrCodeInDatabase(String mappingStatus, String qrStatus) {
		String[] qrCodeDetails = new String[2];
		String phoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET);
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		if (mappingStatus.equals("1")) {
			if (qrStatus.equals("0")) {
				walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", qrStatus,
						MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
								MerchantHeaders.MERCHANTGUID));
			} else if (qrStatus.equals("1")) {
				walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", qrStatus,
						MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
								MerchantHeaders.MERCHANTGUID));
			}
		}

		return qrCodeDetails[1];
	}

	/* function for fetching fare details between source and destination */
	public Map<String, Integer> fetchFareDetailsFromCatalog(Attributes attributes) {
		Map<String, String> params = new HashMap<>();
		params.put("attributes", "all");
//		params.put("source_id", getUTSFareInfo.getRequestPojo().getRequest().getSourceId());
//		params.put("destination_id", getUTSFareInfo.getRequestPojo().getRequest().getDestinationId());
//		params.put("route_id", getUTSFareInfo.getRequestPojo().getRequest().getRouteId());
//		params.put("bus_type", getUTSFareInfo.getRequestPojo().getRequest().getBusType());

		params.put("source_id", attributes.getSourceId());
		params.put("destination_id", attributes.getDestinationId());
		params.put("route_id", attributes.getRouteId());
		params.put("bus_type", attributes.getBusType());

		CatalogForUTS catalogForUTS = new CatalogForUTS(params);
		catalogForUTS.createRequestJsonAndExecute();

		List<GridLayout> availableRouteList = catalogForUTS.getResponsePojo().getGridLayout();

		Map<String, Integer> fareMap = new HashMap<>(); 
		for (GridLayout gridLayout : availableRouteList) {
			fareMap.put(gridLayout.getAttributes().getPassengerType(), gridLayout.getActualPrice());
		}

		return fareMap;
	}

	/*
	 * helper function for fetching all available route from catalog used for UTR
	 * QrCode
	 */
	public List<GridLayout> getAllAvailableRouteFromCatalogForUTS() {
		List<GridLayout> availableRouteList = null;
		Map<String, String> params = new HashMap<>();
		params.put("attributes", "all");

		CatalogForUTS catalogForUTS = new CatalogForUTS(params);
		catalogForUTS.createRequestJsonAndExecute();
		availableRouteList = catalogForUTS.getResponsePojo().getGridLayout();
		if(availableRouteList==null) {
			throw new NoDataFound("Data from Catalog is Null");
		}else {
		return availableRouteList;
		}
	}

	/*
	 * function for getting detail between source and destination from catalog for
	 * UTS QrCode
	 */
	public Attributes getAttributeObjectFromCatalog() {
		Attributes attributes = null;

		List<GridLayout> availableRouteList = getAllAvailableRouteFromCatalogForUTS();

		if (availableRouteList != null) {
			attributes = availableRouteList.get(0).getAttributes();
		}

		return attributes;
	}

	/*
	 * Used in MerchantQrCode Code Test Cases for Response validation This function
	 * contains all the validations
	 */

	public void merchantQrCodeValidation(MerchantQrCode merchantQrCode, String merchantId) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.QRCODEID, merchantQrCode.getResponsePojo().getResponse().getQrCodeId());
		params.put(DBEnums.MAPPINGID, merchantId);
		params.put(DBEnums.MAPPINGTYPE, "2");
		params.put(DBEnums.DISPLAYNAME, merchantQrCode.getResponsePojo().getResponse().getDisplayName());
		params.put(DBEnums.QRSTATUS, "1");
		params.put(DBEnums.TYPEOFQRCODE, "QR_MERCHANT");
		dbValidation.isEntryPresent(DBQueries.mapDynamicQrCode, params);
		if (merchantId.equals(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDMERCHANTWITHNOQRCODE,
				MerchantHeaders.ID))) {
			dbValidation.runUpdateQuery(DBQueries.UpdateAssignQrCode, params);
		}
	}

	/*
	 * Used in FetchQrMappingDetail Code Test Cases for Response validation This
	 * function contains all the validations
	 */
	public void fetchQrMappingDetailValidation(FetchQrMappingDetail fetchQrMappingDetail) {

		/* check whether sticketId and qrCodeId is not null */
		AssertionValidations.verifyAssertNotNull(fetchQrMappingDetail.getResponsePojo().getResponse().getStickerId());
		AssertionValidations.verifyAssertNotNull(fetchQrMappingDetail.getResponsePojo().getResponse().getQrCodeId());

		/* DB validation */
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.STICKERID, fetchQrMappingDetail.getResponsePojo().getResponse().getStickerId());
		params.put(DBEnums.QRCODEID, fetchQrMappingDetail.getResponsePojo().getResponse().getQrCodeId());
		params.put(DBEnums.MAPPINGSTATUS, fetchQrMappingDetail.getResponsePojo().getResponse().getMappingStatus());
		if (fetchQrMappingDetail.getResponsePojo().getResponse().getBatchId() != null) {
			params.put(DBEnums.BATCHID, fetchQrMappingDetail.getResponsePojo().getResponse().getBatchId());
			dbValidation.isEntryPresent(DBQueries.fetchQrMappingDetailDBVerification, params);
		} else {
			dbValidation.isEntryPresent(DBQueries.fetchQrMappingDetailWithBatchIdNull, params);
		}
	}

	/*
	 * Used in MapDee Code Test Cases for Response validation This function contains
	 * all the validations
	 */
	public void mapDeeplinkToQrValidation(MapDeeplinkToQr mapDeeplinkToQr, boolean withAdditionalParam) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.QRCODEID, mapDeeplinkToQr.getResponsePojo().getResponse().getQrCodeId());
		params.put(DBEnums.MAPPINGID, mapDeeplinkToQr.getResponsePojo().getResponse().getMappingId());
		params.put(DBEnums.MAPPINGTYPE, mapDeeplinkToQr.getResponsePojo().getResponse().getMappingType());
		params.put(DBEnums.QRSTATUS, mapDeeplinkToQr.getResponsePojo().getResponse().getStatus());
		params.put(DBEnums.DISPLAYNAME, mapDeeplinkToQr.getResponsePojo().getResponse().getDisplayName());
		params.put(DBEnums.METADATA, mapDeeplinkToQr.getResponsePojo().getResponse().getMetadata());
		params.put(DBEnums.TYPEOFQRCODE, mapDeeplinkToQr.getResponsePojo().getResponse().getTypeOfQrCode());
		String secondaryPhoneNumber = mapDeeplinkToQr.getResponsePojo().getResponse().getSecondaryPhoneNumber();
		if (secondaryPhoneNumber == null) {
			if (withAdditionalParam) {
				params.put(DBEnums.POSID, mapDeeplinkToQr.getResponsePojo().getResponse().getPosId());
				params.put(DBEnums.TAGLINE, mapDeeplinkToQr.getResponsePojo().getResponse().getTagLine());
				dbValidation.isEntryPresent(DBQueries.dynamicQrCodeValidationWithAdditionalParameter, params);
			} else {
				dbValidation.isEntryPresent(DBQueries.dynamicQrCodeValidation, params);
			}
		} else {
			params.put(DBEnums.POSID, mapDeeplinkToQr.getResponsePojo().getResponse().getPosId());
			params.put(DBEnums.TAGLINE, mapDeeplinkToQr.getResponsePojo().getResponse().getTagLine());
			params.put(DBEnums.PAYEESECONDARYNUMBER, secondaryPhoneNumber);
			dbValidation.isEntryPresent(DBQueries.dynamicQrCodeValidationWithSecondaryPhoneNumber, params);
		}
	}
}
