package com.paytm.helpers;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.enums.DBEnums;
import com.paytm.enums.ValidationInterface;
import com.paytm.exceptions.DBExceptions.DBEntryNSTR.NSTRAmountMismatch;
import com.paytm.exceptions.DBExceptions.DBEntryNSTR.NSTREntryMissing;
import com.paytm.exceptions.DBExceptions.DBEntryNSTR.NSTRMultiPPiMismatch;
import com.paytm.utils.DBValidation;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonValidation {

    private static volatile CommonValidation instance;

    private CommonValidation(){

    }

    public static CommonValidation getInstance(){
        if(instance == null) {
            synchronized (CommonValidation.class){
                if(instance==null){
                    instance = new CommonValidation();
                }
            }
        }
        return instance;
    }


    public void basicAsserts(APIInterface obj,ValidationInterface statusDetails) {
    		AssertionValidations.verifyAssertEqual(obj.getApiResponse().getStatusCode(), statusDetails.gethttpResponseCode());
    		AssertionValidations.verifyAssertEqual(obj.getResponsePojo().getStatus(),statusDetails.getStatus());
    		AssertionValidations.verifyAssertEqual(obj.getResponsePojo().getStatusCode(),statusDetails.getStatusCode());
    		AssertionValidations.verifyAssertEqual(obj.getResponsePojo().getStatusMessage(),statusDetails.getStatusMessage());
    }

    public void httpCodeAssert(APIInterface obj,ValidationInterface statusDetails) {
    	AssertionValidations.verifyAssertEqual(obj.getApiResponse().getStatusCode(),statusDetails.gethttpResponseCode());
    }
    
    /**
     *
     * Validation of nstr entry based on 4 parameters merchantOrderId, txnAmount, txnStatus, multippi flag
     *
     * @param merchantOrderId - Order Id of txn
     * @param txnStatus - Txn Status (i.e. 1 or ..)
     * @param multiPPIEnable - Multippi flag (1 or 0)
     */
    public void nstrTxnValidation(String merchantOrderId, int txnStatus,  int multiPPIEnable){
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.MERCHANTORDERID, merchantOrderId);
        params.put(DBEnums.TXNAMOUNT, FundThreadManager.getInstance().getTotalAmount());
        params.put(DBEnums.TXNSTATUS,txnStatus);
        params.put(DBEnums.MULTIPPIENABLE,multiPPIEnable);
        DBValidation dbValidation = new DBValidation();
        try{
        dbValidation.isEntryPresent(DBQueries.NSTR.nstrTxnValidate,params);
        }catch(AssertionError ae){
            List<Map<String, Object>> nstrData = dbValidation.runDbQuery(DBQueries.NSTR.fetchNstrDetails,params,"log4j");
            if(nstrData.size()!=0){
                if(new BigDecimal(nstrData.get(0).get("txn_amount").toString()).compareTo(FundThreadManager.getInstance().getTotalAmount())!=0){
                    throw new NSTRAmountMismatch("Amount found in nstr and expected amount are different. Expected value is: "+FundThreadManager.getInstance().getTotalAmount()+", and actual value is: "+nstrData.get(0).get("txn_amount").toString());
                }
                if((Integer)nstrData.get(0).get("txn_status")!=txnStatus){
                    throw new NSTRMultiPPiMismatch("TxnStatus found in nstr and expected amount are different. Expected value is: "+txnStatus+", and actual value is: "+nstrData.get(0).get("txn_status"));
                }
                if(!nstrData.get(0).get("multi_ppi_txn").toString().equalsIgnoreCase(multiPPIEnable==0?"false":"true")){
                    throw new NSTRMultiPPiMismatch("MultiPPI flag found in nstr and expected amount are different. Expected value is: "+(multiPPIEnable==0?"false":"true")+", and actual value is: "+nstrData.get(0).get("multi_ppi_txn").toString());
                }
                throw ae;
            }else{
                throw new NSTREntryMissing("Entry missing in nstr for merchantOrderId: "+merchantOrderId);
            }
        }
    }

    /**
     *
     * Validation of nstr entry based on 4 parameters merchantOrderId, txnAmount, txnStatus, multippi flag and parent merchant Order id
     *
     * @param merchantOrderId - Order Id of txn
     * @param txnStatus - Txn Status (i.e. 1 or ..)
     * @param multiPPIEnable - Multippi flag (1 or 0)
     * @param parentMerchantOrderId - Merchant order id of parent txn
     */

    public void nstrTxnValidationRefund(String merchantOrderId, int txnStatus,  int multiPPIEnable, String parentMerchantOrderId){
        Map<Object, Object> params = new HashMap<>();
        params.put(DBEnums.MERCHANTORDERID, merchantOrderId);
        params.put(DBEnums.PARENTMERCHANTORDERID, parentMerchantOrderId);
        params.put(DBEnums.TXNAMOUNT, FundThreadManager.getInstance().getTotalAmount());
        params.put(DBEnums.TXNSTATUS,txnStatus);
        params.put(DBEnums.MULTIPPIENABLE,multiPPIEnable);
        DBValidation dbValidation = new DBValidation();
        try {
            dbValidation.isEntryPresent(DBQueries.NSTR.refundNstrTxnValidation, params);
        }catch(AssertionError ae){
            List<Map<String, Object>> nstrData = dbValidation.runDbQuery(DBQueries.NSTR.fetchNstrDetails,params,"log4j");
            if(nstrData.size()!=0){
                if(new BigDecimal(nstrData.get(0).get("txn_amount").toString()).compareTo(FundThreadManager.getInstance().getTotalAmount())!=0){
                    throw new NSTRAmountMismatch("Amount found in nstr and expected amount are different. Expected value is: "+FundThreadManager.getInstance().getTotalAmount()+", and actual value is: "+nstrData.get(0).get("txn_amount").toString());
                }
                if((Integer)nstrData.get(0).get("txn_status")!=txnStatus){
                    throw new NSTRMultiPPiMismatch("TxnStatus found in nstr and expected amount are different. Expected value is: "+txnStatus+", and actual value is: "+nstrData.get(0).get("txn_status"));
                }
                if(!nstrData.get(0).get("multi_ppi_txn").toString().equalsIgnoreCase(multiPPIEnable==0?"false":"true")){
                    throw new NSTRMultiPPiMismatch("MultiPPI flag found in nstr and expected amount are different. Expected value is: "+(multiPPIEnable==0?"false":"true")+", and actual value is: "+nstrData.get(0).get("multi_ppi_txn").toString());
                }
                throw ae;
            }else{
                throw new NSTREntryMissing("Entry missing in nstr for merchantOrderId: "+merchantOrderId);
            }
        }
    }

}
