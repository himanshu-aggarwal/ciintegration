package com.paytm.helpers;

import com.paytm.apiPojo.wallet.oldService.CheckUserBalanceResponse.Response.SubWalletDetailsList;
import com.paytm.apiRequestBuilders.wallet.oldService.CheckUserBalance;
import com.paytm.apiRequestBuilders.wallet.oldService.FundCheckBalance;
import com.paytm.apiRequestBuilders.wallet.walletWeb.CheckBalance;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.Constants;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.DBEnums;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserHeaders;
import com.paytm.framework.utils.DatabaseUtil;
import com.paytm.utils.DBValidation;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckUserBalanceHelpers {

	public Map<Object, Object> serverResponseSubWalletDetailsMap(String aggregate, CheckUserBalance checkUserBalance) {
		Map<Object, Object> myList = new HashMap<Object, Object>();
		List<SubWalletDetailsList> array = new ArrayList<>();
		if (checkUserBalance.getResponsePojo().getResponse().getSubWalletDetailsList() != null)
			array = checkUserBalance.getResponsePojo().getResponse().getSubWalletDetailsList();
		DecimalFormat dc = new DecimalFormat("###0.00");
		if (!aggregate.equals("NA")) {
			SubWalletDetailsList paytmWallet = array.get(0);
			List<Object> paytmList = new ArrayList<>();
			paytmList.add("0");
			paytmList.add(dc.format(paytmWallet.getBalance()));
			myList.put("", paytmList);
		}
		for (int i = 1; i < array.size(); i++) {
			SubWalletDetailsList site = array.get(i);
			if (aggregate.equals("no")) {
				String ppiId = "";
				if (site.getPpiDetailsId() != null)
					ppiId = String.valueOf(site.getPpiDetailsId());
				String subWalletType = site.getSubWalletType();
				BigDecimal balance = site.getBalance();
				List<Object> list = new ArrayList<>();
				list.add(subWalletType);
				list.add(dc.format(balance));
				myList.put(ppiId, list);
			} else if (aggregate.equals("yes")) {
				String ppiId = "";
				if (site.getPpiDetailsId() != null)
					ppiId = String.valueOf(site.getPpiDetailsId());
				String subWalletType = site.getSubWalletType();
				BigDecimal balance = site.getBalance();
				List<Object> list = new ArrayList<>();
				list.add(ppiId);
				list.add(dc.format(balance));
				myList.put(subWalletType, list);
			}
		}
		return myList;
	}

	public BigDecimal fetchUserPaytmBalance(String uid) {
		BigDecimal amount = BigDecimal.ZERO;
		Map<Object, Object> params = new HashMap<>();
		DBValidation db = new DBValidation();
		params.put(DBEnums.USERID, uid);
		String query = DBQueries.scratchpadBalance;
		List<Map<String, Object>> result = db.runDbQuery(query, params, "log4j");
		BigDecimal scratchpadAmount = BigDecimal.ZERO;
		BigDecimal giftAmount = BigDecimal.ZERO;
		if (result.size() > 0 && null != result.get(0).get("mytotal"))
			scratchpadAmount = new BigDecimal(result.get(0).get("mytotal").toString());
		String giftQuery = DBQueries.giftVoucherWalletDetailsBalance;
		List<Map<String, Object>> giftResult = db.runDbQuery(giftQuery, params, "log4j");
		if (giftResult.size() > 0 && null != giftResult.get(0).get("giftVoucherAmount"))
			giftAmount = new BigDecimal(giftResult.get(0).get("giftVoucherAmount").toString());
		String balanceQuery = DBQueries.walletDetailsBalance;
		List<Map<String, Object>> walletDetailsResult = db.runDbQuery(balanceQuery, params, "log4j");
		if (walletDetailsResult.size() > 0 && null != walletDetailsResult.get(0).get("mytotal")) {
			amount = new BigDecimal(walletDetailsResult.get(0).get("mytotal").toString());
			amount = amount.add(scratchpadAmount);
			amount = amount.add(giftAmount);
		}
		return amount;
	}

	public BigDecimal fetchAgentBalance(String walletGuid) {
		DBValidation db = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.WALLETGUID, walletGuid.replace("-", ""));
		List<Map<String, Object>> result = db.runDbQuery(DBQueries.fetchUserIdFromWalletGuid, params, "log4j");
		System.out.println("result is " + result);
		String userId = result.get(0).get("owner_id").toString().replaceAll("[\\[\\]]", "");
		BigDecimal amount = fetchUserPaytmBalance(userId);
		return amount;
	}

	public void validateAgentBalance(FundCheckBalance fundCheckBalance) {
		BigDecimal amount = fetchAgentBalance(fundCheckBalance.getRequestPojo().getRequest().getWalletGUID());
		AssertionValidations.verifyAssertEqual(fundCheckBalance.getResponsePojo().getResponse().getAmount().setScale(2),
				amount.setScale(2));
	}

	public BigDecimal fetchSubWalletBalanceDB(String query) {
		BigDecimal amount = BigDecimal.ZERO;
		List<Map<String, Object>> result = DatabaseUtil.getInstance()
				.executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, query);
		if (result.size() > 0 && null != result.get(0).get("Balance"))
			amount = (BigDecimal) result.get(0).get("Balance");
		return amount;
	}

	private List<Object> aggregateDBSubWalletDetails(List<Map<String, Object>> result, int i) {
		DecimalFormat dc = new DecimalFormat("###0.00");
		String ppi_id = result.get(i).get("id").toString();
		String ppi_type = result.get(i).get("ppi_type").toString();
		BigDecimal sumBalance = new BigDecimal(result.get(i).get("SUM(a.balance)").toString());
		int countppiType = Integer.parseInt(result.get(i).get("COUNT(a.ppi_type)").toString());
		if (countppiType > 1)
			ppi_id = "";
		List<Object> list = new ArrayList<>();
		list.add(ppi_id);
		list.add(ppi_type);
		list.add(dc.format(sumBalance));
		list.add(countppiType);
		return list;
	}

	private List<Object> nonAggregateDBListSubWalletDetails(List<Map<String, Object>> result, int i) {
		DecimalFormat dc = new DecimalFormat("###0.00");
		String ppi_type = result.get(i).get("ppi_type").toString();
		BigDecimal balance = new BigDecimal(result.get(i).get("Balance").toString());
		List<Object> list = new ArrayList<>();
		list.add(ppi_type);
		list.add(dc.format(balance));
		return list;
	}

	public Map<Object, Object> dBMapSubWalletDetails(String query, String aggregate, BigDecimal paytmAmountFromDB) {
		List<Map<String, Object>> result = DatabaseUtil.getInstance()
				.executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, query);
		Map<Object, Object> myList = new HashMap<Object, Object>();
		Map<Object, Object> myList1 = new HashMap<Object, Object>();
		DecimalFormat dc = new DecimalFormat("###0.00");
		int count = 0;
		BigDecimal balance1 = BigDecimal.ZERO;
		BigDecimal balanceN = BigDecimal.ZERO;
		if (!aggregate.equals("NA")) {
			List<Object> paytmList = new ArrayList<>();
			paytmList.add("0");
			paytmList.add(dc.format(paytmAmountFromDB));
			myList.put("", paytmList);
			for (int i = 0; i < result.size(); i++) {
				if (aggregate.equals("no")) {
					String ppi_id = result.get(i).get("id").toString();
					List<Object> list = nonAggregateDBListSubWalletDetails(result, i);
					myList.put(ppi_id, list);
					myList1.put(ppi_id, list);
				} else if (aggregate.equals("yes")) {
					List<Object> list = aggregateDBSubWalletDetails(result, i);
					String ppiType = list.get(1).toString();
					list.remove(1);
					list.remove(2);
					myList.put(ppiType, list);
				}
			}
		}
		if (!(myList1.isEmpty())) {
			for (Object key : myList1.keySet()) {
				List<Object> list1 = (List<Object>) myList.get(key);
				if (list1.get(0).toString().equals("12")) {
					balance1 = new BigDecimal(list1.get(1).toString());
					count++;
					if (count >= 1) {
						balanceN = balance1.add(balanceN);
						myList.remove(key);
						List<Object> listN = new ArrayList<>();
						listN.add("12");
						listN.add(dc.format(balanceN));
						if (count > 1)
							myList.put("", listN);
					}
				}
			}
		}
		return myList;
	}

	public void verifyUserBalanceDetails(String actualResponse, CheckUserBalance checkUserBalance, String aggregate,
			String detailQuery) {
		DecimalFormat dc = new DecimalFormat("###0.00");
		String ssoId = checkUserBalance.getResponsePojo().getResponse().getSsoId();
		if (!aggregate.equals("NA"))
			AssertionValidations.verifyAssertEqual(checkUserBalance.getResponsePojo().getResponse()
					.getSubWalletDetailsList().get(0).getSubWalletType(), "0");
		BigDecimal paytmAmountFromDB = fetchUserPaytmBalance(ssoId);
		paytmAmountFromDB = new BigDecimal(dc.format(paytmAmountFromDB));
		Map<Object, Object> myList = serverResponseSubWalletDetailsMap(aggregate, checkUserBalance);
		Map<Object, Object> dbMap = dBMapSubWalletDetails(detailQuery, aggregate, paytmAmountFromDB);
		System.out.println("myList is " + myList);
		System.out.println("dbMap is " + dbMap);
		CommonHelpers.getInstance().compareObjectMap(myList, dbMap,
				"Mismatch in SubWallet Details from DB and Server Response");
	}

	public void verifyUserBalance(String actualresponse, CheckUserBalance checkUserBalance, String balanceQuery){
		String ssoId = null;
		ssoId = checkUserBalance.getResponsePojo().getResponse().getSsoId();
		BigDecimal subWalletBalanceFromDB = BigDecimal.ZERO;
		BigDecimal paytmAmountFromDB = fetchUserPaytmBalance(ssoId);
		DecimalFormat dc = new DecimalFormat("###0.00");
		paytmAmountFromDB = new BigDecimal(dc.format(paytmAmountFromDB));
		subWalletBalanceFromDB = fetchSubWalletBalanceDB(balanceQuery);
		subWalletBalanceFromDB = new BigDecimal(dc.format(subWalletBalanceFromDB));
		BigDecimal totalAmount = paytmAmountFromDB.add(subWalletBalanceFromDB);
		BigDecimal paytmWalletBalance_Response = checkUserBalance.getResponsePojo().getResponse()
				.getPaytmWalletBalance();
		paytmWalletBalance_Response = new BigDecimal(dc.format(paytmWalletBalance_Response));
		BigDecimal totalBalance_Response = checkUserBalance.getResponsePojo().getResponse().getTotalBalance();
		totalBalance_Response = new BigDecimal(dc.format(totalBalance_Response));
		BigDecimal otherSubWalletBalance_Response = checkUserBalance.getResponsePojo().getResponse()
				.getOtherSubWalletBalance();
		otherSubWalletBalance_Response = new BigDecimal(dc.format(otherSubWalletBalance_Response));
		AssertionValidations.verifyAssertEqual(paytmWalletBalance_Response, paytmAmountFromDB);
		AssertionValidations.verifyAssertEqual(totalBalance_Response, totalAmount);
		AssertionValidations.verifyAssertEqual(otherSubWalletBalance_Response, subWalletBalanceFromDB);
	}

	public void callCheckUserMerchantBalance(MerchantTypes mType, CheckUserBalance checkUserBalance){
		DBValidation db = new DBValidation();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.NAME);
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.USERID, UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		params.put(DBEnums.MERCHANTGUID, merchantGuid.replaceAll("-", ""));
		params.put(DBEnums.MERCHANTID, mid);
		List<Map<String, Object>> list = db.runDbQuery(DBQueries.fetchMerchantCategory, params, "log4j");
		if (list.get(0).get("sub_category") == null)
			params.put(DBEnums.SUBCATEGORY, "NULL");
		else
			params.put(DBEnums.SUBCATEGORY, list.get(0).get("sub_category").toString().replaceAll("[\\[\\]]", ""));
		verifySubwalletMerchantUserBalance(params, checkUserBalance);
	}

	public void callCheckUserFlaggedMerchantBalance(CheckUserBalance checkUserBalance){
		Map<Object, Object> params = new HashMap<>();
		DBValidation db = new DBValidation();
		params.put(DBEnums.USERID, UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		params.put(DBEnums.MERCHANTID, UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID));
		List<Map<String, Object>> list = db.runDbQuery(DBQueries.fetchFlaggedMerchantCategory, params, "log4j");
		if (list.size() == 0 || list.get(0).get("sub_category") == null)
			params.put(DBEnums.SUBCATEGORY, "NULL");
		else
			params.put(DBEnums.SUBCATEGORY, list.get(0).get("sub_category").toString().replaceAll("[\\[\\]]", ""));
		verifySubwalletMerchantUserBalance(params, checkUserBalance);
	}

	public void validatePaytmBalance(CheckBalance checkBalance) {
		String ssoId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal paytmAmountFromDB = fetchUserPaytmBalance(ssoId);
		BigDecimal paytmWalletBalance_Response = checkBalance.getResponsePojo().getResponse().getAmount();
		paytmWalletBalance_Response = new BigDecimal(paytmWalletBalance_Response.toString());
		AssertionValidations.verifyAssertEqual(paytmWalletBalance_Response.setScale(2), paytmAmountFromDB.setScale(2));
	}

	private void verifySubwalletMerchantUserBalance(Map<Object, Object> params, CheckUserBalance checkUserBalance){
		String ssoId = null;
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		ssoId = checkUserBalance.getResponsePojo().getResponse().getSsoId();
		BigDecimal paytmAmountFromDB = fetchUserPaytmBalance(ssoId);
		List<Object> paytmList = new ArrayList<>();
		paytmList.add("0");
		paytmList.add(paytmAmountFromDB.setScale(2));
		DecimalFormat dc = new DecimalFormat("###0.00");
		paytmAmountFromDB = new BigDecimal(dc.format(paytmAmountFromDB));
		Map<Object, Object> userSubWalletDetail = subwalletIssuer.userSubWalletDetail(params);
		Map<Object, Object> serverResponse = serverResponseSubWalletDetailsMap("yes", checkUserBalance);
		BigDecimal subWalletAmountFromDB = BigDecimal.ZERO;
		subWalletAmountFromDB = (BigDecimal) userSubWalletDetail.get("totalAmount");
		BigDecimal totalAmountFromDB = paytmAmountFromDB.add(subWalletAmountFromDB);
		userSubWalletDetail.put("", paytmList);
		userSubWalletDetail.remove("totalAmount");
		BigDecimal paytmWalletBalance_Response = checkUserBalance.getResponsePojo().getResponse()
				.getPaytmWalletBalance();
		paytmWalletBalance_Response = new BigDecimal(dc.format(paytmWalletBalance_Response));
		BigDecimal totalBalance_Response = checkUserBalance.getResponsePojo().getResponse().getTotalBalance();
		totalBalance_Response = new BigDecimal(dc.format(totalBalance_Response));
		BigDecimal otherSubWalletBalance_Response = checkUserBalance.getResponsePojo().getResponse()
				.getOtherSubWalletBalance();
		otherSubWalletBalance_Response = new BigDecimal(dc.format(otherSubWalletBalance_Response));
		AssertionValidations.verifyAssertEqual(paytmWalletBalance_Response.setScale(2), paytmAmountFromDB.setScale(2));
		AssertionValidations.verifyAssertEqual(totalBalance_Response.setScale(2), totalAmountFromDB.setScale(2));
		AssertionValidations.verifyAssertEqual(otherSubWalletBalance_Response.setScale(2),
				subWalletAmountFromDB.setScale(2));
		System.out.println("serverResponse is " + serverResponse);
		System.out.println("userSubWalletDetail is " + userSubWalletDetail);
		CommonHelpers.getInstance().compareObjectMap(serverResponse, userSubWalletDetail,
				"Mismatch in SubWallet Details from DB and Server Response");
	}

	public void updateExpiryDate(String custId) {
		Map<Object, Object> params = new HashMap<>();
		DBValidation db = new DBValidation();
		String futureDateTime = CommonHelpers.getInstance().getFutureHour("yyyy-MM-dd HH:mm:ss", -1);
		params.put(DBEnums.TIMESTAMP, futureDateTime);
		params.put(DBEnums.CUSTID, custId);
		db.runUpdateQuery(DBQueries.updateExpiryPpi, params);
	}

}
