package com.paytm.helpers;

import com.paytm.apiPojo.SubWalletConsolidatedTxn;
import com.paytm.constants.Constants;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.enums.DBEnums;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.framework.utils.DatabaseUtil;
import com.paytm.utils.DBValidation;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserSubwalletConsolidated {
	private List<Map<String, Object>> consolidatedSubwalletTxn;

	public void setDailyCreditAmountLimit(SubWalletConsolidatedTxn consolidate, BigDecimal amount) throws IOException {
		if (consolidate.getCredit() == null)
			consolidate.createDefaultCreditMap(consolidate);
		BigDecimal amountVal = consolidate.getCredit().getThroughputAmount();
		amount = amount.add(amountVal);
		consolidate.getCredit().setThroughputAmount(amount);
	}

	public void setDailyCreditCountLimit(SubWalletConsolidatedTxn consolidate, int count) throws IOException {
		if (consolidate.getCredit() == null)
			consolidate.createDefaultCreditMap(consolidate);
		int countVal = consolidate.getCredit().getTransactionCount();
		count = count + countVal;
		consolidate.getCredit().setTransactionCount(count);
	}

	public void setDailyDebitAmountLimit(SubWalletConsolidatedTxn consolidate, BigDecimal amount) throws IOException {
		if (consolidate.getDebit() == null)
			consolidate.createDefaultDebitMap(consolidate);
		BigDecimal amountVal = consolidate.getDebit().getThroughputAmount();
		amount = amount.add(amountVal);
		consolidate.getDebit().setThroughputAmount(amount);
	}

	public void setDailyDebitCountLimit(SubWalletConsolidatedTxn consolidate, int count) throws IOException {
		if (consolidate.getDebit() == null)
			consolidate.createDefaultDebitMap(consolidate);
		int countVal = consolidate.getDebit().getTransactionCount();
		count = count + countVal;
		consolidate.getDebit().setTransactionCount(count);
	}

	public void setMonthlyCreditAmountLimit(SubWalletConsolidatedTxn consolidate, BigDecimal amount)
			throws IOException {
		if (consolidate.getCredit() == null)
			consolidate.createDefaultCreditMap(consolidate);
		BigDecimal amountVal = consolidate.getCredit().getThroughputAmount();
		amount = amount.add(amountVal);
		consolidate.getCredit().setThroughputAmount(amount);
	}

	public void setMonthlyCreditCountLimit(SubWalletConsolidatedTxn consolidate, int count) throws IOException {
		if (consolidate.getCredit() == null)
			consolidate.createDefaultCreditMap(consolidate);
		int countVal = consolidate.getCredit().getTransactionCount();
		count = count + countVal;
		consolidate.getCredit().setTransactionCount(count);
	}

	public void setMonthlyDebitAmountLimit(SubWalletConsolidatedTxn consolidate, BigDecimal amount) throws IOException {
		if (consolidate.getDebit() == null)
			consolidate.createDefaultDebitMap(consolidate);
		BigDecimal amountVal = consolidate.getDebit().getThroughputAmount();
		amount = amount.add(amountVal);
		consolidate.getDebit().setThroughputAmount(amount);
	}

	public void setMonthlyDebitCountLimit(SubWalletConsolidatedTxn consolidate, int count) throws IOException {
		if (consolidate.getDebit() == null)
			consolidate.createDefaultDebitMap(consolidate);
		int countVal = consolidate.getDebit().getTransactionCount();
		count = count + countVal;
		consolidate.getDebit().setTransactionCount(count);
	}

	public void setYearlyCreditAmountLimit(SubWalletConsolidatedTxn consolidate, BigDecimal amount) throws IOException {
		if (consolidate.getCredit() == null)
			consolidate.createDefaultCreditMap(consolidate);
		BigDecimal amountVal = consolidate.getCredit().getThroughputAmount();
		amount = amount.add(amountVal);
		consolidate.getCredit().setThroughputAmount(amount);
	}

	public void setYearlyCreditCountLimit(SubWalletConsolidatedTxn consolidate, int count) throws IOException {
		if (consolidate.getCredit() == null)
			consolidate.createDefaultCreditMap(consolidate);
		int countVal = consolidate.getCredit().getTransactionCount();
		count = count + countVal;
		consolidate.getCredit().setTransactionCount(count);
	}

	public void setYearlyDebitAmountLimit(SubWalletConsolidatedTxn consolidate, BigDecimal amount) throws IOException {
		if (consolidate.getDebit() == null)
			consolidate.createDefaultDebitMap(consolidate);
		BigDecimal amountVal = consolidate.getDebit().getThroughputAmount();
		amount = amount.add(amountVal);
		consolidate.getDebit().setThroughputAmount(amount);
	}

	public void setYearlyDebitCountLimit(SubWalletConsolidatedTxn consolidate, int count) throws IOException {
		if (consolidate.getDebit() == null)
			consolidate.createDefaultDebitMap(consolidate);
		int countVal = consolidate.getDebit().getTransactionCount();
		count = count + countVal;
		consolidate.getDebit().setTransactionCount(count);
	}

	private List<Map<String, Object>> fetchDailyTxnData(String sctId) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.SCTID, sctId);
		List<Map<String, Object>> dailyTxn = dbValidation.runDbQuery(DBQueries.userSubWalletConsolidatedDailyTxn, params,"log4j");
		return dailyTxn;
	}

	public SubWalletConsolidatedTxn dailyTxnQuery(String sctId, SubWalletConsolidatedTxn consolidate1)
		{
		String curDayTime = CommonHelpers.getInstance().getCurrentDateTime("yyyy-MM-dd");
		SubWalletConsolidatedTxn consolidateObject = new SubWalletConsolidatedTxn();
		List<Map<String, Object>> dailyTxn = fetchDailyTxnData(sctId);
		if (dailyTxn.size() > 0) {
			String updateTimestamp = dailyTxn.get(0).get("update_timestamp").toString();
			String updatedDayTime = CommonHelpers.getInstance().getDateFormat(updateTimestamp, "yyyy-MM-dd hh:mm:ss",
					"yyyy-MM-dd");
			if (updatedDayTime.equals(curDayTime))
				consolidate1 = consolidateObject.test(dailyTxn.get(0).get("daily_txn").toString());
		}
		return consolidate1;
	}

	public SubWalletConsolidatedTxn validateDailyExtendedDetails(Map<String, List<Object>> consolidatedMap,
			List<Map<String, Object>> extendedDetails, Boolean isCredit) throws IOException {
		SubWalletConsolidatedTxn consolidate = new SubWalletConsolidatedTxn();
		for (int i = 0; i < extendedDetails.size(); i++) {
			SubWalletConsolidatedTxn consolidateObject = new SubWalletConsolidatedTxn();
			SubWalletConsolidatedTxn consolidate1 = new SubWalletConsolidatedTxn();
			String sctId = extendedDetails.get(i).get("sct_id").toString();
			BigDecimal amount = new BigDecimal(extendedDetails.get(i).get("txn_amount").toString());
			List<Map<String, Object>> dailyTxn = fetchDailyTxnData(sctId);
			String afterTimestamp = dailyTxn.get(0).get("update_timestamp").toString();
			if (dailyTxn.size() > 0)
				consolidate1 = consolidateObject.test(dailyTxn.get(0).get("daily_txn").toString());
			System.out.println("consolidatedMap is " + consolidatedMap);
			System.out.println("SctId is " + consolidatedMap.get(sctId));
			if (!consolidatedMap.isEmpty() && consolidatedMap.containsKey(sctId)
					&& consolidatedMap.get(sctId).get(0) != null) {
				String beforeTimestamp = consolidatedMap.get(sctId).get(3).toString();
				Boolean value = CommonHelpers.getInstance().checkIfCountersNeedToBeRolledOver("yyyy-MM-dd",
						beforeTimestamp, afterTimestamp);
				if (value.equals(false))
					consolidate = consolidateObject.test(consolidatedMap.get(sctId).get(0).toString());
				else
					consolidate = new SubWalletConsolidatedTxn();
			} else
				consolidate = new SubWalletConsolidatedTxn();
			if (isCredit) {
				setDailyCreditAmountLimit(consolidate, amount);
				setDailyCreditCountLimit(consolidate, 1);
			} else {
				setDailyDebitAmountLimit(consolidate, amount);
				setDailyDebitCountLimit(consolidate, 1);
			}
			CommonHelpers.getInstance().comparePOJO(consolidate, consolidate1);
		}
		return consolidate;
	}

	public SubWalletConsolidatedTxn validateMonthlyExtendedDetails(Map<String, List<Object>> consolidatedMap,
			List<Map<String, Object>> extendedDetails, Boolean isCredit) throws IOException {
		DBValidation dbValidation = new DBValidation();
		SubWalletConsolidatedTxn consolidate = new SubWalletConsolidatedTxn();
		SubWalletConsolidatedTxn consolidate1 = new SubWalletConsolidatedTxn();
		for (int i = 0; i < extendedDetails.size(); i++) {
			SubWalletConsolidatedTxn consolidateObject = new SubWalletConsolidatedTxn();
			String sctId = extendedDetails.get(i).get("sct_id").toString();
			BigDecimal amount = new BigDecimal(extendedDetails.get(i).get("txn_amount").toString());
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.SCTID, sctId);
			String monthlyQuery = dbValidation.fetchQuery(DBQueries.userSubWalletConsolidatedMonthlyTxn, params);
			List<Map<String, Object>> monthlyTxn = DatabaseUtil.getInstance()
					.executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, monthlyQuery);
			String afterTimestamp = monthlyTxn.get(0).get("update_timestamp").toString();
			if (monthlyTxn.size() > 0)
				consolidate1 = consolidateObject.test(monthlyTxn.get(0).get("monthly_txn").toString());
			if (!consolidatedMap.isEmpty() && consolidatedMap.containsKey(sctId)
					&& consolidatedMap.get(sctId).get(1) != null) {
				String beforeTimestamp = consolidatedMap.get(sctId).get(3).toString();
				Boolean value = CommonHelpers.getInstance().checkIfCountersNeedToBeRolledOver("yyyy-MM",
						beforeTimestamp, afterTimestamp);
				if (value.equals(false))
					consolidate = consolidateObject.test(consolidatedMap.get(sctId).get(1).toString());
				else
					consolidate = new SubWalletConsolidatedTxn();
			} else
				consolidate = new SubWalletConsolidatedTxn();
			if (isCredit) {
				setMonthlyCreditAmountLimit(consolidate, amount);
				setMonthlyCreditCountLimit(consolidate, 1);
			} else {
				setMonthlyDebitAmountLimit(consolidate, amount);
				setMonthlyDebitCountLimit(consolidate, 1);
			}
			CommonHelpers.getInstance().comparePOJO(consolidate, consolidate1);
		}
		return consolidate;
	}

	public SubWalletConsolidatedTxn validateYearlyExtendedDetails(Map<String, List<Object>> consolidatedMap,
			List<Map<String, Object>> extendedDetails, Boolean isCredit) throws IOException {
		DBValidation dbValidation = new DBValidation();
		SubWalletConsolidatedTxn consolidate = new SubWalletConsolidatedTxn();
		SubWalletConsolidatedTxn consolidate1 = new SubWalletConsolidatedTxn();
		for (int i = 0; i < extendedDetails.size(); i++) {
			SubWalletConsolidatedTxn consolidateObject = new SubWalletConsolidatedTxn();
			String sctId = extendedDetails.get(i).get("sct_id").toString();
			BigDecimal amount = new BigDecimal(extendedDetails.get(i).get("txn_amount").toString());
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.SCTID, sctId);
			String yearlyQuery = dbValidation.fetchQuery(DBQueries.userSubWalletConsolidatedYearlyTxn, params);
			List<Map<String, Object>> yearlyTxn = DatabaseUtil.getInstance()
					.executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, yearlyQuery);
			String afterTimestamp = yearlyTxn.get(0).get("update_timestamp").toString();
			if (yearlyTxn.size() > 0)
				consolidate1 = consolidateObject.test(yearlyTxn.get(0).get("yearly_txn").toString());
			if (!consolidatedMap.isEmpty() && consolidatedMap.containsKey(sctId)
					&& consolidatedMap.get(sctId).get(2) != null) {
				String beforeTimestamp = consolidatedMap.get(sctId).get(3).toString();
				Boolean value = CommonHelpers.getInstance().checkIfCountersNeedToBeRolledOver("yyyy", beforeTimestamp,
						afterTimestamp);
				if (value.equals(false))
					consolidate = consolidateObject.test(consolidatedMap.get(sctId).get(2).toString());
				else
					consolidate = new SubWalletConsolidatedTxn();
			} else
				consolidate = new SubWalletConsolidatedTxn();
			if (isCredit) {
				setYearlyCreditAmountLimit(consolidate, amount);
				setYearlyCreditCountLimit(consolidate, 1);
			} else {
				setYearlyDebitAmountLimit(consolidate, amount);
				setYearlyDebitCountLimit(consolidate, 1);
			}
			CommonHelpers.getInstance().comparePOJO(consolidate, consolidate1);
		}
		return consolidate;
	}

	public void resetSubWalletLimit(Map<String, Map<String, Object>> merchantSpecificDetail, MerchantTypes mGuid)
			throws IOException {
		System.out.println("Merchant Specific Detail is " + merchantSpecificDetail);
		System.out.println("mGuid is " + mGuid);
		DBValidation db = new DBValidation();
		SubWalletConsolidatedTxn consolidateObject = new SubWalletConsolidatedTxn();
		String mid = MerchantManager.getInstance().getMerchantDetails(mGuid, MerchantHeaders.ID);
		if(merchantSpecificDetail.containsKey(mid)) {
			String sctId = merchantSpecificDetail.get(mid).get("sctId").toString().replaceAll("[\\[\\]]", "");
			String[] sctIdArray = sctId.split(",", -1);
			for (int i = 0; i < sctIdArray.length; i++) {
				Map<Object, Object> params = new HashMap<>();
				params.put(DBEnums.SCTID, sctIdArray[i]);
				List<Map<String, Object>> list = db.runDbQuery(DBQueries.userSubWalletConsolidatedDailyTxn, params, "log4j");
				if (list.size() > 0) {
					SubWalletConsolidatedTxn consolidate = consolidateObject.test(list.get(0).get("daily_txn").toString());
					if (consolidate.getDebit() != null) {
						if (!(consolidate.getDebit().getThroughputAmount().compareTo(BigDecimal.ZERO) == 0)
								&& !(consolidate.getDebit().getTransactionCount() == 0)) {
							consolidate.createDefaultDebitMap(consolidate);
							String json = JacksonJsonImpl.getInstance().toJSon(consolidate);
							params.put(DBEnums.DAILYTXNJSON, json);
							String query = db.fetchQuery(DBQueries.updateDailyConsolidatedTxn, params);
							DatabaseUtil.getInstance()
									.executeUpdateQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, query);
						}
					}
				}
			}
		}

	}

	public List<Map<String, Object>> extendedDetailMap(String txnId) {
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.WALLETSYSTEMTXNID, txnId);
		List<Map<String, Object>> extendedDetails = dbValidation.runDbQuery(DBQueries.fetchSctIdAmountPpi, params, "log4j");
		return extendedDetails;
	}

	public static void main(String[] args) throws IOException {
		UserSubwalletConsolidated sub = new UserSubwalletConsolidated();
		sub.fetchConsolidatedData("subwalletmerchant", "8914914f7556476c8d501741b27babf0", "11069336");
	}

	public void fetchConsolidatedData(String mid, String mGuid, String custId){
		DBValidation dbValidation = new DBValidation();
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		Map<Object, Object> paramMap = new HashMap<>();
		paramMap.put(DBEnums.MERCHANTID, mid);
		paramMap.put(DBEnums.MERCHANTGUID, mGuid);
		paramMap.put(DBEnums.USERID, custId);
		List<Map<String, Object>> consolidateMap = new ArrayList<>();
		Map<String, Map<String, Object>> merchantSpecificSubwallet = subwalletIssuer
				.checkBalanceMerchantSpecificDetail(paramMap);
		for (Object key : merchantSpecificSubwallet.keySet()) {
			if (merchantSpecificSubwallet.get(key).containsKey("sctId")) {
				List<Map<String, Object>> tempMap = new ArrayList<>();
				List<String> sctIdList = (List<String>) merchantSpecificSubwallet.get(key).get("sctId");
				paramMap.put(DBEnums.SCTID,sctIdList.toString().replaceAll("[\\[\\]]", ""));
				tempMap = dbValidation.runDbQuery(DBQueries.userSubWalletConsolidatedTxn, paramMap,"log4j");
				if(tempMap.size() > 0)
				consolidateMap.addAll(tempMap);
			}
		}
		this.consolidatedSubwalletTxn = consolidateMap;
	}

	public void fetchConsolidatedData(String payerCustId, String payeeCustId){
		DBValidation dbValidation = new DBValidation();
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		Map<Object, Object> paramMap = new HashMap<>();
		paramMap.put(DBEnums.MERCHANTID, payeeCustId);
//		paramMap.put(DBEnums.MERCHANTGUID, mGuid);
		paramMap.put(DBEnums.USERID, payerCustId);
		List<Map<String, Object>> consolidateMap = new ArrayList<>();
		Map<String, Map<String, Object>> merchantSpecificSubwallet = subwalletIssuer
				.checkBalanceMerchantSpecificDetailP2P(paramMap);
		for (Object key : merchantSpecificSubwallet.keySet()) {
			if (merchantSpecificSubwallet.get(key).containsKey("sctId")) {
				List<Map<String, Object>> tempMap = new ArrayList<>();
				List<String> sctIdList = (List<String>) merchantSpecificSubwallet.get(key).get("sctId");
				paramMap.put(DBEnums.SCTID,sctIdList.toString().replaceAll("[\\[\\]]", ""));
				tempMap = dbValidation.runDbQuery(DBQueries.userSubWalletConsolidatedTxn, paramMap,"log4j");
				if(tempMap.size() > 0)
					consolidateMap.addAll(tempMap);
			}
		}
		for(Map<String,Object> testing : consolidateMap){
			if(testing.get("id").toString().equalsIgnoreCase("135609")){
				System.out.println("Entry found");
			}
		}
		this.consolidatedSubwalletTxn = consolidateMap;
	}



	public Map<String, List<Object>> fetchConsolidatedData() {
		List<Map<String, Object>> consolidatedSubwalletTxn = this.consolidatedSubwalletTxn;
		Map<String, List<Object>> map = new HashMap<>();
		System.out.println("consolidatedSubwalletTxn is " + consolidatedSubwalletTxn);
		for (int i = 0; i < consolidatedSubwalletTxn.size(); i++) {
			List<Object> list = new ArrayList<>();
			list.add(consolidatedSubwalletTxn.get(i).get("daily_txn"));
			list.add(consolidatedSubwalletTxn.get(i).get("monthly_txn"));
			list.add(consolidatedSubwalletTxn.get(i).get("yearly_txn"));
			list.add(consolidatedSubwalletTxn.get(i).get("update_timestamp"));
			map.put(consolidatedSubwalletTxn.get(i).get("id").toString(), list);
		}
		System.out.println("map is " + map);
		return map;
	}

	/**
	 *
	 * @param extendedDetails
	 * @param isCredit :- If the entry is supposed to be credit or debit
	 * @throws IOException
	 */
	public void validateSubwalletConsolidate(List<Map<String, Object>> extendedDetails, Boolean isCredit)
			throws IOException {
		Map<String, List<Object>> consolidatedMap = fetchConsolidatedData();
		validateDailyExtendedDetails(consolidatedMap, extendedDetails, isCredit);
		validateMonthlyExtendedDetails(consolidatedMap, extendedDetails, isCredit);
		validateYearlyExtendedDetails(consolidatedMap, extendedDetails, isCredit);
	}

}
