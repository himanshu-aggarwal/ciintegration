package com.paytm.helpers;

import com.google.common.collect.Lists;
import com.paytm.apiRequestBuilders.wallet.oldService.CheckUserBalance;
import com.paytm.apiRequestBuilders.wallet.walletWeb.AddFundsToSubWallet;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.Constants;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.DBEnums;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserHeaders;
import com.paytm.constants.LocalConfig;
import com.paytm.utils.DBValidation;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

public class SubWalletHelpers{

	public Map<String, BigDecimal> expectedWalletBalMap;

	public void adjustMoneyInMainWallet(String mobileNumber, BigDecimal amount, BigDecimal currentBalance) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		if (currentBalance.compareTo(amount) < 0) {
			BigDecimal leftamount = amount.subtract(currentBalance);
			try {
				walletAPIHelpers.mainWalletAdd(leftamount, mobileNumber);
			} catch (AssertionError e) {
				System.out.println("Add balance to main wallet failed,adding directly");
				walletAPIHelpers.addBalanceToMainWalletDirectly(amount, mobileNumber);
			}
		} else if (currentBalance.compareTo(amount) > 0) {
			BigDecimal leftamount = currentBalance.subtract(amount);
			String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
					MerchantHeaders.MERCHANTGUID);
			try {
				walletAPIHelpers.mainWalletWithdraw(leftamount, mobileNumber, merchantGuid);
			} catch (AssertionError e) {
				System.out.println("Withdraw balance to main wallet failed,adding directly");
				walletAPIHelpers.addBalanceToMainWalletDirectly(amount, mobileNumber);
			}
		}
	}

	// Ordering of current wallet balance map based on priority
	public Map<String, BigDecimal> priorityBasedCUBMap(Map<String, BigDecimal> userMap) {
		Map<String, BigDecimal> priorityMap = createPrioritySubWalletMap();
		Map<String, BigDecimal> currentWalletBalMap = new LinkedHashMap<>();
		for (String key : priorityMap.keySet()) {
			if (userMap.containsKey(key))
				currentWalletBalMap.put(key, userMap.get(key).setScale(2));
		}
		System.out.println("currentWalletBalMap is " + currentWalletBalMap);

		return currentWalletBalMap;
	}

	public Map<String, BigDecimal> reversePriorityBasedCUBMap(Map<String, BigDecimal> userMap) {
		Map<String, BigDecimal> priorityMap = createReversePrioritySubWalletMap();
		Map<String, BigDecimal> currentWalletBalMap = new LinkedHashMap<>();

		for (String key : priorityMap.keySet()) {
			if (userMap.containsKey(key)) {
				currentWalletBalMap.put(key, userMap.get(key).setScale(2));
			}
		}
		return currentWalletBalMap;
	}

	// Comparing 2 maps and assert if comparision is failure
	public void compareStringMap(Map<String, BigDecimal> actualMap, Map<String, BigDecimal> expectedMap,
			String assertMessage) {
		System.out.println("Final map received: " + actualMap);
		System.out.println("Map created is: " + expectedMap);
		if (actualMap.size() == expectedMap.size()) {
			for (Object ch1 : actualMap.keySet()) {
				DecimalFormat df = new DecimalFormat("###.##");
				Object myListVal = Math.abs(Double.parseDouble(actualMap.get(ch1).toString()));
				Object dbMapVal = Math.abs(Double.parseDouble(expectedMap.get(ch1).toString()));
				if (!(df.format(myListVal).equals(df.format(dbMapVal)))) {
					AssertionValidations.verifyAssertFail(assertMessage);
					return;
				}
			}
		} else {
			AssertionValidations.verifyAssertFail(assertMessage);
			return;
		}
	}

	public BigDecimal fetchTotalBalance(CheckUserBalance checkUserBalance, BigDecimal totalAmount) {
		BigDecimal amount = checkUserBalance.getResponsePojo().getResponse().getTotalBalance().add(totalAmount);
		return amount;
	}

	// New Function to test -- Created by Surili <Using>
	public void callAddSubWalletFund(Map<String, Map<String, Object>> merchantSpecificDetail, String mobileNumber,
			MerchantTypes cGuid, int length) {
		Map<String, BigDecimal> userSubWalletType = FundThreadManager.getInstance().walletMapViaWalletType();
		userSubWalletType.remove("MAIN");
		String corporateMGuid = MerchantManager.getInstance().getMerchantDetails(cGuid,
				MerchantHeaders.CORPORATEWALLETGUID);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(cGuid, MerchantHeaders.MERCHANTGUID);
		String id = MerchantManager.getInstance().getMerchantDetails(cGuid, MerchantHeaders.ID);
		for (String key : userSubWalletType.keySet()) {
			if (userSubWalletType.get(key).compareTo(BigDecimal.ZERO) == 1) {
				BigDecimal amount = new BigDecimal(Double.parseDouble(userSubWalletType.get(key).toString())
						/ Double.parseDouble(String.valueOf(length)));
				if (amount.compareTo(BigDecimal.ZERO) == 1) {
					WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
					if (!merchantSpecificDetail.containsKey(id))
						walletAPIHelpers.subWalletAdd(mobileNumber, key, amount, mGuid, corporateMGuid);
					else if (!merchantSpecificDetail.get(id).containsKey(LocalConfig.subWalletMapping.get(key)))
						walletAPIHelpers.subWalletAdd(mobileNumber, key, amount, mGuid, corporateMGuid);
					else if (((BigDecimal) merchantSpecificDetail.get(id)
							.get(LocalConfig.subWalletMapping.get(key).toString())).compareTo(amount) < 0) {
						BigDecimal presAmount = (BigDecimal) merchantSpecificDetail.get(id)
								.get(LocalConfig.subWalletMapping.get(key).toString());
						walletAPIHelpers.subWalletAdd(mobileNumber, key, (amount.subtract(presAmount)), mGuid, corporateMGuid);
					}
				}
			}
		}
	}


	// // New Function to test -- Created by Surili <Using>
	public void adjustFundsSubWallets(Map<String, BigDecimal> currentBalanceMap, String mobileNumber,
			MerchantTypes merchantType) {
		String corporateMGuid = MerchantManager.getInstance().getMerchantDetails(merchantType,
				MerchantHeaders.CORPORATEWALLETGUID);
		Map<String, BigDecimal> userSubWalletType = FundThreadManager.getInstance().walletMapViaWalletType();
		userSubWalletType.remove("MAIN");
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantType, MerchantHeaders.MERCHANTGUID);
		for (String key : userSubWalletType.keySet()) {
			BigDecimal expectedBalance = userSubWalletType.get(key);
			BigDecimal currentBalance = BigDecimal.ZERO;
			if (currentBalanceMap.containsKey(LocalConfig.subWalletMapping.get(key))) {
				currentBalance = currentBalanceMap.get(LocalConfig.subWalletMapping.get(key));
			}
			WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
			if (currentBalance.compareTo(expectedBalance) < 0) {
				BigDecimal amount = expectedBalance.subtract(currentBalance);
				walletAPIHelpers.subWalletAdd(mobileNumber, key, amount, mGuid, corporateMGuid);
			} else if (currentBalance.compareTo(expectedBalance) > 0) {
				BigDecimal amount = currentBalance.subtract(expectedBalance);
				mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTGUID);
				walletAPIHelpers.subWalletWithdraw(mobileNumber, key, amount, mGuid);
			}
			currentBalanceMap.put(LocalConfig.subWalletMapping.get(key), expectedBalance.setScale(2));
		}

	}

	// // New Function to test -- Created by Surili <Using>
	public Map<String, BigDecimal> getExpectedWalletBalMap(Map<String, BigDecimal> currentWalletBalMap) {
		Map<String, BigDecimal> subWalletMap = FundThreadManager.getInstance().walletMapViaWalletId();
		Map<String, BigDecimal> expectedWalletBalMap = new HashMap<>();
		expectedWalletBalMap.putAll(currentWalletBalMap);
		for (String key : subWalletMap.keySet()) {
			BigDecimal value;
			BigDecimal value1;
			value = subWalletMap.get(key);
			if (value.compareTo(BigDecimal.ZERO) > 0) {
				for (String key1 : currentWalletBalMap.keySet()) {
					if ((key.equals(key1))
							|| (!subWalletMap.containsKey(key1) && value.compareTo(BigDecimal.ZERO) > 0)) {
						if (expectedWalletBalMap.get(key1).compareTo(value) > 0) {
							value1 = expectedWalletBalMap.get(key1).subtract(value);
							expectedWalletBalMap.put(key1, value1.setScale(2 , BigDecimal.ROUND_HALF_UP));
							value = BigDecimal.ZERO;
							break;
						} else {
							value = value.subtract(expectedWalletBalMap.get(key1));
							expectedWalletBalMap.put(key1, BigDecimal.ZERO);
						}
					}
				}
				value = expectedWalletBalMap.get("0").subtract(value);
				if (value.compareTo(BigDecimal.ZERO) > 0 && expectedWalletBalMap.get("0").compareTo(value) > 0)
					expectedWalletBalMap.put("0", value.setScale(2));
			}
		}

		System.out.println("priorityMap after is " + expectedWalletBalMap);
		return expectedWalletBalMap;
	}

	public Map<String, BigDecimal> getReverseExpectedWalletBalMap(Map<String, BigDecimal> currentWalletBalMap) {
		Map<String, BigDecimal> subWalletMap = FundThreadManager.getInstance().walletMapViaWalletId();
		Map<String, BigDecimal> expectedWalletBalMap = new HashMap<>();
		expectedWalletBalMap.putAll(currentWalletBalMap);
		for (String key : subWalletMap.keySet()) {
			BigDecimal value;
			BigDecimal value1;
			value = subWalletMap.get(key);
			if (value.compareTo(BigDecimal.ZERO) > 0) {
				for (String key1 : currentWalletBalMap.keySet()) {
					if ((key.equals(key1))
							|| (!subWalletMap.containsKey(key1) && value.compareTo(BigDecimal.ZERO) > 0)) {
						value1 = expectedWalletBalMap.get(key1).add(value);
						expectedWalletBalMap.put(key1, value1.setScale(2));
						value = BigDecimal.ZERO;
					}
				}
				value = expectedWalletBalMap.get("0").subtract(value);
				if (value.compareTo(BigDecimal.ZERO) > 0 && expectedWalletBalMap.get("0").compareTo(value) > 0)
					expectedWalletBalMap.put("0", value.setScale(2));
			}
		}

		System.out.println("priorityMap after is " + expectedWalletBalMap);
		return expectedWalletBalMap;
	}

	// New Function to test -- Created by Surili <Using>
	public void addFunds(Map<String, Map<String, Object>> merchantSpecificDetail, MerchantTypes corporateGuid,
			int length) {
		callAddSubWalletFund(merchantSpecificDetail,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER), corporateGuid, length);
	}

	// // New Function to test -- Created by Surili <Using>
	public void addMoneyInSubwalletMainwallet(MerchantTypes mType, String mobileNumber) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = FundThreadManager.getInstance().getMainWalletAmount();
		setSubWalletBalanceZero(mobileNumber);
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
		Map<String, BigDecimal> currentBalanceMap = fetchSubWalletBalance(beforeCheckUserBalance);
		adjustMoneyInMainWallet(mobileNumber, mainWalletAmount, currentBalanceMap.get("0"));
		adjustFundsSubWallets(currentBalanceMap, mobileNumber, mType);
	}

	public void addMoneyInSubwalletMainwalletP2P(MerchantTypes mType, String mobileNumber) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		BigDecimal mainWalletAmount = FundThreadManager.getInstance().getMainWalletAmount();
		setSubWalletBalanceZero(mobileNumber);
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseP2P();
		Map<String, BigDecimal> currentBalanceMap = fetchSubWalletBalance(beforeCheckUserBalance);
		adjustMoneyInMainWallet(mobileNumber, mainWalletAmount, currentBalanceMap.get("0"));
		adjustFundsSubWallets(currentBalanceMap, mobileNumber, mType);
	}

	public void setSubWalletBalanceZero(String mobileNumber){
		DBValidation dbValidation = new DBValidation();
		Map<Object,Object> params = new HashMap<>();
		params.put(DBEnums.MOBILENO,mobileNumber);
		dbValidation.runUpdateQuery(DBQueries.setSubWalletBalanceZero,params);
	}

	// // New Function to test -- Created by Surili <Using>
	public void createDebitTxnMap(MerchantTypes mType) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
		Map<String, BigDecimal> userMap = fetchSubWalletBalance(beforeCheckUserBalance);
		System.out.println("userMap is " + userMap);
		Map<String, BigDecimal> currentWalletBalMap = priorityBasedCUBMap(userMap);
		currentWalletBalMap = getExpectedWalletBalMap(currentWalletBalMap);
		BigDecimal totalAmount = fetchTotalBalance(beforeCheckUserBalance,
				FundThreadManager.getInstance().getTotalAmount().negate());
		currentWalletBalMap.put("totalAmount", totalAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
		this.expectedWalletBalMap = currentWalletBalMap;
	}

	public void createDebitTxnMapP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseP2P();
		Map<String, BigDecimal> userMap = fetchSubWalletBalance(beforeCheckUserBalance);
		Map<String, BigDecimal> currentWalletBalMap = priorityBasedCUBMap(userMap);
		currentWalletBalMap = getExpectedWalletBalMap(currentWalletBalMap);
		BigDecimal totalAmount = fetchTotalBalance(beforeCheckUserBalance,
				FundThreadManager.getInstance().getTotalAmount().negate()).add(getCashBackType());
		currentWalletBalMap.put("totalAmount", totalAmount.setScale(2));
		this.expectedWalletBalMap = currentWalletBalMap;
	}

	public void createDebitTxnMap() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseALL();
		Map<String, BigDecimal> userMap = fetchSubWalletBalance(beforeCheckUserBalance);
		Map<String, BigDecimal> currentWalletBalMap = priorityBasedCUBMap(userMap);
		currentWalletBalMap = getExpectedWalletBalMap(currentWalletBalMap);
		BigDecimal totalAmount = fetchTotalBalance(beforeCheckUserBalance,
				FundThreadManager.getInstance().getTotalAmount().negate());
		currentWalletBalMap.put("totalAmount", totalAmount.setScale(2));
		this.expectedWalletBalMap = currentWalletBalMap;
	}

	public void createCreditTxnMap(MerchantTypes mType) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
		Map<String, BigDecimal> userMap = fetchSubWalletBalance(beforeCheckUserBalance);
		Map<String, BigDecimal> currentWalletBalMap = reversePriorityBasedCUBMap(userMap);
		currentWalletBalMap = getReverseExpectedWalletBalMap(currentWalletBalMap);
		BigDecimal totalAmount = fetchTotalBalance(beforeCheckUserBalance, FundThreadManager.getInstance().getTotalAmount());
		currentWalletBalMap.put("totalAmount", totalAmount.setScale(2));
		this.expectedWalletBalMap = currentWalletBalMap;
	}

	public BigDecimal fetchTotalWalletBalance(CheckUserBalance checkUserBalance) {
		BigDecimal totalBalance = checkUserBalance.getResponsePojo().getResponse().getTotalBalance();
		return totalBalance;
	}

	// // New Function to test -- Created by Surili <Using>
	public Map<String, BigDecimal> fetchSubWalletBalance(CheckUserBalance checkUserBalance) {
		Map<String, BigDecimal> subWalletMap = new HashMap<>();
		int subWalletDetailsListSize = checkUserBalance.getResponsePojo().getResponse().getSubWalletDetailsList()
				.size();
		for (int i = 0; i < subWalletDetailsListSize; i++) {
			String subWalletType = checkUserBalance.getResponsePojo().getResponse().getSubWalletDetailsList().get(i)
					.getSubWalletType();
			BigDecimal amount = checkUserBalance.getResponsePojo().getResponse().getSubWalletDetailsList().get(i)
					.getBalance().setScale(2);
			if (subWalletMap.containsKey(subWalletType))
				subWalletMap.put(subWalletType, subWalletMap.get(subWalletType).add(amount));
			else
				subWalletMap.put(subWalletType, amount.setScale(2));
		}
		System.out.println("subWalletMap is " + subWalletMap);

		return subWalletMap;
	}

	public static Map<String, BigDecimal> createPrioritySubWalletMap() {
		Map<String, BigDecimal> subWalletPrioritymap = new LinkedHashMap<>();
		for (String subwallet : priorityList()) {
			subWalletPrioritymap.put(subwallet, BigDecimal.ZERO);
		}
		return subWalletPrioritymap;
	}

	public static Map<String, BigDecimal> createReversePrioritySubWalletMap() {
		Map<String, BigDecimal> subWalletPrioritymap = new LinkedHashMap<>();
		for (String subwallet : reversePriorityList()) {
			subWalletPrioritymap.put(subwallet, BigDecimal.ZERO);
		}
		return subWalletPrioritymap;
	}

	public static List<String> priorityList() {
		List<String> subWalletPriorityList = Arrays
				.asList(new String[] { "7", "8", "2", "9", "11", "3", "12", "10", "0", "6" });
		return subWalletPriorityList;
	}

	public static List<String> reversePriorityList() {
		return Lists.reverse(priorityList());
	}

	// Testing
	public void extendedPpiInfoVerification(String merchantOrderId, String query, MerchantTypes... merchantTypes) {
		Map<String, BigDecimal> map = FundThreadManager.getInstance().walletMapViaWalletId();
		List<String> merchantIdList = new ArrayList<>();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.MERCHANTORDERID, merchantOrderId);
		if (merchantTypes.length != 0) {
			for (MerchantTypes merchantType : merchantTypes) {
				merchantIdList.add(MerchantManager.getInstance().getMerchantDetails(merchantType, MerchantHeaders.ID));
			}
			params.put(DBEnums.MERCHANTLIST, merchantIdList.toString().replaceAll("[\\[\\]]", ""));
		}
		map.remove("0");
		for (String key : map.keySet()) {
			BigDecimal value = map.get(key);
			DBValidation dbValidation = new DBValidation();
			params.put(DBEnums.PPITYPE, key);
			params.put(DBEnums.TXNSUBWALLETAMOUNT, value);
			if (value.compareTo(BigDecimal.ZERO) == 0) {
				dbValidation.isEntryAbsent(query, params);
			} else {
				dbValidation.isEntryPresent(query, params);
			}
		}
	}

	// // New Function to test -- Created by Surili <Using>
	public CheckUserBalance fetchCheckBalanceResponse(String merchantGuid, String token) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalance checkUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(merchantGuid);
		return checkUserBalance;
	}

	public void callCheckUserBalanceInsufficientBalance(String mobileNumber, MerchantTypes mType, String[] issuerId) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		setSubWalletBalanceZero(mobileNumber);
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
		BigDecimal mainWalletAmount = beforeCheckUserBalance.getResponsePojo().getResponse().getPaytmWalletBalance();
		adjustMoneyInMainWallet(mobileNumber, FundThreadManager.getInstance().getMainWalletAmount(), mainWalletAmount);
		if (issuerId.length >= 1 && !issuerId[0].equals("")) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.MERCHANTID,
					MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.NAME));
			params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
			params.put(DBEnums.USERID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber));
			Map<String, Map<String, Object>> merchantSpecificDetail = subwalletIssuer
					.checkBalanceMerchantSpecificDetail(params);
			addIssuerFunds(issuerId, mGuid, merchantSpecificDetail);
		}
		beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
		this.expectedWalletBalMap = fetchSubWalletBalance(beforeCheckUserBalance);
		BigDecimal amount = fetchTotalBalance(beforeCheckUserBalance, BigDecimal.ZERO);
		this.expectedWalletBalMap.put("totalAmount", amount);
	}

	public void callCheckUserBalanceInsufficientBalanceP2P(String payerMobileNumber, String payeeMobileNumber,
			String[] issuerId) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		setSubWalletBalanceZero(payerMobileNumber);
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseP2P();
		BigDecimal mainWalletAmount = beforeCheckUserBalance.getResponsePojo().getResponse().getPaytmWalletBalance();
		adjustMoneyInMainWallet(payerMobileNumber, FundThreadManager.getInstance().getMainWalletAmount(),
				mainWalletAmount);
		if (issuerId.length >= 1 && !issuerId[0].equals("")) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.MERCHANTID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payeeMobileNumber));
			params.put(DBEnums.USERID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payerMobileNumber));
			Map<String, Map<String, Object>> merchantSpecificDetail = subwalletIssuer
					.checkBalanceMerchantSpecificDetailP2P(params);
			addIssuerFundsP2P(issuerId, merchantSpecificDetail);
		}
		beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseP2P();
		this.expectedWalletBalMap = fetchSubWalletBalance(beforeCheckUserBalance);
		BigDecimal amount = fetchTotalBalance(beforeCheckUserBalance, BigDecimal.ZERO);
		this.expectedWalletBalMap.put("totalAmount", amount);
	}

	public void callCheckUserBalanceInsufficientBalance(MerchantTypes mType) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
		this.expectedWalletBalMap = fetchSubWalletBalance(beforeCheckUserBalance);
		BigDecimal amount = fetchTotalBalance(beforeCheckUserBalance, BigDecimal.ZERO);
		this.expectedWalletBalMap.put("totalAmount", amount);

	}

	public void callCheckUserBalanceInsufficientBalanceP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseP2P();
		this.expectedWalletBalMap = fetchSubWalletBalance(beforeCheckUserBalance);
		BigDecimal amount = fetchTotalBalance(beforeCheckUserBalance, BigDecimal.ZERO);
		this.expectedWalletBalMap.put("totalAmount", amount);

	}

	/**
	 * Create Expected Map function for Transactions where balance is not changing
	 * and checkBalance is hit using detailInfo "Yes"
	 */
	public void callCheckUserBalanceInsufficientBalance() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseALL();
		this.expectedWalletBalMap = fetchSubWalletBalance(beforeCheckUserBalance);
		BigDecimal amount = fetchTotalBalance(beforeCheckUserBalance, BigDecimal.ZERO);
		this.expectedWalletBalMap.put("totalAmount", amount);

	}

	// New Function to test -- Created by Surili <Using>
	private void addIssuerFunds(String[] issuerId, String mGuid,
			Map<String, Map<String, Object>> merchantSpecificDetail) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		for (int i = 0; i < issuerId.length; i++) {
			addFunds(merchantSpecificDetail, MerchantTypes.valueOf(issuerId[i]), issuerId.length);
		}
		if (!(FundThreadManager.getInstance().getMainWalletAmount().compareTo(BigDecimal.ZERO) == 0)) {
			CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
			Map<String, BigDecimal> userMap = fetchSubWalletBalance(beforeCheckUserBalance);
			adjustMoneyInMainWallet(UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER),
					FundThreadManager.getInstance().getMainWalletAmount(), userMap.get("0"));
		}
	}

	private void addIssuerFundsP2P(String[] issuerId, Map<String, Map<String, Object>> merchantSpecificDetail) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		for (int i = 0; i < issuerId.length; i++) {
			addFunds(merchantSpecificDetail, MerchantTypes.valueOf(issuerId[i]), issuerId.length);
		}
		if (!(FundThreadManager.getInstance().getMainWalletAmount().compareTo(BigDecimal.ZERO) == 0)) {
			CheckUserBalance beforeCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseP2P();
			Map<String, BigDecimal> userMap = fetchSubWalletBalance(beforeCheckUserBalance);
			adjustMoneyInMainWallet(UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER),
					FundThreadManager.getInstance().getMainWalletAmount(), userMap.get("0"));
		}
	}

	// // New Function to test -- Created by Surili <Using>
	public void callCheckUserBalance(String mobileNumber, MerchantTypes mType, String[] issuerId) {
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		setSubWalletBalanceZero(mobileNumber);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		if (issuerId.length >= 1 && !issuerId[0].equals("")) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.MERCHANTID,
					MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.NAME));
			params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
			params.put(DBEnums.USERID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber));
			Map<String, Map<String, Object>> merchantSpecificDetail = subwalletIssuer
					.checkBalanceMerchantSpecificDetail(params);
			addIssuerFunds(issuerId, mGuid, merchantSpecificDetail);
		} else
			addMoneyInSubwalletMainwallet(mType, mobileNumber);
		createDebitTxnMap(mType);
	}

	public void callCheckUserBalanceP2P(String payerMobileNumber, String payeeMobileNumber, String[] issuerId) {
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		setSubWalletBalanceZero(payerMobileNumber);
		if (issuerId.length >= 1 && !issuerId[0].equals("")) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.MERCHANTID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payeeMobileNumber));
			params.put(DBEnums.USERID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payerMobileNumber));
			Map<String, Map<String, Object>> merchantSpecificDetail = subwalletIssuer
					.checkBalanceMerchantSpecificDetailP2P(params);
			addIssuerFundsP2P(issuerId, merchantSpecificDetail);
		} else
			addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT, payerMobileNumber);
		createDebitTxnMapP2P();
	}

	// // New Function to test -- Created by Surili <Using>
	public void callCheckUserBalance(String mobileNumber, MerchantTypes mType) {
		addMoneyInSubwalletMainwallet(mType, mobileNumber);
		createDebitTxnMap(mType);
	}

	// New Function to test -- Created by Surili <Using>
	public void callCheckUserBalanceLimitReset(String mobileNumber, MerchantTypes mType, String[] issuerId)
			throws IOException {
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		if (issuerId.length >= 1 && !issuerId[0].toString().equals("")) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.MERCHANTID,
					MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.NAME));
			params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
			params.put(DBEnums.USERID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber));
			Map<String, Map<String, Object>> merchantSpecificDetail = subwalletIssuer
					.checkBalanceMerchantSpecificDetail(params);
			resetSubWalletLimits(issuerId, merchantSpecificDetail);
			addIssuerFunds(issuerId, mGuid, merchantSpecificDetail);
		} else
			addMoneyInSubwalletMainwallet(mType, mobileNumber);
		createDebitTxnMap(mType);
	}

	public void callCheckUserBalanceLimitResetP2P(String payerMobileNumber, String payeeMobileNumber, String[] issuerId)
			throws IOException {
		IssuerMappingSubWallet subwalletIssuer = new IssuerMappingSubWallet();
		if (issuerId.length >= 1 && !issuerId[0].toString().equals("")) {
			Map<Object, Object> params = new HashMap<>();
			params.put(DBEnums.MERCHANTID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payeeMobileNumber));
			// params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
			params.put(DBEnums.USERID,
					UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payerMobileNumber));
			Map<String, Map<String, Object>> merchantSpecificDetail = subwalletIssuer
					.checkBalanceMerchantSpecificDetailP2P(params);
			resetSubWalletLimits(issuerId, merchantSpecificDetail);
			addIssuerFundsP2P(issuerId, merchantSpecificDetail);
		} else
			addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT, payerMobileNumber);
		createDebitTxnMapP2P();
	}

	private void resetSubWalletLimits(String[] issuerId, Map<String, Map<String, Object>> merchantSpecificDetail)
			throws IOException {
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		for (int i = 0; i < issuerId.length; i++) {
			userSubwalletConsolidated.resetSubWalletLimit(merchantSpecificDetail,
					MerchantTypes.valueOf(issuerId[i].toString()));
		}
	}

	// // New Function to test -- Created by Surili <Using>
	public void validateCheckUserBalance(MerchantTypes mType) {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mGuid = MerchantManager.getInstance().getMerchantDetails(mType, MerchantHeaders.MERCHANTGUID);
		CheckUserBalance afterCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseMerchant(mGuid);
		Map<String, BigDecimal> actualWalletBalMap = fetchSubWalletBalance(afterCheckUserBalance);
		actualWalletBalMap.put("totalAmount", fetchTotalWalletBalance(afterCheckUserBalance));

		System.out.println("Actual Map is " + actualWalletBalMap);
		System.out.println("Expected Map is " + this.expectedWalletBalMap);

		compareStringMap(actualWalletBalMap, this.expectedWalletBalMap,
				"SubWallet Transaction withdraw balance does not match");
	}

	public void validateCheckUserBalanceP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalance afterCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseP2P();
		Map<String, BigDecimal> actualWalletBalMap = fetchSubWalletBalance(afterCheckUserBalance);
		actualWalletBalMap.put("totalAmount", fetchTotalWalletBalance(afterCheckUserBalance));

		System.out.println("Actual Map is " + actualWalletBalMap);
		System.out.println("Expected Map is " + this.expectedWalletBalMap);

		compareStringMap(actualWalletBalMap, this.expectedWalletBalMap,
				"SubWallet Transaction withdraw balance does not match");
	}

	public void validateCheckUserBalance() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalance afterCheckUserBalance = walletAPIHelpers.fetchCheckBalanceResponseALL();
		Map<String, BigDecimal> actualWalletBalMap = fetchSubWalletBalance(afterCheckUserBalance);
		actualWalletBalMap.put("totalAmount", fetchTotalWalletBalance(afterCheckUserBalance));

		System.out.println("Actual Map is " + actualWalletBalMap);
		System.out.println("Expected Map is " + this.expectedWalletBalMap);

		compareStringMap(actualWalletBalMap, this.expectedWalletBalMap,
				"SubWallet Transaction withdraw balance does not match");
	}

	/**
	 *
	 * Add Funds to the subwallet for the specific user and return the ppiId from response
	 *
	 * @param mobileNumber - User Mobile Number to add Funds
	 * @param userSubWalletType - SubWallet to add Funds to
	 * @param totalAmount - Total Amount to add
	 * @param merchantTypes - Add Funds w.r.t. merchant
	 *
	 * @return - PPIId funds are added into
	 */
	public Integer addSubWalletFundsAndGetPPIId(String mobileNumber, String userSubWalletType, BigDecimal totalAmount, MerchantTypes merchantTypes){
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,MerchantHeaders.MERCHANTGUID);
		String corporateWalletGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,MerchantHeaders.CORPORATEWALLETGUID);
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		AddFundsToSubWallet addFundsToSubWallet = walletAPIHelpers.subWalletAdd(mobileNumber,userSubWalletType,totalAmount,mGuid,corporateWalletGuid);
		int subWalletPPIId = addFundsToSubWallet.getResponsePojo().getResponse().getSubWalletDetails().get(0).getPpiDetailsId();
		return subWalletPPIId;
	}


	/**
	 * Update the expiry_timestamp in the table for ppiId
	 *
	 * @param ppiId - Id from ppi details table
	 */
	public void expireSubWalletByOneHourViaPPIId(Integer ppiId) {
		Map<Object, Object> params = new HashMap<>();
		DBValidation db = new DBValidation();
		String dateTime = CommonHelpers.getInstance().getFutureHour("yyyy-MM-dd HH:mm:ss", -1);
		params.put(DBEnums.TIMESTAMP, dateTime);
		params.put(DBEnums.PPIID, ppiId);
		db.runUpdateQuery(DBQueries.PPIDetails.updateExpiryDateViaId, params);
	}


	/**
	 * Update create and update timestamp in the NSTR table
	 *
	 * @param merchantOrderId - Merchant Order Id for update
	 */
	public void updateNSTRTimeStamp(String merchantOrderId){
		Map<Object, Object> params = new HashMap<>();
		DBValidation db = new DBValidation();
		String dateTime = CommonHelpers.getInstance().getFututeDate("yyyy-MM-dd HH:mm:ss", -2);
		params.put(DBEnums.TIMESTAMP,dateTime);
		params.put(DBEnums.MERCHANTORDERID,merchantOrderId);
		db.runUpdateQuery(DBQueries.NSTR.updateTimeStampNSTR, params);
	}


	/**
	 *
	 * @return - CashBack amount if property is set than cashback is 0.15 else 0
	 */
	private BigDecimal getCashBackType(){
		DBValidation dbValidation = new DBValidation();
		List<Map<String, Object>> result = dbValidation.runDbQuery(DBQueries.getSendMoneyCashBackType,null,"log4j");
		if(result!=null&&result.size()!=0){
			System.out.println(result.get(0).get("property_value"));
			if (result.get(0).get("property_value").toString().equalsIgnoreCase("1")) {
				return BigDecimal.valueOf(0.15);
			}
		}
		return BigDecimal.ZERO;
	}


	/**
	 * Function to calculate the commission + service tax
	 * @param merchantGuid - MerchantGuid
	 * @param industryType - Industry type like retalil or pvt_ltd.
	 */
	public BigDecimal calculateCommisionValue(String merchantGuid, String industryType) {
		BigDecimal totalAmount = FundThreadManager.getInstance().getTotalAmount();
		BigDecimal commissionAmount = BigDecimal.ZERO;
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.MERCHANTGUID, merchantGuid);
		List<Map<String, Object>> tempMap = dbValidation.runDbQuery(DBQueries.getExemptServiceTax, params,
				"log4j");
		if(tempMap.size()!=0) {
			Boolean exemptServiceTax = (Boolean) tempMap.get(0).get("exempt_service_tax");
			params.put(DBEnums.INDUSTRYTYPE, industryType);
			params.put(DBEnums.TOTALAMOUNT, totalAmount);
			List<Map<String, Object>> resultMap = dbValidation.runDbQuery(DBQueries.getCommisionBand, params,
					"log4j");
			if (resultMap.size() == 0) {
				return commissionAmount;
			}
			Map<String, Object> commissionResult = resultMap.get(0);
			for (Map<String, Object> result : resultMap) {
				if (totalAmount.compareTo(new BigDecimal(result.get("start_slab").toString())) >= 0 && totalAmount.compareTo(new BigDecimal(result.get("end_slab").toString())) <= 0) {
					commissionResult = result;
				}
			}
			String modeType = (String) commissionResult.get("commission_mode");
			BigDecimal serviceTax;
// fethcing commsion value based on commision mode
			if (modeType.equals("ABSOLUTE")) {
				commissionAmount = (BigDecimal) commissionResult.get("flat_value");

			} else if (modeType.equals("PERCENTAGE")) {

				BigDecimal dbPerRange = (BigDecimal) commissionResult.get("percentage_value");
				BigDecimal b = new BigDecimal("100");
				BigDecimal c = dbPerRange.divide(b);
				commissionAmount = c.multiply(totalAmount);

			} else if (modeType.equals("BOTH")) {
				BigDecimal dbPerRange = (BigDecimal) commissionResult.get("percentage_value");
				BigDecimal b = new BigDecimal("100");
				BigDecimal c = dbPerRange.divide(b);
				commissionAmount = (c.multiply(totalAmount)).add((BigDecimal) resultMap.get(0).get("flat_value"));

			} else {
				commissionAmount = BigDecimal.ZERO;
			}
// To Calculate Service TAX or GST
			if (exemptServiceTax.equals("1")) {

				serviceTax = BigDecimal.ZERO;

			} else {

				serviceTax = (commissionAmount.multiply(new BigDecimal(Constants.SystemVariables.seviceTaxPercentage))).divide(new BigDecimal(100));

			}
			commissionAmount = commissionAmount.add(serviceTax);
			FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
			if ((Boolean) tempMap.get(0).get("offline_post_convenience"))
				FundThreadManager.getInstance().withPostConvMerchant(true);
		}
		return commissionAmount;
	}



}
