package com.paytm.utils;

import java.util.Set;

import com.paytm.framework.utils.RedisUtil;
import com.paytm.constants.LocalConfig;

import redis.clients.jedis.Jedis;

public class Redis {

	public Jedis createWalletRedisConnection() {
		Jedis jedis = RedisUtil.getInstance().getConnection(LocalConfig.WALLET_REDIS_URL);
		return jedis;
	}

	public void setRedisKey(Jedis jedis, String key, String value) {
		jedis.set(key, value);
	}

	public void deleteRedisKey(Jedis jedis, String pattern) {
		Set<String> keys = jedis.keys(pattern);
		for (String key : keys) {
			jedis.del(key);
		}
	}

	public String  getRedisKey(Jedis jedis, String key) {
		String redisKeys = jedis.get(key);
		return redisKeys;
	}

}
