package com.paytm.utils.jsonProcessor;

public interface Json {
    public <T> String toJSon(T t) throws Exception;

    public <T> T fromJson(String jSon,
                          Class<T> clazz) throws Exception;
}
