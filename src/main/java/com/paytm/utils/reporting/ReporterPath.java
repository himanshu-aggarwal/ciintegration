package com.paytm.utils.reporting;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.paytm.constants.Constants;

public class ReporterPath {
	private static File reportFolderPath;
	public static String reportFileName;
	public static String log4jReportFileName;

	public static void getReportPath() {
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-HH-mm-ss");
		Date date = new Date();
		String folderName = System.getProperty("reportFolderName") + "-" + dateFormat.format(date);
		if (Constants.SystemVariables.JENKINSRUN)
			reportFolderPath = new File(System.getProperty("jenkinsReportLoc") + folderName);
		else
			reportFolderPath = new File(System.getProperty("localReportLoc") + folderName);
		reportFolderPath.mkdir();
		File file = new File(reportFolderPath, System.getProperty("reportName"));
		reportFileName = file.getAbsolutePath();
	}

	public static void moveFileToDirectory() {
		File file = new File(reportFolderPath, "log4Jreport.log");
		log4jReportFileName = file.getAbsolutePath();
		File sourceFile = new File(System.getProperty("user.dir"), "log4j.log");
		if (sourceFile.exists()) {
			File movedFile = new File(log4jReportFileName);
			if (movedFile.exists())
				movedFile.delete();
			sourceFile.renameTo(new File(log4jReportFileName));
		}
	}

}
