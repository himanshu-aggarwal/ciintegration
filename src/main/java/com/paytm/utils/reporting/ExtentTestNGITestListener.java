package com.paytm.utils.reporting;

import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtentTestNGITestListener
		implements ITestListener, ISuiteListener, IRetryAnalyzer, IAnnotationTransformer {
	int counter = 0;
	int retryLimit = 1;

	@Override
	public void onStart(ITestContext context) {
	}

	@Override
	public void onFinish(ITestContext context) {
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		ExtentManager.reportPassed();
	}

	@Override
	public void onTestFailure(ITestResult result) {
		ExtentManager.reportFailed(result);
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		ExtentManager.reportSkipped(result);
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	@Override
	public void onTestStart(ITestResult result) {
	}

	@Override
	public void onStart(ISuite suite) {
		ReporterPath.getReportPath();
		ExtentManager.extentReportingInstance(ReporterPath.reportFileName);
	}

	@Override
	public void onFinish(ISuite suite) {

	}

	@Override
	public boolean retry(ITestResult result) {
		if (counter < retryLimit) {
			ExtentManager.extent.removeTest(ExtentManager.test.get());
			counter++;
			return true;
		}
		return false;
	}

/*
	@Override
	public void alter(List<XmlSuite> suites) {
		for (XmlSuite xmlSuite : suites) {

			// Setting up tests/runModules
			UpdateRunModules: {
				List<String> runModule = new ArrayList<>(
						Arrays.asList(Constants.SystemVariables.TESTNG_RUNMODULE.split(",", -1)));
				if (!runModule.get(0).equalsIgnoreCase("ALL")) {
					List<String> copyRunModule = new ArrayList<>(runModule);
					List<XmlTest> copyXmlTests = new ArrayList<>(xmlSuite.getTests());
					xmlSuite.getTests().clear();
					for (String rModule : copyRunModule) {
						Boolean checker = false;
						for (XmlTest xmlTest : copyXmlTests) {
							if (xmlTest.getName().equalsIgnoreCase(rModule)) {
								xmlSuite.getTests().add(xmlTest);
								checker = true;
								break;
							}
						}
						if (!checker) {
							throw new RuntimeException("RunModule entered is missing: " + rModule);
						}
					}
				}
			}

			UpdateRunApisAndTest: {
				List<XmlTest> xmlTests = xmlSuite.getTests();
				for (XmlTest xmlTest : xmlTests) {
					// Setting groups/runMode
					List<String> copytest = new ArrayList<>();
					copytest.add(Constants.SystemVariables.TESTNG_RUNMODE.toLowerCase());
					xmlTest.setIncludedGroups(copytest);

					// Setting runApis/Classes
					List<String> runApis = new ArrayList<>(
							Arrays.asList(Constants.SystemVariables.TESTNG_RUNAPIS.split(",", -1)));
					if (!runApis.get(0).equalsIgnoreCase("ALL")) {
						List<String> copyRunApis = new ArrayList<>(runApis);
						List<XmlClass> copyXmlClasses = new ArrayList<>(xmlTest.getXmlClasses());
						xmlTest.getXmlClasses().clear();
						for (String rApis : copyRunApis) {
							Boolean checker = false;
							for (XmlClass xmlClass : copyXmlClasses) {
								String[] xClassName = xmlClass.getName().split("\\.", -1);
								if (xClassName[xClassName.length - 1].equalsIgnoreCase(rApis)) {
									xmlTest.getXmlClasses().add(xmlClass);
									checker = true;
									break;
								}
							}
							if (!checker) {
								throw new RuntimeException("RunModule entered is missing: " + rApis);
							}
						}
					}
				}
			}

			UpdateParallelRun: {
				// Setting parallelRun Mode
				String parallelRunMode = Constants.SystemVariables.TESTNG_PARALLELRUNMODE;
				System.out.println(parallelRunMode);
				if (parallelRunMode.equalsIgnoreCase("Methods")) {
					xmlSuite.setParallel(XmlSuite.ParallelMode.METHODS);
				} else if (parallelRunMode.equalsIgnoreCase("Classes")) {
					xmlSuite.setParallel(XmlSuite.ParallelMode.CLASSES);
				} else if (parallelRunMode.equalsIgnoreCase("Tests")) {
					xmlSuite.setParallel(XmlSuite.ParallelMode.TESTS);
				} else if (parallelRunMode.equalsIgnoreCase("Instances")) {
					xmlSuite.setParallel(XmlSuite.ParallelMode.INSTANCES);
				} else {
					xmlSuite.setParallel(XmlSuite.ParallelMode.NONE);
				}

				// Setting thread Count
				System.out.println(Constants.SystemVariables.TESTNG_PARALLELTHREADCOUNT);
				if (!xmlSuite.getParallel().toString().equalsIgnoreCase("None")) {
					xmlSuite.setThreadCount(Integer.parseInt(Constants.SystemVariables.TESTNG_PARALLELTHREADCOUNT));
				}
			}
		}
	}
*/
	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		annotation.setRetryAnalyzer(ExtentTestNGITestListener.class);

	}

}
