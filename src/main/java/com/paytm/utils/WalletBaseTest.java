package com.paytm.utils;

import com.paytm.constants.Constants;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.framework.core.ExtentManager;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.UserTypeExt;
import com.paytm.utils.reporting.ReporterPath;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WalletBaseTest {

	private static Map<String, List<String>> testCaseDump;
	private static int ifailTestCase = 0;
	private static int ipassTestCase = 0;

	@BeforeSuite(alwaysRun = true)
	public void fetchTestData() {
		UserManager.getInstance().readUserData();
		MerchantManager.getInstance().readMerchantData();
		if (Constants.SystemVariables.testCaseListRequired) {
			testCaseDump = new LinkedHashMap<>();
		}
	}

	@AfterSuite(alwaysRun = true)
	public void saveTestData() {
		UserManager.getInstance().writeUserData();
		if (Constants.SystemVariables.testCaseListRequired) {
			writeDataInTxn();
		}
		ReporterPath.moveFileToDirectory();
		System.out.println("Report Location is " + ReporterPath.reportFileName);
	}

	@BeforeMethod(alwaysRun = true)
	public void createExtentTest(Method method, ITestContext context, Object[] testArgs) {
		String categoryName = null;
		if (method.getDeclaringClass().isAnnotationPresent(ClassNameExt.class)) {
			ClassNameExt classNameExt = method.getDeclaringClass().getAnnotation(ClassNameExt.class);
			categoryName = classNameExt.ClassNameProvided();
		} else
			categoryName = method.getDeclaringClass().getSimpleName();
		String methodName;
		String userType = "";
		if(testArgs.length!=0) {
			userType = "[" + testArgs[0] + "]";
		}
		methodName = method.getName() + userType;
		ExtentManager.createTest(methodName, categoryName);
		Test test = method.getAnnotation(Test.class);
		ExtentManager.logInfo(test.description()+userType, "extent");
		if (Constants.SystemVariables.testCaseListRequired) {
			addTestCaseDump(method.getDeclaringClass().getSimpleName(), test.description()+userType);
		}
	}

	@BeforeTest(alwaysRun = true)
	public void beforeTest() {
	}

	@AfterMethod(alwaysRun = true)
	public void releaseUser(ITestResult result) {
		FundThreadManager.removeInstance();
		UserManager.getInstance().releaseUser();
		ExtentManager.flush();
		if (result.getStatus() == ITestResult.SUCCESS)
			ipassTestCase++;
		else if (result.getStatus() == ITestResult.FAILURE)
			ifailTestCase++;

		System.out.println("Total Pass TestCases till now : " + ipassTestCase);
		System.out.println("Total Fail TestCases till now : " + ifailTestCase);
	}

	public synchronized void addTestCaseDump(String className, String testCaseDesc) {
		List<String> testCases = new LinkedList<>();
		for (String keys : WalletBaseTest.testCaseDump.keySet()) {
			if (keys.equalsIgnoreCase(className)) {
				testCases = WalletBaseTest.testCaseDump.get(className);
				break;
			}
		}
		testCases.add(testCaseDesc);
		WalletBaseTest.testCaseDump.put(className, testCases);
	}

	@DataProvider(name = "fetchUserType")
	public Object[][] fetchUserType(Method m) {
		UserTypeExt classNameExt = m.getAnnotation(UserTypeExt.class);
		Object[][] data = null;
		String[] name = classNameExt.UserNameProvided().getName();
		data = new Object[name.length][1];
		for (int i = 0; i < name.length; i++) {
			data[i][0] = name[i];
		}
		return data;
	}

	public void writeDataInTxn() {
		try {
			PrintWriter writer = new PrintWriter("TestCaseDescriptions.txt", "UTF-8");
			for (String apiName : WalletBaseTest.testCaseDump.keySet()) {
				writer.println(apiName);
				for (String testCase : WalletBaseTest.testCaseDump.get(apiName)) {
					writer.println(testCase);
				}
				writer.println("---------------------------------");
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

}
