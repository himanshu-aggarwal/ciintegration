package com.paytm.utils;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class OfflinePaymentTokenAnalyzer {

    public static Map<String, Long> map = new HashMap<String, Long>();
    public static int otp;
    public static String offlineTokenOtp;
    //public static List<String> offlineTokenList;
    private static byte[] hmac_sha(String crypto, byte[] keyBytes,
                                   byte[] text) {
        try {
            Mac hmac;
            hmac = Mac.getInstance(crypto);
            SecretKeySpec macKey =
                    new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (GeneralSecurityException gse) {
            throw new UndeclaredThrowableException(gse);
        }
    }


    private static byte[] hexStr2Bytes(String hex) {
        // Adding one byte to get the right conversion
        // Values starting with "0" can be converted
        byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();

        // Copy all the REAL bytes, not the "first"
        byte[] ret = new byte[bArray.length - 1];
        for (int i = 0; i < ret.length; i++)
            ret[i] = bArray[i + 1];
        return ret;
    }

    private static final int[] DIGITS_POWER
            // 0  1   2    3     4      5       6        7         8
            = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};


    private static int generateTOTP(String key,
                                    String time,
                                    String returnDigits,
                                    String crypto) {
        int codeDigits = Integer.decode(returnDigits).intValue();
        // Using the counter
        // First 8 bytes are for the movingFactor
        // Compliant with base RFC 4226 (HOTP)
        while (time.length() < 16)
            time = "0" + time;

        // Get the HEX in a Byte[]
        byte[] msg = hexStr2Bytes(time);
        byte[] k = hexStr2Bytes(key);
        byte[] hash = hmac_sha(crypto, k, msg);

        // put selected bytes into result int
        int offset = hash[hash.length - 1] & 0xf;

        int binary =
                ((hash[offset] & 0x7f) << 24) |
                        ((hash[offset + 1] & 0xff) << 16) |
                        ((hash[offset + 2] & 0xff) << 8) |
                        (hash[offset + 3] & 0xff);

        int otp = binary % DIGITS_POWER[codeDigits];

        // System.out.println("Real OTP " + otp);
        // return getAlphaNumericString(otp).toUpperCase();
        String result=Integer.toString(otp);
        while (result.length() < codeDigits) {
            result = "0" + result;
        }
        return Integer.parseInt(result);


    }

    private static String sha256(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static int getOTP(String token, long counterInSec, int noOfDigit, Date date) {
        String seed = null;  //token string converted to sha256 hash first then will use as seed
        try {
            seed = sha256(token);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //Date date = new Date();

        long T = (date.getTime()) / TimeUnit.SECONDS.toMillis(counterInSec);

        String steps = Long.toHexString(T).toUpperCase();
        int otp;
        otp = generateTOTP(seed, steps, String.valueOf(noOfDigit), "HmacSHA256");
        map.put(token, System.currentTimeMillis());
        if (Integer.toString(otp).length() == 6){
            return otp;
        }
        else {
            otp = generateTOTP(seed, steps, String.valueOf(noOfDigit), "HmacSHA256");
            if (Integer.toString(otp).length() == 6){
            }
        }
        return otp;
    }

    private static String getAlphaNumericStringOTP(int value) {
        String s = "ABCDE" + Integer.toString(value, 16);
        //  System.out.println("sc  "+ s);
        return s.substring(s.length() - 5);

        //System.out.println("yoyo  "+RandomStringUtils.random(5,s));
        //return  null;
    }

    public static boolean validateOfflineTokenOTP(String token, String otpCode,int counterInSec, Date date, int windowSize){
        String seed = null;
        try {
            seed = sha256(token);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        long T = (date.getTime()) / TimeUnit.SECONDS.toMillis(counterInSec);

        for (int i = -((windowSize - 1) / 2); i <= windowSize / 2; ++i) {
            String step = Long.toHexString(T + i).toUpperCase();

            int otp = generateTOTP(seed, step, "6", "HmacSHA256");
            String hash = getAlphaNumericStringOTP(otp).toUpperCase();
            if (hash.equals(otpCode)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {

        //System.out.print("OTP " + getOTP("2ceeb526-1c9e-4f06-8b54-5251711ec0c9", 30, 6));
        Date d=new Date();
        int otp=getOTP("53235904-2d7e-4625-8bef-87e97b3f4000", 60, 6, d);
        System.out.println(otp);
        String offlineTokenOtp=getAlphaNumericStringOTP(otp).toUpperCase();
        System.out.println(" offline +++ "+offlineTokenOtp);

        System.out.println(validateOfflineTokenOTP("adcee245-b122-41f5-b2c5-8721c5f04700" ," offline"+offlineTokenOtp, 60,d, 5));
    }
    public static String getNewOfflineTokenOtp(String ssoToken){
        Long lastGeneratedOTPTimeInMilliseconds = map.get(ssoToken);
        if (lastGeneratedOTPTimeInMilliseconds != null) {
            Integer currentTime = org.joda.time.LocalDateTime.now().getSecondOfMinute();
            Integer diff = 65 - currentTime;
            if (diff < 61) {
                try {
                    Thread.sleep(diff*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        Date d=new Date();
        otp =getOTP(ssoToken, 60, 6, d);
        return getAlphaNumericStringOTP(otp).toUpperCase();

    }

    public static String getOfflineTokenOtp(String ssoToken){
        Date d=new Date();
        otp =getOTP(ssoToken, 60, 6, d);
        String offlineTokenOtp=getAlphaNumericStringOTP(otp).toUpperCase();
        return offlineTokenOtp;
    }
    
    public static String getOfflineTotp(String token) {
    	Date d=new Date();
    	return String.valueOf(getOTP(token, 60, 6, d));
    }
    
    public static String getNewOfflineTotp(String token) {
    	Long lastGeneratedOTPTimeInMilliseconds = map.get(token);
        if (lastGeneratedOTPTimeInMilliseconds != null) {
            Integer currentTime = org.joda.time.LocalDateTime.now().getSecondOfMinute();
            Integer diff = 65 - currentTime;
            if (diff < 61) {
                try {
                    Thread.sleep(diff*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        Date d=new Date();
        otp =getOTP(token, 60, 6, d);
        return String.valueOf(otp);
    }

}
