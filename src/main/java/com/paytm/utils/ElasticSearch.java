package com.paytm.utils;

import java.net.UnknownHostException;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;

import com.paytm.framework.utils.ElasticUtil;
import com.paytm.constants.LocalConfig;

public class ElasticSearch {


    public static Client getElasticConnection () throws UnknownHostException{
        TransportClient transportClient = ElasticUtil.getInstance().getClient(LocalConfig.WALLET_ES_HOST_CONNECTION_URL);
        return transportClient;
    }

    public void elasticQueryBuilder() throws UnknownHostException{
       // getElasticConnection().prepareSearch().setQuery().execute().actionGet();
    }

   public static class elasticQueryBuilder{

        public String indeses;
        public String documents;

        public elasticQueryBuilder setIndeses(String indeses){

            this.indeses=indeses;
            return this;
        }

    }
}
