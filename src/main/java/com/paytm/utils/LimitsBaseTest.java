package com.paytm.utils;

import com.paytm.apiPojo.LimitConfig;
import com.paytm.constants.DBQueries;
import com.paytm.enums.LimitEnums;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class LimitsBaseTest extends WalletBaseTest {

    String orignalLimitsQuery;

    @BeforeTest(alwaysRun = true)
    public void limitsBeforeTest() {
        DBValidation dbValidations = new DBValidation();
        List<Map<String, Object>> systemLimits = dbValidations.runDbQuery(DBQueries.getSystemDefinedLimits,null,"log4j");
        this.orignalLimitsQuery = createSystemLimitsInsertQuery(systemLimits);
        systemLimits = updateLimits(systemLimits);
        String increaseLimitsQuery = createSystemLimitsInsertQuery(systemLimits);
        truncateSystemLimitConfig();
        updateDB(increaseLimitsQuery);
    }



    @AfterTest(alwaysRun = true)
    public void limitsAfterTest() {
        truncateSystemLimitConfig();
        updateDB(this.orignalLimitsQuery);
        System.out.println("done reverse");
    }


    private void updateDB(String query){
        DBValidation dbValidation = new DBValidation();
        dbValidation.runUpdateQuery(query,null);
        WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
        walletAPIHelpers.cacheRefresh();
    }

    private void truncateSystemLimitConfig(){
        DBValidation dbValidation = new DBValidation();
        dbValidation.runUpdateQuery(DBQueries.truncateSystemDefinedLimitsTable,null);
    }

    private String createSystemLimitsInsertQuery(List<Map<String, Object>> systemLimits){
        String insertQuery = "INSERT INTO wallet.limit_config (limit_name,trust_factor,wallet_rbi_type,pan_verified,config,create_timestamp,update_timestamp) VALUES ";
        int index=1;
        for(Map<String,Object> limit : systemLimits){
            String limitAdd="('"+limit.get("limit_name")+"','"+limit.get("trust_factor")+"','"+limit.get("wallet_rbi_type")+"','"+limit.get("pan_verified")+"','"+limit.get("config")+"','"+limit.get("create_timestamp")+"','"+limit.get("update_timestamp")+"')";
            insertQuery=insertQuery+limitAdd;
            if(index!=systemLimits.size()){
                insertQuery=insertQuery+",";
            }
            index++;
        }
        System.out.println(insertQuery);
        return insertQuery;
    }

    private List<Map<String, Object>> updateLimits(List<Map<String, Object>> systemLimits){
        BigDecimal amount = new BigDecimal(1000000);
        int count = 10000;
        for(Map<String,Object> limit : systemLimits) {
            LimitEnums.LimitTypeEnums limitName = LimitEnums.LimitTypeEnums.valueOf(limit.get("limit_name").toString());
            String config = limit.get("config").toString();
            LimitConfig limitConfig = new LimitConfig();
            try {
                limitConfig = limitConfig.createPojo(config);
            }catch(IOException ie){
                ie.printStackTrace();
            }
            if(limitName.equals(LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT)||limitName.equals(LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT)||limitName.equals(LimitEnums.LimitTypeEnums.TXN_TYPE_DURATION_P2P)||limitName.equals(LimitEnums.LimitTypeEnums.TXN_TYPE_DURATION_AMC)||limitName.equals(LimitEnums.LimitTypeEnums.CUMULATIVE_P2P_P2B)||limitName.equals(LimitEnums.LimitTypeEnums.TXN_TYPE_DURATION_ADD_MONEY_GV)){
                if(limitConfig.getPeriodLimits().getDayPeriodLimit()!=null){
                    limitConfig.getPeriodLimits().getDayPeriodLimit().setAmount(amount);
                    limitConfig.getPeriodLimits().getDayPeriodLimit().setCount(count);
                }
                if(limitConfig.getPeriodLimits().getYearPeriodLimit()!=null){
                    limitConfig.getPeriodLimits().getYearPeriodLimit().setAmount(amount);
                    limitConfig.getPeriodLimits().getYearPeriodLimit().setCount(count);
                }
                if(limitConfig.getPeriodLimits().getMonthPeriodLimit()!=null){
                    limitConfig.getPeriodLimits().getMonthPeriodLimit().setAmount(amount);
                    limitConfig.getPeriodLimits().getMonthPeriodLimit().setCount(count);
                }
            }else if(limitName.equals(LimitEnums.LimitTypeEnums.RBI_BALANCE)||limitName.equals(LimitEnums.LimitTypeEnums.WALLET_AGGREGATE_BALANCE_LIMIT)){
                limitConfig.setBalance(amount);
            }else{
                throw new RuntimeException("Invalid limit found");
            }
            try {
                config = JacksonJsonImpl.getInstance().toJSon(limitConfig);
                config = config.replaceAll("\\r|\\n","").replaceAll(" ","");
            }catch(IOException ie){
                ie.printStackTrace();
            }
            limit.put("config",config);
        }
        return systemLimits;
    }

}
