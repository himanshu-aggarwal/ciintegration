package com.paytm.utils;

import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.Constants;
import com.paytm.enums.DBEnums;
import com.paytm.framework.core.ExtentManager;
import com.paytm.framework.utils.DatabaseUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DBValidation {

	private List<String> getMatches(String stringToSearch) {
		Pattern p = Pattern.compile("(\\$)(.*?)(\\$)"); // the pattern to search
		Matcher matcher = p.matcher(stringToSearch);
		List<String> listMatches = new ArrayList<String>();
		while (matcher.find())
			listMatches.add(matcher.group(2));
		return listMatches;
	}

	public String fetchQuery(String dbQuery, Map<Object, Object> params) {
		String query = null;
		try {
			List<String> listMatches = getMatches(dbQuery);
			query = dbQuery;
			for (int j = 0; j < listMatches.size(); j++) {
				String key = listMatches.get(j);
				if (params.containsKey(DBEnums.valueOf(key))) {
					query = query.replaceAll("\\b"+key+"\\b", params.get(DBEnums.valueOf(key)).toString()).replace("$", "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return query;
	}

	public List<Map<String, Object>> runWalletTrailDbQuery(String dbQuery, Map<Object, Object> params,
			String printInReport) {
		String query = fetchQuery(dbQuery, params);
		ExtentManager.logInfo("DB Query executed : " + query, printInReport);
		List<Map<String, Object>> result = DatabaseUtil.getInstance().executeSelectQuery(Constants.DBConnectionURL.WALLET_TRAIL_DB_CONNECTION_URL,
				query);
		return result;
	}

	public List<Map<String, Object>> runDbQuery(String dbQuery, Map<Object, Object> params, String printInReport) {
		String query = dbQuery;
		List<Map<String, Object>> result = null;
		try {
			if (params != null) {
				query = fetchQuery(dbQuery, params);
			}
			System.out.println("Query is " + query);
			ExtentManager.logInfo("DB Query executed : " + query, printInReport);
			result = DatabaseUtil.getInstance().executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL,
					query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void runUpdateQuery(String dbQuery, Map<Object, Object> params) {
		String query = fetchQuery(dbQuery, params);
		System.out.println("Query is " + query);
		
		DatabaseUtil.getInstance().executeUpdateQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, query);
	}

	public List<Map<String, Object>> isEntryPresent(String dbQuery, Map<Object, Object> params) {
		List<Map<String, Object>> result = runDbQuery(dbQuery, params,"extent");
		AssertionValidations.verifyAssertEqual(result.size() > 0, true);
		return result;
	}

	public void isEntryAbsent(String dbQuery, Map<Object, Object> params) {
		List<Map<String, Object>> result = runDbQuery(dbQuery, params,"extent");
		AssertionValidations.verifyAssertEqual(result.size() == 0, true);
	}

}
