package com.paytm.assertions;

import java.math.BigDecimal;

import org.assertj.core.api.Assertions;

public class AssertionValidations {

	public static void verifyAssertEqual(String actual, String expected) throws AssertionError {
		Assertions.assertThat(actual).isEqualTo(expected);
	}

	public static void verifyAssertEqual(Object actual, Object expected) throws AssertionError {
		Assertions.assertThat(actual).isEqualTo(expected);
	}

	public static void verifyAssertEqual(boolean actual, boolean expected) throws AssertionError {
		Assertions.assertThat(actual).isEqualTo(expected);
	}

	public static void verifyAssertEqual(BigDecimal actual, BigDecimal expected) throws AssertionError {
		Assertions.assertThat(actual).isEqualTo(expected);
	}

	public static void verifyAssertEqual(Integer actual, Integer expected) throws AssertionError {
		Assertions.assertThat(actual).isEqualTo(expected);
	}

	public static void verifyAssertEqual(int actual, int expected) throws AssertionError {
		Assertions.assertThat(actual).isEqualTo(expected);
	}

	public static void verifyAssertNotNull(String expected) throws AssertionError {
		Assertions.assertThat(expected).isNotNull();
	}

	public static void verifyAssertNotNull(Object expected) throws AssertionError {
		Assertions.assertThat(expected).isNotNull();
	}

	public static void verifyAssertNull(String expected) throws AssertionError {
		Assertions.assertThat(expected).isNull();
	}

	public static void verifyAssertFail(String message) throws AssertionError {
		Assertions.fail(message);
	}

}
