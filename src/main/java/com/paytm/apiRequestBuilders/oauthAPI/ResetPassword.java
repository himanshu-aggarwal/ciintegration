package com.paytm.apiRequestBuilders.oauthAPI;

import java.io.IOException;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.oauthAPI.ResetPasswordRequest;
import com.paytm.apiPojo.oauthAPI.ResetPasswordResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class ResetPassword extends APIInterface {

	public static final String defaultRequest = "{  \"request\": {\"body\": {  \"userId\": \"216810000000383501981\",  \"newPassword\": \"$2a$10$z3iAIMrr3NUJRSlhn7kh1O.r6NzxzC9vV4Qm2YcZ0DYMEQWul2Qpa\",  \"securityId\": \"sidd260e28555384f59a35a710285bb9314ejfatqtnayqz3zwuo6Amcv3bif92c\",  \"envInfo\": {\"terminalType\": \"WEB\",\"websiteLanguage\": \"en_US\",\"clientIp\": \"219.65.43.2\",\"osType\": \"Linux\",\"extendInfo\": \"{\\\"userAgent\\\":\\\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36\\\"}\"  },  \"auditInfo\": {\"actionReason\": \"RESET_PASSWORD\"  },  \"actorContext\": {\"actorId\": \"216810000000236553130\",\"actorType\": \"USER\"  }},\"head\": {  \"version\": \"1.2\",  \"function\": \"alipayplus.user.security.resetPassword\",  \"clientId\": \"2016030715243903536806\",  \"reqTime\": \"2017-08-23T19:49:26+05:30\",  \"reqMsgId\": \"00001503497966915\",  \"clientSecret\": \"ifUJTbL6DnHwYU2xumZ9EEOmCm75wub5\"}  },  \"signature\": \"CGu5U2iaSjvRg4EtBqTjjsf846+m/2VdTyvvoh/EmFZJ70opAlccNqokCpGwon9njaQp3j4kVlKagPeqjRCmNT+Zk2BfAMpVYPfo4bidbFUkqxhR7JlZX9M8+BEgornWFYa9IoKJai2EekY6AF3G+j9D7s8BIzzUUMw0M+r7eO++ERqdrBRX3yejEsvW+DYKNuejNekM1oOVF4+gTTRMXxagR2tq3PVceVD6mDl2xO7D+Zag+RsN9O5yR4EjwZ42Xb/LmSQevO4/VwcZWK3J8gHExehzYFB5Gq2Uvj7B/x+1wlp02JtYN+Iq2Ykq5im0LFSeXUMatlrPOeUEFdC3iQ==\"}";

	private ResetPasswordRequest resetPasswordRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private ResetPasswordResponse resetPasswordResponse;
	private Response response;
	private Boolean captureExtent = true;
	private String request;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public ResetPassword(String request) {
		setMethod(MethodType.POST);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.setBaseUri(LocalConfig.ALIPAY_OAUTH_URL);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.RESETPASSWORD);
		this.request = request;
		if (this.request != "") {
			try {
				resetPasswordRequest = JacksonJsonImpl.getInstance().fromJson(this.request, ResetPasswordRequest.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public ResetPasswordRequest getRequestPojo() {
		return resetPasswordRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if (this.request != "")
				this.request = JacksonJsonImpl.getInstance().toJSon(resetPasswordRequest);
			System.out.println("Reset Password request is " + this.request);
			requestSpecBuilder.setBody(this.request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "log4j");
				ExtentManager.logInfo("Method is " + "POST", "log4j");
				ExtentManager.logInfo("Url is " + Constants.AuthAPIresource.RESETPASSWORD, "log4j");
				ExtentManager.logInfo("Response : " + response.asString(), "log4j");
			}
			resetPasswordResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					ResetPasswordResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public ResetPasswordResponse getResponsePojo() {
		return resetPasswordResponse;
	}

}
