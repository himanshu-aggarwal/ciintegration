package com.paytm.apiRequestBuilders.oauthAPI;

import java.io.IOException;
import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.oauthAPI.QueryUserInfoRequest;
import com.paytm.apiPojo.oauthAPI.QueryUserInfoResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class QueryUserInfo extends APIInterface {

	public static final String defaultRequest = "{  \"request\": {\"body\": {  \"loginIdInfo\": {\"loginId\": \"91-7042249719\",\"loginIdType\": \"MOBILE_NO\"  },  \"userQueryType\": \"BY_LOGIN_ID\"},\"head\": {  \"version\": \"1.2\",  \"function\": \"alipayplus.user.profile.queryUserInfo\",  \"clientId\": \"2016030715243903536806\",  \"reqTime\": \"2017-07-18T10:26:58+05:30\",  \"reqMsgId\": \"00001500353818988\",  \"clientSecret\": \"ifUJTbL6DnHwYU2xumZ9EEOmCm75wub5\"}  },  \"signature\": \"NyX2EXFnE/zu80lH0zzrwG4/0IzxpVrXY77fjZsJgMmOeeD5qXHeKuewwUJmxxjXOIz2SR4OP0Dm1eVe8MR9wu5/MmhgX1iFQpOBiCPyjNdXJagFC965b6vlk82jp8t69fmmO5HvJmtKWMOcGmgVx33NCTAbvYxGSJvorQcPqtcsasqSg+b5oPG5kWxapQdbSqcBH2t+glNnMQTESgqZd0YLmvc0ElI0joRQRLvXhAdXnEaFqPgJW5usr9Dlw7WoFx+sUVNKHccK16aIC0t1Vg3NW0Jzdm7/cs6+ygHCYGOpevMrBvvWe9JnrW62uBxHzs86h3R++4I2ttSN7L5bSw==\"}";

	private QueryUserInfoRequest queryUserInfoRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private QueryUserInfoResponse queryUserInfoResponse;
	private Response response;
	private Boolean captureExtent = true;
	private String request;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public QueryUserInfo(String request) {
		setMethod(MethodType.POST);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.setBaseUri(LocalConfig.ALIPAY_OAUTH_URL);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.QUERYUSERINFO);
		this.request = request;
		if (this.request != "") {
			try {
				queryUserInfoRequest = JacksonJsonImpl.getInstance().fromJson(this.request, QueryUserInfoRequest.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public QueryUserInfoRequest getRequestPojo() {
		return queryUserInfoRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if (this.request != "")
				this.request = JacksonJsonImpl.getInstance().toJSon(queryUserInfoRequest);
			System.out.println("QueryUserInfo request is " + this.request);
			requestSpecBuilder.setBody(this.request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "log4j");
				ExtentManager.logInfo("Method is " + "POST", "log4j");
				ExtentManager.logInfo("Url is " + Constants.AuthAPIresource.QUERYUSERINFO, "log4j");
				ExtentManager.logInfo("Response : " + response.asString(), "log4j");
			}
			queryUserInfoResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					QueryUserInfoResponse.class);
			
			System.out.println("QueryUser Response is " + response.asString());
			
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public QueryUserInfoResponse getResponsePojo() {
		return queryUserInfoResponse;
	}

}
