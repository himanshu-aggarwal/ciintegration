package com.paytm.apiRequestBuilders.oauthAPI;

import java.io.IOException;
import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.oauthAPI.CreatePasswordRequest;
import com.paytm.apiPojo.oauthAPI.CreatePasswordResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class CreatePassword extends APIInterface {

	public static final String defaultRequest = "{\"request\":{\"head\":{\"function\": \"alipayplus.user.security.createPassword\",\"clientId\": \"2016030715243903536806\",\"reqTime\": \"2017-09-14T18:36:54+05:30\",\"reqMsgId\": \"00001505394414393\",\"clientSecret\": \"ifUJTbL6DnHwYU2xumZ9EEOmCm75wub5\",\"accessToken\":\"234567a\",\"reserve\":\"{ \\\"attr1\\\":\\\"val1\\\" }\", \"version\": \"1.2\"},\"body\":{    \"userId\":\"216810000000376747330\",    \"password\":\"$2a$10$z3iAIMrr3NUJRSlhn7kh1O.r6NzxzC9vV4Qm2YcZ0DYMEQWul2Qpa\",    \"envInfo\":{ \"sessionId\":\"8EU6mLl5mUpUBgyRFT4v7DjfQ3fcauthcenter\", \"tokenId\":\"a8d359d6-ca3d-4048-9295-bbea5f6715a6\", \"websiteLanguage\":\"en_US\", \"clientIp\":\"10.15.8.189\", \"osType\":\"Windows.PC\", \"orderOsType\":\"IOS\", \"merchantAppVersion\":\"1.0\", \"extendInfo\":\"{\\\"deviceId\\\":\\\"CV19A56370e8a00d54293aab8001e4794\\\"}\"    },    \"auditInfo\":{ \"actionReason\":\"user claim\", \"thirdClientId\":\"2016061912345678900987654321234\"    },    \"actorContext\":{ \"actorId\":\"224234234\", \"actorType\":\"BACK_OFFICE\"    }}   },   \"signature\":\"signature string\"}";

	private CreatePasswordRequest createPasswordRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private CreatePasswordResponse createPasswordResponse;
	private Response response;
	private Boolean captureExtent = true;
	private String request;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public CreatePassword(String request) {
		setMethod(MethodType.POST);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.setBaseUri(LocalConfig.ALIPAY_OAUTH_URL);
		requestSpecBuilder.setBasePath(Constants.AuthAPIresource.CREATEPASSWORD);
		this.request = request;
		if (this.request != "") {
			try {
				createPasswordRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
						CreatePasswordRequest.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public CreatePasswordRequest getRequestPojo() {
		return createPasswordRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if (this.request != "")
				this.request = JacksonJsonImpl.getInstance().toJSon(createPasswordRequest);
			System.out.println("Create Password request is " + this.request);
			requestSpecBuilder.setBody(this.request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "log4j");
				ExtentManager.logInfo("Method is " + "POST", "log4j");
				ExtentManager.logInfo("Url is " + Constants.AuthAPIresource.CREATEPASSWORD, "log4j");
				ExtentManager.logInfo("Response : " + response.asString(), "log4j");
			}
			createPasswordResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					CreatePasswordResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public CreatePasswordResponse getResponsePojo() {
		return createPasswordResponse;
	}

}
