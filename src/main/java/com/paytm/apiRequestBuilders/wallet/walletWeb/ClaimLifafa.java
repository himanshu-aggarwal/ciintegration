package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.ClaimLifafaRequest;
import com.paytm.apiPojo.wallet.walletWeb.ClaimLifafaResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ClaimLifafa extends APIInterface {
    public static final String defaultRequest = "{\"lifafaKey\": \"Dm0WanSnKNjfezDhH5de6Q\"}";

    private ClaimLifafaRequest claimLifafaRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private ClaimLifafaResponse claimLifafaResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public ClaimLifafa(String request, String token) {
        setMethod(MethodType.POST);
        headerMap.put("ssotoken", token);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CLAIMLIFAFA);
        this.request = request;
        if(this.request!="") {
            try {
                claimLifafaRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        ClaimLifafaRequest.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ClaimLifafaRequest getRequestPojo() {
        return claimLifafaRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(claimLifafaRequest);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.CLAIMLIFAFA, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }

            claimLifafaResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    ClaimLifafaResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public ClaimLifafaResponse getResponsePojo() {
        return claimLifafaResponse;
    }
}
