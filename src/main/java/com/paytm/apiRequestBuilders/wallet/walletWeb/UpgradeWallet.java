package com.paytm.apiRequestBuilders.wallet.walletWeb;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.UpgradeWalletRequest;
import com.paytm.apiPojo.wallet.walletWeb.UpgradeWalletResponse;
import com.paytm.constants.Constants;
import com.paytm.constants.LocalConfig;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class UpgradeWallet extends APIInterface {

	public static final String defaultRequest = "{ \"request\": { \"ssoId\":\"YamlReader.RbiBasicUser1.id\", \"idProofVerified\":true, \"walletState\": \"PAYTM_MIN_KYC_EXPIRED\", \"rbiMigration\":false, \"addressProofVerified\":true  }, \"ipAddress\": \"127.0.0.1\", \"platformName\": \"PayTM\", \"operationType\": \"WALLET_UPGRADE\"}";

	private UpgradeWalletRequest upgradeWalletRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private UpgradeWalletResponse upgradeWalletResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String basePath;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public UpgradeWallet(String request, String token) {
		headerMap.put("ssotoken", token);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
		this.basePath = Constants.WALLETAPIresource.UPGRADEWALLET;
		requestSpecBuilder.setBasePath(this.basePath);
		this.request = request;
		if (this.request != "") {
			try {
				upgradeWalletRequest = JacksonJsonImpl.getInstance().fromJson(this.request, UpgradeWalletRequest.class);
				upgradeWalletRequest.setUpdateAmountManager(true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public UpgradeWalletRequest getRequestPojo() {
		return upgradeWalletRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if (this.request != "")
				this.request = JacksonJsonImpl.getInstance().toJSon(upgradeWalletRequest);
			setMethod(BaseApi.MethodType.POST);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + this.basePath, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			upgradeWalletResponse = JacksonJsonImpl.getInstance().responsefromJson(response,
					UpgradeWalletResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public UpgradeWalletResponse getResponsePojo() {
		return upgradeWalletResponse;
	}

}
