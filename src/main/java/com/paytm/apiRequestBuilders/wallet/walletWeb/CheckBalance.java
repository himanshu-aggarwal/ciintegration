package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.CheckBalanceResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CheckBalance extends APIInterface {

	private RequestSpecBuilder requestSpecBuilder;
	private CheckBalanceResponse checkBalanceResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
//	private requestPojo checkBalanceRequest;
	private Boolean captureExtent = true;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public CheckBalance(String token) {
		setMethod(BaseApi.MethodType.POST);
		headerMap.put("ssotoken", token);
		headerMap.put("is_admin", "false");
		headerMap.put("tokentype", "OAUTH");
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.addHeaders(headerMap);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CHECKBALANCE);
	}

/*
	public requestPojo getRequestPojo() {
		return checkBalanceRequest;
	}
*/

	public void createRequestJsonAndExecute() {
		try {
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is NA", "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.CHECKBALANCE, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			checkBalanceResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					CheckBalanceResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public CheckBalanceResponse getResponsePojo() {
		return checkBalanceResponse;
	}

}