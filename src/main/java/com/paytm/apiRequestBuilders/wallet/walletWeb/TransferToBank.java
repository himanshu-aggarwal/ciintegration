package com.paytm.apiRequestBuilders.wallet.walletWeb;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.TransferToBankRequest;
import com.paytm.apiPojo.wallet.walletWeb.TransferToBankResponse;
import com.paytm.constants.Constants;
import com.paytm.constants.LocalConfig;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class TransferToBank extends APIInterface {

	public static final String defaultRequest = "{\"request\": { \"payerGuid\": \"\", \"bankName\":\"ICICI Bank\", \"nickName\":\"Sonal Gaur\", \"bankAccountNo\":\"119884123461\", \"ifscCode\":\"ICICI000\", \"currencyCode\":\"INR\", \"comment\":\"Loan\", \"amount\":\"2\" , \"senderName\":\"Sonal\" , \"accType\":\"10\", \"contactNo\":\"YamlReader.UthUser.mobile\" },\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"P2B_TRANSFER\"}";

	private TransferToBankRequest transferToBankRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private TransferToBankResponse transferToBankResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String basePath;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public TransferToBank(String request, String token) {
		headerMap.put("ssotoken", token);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
		this.basePath = Constants.WALLETAPIresource.TRANSFERTOBANK;
		requestSpecBuilder.setBasePath(this.basePath);
		this.request = request;
		if (this.request != "") {
			try {
				transferToBankRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
						TransferToBankRequest.class);
				transferToBankRequest.setUpdateAmountManager(true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public TransferToBankRequest getRequestPojo() {
		return this.transferToBankRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if (this.request != "")
				this.request = JacksonJsonImpl.getInstance().toJSon(this.transferToBankRequest);
			setMethod(BaseApi.MethodType.POST);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + this.basePath, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			transferToBankResponse = JacksonJsonImpl.getInstance().responsefromJson(response,
					TransferToBankResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public TransferToBankResponse getResponsePojo() {
		return transferToBankResponse;
	}

}
