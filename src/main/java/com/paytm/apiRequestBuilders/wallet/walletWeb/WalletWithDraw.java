package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.WalletWithdrawRequest;
import com.paytm.apiPojo.wallet.walletWeb.WalletWithdrawResponse;
import com.paytm.constants.Constants;
import com.paytm.enums.BasePathEnums;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.helpers.CommonHelpers;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WalletWithDraw extends APIInterface {

    private Response response;    public static final String defaultRequest = "{\"request\": {\"skipRefill\": true,\"totalAmount\": \"10\",\"currencyCode\": \"INR\",\"merchantGuid\": \"\",\"merchantOrderId\": \"1234567\",\"industryType\":\"Retail\",\"posId\" : \"{{$timestamp}}\",\"comment\":\"withdraw transaction\"},\"platformName\": \"PayTM\",\"ipAddress\": \"192.168.40.11\",\"operationType\": \"WITHDRAW_MONEY\"}";
    public static final String subWalletRequest = "{\"request\": {\"skipRefill\": true,\"totalAmount\": \"10\",\"currencyCode\": \"INR\",\"merchantGuid\": \"\",\"industryType\":\"PVT_LTD\",\"merchantOrderId\": \"{{$timestamp}}\",\"pgTxnId\":\"\",\"itemDetails\": [],\"subWalletAmount\":{ \"FOOD\":0, \"TOLL\":0,\"GIFT\":0,\"FUEL\":0, \"CLOSED_LOOP_WALLET\":0, \"CLOSED_LOOP_SUB_WALLET\":0, \"INTERNATIONAL_FUNDS_TRANSFER\":0, \"GIFT_VOUCHER\":0 } },    \"platformName\": \"PayTM\",    \"ipAddress\": \"192.168.40.11\",    \"operationType\": \"WITHDRAW_MONEY\"}";

    private WalletWithdrawRequest walletWithdrawRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private WalletWithdrawResponse walletWithdrawResponse;

    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;
    private String basePath;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

	public WalletWithDraw(String request, String token) {
//		String className = CommonHelpers.getInstance().getCallerClassName(Thread.currentThread().getStackTrace());
		headerMap.put("ssotoken", token);
		requestSpecBuilder = getRequestSpecBuilder();
		// setAPIBasePath(className);
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
		this.basePath = Constants.WALLETAPIresource.WITHDRAWMONEY;
		requestSpecBuilder.setBasePath(this.basePath);
		this.request = request;
		if (this.request != "") {
			try {
				walletWithdrawRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
						WalletWithdrawRequest.class);
				walletWithdrawRequest.setUpdateAmountManager(true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public WalletWithDraw(String request) {
		String className = CommonHelpers.getInstance().getCallerClassName(Thread.currentThread().getStackTrace());
		requestSpecBuilder = getRequestSpecBuilder();
		setAPIBasePath(className);
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
		this.request = request;
		try {
			walletWithdrawRequest = JacksonJsonImpl.getInstance().fromJson(this.request, WalletWithdrawRequest.class);
			walletWithdrawRequest.setUpdateAmountManager(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setToken(String token) {
		this.headerMap.put("ssotoken", token);
		requestSpecBuilder.addHeaders(this.headerMap);
	}

	public void setPhone(String phone) {
		this.headerMap.put("phone", phone);
		requestSpecBuilder.addHeaders(this.headerMap);
	}
	
	public void setCode(String code) {
		this.headerMap.put("code", code);
		requestSpecBuilder.addHeaders(this.headerMap);
	}
	
	public void setOtp(String otp) {
		this.headerMap.put("otp", otp);
		requestSpecBuilder.addHeaders(this.headerMap);
	}
	
	public void setCustId(String custId) {
		this.headerMap.put("custid", custId);
		requestSpecBuilder.addHeaders(this.headerMap);
	}

	public WalletWithdrawRequest getRequestPojo() {
		return walletWithdrawRequest;
	}

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(walletWithdrawRequest);
            setMethod(BaseApi.MethodType.POST);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.WITHDRAWMONEY, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            walletWithdrawResponse = JacksonJsonImpl.getInstance().responsefromJson(response,
                    WalletWithdrawResponse.class);
            walletWithdrawRequest.setUpdateAmountManager(true);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

	public Response getApiResponse() {
		return response;
	}

	public WalletWithdrawResponse getResponsePojo() {
		return walletWithdrawResponse;
	}

	public void setAPIBasePath(String className) {
		// System.out.println(BasePathEnums.WalletWithdrawEnum.valueOf(className).getName());
		this.basePath = BasePathEnums.WalletWithdraw.valueOf(className).getName();
		requestSpecBuilder.setBasePath(this.basePath);

	}

}
