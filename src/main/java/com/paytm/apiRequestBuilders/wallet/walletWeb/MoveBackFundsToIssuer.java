package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.MoveBackFundsToIssuerRequest;
import com.paytm.apiPojo.wallet.walletWeb.MoveBackFundsToIssuerResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;

public class MoveBackFundsToIssuer extends APIInterface {

    public static final String defaultRequest = "{\"request\": {\"subwalletID\": \"9979\",\"mid\": \"C5F5ABFE-3C4F-43DF-9324-1FBD0A3B4AEE\"}}";

    private MoveBackFundsToIssuerRequest moveBackFundsToIssuerRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private MoveBackFundsToIssuerResponse moveBackFundsToIssuerResponse;
    private Response response;
    //private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public MoveBackFundsToIssuer(String request) {
        setMethod(MethodType.POST);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.MOVEBACKFUNDSTOISSUER);
        this.request = request;
        if(this.request!="") {
            try {
                moveBackFundsToIssuerRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        MoveBackFundsToIssuerRequest.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public MoveBackFundsToIssuerRequest getRequestPojo() {
        return moveBackFundsToIssuerRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(moveBackFundsToIssuerRequest);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is NA", "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.MOVEBACKFUNDSTOISSUER, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            moveBackFundsToIssuerResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    MoveBackFundsToIssuerResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public MoveBackFundsToIssuerResponse getResponsePojo() {
        return moveBackFundsToIssuerResponse;
    }

}
