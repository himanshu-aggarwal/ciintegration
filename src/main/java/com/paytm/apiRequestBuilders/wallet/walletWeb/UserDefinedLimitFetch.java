package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitFetchResponse;
import com.paytm.constants.Constants;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.UserHeaders;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UserDefinedLimitFetch extends  APIInterface {

    private RequestSpecBuilder requestSpecBuilder;
    private UserDefinedLimitFetchResponse userDefinedLimitFetchResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public UserDefinedLimitFetch(String phoneNumber) {
        headerMap.put("ssotoken", UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN,phoneNumber));
        setMethod(BaseApi.MethodType.GET);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.FETCHUSERDEFINEDLIMIT);
    }

    public void createRequestJsonAndExecute() {
        try {
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is NA", "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " +LocalConfig.WALLET_HOST+ Constants.WALLETAPIresource.FETCHUSERDEFINEDLIMIT, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            userDefinedLimitFetchResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    UserDefinedLimitFetchResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public UserDefinedLimitFetchResponse getResponsePojo() {
        return userDefinedLimitFetchResponse;
    }



}
