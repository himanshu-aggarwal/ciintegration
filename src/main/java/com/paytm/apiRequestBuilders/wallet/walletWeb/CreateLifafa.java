package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.CreateLifafaRequest;
import com.paytm.apiPojo.wallet.walletWeb.CreateLifafaResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreateLifafa extends APIInterface {
    public static final String defaultRequest = "{\"orderId\":\"\",  \"name\":\"vinay\",  \"message\":\"diwali\",  \"proposedReceiverCount\":1,  \"proposedQuantity\":10,\"unit\":\"INR\",\"themeGuid\":\"60181A1D-7835-11E7-AF88-3440B5D5E770\",\"creatorType\":\"USER\",\"creatorId\":\"1107222923\",\"distributionType\":\"SINGLE\",  \"strategyType\":\"UNIFORM\",\"recipientList\":[],\"gamify\": false,\"category\":\"Wallet\",\"activationTimeStamp\":\"2019-12-10\",\"expiryTimeStamp\" : \"2019-12-15\",\"recipientListType\":\"PHONENO\",\"minQuantity\" : 1,\"merchantLogo\" : \"abc\"}";

    private CreateLifafaRequest createLifafaRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private CreateLifafaResponse createLifafaResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public CreateLifafa(String request, String token) {
        setMethod(MethodType.POST);
        headerMap.put("ssotoken", token);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CREATELIFAFA);
        this.request = request;
        if(this.request!="") {
            try {
                createLifafaRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        CreateLifafaRequest.class);
                createLifafaRequest.setUpdateAmountManager(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public CreateLifafaRequest getRequestPojo() {
        return createLifafaRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(createLifafaRequest);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.CREATELIFAFA, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }

            createLifafaResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    CreateLifafaResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public CreateLifafaResponse getResponsePojo() {
        return createLifafaResponse;
    }
}
