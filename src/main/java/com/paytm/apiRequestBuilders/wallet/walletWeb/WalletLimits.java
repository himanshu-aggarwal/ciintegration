package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.WalletLimitsListResponse;
import com.paytm.apiPojo.wallet.walletWeb.WalletLimitsRequest;
import com.paytm.apiPojo.wallet.walletWeb.WalletLimitsResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WalletLimits extends APIInterface {

    public static final String singleWalletLimits = "{\"request\":{\"ssoId\":\"\",\"walletOperationType\":\"WITHDRAW_MONEY\",\"amount\":\"5\"},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"WALLET_LIMIT\"}";
    public static final String multipleWalletLimits = "{\"request\":{\"ssoId\":\"\",\"walletOperationTypeList\" : [ \"WITHDRAW_MONEY\" ], \"walletOperationTypeBasedAmountList\" : [ 1010 ]},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"WALLET_LIMIT\"}";

    private WalletLimitsRequest walletLimitsRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private WalletLimitsResponse walletLimitsResponse;
    private WalletLimitsListResponse walletLimitsListResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public WalletLimits(String request, String token) {
        headerMap.put("ssotoken", token);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.WALLETLIMITS);
        this.request = request;
        if(this.request!="") {
            try {
                walletLimitsRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        WalletLimitsRequest.class);
                walletLimitsRequest.setUpdateAmountManager(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public WalletLimitsRequest getRequestPojo() {
        return walletLimitsRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(walletLimitsRequest);
            setMethod(BaseApi.MethodType.POST);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.WALLETLIMITS, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            if(walletLimitsRequest.getRequest().getWalletOperationTypeList()==null) {
                walletLimitsResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                        WalletLimitsResponse.class);
            }else{
                walletLimitsListResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                        WalletLimitsListResponse.class);
            }
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public WalletLimitsResponse getResponsePojo() {
        return walletLimitsResponse;
    }

    public WalletLimitsListResponse getListResponsePojo() {
        return walletLimitsListResponse;
    }

}
