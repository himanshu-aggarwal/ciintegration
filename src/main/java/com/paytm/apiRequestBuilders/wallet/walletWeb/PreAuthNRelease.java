package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.PreAuthNReleaseRequest;
import com.paytm.apiPojo.wallet.walletWeb.PreAuthNReleaseResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PreAuthNRelease extends APIInterface {

    public static final String normalPreAuthRequest = "{\"request\":{\"merchantOrderId\":\"\",\"merchantGuid\":\"8914914f-7556-476c-8d50-1741b27babf0\",\"amount\":4,\"currencyCode\":\"INR\",\"operationType\":\"PREAUTH\"},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"PREAUTH_RELEASE\"}";
    public static final String subWalletPreAuthRequest = "{\"request\":{\"merchantOrderId\":\"\",\"merchantGuid\":\"8914914f-7556-476c-8d50-1741b27babf0\",\"amount\":4,\"currencyCode\":\"INR\",\"operationType\":\"PREAUTH\",\"subWalletAmount\":{\"FOOD\":0,\"GIFT_VOUCHER\":0,\"GIFT\":0,\"TOLL\":0,\"FUEL\":0,\"CLOSED_LOOP_WALLET\":0,\"CLOSED_LOOP_SUB_WALLET\":0,\"INTERNATIONAL_FUNDS_TRANSFER\":0,\"CASHBACK\":0 }},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"PREAUTH_RELEASE\"}";

    private PreAuthNReleaseRequest preAuthNReleaseRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private PreAuthNReleaseResponse preAuthNReleaseResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public PreAuthNRelease(String request, String token) {
        headerMap.put("ssotoken", token);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.PREAUTHNRELEASE);
        this.request = request;
        if(this.request!="") {
            try {
                preAuthNReleaseRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        PreAuthNReleaseRequest.class);
                preAuthNReleaseRequest.setUpdateAmountManager(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public PreAuthNReleaseRequest getRequestPojo() {
        return preAuthNReleaseRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(preAuthNReleaseRequest);
            setMethod(BaseApi.MethodType.POST);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.PREAUTHNRELEASE, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            preAuthNReleaseResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    PreAuthNReleaseResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public PreAuthNReleaseResponse getResponsePojo() {
        return preAuthNReleaseResponse;
    }


}
