package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.SendMoneyRequest;
import com.paytm.apiPojo.wallet.walletWeb.SendMoneyResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SendMoney extends APIInterface {

    public static final String defaultRequest = "{\"request\":{\"mode\":\"\",\"payeePhoneNumber\":\"9551110005\",\"amount\":\"20\",\"payeeSsoId\":\"\",\"isToVerify\":\"1\",\"comment\":\"Testing\",\"merchantOrderId\":1827206878,\"payeeEmailId\":\"\",\"currencyCode\":\"INR\",\"isLimitApplicable\":\"1\"},\"metadata\":\"Testing\",\"channel\":\"\",\"ipAddress\":\"127.0.0.1\",\"operationType\":\"P2P_TRANSFER\",\"platformName\":\"PayTM\",\"version\":\"\"}";
    public static final String subWalletRequest = "{\"request\": {\"isToVerify\" : 1,\"isLimitApplicable\" : 1,\"payeeEmailId\":\"\",\"payeePhoneNumber\": \"9953725098\",\"merchantOrderId\" : \"1519816657\",\"amount\": 1,\"currencyCode\": \"INR\",\"comment\":\"Loan1\" ,\"subWalletAmount\":{\"FOOD\":0,\"GIFT\":0,\"TOLL\":0,\"FUEL\":0,\"GIFT_VOUCHER\":0,\"CLOSED_LOOP_WALLET\":0,\"CLOSED_LOOP_SUB_WALLET\":0,\"INTERNATIONAL_FUNDS_TRANSFER\":0,\"CASHBACK\":0}}, \"ipAddress\": \"127.0.0.1\", \"platformName\": \"PayTM\", \"operationType\": \"P2P_TRANSFER\", \"version\":\"2.09\"}";


    private SendMoneyRequest sendMoneyRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private SendMoneyResponse sendMoneyResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public SendMoney(String request,String token) {
        setMethod(BaseApi.MethodType.POST);
        headerMap.put("ssotoken", token);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.SENDMONEY);
        this.request = request;
        if(this.request!="") {
            try {
                sendMoneyRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        SendMoneyRequest.class);
                sendMoneyRequest.setUpdateAmountManager(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public SendMoneyRequest getRequestPojo() {
        return sendMoneyRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(sendMoneyRequest);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Url is " +LocalConfig.WALLET_HOST+ Constants.WALLETAPIresource.SENDMONEY, "extent");
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            sendMoneyResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    SendMoneyResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public SendMoneyResponse getResponsePojo() {
        return sendMoneyResponse;
    }
}
