package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.AddMoneyRequest;
import com.paytm.apiPojo.wallet.walletWeb.AddMoneyResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AddMoney extends APIInterface {

    public static final String defaultRequest = "{\"request\": {\"pgTxnId\": \"PGTxnID12312312\",\"txnAmount\": \"10\",\"txnCurrency\": \"INR\",\"txnStatus\": \"SUCCESS\",\"merchantOrderId\": \"MOI1231321312\",\"merchantGuid\": \"17BFF689-1789-4BA4-8B59-3E990DF7B1A1\",\"pgResponseCode\": \"01\",\"pgResponseMessage\": \"Txn Successful.\",\"bankTxnId\": \"BTxnId1234132\",\"bankName\": \"ICICI\",\"gatewayName\": \"ICICI\",\"paymentMode\": \"CC\",\"binNumber\": null,\"cardType\": null,\"mid\": \"scwpay09224240900570\",\"requestType\": \"WEB\"},\"metadata\": \"INR\",\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"ADD_MONEY_VIA_MERCHANT\"}";

    private AddMoneyRequest addMoneyRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private AddMoneyResponse addMoneyResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public AddMoney(String request, String token) {
        setMethod(BaseApi.MethodType.POST);
        headerMap.put("ssotoken", token);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.ADDMONEY);
        this.request=request;
        if(this.request!="") {
            try {
                addMoneyRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        AddMoneyRequest.class);
                addMoneyRequest.setUpdateAmountManager(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public AddMoneyRequest getRequestPojo() {
        return addMoneyRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(addMoneyRequest);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.ADDMONEY, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }

            addMoneyResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    AddMoneyResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public AddMoneyResponse getResponsePojo() {
        return addMoneyResponse;
    }

}
