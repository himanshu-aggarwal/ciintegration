package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.OTPWithdrawRequest;
import com.paytm.apiPojo.wallet.walletWeb.WalletWithdrawResponse;
import com.paytm.constants.Constants;
import com.paytm.enums.BasePathEnums;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.helpers.CommonHelpers;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ValidateOTPWithdraw extends APIInterface {
	
    public static final String defaultRequest = "{\"request\": {\"skipRefill\": true,\"totalAmount\": \"10\",\"currencyCode\": \"INR\",\"merchantGuid\": \"125fd26c-4d98-11e2-b20c-e89a8ff309ea\",\"merchantOrderId\": \"1234567\",\"industryType\":\"Retail\",\"posId\" : \"{{$timestamp}}\",\"comment\":\"withdraw transaction\"},\"platformName\": \"PayTM\",\"ipAddress\": \"192.168.40.11\",\"operationType\": \"WITHDRAW_MONEY\"}";

	private OTPWithdrawRequest OTPwalletWithdrawRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private WalletWithdrawResponse OTPwalletWithdrawResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;
    private String basePath;
    

	public void setToken(String token) {
		this.headerMap.put("ssotoken", token);
		requestSpecBuilder.addHeaders(this.headerMap);
	}

	public void setPhone(String phone) {
		this.headerMap.put("phone", phone);
		requestSpecBuilder.addHeaders(this.headerMap);
	}
    
    
    public ValidateOTPWithdraw(String request) {
        //  headerMap.put("ssotoken", token);
    	  String className=CommonHelpers.getInstance().getCallerClassName(Thread.currentThread().getStackTrace());
          requestSpecBuilder = getRequestSpecBuilder();
          setAPIBasePath(className);
          requestSpecBuilder.setContentType(ContentType.JSON);
          requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
          this.request = request;
          try {
        	  OTPwalletWithdrawRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
        			  OTPWithdrawRequest.class);
        	//  OTPwalletWithdrawRequest.setUpdateAmountManager(true);
          } catch (IOException e) {
              e.printStackTrace();
          }
      }
	
    public OTPWithdrawRequest getRequestPojo() {
        return OTPwalletWithdrawRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(OTPwalletWithdrawRequest);
            setMethod(BaseApi.MethodType.POST);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + this.basePath, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            OTPwalletWithdrawResponse = JacksonJsonImpl.getInstance().responsefromJson(response,
                    WalletWithdrawResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public WalletWithdrawResponse getResponsePojo() {
        return OTPwalletWithdrawResponse;
    }
    
    public void setAPIBasePath(String className) {
    	this.basePath=BasePathEnums.ValidateOTPWithdraw.valueOf(className).getName();
		requestSpecBuilder.setBasePath(this.basePath);		
	
}

}
