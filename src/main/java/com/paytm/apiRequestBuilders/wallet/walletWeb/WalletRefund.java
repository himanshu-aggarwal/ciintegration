package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.WalletRefundRequest;
import com.paytm.apiPojo.wallet.walletWeb.WalletRefundResponse;
import com.paytm.constants.Constants;
import com.paytm.constants.LocalConfig;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WalletRefund extends APIInterface {

	public static final String defaultRequest = "{\"request\":{\"txnGuid\":\"\",\"pgTxnId\":null,\"amount\":\"\",\"currencyCode\":\"INR\",\"merchantGuid\":\"\",\"merchantOrderId\":\"\",\"itemDetails\":[],\"isCommissionRollback\":\"C\"},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"REFUND_MONEY\"}";
	public static final String subWalletRefundRequest = "{\"request\":{\"txnGuid\":\"\",\"pgTxnId\":null,\"amount\":\"\",\"currencyCode\":\"INR\",\"merchantGuid\":\"\",\"merchantOrderId\":\"\",\"itemDetails\":[],\"isCommissionRollback\":\"C\",\"subWalletAmount\":{\"FOOD\":0,\"GIFT\":0,\"TOLL\":0,\"FUEL\":0,\"CLOSED_LOOP_WALLET\":0,\"CLOSED_LOOP_SUB_WALLET\":0,\"CASHBACK\":0,\"INTERNATIONAL_FUNDS_TRANSFER\":0,\"GIFT_VOUCHER\":0}},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"REFUND_MONEY\"}";
	public static final String defaultRequestWithoutCommissionRollBack = "{\"request\":{\"txnGuid\":\"\",\"pgTxnId\":null,\"amount\":\"\",\"currencyCode\":\"INR\",\"merchantGuid\":\"\",\"merchantOrderId\":\"\",\"itemDetails\":[],\"baseAmount\":\"\",\"commissionRollBackAmount\":\"\"},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"REFUND_MONEY\"}";
	public static final String subWalletRefundRequestWithoutCommissionRollBack = "{\"request\":{\"txnGuid\":\"\",\"pgTxnId\":null,\"amount\":\"\",\"currencyCode\":\"INR\",\"merchantGuid\":\"\",\"merchantOrderId\":\"\",\"itemDetails\":[],\"baseAmount\":\"\",\"commissionRollBackAmount\":\"\",\"subWalletAmount\":{\"FOOD\":0,\"GIFT\":0,\"TOLL\":0,\"FUEL\":0,\"CLOSED_LOOP_WALLET\":0,\"CLOSED_LOOP_SUB_WALLET\":0,\"CASHBACK\":0,\"INTERNATIONAL_FUNDS_TRANSFER\":0,\"GIFT_VOUCHER\":0}},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"REFUND_MONEY\"}";

	private WalletRefundRequest walletRefundRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private WalletRefundResponse walletRefundResponse;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private Response response;
	private String request;

	public WalletRefund(String request, String token) {
		headerMap.put("ssotoken", token);
		setMethod(BaseApi.MethodType.POST);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.WALLETREFUND);
		this.request = request;
		if(this.request!="") {
			try {
				walletRefundRequest = JacksonJsonImpl.getInstance().fromJson(this.request, WalletRefundRequest.class);
				FundThreadManager.removeInstance();
				walletRefundRequest.setUpdateAmountManager(true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public WalletRefundRequest getRequestPojo() {
		return walletRefundRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				this.request = JacksonJsonImpl.getInstance().toJSon(walletRefundRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.WALLETREFUND, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			walletRefundResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					WalletRefundResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public WalletRefundResponse getResponsePojo() {
		return walletRefundResponse;
	}
}
