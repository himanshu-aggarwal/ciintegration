package com.paytm.apiRequestBuilders.wallet.walletWeb;

import java.util.HashMap;
import java.util.Map;
import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.constants.Constants;
import com.paytm.constants.LocalConfig;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class CacheRefresh extends APIInterface {

	public static final String defaultRequest = "{\"request\": { \"cacheName\":\"ALL\" }, \"ipAddress\": \"127.0.0.1\", \"platformName\": \"PayTM\", \"operationType\": \"REQUEST_MONEY\" }";
	private RequestSpecBuilder requestSpecBuilder;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public CacheRefresh(String request, String token) {
		setMethod(BaseApi.MethodType.POST);
		headerMap.put("ssotoken", token);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CACHEREFRESH);
		this.request = request;
	}

	public void createRequestJsonAndExecute() {
		requestSpecBuilder.setBody(request);
		response = execute();
		if (this.captureExtent) {
			ExtentManager.logInfo("Request is " + request, "extent");
			ExtentManager.logInfo("Header is " + headerMap, "extent");
			ExtentManager.logInfo("Method is " + "POST", "extent");
			ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.ADDMONEY, "extent");
			ExtentManager.logInfo("Response : " + response.asString(), "extent");
		}
	}

	public Response getApiResponse() {
		return response;
	}

	@Override
	public responsePojo getResponsePojo() {
		return null;
	}

}
