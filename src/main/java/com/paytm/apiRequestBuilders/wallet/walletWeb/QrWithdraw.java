package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.QrWithdrawRequest;
import com.paytm.apiPojo.wallet.walletWeb.QrWithdrawResponse;
import com.paytm.apiPojo.wallet.walletWeb.WalletWithdrawRequest;
import com.paytm.apiPojo.wallet.walletWeb.WalletWithdrawResponse;
import com.paytm.constants.Constants;
import com.paytm.constants.LocalConfig;
import com.paytm.enums.BasePathEnums;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.helpers.CommonHelpers;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class QrWithdraw extends APIInterface {

    private Response response;
    public static final String defaultRequest = "{ \"request\":{ \"totalAmount\":\"\", \"currencyCode\":\"INR\", \"merchantGuid\":\"\", \"industryType\":\"PVT_LTD\", \"sourceId\":\"1\", \"sourceName\":\"Goverdhan Chowk\", \"destinationId\":\"17\", \"destinationName\":\"Raiya Chowk\", \"busType\":\"AC\", \"routeId\":1, \"uniqueReferenceLabel\":\"UTS\", \"uniqueReferenceValue\":\"UTS-79660\", \"routeName\":\"BRTS\", \"paxDetails\":[ { \"quantity\":\"\", \"pax\":\"\" } ] }, \"platformName\":\"PayTM\", \"ipAddress\":\"192.168.1.100\", \"channel\":\"WEB\", \"version\":\"1.0\", \"operationType\":\"WITHDRAW_MONEY\" }";

    private QrWithdrawRequest qrWithdrawRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private QrWithdrawResponse qrWithdrawResponse;

    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;
    private String basePath;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public QrWithdraw(String request,String token) {
        String className=CommonHelpers.getInstance().getCallerClassName(Thread.currentThread().getStackTrace());
        headerMap.put("ssotoken", token);
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.QRWITHDRAW);
        this.request = request;
        if(this.request!="") {
            try {
                qrWithdrawRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        QrWithdrawRequest.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public QrWithdrawRequest getRequestPojo() {
        return qrWithdrawRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(qrWithdrawRequest);
            setMethod(BaseApi.MethodType.POST);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.WITHDRAWMONEY, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            qrWithdrawResponse = JacksonJsonImpl.getInstance().responsefromJson(response,
                    QrWithdrawResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public QrWithdrawResponse getResponsePojo() {
        return qrWithdrawResponse;
    }

}


