package com.paytm.apiRequestBuilders.wallet.walletWeb;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitSetRequest;
import com.paytm.apiPojo.wallet.walletWeb.UserDefinedLimitSetResponse;
import com.paytm.constants.Constants;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.UserHeaders;
import com.paytm.framework.api.BaseApi;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserDefinedLimitSet extends APIInterface {

    public static final String normalUDLSetRequest = "{\"request\" : {\"configList\" : {\"WALLET_TO_WALLET_TRANSFER_LIMIT\" : {\"status\" : 1,\"periodLimits\" :  {\"dayPeriodLimit\": {\"amount\":1000000,\"count\":2000},\"monthPeriodLimit\": {\"amount\":1000000,\"count\":2000}}},\"WALLET_TO_BANK_TRANSFER_LIMIT\" : {\"status\" : 1,\"periodLimits\" :  {\"dayPeriodLimit\": {\"amount\":1000000,\"count\":2000},\"monthPeriodLimit\": {\"amount\":1000000,\"count\":2000}}},\"PURCHASE_LIMIT\" : {\"status\" : 1,\"periodLimits\" :  {\"dayPeriodLimit\": {\"amount\":1000000,\"count\":2000},\"monthPeriodLimit\": {\"amount\":1000000,\"count\":2000}}}}}}";
    public static final String allFalseUDLSetRequest = "{\"request\" : {\"configList\" : {\"WALLET_TO_WALLET_TRANSFER_LIMIT\" : {\"status\" : 0},\"WALLET_TO_BANK_TRANSFER_LIMIT\" : {\"status\" : 0},\"PURCHASE_LIMIT\" : {\"status\" : 0}}}}";

    private UserDefinedLimitSetRequest userDefinedLimitSetRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private UserDefinedLimitSetResponse userDefinedLimitSetResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }


    public UserDefinedLimitSet(String request) {
        this.request = request;
        try {
            userDefinedLimitSetRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                    UserDefinedLimitSetRequest.class);
            System.out.println(userDefinedLimitSetRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public UserDefinedLimitSet(String request, String phoneNumber) {
        setMethod(BaseApi.MethodType.POST);
        headerMap.put("ssotoken", UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN,phoneNumber));
        LinkedHashMap<String, String> formParams = new LinkedHashMap<>();
        formParams.put("client","android");
        formParams.put("deviceIdentifier","124");
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        requestSpecBuilder.setBaseUri(LocalConfig.WALLET_HOST);
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.SETUSERDEFINEDLIMIT);
        requestSpecBuilder.addQueryParams(formParams);
        this.request = request;
        if(this.request!="") {
            try {
                userDefinedLimitSetRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        UserDefinedLimitSetRequest.class);
                System.out.println(userDefinedLimitSetRequest);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public UserDefinedLimitSetRequest getRequestPojo() {
        return userDefinedLimitSetRequest;
    }

    public String createRequestJsonOnly(){
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(userDefinedLimitSetRequest);
            return this.request;
        }catch(IOException ie){
            ie.printStackTrace();
            return null;
        }
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(userDefinedLimitSetRequest);
            requestSpecBuilder.setBody(this.request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + LocalConfig.WALLET_HOST + Constants.WALLETAPIresource.SETUSERDEFINEDLIMIT, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            userDefinedLimitSetResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                    UserDefinedLimitSetResponse.class);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public UserDefinedLimitSetResponse getResponsePojo() {
        return userDefinedLimitSetResponse;
    }



}
