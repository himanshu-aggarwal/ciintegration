package com.paytm.apiRequestBuilders.wallet.oldService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.oldService.CheckUserBalanceRequest;
import com.paytm.apiPojo.wallet.oldService.CheckUserBalanceResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class CheckUserBalance  extends APIInterface {

	public static final String defaultRequest = "{\"request\":{\"isDetailInfo\":\"yes\"}}";
	public static final String merchantRequest = "{\"request\":{\"merchantGuid\":\"\", \"mid\":\"\"}}";
	public static final String flaggedRequest = "{\"request\": { \"payeePhoneNo\":\"\",\"payeeSsoId\":\"\"}}";
	public static final String addableAmountRequest = "{\"request\": {\"mid\": \"XujCgU92328148871618\",\"isDetailInfo\": \"\",\"computeAddableAmount\": 1,\"isClubSubwalletsRequired\": \"NO\"	}}";


	private CheckUserBalanceRequest checkUserBalanceRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private CheckUserBalanceResponse checkUserBalanceResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public CheckUserBalance(String request, String token) {
		setMethod(MethodType.POST);
		headerMap.put("ssotoken", token);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_OLD_SERVICE_HOST);
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CHECKUSERBALANCE);
		this.request = request;
		if (this.request!="") {
			try {
				checkUserBalanceRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
						CheckUserBalanceRequest.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public CheckUserBalanceRequest getRequestPojo() {
		return checkUserBalanceRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if (this.request!="")
				this.request = JacksonJsonImpl.getInstance().toJSon(checkUserBalanceRequest);
			requestSpecBuilder.setBody(this.request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + LocalConfig.WALLET_OLD_SERVICE_HOST + Constants.WALLETAPIresource.CHECKUSERBALANCE, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			checkUserBalanceResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					CheckUserBalanceResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public CheckUserBalanceResponse getResponsePojo() {
		return checkUserBalanceResponse;
	}

}
