package com.paytm.apiRequestBuilders.wallet.oldService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.oldService.FundCheckRequest;
import com.paytm.apiPojo.wallet.oldService.FundCheckResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class FundCheckBalance extends APIInterface {

	public static final String defaultRequest = "{\"request\":{\"walletGUID\":\"\",\"channelType\":\"BC\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";
	private FundCheckRequest fundCheckBalanceRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private FundCheckResponse fundCheckBalanceResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public FundCheckBalance(String request) {
		setMethod(MethodType.POST);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.setBaseUri(LocalConfig.WALLET_OLD_SERVICE_HOST);
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.FUNDCHECKBALANCE);
		this.request = request;
		if (this.request!="") {
			try {
				fundCheckBalanceRequest = JacksonJsonImpl.getInstance().fromJson(this.request, FundCheckRequest.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public FundCheckRequest getRequestPojo() {
		return fundCheckBalanceRequest;
	}

	public void setToken(String token) {
		headerMap.put("ssotoken", token);
		requestSpecBuilder.addHeaders(headerMap);
	}

	public void setMid(String mid) {
		headerMap.put("mid", mid);
		requestSpecBuilder.addHeaders(headerMap);
	}

	public void createRequestJsonAndExecute() {
		try {
			if (this.request!="")
				this.request = JacksonJsonImpl.getInstance().toJSon(fundCheckBalanceRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + LocalConfig.WALLET_OLD_SERVICE_HOST + Constants.WALLETAPIresource.FUNDCHECKBALANCE, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			fundCheckBalanceResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					FundCheckResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public FundCheckResponse getResponsePojo() {
		return fundCheckBalanceResponse;
	}

}
