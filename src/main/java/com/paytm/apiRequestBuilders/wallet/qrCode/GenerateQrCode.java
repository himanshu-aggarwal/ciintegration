package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.GenerateQrCodeRequest;
import com.paytm.apiPojo.wallet.qrCode.GenerateQrCodeResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GenerateQrCode extends APIInterface{
	
	public static final String defaultRequest = "{\"request\": {\"createRequest\": {\"agentPhoneNo\":\"\",\"agentEmailId\":\"\",\"businessType\":\"P2P\",\"batchSize\": null,\"batchCount\":null},\"mapRequest\": {\"stickerId\": \"\",\"phoneNo\": \"\",\"emailId\": \"\",\"displayName\": \"PGPlusMerchant\",\"category\": \"\",\"subCategory\": \"\",\"tagLine\": null,\"secondaryPhoneNumber\": \"\",\"mappingType\": \"MERCHANT\",\"merchantGuid\": \"\",\"typeOfQrCode\": \"MERCHANT_QR_CODE\",\"sourceId\": \"1234\",\"merchantMid\": \"PGPlus34267938307585\",\"posId\": \"\",\"mappedBy\" : \"\",\"qrCodeId\" : \"\",\"latitude\" : \"\",\"longitude\" : \"\",\"allowedDistanceLimit\":\"3\",\"isDistanceLimitMandatory\": \"\",\"deepLink\":\"\",\"status\":1},\"operationType\" :[\"CREATE\"]},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";

	private GenerateQrCodeRequest generateQrCodeRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private GenerateQrCodeResponse generateQrCodeResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
    private String URL;

	public void setCaptureExtent(Boolean captureExtent) { 
        this.captureExtent = captureExtent;
    }
	
	public GenerateQrCode(String request, String clientId, String hash) {
		setMethod(MethodType.POST);
        if(clientId != null) {
			headerMap.put("clientid", clientId);
		}
		if(hash != null) {
			headerMap.put("hash", hash);
		}
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        if(Constants.SystemVariables.NEWQRMODULE) {
            requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
            URL = LocalConfig.QR_CODE_HOST;
        }else{
            requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
            URL = LocalConfig.WALLET_MERCHANT_HOST;
        }
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.GENERATEQRCODE);
        URL=URL+Constants.WALLETAPIresource.GENERATEQRCODE;
        this.request = request;
        if(this.request!="") {
            try {
                generateQrCodeRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        GenerateQrCodeRequest.class);
            } catch (IOException ie) {
                ie.printStackTrace();
            }
        }
	}
	
	public GenerateQrCodeRequest getRequestPojo() {
        return generateQrCodeRequest;
    }
	
	public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                request = JacksonJsonImpl.getInstance().toJSon(generateQrCodeRequest);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + URL, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            if(getApiResponse().getStatusCode() == 200) {
                generateQrCodeResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                        GenerateQrCodeResponse.class);
            }
        }catch(IOException ie) {
            ie.printStackTrace();
        }
    } 
	
	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}
	
	public Response getApiResponse() {
        return response;
    }

    public GenerateQrCodeResponse getResponsePojo() {
        return generateQrCodeResponse;
    }
}
