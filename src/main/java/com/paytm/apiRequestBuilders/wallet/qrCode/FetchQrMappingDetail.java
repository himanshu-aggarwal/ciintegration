package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.FetchQrMappingDetailRequest;
import com.paytm.apiPojo.wallet.qrCode.FetchQrMappingDetailResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FetchQrMappingDetail extends APIInterface {

	public static final String defaultRequest = "{\"request\": {\"stickerId\":\"\",\"qrCodeId\":\"\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";
	public static final String qrCodeIdRequest = "{\"request\": {\"qrCodeId\":\"\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";
	
	private FetchQrMappingDetailRequest fetchQrMappingDetailRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private FetchQrMappingDetailResponse fetchQrMappingDetailResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String URL;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public FetchQrMappingDetail(String request, String clienId, String hash) {
		setMethod(MethodType.POST);
		if (clienId != null) {
			headerMap.put("clientid", clienId);
		}
		if (hash != null) {
			headerMap.put("hash", hash);
		}
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
			URL = LocalConfig.WALLET_MERCHANT_HOST;
		}
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.FETCHQRMAPPINGDETAIL);
		URL=URL+Constants.WALLETAPIresource.FETCHQRMAPPINGDETAIL;
		this.request = request;
		if(this.request!="") {
			try {
				fetchQrMappingDetailRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
						FetchQrMappingDetailRequest.class);
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}

	public FetchQrMappingDetailRequest getRequestPojo() {
		return fetchQrMappingDetailRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				request = JacksonJsonImpl.getInstance().toJSon(fetchQrMappingDetailRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + URL, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			if (getApiResponse().getStatusCode() != 400) {
				fetchQrMappingDetailResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
						FetchQrMappingDetailResponse.class);
			}
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}

	public Response getApiResponse() {
		return response;
	}

	public FetchQrMappingDetailResponse getResponsePojo() {
		return fetchQrMappingDetailResponse;
	}
}


