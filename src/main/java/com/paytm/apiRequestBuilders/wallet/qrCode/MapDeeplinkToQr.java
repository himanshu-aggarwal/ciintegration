package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.MapDeeplinkToQrRequest;
import com.paytm.apiPojo.wallet.qrCode.MapDeeplinkToQrResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MapDeeplinkToQr extends APIInterface{
	
	public static final String defaultRequest = "{\"request\": {\"deepLink\":\"\",\"merchantMid\":\"\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";
	public static final String requestWithStickerId = "{\"request\": {\"deepLink\":\"\",\"merchantMid\":\"\",\"stickerId\":\"\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";
	public static final String requestWithAdditinalParameter = "{\"request\":{\"deepLink\":\"\",\"merchantMid\":\"\",\"displayName\":\"\",\"status\":\"\",\"posId\":\"\",\"phoneNo\":\"\",\"tagLine\":\"\"},\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"QR_CODE\"}";
	
	private MapDeeplinkToQrRequest mapDeeplinkToQrRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private MapDeeplinkToQrResponse mapDeeplinkToQrResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String URL;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}
	
	public MapDeeplinkToQr(String request, String clientId, String hash, String merchantGuid) {
		setMethod(MethodType.POST);
		if(clientId != null) {
			headerMap.put("clientid", clientId);
		}
		if(hash != null) {
			headerMap.put("hash", hash);
		}
		if(merchantGuid != null) {
			headerMap.put("merchantGuid", merchantGuid);
		}
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
			URL = LocalConfig.WALLET_MERCHANT_HOST;
		}
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.MAPDEEPLINKTOQR);
		URL=URL+Constants.WALLETAPIresource.MAPDEEPLINKTOQR;
        this.request = request;
		if(this.request!="") {
			try {
				mapDeeplinkToQrRequest = JacksonJsonImpl.getInstance().fromJson(this.request, MapDeeplinkToQrRequest.class);
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}
	
	public MapDeeplinkToQrRequest getRequestPojo() {
		return mapDeeplinkToQrRequest;
	}
	
	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				this.request = JacksonJsonImpl.getInstance().toJSon(mapDeeplinkToQrRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + URL, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
			
			if(getApiResponse().getStatusCode() != 400) {
				mapDeeplinkToQrResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(), MapDeeplinkToQrResponse.class);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}
	
	public Response getApiResponse() {
		return response;
	}
	
	public MapDeeplinkToQrResponse getResponsePojo() {
		return mapDeeplinkToQrResponse;
	}
}
