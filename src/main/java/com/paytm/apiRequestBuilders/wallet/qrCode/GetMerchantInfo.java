package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.GetMerchantInfoRequest;
import com.paytm.apiPojo.wallet.qrCode.GetMerchantInfoResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class GetMerchantInfo extends APIInterface {

    public static String normalRequest = "{\"request\": {\"mid\":\"XujCgU92328148871618\",\"merchantGuid\":\"1B94921F-7EB1-4B5C-8D15-D3AAEA46162C\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";


    private GetMerchantInfoRequest getMerchantInfoRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private GetMerchantInfoResponse getMerchantInfoResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;
    private String URL;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public GetMerchantInfo(String request, String clientId, String hash) {
        setMethod(MethodType.POST);
        if(clientId != null) {
            headerMap.put("clientid", clientId);
        }
        if(hash != null) {
            headerMap.put("hash", hash);
        }
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        if(Constants.SystemVariables.NEWQRMODULE) {
            requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
            URL = LocalConfig.QR_CODE_HOST;
        }else{
            requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
            URL = LocalConfig.WALLET_MERCHANT_HOST;
        }
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.GETMERCHANTINFO);
        URL=URL+Constants.WALLETAPIresource.GETMERCHANTINFO;
        this.request = request;
        if(this.request!="") {
            try {
                getMerchantInfoRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        GetMerchantInfoRequest.class);
            } catch (IOException ie) {
                ie.printStackTrace();
            }
        }
    }

    public GetMerchantInfoRequest getRequestPojo() {
        return getMerchantInfoRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(getMerchantInfoRequest);
            requestSpecBuilder.setBody(request);
            response = execute();
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + URL, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            if(getApiResponse().getStatusCode() == 200) {
                getMerchantInfoResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                        GetMerchantInfoResponse.class);
            }
        }catch(IOException ie) {
            ie.printStackTrace();
        }
    }
    
    public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}

    public Response getApiResponse() {
        return response;
    }

    public GetMerchantInfoResponse getResponsePojo() {
        return getMerchantInfoResponse;
    }


}
