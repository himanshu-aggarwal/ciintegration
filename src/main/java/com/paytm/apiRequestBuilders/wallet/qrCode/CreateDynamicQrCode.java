package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.CreateDynamicQrCodeRequest;
import com.paytm.apiPojo.wallet.qrCode.CreateDynamicQrCodeResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreateDynamicQrCode extends  APIInterface {


    public static final String defaultRequest = "{\"request\": {\"batchCount\":\"1\",\"batchSize\": \"1\",\"agentPhoneNo\":\"\",\"businessType\":\"QR_MERCHANT\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";
    
    private CreateDynamicQrCodeRequest createDynamicQrCodeRequest;
    private RequestSpecBuilder requestSpecBuilder;
    private CreateDynamicQrCodeResponse createDynamicQrCodeResponse;
    private Response response;
    private Map<String, String> headerMap = new HashMap<>();
    private Boolean captureExtent = true;
    private String request;
    private String URL;

    public void setCaptureExtent(Boolean captureExtent) {
        this.captureExtent = captureExtent;
    }

    public CreateDynamicQrCode(String request, String clientId, String hash) {
        setMethod(MethodType.POST);
        if(clientId != null) {
			headerMap.put("clientid", clientId);
		}
		if(hash != null) {
			headerMap.put("hash", hash);
		}
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
        if(Constants.SystemVariables.NEWQRMODULE) {
            requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
            URL = LocalConfig.QR_CODE_HOST;
        }else{
            requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
            URL = LocalConfig.WALLET_MERCHANT_HOST;
        }
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CREATEDYNAMICQRCODE);
        this.request = request;
        if(this.request!="") {
            try {
                createDynamicQrCodeRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
                        CreateDynamicQrCodeRequest.class);
            } catch (IOException ie) {
                ie.printStackTrace();
            }
        }
    }

    public CreateDynamicQrCodeRequest getRequestPojo() {
        return createDynamicQrCodeRequest;
    }

    public void createRequestJsonAndExecute() {
        try {
            if(this.request!="")
                this.request = JacksonJsonImpl.getInstance().toJSon(createDynamicQrCodeRequest);
            requestSpecBuilder.setBody(request);
            response = execute(); 
            if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
                ExtentManager.logInfo("Url is " + URL, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
            if(getApiResponse().getStatusCode() == 200) {
                createDynamicQrCodeResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
                        CreateDynamicQrCodeResponse.class);
            }
        }catch(IOException ie) {
            ie.printStackTrace();
        }
    }

    public Response getApiResponse() {
        return response;
    }

    public CreateDynamicQrCodeResponse getResponsePojo() {
        return createDynamicQrCodeResponse;
    }

    public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}
}
