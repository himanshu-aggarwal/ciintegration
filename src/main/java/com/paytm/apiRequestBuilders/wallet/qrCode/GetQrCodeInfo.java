package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.GetQrCodeInfoRequest;
import com.paytm.apiPojo.wallet.qrCode.GetQrCodeInfoResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GetQrCodeInfo extends APIInterface {

	public static final String defaultRequest = "{\"request\": {\"qrCodeId\":\"\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";

	private GetQrCodeInfoRequest getQrCodeInfoRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private GetQrCodeInfoResponse getQrCodeInfoResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String URL;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public GetQrCodeInfo(String request, String ssoToken) {
		setMethod(MethodType.POST);
		if(ssoToken != null) {
			headerMap.put("ssotoken", ssoToken);
		}
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_OLD_SERVICE_HOST);
			URL = LocalConfig.WALLET_OLD_SERVICE_HOST;
		}
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.GETQRCODEINFO);
		URL=URL+Constants.WALLETAPIresource.GETQRCODEINFO;
		this.request = request;
		if(this.request!="") {
			try {
				getQrCodeInfoRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
						GetQrCodeInfoRequest.class);
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}

	public GetQrCodeInfoRequest getRequestPojo() {
		return getQrCodeInfoRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				this.request = JacksonJsonImpl.getInstance().toJSon(getQrCodeInfoRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + URL, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			if (getApiResponse().getStatusCode() != 400) {
				getQrCodeInfoResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
						GetQrCodeInfoResponse.class);
			}
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public Response getApiResponse() {
		return response;
	}

	public GetQrCodeInfoResponse getResponsePojo() {
		return getQrCodeInfoResponse;
	}

	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}
}
