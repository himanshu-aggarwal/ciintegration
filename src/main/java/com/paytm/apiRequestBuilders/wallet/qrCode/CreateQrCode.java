package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.CreateQrCodeRequest;
import com.paytm.apiPojo.wallet.qrCode.CreateQrCodeResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreateQrCode extends APIInterface {

	public static final String defaultRequest = "{\"request\": {\"merchantGuid\": \"\",\"requestType\":\"QR_ORDER\",\"orderDetails\":\"DETTOL\",\"productId\":\"10011234\",\"productDetails\":\"FOOD\",\"orderId\":\"1012\",\"amount\":\"10\",\"merchantName\":\"\",\"mid\":\"\",\"expiryDate\":\"\",\"posId\":\"\",\"qrInfo\" : {\"handle\" : \"upi://pay?pa=nadeem@npci&pn=nadeem%20chinna&mc=0000&tr=4894398cndhcd25&tn=Pay%20to%20mystar%20store&am=2.55&mam=null&cu=INR&url=https://mystar.com/order=000\"},\"imageRequired\":true},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";	
	
	private CreateQrCodeRequest createQrCodeRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private CreateQrCodeResponse createQrCodeResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String URL;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}
	
	public CreateQrCode(String request, String merchantGuid) {
		setMethod(MethodType.POST);
		if (merchantGuid != null) {
			headerMap.put("merchantGuid", merchantGuid);
		}
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
			URL = LocalConfig.WALLET_MERCHANT_HOST;
		}
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CREATEQRCODE);
		this.request = request;
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.V2CREATEQRCODE);
		this.request = request;
		if(this.request!="") {
			try {
				createQrCodeRequest = JacksonJsonImpl.getInstance().fromJson(this.request, CreateQrCodeRequest.class);
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}

	public CreateQrCode(String request, String clientId, String hash) {
		setMethod(MethodType.POST);
		if (clientId != null) {
			headerMap.put("clientid", clientId);
		}
		if (hash != null) {
			headerMap.put("hash", hash);
		}
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
			URL = LocalConfig.WALLET_MERCHANT_HOST;
		}
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.CREATEQRCODE);
		this.request = request;
		if(this.request!="") {
			try {
				createQrCodeRequest = JacksonJsonImpl.getInstance().fromJson(this.request, CreateQrCodeRequest.class);
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}

	public CreateQrCodeRequest getRequestPojo() {
		return createQrCodeRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				request = JacksonJsonImpl.getInstance().toJSon(createQrCodeRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + URL, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			if (getApiResponse().getStatusCode() == 200) {
				createQrCodeResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
						CreateQrCodeResponse.class);
			}
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}

	public Response getApiResponse() {
		return response;
	}

	public CreateQrCodeResponse getResponsePojo() {
		return createQrCodeResponse;
	}
}
