package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.MapDynamicQrCodeRequest;
import com.paytm.apiPojo.wallet.qrCode.MapDynamicQrCodeResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MapDynamicQrCode extends APIInterface{
	
	public static final String defaultRequest = "{\"request\": {\"stickerId\": \"\",\"qrCodeId\": \"\",\"phoneNo\": \"\",\"emailId\": null,\"displayName\": \"Shrawan_Test\",\"category\": null,\"subCategory\": null,\"tagLine\": null,\"secondaryPhoneNumber\": null,\"mappingType\": \"USER\",\"typeOfQrCode\": \"USER_QR_CODE\",\"sourceId\": \"\",\"merchantMid\": null,\"status\":1},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\",\"deviceId\": null,\"metadata\": null,\"channel\": null,\"version\": null,\"mode\": null}";
				
	private MapDynamicQrCodeRequest mapDynamicQrCodeRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private MapDynamicQrCodeResponse mapDynamicQrCodeResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String URL;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public MapDynamicQrCode(String request, String clientId, String hash) {
		setMethod(MethodType.POST);
		if(clientId != null) {
			headerMap.put("clientid", clientId);
		}
		if(hash != null) {
			headerMap.put("hash", hash);
		}
        requestSpecBuilder = getRequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
			URL = LocalConfig.WALLET_MERCHANT_HOST;
		}
        requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.MAPDYNAMICQRCODE);
		URL=URL+Constants.WALLETAPIresource.MAPDYNAMICQRCODE;
        this.request = request;
		if(this.request!="") {
			try {
				mapDynamicQrCodeRequest = JacksonJsonImpl.getInstance().fromJson(this.request, MapDynamicQrCodeRequest.class);
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}
	
	public MapDynamicQrCodeRequest getRequestPojo() {
		return mapDynamicQrCodeRequest;
	}
	
	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				this.request = JacksonJsonImpl.getInstance().toJSon(mapDynamicQrCodeRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + URL, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
			
			if(getApiResponse().getStatusCode() == 200) {
				mapDynamicQrCodeResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(), MapDynamicQrCodeResponse.class);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}
	
	public Response getApiResponse() {
		return response;
	}
	
	public MapDynamicQrCodeResponse getResponsePojo() {
		return mapDynamicQrCodeResponse;
	}

	
}
