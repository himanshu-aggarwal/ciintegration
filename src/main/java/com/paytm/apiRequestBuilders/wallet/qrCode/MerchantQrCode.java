package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.MerchantQrCodeRequest;
import com.paytm.apiPojo.wallet.qrCode.MerchantQrCodeResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MerchantQrCode extends APIInterface{
	
	public static final String defaultRequest = "{\"request\": {\"requestType\":\"QR_MERCHANT\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";

	private MerchantQrCodeRequest merchantQrCodeRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private MerchantQrCodeResponse merchantQrCodeResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String URL;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}

	public MerchantQrCode(String request, String ssoToken) {
		setMethod(MethodType.POST);
		if(ssoToken != null) {
			headerMap.put("ssotoken", ssoToken);
		}
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
			URL = LocalConfig.WALLET_MERCHANT_HOST;
		}
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.MERCHANTQRCODE);
		URL=URL+Constants.WALLETAPIresource.MERCHANTQRCODE;
		this.request = request;
		if(this.request!="") {
			try {
				merchantQrCodeRequest = JacksonJsonImpl.getInstance().fromJson(this.request,
						MerchantQrCodeRequest.class);
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}

	public MerchantQrCodeRequest getRequestPojo() {
		return merchantQrCodeRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				this.request = JacksonJsonImpl.getInstance().toJSon(merchantQrCodeRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
				ExtentManager.logInfo("Request is " + request, "extent");
				ExtentManager.logInfo("Header is " + headerMap, "extent");
				ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + URL, "extent");
				ExtentManager.logInfo("Response : " + response.asString(), "extent");
			}
			if (getApiResponse().getStatusCode() != 400) {
				merchantQrCodeResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
						MerchantQrCodeResponse.class);
			}
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}

	public Response getApiResponse() {
		return response;
	}

	public MerchantQrCodeResponse getResponsePojo() {
		return merchantQrCodeResponse;
	}
}
