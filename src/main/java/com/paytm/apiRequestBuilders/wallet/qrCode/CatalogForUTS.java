package com.paytm.apiRequestBuilders.wallet.qrCode;

import java.io.IOException;
import java.util.Map;

import com.paytm.apiPojo.wallet.qrCode.CatalogForUTSResponse;
import com.paytm.framework.api.BaseApi;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class CatalogForUTS extends BaseApi {

	private CatalogForUTSResponse catalogForUTSResponse;
	private RequestSpecBuilder requestSpecBuilder;
	private Response response;

	public CatalogForUTS(Map<String, String> params) {
		setMethod(MethodType.GET);
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setBaseUri("https://catalog.paytm.com/v1/g/unreserved-ticket");
		requestSpecBuilder.addParams(params);
	}

	public void createRequestJsonAndExecute() {
		try {
			response = execute();
			catalogForUTSResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(),
					CatalogForUTSResponse.class);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	public CatalogForUTSResponse getResponsePojo() {
		return catalogForUTSResponse;
	}
}
