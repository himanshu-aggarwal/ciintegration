package com.paytm.apiRequestBuilders.wallet.qrCode;

import com.paytm.apiPojo.wallet.APIInterface;
import com.paytm.apiPojo.wallet.qrCode.EditQrCodeDetailsRequest;
import com.paytm.apiPojo.wallet.qrCode.EditQrCodeDetailsResponse;
import com.paytm.constants.Constants;
import com.paytm.framework.core.ExtentManager;
import com.paytm.utils.jsonProcessor.JacksonJsonImpl;
import com.paytm.constants.LocalConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditQrCodeDetails extends APIInterface{

	public static final String defaultRequest = "{ \"request\": {\"qrCodeId\":\"\",\"tagLine\":\"\",\"posId\":\"\",\"secondaryPhoneNumber\":\"\",\"sourceName\": \"\",\"sourceId\": \"\",\"deepLink\":\"\",\"activationStatus\" : 1,\"displayName\":\"vijay11\"},\"ipAddress\": \"127.0.0.1\",\"platformName\": \"PayTM\",\"operationType\": \"QR_CODE\"}";
	
	private EditQrCodeDetailsRequest editQrCodeDetailsRequest;
	private RequestSpecBuilder requestSpecBuilder;
	private EditQrCodeDetailsResponse editQrCodeDetailsResponse;
	private Response response;
	private Map<String, String> headerMap = new HashMap<>();
	private Boolean captureExtent = true;
	private String request;
	private String URL;

	public void setCaptureExtent(Boolean captureExtent) {
		this.captureExtent = captureExtent;
	}
	
	public EditQrCodeDetails(String request, String clientId, String hash) {
		setMethod(MethodType.POST);
		if (clientId != null) {
			headerMap.put("clientid", clientId);
		}
		if (hash != null) {
			headerMap.put("hash", hash);
		}
		requestSpecBuilder = getRequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.addHeaders(headerMap);
		if(Constants.SystemVariables.NEWQRMODULE) {
			requestSpecBuilder.setBaseUri(LocalConfig.QR_CODE_HOST);
			URL = LocalConfig.QR_CODE_HOST;
		}else{
			requestSpecBuilder.setBaseUri(LocalConfig.WALLET_MERCHANT_HOST);
			URL = LocalConfig.WALLET_MERCHANT_HOST;
		}
		requestSpecBuilder.setBasePath(Constants.WALLETAPIresource.EDITQRCODEDETAILS);
		URL=URL+Constants.WALLETAPIresource.EDITQRCODEDETAILS;
		this.request = request;
		if (this.request != ""){
			try {
				editQrCodeDetailsRequest = JacksonJsonImpl.getInstance().fromJson(this.request, EditQrCodeDetailsRequest.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public EditQrCodeDetailsRequest getRequestPojo() {
		return editQrCodeDetailsRequest;
	}

	public void createRequestJsonAndExecute() {
		try {
			if(this.request!="")
				request = JacksonJsonImpl.getInstance().toJSon(editQrCodeDetailsRequest);
			requestSpecBuilder.setBody(request);
			response = execute();
			if (this.captureExtent) {
                ExtentManager.logInfo("Request is " + request, "extent");
                ExtentManager.logInfo("Header is " + headerMap, "extent");
                ExtentManager.logInfo("Method is " + "POST", "extent");
				ExtentManager.logInfo("Url is " + URL, "extent");
                ExtentManager.logInfo("Response : " + response.asString(), "extent");
            }
			
			if(getApiResponse().getStatusCode() == 200) {
				editQrCodeDetailsResponse = JacksonJsonImpl.getInstance().fromJson(response.asString(), EditQrCodeDetailsResponse.class);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setQueryParam(String param) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("locale", param);
		requestSpecBuilder.addQueryParams(queryParams);
	}
	
	public Response getApiResponse() {
		return response;
	}
	
	public EditQrCodeDetailsResponse getResponsePojo() {
		return editQrCodeDetailsResponse;
	}
}
