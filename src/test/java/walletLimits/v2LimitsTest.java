package walletLimits;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletLimits;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.constants.UserTypeExt;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.APIStatus;
import com.paytm.enums.DBEnums;
import com.paytm.enums.LimitEnums;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserHeaders;
import com.paytm.enums.UserTypes;
import com.paytm.enums.WalletRbiTypes;
import com.paytm.helpers.BeneficiaryLimitsValidation;
import com.paytm.helpers.LimitAuditInfoValidation;
import com.paytm.helpers.SubWalletHelpers;
import com.paytm.helpers.UserLimitsValidation;
import com.paytm.utils.DBValidation;
import com.paytm.utils.LimitsBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletLimits-V2LimitsAPI")
public class v2LimitsTest extends LimitsBaseTest {

	@Test(description = "No limit breach in case of no limit exceeds with operation type as ADD_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC001_V2Limit_AddMoney_SUCCESS(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		walletLimits.createRequestJsonAndExecute();
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.SUCCESS);
	}

	@Test(description = "Credit Throughput limit breach of year with operation type as ADD_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLEXCEPTPRIMEANDBASIC)
	public void TC002_V2Limit_AddMoney_CreditYearLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT,
					LimitEnums.LimitPeriodEnums.year, amount);
			walletLimits.createRequestJsonAndExecute();
			if (UserTypes.valueOf(userType).equals(UserTypes.PAYTM_ADHAAR_OTP_KYC)
					|| UserTypes.valueOf(userType).equals(UserTypes.PAYTM_MIN_KYC)) {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1001);
			} else {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_3002);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Credit Throughput limit breach of month with operation type as ADD_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLEXCEPTPRIMEANDADHAAROTPKYC)
	public void TC003_V2Limit_AddMoney_CreditMonthLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			if (UserTypes.valueOf(userType).equals(UserTypes.PAYTM_BASIC_WALLET)
					|| UserTypes.valueOf(userType).equals(UserTypes.PAYTM_MIN_KYC)) {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1001);
			} else {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_3002);
			}
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Debit Throughput limit breach of month with operation type as ADD_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLMINKYCWITHPRIMITIVEANDCLOSED)
	public void TC004_V2Limit_AddMoney_DebitMonthLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$ADD,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_2001);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Rbi Balance limit breach of month with operation type as ADD_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC005_V2Limit_AddMoney_RbiBalanceLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.RBI_BALANCE, null, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1000);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.RBI_BALANCE);
		}
	}

	@Test(description = "Wallet Aggregate limit breach of month with operation type as ADD_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC006_V2Limit_AddMoney_WalletAggregateLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.WALLET_AGGREGATE_BALANCE_LIMIT,
					null, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1000);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.WALLET_AGGREGATE_BALANCE_LIMIT);
		}
	}

	@Test(description = "No limit breach in case of no limit exceeds with operation type as WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC007_V2Limit_WithDrawMoney_SUCCESS(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		walletLimits.createRequestJsonAndExecute();
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.SUCCESS);
	}

	@Test(description = "No limit breach in case of no limit exceeds with operation type as WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLMINKYCWITHPRIMITIVEANDCLOSED)
	public void TC008_V2Limit_WithDrawMoney_DebitMonthLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$WITHDRAW,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_2001);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase day amount limit exceeds with operation type as WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC009_V2Limit_WithDrawMoney_PurchaseDayAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitAmount(phone, LimitEnums.LimitPeriodEnums.day,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0014);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase month amount limit exceeds with operation type as WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC010_V2Limit_WithDrawMoney_PurchaseMonthAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitAmount(phone, LimitEnums.LimitPeriodEnums.month,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0016);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase day count limit exceeds with operation type as WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC011_V2Limit_WithDrawMoney_PurchaseDayCountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitCount(phone, LimitEnums.LimitPeriodEnums.day,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0015);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase month count limit exceeds with operation type as WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC012_V2Limit_WithDrawMoney_PurchaseMonthCountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitCount(phone, LimitEnums.LimitPeriodEnums.month,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0017);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "No limit breach in case of no limit exceeds with operation type as WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC013_V2Limit_WithDrawMoney_InsufficientBalance(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().setUpdateAmountManager(false);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		FundThreadManager.getInstance().withTotalAmount(new BigDecimal(5));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		walletLimits.createRequestJsonAndExecute();
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.WLWTL_1001);
	}

	@Test(description = "No limit breach in case of no limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC014_V2Limit_AddnPay_SUCCESS(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		walletLimits.createRequestJsonAndExecute();
		walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.SUCCESS);
	}

	@Test(description = "Credit Throughput limit breach of year with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLEXCEPTPRIMEANDBASIC)
	public void TC015_V2Limit_AddnPay_CreditYearLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT,
					LimitEnums.LimitPeriodEnums.year, amount);
			walletLimits.createRequestJsonAndExecute();
			if (UserTypes.valueOf(userType).equals(UserTypes.PAYTM_ADHAAR_OTP_KYC)
					|| UserTypes.valueOf(userType).equals(UserTypes.PAYTM_MIN_KYC)) {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1001);
			} else {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_3002);
			}
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Credit Throughput limit breach of month with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLEXCEPTPRIMEANDADHAAROTPKYC)
	public void TC016_V2Limit_AddnPay_CreditMonthLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			if (UserTypes.valueOf(userType).equals(UserTypes.PAYTM_ADHAAR_OTP_KYC)
					|| UserTypes.valueOf(userType).equals(UserTypes.PAYTM_MIN_KYC)) {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1001);
			} else {
				walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_3002);
			}
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Debit Throughput limit breach of month for ADD_MONEY only with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLMINKYCWITHPRIMITIVEANDCLOSED)
	public void TC017_V2Limit_AddnPay_DebitMonthLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		FundThreadManager.getInstance().withTotalAmount(amount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$ADD,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.RWL_2001);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Debit Throughput limit breach of month for all with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLMINKYCWITHPRIMITIVEANDCLOSED)
	public void TC018_V2Limit_AddnPay_DebitMonthLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		FundThreadManager.getInstance().withTotalAmount(BigDecimal.ZERO);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$ADD,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.RWL_2001_II);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase day amount limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC019_V2Limit_AddnPay_PurchaseDayAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitAmount(phone, LimitEnums.LimitPeriodEnums.day,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.UDL_0014);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase month amount limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC020_V2Limit_AddnPay_PurchaseMonthAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitAmount(phone, LimitEnums.LimitPeriodEnums.month,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.UDL_0016);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase day count limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC021_V2Limit_AddnPay_PurchaseDayCountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitCount(phone, LimitEnums.LimitPeriodEnums.day,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.UDL_0015);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User Purchase limit breach in case of Purchase month count limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC022_V2Limit_AddnPay_PurchaseMonthCountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitCount(phone, LimitEnums.LimitPeriodEnums.month,
					LimitEnums.LimitTypeEnums.PURCHASE_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.UDL_0017);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "No limit breach in case of no limit exceeds with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC023_V2Limit_P2P_SUCCESS(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(400);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		walletLimits.createRequestJsonAndExecute();
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.SUCCESS);
	}

	@Test(description = "TXN type duration amount Throughput limit breach of month for payer with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC024_V2Limit_P2P_TXNTypeDurationAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(phone, LimitEnums.LimitTypeEnums.TXN_TYPE_DURATION_P2P,
					LimitEnums.LimitPeriodEnums.day, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0004);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.TXN_TYPE_DURATION_P2P);
		}
	}

	@Test(description = "TXN type duration count Throughput limit breach of month for payer with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC025_V2Limit_P2P_TXNTypeDurationCountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemCountLimit(phone, LimitEnums.LimitTypeEnums.TXN_TYPE_DURATION_P2P,
					LimitEnums.LimitPeriodEnums.day);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0002);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(phone, LimitEnums.LimitTypeEnums.TXN_TYPE_DURATION_P2P);
		}
	}

	@Test(description = "Credit Year Throughput limit breach of year for payee with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC026_V2Limit_P2P_PayeeCreditYearLimit(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(payeePhone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT,
					LimitEnums.LimitPeriodEnums.year, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1001);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(payeePhone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Credit Year Throughput limit breach of month for payee with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC027_V2Limit_P2P_PayeeCreditMonthLimit(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(payeePhone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1001);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(payeePhone, LimitEnums.LimitTypeEnums.CREDIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Debit Throughput limit breach of month for payee with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC028_V2Limit_P2P_PayeeDebitMonthLimit(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(payeePhone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT$ADD,
					LimitEnums.LimitPeriodEnums.month, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_2001);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(payeePhone, LimitEnums.LimitTypeEnums.DEBIT_THROUGHPUT_LIMIT);
		}
	}

	@Test(description = "Rbi Balance limit breach of month with for payee operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC029_V2Limit_P2P_PayeeRbiBalanceLimit(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(payeePhone, LimitEnums.LimitTypeEnums.RBI_BALANCE, null, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1000);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(payeePhone, LimitEnums.LimitTypeEnums.RBI_BALANCE);
		}
	}

	@Test(description = "Wallet Aggregate limit breach of month for payee with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC030_V2Limit_P2P_PayeeWalletAggregateLimit(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		LimitAuditInfoValidation limitAuditInfoValidation = new LimitAuditInfoValidation();
		try {
			limitAuditInfoValidation.setSystemLimit(payeePhone,
					LimitEnums.LimitTypeEnums.WALLET_AGGREGATE_BALANCE_LIMIT, null, amount);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_1000);
		} catch (Exception e) {

		} finally {
			limitAuditInfoValidation.resetLimits(payeePhone, LimitEnums.LimitTypeEnums.WALLET_AGGREGATE_BALANCE_LIMIT);
		}
	}

	@Test(description = "User WalletToWallet limit breach in case of Purchase day amount limit exceeds with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC031_V2Limit_P2P_WalletToWalletDayAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitAmount(phone, LimitEnums.LimitPeriodEnums.day,
					LimitEnums.LimitTypeEnums.WALLET_TO_WALLET_TRANSFER_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0006);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User WalletToWallet limit breach in case of Purchase month amount limit exceeds with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC032_V2Limit_P2P_WalletToWalletMonthAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitAmount(phone, LimitEnums.LimitPeriodEnums.month,
					LimitEnums.LimitTypeEnums.WALLET_TO_WALLET_TRANSFER_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0008);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User WalletToWallet limit breach in case of Purchase day count limit exceeds with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC033_V2Limit_P2P_WalletToWalletDayCountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitCount(phone, LimitEnums.LimitPeriodEnums.day,
					LimitEnums.LimitTypeEnums.WALLET_TO_WALLET_TRANSFER_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0007);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "User WalletToWallet limit breach in case of Purchase month count limit exceeds with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC034_V2Limit_P2P_WalletToWalletMonthCountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		UserLimitsValidation userLimitsValidation = new UserLimitsValidation();
		try {
			userLimitsValidation.setUserDefinedLimitCount(phone, LimitEnums.LimitPeriodEnums.month,
					LimitEnums.LimitTypeEnums.WALLET_TO_WALLET_TRANSFER_LIMIT);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.UDL_0009);
		} catch (Exception e) {

		} finally {
			userLimitsValidation.resetLimits(phone);
		}
	}

	@Test(description = "Beneficiary Amount limit breach with operation type as P2P_TRANSFER for non-Beneficiary", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC035_V2Limit_P2P_BeneficiaryAmountLimit(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		BeneficiaryLimitsValidation beneficiaryLimitsValidation = new BeneficiaryLimitsValidation();
		try {
			beneficiaryLimitsValidation.updateBeneficiaryAmountP2P(phone, payeePhone);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.AL_0006);
		} catch (Exception e) {

		} finally {
			beneficiaryLimitsValidation.resetBeneficiaryCounterValue();
		}
	}

	@Test(description = "Beneficiary Amount limit breach with operation type as P2P_TRANSFER for Beneficiary", groups = {
			"regression" })
	public void TC036_V2Limit_P2P_BeneficiaryAmountLimit() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC, UserTypes.BENEFICIARY);
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,
				UserTypes.BENEFICIARY);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		BeneficiaryLimitsValidation beneficiaryLimitsValidation = new BeneficiaryLimitsValidation();
		try {
			beneficiaryLimitsValidation.updateBeneficiaryAmountP2P(phone, payeePhone);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.AL_0001);
		} catch (Exception e) {

		} finally {
			beneficiaryLimitsValidation.resetBeneficiaryCounterValue();
		}
	}

	@Test(description = "Beneficiary Count limit breach with operation type as P2P_TRANSFER for Beneficiary", groups = {
			"regression" })
	public void TC037_V2Limit_P2P_BeneficiaryCountLimit() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC, UserTypes.BENEFICIARY);
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,
				UserTypes.BENEFICIARY);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		BeneficiaryLimitsValidation beneficiaryLimitsValidation = new BeneficiaryLimitsValidation();
		try {
			beneficiaryLimitsValidation.updateBeneficiaryCountP2P(phone, payeePhone);
			walletLimits.createRequestJsonAndExecute();
			walletLimitsAssert(walletLimits, APIStatus.WalletLimits.AL_0002);
		} catch (Exception e) {

		} finally {
			beneficiaryLimitsValidation.resetBeneficiaryCounterValue();
		}
	}

	@Test(description = "Credit limit breach User with operation type as ADD_MONEY ", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC038_V2Limit_AddMoney_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "1");
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	@Test(description = "Credit and Debit limit breach User with operation type as ADD_MONEY ", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC039_V2Limit_AddMoney_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "3");
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("ADD_MONEY");
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	@Test(description = "Debit limit breach User with operation type as WITHDRAW_MONEY ", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC040_V2Limit_WithdrawMoney_FAILURE(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "2");
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	@Test(description = "Credit and Debit limit breach User with operation type as WITHDRAW_MONEY ", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC041_V2Limit_WithdrawMoney_FAILURE(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "3");
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("WITHDRAW_MONEY");
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	@Test(description = "Credit and Debit limit breach in case of no limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC042_V2Limit_AddnPay_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "3");
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.BOTHRWL_0008);
	}

	@Test(description = "Debit limit breach in case of no limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC043_V2Limit_AddnPay_FAILURE(String userType) {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "2");
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT, phone);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.WITHDRAWRWL_0008);
	}

	@Test(description = "Credit limit breach in case of no limit exceeds with operation type as ADD_MONEY,WITHDRAW_MONEY", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC044_V2Limit_AddnPay_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "1");
		BigDecimal amount = new BigDecimal(10);
		List<String> operationTypeList = new LinkedList<>(Arrays.asList("ADD_MONEY", "WITHDRAW_MONEY"));
		List<BigDecimal> operationTypeAmount = new LinkedList<>(Arrays.asList(amount, amount));
		WalletLimits walletLimits = new WalletLimits(WalletLimits.multipleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withWalletOperationTypeList(operationTypeList)
				.withWalletOperationTypeBasedAmountList(operationTypeAmount);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsListAssert(walletLimits, APIStatus.WalletLimitsList.ADDRWL_0008);
	}

	@Test(description = "Debit limit breach for Payer with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC045_V2Limit_P2P_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "2");
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	@Test(description = "Credit and Debit limit breach for Payer with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC046_V2Limit_P2P_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(custId, "3");
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(custId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	@Test(description = "Credit and Debit limit breach for Payee with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC047_V2Limit_P2P_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(payeeCustId, "3");
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(payeeCustId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	@Test(description = "Credit limit breach for Payee with operation type as P2P_TRANSFER", groups = {
			"regression" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLUSERS)
	public void TC048_V2Limit_P2P_FAILURE(String userType) {
		UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String payeePhone = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		userAttributeMapping(payeeCustId, "1");
		BigDecimal amount = new BigDecimal(10);
		WalletLimits walletLimits = new WalletLimits(WalletLimits.singleWalletLimits, token);
		walletLimits.getRequestPojo().getRequest().withSsoId(custId).withAmount(amount)
				.withWalletOperationType("P2P_TRANSFER").withTargetPhoneNo(payeePhone).withTargetCustId(payeeCustId);
		walletLimits.createRequestJsonAndExecute();
		setuserAttributeMappingto0(payeeCustId);
		walletLimitsAssert(walletLimits, APIStatus.WalletLimits.RWL_0008);
	}

	private void userAttributeMapping(String custId, String freezeCounter) {
		Map<Object, Object> params = new HashMap<>();
		DBValidation db = new DBValidation();
		params.put(DBEnums.CUSTID, custId);
		List<Map<String, Object>> attributeMapping = db.runDbQuery(DBQueries.fetchUserAttributeMapping, params,
				"log4j");
		params.put(DBEnums.LIMITFREEZECOUNTER, freezeCounter);
		if (attributeMapping.size() == 0) {
			List<Map<String, Object>> list = db.runDbQuery(DBQueries.fetchUserCustId, params, "log4j");
			String userGuid = list.get(0).get("guid").toString().replaceAll("[\\[\\]]", "");
			params.put(DBEnums.USERGUID, userGuid);
			db.runUpdateQuery(DBQueries.insertUserAttributeMapping, params);
		} else
			db.runUpdateQuery(DBQueries.updateUserAttributeMapping, params);

	}

	private void setuserAttributeMappingto0(String custId) {
		Map<Object, Object> params = new HashMap<>();
		DBValidation db = new DBValidation();
		params.put(DBEnums.CUSTID, custId);
		params.put(DBEnums.LIMITFREEZECOUNTER, "0");
		db.runUpdateQuery(DBQueries.updateUserAttributeMapping, params);
	}

	public void walletLimitsAssert(WalletLimits walletLimits, APIStatus.WalletLimits statusDetails) {
		AssertionValidations.verifyAssertEqual(walletLimits.getApiResponse().getStatusCode(),
				statusDetails.gethttpResponseCode());
		AssertionValidations.verifyAssertEqual(walletLimits.getResponsePojo().getStatus(), statusDetails.getStatus());
		AssertionValidations.verifyAssertEqual(walletLimits.getResponsePojo().getStatusCode(),
				statusDetails.getStatusCode());
		AssertionValidations.verifyAssertEqual(walletLimits.getResponsePojo().getStatusMessage(),
				statusDetails.getStatusMessage());
		if (!statusDetails.equals(APIStatus.WalletLimits.WLWTL_1001)) {
			AssertionValidations.verifyAssertEqual(walletLimits.getResponsePojo().getResponse().getLimitMessage(),
					statusDetails.getLimitMessage());
			AssertionValidations.verifyAssertEqual(walletLimits.getResponsePojo().getResponse().getMessage(),
					statusDetails.getMessage());
		}
	}

	public void walletLimitsListAssert(WalletLimits walletLimits, APIStatus.WalletLimitsList statusDetails) {
		AssertionValidations.verifyAssertEqual(walletLimits.getApiResponse().getStatusCode(),
				statusDetails.gethttpResponseCode());
		AssertionValidations.verifyAssertEqual(walletLimits.getListResponsePojo().getStatus(),
				statusDetails.getStatus());
		AssertionValidations.verifyAssertEqual(walletLimits.getListResponsePojo().getStatusCode(),
				statusDetails.getStatusCode());
		AssertionValidations.verifyAssertEqual(walletLimits.getListResponsePojo().getStatusMessage(),
				statusDetails.getStatusMessage());
		AssertionValidations.verifyAssertEqual(
				walletLimits.getListResponsePojo().getResponse().get(0).getLimitMessage(),
				statusDetails.getFirstLimitMessage());
		AssertionValidations.verifyAssertEqual(walletLimits.getListResponsePojo().getResponse().get(0).getMessage(),
				statusDetails.getFirstMessage());
		AssertionValidations.verifyAssertEqual(
				walletLimits.getListResponsePojo().getResponse().get(1).getLimitMessage(),
				statusDetails.getSecondLimitMessage());
		AssertionValidations.verifyAssertEqual(walletLimits.getListResponsePojo().getResponse().get(1).getMessage(),
				statusDetails.getSecondMessage());
	}

}
