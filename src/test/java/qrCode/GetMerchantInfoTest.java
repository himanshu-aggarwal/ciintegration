package qrCode;

import com.paytm.apiRequestBuilders.wallet.qrCode.GetMerchantInfo;
import com.paytm.datamanager.MerchantManager;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.paytm.constants.Constants;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import static com.paytm.constants.Constants.SystemVariables.CLIENTID;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-GetMerchantInfo")
public class GetMerchantInfoTest extends WalletBaseTest {
    @Test(description = "When Valid Mid is passed whose Qr_Merchant type exists", groups = {"regression", "sanity", "smoke"})
    public void TC001_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(null);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.SUCCESS);
        qrCodeHelpers.getMetchantInfoParameterValidation(getMerchantInfo);
        qrCodeHelpers.getMerchantInfoValidation(getMerchantInfo.getResponsePojo().getResponse().getEncryptedData(), mid);
    }

    @Test(description = "When Valid GUID is passed whose Qr_Merchant type exists", groups = {"regression", "sanity"})
    public void TC002_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.MERCHANTGUID);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        getMerchantInfo.getRequestPojo().getRequest().withMid(null).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.SUCCESS);
        qrCodeHelpers.getMetchantInfoParameterValidation(getMerchantInfo);
        qrCodeHelpers.getMerchantInfoValidation(getMerchantInfo.getResponsePojo().getResponse().getEncryptedData(), mid);
    }

    @Test(description = "When merchant mid having no qr mapped to it is passed in the request", groups = {"regression"})
    public void TC003_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDMERCHANTWITHNOQRCODE, MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(null);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.DQR_0028);
    }

    @Test(description = "When merchant Guid having no qr mapped to it is passed in the request", groups = {"regression"})
    public void TC004_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDMERCHANTWITHNOQRCODE, MerchantHeaders.MERCHANTGUID);
        getMerchantInfo.getRequestPojo().getRequest().withMid(null).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.DQR_0028);
    }

    @Test(description = "When merchant Guid and mid of two different merchants in the Request", groups = {"regression"})
    public void TC005_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDMERCHANTWITHNOQRCODE, MerchantHeaders.MERCHANTGUID);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.QR_1030);
    }

    @Test(description = "When merchant Guid and mid of two different merchants in the Request", groups = {"regression"})
    public void TC006_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        getMerchantInfo.getRequestPojo().getRequest().withMid(null).withMerchantGuid(null);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.QR_1002);
    }

    @Test(description = "When merchant Guid and mid of two different merchants in the Request", groups = {"regression"})
    public void TC007_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDMERCHANTWITHNOQRCODE, MerchantHeaders.MERCHANTGUID);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.QR_1030);
    }

    @Test(description = "When merchant Guid and mid of valid merchant whose QR exists in DB", groups = {"regression", "smoke",
            "sanity"})
    public void TC008_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.MERCHANTGUID);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.SUCCESS);
        qrCodeHelpers.getMetchantInfoParameterValidation(getMerchantInfo);
        qrCodeHelpers.getMerchantInfoValidation(getMerchantInfo.getResponsePojo().getResponse().getEncryptedData(), mid);
    }
    @Test(description = "Case to verify when clientId is not passed in headers", groups = { "regression" })
    public void TC009_ClientIdNotPassedInHeaders() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, null,Constants.SystemVariables.HASH);
        String mid =MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(null);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(getMerchantInfo, APIStatus.GetMerchantInfo.UNAUTHORIZED_ACCESS);
    }

    @Test(description = "Case to verify when HASH is not passed in headers", groups = { "regression","smoke",
            "sanity" })
    public void TC010_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, null);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.MERCHANTGUID);
        String mid =MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(getMerchantInfo, APIStatus.GetMerchantInfo.UNAUTHORIZED_ACCESS);
    }

    @Test(description = "Case to verify when clientId different", groups = { "regression" })
    public void TC011_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest,
                "d02a898248eb4a2d82873fe8116a5f40", Constants.SystemVariables.HASH);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.MERCHANTGUID);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(getMerchantInfo, APIStatus.GetMerchantInfo.UNAUTHORIZED_ACCESS);
    }
    @Test(description = "Case to veirfy when hash is different", groups = { "regression" })
    public void TC012_GetMerchantInfo() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest,
                Constants.SystemVariables.CLIENTID, "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.MERCHANTGUID);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(getMerchantInfo, APIStatus.GetMerchantInfo.UNAUTHORIZED_ACCESS);
    }

    @Test(description = "Case to verify v4/createDynamicQRCode when operationType is blank in request", groups = {
            "regression" })
    public void TC013_OperationTypeBlank() {
        GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest,
                Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        String guid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.MERCHANTGUID);
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().withOperationType("null").getRequest().withMid(mid).withMerchantGuid(guid);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(getMerchantInfo, APIStatus.GetMerchantInfo.BADREQUEST);
    }
    
    @Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC014_InvalidLocaleValue() {
    	GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		getMerchantInfo.setQueryParam("en-IN");
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(null);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(getMerchantInfo, APIStatus.GetMerchantInfo.INTERNALSERVERERROR);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC015_ValidLocaleValue() {
		GetMerchantInfo getMerchantInfo = new GetMerchantInfo(GetMerchantInfo.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		getMerchantInfo.setQueryParam("en_IN");
        String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.NAME);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        getMerchantInfo.getRequestPojo().getRequest().withMid(mid).withMerchantGuid(null);
        getMerchantInfo.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(getMerchantInfo, APIStatus.GetMerchantInfo.SUCCESS);
        qrCodeHelpers.getMetchantInfoParameterValidation(getMerchantInfo);
        qrCodeHelpers.getMerchantInfoValidation(getMerchantInfo.getResponsePojo().getResponse().getEncryptedData(), mid);
	}
}
