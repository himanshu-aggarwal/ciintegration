package qrCode;


import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.paytm.apiRequestBuilders.wallet.qrCode.CreateDynamicQrCode;
import com.paytm.constants.Constants;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.QrCodeHelpers;

//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;

import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-CreateDynamicQrCode")
public class CreateDynamicQrCodeTest extends WalletBaseTest {

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is P2P and all other parameter are valid", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_CreateP2PBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "01");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is QR_MERCHANT and all other parameter are valid", groups = {
			"regression", "smoke", "sanity" })
	public void TC002_CreateQrMerchantBussinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is QR_PRODUCT and all other parameter are valid", groups = {
			"regression" })
	public void TC003_CreateQrProductBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_PRODUCT");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is QR_PRICEDPRODUCT and all other parameter are valid", groups = {
			"regression" })
	public void TC004_CreateQrPricedProductBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_PRICEDPRODUCT");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is QR_ORDER and all other parameter are valid", groups = {
			"regression" })
	public void TC005_CreateQrOrderBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_ORDER");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "04");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is QR_USER and all other parameter are valid", groups = {
			"regression" })
	public void TC006_CreateQrUSerBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_USER");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "01");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is QR_DEBIT and all other parameter are valid", groups = {
			"regression" })
	public void TC007_CreateQrDebitBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_DEBIT");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "01");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when invalid businessType is passed in request", groups = {
			"regression", "sanity" })
	public void TC008_InvalidBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.DQR_0016);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is blank", groups = { "regression" })
	public void TC009_BlankBusinessType() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.DQR_0016);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when null string is passed as businessType", groups = {
			"regression" })
	public void TC010_BusinessTypeNullString() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("null");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.DQR_0016);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when businessType is not passed in parameters", groups = {
			"regression" })
	public void TC011_BusinessTypeNotPassed() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType(null);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.DQR_0016);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when agentPhoneNumber is not registered at oauth end", groups = {
			"regression", })
	public void TC012_UnregisteredPhoneNumber() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.INVALIDUSER);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.DQR_0003);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when agentPhoneNumber starts with 5", groups = {
			"regression", "sanity" })
	public void TC013_PhoneNumberWithFive() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.NUMBERWITH5);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "01");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when agentPhoneNumber parameter not passed in the request", groups = {
			"regression" })
	public void TC014_PhoneNumberNotPassed() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(null);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when agentPhonenumber is blank in request", groups = {
			"regression" })
	public void TC015_PhoneNumberBlank() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo("");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when batchCount is greater than 1", groups = {
			"regression" })
	public void TC016_BatchCountMoreThanOne() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBatchCount(4);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when batchSize is greater than 1", groups = {
			"regression" })
	public void TC017_BatchSizeMoreThanOne() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBatchSize(4)
				.withBatchCount(2);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when batchCount and batchSize is 0", groups = {
			"regression" })
	public void TC018_BatchSizeAndCountZero() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBatchCount(0)
				.withBatchSize(0);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		;
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when batchCount parameter not passed in request", groups = {
			"regression" })
	public void TC019_BatchCountNotPassed() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBatchCount(null);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode, APIStatus.CreateDynamicQRCode.BADREQUEST);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when batchSize parameter not passed in request", groups = {
			"regression" })
	public void TC020_BatchSizeNotPassed() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBatchSize(null);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode, APIStatus.CreateDynamicQRCode.BADREQUEST);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when agentPhonenumber is starts with digit 4", groups = {
			"regression", "sanity" })
	public void TC021_PhoneNumberWithFour() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = "4360758640";
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.DQR_0003);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when plateformName is blank in request", groups = {
			"regression" })
	public void TC022_PlateformNameBlank() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withPlateformName("").getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when plateformName is null in request", groups = {
			"regression" })
	public void TC023_PlateformNameNullString() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withPlateformName("null").getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when plateformName is other than PayTM", groups = {
			"regression" })
	public void TC024_PlateformNameOtherThanPayTm() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withPlateformName("abcd").getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when operationType is blank in request", groups = {
			"regression" })
	public void TC025_OperationTypeBlank() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withOperationType("").getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode, APIStatus.CreateDynamicQRCode.BADREQUEST);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when operationType is null in request", groups = {
			"regression" })
	public void TC026_OperationTypeNullString() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withOperationType("null").getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode, APIStatus.CreateDynamicQRCode.BADREQUEST);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when operationType is other than QR_CODE", groups = {
			"regression" })
	public void TC027_OperationTypeOtherThanQR_CODE() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withOperationType("abcd").getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode, APIStatus.CreateDynamicQRCode.BADREQUEST);
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when operationType parameter not passed in request", groups = {
			"regression" })
	public void TC028_OperationTypeNotPassed() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withOperationType(null).getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify v4/createDynamicQRCode when plateformName parameter not passed in request", groups = {
			"regression" })
	public void TC029_PlateformNameNotPassed() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().withPlateformName(null).getRequest().withAgentPhoneNo(agentPhoneNumber);
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "05");
	}

	@Test(description = "Case to verify when clientId is not passed in headers", groups = { "regression" })
	public void TC030_ClientIdNotPassedInHeaders() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest, null,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode,
				APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when clientId different", groups = { "regression" })
	public void TC031_ClientIdIsDifferent() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				"d02a898248eb4a2d82873fe8116a5f40", Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode,
				APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when hash is not passed in headers", groups = { "regression" })
	public void TC032_HashNotPassedInHeaders() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, null);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode,
				APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to veirfy when hash is different", groups = { "regression" })
	public void TC033_HashIsDifferent() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode,
				APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC034_InvalidLocaleValue() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.setQueryParam("en-IN");
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createDynamicQrCode, APIStatus.CreateDynamicQRCode.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC035_ValidLocaleValue() {
		CreateDynamicQrCode createDynamicQrCode = new CreateDynamicQrCode(CreateDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET);
		createDynamicQrCode.setQueryParam("en_IN");
		createDynamicQrCode.getRequestPojo().getRequest().withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		createDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createDynamicQrCode, APIStatus.CreateDynamicQRCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.dynamicQrCodesParameterValidations(createDynamicQrCode, "01");
	}
}
