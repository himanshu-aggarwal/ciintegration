package qrCode;

import static com.paytm.constants.Constants.SystemVariables.CLIENTID;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.paytm.apiRequestBuilders.wallet.qrCode.AssignQrCode;
import com.paytm.apiRequestBuilders.wallet.qrCode.MerchantQrCode;
import com.paytm.assertions.AssertionValidations;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserHeaders;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.Constants;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import com.paytm.utils.Redis;

import redis.clients.jedis.Jedis;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-MerchantQrCode")
public class MerchantQrCodeTest extends WalletBaseTest {

	@Test(description = "case to verify when requestType is QR_MERCHANT", groups = { "regression", "smoke", "sanity" })
	public void TC001_WhenRequestTypeIsQrMerchant() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1020);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.merchantQrCodeValidation(merchantQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.ID));
	}

	@Test(description = "case to verify when requestType is P2P", groups = { "regression" })
	public void TC002_WhenRequestTypeIsP2P() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType("P2P");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when requestType is QR_PRODUCT", groups = { "regression" })
	public void TC003_WhenRequestTypeIsP2P() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when requestType is QR_PRICEDPRODUCT", groups = { "regression" })
	public void TC004_WhenRequestTypeIsP2P() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when requestType is QR_ORDER", groups = { "regression" })
	public void TC005_WhenRequestTypeIsP2P() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when requestType is QR_USER", groups = { "regression" })
	public void TC006_WhenRequestTypeIsP2P() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType("QR_USER");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when requestType is QR_DEBIT", groups = { "regression" })
	public void TC007_WhenRequestTypeIsP2P() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType("QR_DEBIT");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when requestType is blank in requeste", groups = { "regression" })
	public void TC008_WhenRequestTypeIsBlank() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType("");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when requestType parameter not passed in request", groups = { "regression" })
	public void TC009_WhenRequestTypeNotPassed() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.getRequestPojo().getRequest().withRequestType(null);
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1011);
	}

	@Test(description = "case to verify when token passed in headers is of non SD merchant user", groups = {
			"regression", "sanity" })
	public void TC010_WhenNonSDMErchantTokenIsPassed() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.NONSDMERCHANT);
	}

	@Test(description = "case to verify when invalid ssoToken is passed in headers", groups = { "regression" })
	public void TC011_InvalidSsoTokenIsPassed() {
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest,
				"1a8fe5e1-bc8d-49bf-81bf-3d7cbcc30922");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.INVALIDTOKEN);
	}

	@Test(description = "case to verify when ssoToken field not passed in headers", groups = { "regression" })
	public void TC012_WhenSsoTokenNotPassed() {
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, null);
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.UNAUTHORIZEDACCESS);
	}

	@Test(description = "case to verify when ssoToken field is blank in request", groups = { "regression" })
	public void TC013_WhenSsoTokenFieldIsBlank() {
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, "");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.BADREQUEST);
	}

	@Test(description = "case to verify when no qr code exist for SD merchant", groups = { "regression", "smoke",
			"sanity" })
	public void TC014_WhenNoQrCodeExistsForSDMerchant() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.NOQR, UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.merchantQrCodeValidation(merchantQrCode, MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SDMERCHANTWITHNOQRCODE, MerchantHeaders.ID));
	}

	@Test(description = "case to verify Qr code entry is deleted from redis after fetching merchantQrCode", groups = {
			"regression", "sanity" })
	public void TC015_QrCodeEntryDeletedFromRedis() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1020);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.merchantQrCodeValidation(merchantQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.ID));
		Redis redis = new Redis();
		Jedis jedis = redis.createWalletRedisConnection();
		AssertionValidations.verifyAssertNull(
				redis.getRedisKey(jedis, "DQR_" + merchantQrCode.getResponsePojo().getResponse().getQrCodeId()));
	}

	@Test(description = "case to verify when merchant not present at ump end", groups = { "regression", "sanity" })
	public void TC016_SDMerchantNotAtUmpEnd() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.NOTPRESENTATUMP, UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.NOSDMERCHANTATUMP);
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC017_InvalidLocaleValue() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.setQueryParam("en-IN");
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(assignQrCode, APIStatus.AssignQRCode.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC018_ValidLocaleValue() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		MerchantQrCode merchantQrCode = new MerchantQrCode(MerchantQrCode.defaultRequest, token);
		merchantQrCode.setQueryParam("en_IN");
		merchantQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(merchantQrCode, APIStatus.MerchantQrCode.QR_1020);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.merchantQrCodeValidation(merchantQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT, MerchantHeaders.ID));
	}
}
