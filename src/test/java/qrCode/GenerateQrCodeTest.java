package qrCode;

import com.paytm.apiRequestBuilders.wallet.qrCode.GenerateQrCode;
import com.paytm.constants.Constants;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


import java.util.ArrayList;
import java.util.List;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-GenerateQrCode")
public class GenerateQrCodeTest extends WalletBaseTest {

	@Test(description = "when operation type is CREATE and batch size and batch count should be null and buisness type is P2P", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_BatchCountAndSizeNullWithP2PType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and buisness type is QR_MERCHANT", groups = {
			"regression" })
	public void TC002_BatchCountAndSizeNullWithQr_MerchantType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and buisness type is QR_PRODUCT", groups = {
			"regression" })
	public void TC003_BatchCountAndSizeNullWithQr_ProductType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_PRODUCT");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and buisness type is QR_PRICEDPRODUCT", groups = {
			"regression" })
	public void TC004_BatchCountAndSizeNullWithQr_PriceproductType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_PRICEDPRODUCT");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and buisness type is QR_ORDER", groups = {
			"regression" })
	public void TC005_BatchCountAndSizewithQr_OrderType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "04", null, null, operationType, null);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and buisness type is QR_USER", groups = {
			"regression", "sanity" })
	public void TC006_BatchCountAndSizeNullWithQr_UserType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and buisness type is QR_DEBIT", groups = {
			"regression" })
	public void TC007_BatchCountAndSizeNullWithQr_DebitType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and business type is invalid ", groups = {
			"regression" })
	public void TC008_BatchCountAndSizeNullWithInvalidBusinessType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().getCreateRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_DEBI");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0043);
	}

	@Test(description = "When operation type is CREATE and batch size and batch count should be null and business type is left blank ", groups = {
			"regression" })
	public void TC009_BatchCountAndSizeNullWithBlankBusinessType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().getCreateRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0043);
	}

	@Test(description = "When operation type is CREATE and batch size is 1 (any int ) and batch count is null and valid business type ", groups = {
			"regression" })
	public void TC010_BatchSizeOneAndCountNullWithValidBusinessType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().getCreateRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("").withBatchSize(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0043);
	}

	@Test(description = "When operation type is CREATE and batch size is null  and batch count is any int value and valid business type ", groups = {
			"regression" })
	public void TC011_BatchSizeNullAndCountOneWithValidBusinessType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().getCreateRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("").withBatchCount(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0043);
	}

	@Test(description = "When operation type is CREATE and batch size is any int value  and batch count is any int value and valid business type", groups = {
			"regression" })
	public void TC012_BatchCountAndSizeOneWithP2PType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().getCreateRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_USER").withBatchCount(1).withBatchSize(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0045);
	}

	@Test(description = "when blank request in create operation type ", groups = { "regression" })
	public void TC013_BlankRequestInCreateOperationType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		generateQrCode.getRequestPojo().getRequest().getCreateRequest().withAgentPhoneNo("").withBusinessType("")
				.withBatchCount("").withBatchSize("");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0043);
	}

	@Test(description = "when batch size and count is 0", groups = { "regression" })
	public void TC014_WhenBatchSizeAndCountZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().getCreateRequest().withAgentPhoneNo(agentPhoneNumber)
				.withBusinessType("QR_USER").withBatchCount(0).withBatchSize(0);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0045);
	}

	@Test(description = "when operation type is BULK_CREATE and batch size and batch count is null and valid Business type and agent phone number should be valid at Oauth end", groups = {
			"regression" })
	public void TC015_BulkCreateWithBatchCountAndSizeNull() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0041);
	}

	@Test(description = "when operation type is BULK_CREATE and batch size is any int  and batch count is null and valid Business type and agent phone number should be valid at Oauth end", groups = {
			"regression" })
	public void TC016_BulkCreateWithBatchSizeOneAndCountNull() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchSize(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0041);
	}

	@Test(description = "when operation type is BULK_CREATE and batch size is null  and batch count is any positive int and valid Business type and agent phone number should be valid at Oauth end", groups = {
			"regression" })
	public void TC017_BulkCreateWithBatchSizeNullAndCountOne() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0041);
	}

	@Test(description = "when operation type is BULK_CREATE and batch size is any positive int and batch count is any positive int and valid Business type and agent phone number should be valid at Oauth end ", groups = {
			"regression", "smoke", "sanity" })
	public void TC018_BulkCreateWithBatchSizeAndCountAnyPositiveInteger() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(2).withBatchSize(3);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", 3, 2, operationType, null);
	}

	@Test(description = "when operation type is BULK_CREATE and batch size is any positive int   and batch count is any positive int and valid Business type and agent phone number should be valid at Oauth end and should start from 5 to 9 any of the number but not 0,1,2,3,4", groups = {
			"regression" })
	public void TC019_BulkCreateWithAgentNumberStartsWithFive() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.NUMBERWITH5);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(2).withBatchSize(3);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", 3, 2, operationType, null);
	}

	@Test(description = "BULK_CREATE in operation type and agent phone number is null and valid business type ", groups = {
			"regression"})
	public void TC020_BulkCreateWithAgentNumberBlank() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo("").withBusinessType("QR_MERCHANT").withBatchCount(2).withBatchSize(3);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0042);
	}

	@Test(description = "when agent number and batch count is blank ", groups = { "regression" })
	public void TC021_AgentNumberAndBatchCountNull() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo("").withBusinessType("QR_MERCHANT").withBatchCount("").withBatchSize(3);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0041);
	}

	@Test(description = "when agent phone number starts with 4 and operationType is BULK_CREATE", groups = {
			"regression" })
	public void TC022_AgentPhoneNumberStartsWithFourwithBulkCreate() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo("4360758640").withBusinessType("QR_MERCHANT").withBatchCount(1).withBatchSize(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0003);
	}

	@Test(description = "when blank business type ", groups = { "regression" })
	public void TC023_InvalidBusinessType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("").withBatchCount(1).withBatchSize(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0040);
	}

	@Test(description = "when agentPhone numbe is less than 10 digit", groups = { "regression" })
	public void TC024_WhenAgentPhoneNumberIsLessThanTenDigit() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo("88726660").withBusinessType("QR_USER").withBatchCount(1).withBatchSize(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0003);
	}

	@Test(description = "when create request is null for BULK_CREATE operation type ", groups = { "regression" })
	public void TC025_WhenCreateRequestParameterIsBlank() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).withCreateRequest(null);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0039);
	}

	@Test(description = "when invalid operation type ", groups = { "regression" })
	public void TC026_InvalidOperationType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREAT");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(1).withBatchSize(1);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(generateQrCode, APIStatus.CreateDynamicQRCode.BADREQUEST);
	}

	@Test(description = "when batch count and size is 0", groups = { "regression" })
	public void TC027_BulkCreateWithBatchCountAndSizeZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(0).withBatchSize(0);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0041);
	}

	@Test(description = "when Business type is P2P and mapping type is USER and typeofQRCode is USER_QR_CODE when status is 1", groups = {
			"regression" })
	public void TC028_MapP2PTypeWithUser_qr_codeTypeWithStatusOne() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("USER_QR_CODE")
				.withPhoneNo(agentPhoneNumber).withMappingType("USER");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when Business type is P2P and mapping type is USER and typeofQRCode is USER_QR_CODE when status is 0", groups = {
			"regression", "sanity" })
	public void TC029_MapP2PTypeWithUser_qr_codeTypeWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when user number is invalid at OAUTH end ", groups = { "regression" })
	public void TC030_MapP2PTypeWithUser_qr_codeTypeWithInvalidPhoneNo() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withPhoneNo("1234567891").withStatus(0)
				.withTypeOfQrCode("USER_QR_CODE").withMappingType("USER");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0011);
	}

	@Test(description = "when Business type is P2P and mapping type is USER and typeofQRCode is USER_QR_CODE when status is 1 and deeplink is given", groups = {
			"regression" })
	public void TC031_MapP2PTypeWithUser_qr_codeTypeWithDeepLinkAndStatusOne() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withDeepLink("qwerty")
				.withTypeOfQrCode("USER_QR_CODE").withMappingType("USER").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when Business type is P2P and mapping type is USER and typeofQRCode is USER_QR_CODE when status is 0 and deeplink is given", groups = {
			"regression" })
	public void TC032_MapP2PTypeWithUser_qr_codeTypeWithDeepLinkAndStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withDeepLink("qwerty")
				.withTypeOfQrCode("USER_QR_CODE").withMappingType("USER").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when Business type is P2P and mapping type is USER and typeofQRCode is invalid", groups = {
			"regression" })
	public void TC033_MapInvalidTypeOfQrCodeWithP2PType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CO")
				.withMappingType("USER").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0021);
	}

	@Test(description = "when Business type is P2P and mapping type is invalid", groups = { "regression" })
	public void TC034_BusinessTypeP2PAndMappingTypeInvalid() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USE").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0037);
	}

	@Test(description = "when Business type is P2P and mapping type is merchant", groups = { "regression" })
	public void TC035_BussinessTypeP2PAndMappingTypeMerchant() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0019);
	}

	@Test(description = "when business type is p2p and typeofQRCode is MERCHANT_QR_CODE", groups = { "regression" })
	public void TC036_BusinessTypeP2PAndMerchantQrCodeType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0021);
	}

	@Test(description = "when business type is blank and display name is blank", groups = { "regression" })
	public void TC037_BusinessTypeAndDisplayNameIsBlank() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0043);
	}

	@Test(description = "when business type is p2p and mapping type is merchant and typeofQrCode is of MERCHANT_QR_CODE and status is 0", groups = {
			"regression" })
	public void TC038_MapP2PWithMerchantQrCodeTypeWithMappingTypeMerchantWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is p2p and mapping type is merchant and typeofQrCode is of MERCHANT_QR_CODE and status =1", groups = {
			"regression" })
	public void TC039_MapP2PWithMerchantQrCodeTypeWithMappingTypeMerchantWithStatusOne() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is p2p and mapping type is user and typeofQrcode is DYNAMIC_QR_CODE", groups = {
			"regression" })
	public void TC040_MapP2PWithDynamicQrCodeTypeWithMappingTypeMerchantWithStatusOne() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DYNAMIC_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, agentPhoneNumber);
	}

	@Test(description = "when buisness type is p2p and mappingtype is user and typeofQrcode is DEBIT_QR_CODE and deeplink is given", groups = {
			"regression" })
	public void TC041_MapP2PWithDebitQrCodeTypeWithDeepLink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DEBIT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("qwerty")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when buisness type is p2p and mappingtype is user and typeofQrcode is DEBIT_QR_CODE and deeplink is blank", groups = {
			"regression" })
	public void TC042_MapP2PWithDebitQrCodeWithDeepLinkBlank() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P").withBatchCount(null).withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DEBIT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is USER and typeofQrCode is USER_QR_CODE and status is 1", groups = {
			"regression" })
	public void TC043_MapQrUserWithUserQrCodeTypeWithStatusOne() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is USER and typeofQrCode is USER_QR_CODE and stauts is 0", groups = {
			"regression" })
	public void TC044_MapQrUserWithUserQrCodeWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is USER and typeofQrCode is USER_QR_CODE and staus is 1 and deeplink is passed", groups = {
			"regression" })
	public void TC045_MapQrUserWithUserQrCodeWithStatusOneAndDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("1234").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is USER and typeofQrCode is USER_QR_CODE and invalid phone number of user", groups = {
			"regression" })
	public void TC046_MapQrUserWithUserQrCodeWithInvalidUserNumber() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDisplayName("")
				.withPhoneNo("4050070082");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0011);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is USER and typeofQrCode is DYANMIC_QR_CODE", groups = {
			"regression" })
	public void TC047_MapQrUserWithDynamicQrCode() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DYNAMIC_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, agentPhoneNumber);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is USER and typeofQrCode is DEBIT_QR_CODE with deeplink", groups = {
			"regression" })
	public void TC048_MapQrUserWithDebitQrCodeWithDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DEBIT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("www.google.com")
				.withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is USER and typeofQrCode is DEBIT_QR_CODE without deeplink", groups = {
			"regression" })
	public void TC049_MapQrUserWithDebitQrUserWithourDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DEBIT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when businesstype is QR_USER and mapping type is MERCHANT and typeofQrCode is MERCHANT_QR_CODE", groups = {
			"regression" })
	public void TC050_MapQrUserWithMerchantQrCodeOfMappingTypeMerchant() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_USER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is USER_QR_CODE", groups = {
			"regression" })
	public void TC051_MapQrDebitWithUserQrCodeOfMappingTypeUser() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is USER_QR_CODE when status is 0", groups = {
			"regression" })
	public void TC052_MapQrDebitWithUserQrCodeWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is USER_QR_CODE when deeplink is passed", groups = {
			"regression" })
	public void TC053_MapQrDebitWithUserQrCodeWithDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("1234").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is DYNAMIC_QR_CODE", groups = {
			"regression" })
	public void TC054_MapQrDebitWithDynamicQrCode() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DYNAMIC_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("1234").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, agentPhoneNumber);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is DYNAMIC_QR_CODE without deeplink", groups = {
			"regression" })
	public void TC055_MapQrDebitWithDynamicQrCodeWithoutDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DYNAMIC_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, agentPhoneNumber);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is DYNAMIC_QR_CODE with status 0", groups = {
			"regression" })
	public void TC056_MapQrDebitWithDynamiQrCodeWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("DYNAMIC_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, agentPhoneNumber);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is DEBIT_QR_CODE", groups = {
			"regression" })
	public void TC057_MapQrDebitWithDebitQrCodeType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DEBIT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("1234").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is DEBIT_QR_CODE  with status 0", groups = {
			"regression" })
	public void TC058_MapQrDebitWithDebitQrCodeWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("DEBIT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("1234").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is user and typeofqrcode is DEBIT_QR_CODE  without deeplink", groups = {
			"regression" })
	public void TC059_MapQrDebitWithDebitQrCodeWithoutDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("DEBIT_QR_CODE")
				.withMappingType("USER").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is merchant and typeofqrcode is MERCHANT_QR_CODE", groups = {
			"regression" })
	public void TC060_MapQrDebitWithMerchantQrCode() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when business type is QR_DEBIT and mapping type is merchant and typeofqrcode is MERCHANT_QR_CODE with status 0", groups = {
			"regression" })
	public void TC061_MapQrDebitWithMerchantQrCodeWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when Business type is QR_MERCHANT and mapping type is MERCHANT and typeofqrcode is MERCHANT_QR_CODE and VALID GUID", groups = {
			"regression" })
	public void TC062_MapQrMerchantWithMerchantQrCodeWithValidGuid() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when Business type is QR_MERCHANT and mapping type is MERCHANT and typeofqrcode is MERCHANT_QR_CODE and INVALID GUID", groups = {
			"regression" })
	public void TC063_MapQrMerchantWithMerchantQrCodeWithInvalidGuid() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid("CE852DAB-418D-4B03-8975-2C496F4E123B").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.GE_1010);
	}

	@Test(description = "when Business type is QR_MERCHANT and mapping type is MERCHANT and typeofqrcode is MERCHANT_QR_CODE and  GUID and mid of two different merchant", groups = {
			"regression" })
	public void TC064_MapQrMerchantWithMerchantQrCodeWithGuidAndMidDifferent() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when Business type is QR_MERCHANT and mapping type is MERCHANT and typeofqrcode is MERCHANT_QR_CODE and mid of merchant", groups = {
			"regression" })
	public void TC065_MapQrMerchantWithMerchantQrCodeWithMerchantMid() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when Business type is QR_MERCHANT and mapping type is MERCHANT and typeofqrcode is MERCHANT_QR_CODE and mid of merchant  and status 0", groups = {
			"regression" })
	public void TC066_MapQrMerchantWithMerchantQrCodeWithMidWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when Business type is QR_MERCHANT and mapping type is MERCHANT and typeofqrcode is MERCHANT_QR_CODE and mid of merchant  and deeplink", groups = {
			"regression" })
	public void TC067_MapQrMerchantWithMerchantQrCodeWithMidAndDeeplinK() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("qwert").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when Buisness type QR_MERCHANT and mapping type is user", groups = { "regression" })
	public void TC068_BusinessTypeQrMerchantAndMappingTypeUser() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("USER")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("qwert").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0021);
	}

	@Test(description = "when Buisness type QR_MERCHANT and mapping type is merchant but typeofQRcode is USER_QR_CODE", groups = {
			"regression" })
	public void TC069_BusinesstypeQrMerchantAndUserQrCodeType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("qwert").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0021);
	}

	@Test(description = "when Buisness type QR_MERCHANT and mapping type is merchant but typeofQRcode is UTS without source id", groups = {
			"regression" })
	public void TC070_MapQrMerchantWithUTSTypeWithoutSourceId() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("UTS")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("qwert").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0021);
	}

	@Test(description = "when Buisness type QR_MERCHANT and mapping type is merchant but typeofQRcode is UTS with source id", groups = {
			"regression" })
	public void TC071_MapQrMerchantWithUTSWithSourceId() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("UTS")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("1234").withDeepLink("qwert").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when Buisness type QR_MERCHANT and mapping type is merchant but typeofQRcode is QR_CODE_DEEPLINK  without deeplink", groups = {
			"regression" })
	public void TC072_MapQrMerchantWithQrCodeDeeplinkWithoutDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("QR_CODE_DEEPLINK")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.QR_1031);
	}

	@Test(description = "when Buisness type QR_MERCHANT and mapping type is merchant but typeofQRcode is QR_CODE_DEEPLINK  with deeplink", groups = {
			"regression" })
	public void TC073_MapQrMerchantWithQrCodeDeeplinkWithDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("QR_CODE_DEEPLINK")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("1234").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when buisness type is QR_PRODUCT", groups = { "regression" })
	public void TC074_BusinessTypeQrProduct() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_PRODUCT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when buisness type QR_PRICEDPRODUCT", groups = { "regression" })
	public void TC075_BusinessTypeQrPriceProduct() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_PRICEDPRODUCT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}

	@Test(description = "when buisness type QR_ORDER", groups = { "regression" })
	public void TC076_BusinessTypeQrOrder() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(1).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "04", null, null, operationType, null);
	}

	@Test(description = "when buisness type QR_ORDER mapping type is MERCHANT and typeofqrcode MERCHANT_QR_CODE AND STAUS is 0", groups = {
			"regression" })
	public void TC077_MapQrOrderWithMerchantQrCodeWithStatusZero() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("123").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "04", null, null, operationType, null);
	}

	@Test(description = "when buisness type QR_ORDER mapping type is MERCHANT and typeofqrcode MERCHANT_QR_CODE without deeplink", groups = {
			"regression" })
	public void TC078_MapQrOrderWithMerchantQrCodeWithoutDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "04", null, null, operationType, null);
	}

	@Test(description = "when buisness type QR_ORDER mapping type is MERCHANT and typeofqrcode UTS", groups = {
			"regression" })
	public void TC079_MapQrOrderWithUTSType() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("UTS")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("1234").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "04", null, null, operationType, null);
	}

	@Test(description = "when buisness type QR_ORDER mapping type is MERCHANT and typeofqrcode UTS without sourceid", groups = {
			"regression" })
	public void TC080_MapQrOrderWithUTSWithoutSourceId() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("UTS")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0021);
	}

	@Test(description = "when buisness type QR_ORDER mapping type is MERCHANT and typeofqrcode QR_CODE_DEEPLINK without deeplink", groups = {
			"regression" })
	public void TC081_MapQrOrderWithQrCodeDeeplinkWithoutDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("QR_CODE_DEEPLINK")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.QR_1031);
	}

	@Test(description = "when buisness type QR_ORDER mapping type is MERCHANT and typeofqrcode QR_CODE_DEEPLINK with deeplink", groups = {
			"regression" })
	public void TC082_MapQrOrderWithQrCodeDeeplinkWithoutDeeplink() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("QR_CODE_DEEPLINK")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("1234").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "04", null, null, operationType, null);
	}

	@Test(description = "when operation type is bulk create then no other operation type is allowed", groups = {
			"regression" })
	public void TC083_NoOtherOperationTypeAllowedWhenBulkCreate() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("BULK_CREATE");
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("QR_CODE_DEEPLINK")
				.withMappingType("MERCHANT")
				.withMerchantMid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.NAME))
				.withSourceId("").withDeepLink("1234").withDisplayName("").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0047);
	}

	@Test(description = "when qrCodeId is present if operation type is only map", groups = { "regression", "sanity" })
	public void TC084_whenOperationTypeOnlyMap() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", agentPhoneNumber);
		List<String> operationType = new ArrayList<String>();
		operationType.add("MAP");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getMapRequest().withStatus(1)
				.withQrCodeId(qrCodeDetails[1]).withTypeOfQrCode("USER_QR_CODE").withMappingType("USER")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when stickerId & qrCodeId both is present if operation type is only map", groups = {
			"regression" })
	public void TC085_whenBothStickerIdAndQrCodeIdPresent() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", agentPhoneNumber);
		List<String> operationType = new ArrayList<String>();
		operationType.add("MAP");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getMapRequest().withStatus(1)
				.withQrCodeId(qrCodeDetails[1]).withStickerId(qrCodeDetails[0]).withTypeOfQrCode("USER_QR_CODE")
				.withMappingType("USER").withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}

	@Test(description = "when stickerId & qrCodeId both is not present if operation type is only map", groups = {
			"regression" })
	public void TC086_whenStickerIdAndqrCodeIdBothNotPresent() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		List<String> operationType = new ArrayList<String>();
		operationType.add("MAP");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getMapRequest().withStatus(1)
				.withQrCodeId("").withStickerId("").withTypeOfQrCode("USER_QR_CODE").withMappingType("USER")
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0046);
	}

	@Test(description = "mapRequest not present in request when operationType is MAP", groups = { "regression" })
	public void TC087_mapRequestNotPresentWhenOperationTypeMAP() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("MAP");
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).withMapRequest(null);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.GE_0001);
	}
	
	@Test(description = "case to verify when map qr to aggregator merchant", groups = { "regression" })
	public void TC088_MapToAggregatorMerchant() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.AGGREGATORMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.DQR_0051);
	}
	
	@Test(description = "case to verify when map qr to child merchant of aggregator merchant", groups = { "regression" })
	public void TC089_MapToChildMerchantOfAggregatorMerchant() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT").withBatchCount(null)
				.withBatchSize(null);
		generateQrCode.getRequestPojo().getRequest().getMapRequest().withStatus(0).withTypeOfQrCode("MERCHANT_QR_CODE")
				.withMappingType("MERCHANT").withMerchantMid("").withSourceId("").withDeepLink("").withDisplayName("")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CHILDMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withPhoneNo(agentPhoneNumber);
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "05", null, null, operationType, null);
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC090_InvalidLocaleValue() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		generateQrCode.setQueryParam("en-IN");
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(generateQrCode, APIStatus.GenerateQrCode.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC091_ValidLocaleValue() {
		GenerateQrCode generateQrCode = new GenerateQrCode(GenerateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		generateQrCode.setQueryParam("en_IN");
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		generateQrCode.getRequestPojo().getRequest().withOperationType(operationType).getCreateRequest()
				.withAgentPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		generateQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(generateQrCode, APIStatus.GenerateQrCode.SUCCESS);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.generateQrCodeParameterValidation(generateQrCode, "01", null, null, operationType, null);
	}
}