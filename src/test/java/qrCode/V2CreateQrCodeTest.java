package qrCode;

import com.paytm.apiRequestBuilders.wallet.qrCode.CreateQrCode;
import com.paytm.datamanager.MerchantManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


import java.util.ArrayList;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-V2CreateQrCode")
public class V2CreateQrCodeTest extends WalletBaseTest {

	private String endTime = " 23:59:59";
	private String startTime = " 00:00:00";

	@Test(description = "when valid GUID in the request and mid is blank and QrCode type is QR_ORDER", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_QrOrderTypeWithValidGuidAndBlankMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER where guid is invalid and mid is blank", groups = { "regression" })
	public void TC002_QrOrderWithInvalidGuidAndMidBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER")
				.withMerchantGuid("7FB8A5A1-2637-4430-A2A2-353C8A928822").withMid("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1023);
	}

	@Test(description = "QrCode type is QR_ORDER where guid is null  and mid is null", groups = { "regression" })
	public void TC003_QrOrderWithBlankGuidAndMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid("").withMid("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1002);
	}

	@Test(description = "QrCode type is QR_ORDER and length  merchant guid is less than 32", groups = { "regression" })
	public void TC004_QrOrderWithMerchantGuidLengthLessThan32() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER")
				.withMerchantGuid("7FB8A5A1-2637-4430-A2A2-353C8A9288").withMid("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1028);
	}

	@Test(description = "when invalid RequestType in the Request", groups = { "regression" })
	public void TC005_InvalidRequetType() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDE").withMerchantGuid(merchantGuid)
				.withMid("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1011);
	}

	@Test(description = "QrCode type is QR_ORDER and order details is interger", groups = { "regression" })
	public void TC006_QrOrderWithOrderDetailsAsInteger() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("123456").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and order details is character ", groups = { "regression" })
	public void TC007_QrOrderWithOrderDetailsAsCharacter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("qwerty").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and order details is left blank", groups = { "regression" })
	public void TC008_QrOrderWithOrderDetailsBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("").withOrderId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1027);
	}

	@Test(description = "QrCode type is QR_ORDER and order details is alphanumeric", groups = { "regression" })
	public void TC009_QrOrderWithOrderDetailsAlphaNumeric() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("qwerty1234")
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and order details is special character", groups = { "regression" })
	public void TC010_QrOrderWithOrderDetailsSpecialCharaters() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("!@#$%^&").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and repeated order id", groups = { "regression" })
	public void TC011_QrOrderWithSameOrderId() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameter = walletAPIHelpers.createQrCodeHelper("QR_ORDER", "orderId");
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("!@#$%^&").withOrderId(repeatedParameter.get(0));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1020);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and order id is of character", groups = { "regression" })
	public void TC012_QrOrderWithOrderIdAsCharacter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("!@#$%^&")
				.withOrderId("orderid" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "when order id param is removed from the request and QrCode type is QR_order", groups = {
			"regression" })
	public void TC013_QrOrderWithNoOrderIdParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("!@#$%^&").withOrderId(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1004);
	}

	@Test(description = "when order id is repeated but the merchant guid is different", groups = { "regression" })
	public void TC014_QrOrderWithRepeatedOrderIdButDifferentMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameter = walletAPIHelpers.createQrCodeHelper("QR_ORDER", "orderId");
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderDetails("!@#$%^&").withOrderId(repeatedParameter.get(0));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and amount is blank", groups = { "regression" })
	public void TC015_QrOrderWithAmountBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withAmount("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.GE_1007);
	}

	@Test(description = "QrCode type id QR_ORDER and amount is negative", groups = { "regression" })
	public void TC016_QrOrderWithAmountNegative() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withAmount("-10");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "QrCode type is QR_ORDER and amount field is filled with character ", groups = { "regression" })
	public void TC017_QrOrderWithAmountAsCharacters() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withAmount("qwerty");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1028);
	}

	@Test(description = "QrCode type is QR_ORDER and guid and mid field is blank and merchant name is passed", groups = {
			"regression" })
	public void TC018_QrOrderWithGuidAndMidBlankAndMerchantNameIsPassed() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantName = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid("").withMid("")
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withMerchantName(merchantName);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1002);
	}

	@Test(description = "QrCode type is QR_ORDER and mid and guid is of same merchant", groups = { "regression" })
	public void TC019_QrOrderWithMidAndGuidOfSameMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMID = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid(merchantMID).withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and guid is blank and mid of a valid merchant is passed ", groups = {
			"regression" })
	public void TC020_QrOrderWithGuidBlankAndValidMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMID = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid("").withMid(merchantMID)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode type is QR_ORDER and invalid mid is passed", groups = { "regression" })
	public void TC021_QrOrderWithInvalidMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid("")
				.withMid("qwert123456789").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1023);
	}

	@Test(description = "QrCode type is QR_ORDER and mid and guid is of two different merchant", groups = {
			"regression" })
	public void TC022_QrOrderWithMidAndGuidOfDifferentMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid(merchantMid).withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1030);
	}

	@Test(description = "QrCode is QR_ORDER and expiry field is blank", groups = { "regression" })
	public void TC023_QrOrderWithExpiryBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withExpiryDate("");
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode is QR_ORDER and expiry field is set of future date", groups = { "regression" })
	public void TC024_QrOrderWithExpirySetOfFuturDate() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String futureDate = qrHelper.getDate("withoutTime", 10);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withExpiryDate(futureDate);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				qrHelper.getDate("withoutTime", 7) + startTime);
	}

	@Test(description = "QrCode is QR_ORDER and expiry field is set of past date", groups = { "regression" })
	public void TC025_QrOrderWithExpirySetOfPastDate() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withExpiryDate("2017-05-26");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1007);
	}

	@Test(description = "QrCode is QR_ORDER and expiry field is set of same  date", groups = { "regression" })
	public void TC026_QrOrderWithExpirySetOfSameDate() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String currDate = qrHelper.getDate("withoutTime", 0);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withExpiryDate(currDate);
		createQrCode.createRequestJsonAndExecute();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode is QR_ORDER and expiry field is not null and validity parameter is past with number of days for which qr is going to be valid", groups = {
			"regression" })
	public void TC027_QrOrderWithExpiryNotNullAndValidity() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String futureDate = qrHelper.getDate("withoutTime", 7);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withExpiryDate(futureDate)
				.withValidity("12");
		createQrCode.createRequestJsonAndExecute();
		String expiryDate = qrHelper.getDate("withoutTime", 12);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode is QR_ORDER when QrHandle parameter is removed", groups = { "regression" })
	public void TC028_QrOrderWithNoQrHandleParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withQrInfo(null);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode is QR_ORDER when imageRequired is true", groups = { "regression" })
	public void TC029_QrOrderWithImageRequiredTrue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withImageRequired(true);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode is QR_ORDER when imageRequired is false", groups = { "regression" })
	public void TC030_QrOrderWithImageRequiredFalse() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withImageRequired(false);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode is QR_ORDER when imageRequired parameter is removed", groups = { "regression", "smoke",
			"sanity" })
	public void TC031_QrOrderWithNoImageRequiredParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withImageRequired(null);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "Creating a successful QR_Code of QR_MERCHANT TYPE  with mandate parameter", groups = {
			"regression" })
	public void TC032_QrMerchantWithMandateParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "QR_MERCHANT with order_id blank", groups = { "regression" })
	public void TC033_QrMerchantWithOrderIdBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("").withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "QR_MERCHANT with repeated pos_id", groups = { "regression" })
	public void TC034_QrMerchantWithRepeatedPosId() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameter = walletAPIHelpers.createQrCodeHelper("QR_MERCHANT", "posId");
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(repeatedParameter.get(0));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1020);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "QR_MERCHANT when amount is removed ", groups = { "regression" })
	public void TC036_QrMerchantWithNoAmountParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "QR_MERCHANT when amount is blank", groups = { "regression", "sanity" })
	public void TC037_QrMerchantWithAmountBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.GE_1007);
	}

	@Test(description = "QR_MERCHANT when amount is negative", groups = { "regression" })
	public void TC038_QrMerchantWithNegativeAmount() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("-10");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "when amount is zero", groups = { "regression" })
	public void TC039_QrMerchantWithAmountZero() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("0");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "When expiry of back date is set", groups = { "regression" })
	public void TC040_QrMerchantWithExpiryOfBackDateIsSet() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate("2018-04-12");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1007);
	}

	@Test(description = "when expiry of future date is set", groups = { "regression" })
	public void TC041_QrMerchantWithExpiryOfFutureDateIsSet() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String futureDate = qrHelper.getDate("withoutTime", 10);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate(futureDate);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				futureDate + endTime);
	}

	@Test(description = "when validity is set with 0", groups = { "regression" })
	public void TC042_QrMerchantWithValidityZero() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withValidity("0");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "creating without QrHandle", groups = { "regression" })
	public void TC043_QrMerchantWithoutQrHandle() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withQrInfo(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "with image required true", groups = { "regression" })
	public void TC044_QrMerchantWithImageRequiredTrue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(true);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "QrCode of QR_MERCHANT with image required false ", groups = { "regression" })
	public void TC045_QrMerchantWithImageRequiredFalse() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(false);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "QrCode of QR_MERCHANT without imageRequired parameter", groups = { "regression" })
	public void TC046_QrMerchantWithoutImageRequiredParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "QrCode of QR_MERCHANT when guid and mid are of two different merchant", groups = {
			"regression" })
	public void TC047_QrMerchantWithGuidAndMidOfDifferentMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid(merchantMid).withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1030);
	}

	@Test(description = "when mid and guid of same merchant is passed", groups = { "regression" })
	public void TC048_QrMerchantWithGuidAndMidOfSameMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid(merchantMid).withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when guid is blank and only mid of a merchant is passed ", groups = { "regression" })
	public void TC049_QrMerchantWithGuidIsBlankAndMidIsGiven() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid("")
				.withMid(merchantMid).withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when guid is null and mid is invalid", groups = { "regression" })
	public void TC050_QrMerchantWithGuidBlankAndMidInvalid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid("")
				.withMid(String.valueOf(System.currentTimeMillis()))
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1023);
	}

	@Test(description = "when both guid and mid are blank", groups = { "regression" })
	public void TC051_QrMerchantWithGuidAndMidBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid("").withMid("")
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1002);
	}

	@Test(description = "creating UPI_QR_CODE with mandate parameter", groups = { "regression", "sanity" })
	public void TC052_UpiQrCodeWithValidParamter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "QrCode type UPI_QR_CODE with repeated orderId", groups = { "regression" })
	public void TC053_UpiQrCodeWithRepeatedOrderId() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameter = walletAPIHelpers.createQrCodeHelper("UPI_QR_CODE", "orderId");
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId(repeatedParameter.get(0)).withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1033);
	}

	@Test(description = "when order id repeated but merchant guid is different", groups = { "regression" })
	public void TC055_UpiQrCodeWithRepeatedOrderIdButOfDifferentMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameter = walletAPIHelpers.createQrCodeHelper("UPI_QR_CODE", "orderId");
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId(repeatedParameter.get(0)).withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "QrCode type is UPI_QR_CODE and amount is blank", groups = { "regression" })
	public void TC056_UpiQrCodeWithAmountBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.GE_1007);
	}

	@Test(description = "QrCode type is UPI_QR_CODE and amount is 0", groups = { "regression" })
	public void TC057_UpiQrCodeWithAmountZero() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("0");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "QrCode type is UPI_QR_CODE and amount parameter is removed", groups = { "regression" })
	public void TC058_UpiQrCodeWithNoAmountParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount(null);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "QrCode type is UPI_QR_CODE and amount is negative", groups = { "regression" })
	public void TC059_UpiQrCodeWithNegativeAmount() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("-10");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "when QrCode is UPI_QR_CODE and mid and guid is of same merchant", groups = { "regression" })
	public void TC060_UpiQrCodeWithGuidAndMidOfSameMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid(merchantMid).withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when QrCode is UPI_QR_CODE and mid and guid is of two different merchant", groups = {
			"regression" })
	public void TC061_UpiQrCodeWithGuidAndMidOfDifferentMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid(merchantMid).withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1030);
	}

	@Test(description = "when QrCode is UPI_QR_CODE and mid is valid and guid is invalid", groups = { "regression" })
	public void TC062_UpiQrCodeWithValidMidAndInvalidGuid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE")
				.withMerchantGuid("7FB8A5A1-2637-4430-A2A2-353C8A228328").withMid(merchantMid)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1023);
	}

	@Test(description = "when guid is blank and mid is only passed", groups = { "regression" })
	public void TC063_UpiQrCodeWithBlankGuidAndValidMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid("")
				.withMid(merchantMid).withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount(null);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when guid is blank and mid is invalid", groups = { "regression" })
	public void TC064_UpiQrMerchantWithGuidBlankAndInvalidMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid("")
				.withMid(String.valueOf(System.currentTimeMillis()))
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1023);
	}

	@Test(description = "when is set to future date", groups = { "regression" })
	public void TC065_UpiQrCodeWithExpirySetToFutureDate() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String furuteDate = qrHelper.getDate("withoutTime", 10);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate(furuteDate);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				furuteDate + endTime);
	}

	@Test(description = "when expiry date is set past date", groups = { "regression" })
	public void TC066_UpiQrCodeWithExpirySetToPastDate() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate("2017-05-26");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1007);
	}

	@Test(description = "when expiry param is left blank", groups = { "regression" })
	public void TC067_UpiQrCodeWithExpiryDateBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate("");
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when expiry param is removed", groups = { "regression" })
	public void TC068_UpiQrCodeWithNoExpiryDateParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate(null);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when expiry is blank and validity is set to 10 days ", groups = { "regression" })
	public void TC069_UpiQrCodeWithBlankExpiryDateAndValidyOfTenDays() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate("").setValidity("10");
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when QrHandle is removed", groups = { "regression" })
	public void TC070_UpiQrCodeWithQrHandleRemoved() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withQrInfo(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1032);
	}

	@Test(description = "when imageRequired param is true", groups = { "regression" })
	public void TC071_UpiQrCodeWithImageRequiredTrue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(true);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when imageRequired param is false", groups = { "regression" })
	public void TC072_UpiQrCodeWithImageRequiredFalse() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(false);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when imageRequired param is removed", groups = { "regression" })
	public void TC073_UpiQrCodeWithImageRequiredParamRemoved() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(null);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "Creating success QR_PRODUCT with mandate parameter", groups = { "regression" })
	public void TC074_QrProductWithMandateParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "Creating QR_PRODUCT with repeated productid", groups = { "regression" })
	public void TC075_QrProductWithRepeatedProdutId() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameter = walletAPIHelpers.createQrCodeHelper("QR_PRODUCT", "productId");
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId(repeatedParameter.get(1)).withPosId(repeatedParameter.get(0))
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1020);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "creating QR_PRODUCT with blank prodctid", groups = { "regression" })
	public void TC076_QrProductWithProductIdBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("").withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1006);
	}

	@Test(description = "when when productid is only character", groups = { "regression" })
	public void TC077_QrProductWithProductIdHavingCharactersInIt() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRODUCTID").withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when productid is only special character", groups = { "regression" })
	public void TC078_QrProductWithProductIdSpecialCharacters() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("!@#&%").withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when productid is removed", groups = { "regression" })
	public void TC079_QrProductWithNoProductIdParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId(null).withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1006);
	}

	@Test(description = "when productid is same but merchant is different", groups = { "regression" })
	public void TC080_QrProductWithSameProductIdButDifferentMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameter = walletAPIHelpers.createQrCodeHelper("QR_PRODUCT", "productId");
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId(repeatedParameter.get(1)).withPosId(repeatedParameter.get(0))
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when amount is blank", groups = { "regression" })
	public void TC081_QrProductWithAmountBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.GE_1007);
	}

	@Test(description = "when amount is zero", groups = { "regression" })
	public void TC082_QrProductWithAmountzero() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("0");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "when amount is negative", groups = { "regression" })
	public void TC083_QrProductWithAmountNegative() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("-10");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "when creating QR_PRODUCT without amount parameter ", groups = { "regression" })
	public void TC084_QrProductWithoutAmountParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when creating QR_PRODUCT with valid guid but invalid mid", groups = { "regression" })
	public void TC085_QrProductWithValidGuidAndInvalidMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withMid(String.valueOf(System.currentTimeMillis()))
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1030);
	}

	@Test(description = "When creating QR_PRODUCT valid guid and mid of two different merchant", groups = {
			"regression" })
	public void TC086_QrProductWithValidGuidAndMidOfDifferentMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String mercahntMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withMid(mercahntMid).withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1030);
	}

	@Test(description = "When creating QR_PRODUCT with only mid", groups = { "regression" })
	public void TC087_QrProductWithOnlyMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String mercahntMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid("")
				.withMid(mercahntMid).withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when creating QR_PRODUCT with invalid mid", groups = { "regression" })
	public void TC088_QrProductWithOnlyMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid("")
				.withMid(String.valueOf(System.currentTimeMillis()))
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1023);
	}

	@Test(description = "when creating QR_prduct with blank mid and guid", groups = { "regression" })
	public void TC089_QrProductWithBlankMidAndGuid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid("").withMid("")
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1002);
	}

	@Test(description = "when creating QR_PRODUCT without QrHandle", groups = { "regression" })
	public void TC090_QrProductWithoutQrHandleParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withQrInfo(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when creating QR_PRODUCT and expiry is set to future date", groups = { "regression" })
	public void TC091_QrProductWithExpirySetOfFutureDate() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String futureDate = qrHelper.getDate("withoutTime", 7);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate(futureDate);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				futureDate + endTime);
	}

	@Test(description = "when creating QR_PRODUCT and exipry is set of back date", groups = { "regression" })
	public void TC092_QrProductWithExpirySetOfBackDate() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate("2017-05-26");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1007);
	}

	@Test(description = "when expiry is set blank", groups = { "regression" })
	public void TC093_QrProductWithExpiryDateBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when expiry is set null and validity param is passed with 20 days", groups = { "regression" })
	public void TC094_QrProductWithExpiryDateBlankAndValidityOftwentyDays() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate("").withValidity("20");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when imageRequired param is false", groups = { "regression" })
	public void TC095_QrProductWithImageRequiredFalse() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(false);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when ImageRequired param is true", groups = { "regression" })
	public void TC096_QrProductWithImageRequiredTrue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(true);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when imageRequired param is removed", groups = { "regression" })
	public void TC097_QrProductWithoutImageRequiredParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRODUCT").withMerchantGuid(merchantGuid)
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withImageRequired(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "creating QR_PRICEDPRODUCT with all the mandate parameter", groups = { "regression" })
	public void TC098_QrPricedProductWithValidParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "creating QR_PRICEDPRODUCT with repeated productID", groups = { "regression" })
	public void TC099_QrPricedProductWithRepeatedProductId() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		ArrayList<String> repeatedParameters = walletAPIHelpers.createQrCodeHelper("QR_PRICEDPRODUCT", "productId");
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId(repeatedParameters.get(1)).withPosId(repeatedParameters.get(0));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1020);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when productid is blank", groups = { "regression" })
	public void TC100_QrPricedProductWithProductIdBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1006);
	}

	@Test(description = "when productid param is removed ", groups = { "regression" })
	public void TC101_QrPricedProductWithProductIdParamRemoved() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1006);
	}

	@Test(description = "when amount is blank", groups = { "regression" })
	public void TC102_QrPricedProductWithAmountBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withAmount("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "when amount is some valid amount", groups = { "regression" })
	public void TC103_QrPricedProductwithValidAmount() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withAmount("1");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when amount is negative", groups = { "regression" })
	public void TC105_QrPricedProductwithAmountNegative() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withAmount("-1");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "when amount param is removed", groups = { "regression" })
	public void TC106_QrPricedProductwithAmountParameterRemoved() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withAmount(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1005);
	}

	@Test(description = "when guid is blank and mid of valid merchant", groups = { "regression" })
	public void TC107_QrPricedProductWithGuidBlankAndValidMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid("")
				.withMid(merchantMid).withProductId("PRO" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when invalid mid is passed and guid is blank", groups = { "regression" })
	public void TC108_QrPricedProductWithGuidBlankAndInvalidMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid("")
				.withMid(String.valueOf(System.currentTimeMillis()))
				.withProductId("PRO" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1023);
	}

	@Test(description = "when mid and guid is of different merchant is passed in the request", groups = {
			"regression" })
	public void TC109_QrPricedProductWithGuidAndMidOfDifferentMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		String merchantMid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid(merchantMid).withProductId("PRO" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1030);
	}

	@Test(description = "when expiry is of future date", groups = { "regression" })
	public void TC110_QrPricedProductWithExpiryOfFutureDate() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String futureDate = qrHelper.getDate("withoutTime", 7);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withExpiryDate(futureDate);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				futureDate + endTime);
	}

	@Test(description = "when expiry of past date", groups = { "regression" })
	public void TC111_QrPricedProductWithExpiryDateOfPastDate() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis()))
				.withExpiryDate("2017-04-21");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1007);
	}

	@Test(description = "when no expry is passed", groups = { "regression", "sanity" })
	public void TC112_QrPricedProductWithExpiryDateBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withExpiryDate("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when qrHandle is not passed  and image required not passed", groups = { "regression" })
	public void TC113_QrPricedProductWithNoQrHandleAndImageRequiredParameterPassed() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withQrInfo(null)
				.withImageRequired(null);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "when image required is false", groups = { "regression" })
	public void TC114_QrPricedProductWithImageRequiredFalse() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withImageRequired(false);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "case to verify when valid merchant guid is passed in headers", groups = { "regression" })
	public void TC115_VerifyWhenValidGuidPassedInHeaders() {
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String futureDate = qrHelper.getDate("withoutTime", 10);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withExpiryDate(futureDate);
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				futureDate + endTime);
	}

	@Test(description = "case to verify when valid merchant guid is passed in header but it don't have API access", groups = {
			"regression" })
	public void TC116_MerchantGuidPassedDontHaveAPIAccess() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOAPIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateQrCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to verify when invaliid guid is passed in headers", groups = { "regression" })
	public void TC117_WhenInvalidGuidIsPassed() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest,
				"125FD26C-4D98-11E2-B20C-E89A8FF3RTYU");
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateQrCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to verify when merchant guid not passed in headers", groups = { "regression" })
	public void TC118_WhenMerchantGuidNotPassedInHeaders() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, null);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateQrCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to verify when merchant guid passed in headers and in request are of different merchant", groups = {
			"regression" })
	public void TC119_MerchantGuidInHeadersAndRequestAreDifferent() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "case to verify when map qr to aggregator merchant", groups = { "regression" })
	public void TC120_MapToAggregatorMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.AGGREGATORMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1034);
	}

	@Test(description = "case to verify when map qr to child merchant of aggregator merchant", groups = {
			"regression" })
	public void TC121_MapToChildMerchantOfAggregatorMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CHILDMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CHILDMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC0122_InvalidLocaleValue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.setQueryParam("en-IN");
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateQrCode.INTERNALSERVERERROR);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC0123_ValidLocaleValue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, merchantGuid);
		createQrCode.setQueryParam("en_IN");
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}
}