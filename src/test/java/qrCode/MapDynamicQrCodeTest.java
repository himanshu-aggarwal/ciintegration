package qrCode;


//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;

import com.paytm.apiRequestBuilders.wallet.qrCode.MapDynamicQrCode;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.Constants;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.*;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import com.paytm.utils.Redis;

import redis.clients.jedis.Jedis;


import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-MapDynamicQrCode")
public class MapDynamicQrCodeTest extends WalletBaseTest {

	@Test(description = "Case to verify when mapping P2P businessType qrCode with USER_QR_CODE typeOfQrCode", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_MapP2PbusinessTypeWithUserQrCodeType() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify mapDynamicQRCode when already mapped qr code is used", groups = { "regression",
			"sanity" })
	public void TC002_MapAlreadyMappedQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		if (mapDynamicQrCode.getApiResponse().getStatusCode() == 200
				&& mapDynamicQrCode.getResponsePojo().getResponse().getQrCodeId() != null) {
			String phoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.NUMBERWITH5);
			mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0])
					.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNumber);
			mapDynamicQrCode.createRequestJsonAndExecute();
			CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0009);
		}
	}

	@Test(description = "Case to verify mapDynamicQRCode to map P2P qrCode with flagged merchant user", groups = {
			"regression", "smoke", "sanity" })
	public void TC003_MapDynamicQrCodeWithFlaggedMerchantUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("DYNAMIC_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, phoneNo,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify mapDynamicQRCode to map DYNAMIC_QR_CODE typeOfQrCode with normal user", groups = {
			"regression" })
	public void TC004_MapDynamicQrCodeWithNormalUSer() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("DYNAMIC_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, phoneNo,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when mapping DEBIT_QR_CODE typeOfQrCode with P2P businessType qrCode", groups = {
			"regression" })
	public void TC005_MapP2PbusinessTypeWithDebitQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("DEBIT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when mappingType is USER and typeOfQrCode is MERCHANT_QR_CODE", groups = {
			"regression" })
	public void TC006_MappinTypeUserAndTypeOfQrCodeMerchantQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when mappingType is USER and typeOfQrCode is QR_CODE_DEEPLINK", groups = {
			"regression" })
	public void TC007_MappinTypeUserAndTypeOfQrCodeQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("QR_CODE_DEEPLINK");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when qrCode of P2P businessType is assign to merchant", groups = {
			"regression" })
	public void TC008_P2PBusinessTypeAssignToMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when stickerId is blank in request", groups = { "regression" })
	public void TC009_StickeridBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId("").withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when typeOfQrCode is blank in request", groups = { "regression" })
	public void TC010_TypeOfQrCodeBlankInRequest() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withTypeOfQrCode("").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when qrCode of QR_MERCHANT businessType is mapped with User", groups = {
			"regression" })
	public void TC011_QrMerchantBusinessTypeMappedWithUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("USER_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0024);
	}

	@Test(description = "Case to verify when qrCode of QR_PRODUCT businessType is mapped with User", groups = {
			"regression" })
	public void TC012_QrProductBusinessTypeMappedWithUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("USER_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0024);
	}

	@Test(description = "Case to verify when qrCode of QR_PRICEDPRODUCT businessType is mapped with User", groups = {
			"regression" })
	public void TC013_QrPricedProductBusinessTypeMappedWithUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("USER_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0024);
	}

	@Test(description = "Case to verify when qrCode of QR_ORDER businessType is mapped with User", groups = {
			"regression" })
	public void TC014_QrOrderBusinessTypeMappedWithUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_ORDER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("USER_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0024);
	}

	@Test(description = "Case to verify when qrCode of QR_USER businessType is mapped with USER_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC015_QrUserBusinessTypeMappedWithUserQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("USER_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_USER businessType is mapped with DYNAMIC_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC016_MapQrUserBusinessTypeWithDynamicQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("DYNAMIC_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, phoneNo,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_USER businessType is mapped with DEBIT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC017_QrUserBusinessTypeMappedWithDebitQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("DEBIT_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_DEBIT businessType is mapped with USER_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC018_QrDebitBusinessTypeMappedWithUserQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("USER_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_DEBIT businessType is mapped with DYNAMIC_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC019_MapQrDebitBusinessTypeWithDynamicQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("DYNAMIC_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, phoneNo,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_DEBIT businessType is mapped with DEBIT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC020_QrDebitBusinessTypeMappedWithDebitQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("USER").withTypeOfQrCode("DEBIT_QR_CODE").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_MERCHANT businessType is mapped with MERCHANT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC021_QrMerchantBusinessTypeMappedWithMerchantQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_MERCHANT businessType is mapped with QR_CODE_DEEPLINK typeOfQrCode", groups = {
			"regression" })
	public void TC022_QrMerchantBusinessTypeMappedWithQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("http://google.com");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_PRODUCT businessType is mapped with MERCHANT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC023_QrProductBusinessTypeMappedWithMerchantQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_PRODUCT businessType is mapped with QR_CODE_DEEPLINK typeOfQrCode", groups = {
			"regression" })
	public void TC024_QrProductBusinessTypeMappedWithQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("http://google.com");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_PRICEDPRODUCT businessType is mapped with MERCHANT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC025_QrPricedProductMappedWithMerchantQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_PRICEDPRODUCT businessType is mapped with QR_CODE_DEEPLINK typeOfQrCode", groups = {
			"regression" })
	public void TC026_QrPricedProductMappedWithQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("http://google.com");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_ORDER businessType is mapped with MERCHANT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC027_QrOrderBusinessTypeMappedWithMerchantQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_ORDER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_ORDER businessType is mapped with QR_CODE_DEEPLINK typeOfQrCode", groups = {
			"regression" })
	public void TC028_QrOrderBusinessTypeMappedWithQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_ORDER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("http://google.com");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_USER businessType is mapped with MERCHANT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC029_QrUserBusinessTypeMappedWithMerchantQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_USER businessType is mapped with QR_CODE_DEEPLINK typeOfQrCode", groups = {
			"regression" })
	public void TC30_QrUserBusinessTypeMappedWithQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("http://google.com");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_DEBIT businessType is mapped with MERCHANT_QR_CODE typeOfQrCode", groups = {
			"regression" })
	public void TC031_QrDebitBusinessTypeMappedWithMerchantQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_DEBIT businessType is mapped with QR_CODE_DEEPLINK typeOfQrCode", groups = {
			"regression" })
	public void TC032_QrDebitBusinessTypeMappedWithQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("http://google.com");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when mappingType is USER and typeOfQrCode is UTS", groups = { "regression" })
	public void TC033_MappingTypeUSerAndTypeOfQrCodeUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when qrCode of QR_MERCHANT businessType is mapped with UTS typeOfQrCode", groups = {
			"regression" })
	public void TC034_QrMerchantBusinessTypeMappedWithUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_PRODUCT businessType is mapped with UTS typeOfQrCode", groups = {
			"regression" })
	public void TC035_QrProductBusinessTypeMappedWithUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_PRICEDPRODUCT businessType is mapped with UTS typeOfQrCode", groups = {
			"regression" })
	public void TC036_QrPricedProductMappedWithUTs() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_ORDER businessType is mapped with UTS typeOfQrCode", groups = {
			"regression" })
	public void TC037_QrOrderBusinessTypeMappedWithUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_ORDER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_USER businessType is mapped with UTS typeOfQrCode", groups = {
			"regression" })
	public void TC038_QrUSerBusinessTypeMappedWithUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of QR_DEBIT businessType is mapped with UTS typeOfQrCode", groups = {
			"regression" })
	public void TC039_QrDebitBusinessTypeMappedWithUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when typeOfQrCode is UTS and sourceId is blank", groups = { "regression" })
	public void TC040_SourceIdBlankWhenTypeOfQrCodeUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when typeOfQrCode is UTS and sourceId is null string", groups = {
			"regression" })
	public void TC041_SourceIdAsNullStringWhenTypeOfQrCodeUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("null");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when typeOfQrCode is UTS and sourceId is not passed in request", groups = {
			"regression" })
	public void TC042_SourceIdNotPassedWhenTypeOfQrCodeUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId(null);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when typeOfQrCode is other than UTS and sourceId is not passed in request", groups = {
			"regression" })
	public void TC043_SourceIdNotPassedWhenTypeOfQrCodeOtherThanUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE").withSourceId(null);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when typeOfQrCode is QR_CODE_DEEPLINK and deepLink parameter is blank ", groups = {
			"regression" })
	public void TC044_DeeplinkBlankWhenTypeOfQrCodeQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.QR_1031);
	}

	@Test(description = "Case to verify when typeOfQrCode is QR_CODE_DEEPLINK and deepLink parameter is null ", groups = {
			"regression" })
	public void TC045_DeeplinkAsNullStringWhenTypeOfQrCodeQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink("null");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when typeOfQrCode is QR_CODE_DEEPLINK and deepLink parameter is not passed in request", groups = {
			"regression" })
	public void TC046_DeeplinkNotPassedWhenTypeOfQrCodeQrCodeDeeplink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("QR_CODE_DEEPLINK").withDeepLink(null);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.QR_1031);
	}

	@Test(description = "Case to verify when qrCodeId is present in request but stickerId is null in request", groups = {
			"regression" })
	public void TC047_StickerIdNullStringQrCodeIdNotNull() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId("null").withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0005);
	}

	@Test(description = "Case to verify when qrCodeId is present in request but stickerId parameter not passed in request", groups = {
			"regression" })
	public void TC048_QrCodeIdPresentButStickerIdNotPassedInResquest() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(null).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when stickerId is present in request but qrCodeId is blank", groups = {
			"regression" })
	public void TC049_StickerIdPresentButQrCodeIdBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId("")
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when stickerId is present in request but qrCodeId is null", groups = {
			"regression" })
	public void TC050_StickerIdPresentButQrCodeIdAsNullString() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId("null")
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when stickerId is present in request but qrCodeId parameter is not passed in request", groups = {
			"regression" })
	public void TC051_StirckerIdPresentButQrCodeIdNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(null)
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when both stickerId and qrCodeId parameter not passed in request", groups = {
			"regression" })
	public void TC052_StickerIdAndQrCodeIdBothNotPassed() {
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(null).withQrCodeId(null).withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0001);
	}

	@Test(description = "Case to verify when qrCode of businessType is P2P/QR_USER/QR_DEBIT and phoneNo is blank", groups = {
			"regression" })
	public void TC053_PhoneNoBlankWhenBusinessTypeIsP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo("");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0002);
	}

	@Test(description = "Case to verify when qrCode of businessType is P2P/QR_USER/QR_DEBIT and phoneNo is as null string", groups = {
			"regression" })
	public void TC054_PhoneNoAsNullStringWhenBusinessTypeIsP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo("null");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0011);
	}

	@Test(description = "Case to verify when qrCode of businessType is P2P/QR_USER/QR_DEBIT and phoneNo parameter is not passed in request", groups = {
			"regression" })
	public void TC055_PhoneNoNotPassedWhenBusinessTypeIsP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(null);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0002);
	}

	@Test(description = "Case to verify when qrCode of businessType is P2P/QR_USER/QR_DEBIT and displayName parameter is blank in request", groups = {
			"regression" })
	public void TC056_DisplayNameBlankWhenBusinessTypeIsP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withDisplayName("").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of businessType is P2P/QR_USER/QR_DEBIT and displayName parameter is null in reuqest", groups = {
			"regression" })
	public void TC057_DisplayNameAsNullStringWhenBusinessTypeIsP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withDisplayName("null").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when qrCode of businessType is P2P/QR_USER/QR_DEBIT and displayName parameter is not passed in request", groups = {
			"regression" })
	public void TC058_DisplayNameNotPassedWhenBusinessTypeIsP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withDisplayName(null).withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when mappingType is blank in request", groups = { "regression" })
	public void TC059_MappingTypeBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0037);
	}

	@Test(description = "Case to verify when mappingType is null in request", groups = { "regression" })
	public void TC060_MappingTypeAsNullString() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("null").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0037);
	}

	@Test(description = "Case to verify when mappingType parameter not passed in request", groups = { "regression" })
	public void TC061_MappingTypeNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("null").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0037);
	}

	@Test(description = "Case to verify when typeOfQrCode is blank in request", groups = { "regression" })
	public void TC062_TypeOfQrCodeBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();

		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withTypeOfQrCode("").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when typeOfQrCode is null in request", groups = { "regression", "sanity" })
	public void TC063_TypeOfQrCodeAsNullString() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withTypeOfQrCode("null").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when typeOfQrCode parameter not passed in request", groups = { "regression" })
	public void TC064_TypeOfQrCodeNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withTypeOfQrCode(null).withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0021);
	}

	@Test(description = "Case to verify when mappingType is MERCHANT and merchantGuid parameter is blank", groups = {
			"regression" })
	public void TC065_MerchantGuidBlankWhenMappingTypeMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("MERCHANT").withMerchantGuid("").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0019);
	}

	@Test(description = "Case to verify when mappingType is MERCHANT and merchantGuid parameter is null", groups = {
			"regression" })
	public void TC066_MerchantGuidAsNullStringWhenMappingTypeMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withMappingType("MERCHANT").withMerchantGuid("null").withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.GE_0001);
	}

	@Test(description = "Case to verify when mappingType is MERCHANT and merchantGuid parameter is not passed in request", groups = {
			"regression" })
	public void TC067_MerchantGuidNotPassedStringWhenMappingTypeMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(null);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0019);
	}

	@Test(description = "Case to verify when platformName is blank", groups = { "regression" })
	public void TC068_PlateformNameBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withPlateformName("").getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when platformName is null", groups = { "regression" })
	public void TC069_PlateformNameAsNullString() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withPlateformName("null").getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when platformName parameter is not passed in request", groups = {
			"regression" })
	public void TC070_PlateformNameNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withPlateformName(null).getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when platformName is other than PayTM in request", groups = { "regression" })
	public void TC071_PlateformNameOtherThanPaytm() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withPlateformName("Payabc").getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when operationType is blank", groups = { "regression" })
	public void TC072_OperationTypeBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withOperationType("").getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.BADREQUEST);
	}

	@Test(description = "Case to verify when operationType is null", groups = { "regression" })
	public void TC073_OperationTypeAsNullString() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withOperationType("null").getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.BADREQUEST);
	}

	@Test(description = "Case to verify when operationType parameter not passed in request", groups = { "regression" })
	public void TC074_OperationTypeNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withOperationType(null).getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "Case to verify when operationType parameter is other than QR_CODE", groups = { "regression" })
	public void TC075_OperationTypeOtherThanQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().withOperationType("QR_ABCD").getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]).withPhoneNo(phoneNo).withMappingType("USER")
				.withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.BADREQUEST);
	}

	@Test(description = "Case to verify when clientId is not passed in headers", groups = { "regression" })
	public void TC076_ClientIdNotPassedInHeaders() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest, null,
				Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when clientId different", groups = { "regression" })
	public void TC077_ClientIdIsDifferent() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				"d02a898248eb4a2d82873fe8116a5f40", Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when hash is not passed in headers", groups = { "regression" })
	public void TC078_HashNotPassedInHeaders() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, null);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to veirfy when hash is different", groups = { "regression" })
	public void TC079_HashIsDifferent() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo);
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to verify when secondaryPhoneNumber passed is valid", groups = { "regression" })
	public void TC080_secondaryPhoneNumberPassedValid() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE")
				.withSecondaryPhoneNumber(UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET));
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "case to verify when secondaryPhoneNumber passed is invalid", groups = { "regression" })
	public void TC081_invalidSecondaryPhoneNumberPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE").withSecondaryPhoneNumber("2658654324");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0011);
	}

	@Test(description = "case to verify when mid is valid when mappingType is MERCHANT", groups = { "regression" })
	public void TC082_validMidPassWhenMappingTypeMERCHANT() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantMid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}

	@Test(description = "case to verify when mid is invalid when mappingType is MERCHANT", groups = { "regression" })
	public void TC083_invalidMidPassWhenMappingTypeMERCHANT() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantMid("abcd12345")
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.GE_1010);
	}

	@Test(description = "for DYNAMIC_QR_CODE, if qr is not mapped and is not verified then entry should be added in wallet_communication", groups = {
			"regression" })
	public void TC084_verifyEntryInWalletCommunicationWhenQrNotMapped() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo("6360758640").withMappingType("USER").withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0003);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, "6360758640", null);
	}

	@Test(description = "Qr code entry is deleted from redis after mapping", groups = { "regression", "sanity" })
	public void TC085_qrCodeEntryDeletedFromRedisAfterMapping() {
		Redis redis = new Redis();
		Jedis jedis = redis.createWalletRedisConnection();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		redis.setRedisKey(jedis, "DQR_" + qrCodeDetails[1], "1");
		AssertionValidations.verifyAssertNotNull(redis.getRedisKey(jedis, "DQR_" + qrCodeDetails[1]));
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
		AssertionValidations.verifyAssertNull(redis.getRedisKey(jedis, "DQR_" + qrCodeDetails[1]));
	}

	@Test(description = "if already mapped can't be mappped to UTS merchant", groups = { "regression", })
	public void TC086_MapAlreadyMappedQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("UTS").withSourceId("1234");
		mapDynamicQrCode.createRequestJsonAndExecute();
		if (mapDynamicQrCode.getApiResponse().getStatusCode() == 200
				&& mapDynamicQrCode.getResponsePojo().getResponse().getQrCodeId() != null) {
			String phoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.NUMBERWITH5);
			mapDynamicQrCode
					.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
					.withPhoneNo(phoneNumber).withMerchantGuid(MerchantManager.getInstance()
							.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID))
					.withTypeOfQrCode("UTS").withSourceId("1234");
			mapDynamicQrCode.createRequestJsonAndExecute();
			CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0020);
		}
	}
	
	@Test(description = "case to verify when map qr to aggregator merchant", groups = { "regression" })
	public void TC087_MapToAggregatorMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.AGGREGATORMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.DQR_0051);
	}
	
	@Test(description = "case to verify when map qr to child merchant of aggregator merchant", groups = { "regression" })
	public void TC088_MapToChildMerchantOfAggregatorMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("MERCHANT").withMerchantGuid(MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.CHILDMERCHANT, MerchantHeaders.MERCHANTGUID))
				.withTypeOfQrCode("MERCHANT_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC089_InvalidLocaleValue() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.setQueryParam("en-IN");
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDynamicQrCode, APIStatus.MapDynamicQrCode.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC090_ValidLocaleValue() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		MapDynamicQrCode mapDynamicQrCode = new MapDynamicQrCode(MapDynamicQrCode.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		mapDynamicQrCode.setQueryParam("en_IN");
		mapDynamicQrCode.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(qrCodeDetails[1])
				.withPhoneNo(phoneNo).withMappingType("USER").withTypeOfQrCode("USER_QR_CODE");
		mapDynamicQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDynamicQrCode, APIStatus.MapDynamicQrCode.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.mapDynamicQrCodeParameterValidation(mapDynamicQrCode, null,
				mapDynamicQrCode.getResponsePojo().getResponse().getSecondaryPhoneNumber());
	}
}
