package qrCode;


import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.paytm.apiRequestBuilders.wallet.qrCode.MapDeeplinkToQr;
import com.paytm.constants.Constants;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletMerchant-MapDeeplinkToQr")
public class MapDeeplinkToQrTest extends WalletBaseTest {

	@Test(description = "case to verify when valid merchantMid, merchantGuid and deeplink is passed in request", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_ValidMerchantGuidAndMidAndDeeplinkIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when merchantGuid and mid is of different merchant", groups = { "regression" })
	public void TC002_MerchantGuidAndMidOfDifferentMerchant() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.GE_1052);
	}

	@Test(description = "case to verify when merchantMid in request is blank", groups = { "regression" })
	public void TC003_WhenMerchantMidIsBlankInRequest() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid("");
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.GE_1052);
	}

	@Test(description = "case to verify when merchantMID is not passed in request", groups = { "regression" })
	public void TC004_WhenMerchantMidNotPassedInRequest() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(null);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.GE_1052);
	}

	@Test(description = "case to verify when merchantGuid is blank in headers", groups = { "regression" })
	public void TC005_WhenMerchantGuidIsBlankInHeaders() {
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, "");
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.GE_1043);
	}

	@Test(description = "case to verify when merchantGuid not passed in heades", groups = { "regression" })
	public void TC006_WhenMerchantGuidNotPassedInHeaders() {
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, null);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.GE_1043);
	}

	@Test(description = "case to verify when deeplink is blank in request", groups = { "regression" })
	public void TC007_WhenDeeplinkIsBlankInRequest() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withDeepLink("");
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.QR_1031);
	}

	@Test(description = "case to verify when deeplink parameter not passed in request", groups = { "regression" })
	public void TC008_WhenDeeplinkIsNotPassedInRequest() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withDeepLink(null);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.QR_1031);
	}

	@Test(description = "case to verify when stickerId is blank in request", groups = { "regression" })
	public void TC009_WhenStickerIdIsBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is mapped to different merchant with status 1", groups = {
			"regression", "sanity" })
	public void TC010_StickerIdPassedIsMappedToDifferentMerchantWithStatusOne() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "QR_CODE_DEEPLINK", "MERCHANT", "1",
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0020);
	}

	@Test(description = "case to verify when stickerId passed is mapped to same merchant but with typeOfQrCode is MERCHANT_QR_CODE", groups = {
			"regression", "smoke", "sanity" })
	public void TC011_StickerIdPassedMappedToSameMerchantMERCHANT_QR_CODE() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "MERCHANT_QR_CODE", "MERCHANT", "1",
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is mapped to same merchant but with typeOfQrCode is UTS", groups = {
			"regression" })
	public void TC012_StickerIdPassedMappedToSameMerchantUTS() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "UTS", "MERCHANT", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is mapped to same merchant but with typeOfQrCode is QR_CODE_DEEPLINK", groups = {
			"regression" })
	public void TC013_StickerIdPassedMappedToSameMerchantQR_CODE_DEEPLINK() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "QR_CODE_DEEPLINK", "MERCHANT", "1",
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is mapped to same merchant but with typeOfQrCode is USER_QR_CODE", groups = {
			"regression", "sanity" })
	public void TC014_StickerIdPassedMappedToUserUSER_QR_CODE() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("P2P", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "USER_QR_CODE", "USER", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0023);
	}

	@Test(description = "case to verify when stickerId passed is mapped to same merchant but with typeOfQrCode is DYNAMIC_QR_CODE", groups = {
			"regression" })
	public void TC015_StickerIdPassedMappedToUserDYNAMIC_QR_CODE() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("P2P", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "DYNAMIC_QR_CODE", "USER", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0023);
	}

	@Test(description = "case to verify when stickerId passed is mapped to same merchant but with typeOfQrCode is DEBIT_QR_CODE", groups = {
			"regression" })
	public void TC016_StickerIdPassedMappedToUserDEBIT_QR_CODE() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("P2P", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "DEBIT_QR_CODE", "USER", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0023);
	}

	@Test(description = "case to verify when stickerId passed is of unmapped qr of bussinessType P2P", groups = {
			"regression" })
	public void TC017_StickerIdPassedOfUnmappedP2PQR() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("P2P", agentPhoneNumber);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0023);
	}

	@Test(description = "case to verify when stickerId passed is of unmapped qr of bussinessType QR_MERCHANT", groups = {
			"regression" })
	public void TC018_StickerIdPassedOfUnmappedQR_MERCHANTQR() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", agentPhoneNumber);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is of unmapped qr of bussinessType QR_PRODUCT", groups = {
			"regression" })
	public void TC019_StickerIdPassedOfUnmappedQR_PRODUCTQR() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", agentPhoneNumber);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is of unmapped qr of bussinessType QR_PRICEDPRODUCT", groups = {
			"regression" })
	public void TC020_StickerIdPassedOfUnmappedQR_PRICEDPRODUCTQR() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", agentPhoneNumber);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is of unmapped qr of bussinessType QR_ORDER", groups = {
			"regression" })
	public void TC021_StickerIdPassedOfUnmappedQR_ORDERQR() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_ORDER", agentPhoneNumber);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when stickerId passed is of unmapped qr of bussinessType QR_USER", groups = {
			"regression" })
	public void TC022_StickerIdPassedOfUnmappedQR_USERQR() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_USER", agentPhoneNumber);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0023);
	}

	@Test(description = "case to verify when stickerId passed is of unmapped qr of bussinessType QR_DEBIT", groups = {
			"regression", "sanity" })
	public void TC023_StickerIdPassedOfUnmappedQR_DEBITQR() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", agentPhoneNumber);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0023);
	}

	@Test(description = "case to verify when stickerId passed is mapped to different merchant with status 0", groups = {
			"regression" })
	public void TC024_StickerIdPassedIsMappedToDifferentMerchantWithStatusZero() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "QR_CODE_DEEPLINK", "MERCHANT", "0",
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0020);
	}

	@Test(description = "case to verify when stickerId passed is mapped to same merchant with status 0﻿", groups = {
			"regression" })
	public void TC025_StickerPassedISMappedToSameMerchantWithStatusZero() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String qrDetails[] = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", agentPhoneNumber);
		walletAPIHelpers.mapDynamicQrCode(qrDetails, agentPhoneNumber, "QR_CODE_DEEPLINK", "MERCHANT", "0",
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID));
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithStickerId,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStickerId(qrDetails[0]);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when displayName, posId, tagLine, phone number and status blank is passed in request", groups = {
			"regression", "sanity" })
	public void TC026_WhenDisplayNamePosIdTagLinePhoneNoIsPassedButStatusIsBlank() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withDisplayName("Test")
				.withPosId(String.valueOf(System.currentTimeMillis())).withTagLine("automation testing")
				.withPhoneNo(phoneNo);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, true);
	}

	@Test(description = "case to verify when status in request is 1", groups = { "regression" })
	public void TC027_WhenStatusIsOneRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStatus("1").withPhoneNo(null)
				.withTagLine(null).withDisplayName(null).withPosId(null);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when status in request is 0", groups = { "regression" })
	public void TC028_WhenStatusIsZeroRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStatus("0").withPhoneNo(null)
				.withTagLine(null).withDisplayName(null).withPosId(null);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}

	@Test(description = "case to verify when invalid phone number is send in request", groups = { "regression" })
	public void TC029_WhenInvalidPhoneNumberIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withDisplayName("Test")
				.withPosId(String.valueOf(System.currentTimeMillis())).withTagLine("automation testing")
				.withPhoneNo("1234567898");
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, true);
	}

	@Test(description = "case to verify when phone number is blank", groups = { "regression" })
	public void TC030_WhenPhoneNumberBlankIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withDisplayName("Test")
				.withPosId(String.valueOf(System.currentTimeMillis())).withTagLine("automation testing")
				.withPhoneNo("");
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, true);
	}

	@Test(description = "case to verify when status sent in request is other than 0/1", groups = { "regression" })
	public void TC031_StatusIsOtherThanZeroOrOne() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withStatus("abcd").withPhoneNo(null)
				.withTagLine(null).withDisplayName(null).withPosId(null);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.BADREQUEST);
	}

	@Test(description = "Case to verify when clientId is not passed in headers", groups = { "regression" })
	public void TC032_ClientIdNotPassedInHeaders() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter, null,
				Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.UNAUTHORIZEDACCESS);
	}

	@Test(description = "Case to verify when clientId different", groups = { "regression" })
	public void TC033_ClientIdIsDifferent() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				"d02a898248eb4a2d82873fe8116a5f40", Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.UNAUTHORIZEDACCESS);
	}

	@Test(description = "Case to verify when hash is not passed in headers", groups = { "regression" })
	public void TC034_HashNotPassedInHeaders() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, null, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.UNAUTHORIZEDACCESS);
	}

	@Test(description = "Case to veirfy when hash is different", groups = { "regression" })
	public void TC035_HashIsDifferent() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.requestWithAdditinalParameter,
				Constants.SystemVariables.CLIENTID, "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c",
				merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.UNAUTHORIZEDACCESS);
	}

	@Test(description = "case to verify when same deeplink is passed in two concurrent request", groups = {
			"regression" })
	public void TC036_SameDeeplinkIsPassedInTwoRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
		if (mapDeeplinkToQr.getApiResponse().getStatusCode() == 200) {
			String deeplink = mapDeeplinkToQr.getRequestPojo().getRequest().getDeepLink();
			mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid).withDeepLink(deeplink);
			mapDeeplinkToQr.createRequestJsonAndExecute();
			CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.DQR_0016);
		}
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC037_InvalidLocaleValue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.setQueryParam("en-IN");
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC038_ValidLocaleValue() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.NAME);
		MapDeeplinkToQr mapDeeplinkToQr = new MapDeeplinkToQr(MapDeeplinkToQr.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH, merchantGuid);
		mapDeeplinkToQr.setQueryParam("en_IN");
		mapDeeplinkToQr.getRequestPojo().getRequest().withMerchantMid(mid);
		mapDeeplinkToQr.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(mapDeeplinkToQr, APIStatus.MapDeeplinkToQr.SUCCESS);
		qrCodeHelpers.mapDeeplinkToQrValidation(mapDeeplinkToQr, false);
	}
}
