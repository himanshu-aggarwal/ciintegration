package qrCode;

import com.paytm.apiPojo.wallet.qrCode.CatalogForUTSResponse.GridLayout.Attributes;
import com.paytm.apiPojo.wallet.qrCode.GetUTSFareInfoResponse.Response.FareDetail;
import com.paytm.apiRequestBuilders.wallet.qrCode.GetUTSFareInfo;
import com.paytm.assertions.AssertionValidations;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.*;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-GetUTSFareInfo") 
public class GetUTSFareInfoTest extends WalletBaseTest {

	@Test(description = "Case to verify when valid parameters are passed in request", groups = { "regression", "smoke",
			"sanity" })
	public void TC001_WhenAllValidParametersArePassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		List<String> paxList = createPaxList(fareMap);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo, fareMap);
	}

	@Test(description = "case to verify when expired ssoToken is passed in headers", groups = { "regression" })
	public void TC002_ExpiredSsoTokenPassedInHeaders() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest,
				"cb02f143-ebd0-4036-a9ee-e49733027784");
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UNAUTHORIZEDACCESS);
	}

	@Test(description = "case to verify when ssoToken not passed in headers", groups = { "regression" })
	public void TC003_SsoTokenNotPassedInHeaders() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, null);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UNAUTHORIZEDACCESS);
	}

	@Test(description = "case to verify when merchantGuid is blank in request", groups = { "regression" })
	public void TC004_WhenMerchantGuidBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest().withMerchantGuid("").withRouteId(attributes.getRouteId())
				.withRouteName(attributes.getRoute()).withSourceId(attributes.getSourceId())
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2006);
	}

	@Test(description = "case to verify when invalid merchantGuid is passed", groups = { "regression", "smoke",
			"sanity" })
	public void TC005_WhenInvalidMerchantGuidIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		List<String> paxList = createPaxList(fareMap);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest().withMerchantGuid("cd7381a5-01c9-4653-a48c-7611")
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo, fareMap);
	}

	@Test(description = "case to verify when invalid routeId is passed in headers", groups = { "regression" })
	public void TC006_WhenInvalidRouteIdPassedInheaders() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(String.valueOf(System.currentTimeMillis())).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2009);
	}

	@Test(description = "case to verify when routeId is blank in request", groups = { "regression" })
	public void TC007_WhenRouteIdIsBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId("").withRouteName(attributes.getRoute()).withSourceId(attributes.getSourceId())
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2013);
	}

	@Test(description = "case to verify when routeId parameter not passed in request", groups = { "regression" })
	public void TC008_WhenRouteIdParameterNotPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(null).withRouteName(attributes.getRoute()).withSourceId(attributes.getSourceId())
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2013);
	}

	@Test(description = "case to verify when routeName is blank in request", groups = { "regression" })
	public void TC009_WhenRouteNameIsBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName("").withSourceId(attributes.getSourceId())
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2014);
	}

	@Test(description = "case to verify when routeName is not passed in request", groups = { "regression" })
	public void TC010_WhenRouteNameNotPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(null).withSourceId(attributes.getSourceId())
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2014);
	}

	@Test(description = "case to verify when valid routeId and invalid routeName is passed in request", groups = {
			"regression", "sanity" })
	public void TC011_WhenValidRouteIdAndInvalidRouteNameIsPassedInHeaders() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		List<String> paxList = createPaxList(fareMap);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId())
				.withRouteName(attributes.getRoute() + String.valueOf(System.currentTimeMillis()))
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo, fareMap);
	}

	@Test(description = "case to verify when invalid sourceId is passed in request", groups = { "regression" })
	public void TC012_WhenInvalidSourceIdIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(String.valueOf(System.currentTimeMillis())).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2009);
	}

	@Test(description = "case to verify when sourceId in blank in request", groups = { "regression" })
	public void TC013_WhenSoucreIdIsBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute()).withSourceId("")
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2002);
	}

	@Test(description = "case to verify when sourceId parameter not passed in request", groups = { "regression" })
	public void TC014_WhenSourceIdNotPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute()).withSourceId(null)
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2002);
	}

	@Test(description = "case to verify when sourceName is blank in request", groups = { "regression" })
	public void TC015_WhenSourceNameIsBlankRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName("")
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2010);
	}

	@Test(description = "case to verify when sourceName not passed in request", groups = { "regression" })
	public void TC016_WhenSourceNameNotPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(null)
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2010);
	}

	@Test(description = "case to verify when sourceId is valid but sourceName is invalid", groups = { "regression",
			"sanity" })
	public void TC017_WhenValidSourceIdAndInvalidSourceNameIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		List<String> paxList = createPaxList(fareMap);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId())
				.withSourceName(attributes.getSource() + String.valueOf(System.currentTimeMillis()))
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo, fareMap);
	}

	@Test(description = "case to verify when invalid destinationId is passed in request", groups = { "regression" })
	public void TC018_WhenInvalidDestinationIdIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(String.valueOf(System.currentTimeMillis()))
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2009);
	}

	@Test(description = "case to verify when destinationId is blank in request", groups = { "regression" })
	public void TC019_WhenDestinationIdIsBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource()).withDestinationId("")
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2003);
	}

	@Test(description = "case to verify when destinationId not passed in request", groups = { "regression" })
	public void TC020_WhenDestinationIdIsNotPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource()).withDestinationId(null)
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2003);
	}

	@Test(description = "case to verify when destinationName is blank in request", groups = { "regression" })
	public void TC021_WhenDestinationNameIsBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName("")
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2011);
	}

	@Test(description = "case to verify when destinationName not passed in request", groups = { "regression" })
	public void TC022_WhenDestinationNameNotPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(null)
				.withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2011);
	}

	@Test(description = "case to verify when destinationId is valid but destinationName is invalid", groups = {
			"regression", "sanity" })
	public void TC023_WhenValidDestinationIdAndInvalidDestinationNameIsPassed() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		List<String> paxList = createPaxList(fareMap);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination() + String.valueOf(System.currentTimeMillis()))
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo, fareMap);
	}

	@Test(description = "case to verify when busType is blank in request", groups = { "regression" })
	public void TC024_WhenBusTypeIsBlankInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType("");
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2004);
	}

	@Test(description = "case to verify when busType not passed in request", groups = { "regression" })
	public void TC025_WhenBusTypeNotPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(null);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2004);
	}

	@Test(description = "case to verify when invalid busType is passed in request", groups = { "regression" })
	public void TC026_WhenInvalidBusTypeIsPassedInRequest() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType() + String.valueOf(System.currentTimeMillis()));
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2007);
	}

	@Test(description = "case to verify when pax array is empty", groups = { "regression" })
	public void TC027_WhenPaxArrayIsEmpty() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(new ArrayList<String>());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2005);
	}

	@Test(description = "case to verify when invalid pax type is present in pax array", groups = { "regression" })
	public void TC028_WhenInvalidPaxTypeIsPresentInPaxArray() {
		List<String> paxList = new ArrayList<>();
		paxList.add("Adult");
		paxList.add("ABCD");
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2008);
	}

	@Test(description = "case to verify when sending same pax type more than once in pax array", groups = {
			"regression" })
	public void TC029_WhenSamePaxTypeIsSendMoreThanOnce() {
		List<String> paxList = new ArrayList<>();
		paxList.add("Adult");
		paxList.add("Adult");
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo, fareMap);
	}

	@Test(description = "case to verify when sourceId & destinationId are of same route but routeId is of different route", groups = {
			"regression" })
	public void TC030_WhenSourceAndDestinationIdOfSameRouteButRouteIdDifferent() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId("53").withRouteName(attributes.getRoute()).withSourceId(attributes.getSourceId())
				.withSourceName(attributes.getSource()).withDestinationId(attributes.getDestinationId())
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2009);
	}

	@Test(description = "case to verify when routeId & sourceId are of same route but destinationId is of different route", groups = {
			"regression" })
	public void TC031_WhenSourceAndRouteIdOfSameRouteButDestinationIdDifferent() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(String.valueOf(System.currentTimeMillis()))
				.withDestinationName(attributes.getDestination()).withBusType(attributes.getBusType());
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.UTS_2009);
	}
	
	@Test(description = "case to verify when sending invalid locale value")
	public void TC031_InvalidLocaleValue() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		List<String> paxList = createPaxList(fareMap);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.setQueryParam("en-IN");
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo, fareMap);
	}

	
	@Test(description = "case to verify when sending valid local value")
	public void TC033_ValidLocaleValue() {
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
		Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
		List<String> paxList = createPaxList(fareMap);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetUTSFareInfo getUTSFareInfo = new GetUTSFareInfo(GetUTSFareInfo.defaultRequest, token);
		getUTSFareInfo.setQueryParam("en_IN");
		getUTSFareInfo.getRequestPojo().getRequest()
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withRouteId(attributes.getRouteId()).withRouteName(attributes.getRoute())
				.withSourceId(attributes.getSourceId()).withSourceName(attributes.getSource())
				.withDestinationId(attributes.getDestinationId()).withDestinationName(attributes.getDestination())
				.withBusType(attributes.getBusType()).withPax(paxList);
		getUTSFareInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getUTSFareInfo, APIStatus.GetUTSFareInfo.SUCCESS);
		getUTSFareInfoValidation(getUTSFareInfo,fareMap);
	}
	
	/**
	 * Used in GetUTSFareInfo Code Test Cases for Response validation This function contains all the validations
	 *
	 * @param getUTSFareInfo - Object of the GetUtsFareInfo to get the response for validation
	 * @param fareMap2 - Details for the root for validation
	 */
	private void getUTSFareInfoValidation(GetUTSFareInfo getUTSFareInfo, Map<String, Integer> fareMap) {
		List<FareDetail> fareDetails = getUTSFareInfo.getResponsePojo().getResponse().getFareDetails();

		for (FareDetail fareDetail : fareDetails) {
			AssertionValidations.verifyAssertEqual(fareDetail.getFare(), fareMap.get(fareDetail.getPax()));
		}

		AssertionValidations.verifyAssertEqual(
				String.valueOf(getUTSFareInfo.getResponsePojo().getResponse().getSourceId()),
				getUTSFareInfo.getRequestPojo().getRequest().getSourceId());
		AssertionValidations.verifyAssertEqual(getUTSFareInfo.getResponsePojo().getResponse().getSourceName(),
				getUTSFareInfo.getRequestPojo().getRequest().getSourceName());
		AssertionValidations.verifyAssertEqual(
				String.valueOf(getUTSFareInfo.getResponsePojo().getResponse().getDestinationId()),
				getUTSFareInfo.getRequestPojo().getRequest().getDestinationId());
		AssertionValidations.verifyAssertEqual(getUTSFareInfo.getResponsePojo().getResponse().getDestinationName(),
				getUTSFareInfo.getRequestPojo().getRequest().getDestinationName());
		AssertionValidations.verifyAssertEqual(
				String.valueOf(getUTSFareInfo.getResponsePojo().getResponse().getRouteId()),
				getUTSFareInfo.getRequestPojo().getRequest().getRouteId());
		AssertionValidations.verifyAssertEqual(getUTSFareInfo.getResponsePojo().getResponse().getRouteName(),
				getUTSFareInfo.getRequestPojo().getRequest().getRouteName());
		AssertionValidations.verifyAssertEqual(getUTSFareInfo.getResponsePojo().getResponse().getBusType(),
				getUTSFareInfo.getRequestPojo().getRequest().getBusType());
	}
	
	/**
	 * Used to create the pax list send in request
	 *
	 * @param fareMap - contains fare for each pax
	 */
	private List<String> createPaxList(Map<String, Integer> fareMap){
		List<String> paxList = new ArrayList<>();
		
		for(String key : fareMap.keySet()) {
			paxList.add(key);
		}
		
		return paxList;
	}
}
