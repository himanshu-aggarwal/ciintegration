package qrCode;


//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;


import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.paytm.apiRequestBuilders.wallet.qrCode.FetchQrCodeDetails;
import com.paytm.constants.Constants;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-FetchQrCodeDetails")
public class FetchQrCodeDetailsTest extends WalletBaseTest {

    @Test(description = "Case to verify when valid qrCodeId is passed having mapping_status 1 and status 1", groups = {"regression", "smoke", "sanity"})
    public void TC001_QrCodeMappingStatusAndStatusOne() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails,APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);
    }

    @Test(description = "Case to verify when valid qrCodeId is passed having mapping_status 0", groups = {"regression", "sanity"})
    public void TC002_QrCodeMappingStatusZero() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.DQR_0050);
    }

    @Test(description = "Case to verify when all parameter is blank in request", groups = {"regression", "smoke", "sanity"})
    public void TC003_QrCodeIdBalnk() {
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId("");
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.DQR_0049);
    }

    @Test(description = "Case to verify when qrCodeId parameter is null in request", groups = {"regression"})
    public void TC004_QrCodeIdPassedAsNullString() {
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId("null");
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.DQR_0050);
    }

    @Test(description = "Case to verify when invalid qrCodeId is passed in reqeust", groups = {"regression"})
    public void TC005_InvalidQrCodeIdPassed() {
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId("abcdefgh");
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.DQR_0050);
    }

    @Test(description = "Case to verify when qrCodeId is not passed in the request", groups = {"regression"})
    public void TC006_QrCodeIdNotPassed() {
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(null);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.DQR_0049);
    }

    @Test(description = "Case to verify when plateformName is blank in request", groups = {"regression"})
    public void TC007_PlateformNameIsBlank() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withPlateformName("").getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);
    }

    @Test(description = "Case to verify when plateformName is passed as null string in request", groups = {"regression"})
    public void TC008_PlateformNameAsNullString() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withPlateformName("null").getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);        
    }

    @Test(description = "Case to verify when plateformName is other than PayTM", groups = {"regression"})
    public void TC009_PlateformNameIsOtherThanPayTm() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withPlateformName("abcd").getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);     
    }

    @Test(description = "Case to verify when operationType is blank in request", groups = {"regression", "sanity"})
    public void TC010_OperationTypeBlank() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withOperationType("").getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,APIStatus.FetchQrCodeDetails.BADREQUEST);
    }

    @Test(description = "Case to verify when operationType is passed as null string in request", groups = {"regression"})
    public void TC011_OperationTypeAsNullString() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withOperationType("null").getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,APIStatus.FetchQrCodeDetails.BADREQUEST);
    }

    @Test(description = "Case to verify when operationType is other than QR_CODE", groups = {"regression"})
    public void TC012_OperationTypeOtherThanQRCODE() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withOperationType("abcd").getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,APIStatus.FetchQrCodeDetails.BADREQUEST);
    }

    @Test(description = "Case to verify when valid qrCodeId is passed having status 0 in dynamic_qr_code table", groups = {"regression", "sanity"})
    public void TC013_QrCodeStatusZERO() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "0");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);   
    }

    @Test(description = "Case to verify when plateformName parameter not passed in request", groups = {"regression"})
    public void TC014_PlateformNameNotPassed() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withPlateformName(null).getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);
    }

    @Test(description = "Case to verify when operationType parameter not passed in request", groups = {"regression"})
    public void TC015_OperationTypeNotPassed() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withOperationType(null).getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails, APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);
    }

    @Test(description = "Case to verify when clientId is not passed in headers", groups = {"regression"})
    public void TC016_ClientIdNotPassedInHeaders() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, null, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withOperationType(null).getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,
				APIStatus.FetchQrCodeDetails.UNAUTHORIZED_ACCESS);
    }

    @Test(description = "Case to verify when clientId different", groups = {"regression"})
    public void TC017_ClientIdIsDifferent() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, "d02a898248eb4a2d82873fe8116a5f40",
                Constants.SystemVariables.HASH);
        fetchQrCodeDetails.getRequestPojo().withOperationType(null).getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,
				APIStatus.FetchQrCodeDetails.UNAUTHORIZED_ACCESS);
    }

    @Test(description = "Case to verify when hash is not passed in headers", groups = {"regression"})
    public void TC018_HashNotPassedInHeaders() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, null);
        fetchQrCodeDetails.getRequestPojo().withOperationType(null).getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,
				APIStatus.FetchQrCodeDetails.UNAUTHORIZED_ACCESS);
    }

    @Test(description = "Case to veirfy when hash is different", groups = {"regression"})
    public void TC019_HashIsDifferent() {
        String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID,
                "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
        fetchQrCodeDetails.getRequestPojo().withOperationType(null).getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,
				APIStatus.FetchQrCodeDetails.UNAUTHORIZED_ACCESS);
    }
    
    @Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC020_InvalidLocaleValue() {
    	String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.setQueryParam("en-IN");
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().httpCodeAssert(fetchQrCodeDetails,APIStatus.FetchQrCodeDetails.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC021_ValidLocaleValue() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
        FetchQrCodeDetails fetchQrCodeDetails = new FetchQrCodeDetails(FetchQrCodeDetails.defaultRequest, Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
        fetchQrCodeDetails.setQueryParam("en_IN");
        fetchQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
        fetchQrCodeDetails.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(fetchQrCodeDetails,APIStatus.FetchQrCodeDetails.SUCCESS);
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        qrCodeHelpers.fetchQrCodeDetailsValidation(fetchQrCodeDetails);
	}
}
