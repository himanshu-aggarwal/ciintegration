package qrCode;

import com.paytm.apiRequestBuilders.wallet.qrCode.EditQrCodeDetails;
import com.paytm.constants.Constants;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;


import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-EditQRCodeDetails")
public class EditQrCodeDetailsTest extends WalletBaseTest {

	@Test(description = "Edit displayName of QR Code for P2P BusineesType.", groups = { "regression", "smoke",
			"sanity" })
	public void TC001_EditDisplayNameOfQrCodeForP2PBusinessType() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When tagline parameter is not passed in request", groups = { "regression" })
	public void TC002_TagLineNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withTagLine(null);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When posId parameter is not passed in request", groups = { "regression" })
	public void TC003_PosIdNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withPosId(null);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When secondaryPhoneNumber parameter is not passed in request", groups = { "regression",
			"sanity" })
	public void TC004_SecondaryPhoneNumberNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withSecondaryPhoneNumber(null);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When sourceName parameter is not passed in request", groups = { "regression" })
	public void TC005_SourceNameNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withSourceName(null);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When sourceId parameter is not passed in request", groups = { "regression" })
	public void TC006_SourceIdNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withSourceId(null);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When deepLink parameter is not passed in request", groups = { "regression" })
	public void TC007_DeepLinkNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink(null);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When activationStatus parameter is not passed in request", groups = { "regression" })
	public void TC008_ActivationStatusNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withActivationStatus(null);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When displayName parameter is not passed in request", groups = { "regression" })
	public void TC009_DisplayNameNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName(null)
				.withActivationStatus(0);
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "status", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When ipAddress parameter is not passed in request", groups = { "regression" })
	public void TC010_IpAddressNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withIpAddress(null).getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When platformName parameter is not passed in request", groups = { "regression" })
	public void TC011_PlateformNameNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withPlateformName(null).getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "When operationType parameter is not passed in request", groups = { "regression" })
	public void TC012_OperationTypeNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withOperationType(null).getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "Case to verify when plateformName is null in request", groups = { "regression" })
	public void TC013_PlateformNameAsNullString() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withPlateformName("null").getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "Case to verify when plateformName is other than PayTM", groups = { "regression" })
	public void TC014_PlateformNameOtherThanPayTM() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withPlateformName("abcd").getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "Case to verify when operationType is blank in request", groups = { "regression" })
	public void TC015_OperationTypeBlankInRequest() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withOperationType("").getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails, APIStatus.EditQrCodeDetails.BADREQUEST);
	}

	@Test(description = "Case to verify when operationType as null string in request", groups = { "regression",
			"sanity" })
	public void TC016_OperationTypeAsNullString() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withOperationType("null").getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails, APIStatus.EditQrCodeDetails.BADREQUEST);
	}

	@Test(description = "Case to verify when operationType is other than QR_CODE", groups = { "regression" })
	public void TC017_OperationTypeOtherThanQrCode() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().withOperationType("ABCD").getRequest().withQrCodeId(qrCodeDetails[1])
				.withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails, APIStatus.EditQrCodeDetails.BADREQUEST);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as USER_QR_CODE and business type as P2P", groups = {
			"regression" })
	public void TC018_TypeOfQrCodeUserQrCodeAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as USER_QR_CODE and business type as QR_USER", groups = {
			"regression" })
	public void TC019_TypeOfQrCodeUserQrCodeAndBusinessTypeQrUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as USER_QR_CODE and business type as QR_DEBIT", groups = {
			"regression" })
	public void TC020_TypeOfQrCodeUserQrCodeAndBusinessTypeQrDebit() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as DYNAMIC_QR_CODE and business type as P2P", groups = {
			"regression", "smoke", "sanity" })
	public void TC021_TypeOfQrCodeDynamicQrCodeAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "DYNAMIC_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "DYNAMIC_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as DYNAMIC_QR_CODE and business type as QR_USER", groups = {
			"regression" })
	public void TC022_TypeOfQrCodeDynamicQrCodeAndBusinessTypeQrUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "DYNAMIC_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "DYNAMIC_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as DYNAMIC_QR_CODE and business type as QR_DEBIT", groups = {
			"regression" })
	public void TC023_TypeOfQrCodeDynamicQrCodeAndBusinessTypeQrDebit() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "DYNAMIC_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "DYNAMIC_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as DEBIT_QR_CODE and business type as P2P", groups = {
			"regression" })
	public void TC024_TypeOfQrCodeDebitQrCodeAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "DEBIT_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "DEBIT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as DEBIT_QR_CODE and business type as QR_USER", groups = {
			"regression" })
	public void TC025_TypeOfQrCodeDebitQrCodeAndQrUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "DEBIT_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "DEBIT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as DEBIT_QR_CODE and business type as QR_DEBIT", groups = {
			"regression" })
	public void TC026_TypeOfQrCodeDebitQrCodeAndBusinessTypeQrDebit() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "DEBIT_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "DEBIT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as MERCHANT_QR_CODE and businessType as P2P", groups = {
			"regression" })
	public void TC027_TypeOfQrCodeMerchantQrCodeAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "MERCHANT_QR_CODE", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withSecondaryPhoneNumber(UserManager.getInstance().getNewPayerUser(UserTypes.NUMBERWITH5));
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "MERCHANT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as MERCHANT_QR_CODE and businessType as QR_MERCHANT", groups = {
			"regression" })
	public void TC028_TypeOfQrCodeMerchantQrCodeAndBusinessTypeQrMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "MERCHANT_QR_CODE", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "MERCHANT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as MERCHANT_QR_CODE and businessType as QR_PRODUCT", groups = {
			"regression" })
	public void TC029_TypeOfQrCodeMerchantQrCodeAndBusinessTypeQrProduct() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "MERCHANT_QR_CODE", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "MERCHANT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as MERCHANT_QR_CODE and businessType as QR_PRICEDPRODUCT", groups = {
			"regression" })
	public void TC030_TypeOfQrCodeMerchantQrCodeAndBusinessTypeQrPricedProduct() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "MERCHANT_QR_CODE", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "MERCHANT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as MERCHNAT_QR_CODE and businessType as QR_ORDER", groups = {
			"regression" })
	public void TC031_TypeOfQrCodeMerchantQrCodeAndBusinessTypeQrOrder() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_ORDER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "MERCHANT_QR_CODE", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "MERCHANT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as MERCHNAT_QR_CODE and businessType as QR_USER", groups = {
			"regression" })
	public void TC032_TypeOfQrCodeMerchantQrCodeAndBusinessTypeQrUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "MERCHANT_QR_CODE", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "MERCHANT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as MERCHNAT_QR_CODE and businessType as QR_DEBIT", groups = {
			"regression" })
	public void TC033_TypeOfQrCodeMerchantQrCodeAndBusinessTypeQrDebit() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "MERCHANT_QR_CODE", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "MERCHANT_QR_CODE", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as UTS and businessType as P2P", groups = {
			"regression" })
	public void TC034_TypeOfQrCodeUTSAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "UTS", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "UTS", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as UTS and businessType as QR_MERCHANT", groups = {
			"regression", "sanity" })
	public void TC035_TypeOfQrCodeUTSAndBusinessTypeQrMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "UTS", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "UTS", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as UTS and businessType as QR_PRODUCT", groups = {
			"regression" })
	public void TC036_TypeOfQrCodeUTSAndBusinessTypeQrProduct() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "UTS", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "UTS", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as UTS and businessType as QR_PRICEDPRODUCT", groups = {
			"regression" })
	public void TC037_TypeOfQrCodeUTSAndBusinessTypeQrPricedProduct() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "UTS", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "UTS", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as UTS and businessType as QR_ORDER", groups = {
			"regression" })
	public void TC038_TypeOfQrCodeUTSAndBusinessTypeQrOrder() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_ORDER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "UTS", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "UTS", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as UTS and businessType as QR_USER", groups = {
			"regression" })
	public void TC039_TypeOfQrCodeUTSAndBusinessTypeQrUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "UTS", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "UTS", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as UTS and businessType as QR_DEBIT", groups = {
			"regression" })
	public void TC040_TypeOfQrCodeUTSAndBusinessTypeQrDebit() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "UTS", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "UTS", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as QR_CODE_DEEPLINK and businessType as P2P", groups = {
			"regression", "sanity" })
	public void TC041_TypeOfQrCodeQrCodeDeeplinkAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "QR_CODE_DEEPLINK", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink("http://google.com");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "QR_CODE_DEEPLINK", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as QR_CODE_DEEPLINK and businessType as QR_MERCHANT", groups = {
			"regression" })
	public void TC042_TypeOfQrCodeQrCodeDeeplinkAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "QR_CODE_DEEPLINK", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink("http://google.com");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "QR_CODE_DEEPLINK", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as QR_CODE_DEEPLINK and businessType as QR_PRODUCT", groups = {
			"regression" })
	public void TC043_TypeOfQrCodeQrCodeDeeplinkAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRODUCT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "QR_CODE_DEEPLINK", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink("http://google.com");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "QR_CODE_DEEPLINK", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as QR_CODE_DEEPLINK and businessType as QR_PRICEDPRODUCT", groups = {
			"regression" })
	public void TC044_TypeOfQrCodeQrCodeDeeplinkAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_PRICEDPRODUCT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "QR_CODE_DEEPLINK", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink("http://google.com");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "QR_CODE_DEEPLINK", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as QR_CODE_DEEPLINK and businessType as QR_ORDER", groups = {
			"regression" })
	public void TC045_TypeOfQrCodeQrCodeDeeplinkAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_ORDER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "QR_CODE_DEEPLINK", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink("http://google.com");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "QR_CODE_DEEPLINK", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as QR_CODE_DEEPLINK and businessType as QR_USER", groups = {
			"regression" })
	public void TC046_TypeOfQrCodeQrCodeDeeplinkAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_USER", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "QR_CODE_DEEPLINK", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink("http://google.com");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "QR_CODE_DEEPLINK", phoneNo);
	}

	@Test(description = "Edit QR Code details having typeOfQrCode as QR_CODE_DEEPLINK and businessType as QR_DEBIT", groups = {
			"regression" })
	public void TC047_TypeOfQrCodeQrCodeDeeplinkAndBusinessTypeP2P() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("QR_DEBIT", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "QR_CODE_DEEPLINK", "MERCHANT", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11")
				.withDeepLink("http://google.com");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "QR_CODE_DEEPLINK", phoneNo);
	}

	@Test(description = "Edit QR Code details when the mapping status is 0 for QR Code", groups = { "regression" })
	public void TC048_EditQrCodeWithMappingStatusZero() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeId).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.DQR_1016);
		QrCodeHelpers.getInstance();
		QrCodeHelpers.setUnMappedQrCode(null);
	}

	@Test(description = "Edit QR Code details when status in dynamic_qr_code table is 0", groups = { "regression" })
	public void TC049_QrCodeWithStatusZero() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "0");
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeId).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers.getInstance();
		QrCodeHelpers.setInactiveQRCodeId(null);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", null);
	}

	@Test(description = "Case to verify when clientId not passed in headers", groups = { "regression" })
	public void TC050_ClientIdNotPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest, null,
				Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails,
				APIStatus.EditQrCodeDetails.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when clientId different", groups = { "regression" })
	public void TC051_ClientIdIsDifferent() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				"d02a898248eb4a2d82873fe8116a5f40", Constants.SystemVariables.HASH);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails,
				APIStatus.EditQrCodeDetails.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when hash is not passed in headers", groups = { "regression" })
	public void TC052_HashNotPassedInHeaders() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, null);
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails,
				APIStatus.EditQrCodeDetails.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to veirfy when hash is different", groups = { "regression" })
	public void TC053_HashIsDifferent() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails,
				APIStatus.EditQrCodeDetails.UNAUTHORIZED_ACCESS);
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC054_InvalidLocaleValue() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.setQueryParam("en-IN");
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(editQrCodeDetails, APIStatus.EditQrCodeDetails.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC055_ValidLocaleValue() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phoneNo = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phoneNo);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetails, phoneNo, "USER_QR_CODE", "USER", "1",MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		EditQrCodeDetails editQrCodeDetails = new EditQrCodeDetails(EditQrCodeDetails.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		editQrCodeDetails.setQueryParam("en_IN");
		editQrCodeDetails.getRequestPojo().getRequest().withQrCodeId(qrCodeDetails[1]).withDisplayName("vijay11");
		editQrCodeDetails.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(editQrCodeDetails, APIStatus.EditQrCodeDetails.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.editQrCodeDetailsValidation(editQrCodeDetails, "display_name", "USER_QR_CODE", phoneNo);
	}
}
