package qrCode;


import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.paytm.apiRequestBuilders.wallet.qrCode.CreateQrCode;
import com.paytm.constants.Constants;
import com.paytm.datamanager.MerchantManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-V4CreateQrCode")
public class CreateQrCodeTest extends WalletBaseTest {

	private String startTime = " 00:00:00";

	@Test(description = "when valid GUID in the request and mid is blank and QrCode type is QR_ORDER", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_QrOrderTypeWithValidGuidAndBlankMid() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QrCode is QR_ORDER when imageRequired parameter is removed", groups = { "regression", "smoke",
			"sanity" })
	public void TC002_QrOrderWithNoImageRequiredParameter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis())).withImageRequired(null);
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}

	@Test(description = "QR_MERCHANT when amount is blank", groups = { "regression", "sanity" })
	public void TC003_QrMerchantWithAmountBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_MERCHANT").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis())).withAmount("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.GE_1007);
	}

	@Test(description = "creating UPI_QR_CODE with mandate parameter", groups = { "regression", "sanity" })
	public void TC004_UpiQrCodeWithValidParamter() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.getRequestPojo().getRequest().withRequestType("UPI_QR_CODE").withMerchantGuid(merchantGuid)
				.withOrderId("QR" + String.valueOf(System.currentTimeMillis()))
				.withPosId(String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate);
	}

	@Test(description = "when no expry is passed", groups = { "regression", "sanity" })
	public void TC005_QrPricedProductWithExpiryDateBlank() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_PRICEDPRODUCT").withMerchantGuid(merchantGuid)
				.withMid("").withProductId("PRO" + String.valueOf(System.currentTimeMillis())).withExpiryDate("");
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				null);
	}

	@Test(description = "Case to verify when clientId is not passed in headers", groups = { "regression" })
	public void TC006_ClientIdNotPassedInHeaders() {
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, null, Constants.SystemVariables.HASH);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when clientId different", groups = { "regression" })
	public void TC007_ClientIdIsDifferent() {
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, "d02a898248eb4a2d82873fe8116a5f40",
				Constants.SystemVariables.HASH);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when hash is not passed in headers", groups = { "regression" })
	public void TC008_HashNotPassedInHeaders() {
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				null);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to veirfy when hash is different", groups = { "regression" })
	public void TC009_HashIsDifferent() {
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				"20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateDynamicQRCode.UNAUTHORIZED_ACCESS);
	}
	
	@Test(description = "case to verify when map qr to aggregator merchant", groups = { "regression" })
	public void TC010_MapToAggregatorMerchant() {
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.AGGREGATORMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_1034);
	}

	@Test(description = "case to verify when map qr to child merchant of aggregator merchant", groups = {
			"regression" })
	public void TC011_MapToChildMerchantOfAggregatorMerchant() {
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER")
				.withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CHILDMERCHANT,
						MerchantHeaders.MERCHANTGUID))
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CHILDMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC012_InvalidLocaleValue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.setQueryParam("en-IN");
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(createQrCode, APIStatus.CreateQrCode.INTERNALSERVERERROR);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC013_ValidLocaleValue() {
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		CreateQrCode createQrCode = new CreateQrCode(CreateQrCode.defaultRequest, Constants.SystemVariables.CLIENTID,
				Constants.SystemVariables.HASH);
		createQrCode.setQueryParam("en_IN");
		createQrCode.getRequestPojo().getRequest().withRequestType("QR_ORDER").withMerchantGuid(merchantGuid)
				.withMid("").withOrderId("QR" + String.valueOf(System.currentTimeMillis()));
		createQrCode.createRequestJsonAndExecute();
		QrCodeHelpers qrHelper = new QrCodeHelpers();
		String expiryDate = qrHelper.getDate("withoutTime", 7);
		CommonValidation.getInstance().basicAsserts(createQrCode, APIStatus.CreateQrCode.QR_0001);
		qrHelper.createQrCodeParameterValidation(createQrCode,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.APIACCESSMERCHANT, MerchantHeaders.ID),
				expiryDate + startTime);
	}
}
