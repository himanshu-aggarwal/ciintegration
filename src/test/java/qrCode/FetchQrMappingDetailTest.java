package qrCode;


import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.paytm.apiRequestBuilders.wallet.qrCode.FetchQrMappingDetail;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.Constants;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-FetchQrMappingDetail")
public class FetchQrMappingDetailTest extends WalletBaseTest {

	@Test(description = "case to verify when valid stickerId is passed and qrCodeId is blank in request", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_ValidStickerIdIsPassedButQrCodeIdIsBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phone);
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId("");
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "case to verify when valid stickerId and qrCodeId is passed in request", groups = {
			"regression" })
	public void TC002_ValidStickerIdAndQrCodeIdIsPassedInRequest() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phone);
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0])
				.withQrCodeId(qrCodeDetails[1]);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "case to verify when valid qrCodeId is passed but stickerId is blank", groups = {
			"regression" })
	public void TC003_ValidQrCodeIdIsPassedButStickerIdIsBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phone);
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId("").withQrCodeId(qrCodeDetails[1]);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.BADREQUEST);
	}

	@Test(description = "case to verify when both qrCodeId and stickerId is blank", groups = {
			"regression" })
	public void TC004_BothQrCodeIdAndStickerIdIsBlank() {
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId("").withQrCodeId("");
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.BADREQUEST);
	}

	@Test(description = "case to verify when valid qrCodeId is passed but stickerId parameter not passed in request", groups = {
			"regression" })
	public void TC005_ValidQrCodeIdButNoStickerIdParameter() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "0");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.qrCodeIdRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "case to verify when valid stickerId is passed but qrCodeId parameter not passed in request", groups = {
			"regression" })
	public void TC006_ValidStickerIdButNoQrCodeIdParameter() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phone);
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId(null);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "case to verify when both stickerId and qrCodeId parameter not passed in request", groups = {
			"regression", "sanity" })
	public void TC007_StickerIdAndQrCodeIdParameterNotPassedInRequest() {
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId(null).withQrCodeId(null);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		AssertionValidations.verifyAssertNull(fetchQrMappingDetail.getResponsePojo().getResponse().getStickerId());
		AssertionValidations.verifyAssertNull(fetchQrMappingDetail.getResponsePojo().getResponse().getQrCodeId());
		AssertionValidations.verifyAssertNull(fetchQrMappingDetail.getResponsePojo().getResponse().getBatchId());
		AssertionValidations.verifyAssertEqual(fetchQrMappingDetail.getResponsePojo().getResponse().getMappingStatus(),
				"0");
	}

	@Test(description = "case to verify when qrCodeId is passed in request of unmapped qrCode", groups = {
			"regression", "sanity" })
	public void TC008_ValidQrCodeIdOfUnmappedQr() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.qrCodeIdRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "case to verify when qrCodeId is passed in request of mapped qrCode with status 1", groups = {
			"regression"})
	public void TC009_ValidQrCodeIdOfMappedQrWithStatusOne() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.qrCodeIdRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "case to verify when qrCodeId is passed in request of mapped qrCode with status 0", groups = {
			"regression", "sanity" })
	public void TC010_ValidQrCodeIdOfMappedQrWithStatusZero() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "0");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.qrCodeIdRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "case to verify when batch Id is null for the given qrCodeId", groups = {
			"regression", "smoke", "sanity" })
	public void TC011_WhenBatchIdForQrCodeIsNull() {
		List<String> operationType = new ArrayList<String>();
		operationType.add("CREATE");
		operationType.add("MAP");
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String qrCodeId = walletAPIHelpers.generateQrCode(operationType);
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.qrCodeIdRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}

	@Test(description = "Case to verify when clientId is not passed in headers", groups = { "regression" })
	public void TC012_ClientIdNotPassedInHeaders() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest, null,
				Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrMappingDetail,
				APIStatus.FetchQrMappingDetail.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when clientId different", groups = { "regression" })
	public void TC013_ClientIdIsDifferent() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				"d02a898248eb4a2d82873fe8116a5f40", Constants.SystemVariables.HASH);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrMappingDetail,
				APIStatus.FetchQrMappingDetail.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to verify when hash is not passed in headers", groups = { "regression" })
	public void TC014_HashNotPassedInHeaders() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, null);
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrMappingDetail,
				APIStatus.FetchQrMappingDetail.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "Case to veirfy when hash is different", groups = { "regression" })
	public void TC015_HashIsDifferent() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
		fetchQrMappingDetail.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrMappingDetail,
				APIStatus.FetchQrMappingDetail.UNAUTHORIZED_ACCESS);
	}
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC016_InvalidLocaleValue() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phone);
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.setQueryParam("en-IN");
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId("");
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC017_ValidLocaleValue() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String[] qrCodeDetails = walletAPIHelpers.createDynamicQrCode("P2P", phone);
		FetchQrMappingDetail fetchQrMappingDetail = new FetchQrMappingDetail(FetchQrMappingDetail.defaultRequest,
				Constants.SystemVariables.CLIENTID, Constants.SystemVariables.HASH);
		fetchQrMappingDetail.setQueryParam("en_IN");
		fetchQrMappingDetail.getRequestPojo().getRequest().withStickerId(qrCodeDetails[0]).withQrCodeId("");
		fetchQrMappingDetail.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fetchQrMappingDetail, APIStatus.FetchQrMappingDetail.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.fetchQrMappingDetailValidation(fetchQrMappingDetail);
	}
}

