package qrCode;

import com.paytm.assertions.AssertionValidations;
import com.paytm.utils.Redis;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.paytm.apiRequestBuilders.wallet.qrCode.AssignQrCode;
import com.paytm.constants.Constants;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import redis.clients.jedis.Jedis;
import static com.paytm.constants.Constants.SystemVariables.CLIENTID;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-AssignQRCode")
public class AssignQRCodeTest extends WalletBaseTest {

	@Test(description = "Case to verfiy v4/assignQrCode when businessType is P2P", groups = { "regression", "smoke",
			"sanity" })
	public void TC001_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.SUCCESS);
		qrCodeHelpers.assignQrCodeParameterValidation(assignQrCode);
		qrCodeHelpers.assignQrCodeDBValidation(assignQrCode.getResponsePojo().getResponse().getStickerId());
		qrCodeHelpers.revertingFlaggedToNormalUser();
		qrCodeHelpers.updateMappingIdInDynamicQrCode(assignQrCode.getResponsePojo().getResponse().getQrCodeId());
	}

	@Test(description = "Assign P2P business type QR code for invalid user doesn't exist at OAUTH end and Business type is P2P", groups = {
			"regression", "sanity" })
	public void TC002_AssignQrCodeTest() {

		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.INVALIDUSER);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_003);
	}

	@Test(description = "Assign dupliate P2P businesstype QR code for valid user.", groups = { "regression" })
	public void TC003_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.SUCCESS);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0026);
		qrCodeHelpers.assignQrCodeParameterValidation(assignQrCode);
		qrCodeHelpers.assignQrCodeDBValidation(assignQrCode.getResponsePojo().getResponse().getStickerId());
		qrCodeHelpers.revertingFlaggedToNormalUser();
		qrCodeHelpers.updateMappingIdInDynamicQrCode(assignQrCode.getResponsePojo().getResponse().getQrCodeId());

	}

	@Test(description = "When Business type passed is QR_MERCHANT and valid number.", groups = { "regression", "smoke",
			"sanity" })
	public void TC004_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("QR_MERCHANT");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0027);
	}

	@Test(description = "When Business type passed is QR_PRODUCT and valid number.", groups = { "regression", })
	public void TC005_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("QR_PRODUCT");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0027);
	}

	@Test(description = "When Business type passed is QR_PRICEDPRODUCT and valid number.", groups = { "regression" })
	public void TC006_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("QR_PRICEDPRODUCT");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0027);
	}

	@Test(description = "When Business type passed is QR_ORDER and valid number.", groups = { "regression" })
	public void TC007_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("QR_ORDER");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0027);
	}

	@Test(description = "When Business type passed is QR_USER  and valid number.", groups = { "regression" })
	public void TC008_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("QR_USER");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0027);
	}

	@Test(description = "When Business type passed is QR_DEBIT and valid number.", groups = { "regression" })
	public void TC009_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("QR_DEBIT");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0027);
	}

	@Test(description = "When Business type is null and valid number.", groups = { "regression" })
	public void TC010_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType(null);
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0027);
	}

	@Test(description = "When Business type is P2P and phone number is null ", groups = { "regression", "sanity" })
	public void TC011_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = null;
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0002);
	}

	@Test(description = "When Business type is P2P and diaplay name is passed in the valid request ", groups = {
			"regression" })
	public void TC012_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID,
				Constants.SystemVariables.HASH);
		String agentPhoneNumber = null;
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0002);
	}

	@Test(description = "When User exists at Auth but not present in user Table ", groups = {"regression"})
	public void TC013_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber=UserManager.getInstance().getNewPayerUser(UserTypes.USERNOTINWALLET);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.GE_1010);
}
	@Test(description = "When entry is present in dyanmic_qr_code table not present in dynamic_qr_code_mapping_details table ", groups = {"regression"})
	public void TC014_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.SUCCESS);
		String qrCodeId = assignQrCode.getResponsePojo().getResponse().getQrCodeId();
		qrCodeHelpers.DynamicQrMappingUpdate(assignQrCode.getResponsePojo().getResponse().getStickerId());
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.DQR_0028);
		qrCodeHelpers.revertingFlaggedToNormalUser();
		qrCodeHelpers.updateMappingIdInDynamicQrCode(qrCodeId);
	}

	@Test(description = "When Qr code key from redis is deleted", groups = {"regression"})
	public void TC015_AssignQrCodeTest() {
		Redis redis =new Redis();
		Jedis jedis= redis.createWalletRedisConnection();
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.SUCCESS);
		qrCodeHelpers.assignQrCodeParameterValidation(assignQrCode);
		qrCodeHelpers.assignQrCodeDBValidation(assignQrCode.getResponsePojo().getResponse().getStickerId());
		qrCodeHelpers.revertingFlaggedToNormalUser();
		qrCodeHelpers.updateMappingIdInDynamicQrCode(assignQrCode.getResponsePojo().getResponse().getQrCodeId());
		AssertionValidations.verifyAssertNull(redis.getRedisKey(jedis,
				"DQR_" +assignQrCode.getResponsePojo().getResponse().getStickerId() ));

	}
	@Test(description = "Case when HASH is null", groups = { "regression"})
	public void TC016_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, null);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(assignQrCode, APIStatus.AssignQRCode.UNAUTHORIZED_ACCESS);

}
	@Test(description = "case when clientId is null", groups = { "regression"})
	public void TC017_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, null, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(assignQrCode, APIStatus.AssignQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "when Hash is differnt", groups = { "regression"})
	public void TC018_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, "20515e62a1e9aa5ab7aff51cb2c890205ac875bd335419413e49a543bb5b3d7c");
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(assignQrCode, APIStatus.AssignQRCode.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case when clientId is null", groups = { "regression"})
	public void TC019_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest,"d02a898248eb4a2d82873fe8116a5f40", Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(assignQrCode, APIStatus.AssignQRCode.UNAUTHORIZED_ACCESS);
	}
	@Test(description = "Case to verfiy when operation type is blank", groups = { "regression"})
	public void TC020_AssignQrCodeTest() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.getRequestPojo().withOperationType("blank").getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(assignQrCode, APIStatus.AssignQRCode.BADREQUEST);

	} 
	
	@Test(description = "case to verify when sending invalid locale value", groups = { "regression"})
	public void TC021_InvalidLocaleValue() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.setQueryParam("en-IN");
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(assignQrCode, APIStatus.AssignQRCode.BADREQUEST);
	}
	
	@Test(description = "case to verify when sending valid local value", groups = { "regression"})
	public void TC022_ValidLocaleValue() {
		AssignQrCode assignQrCode = new AssignQrCode(AssignQrCode.normalRequest, CLIENTID, Constants.SystemVariables.HASH);
		String agentPhoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.setDynamicQRCodeToNull(agentPhoneNumber);
		assignQrCode.setQueryParam("en_IN");
		assignQrCode.getRequestPojo().getRequest().withPhoneNo(agentPhoneNumber).withBusinessType("P2P");
		assignQrCode.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(assignQrCode, APIStatus.AssignQRCode.SUCCESS);
		qrCodeHelpers.assignQrCodeParameterValidation(assignQrCode);
		qrCodeHelpers.assignQrCodeDBValidation(assignQrCode.getResponsePojo().getResponse().getStickerId());
		qrCodeHelpers.revertingFlaggedToNormalUser();
		qrCodeHelpers.updateMappingIdInDynamicQrCode(assignQrCode.getResponsePojo().getResponse().getQrCodeId());
	}
}