package qrCode;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.paytm.datamanager.MerchantManager;
import com.paytm.apiRequestBuilders.wallet.qrCode.GetQrCodeInfo;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserHeaders;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.AuthHelpers;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import com.paytm.utils.OfflinePaymentTokenAnalyzer;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "QRCode-GetQrCodeInfo")
public class GetQrCodeInfoTest extends WalletBaseTest {

	@Test(description = "Case to verify when valid qrCodeId is passed having mapping_status 1 and status 1", groups = {
			"regression", "smoke", "sanity" })
	public void TC001_GetQrCodeInfoMappingStatusAndStatusOne() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}

	@Test(description = "Case to verify when valid qrCodeId is passed having mapping_status 0", groups = { "regression",
			"smoke", "sanity" })
	public void TC002_GetQrCodeInfoMappingStatusZero() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("0", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.QR_1018);
	}

	@Test(description = "Case to verify when qrCodeId parameter is blank in request", groups = { "regression" })
	public void TC003_GetQrCodeInfoWhenQrCodeIdBlank() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId("");
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.QR_1018);
	}

	@Test(description = "Case to verify when wrong qrCodeId is passed in reqeust", groups = { "regression", "sanity" })
	public void TC004_QrCodeIdIsWorngPassed() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId("abcdefgh");
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.QR_1018);
	}

	@Test(description = "Case to verify when request does not have qrCodeId parameter", groups = { "regression" })
	public void TC005_QrCodeIdParameterNotPassed() {
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(null);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.GE_0001);
	}

	@Test(description = "Case to verify when plateformName is blank in request", groups = { "regression" })
	public void TC006_PlateformNameIsBalnk() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().withPlateformName("").getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}

	@Test(description = "Case to verify when plateformName is null in request", groups = { "regression" })
	public void TC007_PlateformNameNullString() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().withPlateformName("null").getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}

	@Test(description = "Case to verify when plateformName is other than PayTM", groups = { "regression" })
	public void TC008_PlateformNameOtherThanPayTM() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().withPlateformName("One97").getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}

	@Test(description = "Case to verify when operationType is blank in request", groups = { "regression" })
	public void TC009_OperationTypeBlank() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().withOperationType("").getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(getQrCodeInfo, APIStatus.GetQrCodeInfo.BADREQUEST);
	}

	@Test(description = "Case to verify when operationType is null in request", groups = { "regression" })
	public void TC010_OperationTypeNullString() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().withOperationType("null").getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(getQrCodeInfo, APIStatus.GetQrCodeInfo.BADREQUEST);
	}

	@Test(description = "Case to verify when operationType is other than QR_CODE", groups = { "regression" })
	public void TC011_OperationTypeOtherThanQRCODE() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().withOperationType("QQR_CODE").getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(getQrCodeInfo, APIStatus.GetQrCodeInfo.BADREQUEST);
	}

	@Test(description = "Case to verify when valid qrCodeId is passed having status 0 in dynamic_qr_code table", groups = {
			"regression", "sanity" })
	public void TC012_QrCodeStatusZero() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "0");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.QR_1018);
	}

	@Test(description = "Case to verify when ssoToken not passed in headers", groups = { "regression" })
	public void TC013_SsoTokenNotPassedInHeaders() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "0");
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, null);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.QR_1018);
	}

	@Test(description = "Case to verify when ssoToken is blank in headers", groups = { "regression" })
	public void TC014_SsoTokenBlankInHeaders() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "0");
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, "");
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.QR_1018);
	}

	@Test(description = "Case to verify when expired ssoToken is passed in headers", groups = { "regression" })
	public void TC015_ExpiredSsoTokenIsPassed() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "0");
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest,
				"cb02f143-ebd0-4036-a9ee-e49733027784");
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.QR_1018);
	}

	@Test(description = "case to verify when qrCodeId passed having 6th-8th digit 02 and 2d is appended at last", groups = { "regression", "sanity" })
	public void TC016_QrCodeIdHaving02InItWith2dAtLast() {
		AuthHelpers authHelpers = new AuthHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.BARCODEUSER);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		String qrCodeId = authHelpers.verifyDeviceAndGetOfflineCode(phone, token) + OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token)
				+ "2d";
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
	}

	@Test(description = "case to verify when qrCodeId passed having 6th-8th digit 02 and 2c is appended at last", groups = { "regression" })
	public void TC017_QrCodeIdHaving02InItWith2cAtLast() {
		AuthHelpers authHelpers = new AuthHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.BARCODEUSER);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		String qrCodeId = authHelpers.verifyDeviceAndGetOfflineCode(phone, token) + OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token)
				+ "2c";
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
	}

	@Test(description = "case to verify when qrCodeId is of type MERCHANT_QR_CODE or QR_CODE_DEEPLINK", groups = { "regression" })
	public void TC018_QrCodeIdOfTypeMerchantQrCodeOrQrCodeDeepLink() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		String[] qrCodeDetail = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phone);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetail, phone, "MERCHANT_QR_CODE", "MERCHANT", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeDetail[1]);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}

	@Test(description = "case to verify when qrCodeId is of type UTS", groups = { "regression" })
	public void TC019_QrCodeIdOfTypeUTS() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		String[] qrCodeDetail = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phone);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetail, phone, "UTS", "MERCHANT", "1", MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID));
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeDetail[1]);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}

	@Test(description = "case to verify when qrCodeId is of type MERCHANT_QR_CODE or QR_CODE_DEEPLINK of inactive Merchant", groups = { "regression" })
	public void TC020_QrCodeIdOfTypeMerchantQrCodeOrQrCodeDeepLinkInactiveMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		String[] qrCodeDetail = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phone);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetail, phone, "MERCHANT_QR_CODE", "MERCHANT", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.INACTIVEMERCHANT, MerchantHeaders.MERCHANTGUID));
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeDetail[1]);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}
	
	@Test(description = "case to verify when qrCodeId is of type UTS of inactive Merchant", groups = { "regression" })
	public void TC021_QrCodeIdOfTypeMerchantQrCodeOrQrCodeDeepLinkInactiveMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		String[] qrCodeDetail = walletAPIHelpers.createDynamicQrCode("QR_MERCHANT", phone);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetail, phone, "UTS", "MERCHANT", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.INACTIVEMERCHANT, MerchantHeaders.MERCHANTGUID));
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeDetail[1]);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}
	
	@Test(description = "case to verify when qrCodeId is of type USER_QR_CODE/DYNAMIC_QR_CODE", groups = { "regression" })
	public void TC022_QrCodeIdOfTypeMerchantQrCodeOrQrCodeDeepLinkInactiveMerchant() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		String[] qrCodeDetail = walletAPIHelpers.createDynamicQrCode("P2P", phone);
		walletAPIHelpers.mapDynamicQrCode(qrCodeDetail, phone, "USER_QR_CODE", "USER", "1", MerchantManager
				.getInstance().getMerchantDetails(MerchantTypes.INACTIVEMERCHANT, MerchantHeaders.MERCHANTGUID));
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeDetail[1]);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}
	
	@Test(description = "case to verify when invalid locale is send", groups = { "regression"})
	public void TC023_InvalidLocalSend() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.setQueryParam("en-IN");
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}
	
	@Test(description = "case to verify when valid locale is send", groups = { "regression"})
	public void TC024_ValidLocalSend() {
		String qrCodeId = QrCodeHelpers.getInstance().getQrIdFromId("1", "1");
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
		GetQrCodeInfo getQrCodeInfo = new GetQrCodeInfo(GetQrCodeInfo.defaultRequest, token);
		getQrCodeInfo.setQueryParam("en_IN");
		getQrCodeInfo.getRequestPojo().getRequest().withQrCodeId(qrCodeId);
		getQrCodeInfo.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(getQrCodeInfo, APIStatus.GetQrCodeInfo.SUCCESS);
		QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
		qrCodeHelpers.getQrCodeInfoValidation(getQrCodeInfo);
	}
}
