package oldService;

import com.paytm.apiRequestBuilders.wallet.oldService.CheckUserBalance;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.CheckUserBalanceHelpers;
import com.paytm.helpers.CommonValidation;
import com.paytm.helpers.LimitAuditInfoValidation;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.utils.DBValidation;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "OldService-CheckUserBalance")
public class CheckUserBalanceTest extends WalletBaseTest {


	@Test(description = "Test case to verify checkUser Balance of Aadhar Otp Kyc User having multiple subwallets with Detail Info Yes", groups = {
			"regression","sanity" })
	public void TC001_checkDetailedBalanceAadharOtpUser(){
		System.out.println("Testing123");
		CheckUserBalanceHelpers checkBalance = new CheckUserBalanceHelpers();
		DBValidation db = new DBValidation();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.WITHSUBWALLETBALANCE);
		String userId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, phone);
		CheckUserBalance checkUser = new CheckUserBalance(CheckUserBalance.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		checkUser.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(checkUser, APIStatus.CheckUserBalance.SUCCESS);
		AssertionValidations.verifyAssertEqual(checkUser.getResponsePojo().getResponse().getSsoId(), userId);
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.USERID, userId);
		String balanceQuery = DBQueries.checkUser_SubWallet_Balance;
		balanceQuery = db.fetchQuery(balanceQuery, params);
		System.out.println("balanceQuery is " + balanceQuery);
		checkBalance.verifyUserBalance(checkUser.getApiResponse().asString(), checkUser, balanceQuery);
		String detailQuery = com.paytm.constants.DBQueries.checkUser_SubWallet_Details;
		detailQuery = db.fetchQuery(detailQuery, params);
		System.out.println("detailQuery is " + detailQuery);
		checkBalance.verifyUserBalanceDetails(checkUser.getApiResponse().asString(), checkUser, "no", detailQuery);
	}

}
