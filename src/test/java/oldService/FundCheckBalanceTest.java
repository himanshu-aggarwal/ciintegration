package oldService;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.paytm.apiRequestBuilders.wallet.oldService.FundCheckBalance;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.UserHeaders;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.CheckUserBalanceHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "OldService-FundCheckBalance")
public class FundCheckBalanceTest extends WalletBaseTest {

	@Test(description = "Test case to verify fund check Balance of BC Agent with all valid Inputs", groups = {
			"regression", "sanity" })
	public void TC001_fundCheckBalance() throws AssertionError, Exception {
		CheckUserBalanceHelpers checkBalance = new CheckUserBalanceHelpers();
		UserManager.getInstance().getNewPayerUser(UserTypes.FUNDCHECKBALANCEUSER);
		String walletGUID = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.FUNDCHECKBALANCEMERCHANT,
				MerchantHeaders.FINANCEWALLETGUID);
		FundCheckBalance fundCheckBalance = new FundCheckBalance(FundCheckBalance.defaultRequest);
		fundCheckBalance.setToken(UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		fundCheckBalance.getRequestPojo().getRequest().setWalletGUID(walletGUID);
		fundCheckBalance.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fundCheckBalance, APIStatus.FundCheckBalance.SUCCESS);
		checkBalance.validateAgentBalance(fundCheckBalance);
	}

	@Test(description = "Test case to verify fund check Balance of BC Agent with invalid Sso Token", groups = {
			"regression" })
	public void TC002_fundCheckBalance() throws AssertionError, Exception {
		String walletGUID = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.FUNDCHECKBALANCEMERCHANT,
				MerchantHeaders.FINANCEWALLETGUID);
		FundCheckBalance fundCheckBalance = new FundCheckBalance(FundCheckBalance.defaultRequest);
		fundCheckBalance.setToken("f85b6a14-c707-4ce1-b990-5e6394938300");
		fundCheckBalance.getRequestPojo().getRequest().setWalletGUID(walletGUID);
		fundCheckBalance.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fundCheckBalance, APIStatus.FundCheckBalance.UNAUTHORIZED);
	}

	@Test(description = "Test case to verify fund check Balance of BC Agent with invalid mid", groups = {
			"regression" })

	public void TC003_fundCheckBalance() throws AssertionError, Exception {
		String walletGUID = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.FUNDCHECKBALANCEMERCHANT,
				MerchantHeaders.FINANCEWALLETGUID);
		FundCheckBalance fundCheckBalance = new FundCheckBalance(FundCheckBalance.defaultRequest);
		fundCheckBalance.setMid("TESTME011");
		fundCheckBalance.getRequestPojo().getRequest().setWalletGUID(walletGUID);
		fundCheckBalance.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fundCheckBalance, APIStatus.FundCheckBalance.UNAUTHORIZED);
	}

	@Test(description = "Test case to verify fund check Balance of BC Agent with all invalid Wallet Guid", groups = {
			"regression", "sanity" })
	public void TC004_fundCheckBalance() throws AssertionError, Exception {
		UserManager.getInstance().getNewPayerUser(UserTypes.FUNDCHECKBALANCEUSER);
		FundCheckBalance fundCheckBalance = new FundCheckBalance(FundCheckBalance.defaultRequest);
		fundCheckBalance.setToken(UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		fundCheckBalance.getRequestPojo().getRequest().setWalletGUID("A86DA659-8CCE-441D-B28A-378B768D92");
		fundCheckBalance.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(fundCheckBalance, APIStatus.FundCheckBalance.FAILURE);
	}
}
