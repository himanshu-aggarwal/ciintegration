package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.CheckBalance;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.ClassNameExt;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.APIStatus;
import com.paytm.enums.UserHeaders;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.*;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.math.BigDecimal;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-CheckBalance")
public class CheckBalanceTest extends WalletBaseTest {

	@Test(description = "Test case to verify CheckBalance of Basic User", groups = { "regression" })
	public void TC001_checkBalanceBasicUser(){
		CheckUserBalanceHelpers checkUserBalance = new CheckUserBalanceHelpers();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		CheckBalance checkBalance = new CheckBalance(
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		checkBalance.createRequestJsonAndExecute();
		AssertionValidations.verifyAssertEqual(String.valueOf(checkBalance.getResponsePojo().getResponse().getStatus()),
				"1");
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getSsoId(),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getWalletGrade(), "Basic");
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.SUCCESS);
		checkUserBalance.validatePaytmBalance(checkBalance);
	}

	@Test(description = "Test Case to verify CheckBalance of Prime User", groups = { "regression" })
	public void TC002_checkBalancePrimeUser(){
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalanceHelpers checkUserBalance = new CheckUserBalanceHelpers();
		String phoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		CheckBalance checkBalance = new CheckBalance(token);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")),phoneNumber);
		checkBalance.createRequestJsonAndExecute();
		AssertionValidations.verifyAssertEqual(String.valueOf(checkBalance.getResponsePojo().getResponse().getStatus()),
				"1");
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getSsoId(),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getWalletGrade(),
				"Premium");
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.SUCCESS);
		checkUserBalance.validatePaytmBalance(checkBalance);

	}

	@Test(description = "Test Case to verify CheckBalance of Aadhar OTP Kyc User", groups = { "regression" })
	public void TC003_checkBalanceAadharOTPUser(){
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalanceHelpers checkUserBalance = new CheckUserBalanceHelpers();
		String phoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		CheckBalance checkBalance = new CheckBalance(token);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")),phoneNumber);
		checkBalance.createRequestJsonAndExecute();
		AssertionValidations.verifyAssertEqual(String.valueOf(checkBalance.getResponsePojo().getResponse().getStatus()),
				"1");
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getSsoId(),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getWalletGrade(),
				"Adhaar OTP Kyc");
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.SUCCESS);
		checkUserBalance.validatePaytmBalance(checkBalance);
	}

	@Test(description = "Test Case to verify CheckBalance of Prime User with Paytm Scope Token", groups = {
			"regression" })
	public void TC004_checkBalanceUser(){
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		CheckUserBalanceHelpers checkUserBalance = new CheckUserBalanceHelpers();
		String phoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.PAYTMSCOPETOKEN);
		CheckBalance checkBalance = new CheckBalance(token);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")),phoneNumber);
		checkBalance.createRequestJsonAndExecute();
		AssertionValidations.verifyAssertEqual(String.valueOf(checkBalance.getResponsePojo().getResponse().getStatus()),
				"1");
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getSsoId(),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getWalletGrade(),
				"Premium");
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.SUCCESS);
		checkUserBalance.validatePaytmBalance(checkBalance);
	}

	@Test(description = "Test Case to verify CheckBalance of Prime User with Invalid Wallet Scope Token", groups = {
			"regression" })
	public void TC005_checkBalanceUser(){
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		CheckBalance checkBalance = new CheckBalance("39f2a2a2-379f-43d8-9710-982f8e2ecff1");
		checkBalance.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.UNAUTHORIZED);
	}

	@Test(description = "Test Case to verify CheckBalance of Prime User with Blank Wallet Scope Token", groups = {
			"regression" })
	public void TC006_checkBalanceUser(){
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		CheckBalance checkBalance = new CheckBalance("");
		checkBalance.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.UNAUTHORIZED);
	}

	@Test(description = "Test Case to verify CheckBalance of Inactive Basic User with Wallet Scope Token", groups = {
			"regression" })
	public void TC007_checkBalanceUser(){
		CheckUserBalanceHelpers checkUserBalance = new CheckUserBalanceHelpers();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC,UserTypes.INACTIVEWALLET);
		CheckBalance checkBalance = new CheckBalance(
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		checkBalance.createRequestJsonAndExecute();
		AssertionValidations.verifyAssertEqual(String.valueOf(checkBalance.getResponsePojo().getResponse().getStatus()),
				"0");
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getSsoId(),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getWalletGrade(),
				"Adhaar OTP Kyc");
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.SUCCESS);
		checkUserBalance.validatePaytmBalance(checkBalance);

	}

	@Test(description = "Test Case to verify CheckBalance of Blocked User with Wallet Scope Token", groups = {
			"regression" })
	public void TC008_checkBalanceUser(){
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		RedisHelper redis = new RedisHelper();
		CheckUserBalanceHelpers checkUserBalance = new CheckUserBalanceHelpers();
		String phoneNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		CheckBalance checkBalance = new CheckBalance(token);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")),phoneNumber);
		redis.insertBlockedRedisKey();
		checkBalance.createRequestJsonAndExecute();
		redis.deleteBlockedRedisKey();
		AssertionValidations.verifyAssertEqual(String.valueOf(checkBalance.getResponsePojo().getResponse().getStatus()),
				"1");
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getSsoId(),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		AssertionValidations.verifyAssertEqual(checkBalance.getResponsePojo().getResponse().getWalletGrade(),
				"Premium");
		CommonValidation.getInstance().basicAsserts(checkBalance, APIStatus.CheckBalance.SUCCESS);
		checkUserBalance.validatePaytmBalance(checkBalance);

	}

}
