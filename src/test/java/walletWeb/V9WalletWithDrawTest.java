package walletWeb;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletWithDraw;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.APIStatus;
import com.paytm.enums.MerchantHeaders;
import com.paytm.enums.MerchantTypes;
import com.paytm.enums.ScratchPad;
import com.paytm.enums.UserHeaders;
import com.paytm.enums.UserTypes;
import com.paytm.helpers.AuthHelpers;
import com.paytm.helpers.CommonValidation;
import com.paytm.helpers.LimitAuditInfoValidation;
import com.paytm.helpers.ScratchPadValidator;
import com.paytm.helpers.SubWalletHelpers;
import com.paytm.helpers.UserSubwalletConsolidated;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-V9WalletWithdraw")
public class V9WalletWithDrawTest extends WalletBaseTest {

	@Test(description = "Test case to verify Withdraw of Basic User", groups = { "regression", "sanity", "smoke" })
	public void TC001_withdrawBasicUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		AuthHelpers authHelpers = new AuthHelpers();
		String encryptedCustId = authHelpers.getEncryptedCustId(token);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId(encryptedCustId);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.TOLLMERCHANT, MerchantHeaders.MERCHANTGUID));
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
	}

	@Test(description = "Test Case to verify Withdraw of Prime User", groups = { "regression", "sanity" })
	public void TC002_withdrawPrimeUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		AuthHelpers authHelpers = new AuthHelpers();
		String encryptedCustId = authHelpers.getEncryptedCustId(token);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId(encryptedCustId);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.TOLLMERCHANT, MerchantHeaders.MERCHANTGUID));
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
	}

	@Test(description = "Test case to verify withdraw from toll subwallet", groups = { "regression", "sanity",
			"smoke" })
	public void TC003_tollSubWalletWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		AuthHelpers authHelpers = new AuthHelpers();
		String encryptedCustId = authHelpers.getEncryptedCustId(token);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT, MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(2);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.setCustId(encryptedCustId);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.TOLLMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.TOLLMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.TOLLMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is able to withdraw for Min Kyc", groups = { "regression",
			"sanity" })
	public void TC004_withdrawForMinKycUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		AuthHelpers authHelpers = new AuthHelpers();
		String encryptedCustId = authHelpers.getEncryptedCustId(token);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId(encryptedCustId);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.TOLLMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is able to withdraw for Aadhar OTP User", groups = {
			"regression", "sanity" })
	public void TC005_withdrawForAadharOTPUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		AuthHelpers authHelpers = new AuthHelpers();
		String encryptedCustId = authHelpers.getEncryptedCustId(token);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId(encryptedCustId);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.TOLLMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Primitive User", groups = { "regression",
			"sanity" })
	public void TC006_withdrawForPrimitiveUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIMITIVE);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		AuthHelpers authHelpers = new AuthHelpers();
		String encryptedCustId = authHelpers.getEncryptedCustId(token);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId(encryptedCustId);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.TOLLMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.TOLLMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is not able to withdraw to inactive merchant", groups = {
			"regression" })
	public void TC007_withdrawToInactiveMerchant() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		AuthHelpers authHelpers = new AuthHelpers();
		String encryptedCustId = authHelpers.getEncryptedCustId(token);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.INACTIVEMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId(encryptedCustId);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1035);
	}

	@Test(description = "case to verify when invalid custId is passed in header", groups = { "regression" })
	public void TC008_InvalidCustIdPassed() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId("abcdefgh@34123==");
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID));
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to verify when custId is blank in header", groups = { "regression" })
	public void TC009_CustIdIsBlank() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCustId("");
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID));
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.UNAUTHORIZED_ACCESS);
	}
}
