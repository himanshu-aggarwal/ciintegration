package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.SendMoney;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.UserManager;
import com.paytm.helpers.*;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.*;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-SendMoney")
public class SendMoneyTest extends WalletBaseTest {


	@Test(description = "Test case to verify Send Money from eky user to other ekyc user ", groups = { "regression", "sanity", "smoke" })
	public void TC001_initialSendMoney() {
		SubWalletHelpers subwallet = new SubWalletHelpers();
		BigDecimal totalAmount = new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest()
				.withPayeePhoneNumber(UserManager.getInstance().getPayeeUserDetails(UserHeaders.MOBILENUMBER))
				.withAmount(totalAmount);
		subwallet.addMoneyInSubwalletMainwalletP2P(MerchantTypes.REGRESSIONMERCHANT, mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for food subwallet", groups = { "regression", "sanity", "smoke"  })
	public void TC002_foodSubWalletSendMoney() {
		BigDecimal totalAmount = new BigDecimal(10);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount).getSubWalletAmount().withfOOD(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for gift subwallet", groups = { "regression" })
	public void TC003_giftSubWalletSendMoney() {
		BigDecimal totalAmount = new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount).getSubWalletAmount().withgIFT(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for toll subwallet", groups = { "regression" })
	public void TC004_tollSubWalletSendMoney() {
		BigDecimal totalAmount = new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount).getSubWalletAmount().withtOLL(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for closed loop wallet", groups = { "regression" })
	public void TC005_closedLoopWalletSendMoney() {
		BigDecimal totalAmount = new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount).getSubWalletAmount()
				.withcLOSED_LOOP_WALLET(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.callCheckUserBalanceInsufficientBalanceP2P();
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.WM_1006);
		subWalletHelpers.validateCheckUserBalanceP2P();
	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for closed loop subwallet", groups = { "regression" })
	public void TC006_closedLoopSubWalletSendMoney() {
		BigDecimal totalAmount = new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount).getSubWalletAmount()
				.withcLOSED_LOOP_SUB_WALLET(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for all subwallets", groups = { "regression" })
	public void TC007_allSubWalletSendMoney() {
		BigDecimal totalAmount = new BigDecimal(30);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5).withgIFT(5).withtOLL(5).withfUEL(5)
				.withcLOSED_LOOP_SUB_WALLET(5).withiNTERNATIONAL_FUNDS_TRANSFER(5);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for all subwallets and main wallet", groups = { "regression", "sanity" })
	public void TC008_allSubWalletWithMainSendMoney() {
		BigDecimal totalAmount = new BigDecimal(40);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5).withgIFT(5).withtOLL(5).withfUEL(5)
				.withcLOSED_LOOP_SUB_WALLET(5).withiNTERNATIONAL_FUNDS_TRANSFER(5);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify send money from payer to flagged Merchant for main wallet", groups = { "regression" })
	public void TC009_mainWalletOnlySendMoney() {
		BigDecimal totalAmount = new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,0);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Test Case to verify Send Money from payer to flagged Merchant which do not exist SubWallets", groups = {
			"regression" })
	public void TC010_subWalletSendMoney() {
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantMobileNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		BigDecimal totalAmount = new BigDecimal(45);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().setUpdateAmountManager(false);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantMobileNumber).withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5).withgIFT(5).withtOLL(5).withfUEL(5).withcLOSED_LOOP_WALLET(5).withcLOSED_LOOP_SUB_WALLET(5).withiNTERNATIONAL_FUNDS_TRANSFER(5);
		FundThreadManager.getInstance().withClosedLoopSubWalletAmount(new BigDecimal(5)).withGiftWalletAmount(new BigDecimal(5)).withTotalAmount(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
		}

	@Test(description = "Executing Test Case to Send MOney from User with insufficient balance in main wallet", groups = {
			"regression" })
	public void TC011_insufficientMainSendMoney() {
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		BigDecimal totalAmount = new BigDecimal(50);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().setUpdateAmountManager(false);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.callCheckUserBalanceInsufficientBalanceP2P();
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.WM_1006);
		subWalletHelpers.validateCheckUserBalanceP2P();
	}


	@Test(description = "Executing Test Case to Send Money from User with insufficient balance in sub wallet but in main wallet", groups = {
			"regression" })
	public void TC012_mainWithInsufficientSubWalletSendMoney() {
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		BigDecimal totalAmount = new BigDecimal(100);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().setUpdateAmountManager(false);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(50);
		FundThreadManager.getInstance().withFoodWalletAmount(new BigDecimal(40)).withTotalAmount(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalanceP2P();
	}

	@Test(description = "Executing Test Case to Send Money from User based on priority", groups = { "regression" })
	public void TC013_prioritySubWalletSendMoney() {
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		BigDecimal totalAmount = new BigDecimal(20);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10).withcLOSED_LOOP_SUB_WALLET(10);
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalanceP2P();
	}

	@Test(description = "Successful execution of Test Case to Send Money from User if a negative amount is given", groups = {
			"regression" })
	public void TC014_negativeAmountSubWalletSendMoney() {
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.FLAGGEDMERCHANT);
		BigDecimal totalAmount = new BigDecimal(30);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest, token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(-10).withgIFT(10);
		subWalletHelpers.addMoneyInSubwalletMainwalletP2P(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		subWalletHelpers.createDebitTxnMapP2P();
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalanceP2P();
	}


	@Test(description = "Executing Test Case to test Send Money when Amount Limit is breached", groups = {
			"regression" })
	public void TC015_AmountBreachSubWalletSendMoney() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		BigDecimal totalAmount = new BigDecimal(30);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_FOODCOURT,UserTypes.ACQUIRER_64045);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		params.put(DBEnums.MERCHANTID, payeeCustId);
		params.put(DBEnums.USERID, custId);
		issuerMapping.subwalletAmountLimitBreach(params);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);

		String[] issuerId = new String[] { "SUBWALLETISSUERAMOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.WalletWithdraw.WM_1006);
		issuerMapping.resetLimit();
		subWalletHelpers.validateCheckUserBalanceP2P();
	}

	@Test(description = "Executing Test Case to test Send Money, when Count Limit is breached", groups = {
			"regression" })
	public void TC016_CountBreachSubWalletSendMoney() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		BigDecimal totalAmount = new BigDecimal(30);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_FOODCOURT,UserTypes.ACQUIRER_64045);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		params.put(DBEnums.MERCHANTID, payeeCustId);
		params.put(DBEnums.USERID, custId);
		issuerMapping.subwalletCountLimitBreach(params);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETISSUERCOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.WalletWithdraw.WM_1006);
		issuerMapping.resetLimit();
		subWalletHelpers.validateCheckUserBalanceP2P();
	}


	@Test(description = "Test case to verify Send Money from flagged merchant acquirer whose category is null in the flagged merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [C]", groups = {
			"regression", "sanity" })
	public void TC017_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_NULL,UserTypes.ACQUIRER_64079);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(20);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}




	@Test(description = "Test case to verify Send Money from flagged merchant acquirer whose category is mapped with the one in subwallet_issuer_config Table for Merchant with config [I] = [A] [C]", groups = {
			"regression" })
	public void TC018_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.CATEGORY_WithdrawFood_64079,UserTypes.ACQUIRER_64079);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(20);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify Send Money from issuer as well as merchant for Merchant with config [I] = [A] [C]", groups = {
			"regression" })
	public void TC019_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_Restaurant,UserTypes.ACQUIRER_64079);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(260);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER", "NOCATEGORYMERCHANT" };
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify Send Money from acquirer whose category is null in the flagged_merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC020_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_NULL,UserTypes.ACQUIRER_64083);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(20);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify Send Money from issuer only if Acquirer merchant category is mapped with the one in subwallet_issuer_config Table for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC021_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_BLANK,UserTypes.ACQUIRER_64083);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(20);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}



	@Test(description = "Test case to verify Send Money from issuer as well as merchant for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC022_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_Restaurant,UserTypes.ACQUIRER_64083);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(260);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2", "NOCATEGORYMERCHANT" };
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify Send Money from Merchant with config [I] = [ ] [ ] and Send Money is done on Merchant  whose category is null in the merchant Table.", groups = {
			"regression" })
	public void TC023_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_NULL,UserTypes.ACQUIRER_64083);
		BigDecimal totalAmount = new BigDecimal(100);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETACQUIRERBLANKCATEGORY4" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalanceP2P(mobileNumber, flaggedMerchantPhoneNo,issuerId);
		sendMoney.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.WalletWithdraw.WM_1006);
		subWalletHelpers.validateCheckUserBalanceP2P();
	}




	@Test(description = "Test case to verify send money Normal merchant with no entry in SUBWALLET_ISSUER_CONFIG table (But accepts food as mapped with wallet category mapping)", groups = {
			"regression" })
	public void TC024_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_Restaurant);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(300);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER4", "SUBWALLETWITHDRAWACQUIRER5",
				"SUBWALLETWITHDRAWACQUIRER6" };
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify send money from  Merchant category mapped with Subwallet_issuer_config table define issuer (For acquirer not present i:e [ I ] = [ ] [ C ]", groups = {
			"regression" })
	public void TC025_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.CATEGORY_FoodUnboardedMerchant);
		BigDecimal totalAmount = new BigDecimal(20);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER5" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalanceP2P(mobileNumber, flaggedMerchantPhoneNo,issuerId);
		sendMoney.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalanceP2P();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.WalletWithdraw.WM_1006);
	}

	@Test(description = "Test case to verify send Money from  Merchant category mapped with Subwallet_issuer_config table define issuer (For acquirer not present i:e  but SIC configurations is  i:e [ I ] = [ A ] [ C ]", groups = {
			"regression" })
	public void TC026_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.CATEGORY_CategoryAlongWithAcquirer);
		BigDecimal totalAmount = new BigDecimal(20);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER6" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalanceP2P(mobileNumber, flaggedMerchantPhoneNo,issuerId);
		sendMoney.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalanceP2P();
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.WalletWithdraw.WM_1006);
	}

	@Test(description = "Test case to verify Send Money from 2 issuers who have the same acquirer)", groups = {
			"regression" })
	public void TC027_foodSubWalletLimitSendMoney() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String flaggedMerchantPhoneNo = UserManager.getInstance().getNewPayeeUser(UserTypes.FLAGGEDMERCHANT,UserTypes.FLAGCATEGORY_Restaurant,UserTypes.ACQUIRER_64101,UserTypes.ACQUIRER_64103);
		String payeeCustId = UserManager.getInstance().getPayeeUserDetails(UserHeaders.CUSTID);
		String payerCustId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		BigDecimal totalAmount = new BigDecimal(180.0);
		SendMoney sendMoney = new SendMoney(SendMoney.subWalletRequest,token);
		sendMoney.getRequestPojo().getRequest().withPayeePhoneNumber(flaggedMerchantPhoneNo)
				.withAmount(totalAmount);
		sendMoney.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER3", "SUBWALLETWITHDRAWISSUER4" };
		subWalletHelpers.callCheckUserBalanceLimitResetP2P(mobileNumber, flaggedMerchantPhoneNo,
				issuerId);
		subWalletHelpers.callCheckUserBalanceP2P(mobileNumber, flaggedMerchantPhoneNo, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(payerCustId,payeeCustId);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		sendMoney.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(sendMoney.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(sendMoney, APIStatus.SendMoney.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),1,1);
		subWalletHelpers.extendedPpiInfoVerification(sendMoney.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.extendedPpiInfoVerificationWithoutPayerId);
		subWalletHelpers.validateCheckUserBalanceP2P();
		limitAudit.validateLimitAuditInfo(mobileNumber, 1,69, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_69,"",sendMoney.getRequestPojo().getRequest().getMerchantOrderId());
	}



}
