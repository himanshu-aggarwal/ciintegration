package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletRefund;
import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletWithDraw;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.*;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.math.BigDecimal;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-SubwalletRefund")
public class SubwalletRefund extends WalletBaseTest {

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from food subwallet", groups = {"regression","sanity" })
	public void TC001_foodSubwalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from gift subwallet", groups = {
			"regression" })
	public void TC002_giftSubwalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from toll subwallet", groups = {
			"regression" })
	public void TC003_tollSubwalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from closed loop wallet", groups = {
			"regression" })
	public void TC004_closedLoopWalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from closed loop subwallet", groups = {
			"regression" })
	public void TC005_closedLoopSubWalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		System.out.println("Response for withdraw is " + walletWithdraw.getApiResponse().asString());
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from fuel subwallet", groups = {
			"regression" })
	public void TC006_fuelWalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from international subwallet", groups = {
			"regression" })
	public void TC007_InternationalSubWalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from all subwallets", groups = {
			"regression" })
	public void TC008_allSubWalletRefund_fullRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(35);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount()
				.withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from all subwallet, including main wallet", groups = {
			"regression" })
	public void TC009_mainWithAllSubWalletRefund_fullRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(45);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from main wallet", groups = { "regression" })
	public void TC010_mainWalletRefund_fullRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,0, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.MainOnly.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from closed loop subwallet", groups = {
			"regression" })
	public void TC011_subWalletRefund_fullRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(45);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOSUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().setUpdateAmountManager(false);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		FundThreadManager.getInstance().withGiftWalletAmount(subWalletAmount)
				.withInternationalWalletAmount(subWalletAmount).withClosedLoopWalletAmount(subWalletAmount)
				.withClosedLoopSubWalletAmount(subWalletAmount).withTotalAmount(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.NOSUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().setUpdateAmountManager(false);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		FundThreadManager.getInstance().withGiftWalletAmount(subWalletAmount)
				.withInternationalWalletAmount(subWalletAmount).withClosedLoopWalletAmount(subWalletAmount)
				.withClosedLoopSubWalletAmount(subWalletAmount).withTotalAmount(totalAmount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.NOSUBWALLETMERCHANT);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.NOSUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.NOSUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.NOSUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	// Need to check this TC
	@Test(description = "Full Refund | Test Case to verify refund, in case of withdraw from Merchant which do not exist SubWallets", groups = {
			"regression" })
	public void TC012_subWalletPriorityRefund_fullRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		BigDecimal refundAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().setUpdateAmountManager(false);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFTVOUCHER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		FundThreadManager.getInstance().withTotalAmount(refundAmount).withFoodWalletAmount(new BigDecimal(10));
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test Case to verify refund is failed, in case the refund request having different subwallet then subwallet in withdraw ", groups = {
			"regression" })
	public void TC013_invalidCapAmountRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		subWalletHelpers.createDebitTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(totalAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.RWS_0001);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Test Case to verify refund is failed, in case mentioned subwallet amount in refund request is different from subwallet amount in withdraw request", groups = {
			"regression" })
	public void TC014_invalidCapAmountRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(30);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		subWalletHelpers.createDebitTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFTVOUCHER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.RWS_0001);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Test Case to verify refund is failed, in case complete refund is requested in main wallet even though withdraw is done with subwallets involved", groups = {
			"regression" })
	public void TC015_invalidCapAmountRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(30);
		BigDecimal refundAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		subWalletHelpers.createDebitTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFTVOUCHER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(0);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(0);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.RWS_0001);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Test Case to verify refund is failed, in case amount mentioned in any of the subwallet is -ve", groups = {
			"regression" })
	public void TC016_invalidCapAmountRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(30);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		subWalletHelpers.createDebitTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFTVOUCHER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(-10);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.RWS_0001);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Test Case to verify refund is failed, in case refund amount is greater than withdraw amount", groups = {
			"regression" })
	public void TC017_invalidRefundAmount() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		BigDecimal refundAmount = new BigDecimal(21);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		subWalletHelpers.createDebitTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.RPG_001);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}


	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from food subwallet", groups = {"regression","sanity" })
	public void TC018_foodSubwalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		BigDecimal totalAmount = new BigDecimal(20);
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from gift subwallet", groups = {
			"regression" })
	public void TC019_giftSubwalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from toll subwallet", groups = {
			"regression" })
	public void TC020_tollSubwalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from closed loop wallet", groups = {
			"regression" })
	public void TC021_closedLoopWalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from closed loop subwallet", groups = {
			"regression" })
	public void TC022_closedLoopSubWalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		System.out.println("Response for withdraw is " + walletWithdraw.getApiResponse().asString());
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from fuel subwallet", groups = {
			"regression" })
	public void TC023_fuelWalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from international subwallet", groups = {
			"regression" })
	public void TC024_InternationalSubWalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(totalAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from all subwallets", groups = {
			"regression" })
	public void TC025_allSubWalletRefund_partialRefund() throws NumberFormatException{
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(35);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount()
				.withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(20);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(subWalletAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(subWalletAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from all subwallet, including main wallet", groups = {
			"regression" })
	public void TC026_mainWithAllSubWalletRefund_partialRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(45);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(20);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | Test Case to verify refund, in case of withdraw from main wallet", groups = { "regression" })
	public void TC027_mainWalletRefund_partialRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,0, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.MainOnly.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | PreConvinience Merchant | CommissionRollback - 'R' | Test Case to verify refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC028_refundforPreConvinienceMerchanct_fullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount).withIsCommissionRollback("R");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Full Refund | PreConvinience Merchant | CommissionRollback - 'C' | Test Case to verify refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC029_refundforPreConvinienceMerchanct_fullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount).withIsCommissionRollback("C");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Partial Refund | PreConvinience Merchant | CommissionRollback - 'R' | Test Case to verify failure during refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC030_refundforPreConvinienceMerchanct_partialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(30);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(refundAmount).withIsCommissionRollback("R");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Partial Refund | PreConvinience Merchant | CommissionRollback - 'C' | Test Case to verify refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC031_refundforPreConvinienceMerchanct_partialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(30);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(refundAmount).withIsCommissionRollback("C");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Full Refund | PostConvinience Merchant | CommissionRollback - 'C' | Test Case to verify refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC032_refundforPostConvinienceMerchanct_fullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withIsCommissionRollback("C");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
 		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Full Refund | PostConvinience Merchant | CommissionRollback - 'R' | Test Case to verify refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC033_refundforPostConvinienceMerchanct_fullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withIsCommissionRollback("R");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Partial Refund | PostConvinience Merchant | CommissionRollback - 'R' | Test Case to verify failure during refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC034_refundforPostConvinienceMerchanct_partialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(30);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(refundAmount).withIsCommissionRollback("R");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Partial Refund | PostConvinience Merchant | CommissionRollback - 'C' | Test Case to verify refund, in case of withdraw from all wallets", groups = { "regression" })
	public void TC035_refundforPostConvinienceMerchanct_partialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(30);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(refundAmount).withIsCommissionRollback("C");
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Partial Refund | Test Case to verify refund to main only, in case of withdraw from food and main subwallet", groups = {"regression","sanity" })
	public void TC036_mainOnlyRefundWithSubWalletTxn_partialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		BigDecimal totalAmount = new BigDecimal(20);
		BigDecimal subWalletAmount = new BigDecimal(10);
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		BigDecimal refundAmount = new BigDecimal(10);
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(refundAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,0, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Test Case to verify refund is failed, in case refund to different merchant than withdraw", groups = {
			"regression" })
	public void TC037_invalidMerchantRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(30);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		subWalletHelpers.createDebitTxnMap(MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,MerchantHeaders.MERCHANTGUID);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withgIFTVOUCHER(null);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1020);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund successfully, in case of (baseAmount+commissionRollBackAmount=amount)", groups = {
			"regression" })
	public void TC038_withBaseandCommissionRollBackAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(totalAmount).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Full Refund | PreConvinience Merchant |Test Case to verify refund successfully, in case of (baseAmount+commissionRollBackAmount<amount)", groups = {
			"regression" })
	public void TC039_withBaseandCommissionRollBackAmtPreConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount).withBaseAmount(totalAmount.subtract(commissionAmount).subtract(BigDecimal.ONE)).withCommissionRollBackAmount(commissionAmount.subtract(BigDecimal.ONE));
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount.subtract(BigDecimal.ONE));
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund successfully, in case of (baseAmount+commissionRollBackAmount=amount)", groups = {
			"regression" })
	public void TC040_withBaseandCommissionRollBackAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount).withBaseAmount(totalAmount.add(new BigDecimal(2))).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund successfully, in case of (commissionRollBackAmount=commissionAmount)", groups = {
			"regression" })
	public void TC041_withExactCommissionRollBackAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(totalAmount).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund successfully, in case of (commissionRollBackAmount<commissionAmount)", groups = {
			"regression" })
	public void TC042_withExactCommissionRollBackAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(totalAmount.add(BigDecimal.ONE)).withCommissionRollBackAmount(commissionAmount.subtract(BigDecimal.ONE));
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount.subtract(BigDecimal.ONE));
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund successfully, in case of (commissionRollBackAmount>commissionAmount)", groups = {
			"regression" })
	public void TC043_withExactCommissionRollBackAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(totalAmount.subtract(BigDecimal.ONE)).withCommissionRollBackAmount(commissionAmount.add(BigDecimal.ONE));
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount.add(BigDecimal.ONE));
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.RPG_001);
	}

	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund successfully, with baseAmount = 0", groups = {
			"regression" })
	public void TC044_OnlyCommissionRollBackAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(BigDecimal.ZERO).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund successfully, with commissionRollBackAmount = 0", groups = {
			"regression" })
	public void TC045_ZeroCommissionRollBackAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(totalAmount.add(commissionAmount)).withCommissionRollBackAmount(BigDecimal.ZERO);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Partial Refund | PreConvinience Merchant |Test Case to verify refund successfully, in case of (commissionRollBackAmount=commissionAmount)", groups = {
			"regression" })
	public void TC046_withExactCommissionRollBackAmtPreConv_PartialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.subtract(new BigDecimal(5))).withBaseAmount(totalAmount.subtract(commissionAmount).subtract(new BigDecimal(5))).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Partial Refund | PreConvinience Merchant |Test Case to verify refund successfully, in case of (commissionRollBackAmount<commissionAmount)", groups = {
			"regression" })
	public void TC047_withExactCommissionRollBackAmtPreConv_PartialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.subtract(new BigDecimal(5))).withBaseAmount(totalAmount.subtract(commissionAmount).subtract(new BigDecimal(5))).withCommissionRollBackAmount(commissionAmount.subtract(BigDecimal.ONE));
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount.subtract(BigDecimal.ONE));
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2R, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}


	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund failure, in case of negative base Amount", groups = {
			"regression" })
	public void TC048_negativeBaseAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(totalAmount.negate()).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Full Refund | PostConvinience Merchant |Test Case to verify refund failure, in case of negative commissionRollback Amount", groups = {
			"regression" })
	public void TC049_negativeCommissionRollAmtPostConv_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequestWithoutCommissionRollBack, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withBaseAmount(totalAmount).withCommissionRollBackAmount(commissionAmount.negate());
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		FundThreadManager.getInstance().withCommissionAmount(commissionAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}

	@Test(description = "Test Case to verify refund failure, in case of again refund for refunded amount", groups = { "regression" })
	public void TC050_refundForAlreadyRefundedTxn() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		WalletRefund refundAgain = new WalletRefund(WalletRefund.defaultRequest, token);
		refundAgain.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refundAgain.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refundAgain.getRequestPojo().getRequest().withAmount(totalAmount);
		refundAgain.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refundAgain, APIStatus.SubWalletRefund.RPG_001);
	}

	@Test(description = "Test Case to verify refund failure, in case of total refundAmount exceding for partial refunded amount", groups = { "regression" })
	public void TC051_refundForAlreadyRefundedTxn() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(new BigDecimal(6));
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		WalletRefund refundAgain = new WalletRefund(WalletRefund.defaultRequest, token);
		refundAgain.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refundAgain.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refundAgain.getRequestPojo().getRequest().withAmount(new BigDecimal(6));
		refundAgain.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refundAgain, APIStatus.SubWalletRefund.RPG_001);
	}


	@Test(description = "Test Case to verify refund success, in case of multi partial refunded", groups = { "regression" })
	public void TC052_multiPartialRefund() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(new BigDecimal(4));
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		WalletRefund refundAgain = new WalletRefund(WalletRefund.defaultRequest, token);
		refundAgain.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refundAgain.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refundAgain.getRequestPojo().getRequest().withAmount(new BigDecimal(4));
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refundAgain.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refundAgain, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,0, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.MainOnly.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test Case to verify refund failure, in case of negative refund Amount", groups = { "regression" })
	public void TC053_negativeRefundAmount() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount.negate());
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Test Case to verify refund failure, in case of zero refund Amount", groups = { "regression" })
	public void TC054_zeroRefundAmount() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(BigDecimal.ZERO);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Test Case to verify refund failure, in case of invalid TxnGuid", groups = { "regression" })
	public void TC055_invalidTxnGuid() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(Integer.toString(CommonHelpers.getInstance().getRandomNumber(10)));
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1009);
	}


	@Test(description = "Test Case to verify refund failure, in case of blank TxnGuid", groups = { "regression" })
	public void TC056_blankTxnGuid() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.TS_1000);
	}

	@Test(description = "Test Case to verify refund failure, in case of null TxnGuid", groups = { "regression" })
	public void TC057_nullTxnGuid() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(null);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.TS_1000);
	}


	@Test(description = "Test Case to verify refund failure, in case of blank refund Amount", groups = { "regression" })
	public void TC058_blankRefundAmount() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}


	@Test(description = "Test Case to verify refund failure, in case of null refund Amount", groups = { "regression" })
	public void TC059_nullRefundAmount() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withAmount(null);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1007);
	}

	@Test(description = "Test Case to verify refund failure, in case of invalid currency Code", groups = { "regression" })
	public void TC060_invalidCurrencyCode() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String withdrawTxnId = Integer.toString(CommonHelpers.getInstance().getRandomNumber(4));
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().withCurrencyCode("INR1");
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1018);
	}

	@Test(description = "Test Case to verify refund failure, in case of blank currency Code", groups = { "regression" })
	public void TC061_blankCurrencyCode() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String withdrawTxnId = Integer.toString(CommonHelpers.getInstance().getRandomNumber(4));
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().withCurrencyCode("");
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1018);
	}


	@Test(description = "Test Case to verify refund failure, in case of null currency Code", groups = { "regression" })
	public void TC062_nullCurrencyCode() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String withdrawTxnId = Integer.toString(CommonHelpers.getInstance().getRandomNumber(4));
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().withCurrencyCode(null);
		refund.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1018);
	}

	@Test(description = "Test Case to verify refund failure, in case of invalid merchant Guid", groups = { "regression" })
	public void TC063_invalidMerchantGuid() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().withMerchantGuid(CommonHelpers.getInstance().getRandomString());
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.WM_1001);
	}


	@Test(description = "Test Case to verify refund failure, in case of blank merchant Guid", groups = { "regression" })
	public void TC064_blankMerchantGuid() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
//		refund.getRequestPojo().getRequest().withMerchantGuid(CommonHelpers.getInstance().getRandomString());
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.WM_1003);
	}


	@Test(description = "Test Case to verify refund failure, in case of null merchant Guid", groups = { "regression" })
	public void TC065_blankMerchantGuid() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.defaultRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId);
		refund.getRequestPojo().getRequest().withAmount(totalAmount);
		refund.getRequestPojo().getRequest().withMerchantGuid(null);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.WM_1003);
	}


	@Test(description = " CommissionRollback - 'R' | Test Case to verify refund failure in case both IsCommissionRollBack and baseAmount are present in request", groups = { "regression" })
	public void TC066_commissionRollback_BaseAmt(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.PostConvEnabledMerchant;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.add(commissionAmount)).withIsCommissionRollback("R").withBaseAmount(totalAmount).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1071);
	}


	@Test(description = " CommissionRollback - 'C' | Test Case to verify refund failure in case both IsCommissionRollBack and baseAmount are present in request, with commissionRollBackAmt not equal to zero", groups = { "regression" })
	public void TC067_commissionRollback_BaseAmt(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount).withIsCommissionRollback("C").withBaseAmount(totalAmount.subtract(commissionAmount)).withCommissionRollBackAmount(commissionAmount);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_1072);
	}


	@Test(description = "Partial Refund | CommissionRollback - 'C' | Test Case to verify refund failure in case both IsCommissionRollBack and baseAmount are present in request, with commissionRollBackAmt equal to zero", groups = { "regression" })
	public void TC068_commissionRollback_BaseAmt_PartialRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		BigDecimal commissionAmount = FundThreadManager.getInstance().getCommissionAmount();
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount.subtract(commissionAmount)).withIsCommissionRollback("C").withBaseAmount(totalAmount.subtract(commissionAmount)).withCommissionRollBackAmount(BigDecimal.ZERO);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());

	}


	@Test(description = "Full Refund | CommissionRollback - 'C' | Test Case to verify refund failure in case both IsCommissionRollBack and baseAmount are present in request, with commissionRollBackAmt equal to zero", groups = { "regression" })
	public void TC069_commissionRollback_BaseAmt_FullRefund(){
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		MerchantTypes merchantTypes = MerchantTypes.REGRESSIONMERCHANT;
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(40);
		BigDecimal subWalletAmount = new BigDecimal(5);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount).withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		subWalletHelpers.addMoneyInSubwalletMainwallet(merchantTypes,mobileNumber);
		walletWithdraw.setCaptureExtent(false);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
		String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
		WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, token);
		refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount).withIsCommissionRollback("C").withBaseAmount(totalAmount).withCommissionRollBackAmount(BigDecimal.ZERO);
		refund.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount).withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		subWalletHelpers.createCreditTxnMap(merchantTypes);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		refund.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
		subWalletHelpers.validateCheckUserBalance(merchantTypes);
		subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
		ScratchPadValidator.getInstance().validateScratchPad(
				ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
						.getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
				refund.getRequestPojo().getRequest().getMerchantOrderId());

	}

    @Test(description = "Test Case to verify refund for expired subwallet", groups = { "regression" })
	public void TC070_expireWalletRefund(){
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        BigDecimal totalAmount = new BigDecimal(2);
        String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
                MerchantHeaders.MERCHANTGUID);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        int ppiId = subWalletHelpers.addSubWalletFundsAndGetPPIId(mobileNumber,"FOOD",totalAmount,merchantTypes);
        WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
        walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
        walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
        walletWithdraw.setCaptureExtent(false);
        walletWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
        String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
        subWalletHelpers.expireSubWalletByOneHourViaPPIId(ppiId);
        WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
        refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount);
        refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
        refund.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.GE_0003);
    }


    @Test(description = "Test Case to verify refund for older txn", groups = { "regression" })
    public void TC071_oldTxnRefund(){
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        MerchantTypes merchantTypes = MerchantTypes.SUBWALLETMERCHANT;
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        BigDecimal totalAmount = new BigDecimal(2);
        String mGuid = MerchantManager.getInstance().getMerchantDetails(merchantTypes,
                MerchantHeaders.MERCHANTGUID);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        subWalletHelpers.addSubWalletFundsAndGetPPIId(mobileNumber,"FOOD",totalAmount,merchantTypes);
        WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
        walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
        walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
        walletWithdraw.setCaptureExtent(false);
        walletWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(walletWithdraw,APIStatus.WalletWithdraw.SUCCESS);
        String withdrawTxnId = walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId();
        subWalletHelpers.updateNSTRTimeStamp(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
        WalletRefund refund = new WalletRefund(WalletRefund.subWalletRefundRequest, UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
        refund.getRequestPojo().getRequest().withTxnGuid(withdrawTxnId).withMerchantGuid(mGuid).withAmount(totalAmount);
        refund.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
        subWalletHelpers.createCreditTxnMap(merchantTypes);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        refund.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(refund, APIStatus.SubWalletRefund.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidationRefund(refund.getRequestPojo().getRequest().getMerchantOrderId(),1,1, walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
        subWalletHelpers.validateCheckUserBalance(merchantTypes);
        subWalletHelpers.extendedPpiInfoVerification(refund.getRequestPojo().getRequest().getMerchantOrderId(),DBQueries.refundExtendedPpiInfoVerificationOrderId,merchantTypes);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 2, (short) 0);
        ScratchPadValidator.getInstance().validateScratchPad(
                ScratchPad.SubWalletMain.Transaction_2, MerchantManager.getInstance()
                        .getMerchantDetails(merchantTypes, MerchantHeaders.MERCHANTWALLETGUID),
                refund.getRequestPojo().getRequest().getMerchantOrderId());
    }



}
