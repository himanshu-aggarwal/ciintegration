package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.ClaimLifafa;
import com.paytm.apiRequestBuilders.wallet.walletWeb.CreateLifafa;
import com.paytm.constants.Constants;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.DBValidation;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.*;
import com.paytm.framework.utils.DatabaseUtil;
import com.paytm.helpers.SubWalletHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.ScratchPadValidator;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-ClaimLifafa")
public class ClaimLifafaTest extends WalletBaseTest {


	@Test(description = "Test case to verify claim Lifafa API for Single Distribution Type ", groups = { "regression" })
	public void TC001_claimSingleLifafa() {
		BigDecimal proposedQuantity=new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		CreateLifafa createLifafa = new CreateLifafa(CreateLifafa.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		String creatorId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber);
		List<String> list = new ArrayList<>();
		String payeePhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String payeeId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payeePhoneNumber);
		list.add(payeePhoneNumber);
		createLifafa.getRequestPojo().withCreatorId(creatorId);
		createLifafa.getRequestPojo().withProposedQuantity(proposedQuantity);
		createLifafa.getRequestPojo().withRecipientList(list);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT,mobileNumber);
		createLifafa.createRequestJsonAndExecute();
		String lifafaKey = createLifafa.getResponsePojo().getLifafaKey();
		ClaimLifafa claimLifafa = new ClaimLifafa(ClaimLifafa.defaultRequest,
				UserManager.getInstance().getPayeeUserDetails(UserHeaders.WALLETSCOPETOKEN));
		claimLifafa.getRequestPojo().withLifafaKey(lifafaKey);
		claimLifafa.createRequestJsonAndExecute();
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.LIFAFAKEY, lifafaKey);
		String lifafaId = fetchLifafaId(params);
		params.put(DBEnums.LIFAFAID, lifafaId);
		params.put(DBEnums.TXNTYPE, "50");
		params.put(DBEnums.PAYEEID, creatorId);
		params.put(DBEnums.PAYERID, creatorId);
		params.put(DBEnums.TXNAMOUNT, createLifafa.getRequestPojo().getProposedQuantity());
		params.put(DBEnums.TXNMESSAGE, "SUCCESS");
		params.put(DBEnums.TXNSTATUS, "1");
		params.put(DBEnums.LIFAFATXNSTATUS, "ACTIVATED");
		params.put(DBEnums.RECEIVERCOUNT, createLifafa.getRequestPojo().getProposedReceiverCount());
		params.put(DBEnums.STRTYPE, createLifafa.getRequestPojo().getStrategyType());
		params.put(DBEnums.DISTYPE, createLifafa.getRequestPojo().getDistributionType());
		dbValidation.isEntryPresent(DBQueries.lifafaDetailsVal, params);
		dbValidation.isEntryPresent(DBQueries.nstrSingleLifafa, params);
		params.put(DBEnums.PAYEEID, payeeId);
		params.put(DBEnums.TXNTYPE, "5");
		String merchantOrderId = dbValidation.runDbQuery(DBQueries.fetchLifafaMerchantOrderIdNSTR,params,"log4j").get(0).get("merchant_order_id").toString();
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_5,"",merchantOrderId);
		dbValidation.isEntryPresent(DBQueries.lifafaRecipientDetailsVal, params);
		dbValidation.isEntryPresent(DBQueries.nstrSingleLifafa, params);
	}

	@Test(description = "Test case to verify claim Lifafa API for Broadcast Distribution Type", groups = { "regression" })
	public void TC002_claimBroadcastLifafa() {
		BigDecimal proposedQuantity=new BigDecimal(10);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		CreateLifafa createLifafa = new CreateLifafa(CreateLifafa.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		String creatorId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber);
		String payeePhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String payeeId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payeePhoneNumber);
		createLifafa.getRequestPojo().withCreatorId(creatorId);
		createLifafa.getRequestPojo().withProposedQuantity(proposedQuantity);
		createLifafa.getRequestPojo().withProposedReceiverCount(2);
		createLifafa.getRequestPojo().withDistributionType("BROADCAST");
		createLifafa.getRequestPojo().withStrategyType("RANDOM");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT,mobileNumber);
		createLifafa.createRequestJsonAndExecute();
		String lifafaKey = createLifafa.getResponsePojo().getLifafaKey();
		ClaimLifafa claimLifafa = new ClaimLifafa(ClaimLifafa.defaultRequest,
				UserManager.getInstance().getPayeeUserDetails(UserHeaders.WALLETSCOPETOKEN));
		claimLifafa.getRequestPojo().withLifafaKey(lifafaKey);
		claimLifafa.createRequestJsonAndExecute();
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.LIFAFAKEY, lifafaKey);
		String lifafaId = fetchLifafaId(params);
		params.put(DBEnums.LIFAFAID, lifafaId);
		params.put(DBEnums.TXNTYPE, "50");
		params.put(DBEnums.PAYEEID, creatorId);
		params.put(DBEnums.PAYERID, creatorId);
		params.put(DBEnums.TXNAMOUNT, createLifafa.getRequestPojo().getProposedQuantity());
		params.put(DBEnums.TXNMESSAGE, "SUCCESS");
		params.put(DBEnums.TXNSTATUS, "1");
		params.put(DBEnums.LIFAFATXNSTATUS, "ACTIVATED");
		params.put(DBEnums.RECEIVERCOUNT, createLifafa.getRequestPojo().getProposedReceiverCount());
		params.put(DBEnums.STRTYPE, createLifafa.getRequestPojo().getStrategyType());
		params.put(DBEnums.DISTYPE, createLifafa.getRequestPojo().getDistributionType());
		dbValidation.isEntryPresent(DBQueries.lifafaDetailsVal, params);
		dbValidation.isEntryPresent(DBQueries.nstrSingleLifafa, params);
		params.put(DBEnums.PAYEEID, payeeId);
		params.put(DBEnums.TXNTYPE, "5");
		dbValidation.isEntryPresent(DBQueries.nstrBroadcastLifafa, params);
		dbValidation.isEntryPresent(DBQueries.amountDistributeDetailsVal, params);
		UserManager.getInstance().releaseUser();
		String payee2PhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_PRIME_WALLET);
		String payee2Id = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payee2PhoneNumber);
		ClaimLifafa claimLifafa2 = new ClaimLifafa(ClaimLifafa.defaultRequest,
				UserManager.getInstance().getPayeeUserDetails(UserHeaders.WALLETSCOPETOKEN));
		claimLifafa2.getRequestPojo().withLifafaKey(lifafaKey);
		claimLifafa2.createRequestJsonAndExecute();
		params.put(DBEnums.PAYEEID, payee2Id);
		dbValidation.isEntryPresent(DBQueries.nstrBroadcastLifafa, params);
		dbValidation.isEntryPresent(DBQueries.amountDistributeDetailsVal, params);
		dbValidation.isEntryPresent(DBQueries.broadcastAmountDistribute, params);
		dbValidation.isEntryPresent(DBQueries.broadcastNstrDetails, params);
	}

	String fetchLifafaId(Map<Object, Object> params) {
		DBValidation dbValidation = new DBValidation();
		String fetchLifafaIdQuery = dbValidation.fetchQuery(DBQueries.fetchLifafaId, params);
		List<Map<String, Object>> result = DatabaseUtil.getInstance()
				.executeSelectQuery(Constants.DBConnectionURL.WALLET_DB_CONNECTION_URL, fetchLifafaIdQuery);
		String lifafaId = result.get(0).get("id").toString();
		return lifafaId;

	}

}
