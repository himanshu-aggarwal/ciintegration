package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.ResendOTPWithdraw;
import com.paytm.apiRequestBuilders.wallet.walletWeb.ValidateOTPWithdraw;
import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletWithDraw;
import com.paytm.constants.ClassNameExt;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.*;
import com.paytm.utils.OfflinePaymentTokenAnalyzer;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.math.BigDecimal;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-V4WalletWithdraw")
public class V4WalletWithDrawTest extends WalletBaseTest {

	@Test(description = "Test case to verify V4 Withdraw of Basic User", groups = { "regression", "sanity" })
	public void TC001_v4ValidateBasicUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to verify V4 Withdraw of Prime User", groups = { "regression", "sanity" })
	public void TC002_v4ValidatePrimeUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Min Kyc", groups = { "regression",
			"sanity" })
	public void TC003_withdrawForMinKycUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_MIN_KYC);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Aadhar OTP User", groups = {
			"regression", "sanity" })
	public void TC004_withdrawForAadharOTPUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Primitive User", groups = { "regression",
			"sanity" })
	public void TC005_withdrawForPrimitiveUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIMITIVE);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify ressend OTP response", groups = { "regression", "sanity" })
	public void TC006_verifyResendOTPResponse() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setPhone(phone);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(resendOtpObj, APIStatus.WalletWithdraw.RESENDOTPSUCCESS);
	}

	@Test(description = "Test case to Verify that user is able to withdraw after resneding OTP", groups = {
			"regression", "sanity" })
	public void TC007_withdrawWithResendOTP() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setPhone(phone);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is not able to resend  OTP after validating OTP for same state", groups = {
			"regression", "sanity" })
	public void TC008_resendingOTPforValidatedState() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setPhone(phone);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		otpwithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj1 = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj1.setPhone(phone);
		resendOtpObj1.setToken(token);
		resendOtpObj1.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		resendOtpObj1.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(resendOtpObj1, APIStatus.WalletWithdraw.OTP_1005);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with invalid phone", groups = {
			"regression", "sanity" })
	public void TC009_withdrawWithInvalidPhone() {
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String phone = "0000000000";
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.ERROR_404);

	}

	@Test(description = "Test case to Verify that user is not able to resend  OTP after validating OTP for same state", groups = {
			"regression", "sanity" })
	public void TC010_resendingOTPWithInvalidState() {
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setPhone(phone);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest().withState("InvalidState");
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(resendOtpObj, APIStatus.WalletWithdraw.GE_0003);

	}

	@Test(description = "case to verify when withdraw and validate hit with different phone number", groups = {
			"regression" })
	public void TC011_WithdrawAndValidateWithDifferentnumber() {
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		String phonenumber = UserManager.getInstance().getNewPayeeUser(UserTypes.NUMBERWITH5);
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phonenumber);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_INVALID);
	}

	@Test(description = "case to verify when ssoToken send in header is of non-sdmerchant", groups = { "regression" })
	public void TC012_ssoTokenOfNonSDMerchant() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.PAYTMSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.unathorized_access);
	}

	@Test(description = "case to verify when ssoToken is of SD Merchant but merchant in request is non-sdmerchant", groups = {
			"regression" })
	public void TC013_nonSDMerchantInRequest() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "case to verify when ssoToken is blank in headers", groups = { "regression" })
	public void TC014_SsoTokenBlankInHeaders() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken("");
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to virify when phone number is blank in headers", groups = { "regression" })
	public void TC015_PhoneNumberIsBlankInHeader() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SDUMPMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.SDUMPMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone("");
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to verify when passing totp of the user in validate transaction in otp field", groups = {
			"regression" })
	public void TC016_passingTotpInValidateTransaction() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getUserWithoutLock(UserTypes.SDMERCHANT);
		String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.PAYTMSCOPETOKEN,mobileNumber);
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String usertoken = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setToken(token);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		String otp = OfflinePaymentTokenAnalyzer.getNewOfflineTotp(usertoken);
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.setPhone(phone);
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp)
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}
}
