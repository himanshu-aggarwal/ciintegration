package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletWithDraw;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.*;
import com.paytm.utils.OfflinePaymentTokenAnalyzer;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-V6WalletWithdraw")
public class V6WalletWithDrawTest extends WalletBaseTest {

	@Test(description = "Test case to verify Withdraw of Basic User", groups = { "regression", "sanity", "smoke" })
	public void TC001_withdrawBasicUser() {
		AuthHelpers authHelpers = new AuthHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID));
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
	}

	@Test(description = "Test Case to verify Withdraw of Prime User", groups = { "regression", "sanity" })
	public void TC002_withdrawPrimeUser() {
		AuthHelpers authHelpers = new AuthHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID));
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
	}

	@Test(description = "Test case to verify withdraw from food subwallet", groups = { "regression", "sanity",
			"smoke" })
	public void TC003_foodSubWalletWithdraw() throws NumberFormatException, IOException {
		AuthHelpers authHelpers = new AuthHelpers();
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(2);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from all the subwallets, including main", groups = { "regression",
			"sanity" })
	public void TC004_allSubWalletWithMainWithdraw() throws IOException {
		AuthHelpers authHelpers = new AuthHelpers();
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		BigDecimal totalAmount = new BigDecimal(45);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [C]", groups = {
			"regression", "sanity" })
	public void TC005_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		AuthHelpers authHelpers = new AuthHelpers();
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is able to withdraw for Min Kyc", groups = { "regression",
			"sanity" })
	public void TC006_withdrawForMinKycUser() {
		AuthHelpers authHelpers = new AuthHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_MIN_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is able to withdraw for Aadhar OTP User", groups = {
			"regression", "sanity" })
	public void TC007_withdrawForAadharOTPUser() {
		AuthHelpers authHelpers = new AuthHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Primitive User", groups = { "regression",
			"sanity" })
	public void TC008_withdrawForPrimitiveUser() {
		AuthHelpers authHelpers = new AuthHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIMITIVE);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is not able to withdraw to inactive merchant", groups = {
			"regression" })
	public void TC009_withdrawToInactiveMerchant() {
		AuthHelpers authHelpers = new AuthHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.INACTIVEMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1035);
	}

	@Test(description = "Executing Test Case to test Withdraw from when Amount Limit is breached", groups = {
			"regression" })
	public void TC010_AmountBreachSubWalletWithdraw() throws IOException {
		AuthHelpers authHelpers = new AuthHelpers();
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		BigDecimal totalAmount = new BigDecimal(30);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETAMOUNTBREACH,
				MerchantHeaders.MERCHANTGUID);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETAMOUNTBREACH,
				MerchantHeaders.NAME);
		params.put(DBEnums.MERCHANTID, mid);
		params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
		params.put(DBEnums.USERID, custId);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		issuerMapping.subwalletAmountLimitBreach(params);
		String[] issuerId = new String[] { "SUBWALLETISSUERAMOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETAMOUNTBREACH,
				issuerId);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
		issuerMapping.resetLimit();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETAMOUNTBREACH);
	}

	@Test(description = "Executing Test Case to test Withdraw from when Count Limit is breached", groups = {
			"regression" })
	public void TC011_CountBreachSubWalletWithdraw() throws IOException {
		AuthHelpers authHelpers = new AuthHelpers();
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(20);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETCOUNTBREACH,
				MerchantHeaders.NAME);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETCOUNTBREACH,
				MerchantHeaders.MERCHANTGUID);
		params.put(DBEnums.MERCHANTID, mid);
		params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
		params.put(DBEnums.USERID, custId);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		issuerMapping.subwalletCountLimitBreach(params);
		String[] issuerId = new String[] { "SUBWALLETISSUERCOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETCOUNTBREACH,
				issuerId);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		issuerMapping.resetLimit();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETCOUNTBREACH);
	}

	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [C]", groups = {
			"regression", "sanity" })
	public void TC012_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		AuthHelpers authHelpers = new AuthHelpers();
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is able to withdraw to Post convenience Enabled merchant", groups = {
			"regression" })
	public void TC013_withdrawAmountToPostConvEnabledmerchant() {
		AuthHelpers authHelpers = new AuthHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PostConvEnabledMerchant,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.PostConvEnabledMerchant);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber, token)
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PostConvEnabledMerchant,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "case to verify when offline code and offline otp is of different user")
	public void TC014_TotpAndPhoneNumberDifferentUser() {
		AuthHelpers authHelpers = new AuthHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		UserManager.getInstance().getNewPayeeUser(UserTypes.NUMBERWITH5);
		String token = UserManager.getInstance().getPayeeUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID));
		String code = authHelpers.verifyDeviceAndGetOfflineCode(mobileNumber,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN))
				+ OfflinePaymentTokenAnalyzer.getNewOfflineTokenOtp(token) + "2c";
		walletWithdraw.setCode(code);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.INVALIDOTP);
	}

	@Test(description = "case to verify when code in headers is blank")
	public void TC015_TotpIsBlankInHeaders() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setCode("");
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID));
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.BAD_REQUEST);
	}
}
