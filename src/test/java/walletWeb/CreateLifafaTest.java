package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.CreateLifafa;
import com.paytm.assertions.AssertionValidations;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.UserManager;
import com.paytm.utils.DBValidation;
import com.paytm.utils.WalletBaseTest;
import com.paytm.enums.*;
import com.paytm.helpers.SubWalletHelpers;
import com.paytm.constants.ClassNameExt;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import com.paytm.helpers.CommonValidation;
import com.paytm.helpers.ScratchPadValidator;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-CreateLifafa")
public class CreateLifafaTest extends WalletBaseTest {

	@Test(description = "Test case to verify create Lifafa API for Single Distribution Type ", groups = { "regression" })
	public void TC001_createSingleLifafa() {
		BigDecimal proposedQuantity=new BigDecimal(10);
		ClaimLifafaTest claim = new ClaimLifafaTest();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		CreateLifafa createLifafa = new CreateLifafa(CreateLifafa.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		String creatorId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber);
		List<String> list = new ArrayList<>();
		String payeePhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String payeeId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payeePhoneNumber);
		list.add(payeePhoneNumber);
		createLifafa.getRequestPojo().withProposedQuantity(proposedQuantity);
		createLifafa.getRequestPojo().withCreatorId(creatorId);
		createLifafa.getRequestPojo().withRecipientList(list);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT,mobileNumber);
		createLifafa.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createLifafa, APIStatus.CreateLifafa.SUCCESS);
		AssertionValidations.verifyAssertNotNull(createLifafa.getResponsePojo().getLifafaKey());
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.TXNTYPE, "50");
		params.put(DBEnums.LIFAFAKEY, createLifafa.getResponsePojo().getLifafaKey());
		String merchantOrderId = dbValidation.runDbQuery(DBQueries.fetchLifafaMerchantOrderIdNSTR,params,"log4j").get(0).get("merchant_order_id").toString();
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_50,"",merchantOrderId);

		String lifafaId = claim.fetchLifafaId(params);
		params.put(DBEnums.LIFAFAID, lifafaId);
		params.put(DBEnums.PAYEEID, creatorId);
		params.put(DBEnums.PAYERID, creatorId);
		params.put(DBEnums.TXNAMOUNT, createLifafa.getRequestPojo().getProposedQuantity());
		params.put(DBEnums.TXNMESSAGE, "SUCCESS");
		params.put(DBEnums.TXNSTATUS, "1");
		params.put(DBEnums.LIFAFATXNSTATUS, "ACTIVATED");
		params.put(DBEnums.STRTYPE, createLifafa.getRequestPojo().getStrategyType());
		params.put(DBEnums.DISTYPE, createLifafa.getRequestPojo().getDistributionType());
		params.put(DBEnums.RECEIVERCOUNT, createLifafa.getRequestPojo().getProposedReceiverCount());
		dbValidation.isEntryPresent(DBQueries.nstrSingleLifafa, params);
		dbValidation.isEntryPresent(DBQueries.lifafaDetailsVal, params);
		params.put(DBEnums.PAYEEID, payeeId);
		dbValidation.isEntryPresent(DBQueries.lifafaRecipientDetailsVal, params);
	}

	@Test(description = "Test case to verify create Lifafa API for Broadcast Distribution Type", groups = { "regression" })
	public void TC002_createBroadcastLifafa() {
		BigDecimal proposedQuantity=new BigDecimal(2);
		ClaimLifafaTest claim = new ClaimLifafaTest();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		CreateLifafa createLifafa = new CreateLifafa(CreateLifafa.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		String creatorId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber);
		String payeePhoneNumber = UserManager.getInstance().getNewPayeeUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String payeeId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, payeePhoneNumber);
		createLifafa.getRequestPojo().withCreatorId(creatorId);
		createLifafa.getRequestPojo().withProposedQuantity(proposedQuantity);
		createLifafa.getRequestPojo().withDistributionType("BROADCAST");
		createLifafa.getRequestPojo().withStrategyType("RANDOM");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SCWMERCHANT,mobileNumber);
		createLifafa.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(createLifafa, APIStatus.CreateLifafa.SUCCESS);
		AssertionValidations.verifyAssertNotNull(createLifafa.getResponsePojo().getLifafaKey());
		DBValidation dbValidation = new DBValidation();
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.TXNTYPE, "50");
		params.put(DBEnums.LIFAFAKEY, createLifafa.getResponsePojo().getLifafaKey());

		String merchantOrderId = dbValidation.runDbQuery(DBQueries.fetchLifafaMerchantOrderIdNSTR,params,"log4j").get(0).get("merchant_order_id").toString();
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_50,"",merchantOrderId);
		String lifafaId = claim.fetchLifafaId(params);
		params.put(DBEnums.LIFAFAID, lifafaId);
		params.put(DBEnums.PAYEEID, creatorId);
		params.put(DBEnums.PAYERID, creatorId);
		params.put(DBEnums.TXNAMOUNT, createLifafa.getRequestPojo().getProposedQuantity());
		params.put(DBEnums.TXNMESSAGE, "SUCCESS");
		params.put(DBEnums.TXNSTATUS, "1");
		params.put(DBEnums.LIFAFATXNSTATUS, "ACTIVATED");
		params.put(DBEnums.STRTYPE, createLifafa.getRequestPojo().getStrategyType());
		params.put(DBEnums.DISTYPE, createLifafa.getRequestPojo().getDistributionType());
		params.put(DBEnums.RECEIVERCOUNT, createLifafa.getRequestPojo().getProposedReceiverCount());
		dbValidation.isEntryPresent(DBQueries.nstrSingleLifafa, params);
		dbValidation.isEntryPresent(DBQueries.lifafaDetailsVal, params);
		params.put(DBEnums.PAYEEID, payeeId);
		dbValidation.isEntryAbsent(DBQueries.lifafaRecipientDetailsVal, params);
	}
}
