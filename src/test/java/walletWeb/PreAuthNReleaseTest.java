package walletWeb;

import com.paytm.apiRequestBuilders.wallet.oldService.CheckUserBalance;
import com.paytm.apiRequestBuilders.wallet.walletWeb.PreAuthNRelease;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.exceptions.DBExceptions.DBEntryMissingException;
import com.paytm.helpers.CommonValidation;
import com.paytm.helpers.LimitAuditInfoValidation;
import com.paytm.helpers.ScratchPadValidator;
import com.paytm.helpers.SubWalletHelpers;
import com.paytm.utils.DBValidation;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-PreAuthNRelease")
public class PreAuthNReleaseTest extends WalletBaseTest {

    @Test(description = "Prime User | Validate that amount is blocked in case of Preauth", groups = {"regression"})
    public void TC001_preAuthPrimeUser() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.NOSUBWALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.normalPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.REGRESSIONMERCHANT);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1, 0);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.REGRESSIONMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());
    }

    @Test(description = "Test case to verify preauth from food subwallet", groups = {"regression"})
    public void TC002_foodSubWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from gift subwallet", groups = {"regression"})
    public void TC003_giftSubWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withgIFT(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from toll subwallet", groups = {"regression"})
    public void TC004_tollSubWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withtOLL(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from closed loop wallet", groups = {"regression"})
    public void TC005_closedLoopWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withcLOSED_LOOP_WALLET(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from closed loop sub wallet", groups = {"regression"})
    public void TC006_closedLoopSubWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from all subwallets", groups = {"regression"})
    public void TC007_allSubWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(35);
        BigDecimal subWalletAmount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withfUEL(subWalletAmount)
                .withcLOSED_LOOP_SUB_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from all subwallets with main", groups = {"regression"})
    public void TC008_allSubWalletwithMainPreAuth() {
        BigDecimal txnamount = new BigDecimal(40);
        BigDecimal subWalletAmount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withfUEL(subWalletAmount)
                .withcLOSED_LOOP_SUB_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from main only", groups = {"regression"})
    public void TC009_mainWalletOnlyPreAuth() {
        BigDecimal txnamount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,0);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from international Fund Transfer sub wallet", groups = {"regression"})
    public void TC010_internationalFundTransSubWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Prime User | Validate that amount is blocked in case of Preauth and then released", groups = {"regression"})
    public void TC011_preAuthNReleasePrimeUser() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET,UserTypes.NOSUBWALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.normalPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.REGRESSIONMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.normalPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.REGRESSIONMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);
        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,0);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1,0);

        subWalletHelpers.validateCheckUserBalance(MerchantTypes.REGRESSIONMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());
    }

    @Test(description = "Test case to verify preauth from food subwallet and then release the amount", groups = {"regression"})
    public void TC012_foodSubWalletPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from gift subwallet and then release the amount", groups = {"regression"})
    public void TC013_giftSubWalletPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withgIFT(txnamount);
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from toll subwallet and then release the amount", groups = {"regression"})
    public void TC014_tollSubWalletPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        CheckUserBalance beforeCheckUserBalance = subWalletHelpers.fetchCheckBalanceResponse(merchantGuid, token);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withtOLL(txnamount);
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from close loop wallet and then release the amount", groups = {"regression"})
    public void TC015_closeLoopWalletPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withcLOSED_LOOP_WALLET(txnamount);
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from close loop subwallet and then release the amount", groups = {"regression"})
    public void TC016_closeLoopSubWalletPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(txnamount);
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from all subwallets and then release the amount", groups = {"regression"})
    public void TC017_allSubWalletsPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(35);
        BigDecimal subWalletAmount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withfUEL(subWalletAmount)
                .withcLOSED_LOOP_SUB_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount);
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from all subwallets and then release the amount", groups = {"regression"})
    public void TC018_allSubWalletsWithMainPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(40);
        BigDecimal subWalletAmount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withfUEL(subWalletAmount)
                .withcLOSED_LOOP_SUB_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();

        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.normalPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from all subwallets and then release the amount", groups = {"regression"})
    public void TC019_mainOnlyPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.normalPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 0);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 0);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test case to verify preauth from international fund transfer subwallet and then release the amount", groups = {"regression"})
    public void TC020_internationalFundTransSubWalletPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(2.0);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(txnamount);
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }


    @Test(description = "Test Case to verify preauth is done from main in case of Merchant having no SubWallets", groups = { "regression" })
    public void TC021_noSubWalletMerchantPreAuth() {
        BigDecimal txnamount = new BigDecimal(45);
        BigDecimal subWalletAmount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOSUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().setUpdateAmountManager(false);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withfUEL(subWalletAmount)
                .withcLOSED_LOOP_SUB_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount);
        FundThreadManager.getInstance().withClosedLoopWalletAmount(subWalletAmount).withClosedLoopSubWalletAmount(subWalletAmount).withTotalAmount(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.NOSUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.NOSUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.NOSUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());
    }



    @Test(description = "Test Case to verify preauth is done from main in case of Merchant having no SubWallets and then release the amount", groups = {"regression"})
    public void TC022_noSubWalletMerchantPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(45);
        BigDecimal subWalletAmount = new BigDecimal(5);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOSUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().setUpdateAmountManager(false);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid);
        preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount).withgIFT(subWalletAmount).withtOLL(subWalletAmount).withfUEL(subWalletAmount)
                .withcLOSED_LOOP_SUB_WALLET(subWalletAmount).withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount).withcLOSED_LOOP_WALLET(subWalletAmount);
        FundThreadManager.getInstance().withClosedLoopWalletAmount(subWalletAmount).withClosedLoopSubWalletAmount(subWalletAmount).withTotalAmount(txnamount);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.NOSUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        subWalletHelpers.createCreditTxnMap(MerchantTypes.NOSUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.NOSUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.NOSUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Executing Test Case to test preauth from User with insufficient balance in main wallet", groups = { "regression" })
    public void TC023_insufficientBalanceMainPreAuth() {
        BigDecimal txnamount = new BigDecimal(50);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
                MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().setUpdateAmountManager(false);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(new BigDecimal(5));
        FundThreadManager.getInstance().withTotalAmount(new BigDecimal(10));
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.WM_1006);
    }


    @Test(description = "Test case to verify preauth from main wallet in case of insufficient balance in food subwallet", groups = {"regression"})
    public void TC024_insufficientBalanceFoodSubWalletPreAuth() {
        BigDecimal txnamount = new BigDecimal(100);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().setUpdateAmountManager(false);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
        FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.callCheckUserBalance(mobileNumber,MerchantTypes.SUBWALLETMERCHANT);
        LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
        limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
        limitAudit.validateLimitAuditInfo(mobileNumber, 1, 22, (short) 1);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "", preAuth.getRequestPojo().getRequest().getMerchantOrderId());

    }

    @Test(description = "Test case to verify preauth from main wallet in case of insufficient balance in food subwallet and then release", groups = {"regression"})
    public void TC025_insufficientBalanceFoodSubWalletPreAuthNRelease() {
        BigDecimal txnamount = new BigDecimal(100);
        String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
        String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT, MerchantHeaders.MERCHANTGUID);
        PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuth.getRequestPojo().setUpdateAmountManager(false);
        preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount).withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
        FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
        subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETMERCHANT,mobileNumber);
        preAuth.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
        String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();

        PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
        preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
        preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount).withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
        FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
        subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETMERCHANT);
        preAuthNRelease.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);


        //nstrValidation of PreAuth Txn for status change
        CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4, 1);
        //nstrValidation of Release Txn
        CommonValidation.getInstance().nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
        subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
        subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
        validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
        ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "", preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());
    }
    
    
	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [C]", groups = {
			"regression", "sanity" })
	public void TC026_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {

		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETWITHDRAWACQUIRER, mobileNumber);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "",
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from issuer only if if Acquirer merchant category is mapped with the one in subwallet_issuer_config Table for Merchant with config [I] = [A] [C]", groups = {
			"regression" })
	public void TC027_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "",
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());
		
		
	
	}
	
	@Test(description = "Test case to verify withdraw from issuer as well as merchant for Merchant with config [I] = [A] [C]", groups = {
			"regression" })
	public void TC028_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER3, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "",
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());
	}
	
	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC029_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER2);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "",
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());
	}
	
	
	@Test(description = "Test case to verify withdraw from issuer only if if Acquirer merchant category is mapped with the one in subwallet_issuer_config Table for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC030_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER2);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "",
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());
	}	
	
	
	@Test(description = "Test case to verify withdraw from issuer as well as merchant for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC031_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER2);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
				MerchantHeaders.MERCHANTWALLETGUID),
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());	
		
	}

	@Test(description = "Test case to verify withdraw from Merchant with config [I] = [ ] [ ] and withdraw is done on Merchant  whose category is null in the merchant Table.", groups = {
			"regression" })
	public void TC032_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY4,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY4,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETACQUIRERBLANKCATEGORY4" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY4, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY4);
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.WM_1006);

	}
	
	
	@Test(description = "Test case to verify withdraw Normal merchant with no entry in SUBWALLET_ISSUER_CONFIG table (But accepts food as mapped with wallet category mapping)", groups = {
			"regression" })
	public void TC033_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		
		BigDecimal txnamount = new BigDecimal(90);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().getRequest().withMerchantGuid(merchantGuid).withAmount(txnamount);
		preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(txnamount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER4", "SUBWALLETWITHDRAWACQUIRER5",
		"SUBWALLETWITHDRAWACQUIRER6" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER4, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantTypes.SUBWALLETWITHDRAWACQUIRER5, MerchantTypes.SUBWALLETWITHDRAWACQUIRER6);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "",
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());
		
		
		
		
		
}
	
	@Test(description = "Test case to verify withdraw from  Merchant category mapped with Subwallet_issuer_config table define issuer (For acquirer not present i:e [ I ] = [ ] [ C ]", groups = {
			"regression" })
	public void TC034_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER5,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal totalAmount = new BigDecimal(20);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().getRequest().withMerchantGuid(mGuid).withAmount(totalAmount);
		preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER5" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER5,
				issuerId);
		preAuth.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER5);
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.WM_1006);
	}
	
	@Test(description = "Test case to verify withdraw from  Merchant category mapped with Subwallet_issuer_config table define issuer (For acquirer not present i:e  but SIC configurations is  i:e [ I ] = [ A ] [ C ]", groups = {
			"regression" })
	public void TC035_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER6,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal totalAmount = new BigDecimal(20);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().getRequest().withMerchantGuid(mGuid).withAmount(totalAmount);
		preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER6" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER6,
				issuerId);
		preAuth.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER6);
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.WM_1006);
	}
	
	
	@Test(description = "Test case to verify withdraw from 2 issuers who have the same acquirer)", groups = {
			"regression" })
	public void TC036_foodSubWalletLimitWithdrawPreAuth() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(90);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				MerchantHeaders.MERCHANTGUID);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().getRequest().withMerchantGuid(merchantGuid).withAmount(txnamount);
		preAuth.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(txnamount);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER3", "SUBWALLETWITHDRAWISSUER4" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER8, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 1,
				1);
		subWalletHelpers.extendedPpiInfoVerification(preAuth.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER3,
				MerchantTypes.SUBWALLETWITHDRAWISSUER4);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_22, "",
				preAuth.getRequestPojo().getRequest().getMerchantOrderId());
			
	}
	//Pre auth and release test cases
	
	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [C]", groups = {
			"regression", "sanity" })
	public void TC037_foodSubWalletLimitWithdrawPreAuthAndRelease() throws NumberFormatException, IOException {

		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.addMoneyInSubwalletMainwallet(MerchantTypes.SUBWALLETWITHDRAWACQUIRER, mobileNumber);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETWITHDRAWACQUIRER);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,
				1);
		CommonValidation.getInstance()
				.nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.refundExtendedPpiInfoVerificationWithMerchantOrderID, MerchantTypes.SUBWALLETWITHDRAWACQUIRER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "",
				preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());

	}
	
	@Test(description = "Test case to verify withdraw from issuer only if if Acquirer merchant category is mapped with the one in subwallet_issuer_config Table for Merchant with config [I] = [A] [C]", groups = {
	"regression" })
public void TC038_foodSubWalletLimitWithdrawPreAuthAndRelease() throws NumberFormatException, IOException {
	
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER2, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,
				1);
		CommonValidation.getInstance()
				.nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.refundExtendedPpiInfoVerificationWithMerchantOrderID, MerchantTypes.SUBWALLETWITHDRAWACQUIRER2);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "",
				preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());
	
	}
	
	@Test(description = "Test case to verify withdraw from issuer as well as merchant for Merchant with config [I] = [A] [C]", groups = {
	"regression" })
public void TC039_foodSubWalletLimitWithdrawPreAuthAndRelease() throws NumberFormatException, IOException {
		
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER3, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,
				1);
		CommonValidation.getInstance()
				.nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.refundExtendedPpiInfoVerificationWithMerchantOrderID, MerchantTypes.SUBWALLETWITHDRAWACQUIRER3);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "",
				preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());
		
		
	}
	
	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [ ]", groups = {
	"regression" })
public void TC040_foodSubWalletLimitWithdrawPreAuthAndRelease() throws NumberFormatException, IOException {
		
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,
				1);
		CommonValidation.getInstance()
				.nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.refundExtendedPpiInfoVerificationWithMerchantOrderID, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "",
				preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());	
	}
	
	
	@Test(description = "Test case to verify withdraw from issuer as well as merchant for Merchant with config [I] = [A] [ ]", groups = {
	"regression" })
public void TC041_foodSubWalletLimitWithdrawPreAuthAndRelease() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(40));
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,
				1);
		CommonValidation.getInstance()
				.nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.refundExtendedPpiInfoVerificationWithMerchantOrderID, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "",
				preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());
		
	}
	
	//Recheck,Its Failing
	@Test(description = "Test case to verify withdraw Normal merchant with no entry in SUBWALLET_ISSUER_CONFIG table (But accepts food as mapped with wallet category mapping)", groups = {
	"regression" })
public void TC042_foodSubWalletLimitWithdrawPreAuthAndRelease() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantHeaders.NAME);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(60));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER4", "SUBWALLETWITHDRAWACQUIRER5",
		"SUBWALLETWITHDRAWACQUIRER6" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER4, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId).getSubWalletAmount().withfOOD(txnamount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,
				1);
		CommonValidation.getInstance()
				.nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.refundExtendedPpiInfoVerificationWithMerchantOrderID, MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,MerchantTypes.SUBWALLETWITHDRAWACQUIRER5, MerchantTypes.SUBWALLETWITHDRAWACQUIRER6);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4);
		validateNewBlockAmtRequest(preAuthNRelease.getResponsePojo().getResponse().getPreAuthId(), 2);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "",
				preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());
		
	}
	
	@Test(description = "Test case to verify withdraw from 2 issuers who have the same acquirer)", groups = {
	"regression" })
public void TC043_foodSubWalletLimitWithdrawPreAuthAndRelease() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();

		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				MerchantHeaders.MERCHANTGUID);
		PreAuthNRelease preAuth = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuth.getRequestPojo().setUpdateAmountManager(false);
		preAuth.getRequestPojo().getRequest().withOperationType("PREAUTH").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).getSubWalletAmount().withfOOD(txnamount);
		FundThreadManager.getInstance().withTotalAmount(txnamount).withFoodWalletAmount(new BigDecimal(60));
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER3", "SUBWALLETWITHDRAWISSUER4" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER8, issuerId);
		UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		preAuth.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuth, APIStatus.PreAuthNRelease.SUCCESS);
		String preAuthId = preAuth.getResponsePojo().getResponse().getPreAuthId();
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId).getSubWalletAmount().withfOOD(txnamount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.SUCCESS);

		CommonValidation.getInstance().nstrTxnValidation(preAuth.getRequestPojo().getRequest().getMerchantOrderId(), 4,
				1);
		CommonValidation.getInstance()
				.nstrTxnValidation(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.refundExtendedPpiInfoVerificationWithMerchantOrderID, MerchantTypes.SUBWALLETWITHDRAWISSUER3,
				MerchantTypes.SUBWALLETWITHDRAWISSUER4);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8);
		validateNewBlockAmtRequest(preAuth.getResponsePojo().getResponse().getPreAuthId(), 2);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_8, "",
				preAuthNRelease.getRequestPojo().getRequest().getMerchantOrderId());
		
	}
	
	
	@Test(description = "Test case to verify withdraw from 2 issuers who have the same acquirer)", groups = {
	"regression" })
public void TC044_foodSubWalletLimitWithdrawPreAuthAndReleaseInvalidPreAuthID() throws NumberFormatException, IOException {
		BigDecimal txnamount = new BigDecimal(100);
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String merchantGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				MerchantHeaders.MERCHANTGUID);
        SubWalletHelpers subWalletHelpers = new SubWalletHelpers();

		String preAuthId = "5235SGFTF345345";
		PreAuthNRelease preAuthNRelease = new PreAuthNRelease(PreAuthNRelease.subWalletPreAuthRequest, token);
		preAuthNRelease.getRequestPojo().setUpdateAmountManager(false);
		preAuthNRelease.getRequestPojo().getRequest().withOperationType("RELEASE").withAmount(txnamount)
				.withMerchantGuid(merchantGuid).withPreAuthId(preAuthId).getSubWalletAmount().withfOOD(txnamount);
		subWalletHelpers.createCreditTxnMap(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8);
		preAuthNRelease.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(preAuthNRelease, APIStatus.PreAuthNRelease.GE_0001);		
	}
	
        private void validateNewBlockAmtRequest(String preAuthId, Integer txnStatus) {
        try {
            Map<Object, Object> params = new HashMap<>();
            params.put(DBEnums.PREAUTHID, preAuthId);
            if (txnStatus == 1) {
                params.put(DBEnums.BLOCKAMTTXNSTATUS, 1);
                params.put(DBEnums.BLOCKAMTTXNMESSAGE, "BLOCK");
            } else if (txnStatus == 2) {
                params.put(DBEnums.BLOCKAMTTXNSTATUS, 2);
                params.put(DBEnums.BLOCKAMTTXNMESSAGE, "UNBLOCK");
            } else if (txnStatus == 3) {
                params.put(DBEnums.BLOCKAMTTXNSTATUS, 3);
                params.put(DBEnums.BLOCKAMTTXNMESSAGE, "FAILURE");
            } else {
                throw new RuntimeException("Invalid txnStatus");
            }
            DBValidation dbValidation = new DBValidation();
            dbValidation.isEntryPresent(DBQueries.newBlockAmountRequest, params);
        } catch (AssertionError ae) {
            throw new DBEntryMissingException("Entry not found in New_Block_Amount_Request table");
        }
    }

}
