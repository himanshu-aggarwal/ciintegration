package walletWeb;

import com.paytm.apiPojo.wallet.qrCode.CatalogForUTSResponse;
import com.paytm.apiPojo.wallet.walletWeb.QrWithdrawRequest;
import com.paytm.apiRequestBuilders.wallet.walletWeb.QrWithdraw;
import com.paytm.constants.ClassNameExt;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.CommonValidation;
import com.paytm.helpers.QrCodeHelpers;
import com.paytm.helpers.SubWalletHelpers;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.apache.commons.lang.ObjectUtils;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.*;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-QrWithdraw")
public class QrWithdrawTest extends WalletBaseTest {

    @Test(description = "Test Case to verify QrWithdraw with Prime User")
    public void TC001(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.SUCCESS);
    }

    @Test(description = "Test Case to verify QrWithdraw with basic user")
    public void TC002(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.SUCCESS);
    }


    @Test(description = "To execute QrWithdraw with insufficient balance.")
    public void TC003(){

        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2026);
    }

    @Test(description = "To execute QrWithdraw with total amount greater than actual total amount")
    public void TC004(){

        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap).add(new BigDecimal(1));
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2019);
    }
    @Test(description = "To execute QrWithdraw when total amount param is not present.")
    public void TC005(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(null).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.GE_0001);
    }


    @Test(description = "To execute QrWithdraw when currency code is null")
    public void TC006(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId()).withCurrencyCode(null)
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2026);
    }

    @Test(description = "To execute QrWithdraw for a merchant whose entry is not present in ticket_details table.")
    public void TC007(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CLWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.CLWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.GE_0003);
    }

    @Test(description = "To execute QrWithdraw when user is Inactive User ")
    public void TC008(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.INACTIVEWALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2026);
    }

    @Test(description = "To execute QrWithdraw when user is Inactive Merchant ")
    public void TC009(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CLWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.CLWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.GE_0003);
    }

    @Test(description = "To execute QrWithdraw when user is Invalid Merchant.")
    public void TC010(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.CLWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.CLWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.GE_0003);
    }
    @Test(description = "To execute QrWithdraw when Source id is invalid. ")
    public void TC011(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId("0000").withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2009);
    }

    @Test(description = "To execute QrWithdraw when Source id is null.")
    public void TC012(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(null).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2002);
    }

    @Test(description = "To execute QrWithdraw when source name is null")
    public void TC013(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId()).withSourceName(null)
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2010);
    }

    @Test(description = "To execute QrWithdraw when destination Id is null ")
    public void TC014(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(null)
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2003);
    }
    @Test(description = "To execute QrWithdraw when destination Id is invalid.")
    public void TC015(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId("00000")
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2009);
    }
    @Test(description = "To execute QrWithdraw when destination name is null.")
    public void TC016(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId()).withDestinationName(null)
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2011);
    }
    @Test(description = "To execute QrWithdraw when sourceId is invalid")
    public void TC017(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId()).withDestinationName(null)
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType("XYZ");
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2011);
    }
    @Test(description = "To execute QrWithdraw when BusType is null")
    public void TC018(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(null);
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2004);
    }

    @Test(description = "To execute QrWithdraw when route Id is null.")
    public void TC019(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(null)
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2016);
    }

    @Test(description = "To execute QrWithdraw when route Id is invalid")
    public void TC020(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId("000000")
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2009);
    }

    @Test(description = "To execute QrWithdraw when route name is null ")
    public void TC021(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId()).withRouteName(null)
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2017);
    }

    @Test(description = "To execute QrWithdraw when pax details are null")
    public void TC022(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap);
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId()).withRouteName(null)
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(null).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2005);
    }

    @Test(description = "To execute QrWithdraw when amount is less than total amount.")
    public void TC023(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap).subtract(new BigDecimal(1));
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2019);
    }

    @Test(description = "when Pax in pax details is null ")
    public void TC024(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap).subtract(new BigDecimal(1));
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2019);
    }
    @Test(description = "when Pax in pax deatils is invalid.")
    public void TC025(){
        SubWalletHelpers subWallet= new SubWalletHelpers();
        QrCodeHelpers qrCodeHelpers = new QrCodeHelpers();
        CatalogForUTSResponse.GridLayout.Attributes attributes = qrCodeHelpers.getAttributeObjectFromCatalog();
        String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
        String token = UserManager.getInstance().getSpecificUserDetail(UserHeaders.WALLETSCOPETOKEN, phone);
        QrWithdraw qrWithdraw = new QrWithdraw(QrWithdraw.defaultRequest, token);
        Map<String, Integer> fareMap = qrCodeHelpers.fetchFareDetailsFromCatalog(attributes);
        HashMap<String, Integer> paxMap = new LinkedHashMap<>();
        paxMap.put("Adult",2);
        List<QrWithdrawRequest.Request.PaxDetails> paxDetails = createPaxList(paxMap);
        BigDecimal totalAmount = getTotalAmount(fareMap, paxMap).subtract(new BigDecimal(1));
        qrWithdraw.getRequestPojo().getRequest().withRouteId(attributes.getRouteId())
                .withSourceId(attributes.getSourceId()).withDestinationId(attributes.getDestinationId())
                .withMerchantGuid(MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT, MerchantHeaders.MERCHANTGUID))
                .withPaxDetails(paxDetails).withTotalAmount(totalAmount).withBusType(attributes.getBusType());
        FundThreadManager.getInstance().withTotalAmount(totalAmount);
        subWallet.callCheckUserBalance(phone , MerchantTypes.SCWMERCHANT);
        qrWithdraw.createRequestJsonAndExecute();
        CommonValidation.getInstance().basicAsserts(qrWithdraw,APIStatus.QrWithdraw.UTS_2019);
    }







    @Deprecated
    public BigDecimal QrWithdrawFunction(Map<String, Integer> fareMap, List<QrWithdrawRequest.Request.PaxDetails> paxDetails){
    BigDecimal totalAmount = BigDecimal.ZERO;
    for(Map.Entry<String, Integer> entry : fareMap.entrySet()){
        paxDetails.add(new QrWithdrawRequest.Request.PaxDetails().withPax(entry.getKey()).withQuantity(2));
        totalAmount = totalAmount.add(new BigDecimal(entry.getValue()*2));
    }

    return totalAmount;
}

    private BigDecimal getTotalAmount(Map<String, Integer> fareMap, Map<String, Integer> paxData){
        BigDecimal totalAmount = BigDecimal.ZERO;
        for(String entry : fareMap.keySet()){
            totalAmount = totalAmount.add(new BigDecimal(fareMap.get(entry)*paxData.get(entry)));
        }

        return totalAmount;
    }


private List<QrWithdrawRequest.Request.PaxDetails> createPaxList(Map<String, Integer> paxData){
        List<QrWithdrawRequest.Request.PaxDetails> paxList = new LinkedList<>();
        for(String key : paxData.keySet()){
            QrWithdrawRequest.Request.PaxDetails paxDetails = new QrWithdrawRequest.Request.PaxDetails();
            paxDetails.setPax(key);
            paxDetails.setQuantity(paxData.get(key));
            paxList.add(paxDetails);
        }
        return paxList;
}

}
