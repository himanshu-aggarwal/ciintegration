package walletWeb;

import com.jcraft.jsch.JSchException;
import com.paytm.apiRequestBuilders.wallet.oldService.CheckUserBalance;
import com.paytm.apiRequestBuilders.wallet.walletWeb.CheckBalance;
import com.paytm.apiRequestBuilders.wallet.walletWeb.ValidateOTPWithdraw;
import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletWithDraw;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.FundThreadManager;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.*;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-WalletWithdraw")
public class WalletWithDrawTest extends WalletBaseTest {

	@Test(description = "Test case to verify Withdraw of Basic User", groups = { "regression", "sanity", "smoke" })
	public void TC001_withdrawBasicUser() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String merchantGuid = MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID);
		BigDecimal totalAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(merchantGuid).withTotalAmount(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.REGRESSIONMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 0);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.REGRESSIONMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.REGRESSIONMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test Case to verify Withdraw of Prime User", groups = { "regression", "sanity" })
	public void TC002_withdrawPrimeUser() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT, MerchantHeaders.MERCHANTGUID));
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);

	}

	@Test(description = "Test case to verify withdraw from food subwallet", groups = { "regression", "sanity",
			"smoke" })
	public void TC003_foodSubWalletWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(2);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from gift subwallet", groups = { "regression" })
	public void TC004_giftSubWalletWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		BigDecimal totalAmount = new BigDecimal(2);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from toll subwallet", groups = { "regression" })
	public void TC005_tollSubWalletWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		BigDecimal totalAmount = new BigDecimal(2);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from closed loop wallet", groups = { "regression" })
	public void TC006_closedLoopWalletWithdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = new BigDecimal(2);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from closed loop subwallet", groups = { "regression" })
	public void TC007_closedLoopSubWalletWithdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = new BigDecimal(2);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from all the subwallets", groups = { "regression" })
	public void TC008_allSubWalletWithdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = new BigDecimal(35);
		BigDecimal subWalletAmount = new BigDecimal(5);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount()
				.withiNTERNATIONAL_FUNDS_TRANSFER(subWalletAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(subWalletAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from all the subwallets, including main", groups = { "regression",
			"sanity" })
	public void TC009_allSubWalletWithMainWithdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = new BigDecimal(45);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from only main wallet", groups = { "regression" })
	public void TC010_mainWalletWithdraw() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 0);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test Case to verify Withdraw from Merchant which do not exist SubWallets", groups = {
			"regression" })
	public void TC011_subWalletWithdraw() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOSUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(45);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().setUpdateAmountManager(false);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withgIFT(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withtOLL(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withiNTERNATIONAL_FUNDS_TRANSFER(5);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfUEL(5);
		FundThreadManager.getInstance().withClosedLoopSubWalletAmount(new BigDecimal(5))
				.withClosedLoopWalletAmount(new BigDecimal(5)).withTotalAmount(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOSUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.NOSUBWALLETMERCHANT);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Executing Test Case to test Withdraw from User with insufficient balance in main wallet", groups = {
			"regression" })
	public void TC012_insufficientMainWalletWithdraw() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = new BigDecimal(50);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().setUpdateAmountManager(false);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(5);
		CheckUserBalance beforeCheckUserBalance = subWalletHelpers.fetchCheckBalanceResponse(mGuid, token);
		Map<String, BigDecimal> userMap = subWalletHelpers.fetchSubWalletBalance(beforeCheckUserBalance);
		subWalletHelpers.adjustFundsSubWallets(userMap, mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
	}

	@Test(description = "Executing Test Case to test Withdraw from User with insufficient balance in sub wallet but in main wallet", groups = {
			"regression" })
	public void TC013_mainWithInsufficientSubWalletWithdraw() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().setUpdateAmountManager(false);
		BigDecimal totalAmount = new BigDecimal(100);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(50);
		FundThreadManager.getInstance().withFoodWalletAmount(new BigDecimal(40)).withTotalAmount(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Executing Test Case to test Withdraw from User based on priority", groups = { "regression" })
	public void TC014_prioritySubWalletWithdraw() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = new BigDecimal(20);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(10);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_SUB_WALLET(10);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Executing Test Case to test Withdraw from User if a negative amount is given", groups = {
			"regression" })
	public void TC015_negativeAmountSubWalletWithdraw() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		BigDecimal totalAmount = new BigDecimal(30);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(-10);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withcLOSED_LOOP_WALLET(10);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
	}

	@Test(description = "Executing Test Case to test Withdraw from when Amount Limit is breached", groups = {
			"regression" })
	public void TC016_AmountBreachSubWalletWithdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		BigDecimal totalAmount = new BigDecimal(30);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETAMOUNTBREACH,
				MerchantHeaders.MERCHANTGUID);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETAMOUNTBREACH,
				MerchantHeaders.NAME);
		params.put(DBEnums.MERCHANTID, mid);
		params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
		params.put(DBEnums.USERID, custId);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		issuerMapping.subwalletAmountLimitBreach(params);
		String[] issuerId = new String[] { "SUBWALLETISSUERAMOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETAMOUNTBREACH,
				issuerId);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
		issuerMapping.resetLimit();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETAMOUNTBREACH);
	}

	@Test(description = "Executing Test Case to test Withdraw from when Count Limit is breached", groups = {
			"regression" })
	public void TC017_CountBreachSubWalletWithdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		BigDecimal totalAmount = new BigDecimal(20);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETCOUNTBREACH,
				MerchantHeaders.NAME);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETCOUNTBREACH,
				MerchantHeaders.MERCHANTGUID);
		params.put(DBEnums.MERCHANTID, mid);
		params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
		params.put(DBEnums.USERID, custId);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		issuerMapping.subwalletCountLimitBreach(params);
		String[] issuerId = new String[] { "SUBWALLETISSUERCOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETCOUNTBREACH,
				issuerId);
		walletWithdraw.createRequestJsonAndExecute();
		issuerMapping.resetLimit();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETCOUNTBREACH);
	}

	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [C]", groups = {
			"regression", "sanity" })
	public void TC018_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from issuer only if if Acquirer merchant category is mapped with the one in subwallet_issuer_config Table for Merchant with config [I] = [A] [C]", groups = {
			"regression" })
	public void TC019_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER2, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER2,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from issuer as well as merchant for Merchant with config [I] = [A] [C]", groups = {
			"regression" })
	public void TC020_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(260);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER", "NOCATEGORYMERCHANT" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER3, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER,
				MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER3,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from acquirer whose category is null in the merchant Table and mapping present in issuer table for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC021_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER2);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from issuer only if if Acquirer merchant category is mapped with the one in subwallet_issuer_config Table for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC022_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER2);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY2,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from issuer as well as merchant for Merchant with config [I] = [A] [ ]", groups = {
			"regression" })
	public void TC023_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(260);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER2", "NOCATEGORYMERCHANT" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER2,
				MerchantTypes.NOCATEGORYMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY3,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from Merchant with config [I] = [ ] [ ] and withdraw is done on Merchant  whose category is null in the merchant Table.", groups = {
			"regression" })
	public void TC024_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY4,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal totalAmount = new BigDecimal(100);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETACQUIRERBLANKCATEGORY4" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber,
				MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY4, issuerId);
		walletWithdraw.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETACQUIRERBLANKCATEGORY4);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
	}

	@Test(description = "Test case to verify withdraw Normal merchant with no entry in SUBWALLET_ISSUER_CONFIG table (But accepts food as mapped with wallet category mapping)", groups = {
			"regression" })
	public void TC025_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(300);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER4", "SUBWALLETWITHDRAWACQUIRER5",
				"SUBWALLETWITHDRAWACQUIRER6" };
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER4, issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
				MerchantTypes.SUBWALLETWITHDRAWACQUIRER5, MerchantTypes.SUBWALLETWITHDRAWACQUIRER6);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER4,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to verify withdraw from  Merchant category mapped with Subwallet_issuer_config table define issuer (For acquirer not present i:e [ I ] = [ ] [ C ]", groups = {
			"regression" })
	public void TC026_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER5,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER5" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER5,
				issuerId);
		walletWithdraw.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER5);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
	}

	@Test(description = "Test case to verify withdraw from  Merchant category mapped with Subwallet_issuer_config table define issuer (For acquirer not present i:e  but SIC configurations is  i:e [ I ] = [ A ] [ C ]", groups = {
			"regression" })
	public void TC027_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER6,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal totalAmount = new BigDecimal(20);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWACQUIRER6" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER5,
				issuerId);
		walletWithdraw.createRequestJsonAndExecute();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER6);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
	}

	@Test(description = "Test case to verify withdraw from 2 issuers who have the same acquirer)", groups = {
			"regression" })
	public void TC028_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(180.0);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER3", "SUBWALLETWITHDRAWISSUER4" };
		subWalletHelpers.callCheckUserBalanceLimitReset(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER3,
				MerchantTypes.SUBWALLETWITHDRAWISSUER4);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test Case to verify Withdraw of Prime User for Merchant for which Callback is configured", groups = {
			"regression" })
	public void TC29_withdrawWithValidateCashBack() {
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		MerchantCallbackHelpers merchantCallback = new MerchantCallbackHelpers();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest,
				UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN));
		walletAPIHelpers.mainWalletAdd(new BigDecimal(Double.parseDouble("100")), mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(MerchantManager.getInstance()
				.getMerchantDetails(MerchantTypes.MERCHANTCALLBACK, MerchantHeaders.MERCHANTGUID));
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		merchantCallback.validateCallbackUrl(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Min Kyc", groups = { "regression",
			"sanity" })
	public void TC030_withdrawForMinKycUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_MIN_KYC);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Aadhar OTP User", groups = {
			"regression", "sanity" })
	public void TC031_withdrawForAadharOTPUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Primitive User", groups = { "regression",
			"sanity" })
	public void TC032_withdrawForPrimitiveUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIMITIVE);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Check Withdraw Transaction for Prime User when wallet Balance is Less than Transaction Amount", groups = {
			"regression" })
	public void TC033_withdrawAmountLesserThanWalletAmount() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		CheckBalance checkBalance = new CheckBalance(token);
		checkBalance.createRequestJsonAndExecute();
		BigDecimal walletCurrentBal = checkBalance.getResponsePojo().getResponse().getAmount();
		BigDecimal totalAmount = walletCurrentBal.add(new BigDecimal(10));
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1006);
	}

	@Test(description = "Check Withdraw Transaction for Prime User to Wrong Merchant", groups = { "regression" })
	public void TC034_withdrawToWrongMerchantGUID() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = CommonHelpers.getInstance().getRandomString();
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1003);
	}

	@Test(description = "Check Withdraw Transaction for Prime User with Negative Amount", groups = { "regression" })
	public void TC035_withdrawNegativeAmountFromUser() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(-10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1007);
	}

	@Test(description = "Check Withdraw Transaction for Prime User with Wrong Currency Code", groups = { "regression" })
	public void TC036_withdrawAmountWithInvalidCurrencyCode() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal totalAmount = new BigDecimal(10);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withCurrencyCode("INRdd");
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1018);
	}

	@Test(description = "Test case to Verify that user is able to withdraw with Amount in fractions", groups = {
			"regression" })
	public void TC037_withdrawWithAmountInFraction() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(5.01);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		totalAmount = CommonHelpers.getInstance().getDecimalRoundedOff(totalAmount, 2);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with expired token", groups = {
			"regression" })
	public void TC038_withdrawWithExpiredToken() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = "39f2a2a2-379f-43d8-9710-982f8esd2ecffa";
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.CODE_403);
	}

	@Test(description = "Test case to Verify that user is not able to withdraw with invalid token", groups = {
			"regression" })
	public void TC039_withdrawWithInvalidToken() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = "457fec20-89e9-4766-a4b0-0e3fba896ss4dc";
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.CODE_403);
	}

	@Test(description = "Test case to Verify that user is not able to withdraw to inactive merchant", groups = {
			"regression" })
	public void TC040_withdrawToInactiveMerchant() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.INACTIVEMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1035);
	}

	@Test(description = "Test case to Verify that user is not able to withdraw Amount when merchant order id is repeated", groups = {
			"regression" })
	public void TC041_withdrawWithRepeatedMerchantOrderID() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withMerchantOrderId("11");
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1011);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw Amount when same Txn id is associated with multipal merchants", groups = {
			"regression" })
	public void TC042_withdrawWithSameTxnIDForTwoMerchants() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withpgTxnId("11");
		walletWithdraw.createRequestJsonAndExecute();
		String mGuid1 = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid1);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1007);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw to merchant with null GUID", groups = {
			"regression" })
	public void TC043_withdrawAmountWithMerchantGUIDisNull() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(null);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.WM_1003);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw to merchant with null Amount", groups = {
			"regression" })
	public void TC044_withdrawWithNullAmount() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(null);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1007);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with blank Currency Code", groups = {
			"regression" })
	public void TC045_withdrawAmountWithCurrencyCodeisBlank() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withCurrencyCode("");
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1018);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with null Currency Code", groups = {
			"regression" })
	public void TC046_withdrawAmountWithCurrencyCodeisNull() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SCWMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withCurrencyCode(null);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.GE_1018);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with Invalid Operation type", groups = {
			"regression" })
	public void TC047_withdrawAmountWithInvalidOprationType() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().withOperationType("WITHDRAW_MONEY1");
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(walletWithdraw, APIStatus.WalletWithdraw.BAD_REQUEST);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with Blank Operation type", groups = {
			"regression" })
	public void TC048_withdrawAmountWithBlankOprationType() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().withOperationType(" ");
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(walletWithdraw, APIStatus.WalletWithdraw.BAD_REQUEST);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with Blank SSO Token", groups = {
			"regression" })
	public void TC049_withdrawAmountWithBlankSSOToken() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, "");
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(walletWithdraw, APIStatus.WalletWithdraw.UNAUTHORIZED_ACCESS);

	}

	@Test(description = "Test case to Verify that Inactive user is not able to withdraw ", groups = { "regression" })
	public void TC050_withdrawAmountForInactiveUser() {
		UserManager.getInstance().getNewPayerUser(UserTypes.INACTIVEWALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(walletWithdraw, APIStatus.WalletWithdraw.GE_1026);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw when amount is zero", groups = {
			"regression" })
	public void TC051_withdrawZeroAmountFromUser() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(0);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().httpCodeAssert(walletWithdraw, APIStatus.WalletWithdraw.GE_1007);
	}

	@Test(description = "Test case to Verify that user is able to withdraw with Amount in USD Currency", groups = {
			"regression" })
	public void TC052_withdrawAmountWithCurrencyUSD() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withCurrencyCode("USD");
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw to merchant set to active", groups = {
			"regression" })
	public void TC053_withdrawAmountWhenOfflineMerchantSetToActive() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.OFFLINEACTIVEMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.OFFLINEACTIVEMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.OFFLINEACTIVEMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw to PPC Enabled merchant", groups = {
			"regression" })
	public void TC054_withdrawAmountToPPCEnabledmerchant() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PPCEnabledMerchant,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.PPCEnabledMerchant);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PPCEnabledMerchant,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw to Post convenience Enabled merchant", groups = {
			"regression" })
	public void TC055_withdrawAmountToPostConvEnabledmerchant() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PostConvEnabledMerchant,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.PostConvEnabledMerchant);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PostConvEnabledMerchant,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw to Pre convenience Enabled merchant", groups = {
			"regression" })
	public void TC056_withdrawAmountToPreConvEnabledmerchant() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.REGRESSIONMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.SUCCESS);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw with valid otp", groups = { "regression" })
	public void TC057_withdrawWithValidOTPUser() throws InterruptedException, JSchException {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.PAYTMSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.REGRESSIONMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp = otpHelpers.FetchOtp(mobileNumber, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with Invalid OTP", groups = {
			"regression" })
	public void TC058_withdrawWithInvalidOTPUser(){
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.REGRESSIONMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.PAYTMSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest, token);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setToken(token);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.getRequestPojo().getRequest().withOtp("000000");
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_INVALID);
	}

}
