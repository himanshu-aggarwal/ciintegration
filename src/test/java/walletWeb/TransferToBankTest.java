package walletWeb;

import com.paytm.apiRequestBuilders.wallet.walletWeb.TransferToBank;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.constants.UserTypeExt;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.CommonValidation;
import com.paytm.helpers.SubWalletHelpers;
import com.paytm.helpers.TransferToBankHelpers;
import com.paytm.helpers.WalletAPIHelpers;
import com.paytm.utils.DBValidation;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-TransferToBank")

public class TransferToBankTest extends WalletBaseTest {

	@Test(description = "Test case to verify TransferToBank of Prime and Aadhar OTP Kyc User", groups = { "regression",
			"sanity", "smoke" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC001_transferToBank(String userType) {
		SubWalletHelpers subwallet = new SubWalletHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		DBValidation db = new DBValidation();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		TransferToBank transferToBank = new TransferToBank(TransferToBank.defaultRequest, token);
		transferToBank.getRequestPojo().getRequest().withContactNo(phone).withAmount(new BigDecimal(10));
		walletAPIHelpers.addBalanceToMainWalletDirectly(new BigDecimal(10), phone);
		subwallet.createDebitTxnMap();
		transferToBank.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(transferToBank, APIStatus.TransferToBank.SUCCESS);
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.WALLETSYSTEMTXNID,
				transferToBank.getResponsePojo().getResponse().getWalletSysTransactionId());
		params.put(DBEnums.TXNAMOUNT, transferToBank.getRequestPojo().getRequest().getAmount());
		params.put(DBEnums.TXNTYPE, "29");
		params.put(DBEnums.TXNSTATUS, "1");
		db.isEntryPresent(DBQueries.nstrTxnTypeVerification, params);
		subwallet.validateCheckUserBalance();
	}

	@Test(description = "Test case to verify TransferToBank of Users other than Prime and Aadhar OTP Kyc User", groups = {
			"regression", "sanity", "smoke" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ALLEXCEPTPRIMEANDADHAAROTPKYC)
	public void TC002_transferToBank(String userType) {
		SubWalletHelpers subwallet = new SubWalletHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		TransferToBank transferToBank = new TransferToBank(TransferToBank.defaultRequest, token);
		transferToBank.getRequestPojo().getRequest().withContactNo(phone).withAmount(new BigDecimal(10));
		walletAPIHelpers.addBalanceToMainWalletDirectly(new BigDecimal(10), phone);
		subwallet.callCheckUserBalanceInsufficientBalance();
		transferToBank.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(transferToBank, APIStatus.TransferToBank.FAILURE);
		subwallet.validateCheckUserBalance();
	}

	@Test(description = "Test case to verify TransferToBank of Users for User going from Prime to Closed", groups = {
			"regression", "sanity", "smoke" })
	public void TC003_transferToBank() {
		SubWalletHelpers subwallet = new SubWalletHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		TransferToBankHelpers transferToBankHelpers = new TransferToBankHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		transferToBankHelpers.upgradeWallet(phone, "PAYTM_CLOSED");
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		TransferToBank transferToBank = new TransferToBank(TransferToBank.defaultRequest, token);
		transferToBank.getRequestPojo().getRequest().withContactNo(phone).withAmount(new BigDecimal(10));
		walletAPIHelpers.addBalanceToMainWalletDirectly(new BigDecimal(10), phone);
		subwallet.callCheckUserBalanceInsufficientBalance();
		transferToBank.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(transferToBank, APIStatus.TransferToBank.FAILURE);
		subwallet.validateCheckUserBalance();
		transferToBankHelpers.upgradeWallet(phone, "PAYTM_PRIME_WALLET");
	}

	@Test(description = "Test case to verify TransferToBank of All Users when Failure Response is coming from PG", groups = {
			"regression", "sanity", "smoke" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC004_transferToBank(String userType) {
		SubWalletHelpers subwallet = new SubWalletHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		DBValidation db = new DBValidation();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		TransferToBank transferToBank = new TransferToBank(TransferToBank.defaultRequest, token);
		transferToBank.getRequestPojo().getRequest().withContactNo(phone).withAmount(new BigDecimal(10))
				.withIfscCode("HDFC0000");
		walletAPIHelpers.addBalanceToMainWalletDirectly(new BigDecimal(10), phone);
		subwallet.callCheckUserBalanceInsufficientBalance();
		transferToBank.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(transferToBank, APIStatus.TransferToBank.P2B_8000);
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.WALLETSYSTEMTXNID,
				transferToBank.getResponsePojo().getResponse().getWalletSysTransactionId());
		params.put(DBEnums.TXNAMOUNT, transferToBank.getRequestPojo().getRequest().getAmount());
		params.put(DBEnums.TXNTYPE, "29");
		params.put(DBEnums.TXNSTATUS, "2");
		db.isEntryPresent(DBQueries.nstrTxnTypeVerification, params);
		params.put(DBEnums.TXNSTATUS, "1");
		params.put(DBEnums.TXNTYPE, "8");
		db.isEntryPresent(DBQueries.nstrRollbackTxn, params);
		subwallet.validateCheckUserBalance();
	}

	@Test(description = "Test case to verify TransferToBank of All Users when Pending Response is coming from PG", groups = {
			"regression", "sanity", "smoke" }, dataProvider = "fetchUserType")
	@UserTypeExt(UserNameProvided = WalletRbiTypes.ONLYPRIMEANDADHAAROTPKYC)
	public void TC005_transferToBank(String userType) {
		SubWalletHelpers subwallet = new SubWalletHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		DBValidation db = new DBValidation();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.valueOf(userType));
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		TransferToBank transferToBank = new TransferToBank(TransferToBank.defaultRequest, token);
		transferToBank.getRequestPojo().getRequest().withContactNo(phone).withAmount(new BigDecimal(10))
				.withIfscCode("HDFC0001");
		walletAPIHelpers.addBalanceToMainWalletDirectly(new BigDecimal(10), phone);
		subwallet.createDebitTxnMap();
		transferToBank.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(transferToBank, APIStatus.TransferToBank.USD_01);
		Map<Object, Object> params = new HashMap<>();
		params.put(DBEnums.WALLETSYSTEMTXNID,
				transferToBank.getResponsePojo().getResponse().getWalletSysTransactionId());
		params.put(DBEnums.TXNAMOUNT, transferToBank.getRequestPojo().getRequest().getAmount());
		params.put(DBEnums.TXNTYPE, "29");
		params.put(DBEnums.TXNSTATUS, "3");
		db.isEntryPresent(DBQueries.nstrTxnTypeVerification, params);
		subwallet.validateCheckUserBalance();
	}

	@Test(description = "Test case to verify TransferToBank of user with duplicate merchantOrderId", groups = {
			"regression" })
	public void TC006_transferToBank() {
		SubWalletHelpers subwallet = new SubWalletHelpers();
		WalletAPIHelpers walletAPIHelpers = new WalletAPIHelpers();
		String phone = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		TransferToBank transferToBank = new TransferToBank(TransferToBank.defaultRequest, token);
		transferToBank.getRequestPojo().getRequest().withContactNo(phone).withAmount(new BigDecimal(10));
		walletAPIHelpers.addBalanceToMainWalletDirectly(new BigDecimal(10), phone);
		transferToBank.createRequestJsonAndExecute();
		String merchantOrderId = transferToBank.getRequestPojo().getRequest().getMerchantOrderId();
		TransferToBank transferToBankNew = new TransferToBank(TransferToBank.defaultRequest, token);
		transferToBankNew.getRequestPojo().getRequest().withContactNo(phone).withAmount(new BigDecimal(10))
				.withMerchantOrderId(merchantOrderId);
		walletAPIHelpers.addBalanceToMainWalletDirectly(new BigDecimal(10), phone);
		subwallet.callCheckUserBalanceInsufficientBalance();
		transferToBankNew.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(transferToBankNew, APIStatus.TransferToBank.P2B_2014);
		subwallet.validateCheckUserBalance();
	}
}
