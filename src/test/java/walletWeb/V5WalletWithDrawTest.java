package walletWeb;

import com.jcraft.jsch.JSchException;
import com.paytm.apiRequestBuilders.wallet.walletWeb.ResendOTPWithdraw;
import com.paytm.apiRequestBuilders.wallet.walletWeb.ValidateOTPWithdraw;
import com.paytm.apiRequestBuilders.wallet.walletWeb.WalletWithDraw;
import com.paytm.constants.ClassNameExt;
import com.paytm.constants.DBQueries;
import com.paytm.datamanager.MerchantManager;
import com.paytm.datamanager.UserManager;
import com.paytm.enums.*;
import com.paytm.helpers.*;
import com.paytm.utils.OfflinePaymentTokenAnalyzer;
import com.paytm.utils.WalletBaseTest;
import com.paytm.utils.reporting.ExtentTestNGITestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Listeners(ExtentTestNGITestListener.class)
@ClassNameExt(ClassNameProvided = "WalletWeb-V5WalletWithdraw")
public class V5WalletWithDrawTest extends WalletBaseTest {

	@Test(description = "Test case to verify V2 Withdraw of Basic User", groups = { "regression", "sanity" })
	public void TC001_v2ValidateBasicUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(otpwithdraw.getResponsePojo().getResponse().getTxnAmount());
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to verify V2 Withdraw of Prime User", groups = { "regression", "sanity" })
	public void TC002_v2ValidatePrimeUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(otpwithdraw.getResponsePojo().getResponse().getTxnAmount());
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to verify V2 Withdraw from food subwallet", groups = { "regression", "sanity" })
	public void TC003_foodSubWalletV2Withdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String custId = UserManager.getInstance().getSpecificUserDetail(UserHeaders.CUSTID, mobileNumber);
		Map<String, BigDecimal> userSubWalletType = new HashMap<>();
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(2);
		userSubWalletType.put("FOOD", totalAmount);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.setPhone(mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.SUBWALLETMERCHANT);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(mobileNumber);
		;
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(mobileNumber, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap((String) otpwithdraw.getResponsePojo().getResponse().getWalletSysTransactionId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.SS_0001);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETMERCHANT);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETMERCHANT);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Min Kyc", groups = { "regression",
			"sanity" })
	public void TC004_withdrawForMinKycUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_MIN_KYC);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(otpwithdraw.getResponsePojo().getResponse().getTxnAmount());
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Aadhar OTP User", groups = {
			"regression", "sanity" })
	public void TC005_withdrawForAadharOTPUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_ADHAAR_OTP_KYC);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(otpwithdraw.getResponsePojo().getResponse().getTxnAmount());
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is able to withdraw for Primitive User", groups = { "regression",
			"sanity" })
	public void TC006_withdrawForPrimitiveUser() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIMITIVE);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(otpwithdraw.getResponsePojo().getResponse().getTxnAmount());
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with Invalid OTP", groups = {
			"regression" })
	public void TC007_withdrawWithInvalidOTPUser() throws IOException, InterruptedException, JSchException {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		otpwithdraw.getRequestPojo().getRequest().withOtp("0000000");
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_INVALID);
	}

	@Test(description = "Test case to Verify ressend OTP response", groups = { "regression", "sanity" })
	public void TC008_verifyResendOTPResponse() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(resendOtpObj, APIStatus.WalletWithdraw.RESENDOTPSUCCESS);
	}

	@Test(description = "Test case to Verify that user is able to withdraw after resneding OTP", groups = {
			"regression", "sanity" })
	public void TC009_withdrawWithResendOTP() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(otpwithdraw.getResponsePojo().getResponse().getTxnAmount());
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "Test case to Verify that user is not able to resend  OTP after validating OTP for same state", groups = {
			"regression", "sanity" })
	public void TC010_resendingOTPforValidatedState() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		otpwithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj1 = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj1.setToken(token);
		resendOtpObj1.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		resendOtpObj1.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(resendOtpObj1, APIStatus.WalletWithdraw.OTP_1005);

	}

	@Test(description = "Test case to Verify that user is not able to withdraw with invalid phone", groups = {
			"regression", "sanity" })
	public void TC011_withdrawWithInvalidPhone() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = "0000000000";
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		// SubWalletHelpers subWalletHelpers=new SubWalletHelpers();
		// subWalletHelpers.callCheckUserBalance(phone,
		// MerchantTypes.NOCATEGORYMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(walletWithdraw, APIStatus.WalletWithdraw.ERROR_404);

	}

	@Test(description = "Test case to Verify that user is not able to resend  OTP after validating OTP for same state", groups = {
			"regression", "sanity" })
	public void TC012_resendingOTPWithInvalidState() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ResendOTPWithdraw resendOtpObj = new ResendOTPWithdraw(ResendOTPWithdraw.defaultRequest);
		resendOtpObj.setToken(token);
		resendOtpObj.getRequestPojo().getRequest().withState("InvalidState");
		// this is to just generate a new OTP and verify through OTP validator.
		resendOtpObj.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(resendOtpObj, APIStatus.WalletWithdraw.GE_0003);

	}

	@Test(description = "Executing Test Case to test Withdraw from when Amount Limit is breached", groups = {
			"regression" })
	public void TC013_AmountBreachSubWalletWithdraw() {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		BigDecimal totalAmount = new BigDecimal(30);
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETAMOUNTBREACH,
				MerchantHeaders.MERCHANTGUID);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETAMOUNTBREACH,
				MerchantHeaders.NAME);
		params.put(DBEnums.MERCHANTID, mid);
		params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
		params.put(DBEnums.USERID, custId);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.setPhone(mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		issuerMapping.subwalletAmountLimitBreach(params);
		String[] issuerId = new String[] { "SUBWALLETISSUERAMOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETAMOUNTBREACH,
				issuerId);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpWithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpWithdraw.setPhone(mobileNumber);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(mobileNumber, "Offline_payment_OTP");
		otpWithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpWithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpWithdraw, APIStatus.WalletWithdraw.WM_1006);
		issuerMapping.resetLimit();
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETAMOUNTBREACH);
	}

	@Test(description = "Executing Test Case to test Withdraw from when Count Limit is breached", groups = {
			"regression" })
	public void TC014_CountBreachSubWalletWithdraw() throws IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		IssuerMappingSubWallet issuerMapping = new IssuerMappingSubWallet();
		Map<Object, Object> params = new HashMap<>();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		BigDecimal totalAmount = new BigDecimal(20);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String custId = UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETCOUNTBREACH,
				MerchantHeaders.NAME);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETCOUNTBREACH,
				MerchantHeaders.MERCHANTGUID);
		params.put(DBEnums.MERCHANTID, mid);
		params.put(DBEnums.MERCHANTGUID, mGuid.replace("-", ""));
		params.put(DBEnums.USERID, custId);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.setPhone(mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		issuerMapping.subwalletCountLimitBreach(params);
		String[] issuerId = new String[] { "SUBWALLETISSUERCOUNTBREACH" };
		subWalletHelpers.callCheckUserBalanceInsufficientBalance(mobileNumber, MerchantTypes.SUBWALLETCOUNTBREACH,
				issuerId);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpWithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpWithdraw.setPhone(mobileNumber);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(mobileNumber, "Offline_payment_OTP");
		otpWithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpWithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpWithdraw.createRequestJsonAndExecute();
		issuerMapping.resetLimit();
		CommonValidation.getInstance().basicAsserts(otpWithdraw, APIStatus.WalletWithdraw.WM_1006);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETCOUNTBREACH);
	}

	@Test(description = "Test case to verify withdraw from 2 issuers who have the same acquirer)", groups = {
			"regression" })
	public void TC015_foodSubWalletLimitWithdraw() throws NumberFormatException, IOException {
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserSubwalletConsolidated userSubwalletConsolidated = new UserSubwalletConsolidated();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				MerchantHeaders.MERCHANTGUID);
		String mid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				MerchantHeaders.NAME);
		BigDecimal totalAmount = new BigDecimal(180.0);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.subWalletRequest);
		walletWithdraw.setPhone(mobileNumber);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid).withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().getSubWalletAmount().withfOOD(totalAmount);
		String[] issuerId = new String[] { "SUBWALLETWITHDRAWISSUER3", "SUBWALLETWITHDRAWISSUER4" };
		subWalletHelpers.callCheckUserBalanceLimitReset(mobileNumber, MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
				issuerId);
		userSubwalletConsolidated.fetchConsolidatedData(mid, mGuid.replace("-", ""),
				UserManager.getInstance().getPayerUserDetails(UserHeaders.CUSTID));
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpWithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpWithdraw.setPhone(mobileNumber);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(mobileNumber, "Offline_payment_OTP");
		otpWithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpWithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpWithdraw.createRequestJsonAndExecute();
		List<Map<String, Object>> extendedDetails = userSubwalletConsolidated
				.extendedDetailMap(walletWithdraw.getResponsePojo().getResponse().getWalletSystemTxnId());
		userSubwalletConsolidated.validateSubwalletConsolidate(extendedDetails, false);
		CommonValidation.getInstance().basicAsserts(otpWithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		CommonValidation.getInstance()
				.nstrTxnValidation(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(), 1, 1);
		subWalletHelpers.extendedPpiInfoVerification(walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId(),
				DBQueries.withdrawExtendedPpiInfoVerificationOrderId, MerchantTypes.SUBWALLETWITHDRAWISSUER3,
				MerchantTypes.SUBWALLETWITHDRAWISSUER4);
		subWalletHelpers.validateCheckUserBalance(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8);
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.SubWalletMain.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.SUBWALLETWITHDRAWACQUIRER8,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}

	@Test(description = "Test case to Verify that user is able to withdraw to Post convenience Enabled merchant", groups = {
			"regression" })
	public void TC016_withdrawAmountToPostConvEnabledmerchant() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		String mobileNumber = UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PostConvEnabledMerchant,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(mobileNumber);
		BigDecimal totalAmount = mainWalletAmount;
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(mobileNumber, MerchantTypes.PostConvEnabledMerchant);
		limitAudit.setBeforeJsonAndTimeStamp(mobileNumber);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpWithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpWithdraw.setPhone(mobileNumber);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(mobileNumber, "Offline_payment_OTP");
		otpWithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpWithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpWithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpWithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = walletWithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(walletWithdraw.getResponsePojo().getResponse().getTxnAmount());
		limitAudit.validateLimitAuditInfo(mobileNumber, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.PostConvEnabledMerchant,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());

	}

	@Test(description = "case to verify when withdraw nad validate hit with different phone number", groups = {
			"regression" })
	public void TC017_WithdrawAndValidateWithDifferentnumber() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		String phonenumber = UserManager.getInstance().getNewPayeeUser(UserTypes.NUMBERWITH5);
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phonenumber);
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_INVALID);
	}

	@Test(description = "case to verify when phone number is blank in headers", groups = { "regression" })
	public void TC018_WhenPhoneNumberIsBlankInHeaders() {
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_PRIME_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		walletWithdraw.createRequestJsonAndExecute();
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone("");
		OtpHelpers otpHelpers = new OtpHelpers();
		String otp;
		otp = otpHelpers.FetchOtp(phone, "Offline_payment_OTP");
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp);
		otpwithdraw.getRequestPojo().getRequest()
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.UNAUTHORIZED_ACCESS);
	}

	@Test(description = "case to verify when passing totp of the user in validate transaction in otp field", groups = {
			"regression" })
	public void TC019_passingTotpInValidateTransaction() {
		LimitAuditInfoValidation limitAudit = new LimitAuditInfoValidation();
		UserManager.getInstance().getNewPayerUser(UserTypes.PAYTM_BASIC_WALLET);
		String mGuid = MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
				MerchantHeaders.MERCHANTGUID);
		String token = UserManager.getInstance().getPayerUserDetails(UserHeaders.WALLETSCOPETOKEN);
		String phone = UserManager.getInstance().getPayerUserDetails(UserHeaders.MOBILENUMBER);
		BigDecimal mainWalletAmount = new BigDecimal(10);
		WalletWithDraw walletWithdraw = new WalletWithDraw(WalletWithDraw.defaultRequest);
		walletWithdraw.setPhone(phone);
		BigDecimal totalAmount = mainWalletAmount;// ?
		walletWithdraw.getRequestPojo().getRequest().withTotalAmount(totalAmount);
		walletWithdraw.getRequestPojo().getRequest().withMerchantGuid(mGuid);
		SubWalletHelpers subWalletHelpers = new SubWalletHelpers();
		subWalletHelpers.callCheckUserBalance(phone, MerchantTypes.NOCATEGORYMERCHANT);
		limitAudit.setBeforeJsonAndTimeStamp(phone);
		walletWithdraw.createRequestJsonAndExecute();
		String otp = OfflinePaymentTokenAnalyzer.getNewOfflineTotp(token);
		ValidateOTPWithdraw otpwithdraw = new ValidateOTPWithdraw(ValidateOTPWithdraw.defaultRequest);
		otpwithdraw.setPhone(phone);
		otpwithdraw.getRequestPojo().getRequest().withOtp(otp)
				.withState(walletWithdraw.getResponsePojo().getResponse().getState().toString());
		otpwithdraw.createRequestJsonAndExecute();
		CommonValidation.getInstance().basicAsserts(otpwithdraw, APIStatus.WalletWithdraw.OTP_VALIDATION);
		String merchantOrderId = otpwithdraw.getResponsePojo().getResponse().getMerchantOrderId();
		BigDecimal txnAmount = new BigDecimal(otpwithdraw.getResponsePojo().getResponse().getTxnAmount());
		CommonValidation.getInstance().nstrTxnValidation(merchantOrderId, 1, 0);
		limitAudit.validateLimitAuditInfo(phone, 1, 1, (short) 1);
		ScratchPadValidator.getInstance().validateScratchPad(ScratchPad.MainOnly.Transaction_1,
				MerchantManager.getInstance().getMerchantDetails(MerchantTypes.NOCATEGORYMERCHANT,
						MerchantHeaders.MERCHANTWALLETGUID),
				walletWithdraw.getRequestPojo().getRequest().getMerchantOrderId());
	}
}
